//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
void WriteTo()
{ DWORD temp;

  WriteFile(Port,wr_buf,wr_buf_len,&temp,&ovr_wr); //������� � ���� ������
  WaitForSingleObject(ovr_wr.hEvent,10000);        //������������� ����� �� 10 � ��� ���� �� ���������� �������� ������ � ����
  GetOverlappedResult(Port,&ovr_wr,&temp,true);
}
//---------------------------------------------------------------------------
void OpenPort()
{ dcb.DCBlength=sizeof(DCB);
  //������� ��������� ����. ����� ������� ���� �� nt-��������� ���� ���������
  // "\\.\PortName", � �� ��� ������ ������� \ ���� ��� ��������� 2 ����
  char PortName[100]="\\\\.\\";
  strcat(PortName,Form1->ComboBox1->Text.c_str());
  Port=CreateFile(PortName,GENERIC_READ|GENERIC_WRITE,0,NULL,OPEN_EXISTING,FILE_FLAG_OVERLAPPED,NULL);
  if(Port==INVALID_HANDLE_VALUE)
  { MessageBox(NULL,"������ �������� �����","Error",MB_OK);
    return;
  }
  else
  { if(GetCommState(Port,&dcb)==0)
    { MessageBox(NULL,"������ ��������� ���������� �����","Error",MB_OK);
      CloseHandle(Port);
      Port=INVALID_HANDLE_VALUE;
      return;
    }
    else
    { dcb.BaudRate=115200;
      dcb.ByteSize=8;
      dcb.Parity=0;
      dcb.StopBits=2;
      dcb.fBinary=true;          //�������� ����� ������
      dcb.fOutxCtsFlow=false;    //�� ����������� CTS
      dcb.fOutxDsrFlow=false;    //�� ����������� DSR
      dcb.fDtrControl=DTR_CONTROL_DISABLE;     //��������� DTR
      dcb.fDsrSensitivity=false;               //������ �� DSR
      dcb.fNull=false;           //��������� ������� �����
      dcb.fRtsControl=RTS_CONTROL_DISABLE;    //��������� RTS
      dcb.fAbortOnError=false;   //��������� ��������� ������/������ ��� ������
   }
   if(SetCommState(Port,&dcb)==0)
   { MessageBox(NULL,"������ ��������� ���������� �����","Error",MB_OK);
     CloseHandle(Port);
     Port=INVALID_HANDLE_VALUE;
     return;
   }

   memset(wr_buf,0,BUFSIZE);        //������� ��� ����� ��� ��������
   wr_buf_len=0;                    //� ��� ������
   SetupComm(Port,BUFSIZE+100,BUFSIZE+100);       //������� �������� ����� � ��������
   PurgeComm(Port,PURGE_RXCLEAR|PURGE_TXCLEAR);   //�������� ����������� � ���������� ������ �����

   Form1->Button1->Enabled=true;        // ������ Start
   Form1->Button2->Enabled=true;        // ������ Stop
   Form1->Button3->Enabled=false;       // ������ Connect
   Form1->Button4->Enabled=true;        // ������ Disconnect
   Form1->ComboBox1->Enabled=false;     // ����� �����
  }
}
//---------------------------------------------------------------------------
void ClosePort()
{ CloseHandle(Port);
  Port=INVALID_HANDLE_VALUE;
  Form1->Button1->Enabled=false;        // ������ Start
  Form1->Button2->Enabled=false;        // ������ Stop
  Form1->Button3->Enabled=true;         // ������ Connect
  Form1->Button4->Enabled=false;        // ������ Disconnect
  Form1->ComboBox1->Enabled=true;       // ����� �����
}
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{ Form1->AutoSize=true;
  Shim=16;
  Speed=255;
  PolPairs=3;
  Accel=9;
  MaskEdit1->Text="0.75";
  MaskEdit2->Text="0128";
  MaskEdit3->Text="09";
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{ OpenPort();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button4Click(TObject *Sender)
{ ClosePort();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{ wr_buf_len=1;
  wr_buf[0]=0x01; // start
  WriteTo();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{ wr_buf_len=1;
  wr_buf[0]=0x02; // stop
  WriteTo();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::CheckBox1Click(TObject *Sender)
{ if(CheckBox1->Checked==true)
  { wr_buf_len=1;
    wr_buf[0]=0x04; // reverse
    WriteTo();
  }
  else
  { wr_buf_len=1;
    wr_buf[0]=0x03; // forward
    WriteTo();
  }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::MaskEdit1Change(TObject *Sender)
{ float temp,tempbyte;
  int tempdown, tempup;
  // ��������� ����� �������� ���
  temp=(MaskEdit1->Text[1]&0x0F)*100+(MaskEdit1->Text[3]&0x0F)*10+(MaskEdit1->Text[4]&0x0F);
  if(temp<57)
  { temp=57;
    MaskEdit1->Text="0.57";
  }
  if(temp>993)
  { temp=993;
    MaskEdit1->Text="9.93";
  }
  tempbyte=temp*256/1200;
  tempdown=floor(tempbyte);
  tempup=ceil(tempbyte);
  if(tempdown!=tempup)
  { if((tempbyte-tempdown)<0.5)
      tempup=tempdown;
  }
  // ������ ��������� �������� ���� �����
  if(TrackBar1->Position!=(1200-temp))
     TrackBar1->Position=1200-temp;

  // �������� �����������
  wr_buf_len=2;
  wr_buf[0]=0x05;       // new shim
  wr_buf[1]=tempup;
  WriteTo();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::TrackBar1Change(TObject *Sender)
{ int temp;
  AnsiString tempstr;

  if(TrackBar1->Position>1143)
   TrackBar1->Position=1143;
  if(TrackBar1->Position<207)
   TrackBar1->Position=207;
  // ������ MaskEdit ���� �����
  temp=(MaskEdit1->Text[1]&0x0F)*100+(MaskEdit1->Text[3]&0x0F)*10+(MaskEdit1->Text[4]&0x0F);
  if(temp!=(1200-TrackBar1->Position))
  { tempstr=IntToStr(1200-TrackBar1->Position);
    if(TrackBar1->Position>1100)
      tempstr.Insert("0",1);  // ���� ����� ����������� - ���������� ������� ����
    tempstr.Insert(".",2);
    MaskEdit1->Text=tempstr;
  }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::MaskEdit2Change(TObject *Sender)
{ float tempbyte, temppos;
  int temp, tempdown, tempup, tempposdown, tempposup;
  AnsiString tempstr;
  // ��������� ����� �������� ���
  temp=(MaskEdit2->Text[1]&0x0F)*1000+(MaskEdit2->Text[2]&0x0F)*100+(MaskEdit2->Text[3]&0x0F)*10+(MaskEdit2->Text[4]&0x0F);
  if(temp<128)
  { temp=128;
    MaskEdit2->Text="0128";
  }
  if(temp>4650)
  { temp=4650;
    MaskEdit2->Text="4650";
  }
  tempbyte=20*1000000/(614.4*temp);
  tempdown=floor(tempbyte);
  tempup=ceil(tempbyte);
  if(tempdown!=tempup)
  { if((tempbyte-tempdown)<0.5)
      tempup=tempdown;
  }
  // �������� ����������
  temppos=20*1000000/(614.4*tempup);
  tempposdown=floor(temppos);
  tempposup=ceil(temppos);
  if(tempposdown!=tempposup)
  { if((temppos-tempposdown)<0.5)
      tempposup=tempposdown;
  }
  // ���������
  tempstr=IntToStr(tempposup);
    if(tempposup<1000)
      tempstr.Insert("0",1);  // ���� ����� - ���������� ������� ����
    if(tempposup<100)
      tempstr.Insert("0",1);  // ���� ����� - ���������� ������� ����
    if(tempposup<10)
      tempstr.Insert("0",1);  // ���� ����� - ���������� ������� ����
    MaskEdit2->Text=tempstr;
  // ������ ��������� ��������, ���� �����
  if(TrackBar2->Position!=(5000-tempposup))
     TrackBar2->Position=5000-tempposup;

  // �������� �����������
  wr_buf_len=2;
  wr_buf[0]=0x06;       // new speed
  wr_buf[1]=tempup;
  WriteTo();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::TrackBar2Change(TObject *Sender)
{ AnsiString tempstr;
  tempstr=IntToStr(5000-TrackBar2->Position);
  if((5000-TrackBar2->Position)<1000)
      tempstr.Insert("0",1);  // ���� ����� - ���������� ������� ����
  if((5000-TrackBar2->Position)>100)
      tempstr.Insert("0",1);  // ���� ����� - ���������� ������� ����
  if((5000-TrackBar2->Position)<10)
      tempstr.Insert("0",1);  // ���� ����� - ���������� ������� ����
  //������ MaskEdit, ���� �����
  MaskEdit2->Text=tempstr;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::MaskEdit3Change(TObject *Sender)
{ int temp;
  temp=(MaskEdit3->Text[1]&0x0F)*10+(MaskEdit3->Text[2]&0x0F);
  if(temp>30)
  { temp=30;
    MaskEdit3->Text="30";
  }
  if(temp<1)
  { temp=1;
    MaskEdit3->Text="01";
  }
  if(TrackBar3->Position!=temp)
     TrackBar3->Position=temp;

  // �������� �����������
  wr_buf_len=2;
  wr_buf[0]=0x07;       // new acceleration
  wr_buf[1]=temp;
  WriteTo();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::TrackBar3Change(TObject *Sender)
{ int temp;
  AnsiString tempstr;

  temp=(MaskEdit3->Text[1]&0x0F)*10+(MaskEdit3->Text[2]&0x0F);
  tempstr=IntToStr(TrackBar3->Position);
  if(TrackBar3->Position!=temp)
  { if(TrackBar3->Position<10)
       tempstr.Insert("0",1);
    MaskEdit3->Text=tempstr;
  }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Label1MouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{ Form1->Label1->Cursor=crHandPoint;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Label1Click(TObject *Sender)
{ ShellExecute(Form1->Handle,"open","http://radiohlam.ru/program/bldc_motor_prg.htm",NULL,NULL,SW_RESTORE);
}
//---------------------------------------------------------------------------

