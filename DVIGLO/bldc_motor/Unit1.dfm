object Form1: TForm1
  Left = 1070
  Top = 152
  Width = 169
  Height = 460
  AutoSize = True
  BorderIcons = [biSystemMenu]
  Caption = 'bldc motor control'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 161
    Height = 433
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 130
      Height = 16
      Caption = 'http://radiohlam.ru'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlue
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsItalic, fsUnderline]
      ParentFont = False
      OnClick = Label1Click
      OnMouseMove = Label1MouseMove
    end
    object Button1: TButton
      Left = 8
      Top = 112
      Width = 73
      Height = 25
      Caption = 'Start'
      Enabled = False
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 80
      Top = 112
      Width = 73
      Height = 25
      Caption = 'Stop'
      Enabled = False
      TabOrder = 1
      OnClick = Button2Click
    end
    object CheckBox1: TCheckBox
      Left = 8
      Top = 144
      Width = 73
      Height = 17
      Caption = ' Reverse'
      TabOrder = 2
      OnClick = CheckBox1Click
    end
    object GroupBox1: TGroupBox
      Left = 8
      Top = 168
      Width = 73
      Height = 209
      Caption = ' Voltage '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      object TrackBar1: TTrackBar
        Left = 8
        Top = 51
        Width = 33
        Height = 150
        Max = 1200
        Orientation = trVertical
        Frequency = 1
        Position = 1143
        SelEnd = 0
        SelStart = 0
        TabOrder = 0
        TickMarks = tmBottomRight
        TickStyle = tsAuto
        OnChange = TrackBar1Change
      end
      object MaskEdit1: TMaskEdit
        Left = 8
        Top = 24
        Width = 44
        Height = 22
        EditMask = '!9.99;1;0'
        MaxLength = 4
        TabOrder = 1
        Text = ' .  '
        OnChange = MaskEdit1Change
      end
    end
    object GroupBox2: TGroupBox
      Left = 80
      Top = 168
      Width = 73
      Height = 209
      Caption = ' Speed '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      object TrackBar2: TTrackBar
        Left = 8
        Top = 51
        Width = 45
        Height = 150
        Max = 5000
        Orientation = trVertical
        Frequency = 1
        Position = 5000
        SelEnd = 0
        SelStart = 0
        TabOrder = 0
        TickMarks = tmBottomRight
        TickStyle = tsAuto
        OnChange = TrackBar2Change
      end
      object MaskEdit2: TMaskEdit
        Left = 8
        Top = 24
        Width = 40
        Height = 22
        EditMask = '!9999;1;0'
        MaxLength = 4
        TabOrder = 1
        Text = '    '
        OnChange = MaskEdit2Change
      end
    end
    object GroupBox3: TGroupBox
      Left = 8
      Top = 384
      Width = 145
      Height = 45
      Caption = ' Acceleration '
      TabOrder = 5
      object TrackBar3: TTrackBar
        Left = 48
        Top = 16
        Width = 89
        Height = 25
        Max = 30
        Min = 1
        Orientation = trHorizontal
        Frequency = 1
        Position = 9
        SelEnd = 0
        SelStart = 0
        TabOrder = 0
        TickMarks = tmBottomRight
        TickStyle = tsAuto
        OnChange = TrackBar3Change
      end
      object MaskEdit3: TMaskEdit
        Left = 8
        Top = 17
        Width = 38
        Height = 22
        EditMask = '!99;1;0'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 2
        ParentFont = False
        TabOrder = 1
        Text = '  '
        OnChange = MaskEdit3Change
      end
    end
    object GroupBox4: TGroupBox
      Left = 8
      Top = 32
      Width = 145
      Height = 77
      Caption = ' Port select '
      TabOrder = 6
      object ComboBox1: TComboBox
        Left = 8
        Top = 16
        Width = 129
        Height = 24
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemHeight = 16
        ParentFont = False
        TabOrder = 0
        Text = 'COM1'
        Items.Strings = (
          'COM1'
          'COM2'
          'COM3'
          'COM4'
          'COM5'
          'COM6'
          'COM7'
          'COM8'
          'COM9'
          'COM10')
      end
      object Button3: TButton
        Left = 8
        Top = 44
        Width = 65
        Height = 25
        Caption = 'Connect'
        TabOrder = 1
        OnClick = Button3Click
      end
      object Button4: TButton
        Left = 72
        Top = 44
        Width = 65
        Height = 25
        Caption = 'Disconnect'
        Enabled = False
        TabOrder = 2
        OnClick = Button4Click
      end
    end
  end
end
