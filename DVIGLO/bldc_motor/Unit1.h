//---------------------------------------------------------------------------
// http://radiohlam.ru
#ifndef Unit1H
#define Unit1H
#define BUFSIZE 1024

#define VOLTAGE_MAX 993 // for shim=212
#define VOLTAGE_MIN 57  // for shim=12
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Mask.hpp>
#include <math.h>
//---------------------------------------------------------------------------
  DCB    dcb;                   // ��������� DCB
  HANDLE Port;                  // ���������� �����
  OVERLAPPED ovr_wr;

  unsigned char wr_buf[BUFSIZE], rd_buf[BUFSIZE]; //������ ������ � ������
  int  wr_buf_len, rd_buf_len;
  unsigned char Shim, Speed, PolPairs, Accel;

void WriteTo(void);
void OpenPort(void);
void ClosePort(void);
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TPanel *Panel1;
        TButton *Button1;
        TButton *Button2;
        TCheckBox *CheckBox1;
        TGroupBox *GroupBox1;
        TTrackBar *TrackBar1;
        TGroupBox *GroupBox2;
        TTrackBar *TrackBar2;
        TGroupBox *GroupBox3;
        TGroupBox *GroupBox4;
        TComboBox *ComboBox1;
        TButton *Button3;
        TLabel *Label1;
        TButton *Button4;
        TMaskEdit *MaskEdit1;
        TMaskEdit *MaskEdit2;
        TTrackBar *TrackBar3;
        TMaskEdit *MaskEdit3;
        void __fastcall Button3Click(TObject *Sender);
        void __fastcall Button4Click(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall CheckBox1Click(TObject *Sender);
        void __fastcall MaskEdit1Change(TObject *Sender);
        void __fastcall TrackBar1Change(TObject *Sender);
        void __fastcall MaskEdit2Change(TObject *Sender);
        void __fastcall TrackBar2Change(TObject *Sender);
        void __fastcall MaskEdit3Change(TObject *Sender);
        void __fastcall TrackBar3Change(TObject *Sender);
        void __fastcall Label1MouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
        void __fastcall Label1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
