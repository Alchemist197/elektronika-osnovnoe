/**
 * Created by lexcorp on 01.09.2017.
 */
$.getJSON("https://api.ipify.org/?format=json", function(e) {
// $.getJSON("https://ipapi.co/" + e.ip +"/json/", function(data) {
    $.getJSON("https://freegeoip.net/json/" + e.ip , function(data) {
        var $search_string = $('meta[name=keywords]').attr("content");
        $search_string = $search_string.split(',');
        var $t_body;
        var $count = 0;
        var $search_words = [];
        // for(var i = 0; i < $search_string.length; i++) {
        //     if($count === 3) break;
        //     $search_string[i] = $search_string[i].trim();
        //     $search_string[i] = $search_string[i].split(" ");
        //     for(var k = 0; k < $search_string[i].length; k++) {
        //         $count++;
        //         $search_words[$count] = (encodeURIComponent($search_string[i][k]));
        //         if($count === 3) break;
        //     }
        // }
        $search_words[1] = '';
        $search_words[2] = '';
        $search_words[3] = '';
        $search_string[0] = $search_string[0].trim();
        $search_string[0] = $search_string[0].split(" ");
        for(var k = 0; k < $search_string[0].length; k++) {
            $count++;
            $search_words[$count] = (encodeURIComponent($search_string[0][k]));
            if ($count === 3) break;
        }

        // var country = data.country;
        var country = data.country_code;
        if(country === 'UA') {
            /*
             ????????-???????? ?? ???????:
             ??????? https://rozetka.com.ua/search/?text=xiaomi+mi+4
             Mobilluck http://www.mobilluck.com.ua/search.php?sw=xiaomi+mi+max *****
             ???????? http://www.foxtrot.com.ua/ru/search?query=xiaomi+mi+4
             COMFY https://comfy.ua/catalogsearch/result?q=xiaomi+mi+4
             MOYO https://www.moyo.ua/search.html?posted=1&s%5Btext%5D=xiaomi+mi+4&x=0&y=0
             */

            $t_body = '<div class="shop_link_column "><a href="https://rozetka.com.ua/search/?text=' + $search_words[1] + '+' + $search_words[2] + '+' + $search_words[3] + '" class="shop_link" target="_blank"><p class="shop_link_text">������ ���� �  <span class="shop_name">�������</span></p><img src="/templates/Tehnoobzor/banner/images/rozetka.png" alt="�������" class="shop_image"><div class="layout"></div></a></div>' +
                '<div class="shop_link_column "><a href="http://www.foxtrot.com.ua/ru/search?query=' + $search_words[1] + '+' + $search_words[2] + '+' + $search_words[3] + '" class="shop_link" target="_blank"><p class="shop_link_text">������ ���� �  <span class="shop_name">��������</span></p><img src="/templates/Tehnoobzor/banner/images/foxtrot.png" alt="��������" class="shop_image"><div class="layout"></div></a></div>' +
                '<div class="shop_link_column "><a href="https://comfy.ua/catalogsearch/result?q=' + $search_words[1] + '+' + $search_words[2] + '+' + $search_words[3] + '" class="shop_link" target="_blank"><p class="shop_link_text">������ ���� �  <span class="shop_name">COMFY</span></p><img src="/templates/Tehnoobzor/banner/images/comfy.png" alt="COMFY" class="shop_image"><div class="layout"></div></a></div>' +
                '<div class="shop_link_column "><a href="https://www.moyo.ua/search.html?posted=1&s%5Btext%5D=' + $search_words[1] + '+' + $search_words[2] + '+' + $search_words[3] + '&x=0&y=0" class="shop_link" target="_blank"><p class="shop_link_text">������ ���� �  <span class="shop_name">MOYO</span></p><img src="/templates/Tehnoobzor/banner/images/moyo.png" alt="MOYO" class="shop_image"><div class="layout"></div></a></div>' +
                '<div class="shop_link_column "><a href="http://www.mobilluck.com.ua/search.php?sw=' + decodeURIComponent($search_words[1]) + '+' + decodeURIComponent($search_words[2]) + '+' + decodeURIComponent($search_words[3]) + '" class="shop_link" target="_blank"><p class="shop_link_text">������ ���� �  <span class="shop_name">Mobilluck</span></p><img src="/templates/Tehnoobzor/banner/images/mobilluck.png" alt="Mobilluck" class="shop_image"><div class="layout"></div></a></div>';
            $('#shop_link_banner').html($t_body);
        }
        else if(country === 'BY') {
            /*
             https://www.21vek.by/search/?term=samsung+galaxy+s3&sa=
             https://24shop.by/search/?q=xiaomi+mi+max
             https://www.amd.by/search/page/1/?findtext=xiaomi+mi+max
             */

            $t_body = '<div class="shop_link_column "><a href="https://24shop.by/search/?q=' + $search_words[1] + '+' + $search_words[2] + '+' + $search_words[3] + '" class="shop_link" target="_blank"><p class="shop_link_text">������ ���� �  <span class="shop_name">24shop</span></p><img src="/templates/Tehnoobzor/banner/images/24shop.png" alt="24shop" class="shop_image"><div class="layout"></div></a></div>' +
                '<div class="shop_link_column "><a href="https://www.21vek.by/search/?term=' + $search_words[1] + '+' + $search_words[2] + '+' + $search_words[3] + '&sa=" class="shop_link" target="_blank"><p class="shop_link_text">������ ���� �  <span class="shop_name">21vek</span></p><img src="/templates/Tehnoobzor/banner/images/21vek.png" alt="21vek" class="shop_image"><div class="layout"></div></a></div>' +
                '<div class="shop_link_column "><a href="https://www.amd.by/search/page/1/?findtext=' + $search_words[1] + '+' + $search_words[2] + '+' + $search_words[3] + '" class="shop_link" target="_blank"><p class="shop_link_text">������ ���� �  <span class="shop_name">AMD.by</span></p><img src="/templates/Tehnoobzor/banner/images/amd.png" alt="AMD.by" class="shop_image"><div class="layout"></div></a></div>';
            $('#shop_link_banner').html($t_body);
        }
        // else if(country === 'KZ') {
        //
        // }
        else {
            /*
             ????????-???????? ?? ??:
             ?????? https://www.ulmart.ru/search?string=xiaomi+mi+max
             MediaMarkt https://www.mediamarkt.ru/search?q=xiaomi%20mi%204
             ?.????? http://www.mvideo.ru/product-list-page-cls?q=xiaomi+mi+4&region_id=1
             ???????? https://www.citilink.ru/search/?text=xiaomi+mi+4
             ????????? https://www.eldorado.ru/search/catalog.php?q=xiaomi+mi+4
             */

            $t_body = '<div class="shop_link_column "><a href="https://www.ulmart.ru/search?string=' + $search_words[1] + '+' + $search_words[2] + '+' + $search_words[3] + '" class="shop_link" target="_blank"><p class="shop_link_text">������ ���� �  <span class="shop_name">������</span></p><img src="/templates/Tehnoobzor/banner/images/ulmart-logo.png" alt="������" class="shop_image"><div class="layout"></div></a></div>' +
                '<div class="shop_link_column "><a href="https://www.mediamarkt.ru/search?q=' + $search_words[1] + '%20' + $search_words[2] + '%20' + $search_words[3] + '" class="shop_link" target="_blank"><p class="shop_link_text">������ ���� �  <span class="shop_name">MediaMarkt</span></p><img src="/templates/Tehnoobzor/banner/images/media-markt.png" alt="MediaMarkt" class="shop_image"><div class="layout"></div></a></div>'  +
                '<div class="shop_link_column "><a href="https://www.eldorado.ru/search/catalog.php?q=' + decodeURIComponent($search_words[1]) + '+' + decodeURIComponent($search_words[2]) + '+' + decodeURIComponent($search_words[3]) + '" class="shop_link" target="_blank"><p class="shop_link_text">������ ���� �  <span class="shop_name">���������</span></p><img src="/templates/Tehnoobzor/banner/images/eldorado.png" alt="���������" class="shop_image"><div class="layout"></div></a></div>' +
                '<div class="shop_link_column "><a href="https://www.citilink.ru/search/?text=' + $search_words[1] + '+' + $search_words[2] + '+' + $search_words[3] + '" class="shop_link" target="_blank"><p class="shop_link_text">������ ���� �  <span class="shop_name">��������</span></p><img src="/templates/Tehnoobzor/banner/images/citi-link.png" alt="��������" class="shop_image"><div class="layout"></div></a></div>' +
                '<div class="shop_link_column "><a href="http://www.mvideo.ru/product-list-page-cls?q=' + $search_words[1] + '+' + $search_words[2] + '+' + $search_words[3] + '&region_id=1" class="shop_link" target="_blank"><p class="shop_link_text">������ ���� �  <span class="shop_name">�.�����</span></p><img src="/templates/Tehnoobzor/banner/images/mvideo-logo.png" alt="�.�����" class="shop_image"><div class="layout"></div></a></div>';
            $('#shop_link_banner').html($t_body);
        }
    });

});