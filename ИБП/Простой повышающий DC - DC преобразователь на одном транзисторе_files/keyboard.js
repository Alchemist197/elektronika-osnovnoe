function loader(){
	lang='en';
	change_lang(lang)
	lang='ru';
	change_lang(lang)
}
function change_lang(lang){
	if(!lang || lang!='ru' && lang!='en') lang='ru';
	if(lang=='ru') {
		$('#ru_keyboard').show();
		$('#en_keyboard').hide();
		$('#ru_lang').show();
		$('#en_lang').hide();
	}else{
		$('#en_keyboard').show();
		$('#ru_keyboard').hide();
		$('#en_lang').show();
		$('#ru_lang').hide();
	}
	keyboard(lang);
}
function keyboard(lang){
	onShift(0)
	var shifton = false;
	// toggles between the normal and the "SHIFT keys" on the keyboard
	function onShift(e) {
		var i;
		if(e==1) {
			for(i=0;i<4;i++) {
				var rowid = "#"+lang+"_row" + i;
				$(rowid).hide();
				$(rowid+"_shift").show();
			}
		}
		else {
			for(i=0;i<4;i++) {
				var rowid = "#"+lang+"_row" + i;
				$(rowid).show();
				$(rowid+"_shift").hide();
			}
		}
	 }
	// function thats called when any of the keys on the keyboard are pressed
	$("#"+lang+"_keyboard input").unbind("click")
	$("#"+lang+"_keyboard input").bind("click", function(e) {
												
		if( $(this).attr('name') == 'backspace' ) {
			$('#message').replaceSelection("", true);
		}
		else if( $(this).attr('name') == lang+"_Shift" ) {
			if(shifton == false) {
				onShift(1);	
				shifton = true;
			}
			
			else {
				onShift(0);
				shifton = false;
			} 
		}
		
		else {
		
			$('#message').replaceSelection($(this).val(), true);
			
			if(shifton == true) {
				onShift(0);
				shifton = false;
			}
		}
	});
};


