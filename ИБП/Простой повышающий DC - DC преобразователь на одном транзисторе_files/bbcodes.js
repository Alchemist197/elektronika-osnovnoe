   /******************************************************/
  /********************* uBBCodes ***********************/
 /*** Скрипт сделал Sectron http://enigmatic-team.ru ***/
/******************************************************/

/***** Блок Настройки *****/
var setup = new Array(1,1,1,1,1)// Замеять BB коды [url,img,email,list,spoiler]

   /******** Отображать "Закрыть окно после вставки" ************/
  /******** Значения: 0 - не закрывать *************************/
 /******************  1 - закрывать ***************************/
/*******************  2 - выбирает пользователь **************/
var close_url = 2; //"Закрыть окно после вставки" у BB-кода url
var close_email = 2; //"Закрыть окно после вставки" у BB-кода email
var close_img = 2; //"Закрыть окно после вставки" у BB-кода img
var close_list = 2; //"Закрыть окно после вставки" у BB-кода list
var close_spoiler = 2; //"Закрыть окно после вставки" у BB-кода spoiler

var max_length_spoiler = 50 //Колличество символов для кнопки спойлера

/**** Больше ничего редактировать не нужно!!! ****/
for(i=0;i<setup.length;i++){
	itvalue=setup[i];
	if(itvalue == 1){
		includeJSfile('/bbcodes/replace/'+i+'.js');
	}
}

/*** KeyBoard Loader ***/
includeJSfile('/bbcodes/keyboard/jquery-fieldselection.js');
includeJSfile('/bbcodes/keyboard/keyboard.js');

var css = document.createElement('link');
css.setAttribute('type','text/css');
css.setAttribute('href','/bbcodes/keyboard/keyboard.css');
css.setAttribute('rel','StyleSheet')
document.getElementsByTagName('head').item(0).appendChild(css);

$('input[title="Keyboard"]').removeAttr('onclick').unbind('click').click(function(){openLayerB('virtualKB',0,'keyboard.xml','Виртуальная клавиатура',560,200);return false;});

/*** Spoiler Setting ***/
if(setup[4] == 1){
	if($('input[title="Spoiler"]').attr('onclick')){
		var field = $('input[title="Spoiler"]').attr('onclick').toString().split(',')[3];
		field = field.replace(/\ +/ig,'').replace(/\"+/ig,'').replace(/\'+/ig,'');
		$('input[title="Spoiler"]').removeAttr('onclick').unbind('click').click(function(){tag_spoiler(field);return false;});
	}
}


/* Past functions */
function url_exec(wh) {
	var isclose = document.getElementById('isclose_url_'+wh).checked;
	isclose = isclose ? '1':'0';
	var selecting=isSelected(wh);
	linkage = (selecting.indexOf("http://") == 0);
	if(selecting.length>0){
		if(linkage == true){
			eval('var enterTITLE  = document.url_'+wh+'.text_link_'+wh+'.value');
			eval('var testURL  = document.url_'+wh+'.url_'+wh+'.value.length');
			if(testURL>0){
				eval('var enterURL  = document.url_'+wh+'.url_'+wh+'.value');
			}else{
				var enterURL  = selecting;
			}
		}
		else
		{
			eval('var enterURL  = document.url_'+wh+'.url_'+wh+'.value');
			eval('var testTITLE  = document.url_'+wh+'.text_link_'+wh+'.value.length');
			if(testTITLE>0){
				eval('var enterTITLE  = document.url_'+wh+'.text_link_'+wh+'.value');
			}else{
				var enterTITLE  = selecting;
			}
		}
	}
	else
	{
		eval('var enterTITLE  = document.url_'+wh+'.text_link_'+wh+'.value'); 
		eval('var enterURL  = document.url_'+wh+'.url_'+wh+'.value');
	}
	if ((enterURL.indexOf("http://") == 0) == false){
		return;
	} else {
		if (enterTITLE=="") {
			enterTITLE=enterURL;
		}
	}
	doInsert("[url="+enterURL+"]"+enterTITLE+"[/url]","",false,wh);
	if((isclose=='1' && close_url == 2) || (close_url == 1)){
		_uWnd.close('tag_url_'+wh);
	}
}

function spoiler_exec(wh) {
	var isclose = document.getElementById('isclose_spoiler_'+wh).checked;
	isclose = isclose ? '1':'0';
	var spoilerSelect=isSelected(wh);
	if (spoilerSelect.length>0){
		if(spoilerSelect.length>100){
			spoilerText=spoilerSelect;
			eval('var spoilerName = document.spoiler_'+wh+'.spoiler_name_'+wh+'.value;');
		}else{
			spoilerName=spoilerSelect;
			eval('var spoilerText = document.spoiler_'+wh+'.spoiler_text_'+wh+'.value;');
		}
	}else{
		eval('var spoilerName = document.spoiler_'+wh+'.spoiler_name_'+wh+'.value;');
		eval('var spoilerText = document.spoiler_'+wh+'.spoiler_text_'+wh+'.value;');
	}
	if (spoilerName.length>0){
		doInsert("[spoiler="+spoilerName+"]"+spoilerText+"[/spoiler]","",false,wh);	
	}else {
		doInsert("[spoiler]"+spoilerText+"[/spoiler]","",false,wh);	
	}
	if((isclose=='1' && close_spoiler == 2) || (close_spoiler == 1)){
		_uWnd.close('tag_spoiler_'+wh);
	}
}

function img_exec(wh) {
	var FoundErrors = '';
	var isclose = document.getElementById('isclose_img_'+wh).checked;
	isclose = isclose ? '1':'0';
	eval('var enterURL = document.img_'+wh+'.img_url_'+wh+'.value;')
	doInsert("[img]"+enterURL+"[/img]","",false,wh);
	if((isclose=='1' && close_img == 2) || (close_img == 1)){
		_uWnd.close('tag_img_'+wh);
	}
}
function email_exec(wh) {
	var needstr = /\w+@\w+\.\w{2,6}/;
	var isclose = document.getElementById('isclose_email_'+wh).checked;
	isclose = isclose ? '1':'0';
	var emailText=isSelected(wh);
	if (emailText.length>0){
		if(needstr.test(emailText) == true){
			var ismail = 1;
			emailAddress = emailText
		}
		else
		{
			var ismail = 0;
		}
		
		if(ismail == 0){
			eval('var emailAddress = document.email_'+wh+'.email_url_'+wh+'.value;')
			eval('var testText=document.email_'+wh+'.email_text_'+wh+'.value.length;');
			if(testText>0){
				eval('var emailText = document.email_'+wh+'.email_text_'+wh+'.value;')
			}
		}
		else
		{
			eval('var emailText = document.email_'+wh+'.email_text_'+wh+'.value;')
			eval('var testAddress = document.email_'+wh+'.email_url_'+wh+'.value.length;')
			if(testAddress>0){
				eval('var emailAddress = document.email_'+wh+'.email_url_'+wh+'.value;')
			}
		}
	}else{
			eval('var emailAddress = document.email_'+wh+'.email_url_'+wh+'.value;')
			eval('var emailText = document.email_'+wh+'.email_text_'+wh+'.value;')
	}
	if(emailAddress.length>0 && (needstr.test(emailAddress) == true)){
		if (emailText.length>0){
			doInsert("[email="+emailAddress+"]"+emailText+"[/email]","",false,wh);	
		}
		else 
		{
			doInsert("[email]"+emailAddress+"[/email]","",false,wh);	
		}
		if((isclose=='1' && close_email == 2) || (close_email == 1)){
			_uWnd.close('tag_email_'+wh);
		}
	}else{
		_uWnd.content('tag_email_'+wh,'<font color="red"><b>Ошибка</b></font>: E-mail не введен или произошла опечатка при вводе Email.<br><input type="button" value="Вернуться" onclick="_uWnd.close(\'tag_email_'+wh+'\');tag_email(\''+wh+'\')">');
		_uWnd.setTitle('tag_email_'+wh,'Ошибка');
	}
	return false;
}
function list_exec(wh,isclose){
	var isclose = document.getElementById('isclose_list_'+wh).checked;
	isclose = isclose ? '1':'0';
	var listvalue = "init";
	var thelist = "";
	i=1;
	while ( document.getElementById(wh+'_item_'+i)){
		listvalue = document.getElementById(wh+'_item_'+i).value;
		if ( (listvalue != "") && (listvalue != null) ){
			if(listvalue==" "){listvalue=""}
			thelist = thelist+"[*]"+listvalue+"\n";
		}
		i++;
	}
	if ( thelist != "" ){
		doInsert( "[list]\n" + thelist + "[/list]\n", "",false,wh);
	}
	if((isclose=='1' && close_list == 2) || (close_list == 1)){
		_uWnd.close('tag_list_'+wh);
	}
}