function symbcheck(strvar) {

for (k=0; k < strvar.length; k++)
	{
	var code = strvar.charCodeAt(k);
	var ch = strvar.substr(k, 1).toUpperCase();
	code = parseInt(code);
	if ((ch < "0" || ch > "9") && (ch < "A" || ch > "Z") && ((code > 255) || (code < 0))) return true;
	}
return false;
}

function ds_checkquery(formname) {

if (formname.query.value.replace(/ /gi, "").length < 3)
	{
	alert("������ ������ ������ ��������� ����� 2 ��������!");
	formname.query.focus();
	return false;
	}
if (symbcheck(formname.query.value))
	{
	alert("������������ ������ � ������ ������!\n����������� ����� ����������� ��������, ����� � ������� '.,+,-,_,/'");
	formname.query.focus();
	return false;
	}
if (formname.searchin.value == 'smd')
	{
	if (!ss_checkquery(datasheet_search)) return false;
	formname.action = '/datasheets/smd/go/';
	}
return true;
}

function ss_checkquery(formname) {

if (formname.query.value.replace(/ /gi, "").length < 1)
	{
	alert("������ ������ ������ ��������� ����� 1 �������!");
	formname.query.focus();
	return false;
	}
if (symbcheck(formname.query.value))
	{
	alert("������������ ������ � ������ ������!\n����������� ����� ����������� ��������, ����� � ������� '.,+,-,_,/'");
	formname.query.focus();
	return false;
	}
if (formname.searchin.value == 'datasheet')
	{	if (!ds_checkquery(smd_search)) return false;
	formname.action = '/datasheets/search/go/';
	}
return true;
}

function dsgotopage(dspn, dsst, dsnh) {

if (dsst) document.datasheet_search.query.value=dsst;
if (dspn > 0) document.datasheet_search.startpage.value=dspn; else document.datasheet_search.startpage.value=1;
if (dsnh > 0) document.datasheet_search.nohistory.value=1; else document.datasheet_search.nohistory.value=0;
document.datasheet_search.submit();
return false;
}

function ssgotopage(dspn, dsst) {

if (dsst) document.smd_search.query.value=dsst;
if (dspn > 0) document.smd_search.startpage.value=dspn; else document.smd_search.startpage.value=1;
document.smd_search.submit();
return false;
}

function unescape_opera_br(text)
{
	if(typeof(RegExp) == 'function') {
                re = /quot;/g;
                newstr=text.replace(re, '');
                re = /&/g;
                return newstr.replace(re, '\"');
        }
        else return text;
}