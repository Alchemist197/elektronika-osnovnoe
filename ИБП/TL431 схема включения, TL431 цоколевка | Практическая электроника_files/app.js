(function (d, w, c) {
	(w[c] = w[c] || []).push(function() {
		try {
			w.yaCounter19155217 = new Ya.Metrika({
				id:19155217, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:false
			});
		} catch(e) { }
	});

	var n = d.getElementsByTagName("script")[0],
		s = d.createElement("script"),
		f = function () { n.parentNode.insertBefore(s, n); };
	s.type = "text/javascript";
	s.async = true;
	s.src = "https://mc.yandex.ru/metrika/watch.js";

	if (w.opera == "[object Opera]") {
		d.addEventListener("DOMContentLoaded", f, false);
	} else { f(); }
})(document, window, "yandex_metrika_callbacks");


(function (d) {
	var topf = d.getElementById('topf');
	var bottomf = d.getElementById('footer');
	var blockf = d.getElementById('blockf');
	if ( null != topf && null != bottomf && null != blockf) {
		window.onscroll = function() {
			var top = topf.getBoundingClientRect().top;
			var bh = bottomf.getBoundingClientRect().top - blockf.getBoundingClientRect().bottom + blockf.getBoundingClientRect().top - 50;
			if( top > 0 ) top = 0;
			if( bh > 0 ) bh = 0;
			topf.style.paddingTop = bh - top + 'px';
		}
	}
})(document);

(function (d) {
if (typeof d[getElementsByClassName] == 'function'){
	var entryContent = d.getElementsByClassName('entry-content')[0];
	if(!entryContent) return;
}

entryContent.oncopy = function() {
var selection = window.getSelection();
if( selection.toString().length < 20 ) {console.log(selection.length);return;}
var newdiv = d.createElement('div');
newdiv.style.position='absolute';
newdiv.style.left='-99999px';
newdiv.innerHTML = selection.toString().split("а").join("a").split("е").join("e") + " Источник: <a href='"+d.location.href+"'>"+d.location.href+"</a>";
var body_element = d.getElementsByTagName('body')[0];
body_element.appendChild(newdiv);
selection.selectAllChildren(newdiv);
window.setTimeout(function() {body_element.removeChild(newdiv); },0);
}

})(document);