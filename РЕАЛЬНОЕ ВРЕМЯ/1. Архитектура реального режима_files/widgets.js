(function(window, document, host, containerId, projectId, snIds, orientation, mode, likeTextEnable, shareShape, shareSize, shareStyle,
          backgroundColor, backgroundAlpha, textColor, buttonsColor, counterBackgroundColor, counterBackgroundAlpha,
          iconColor, showShareCounter, shareCounterType, selectionEnable, followingEnable, vkAppid, shareCounterSize, url) {

    function setAttribute(element, name, value) {
        (element.setAttribute && element.setAttribute(name, value)) || (element[name] = value);
    }

    function setData(element, data) {
        for (var name in data) {
            if (data.hasOwnProperty(name)) {
                setAttribute(element, "data-"+name, data[name]);
            }
        }
    }

    function convertOrientation(old) {
        switch (old) {
            case 0 : return "vertical";
            case 1 : return "horizontal";
            case 2 : return "fixed-left";
            case 3 : return "fixed-right";
            case 4 : return "fixed-top";
            case 5 : return "fixed-bottom";
            default : return "horizontal";
        }
    }

    function convertMode(old) {
        switch (old) {
            case 0 : return "like";
            case 1 : return "share";
            case 2 : return "follow";
            default : return "share";
        }
    }

    function convertLikeTextEnable(old) {
        switch (old) {
            case 0 : return false;
            case 1 : return true;
            default : return true;
        }
    }

    function convertShareShape(old) {
        switch (old) {
            case 1 : return "round";
            case 2 : return "round-rectangle";
            case 3 : return "rectangle";
            default : return "round-rectangle";
        }
    }

    function convertShareSize(old) {
        switch (old) {
            case 1 : return 40;
            case 2 : return 30;
            case 3 : return 20;
            default : return 40;
        }
    }

    function convertShareCounterType(mc, mcs) {
        switch (mc) {
            case 0 : return "disable";
            case 1 :
                switch (mcs) {
                    case 0: return "common";
                    case 1: return "separate";
                    default : return "common";
                }
                break;
            default : return "common";
        }
    }

    function convertSelectionEnable(old) {
        switch (old) {
            case 0 : return "false";
            case 1 : return "true";
            default : return "false";
        }
    }

    function convertFollowingEnable(old) {
        switch (old) {
            case 0 : return "false";
            case 1 : return "true";
            default : return "false";
        }
    }

    var container = document.getElementById(containerId);
    if (container) {
        setData(container, {
            "pid" : projectId,
            "sn-ids" : snIds,
            "orientation": convertOrientation(orientation),
            "mode": convertMode(mode),
            "like-text-enable": convertLikeTextEnable(likeTextEnable),
            "share-shape": convertShareShape(shareShape),
            "share-size": convertShareSize(shareSize),
            "share-style": shareStyle,
            "background-color": backgroundColor,
            "background-alpha": backgroundAlpha,
            "text-color": textColor,
            "buttons-color": buttonsColor,
            "counter-background-color": counterBackgroundColor,
            "counter-background-alpha": counterBackgroundAlpha,
            "icon-color": iconColor,
            "share-counter-type" : convertShareCounterType(showShareCounter, shareCounterType),
            "selection-enable" : convertSelectionEnable(selectionEnable),
            "following-enable" : convertFollowingEnable(followingEnable),
            "vk-appid": vkAppid,
            "share-counter-size": shareCounterSize,
            "url": url
        });

        if (!window.__utlWdgt ) {
            container.className += " uptolike-buttons";
            window.__utlWdgt = true;
            var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
            s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
            s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://'+host+'/widgets/v1/uptolike.js';
            var h=d[g]('body')[0];
            h.appendChild(s);
        }
        else {
            var $uptolike = window["__utl"];
            if ($uptolike) {
                var $buttons = $uptolike.$(container);
                if ($buttons.size() > 0) {
                    $uptolike.require("//"+host+"/widgets/v1/widgetsModule.js", function () {
                        $buttons.buttons({"host": host});
                    });
                }
            }
        }
    }
})(window, document, "w.uptolike.com", "__uptlk6862", 38135, "fb.ok.mr.vk.gp.tw", 3, 1, 0, 2, 2,
1, "ededed", "0.0", "000000", "ff9300",
    "ffffff", "1.0", "ffffff", 1, 0,
1, 0, "3595111", 12, "http://www.realcoding.net/articles/1-arkhitektura-realnogo-rezhima.html");