/* Produce a rectangular wave on output 0 of a parallel port */


#define MODULE
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <asm/io.h>

#include <linux/rt_sched.h>
#include <math.h>



/* the address of the parallel port -- you probably should change this */

#define LPT 0x278



RT_TASK mytask;

void fun(int t) {
	int output;
	double sine;

	rt_use_fp(1);
	while (1) {
		sine = sin((double)(rt_get_time())/10000.0);
		output = (sine > 0) ? 1 : 0;
		outb(output, LPT);
		rt_task_wait();
	}
}


int init_module(void)
{
	RTIME now = rt_get_time();

	rt_task_init(&mytask, fun, 1, 3000, 4);

	rt_task_make_periodic(&mytask, now + 1000, 1000);
	return 0;
}


void cleanup_module(void)
{
	rt_task_delete(&mytask);
}
