function setTitle(title) {
	document.title=title;
}
function setNavMenu(where,top) {
	document.write('<a href='+top+' title=\"CERN Linux main page\">Main</a>');
	if (where.match(top+'/updates/')) {
	  document.write(' &gt; <a href=\"'+top+'/updates/\" title=\"Software updates\">Updates</a>'); }
        else if (where.match(top+'/docs/')) {
          document.write(' &gt; <a href=\"'+top+'/docs/\" title=\"Documentation\">Docs</a>'); }
	else if (where.match(top+'/scientific5/hardware/')) {
          document.write(' &gt; <a href=\"'+top+'/scientific5/\" title=\"Scientific Linux CERN 5 page(s)\">SLC5</a> &gt; <a href=\"'+top+'/scientific5/hardware/\" title=\"Hardware\">Hardware</a>'); }
        else if (where.match(top+'/scientific5/docs/')) {
          document.write(' &gt; <a href=\"'+top+'/scientific5/\" title=\"Scientific Linux CERN 5 page(s)\">SLC5</a> &gt; <a href=\"'+top+'/scientific5/docs/\" title=\"Documentation\">Docs</a>'); }
	else if (where.match(top+'/scientific5/')) {
          document.write(' &gt; <a href=\"'+top+'/scientific5/\" title=\"Scientific Linux CERN 5 page(s)\">SLC5</a>'); }
        else if (where.match(top+'/scientific4/hardware/')) {
          document.write(' &gt; <a href=\"'+top+'/scientific4/\" title=\"Scientific Linux CERN 4 page(s)\">SLC4</a> &gt; <a href=\"'+top+'/scientific4/hardware/\" title=\"Hardware\">Hardware</a>'); }
	else if (where.match(top+'/scientific4/docs/')) {
	  document.write(' &gt; <a href=\"'+top+'/scientific4/\" title=\"Scientific Linux CERN 4 page(s)\">SLC4</a> &gt; <a href=\"'+top+'/scientific4/docs/\" title=\"Documentation\">Docs</a>'); }
	else if (where.match(top+'/scientific4/')) {
	  document.write(' &gt; <a href=\"'+top+'/scientific4/\" title=\"Scientific Linux CERN 4 page(s)\">SLC4</a>'); }	
	else if (where.match(top+'/docs/')) {
	  document.write(' &gt; <a href=\"'+top+'/docs/\" title=\"SLC Documentation pages\">Docs</a>'); }  
	else if (where.match(top+'/install/')) {
	  document.write(' &gt; <a href=\"'+top+'install/\" title=\"How to install SLC ?\">Install</a>'); } 
	else if (where.match(top+'/support/')) {
	  document.write(' &gt; <a href=\"'+top+'/support/\" title=\"Looking for help ?\">Support</a>'); }  
	}




