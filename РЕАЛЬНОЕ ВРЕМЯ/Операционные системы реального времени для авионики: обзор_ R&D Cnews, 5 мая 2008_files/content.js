var PageContent               =  1;
var DepartmentContent         =  2;
var ArticlesContent           =  3;
var PhotosContent             =  4;
var FaqsContent               =  5;
var NewsContent               =  6;
var LinksContent              =  7;
var AddLinkContent            =  8;
var AddFaqContent             =  9;
var ArticleContent            = 10;
var ArticleDepartmentContent  = 11;
var PhotoContent              = 12;
var PhotoDepartmentContent    = 13;
var FaqContent                = 14;
var FaqDepartmentContent      = 15;
var OneNewsContent            = 16;
var LinkContent               = 17;
var LinkDepartmentContent     = 18;
var MainContent               = 19;
var MapContent                = 20;

function ow(uri, name, width, height)
{
	window.open(uri, name, 'width='+width+',height='+height+',status=yes,menubar=no,resizable=yes,scrollbars=1');
}

function openDialog(href, width, height, resizable, wnd_name)
{
	if (!width)
		width = 300;
	if (!height)
		height = 300;
	if (!resizable)
		resizable = 'yes';
	w = window.open(href, wnd_name, "height="+height+",width="+width+",resizable="+resizable+",directories=no,status=no,titlebar=no,toolbar=no,menubar=no,location=no,scrollbars=yes");
	w.focus();
}

function ChangeAdminSite()
{
	var selectObject = document.getElementById('adminsite');
	if (selectObject.options[selectObject.selectedIndex].text == "����")
		window.location = "http://" + site_name;
	else
		window.location = "/admin/?menu=6";
}

function DeleteEntryItem(message, id, parent)
{
	if (window.confirm(message) == true)
	{
		window.location = "/content-manager/scripts/execute.php?action=delete&id=" + id + "&parent=" + parent;
	}
}

function ActivateEntry(message, id, parent)
{
	if (window.confirm(message) == true)
	{
		window.location = "/content-manager/scripts/execute.php?action=activate&id=" + id + "&parent=" + parent;
	}
}

function StopEntry(message, id, parent)
{
	if (window.confirm(message) == true)
	{
		window.location = "/content-manager/scripts/execute.php?action=stop&id=" + id + "&parent=" + parent;
	}
}

function CheckAll(form)
{
	f = document.forms[form];
	v  = false;
	for (i = 0; i < f.elements.length; ++i)
		if (f.elements[i].type == "checkbox" && f.elements[i].name == "all")
		{
			v = f.elements[i].checked;
			break;
		}
	for (i = 0; i < f.elements.length; ++i)
		if (f.elements[i].type == "checkbox" && f.elements[i].name != "all")
			f.elements[i].checked = v;
}

function DoWithAll(form, message, parent, action)
{
	f = document.forms[form];
	bFound = false;
	for (i = 0; i < f.elements.length; ++i)
		if (f.elements[i].type == "checkbox" && f.elements[i].name != "all" && f.elements[i].checked == true)
		{
			bFound = true;
			break;
		}
	if (bFound)
		if (window.confirm(message) == true)
		{
			ids = "";
			for (i = 0; i < f.elements.length; ++i)
				if (f.elements[i].type == "checkbox" && f.elements[i].name != "all" && f.elements[i].checked == true)
				{
					if (ids != "") ids += "+";
					ids += f.elements[i].name;
				}
			window.location = "/content-manager/scripts/execute.php?action=" + action + "&id=" + ids + "&parent=" + parent;
		}
}

function DeleteAll(form, message, parent)
{
	DoWithAll(form, message, parent, "delete");
}

function ActivateAll(form, message, parent)
{
	DoWithAll(form, message, parent, "activate");
}

function StopAll(form, message, parent)
{
	DoWithAll(form, message, parent, "stop");
}

function MoveAll(form, message, parent, sel)
{
	p = document.forms[form].elements[sel].value;
	if (p == -1)
	{
		DoWithAll(form, message, parent, "stop");
	}
	else
	{
		DoWithAll(form, message, p, "move");
	}
}

function IsNumber(str)
{
	for (var i = 0; i < str.length; i++)
	{
		var ch = str.substring(i, i + 1)
		if (ch < "0" || ch > "9")
			return false;
	}
	return true;
}

function IsUrl(str)
{
	for (var i = 0; i < str.length; i++)
	{
		var ch = str.substring(i, i + 1)
		if ((ch < "0" || ch > "9") && (ch < "a" || ch > "z") && (ch < "A" || ch > "Z") && ch != "-" && ch != "_" && ch != "/" && ch != "." && ch != ":")
			return false;
	}
	return true;
}

function CheckRequiredFields(formName)
{
	error = false;
	f = document.forms[formName];
	type = f.elements["type"].value;
	errorString = "";
	if (type != FaqContent && type != LinkContent && type != OneNewsContent && type != PhotoContent && f.elements["name"] != null && f.elements["name"].value == "")
	{
		errorString += "<li><nobr>�������� �� ������ ���� ������</nobr></li>";
		error = true;
	}
	if (type != LinkContent && type != OneNewsContent && f.elements["path"] != null && f.elements["path"].value != "" && f.elements["path"].value.length < 5 && IsNumber(f.elements["path"].value))
	{
		errorString += "<li><nobr>������ ������ ���� ������ 4 ����, ��� ������ ��������� �����</nobr></li>";
		error = true;
	}
	if (type != LinkContent && type != OneNewsContent && f.elements["path"] != null && f.elements["path"].value == "add")
	{
		errorString += "<li><nobr>add - ��������������� ��� ���������� ������ ��� �������</nobr></li>";
		error = true;
	}
	if (type != LinkContent && type != OneNewsContent && f.elements["path"] != null && f.elements["path"].value == "print")
	{
		errorString += "<li><nobr>print - ��������������� ��� ������</nobr></li>";
		error = true;
	}
	if (type != LinkContent && type != OneNewsContent && f.elements["path"] != null && f.elements["path"].value == "")
	{
		errorString += "<li><nobr>������ �� ������ ���� ������</nobr></li>";
		error = true;
	}
	if (type != LinkContent && type != OneNewsContent && f.elements["path"] != null && !IsUrl(f.elements["path"].value))
	{
		errorString += "<li><nobr>������ ������ ��������� ��������� �������</nobr></li>";
		error = true;
	}
	if ((type == FaqContent || type == OneNewsContent) && (f.elements["short"].value == "" || f.elements["short"].value == "<br>" || f.elements["short"].value == "<br>\r\n" || f.elements["short"].value == "<br>\n"))
	{
		if (type == OneNewsContent)
			errorString += "<li><nobr>������� ������ �� ������ ���� ������</nobr></li>";
		else
			errorString += "<li><nobr>������ �� ������ ���� ������</nobr></li>";
		error = true;
	}
	if ((type == FaqContent) && f.elements["page"] != null && (f.elements["page"].value == "" || f.elements["page"].value == "<br>" || f.elements["page"].value == "<br>\r\n" || f.elements["page"].value == "<br>\n"))
	{
		errorString += "<li><nobr>����� �� ������ ���� ������</nobr></li>";
		error = true;
	}
	if ((type == ArticleContent || type == DepartmentContent || type == MainContent) && f.elements["page"].value == "")
	{
		errorString += "<li><nobr>���������� �������� �� ������ ���� ������</nobr></li>";
		error = true;
	}
	if (type == ArticlesContent || type == FaqsContent || type == LinksContent || type == PhotosContent)
	{
		if (f.elements["count"].value == "")
		{
			if (type == PhotosContent)
				errorString += "<li><nobr>���� \"���� �� ��������\" �� ������ ���� ������</nobr></li>";
			else
				errorString += "<li><nobr>���� \"������� �� ��������\" �� ������ ���� ������</nobr></li>";
			error = true;
		}
	}
	if ((type == FaqsContent || type == FaqContent || type == LinksContent || type == LinkContent) && f.elements["email"].value == "" && f.elements["site"] != null)
	{
		if (type == FaqContent)
			errorString += "<li><nobr>���� ��. ����� ��� ������� �� ������ ���� ������</nobr></li>";
		else
			errorString += "<li><nobr>��. ����� �� ������ ���� ������</nobr></li>";
		error = true;
	}
	if (type == LinkContent)
	{
		if (f.elements["link"].value == "")
		{
			errorString += "<li><nobr>HTML-��� ��������� ������ �� ������ ���� ������</nobr></li>";
			error = true;
		}
		if (f.elements["url"].value == "" && f.elements["site"] != null)
		{
			errorString += "<li><nobr>URL �������� � ����� ������� �� ������ ���� ������</nobr></li>";
			error = true;
		}
	}
	if (type == OneNewsContent)
	{
		if (f.elements["day"].value == "" || f.elements["month"].value == "" || f.elements["year"].value == "")
		{
			errorString += "<li><nobr>���� �� ������ ���� ������</nobr></li>";
			error = true;
		}
		if (!IsNumber(f.elements["day"].value) || !IsNumber(f.elements["month"].value) || !IsNumber(f.elements["year"].value))
		{
			errorString += "<li><nobr>���� ������ ���� ������� �������</nobr></li>";
			error = true;
		}
		var day = f.elements["day"].value;
		var month = f.elements["month"].value;
		var year = f.elements["year"].value;
		if (day < 1 || day > 31)
		{
			errorString += "<li><nobr>���� ������ ���� �� 1 �� 31</nobr></li>";
			error = true;
		}
//		if (month < 1 || month > 12)
//		{
//			errorString += "<li><nobr>����� ������ ���� �� 1 �� 12</nobr></li>";
//			error = true;
//		}
		if (year < 1900 || year > 2500)
		{
			errorString += "<li><nobr>��� ������ ���� �� 1900 �� 2500</nobr></li>";
			error = true;
		}
	}
	if (type == PhotoContent)
	{
		if (f.elements["full_photo"].value == "" && f.elements["full"].value == "")
		{
			errorString += "<li><nobr>���������� ������ ��������������</nobr></li>";
			error = true;
		}
	}

	if (error)
	{
		if (document.getElementById)
		{
			document.getElementById("wrong").innerHTML = "<nobr>��� ���������� �������� ���� �������� ��������� ������:</nobr><br /><ul>" + errorString + "</ul>";
			window.scroll(0,0);
		}
		return false;
	}
	else
	{
		if (document.getElementById)
			document.getElementById("wrong").innerHTML = "&nbsp;";
	}
	return true;
}

function printPage()
{
	var url = document.location.href;
	if (url.substring(url.length - 1, url.length) == "/")
		url = url.substring(0, url.length - 1);
	var questionIndex = url.indexOf("?");
	if (questionIndex != -1)
		url = url.substring(0, questionIndex - 1);
	document.location.href = url + '/print/';
}
