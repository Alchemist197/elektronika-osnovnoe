﻿/*********************************
  MyBB.ru
  Регулировка размера шрифта в постах + Быстром Ответе
  Версия: V1.1.0 
  Автор: Alex_63
  Дата: 24.02.2016
  Последние изменения: 13.02.2017
*********************************/

if($('.post,#main-reply').length)(function(){

  var sld = '<div id="fntSlider" class="FNTslider"><div class="before"></div><div class="thumb"></div></div>';
  var pstSp = $('.post:first h3 > span');pstSp.find('strong').length ? pstSp.find('strong').after(sld) : pstSp.append(sld);
  var sliderElem = $('#fntSlider')[0]; 
  var thumbElem = $('#fntSlider > .thumb')[0];
  var thumbBefor= $('#fntSlider > .before')[0];
  var slideWidth = $('#fntSlider').width();
  var p00 = parseInt($('#fntSlider').css('padding-left'));

  if(thumbElem)thumbElem.title='Размер шрифта';
  if(!thumbElem || !thumbBefor)thumbElem = thumbBefor = $('<div/>')[0];
  $('#fntSlider > .thumb').tipsy({fade:true,gravity:'nw'});

  var lSFont=localStorage.getItem('FNTsize');
  var st_Size = '';
  if(lSFont){lSFont=lSFont.split(',');
    var k01=lSFont[0];var k02=lSFont[1];
    thumbElem.style.left=k01+'px';
    thumbBefor.style.width=k01+'px';
    $('#main-reply,.post-content p').css('font-size',k02+'px');
    st_Size=lSFont[2];
  } else {
    var Fnt=$('.post-content p:first').css('font-size');
    Fnt=Fnt.replace('px','');Fnt=Math.round(parseFloat(Fnt));
    st_Size=''+Fnt;Fnt-=6;Fnt=Fnt*5;
    thumbElem.style.left=Fnt+'px';
    thumbBefor.style.width=Fnt+'px';
  }

  function setFont(f) {
    var k00=parseInt(f/5);k00+=6;
    if(k00>30){k00=30;}
    $('#main-reply,.post-content p').css('font-size',k00+'px');
    localStorage.setItem('FNTsize',f+','+k00+','+st_Size.replace('px',''));
  }
  function getCoords(elem) {var b=elem.getBoundingClientRect();return{top:b.top+pageYOffset,left:b.left+pageXOffset};}
  $(thumbElem).on('mousedown touchstart',function(e) {
      var thumbCoords = getCoords(thumbElem);
      var pageX = ( e.type=='mousedown' ? e.pageX : e.originalEvent ? e.originalEvent.touches[0].pageX : e.touches[0].pageX );
      var shiftX = pageX - thumbCoords.left;

      var sliderCoords = getCoords(sliderElem);
      sliderCoords.left += p00;
      document.onmousemove = document.ontouchmove = function(e) {
        var pageX = ( e.type=='mousemove' ? e.pageX : e.originalEvent ? e.originalEvent.touches[0].pageX : e.touches[0].pageX );
        var newLeft = pageX - shiftX - sliderCoords.left;
        if (newLeft < 0) {newLeft = 0;}
        var rightEdge = slideWidth - thumbElem.offsetWidth;
        if (newLeft > rightEdge) {newLeft = rightEdge;}
        thumbElem.style.left = newLeft + 'px';
        thumbBefor.style.width=newLeft + 'px';
        setFont(newLeft);
      }
      document.onmouseup = document.ontouchend = function(){document.onmousemove=document.onmouseup=document.ontouchend=document.ontouchmove=null;};
      return false;
  });
  thumbElem.ondragstart = function() {return false;};

  sliderElem.ondblclick = function(){
    var stFnt=st_Size;//alert(stFnt);
    $('#main-reply,.post-content p').css('font-size',stFnt+'px');
    stFnt=parseInt(stFnt);stFnt-=6;stFnt=stFnt*5;
    localStorage.removeItem('FNTsize');
    thumbElem.style.left=stFnt+'px';//alert(Fnt);
    thumbBefor.style.width=stFnt+'px';
  };

}())