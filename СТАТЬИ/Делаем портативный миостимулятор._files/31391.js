﻿(function($){
    $(function(){
        var $punMain = $('#pun-main')
            ,$modal = $('#modal-m')
            ,postsCache = {};
        
        if (!$punMain.length) return;

        var popupContent = function(topicId) {
            if (typeof topicId == 'undefined') return;
            if (typeof postsCache[topicId] == 'undefined') return;

            var userName = postsCache[topicId]['author'].replace(/mybb@mybb.ru \((.*)\)/, "$1");
            var postContent = postsCache[topicId]['title'];

            $modal.find('.name-author strong:first').html(userName);
            $modal.find('.p-messages').html(postContent);
        };
        
        $('.category .tcr a, .forum .tcr a').hover(function(){
            var link = $(this).attr('href');
            if(document.URL.indexOf('/search.php?action=show_recent')!=-1||document.URL.indexOf('/search.php?action=show_new')!=-1)link = 

$(this).parents('tr:first').find('.tcl a:first').attr('href');
            var topicId = /\?id=(\d+)/.exec(link)[1];

            $modal.css('top', $(this).offset().top+25).fadeIn(10);
            $modal.find('.p-messages').html('<img src="http://hostjs.mybb.ru/files/0015/c4/3f/79458.gif" alt="Загружаю" />');

            if (typeof postsCache[topicId] == 'undefined') {
                $.get('/export.php', {type: 'rss', tid: topicId}, function(data){
                    if (data) {
                        postsCache[topicId] = {
                            title:  $(data).find('channel').find('item:first').find('description').text(),
                            author: $(data).find('channel').find('item:first').find('author').text()
                        };
                        popupContent(topicId);
                    }
                });
            } else popupContent(topicId);
        }, function(){
            $modal.fadeOut(100);
        });
    });
})(jQuery);