﻿/*************************
  mybb.ru, 
  Проверка ника при регистрации
  18.09.2015; 
  v1.3.3;
  Правка: 29.11.2015
  Author: Alex_63
*************************/

$(function() {
  var obj ={
    0:'',
    1:'Имя слишком короткое',
    2:'Имя свободно',
    3:'Имя занято',
    4:'Имя содержит недопустимые символы'
  };
  var evnt = 'input'; if($.browser.msie) { evnt = 'change'; };
  var checkName; 
  $('#username').unbind('change');
  function infoName(n) {
    if(n && n!=2){ $('#username').removeClass('normal').addClass('error'); }
    else {$('#username').removeClass('error').addClass('normal');}
    if(n) {$('#username-status').html('( <span class="stclr">'+ obj[n] +'</span> )');}
    else {$('#username-status').html('');}
    $('#username.error').parent().next('#username-status').find('.stclr').addClass('error');
    $('#username.normal').parent().next('#username-status').find('.stclr').addClass('normal');
  }
  $('#username').bind(evnt,function(e) {
    infoName(0);
    if ($(this).val().length <= 1) {
      infoName(1); return;
    }
    if ($(this).val().indexOf('\'')!=-1 || $(this).val().indexOf('\"')!=-1) {
      infoName(4); return;
    }
    $('#username-status').html('<img src="http://test111.1bb.ru/files/0015/41/f2/79436.gif" class="preloader" />');
    clearTimeout(checkName);
    checkName = setTimeout(function () {
      $.get('/api.php',{method: 'users.get', username: $('#username').val(), fields: 'user_id'},function(data){
        $('.preloader').remove();
        if (data.error) {
          infoName(2);
        } else if (data.response) {
          infoName(3);
        }
      }, 'json');
    },450);
  });
  $('.button[name="register"]').click(function (){
    var sts = $(this).parents('#register').find('#username-status>span').text();
    if(sts == obj['1']) {alert('Ошибка: Вы ввели слишком короткое имя.');return false;
    }else if(sts == obj['3']) {alert('Ошибка: данное имя пользователя уже занято!');return false;}
    else if(sts == obj['4']){alert('Ошибка: имя пользователя не должно содержать двойных и одинарных кавычек.');return false;}
  });
});