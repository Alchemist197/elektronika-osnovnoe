﻿/****************************************************************
   Ограничение редактирования/удаления сообщений по времени
   Автор: (c) Alex_63, 26.09.2016; v1.0.0
   Last Changes: 26.09.2016 14:05 GMT +0300
****************************************************************/

if(window.timeLimits)timeLimits.enable = function() {

  if(GroupID==3)return;

  if(timeLimits.edit_time>3600)timeLimits.edit_time = 3600;
  if(timeLimits.delete_time>3600)timeLimits.delete_time = 3600;

  var time_edit   = (timeLimits.edit_time==-1)?-1:(timeLimits.edit_time||180)*60*1000;
  var time_delete = (timeLimits.delete_time==-1)?-1:(timeLimits.delete_time||180)*60*1000;;

  //===========================================//


  //============ setErrorInfo()  ==============//
  function setErrorInfo () {
      var message = 'Вы не можете ' + (($('#pun-edit').length)? 'отредактировать' :'удалить') + ' это сообщение.';
      var title = $('#pun-title h1 > span').text(); document.title = title;
      var crumbs = '<strong>Вы здесь</strong> »&nbsp;<a href="/">'+title+'</a> »&nbsp;Информация';
      $('#pun-crumbs1 .crumbs').html(crumbs); $().pun_aboutReady(function(){$('#pun-crumbs2 .crumbs').html(crumbs);});
      $('#pun-main').html('<h1><span>Информация</span></h1><div class=info><div class=container>'+message+'</div></div>');
      $('.punbb').attr('id','#pun-message');

  }   //End// - setErrorInfo
  //===========================================//


  //============ РАБОТА В ТОПИКЕ ==============//
  if($('#pun-viewtopic').length)$().pun_mainReady(function(){
    $('.post[data-posted]').each(function(){
       var time = parseInt($(this).attr('data-posted'))*1000;
       var diff = RequestTime*1000 - time;
       if(timeLimits.no_limit_id.indexOf(UserID)==-1){
         if(diff > time_edit && timeLimits.edit_groups.indexOf(GroupID)!=-1 && time_edit!=-1){
            $(this).find('.pl-edit').remove();
         }
         if(diff > time_delete && timeLimits.delete_groups.indexOf(GroupID)!=-1 && time_delete!=-1){
            $(this).find('.pl-delete').remove();
         }
       }
    });
    $('style#hide_edit01').remove();

  }); //End// - Работа в топике
  //===========================================//


  //===== РАБОТА на странице #pun-edit ========//
  if($('#pun-edit').length)$().pun_mainReady(function(){
    var time = $('#post').attr('data-posted');
    time = parseInt(time)*1000;
    var diff =  RequestTime*1000 - time;
    if(timeLimits.no_limit_id.indexOf(UserID)==-1 && time_edit!=-1){
      if(diff > time_edit && timeLimits.edit_groups.indexOf(GroupID)!=-1){
         setErrorInfo()
      }
    }
    $('style#hide_edit02').remove();

  }); //End// - Работа на #pun-edit
  //===========================================//


  //===== РАБОТА на странице #pun-delete ======//
  if($('#pun-delete').length)$().pun_mainReady(function(){
    var time = $('.main>.formal>form').attr('data-posted');
    time = parseInt(time)*1000;
    var diff =  RequestTime*1000 - time;
    if(timeLimits.no_limit_id.indexOf(UserID)==-1 && time_delete!=-1){
      if(diff > time_delete && timeLimits.delete_groups.indexOf(GroupID)!=-1){
         setErrorInfo()
      }
    }
    $('style#hide_edit02').remove();

  }); //End// - Работа на #pun-delete
  //===========================================//


}