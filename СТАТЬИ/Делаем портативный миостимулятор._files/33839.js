﻿(function(){
function setcOOkie2(a,b,c) {if(c){var d = new Date();d.setTime(d.getTime()+c);}if(a && b) document.cookie = a+'='+ encodeURIComponent(b) +(c ? '; expires='+d.toUTCString() : '');else return false;}
function getcOOkie2(a) {var b = new RegExp(a+'=([^;]){1,}');var c = b.exec(document.cookie);if(c) c = c[0].split('=');else return false;return c[1] ? decodeURIComponent(c[1]) : false;}

var tems = getcOOkie2('delDone');
var pageRest = getcOOkie2('restorelnkDel')
if(tems && pageRest && ($('#pun-viewtopic').length||$('#pun-viewforum').length) && tems == document.URL.split(/&p=|#p/g)[0] &&  tems == pageRest.split(/&p=|#p/g)[0]){
  setcOOkie2('delDone',true,-1);
  setcOOkie2('restorelnkDel',true,-1);
  window.stop();location.replace(pageRest);
}

$(document).ready(function(){
    var lnk = document.URL;
  $('#pun-viewtopic #topic-modmenu').live('change',function() {
    if($(this).find('option:selected').val().search(/open=|close=/)==-1) return;
    var Tema =  lnk.split(/&p=|#p/g)[0];
    setcOOkie2('restorelnkDel',lnk);
    setcOOkie2('delDone',Tema,2*40*1000);
  });
  $('#pun-viewtopic').find('.pl-delete > a').live('click',function() {
    var thPost = $(this).parents('.post');
    var firstPst = $('.post').not(thPost).eq(0);
    var Tema = document.URL.split(/&p=|#p/g)[0];
    var pageNum  = +($('.pagelink:first').find('strong:last,.current').text());
    if(firstPst.length){pageNum=pageNum} else { pageNum = pageNum - 1}
    var lnk = Tema+'&p='+pageNum;
    setcOOkie2('restorelnkDel',lnk)
  });
  $('#pun-multidelete').find('form[action*="moderate.php?fid"] input[name="delete_posts"]').live('click',function() {
    var thPost = $('.post').find('.pl-select input[type="checkbox"]').not(':checked').parents('.post');
    var Tema = $('p.crumbs:first a:last').attr('href');
    var pageNum  = +($('.pagelink:first').find('strong:last,.current').text());
    if(thPost.length){pageNum=pageNum} else { pageNum = pageNum - 1}
    var lnk = Tema+'&p='+pageNum;
    setcOOkie2('restorelnkDel',lnk);
  });
  $('#pun-modviewforum').find('form[action*="moderate.php?fid"] input[name="delete_topics"]').live('click',function() {
    var thTems = $('.forum').find('td.tcmod input[type="checkbox"]').not(':checked').parents('td.tcmod');
    var forum = $('p.crumbs:first a:last').attr('href');
    var pageNum  = +($('.pagelink:first').find('strong:last,.current').text());
    if(thTems.length){pageNum=pageNum} else { pageNum = pageNum - 1}
    var lnk = forum+'&p='+pageNum;
    setcOOkie2('restorelnkDel',lnk);
  });
  $('#pun-delete,#pun-deleteposts,#pun-deletetopic').find('form').find('input[value="Удалить"]').live('click',function() {
    var Tema = $('p.crumbs:first a:last').attr('href');
    setcOOkie2('delDone',Tema,2*40*1000);
  });
});
}());