/****************************************************
 * View the post voters v2.2 / 2.0.1
 * Author: Romych
 * Date: 16.05.2016 / 28.08.2017
****************************************************/

FORUM.isDifferentHostname = false;
try {
	self.document.location.hostname == top.document.location.hostname
} catch (e) {
	FORUM.isDifferentHostname = true
}
if (self == top || self.parent == top && FORUM.isDifferentHostname) {
	delete FORUM.isDifferentHostname;
	$(function() {
		var lang_obj = {
			"�������� ����": {en: "Show all"},
			"�������": {en: "voters"},
			"�������": {en: "Voters"}
		},
			lang = $("html")[0].lang;

		function _(text) {
			return (lang == "ru" || !(lang_obj[text] && lang_obj[text][lang])) ? text : lang_obj[text][lang]
		};

		function a() { 
			/*==|||==;

#respect td {
	vertical-align: top;
}
#respect figure {
	margin: 2px;
	width: 48px;
	word-wrap: break-word;
}
#respect figure:hover .vote {
	display: none;
}
#respect figcaption {
	font-size: .9em;
	margin-top: -5px;
}
#respect .person {
	border-radius: 2px;
	height: 40px;
	width: 40px;
}
#respect .prof {
	color: #fff!important;
}
#respect .prof:hover {
	text-decoration: underline !important;
}
.rating_theme .person {
	height: 78px;
	width: 78px;
}
figure .vote {
	border-radius: 50%;
	color: #fff;
	box-shadow: 0 1px 1px 1px rgba(0,0,0,.3);
	width: 14px;
	height: 14px;
	position: absolute;
	z-index: 2;
}
figure .vote.plus {
	background: #75CD33;
	line-height: 15px;
}
figure .vote.plus:before {
	content: "+";
	font-family: arial,helvetica,sans-serif;
	font-size: 17px;
	width: 14px;
	height: 14px;
	font-weight: 700;
	text-shadow: 0 0 1px rgba(0,0,0,.5)
}
figure .vote.minus {
	background: #DC261D;
	line-height: 13px;
}
figure .vote.minus:before {
	content: "�";
	font-family: arial,helvetica,sans-serif;
	font-size: 15px;
	width: 14px;
	height: 14px;
	font-weight: 700;
	text-shadow: 0 0 1px rgba(0,0,0,.5)
}
#all_voters {
	border-collapse: collapse;
}
#all_voters tbody {
	display: block;
	max-height: 40em !important;
	overflow-x: hidden;
	overflow-y: auto;
}
#pun .punbb .main #all_voters td {
	vertical-align: top;
	border: none 0 transparent !important;
	padding: 0;
	text-align: center;
}
#all_voters figure {
	margin: 5px;
	width: 82px;
	word-wrap: break-word;
	position: relative;
}
#all_voters figure a {
	display: inline-block;
	height: 100%;
	backface-visibility: hidden;
	box-shadow: 0 0 1px rgba(0,0,0,0);
	padding-bottom: .6em;
	-moz-osx-font-smoothing: grayscale;
	position: relative;
	transform: translateZ(0);
	-webkit-backface-visibility: hidden;
	-webkit-transform: translateZ(0);
}
#all_voters figure a:before {
	background: currentColor;
	bottom: 0;
	content: "";
	height: 4px;
	left: 50%;
	position: absolute;
	right: 50%;
	transition-duration: .3s;
	transition-property: left,right;
	transition-timing-function: ease-out;
	-webkit-transition-duration: .3s;
	-webkit-transition-property: left,right;
	-webkit-transition-timing-function: ease-out;
	z-index: -1;
}
#all_voters figure:hover .vote {
	display: none;
}
#all_voters figure:hover a:before,#all_voters figure:focus a:before,#all_voters figure:active a:before {
	left: 0;
	right: 0;
}
#all_voters figcaption {
	font-size: .9em;
}
.tipsy .tipsy-inner {
	max-width: 820px !important;
	min-width: 30px !important;
}
.prof {
	text-decoration: none !important;
}
@media screen and (max-width:540px) {
	.rating_theme .person {
		height: 40px!important;
		width: 40px!important;
	}
	#all_voters figure {
		margin: 2px 5px;
		width: auto !important;
	}
	#pun .punbb .main .pun-modal.rating_theme .closer {
		position: absolute !important;
	}
	#all_voters tbody {
		padding-right: 1em;
		margin-right: -1em;
		max-height: 30em !important;
	}
}
@media screen and (max-width:400px) {
	#all_voters figure {
		margin: 2px 2px;
		width: auto !important;
	}
}
			==|||==;*/ 
		}
		a = "<style>" + (a.toString().split("==|||==;")[1]) + "</style>";
		$(a).appendTo("head");
		$("div.post").map(function() {
			try {
				var h = $(this).find("div.post-content").attr("id").slice(1, -8)
			} catch (d) {
				h = 0
			}
			var b = $(this).find(".post-rating p");
			var l, c, i, f, j = [];

			function k() {
				$(".tipsy").fadeOut(200);
				setTimeout(function() {
					$(".tipsy").remove()
				}, 200)
			}

			function g() {
				c = setTimeout(function() {
					k()
				}, 500)
			}
			$(b).on("mouseenter", function() {
				$(this).tipsy({
					fade: !0,
					gravity: "se",
					trigger: "manual",
					html: !0
				});
				var m = '<table id="respect"><tr><td style="font-weight:bolder"> ' + _('�������') + ': </td></tr><tr>';
				var n = '<table id="all_voters" width="100%"><tr>';
				var $n;
				l = setTimeout(function() {
					$.get("/api.php", {
						method: "post.getVotes",
						sort_dir: "desc",
						limit: 100,
						post_id: h,
						fields: "user_id,username,avatar,value,datetime"
					}, function(s) {
						if (!s.response) return;
						var o = s.response.votes;
						for (var r in o) {
							var q = o[r];
							f = q.value;
							if (f == 1) {
								f = "plus"
							} else {
								f = "minus"
							}
							i = q.avatar;
							if (i == "") {
								i = StaticURL + "/i/default_avatar.jpg"
							} else {
								var _j = new Image();
								_j.src = i;
								_j.data = q.user_id;
								_j.onload = function() {
									if (this.height > this.width) {
										$('.tipsy .person[data="' + this.data + '"]').css("background-size", "100% auto");
										if ($n)
											$n.find('.person[data="' + this.data + '"]').css("background-size", "100% auto");
									}
								}
							}
							j[r] = '<td><figure><span class="vote ' + f + '"></span><a class="prof" href="/profile.php?id=' + q.user_id + '"><img data="' + q.user_id + '" class="person" src="' + StaticURL + '/i/blank.gif" style="background:url(\'' + i + '\');background-size:auto 100%;background-position:center center" title="' + q.datetime + '"><figcaption><p>' + q.username + "</p></figcaption></a></figure></td>"
						}
						m += j.slice(0, 5).join(" ");
						m += "</tr></table>";
						if (j.length >= 6) {
							m += '<p id="showt" style="cursor:pointer;font-weight:bold;margin:5px;"> ' + _('�������� ����') + ' </p>'
						}
						$(b).attr("original-title", m).tipsy("show");
						for (var p = 0; p < j.length; p++) {
							n += j[p];
							if (p % 5 == 4) {
								n += "</tr><tr>"
							}
						}
						n += "</tr></table>";
						$n = $(n);
						$("#showt").mybbModal({
							theme: "rating_theme",
							title: j.length + " " + _("�������"),
							onopen: function() {
								if ($('.pun-modal:visible').length)
									k()
							},
							onfirstset: function() {
								var t = $(".pun-modal.rating_theme").find("table tr:first"),
									w = t.width(),
									h = t.height();
								w += parseInt($(".rating_theme .container").css("padding-left")) * 2;
								if (h * $(".rating_theme tr").length > $("#all_voters").height())
									w += 16;
								$(".pun-modal.rating_theme .modal-inner").css("width", w + "px");
								var l = t.find("figure").length;
								$(".pun-modal.rating_theme figure").css("max-width", ($("#all_voters").width() / l - parseInt(t.find("figure:first").css("margin-left"))*2) + "px");
							},
							onclose: function() {
								setTimeout(function() {
									$(".pun-modal.rating_theme").remove();
								}, $.fn.mybbModal.defaults.animationDuration);
							},
							content: $n,
							css: {
								width: "38.5em",
								marginTop: "4%"
							}
						})
					}, "json")
				}, 600)
			}).on("mouseleave", function() {
				clearTimeout(l);
				g();
				$(".tipsy").on("mouseover", function() {
					clearTimeout(c)
				});
				$(".tipsy").on("mouseout", function() {
					g()
				})
			});
		})
	})
};