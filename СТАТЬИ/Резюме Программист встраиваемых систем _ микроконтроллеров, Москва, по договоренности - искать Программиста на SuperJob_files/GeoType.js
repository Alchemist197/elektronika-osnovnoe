
(function(){'use strict';var GeoType;GeoType={TOWN:'town',REGION:'region',FEDERATION_SUBJECT:'oblast',COUNTRY:'country'};angular.module('hr.common').constant('GeoType',GeoType)})();