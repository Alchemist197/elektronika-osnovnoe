
ComboBoxLocalDatasource=function(initialList){this.list=initialList;this.filter=function(mask,object,callback){if(mask==''){callback.apply(object,[this.list]);}
else{var patt=RegExp('(^|[^А-ЯЁа-яё])'+mask,"i");var result=[];result=jQuery.map(this.list,function(element){if(element.objects){var result=jQuery.map(element.objects,function(subelement){if(subelement.title.trim().match(patt)){var result={'title':element.title,'id':element.id};if(element.descr){result.descr=element.descr;}
return result;}});return result;}
else{if(element.title.trim().match(patt)){var result={'title':element.title,'id':element.id};if(element.descr){result.descr=element.descr;}
return result;}}});callback.apply(object,[result]);}};};ComboBoxRemoteDatasource=function(remote_address,param_list){this.remote=remote_address;this.parameters=param_list;this.filter=function(mask,object,callback){$.post(this.remote,{'requestType':this.parameters.type,'requestBody':mask},function(data){callback.apply(object,[data]);},'json');};};ComboBox=function(root,options,_dispatcher){this.dispatcher=_dispatcher;this.comboBox=root;var default_config={appendToComboBox:false,hideNameAttr:false,keyUpCheckDelay:50,kbSkipFirst:5,kbInterval:100,theme:'cbDefault',inputCanFloat:false,onSelect:function(){},onShowDropdownList:function(){},dropdownClass:'dropdown',generateValueOnSelect:function($option){return $('span.cbOptionTitle',$option).text();},generateTitleOnSelect:function($option){return'';}};options=$.extend(default_config,options);var cbSelect=$(this.comboBox).children('select');this.id=$(this.comboBox).attr('id');this.name=options.hideNameAttr?undefined:cbSelect.attr('name');this.drawnOptions={};this.blurCancelled=false;this.kbTimer=false;this.kbTimerType='none';this.kbInterval=options.kbInterval;this.kbSkipFirst=options.kbSkipFirst;this.keyUpCancelled=false;this.keyUpTimer=false;this.keyUpCheckDelay=options.keyUpCheckDelay;this.theme=options.theme;this.title=cbSelect.attr('title')||false;this.inputCanFloat=options.inputCanFloat;this.selectedIndex=-1;this.onSelect=options.onSelect;this.onShowDropdownList=options.onShowDropdownList;this.dropdownClass=options.dropdownClass;this.appendTo=options.appendToComboBox?$(this.comboBox):window.document.body;this.generateValueOnSelect=options.generateValueOnSelect;this.generateTitleOnSelect=options.generateTitleOnSelect;this.getInputEventUse=true;var reference=this;var cb_total_width=$(this.comboBox).addClass(this.theme).outerWidth();this.$label=$(document.createElement('label')).attr({'for':'cb_'+this.name}).text(options.label).appendTo(this.comboBox);this.$input=$(document.createElement('input')).attr({'type':'text','id':cbSelect.attr('id'),'autocomplete':'off','name':this.name}).appendTo(this.comboBox);this.$button=$(document.createElement('div')).addClass('dropdownBt').appendTo(this.comboBox);this.$buttonF=$(document.createElement('div')).addClass('filterBt').appendTo(this.comboBox);this.$progressBar=$(document.createElement('div')).addClass('progressBar').appendTo(this.comboBox);this.$dropdown=$(document.createElement('div')).addClass(reference.dropdownClass).appendTo(this.appendTo);this.$opts_holder=$(document.createElement('div')).attr({'class':'optionsHolder'});this.$dropdown.append(this.$opts_holder).bind('scroll',function(){var dd=reference.$dropdown;var oh=reference.optionHeight;var ls=reference.optList.length;var scrt=dd.scrollTop();var h=dd.height();var minNo=Math.floor(scrt/oh);var maxNo=Math.ceil((scrt+h)/oh);var i=minNo;while(i<=maxNo&&i>=0&&i<ls){if(!reference.drawnOptions[i]){reference.drawOption(i);}
i++;}});var cb_position=this.$input.position();var cb_height=this.$input.outerHeight();var old_width=cbSelect.width();this.min_width=this.$dropdown.css('width').replace(/[^0-9]*$/,'');if(this.min_width==''){this.min_width=old_width;}
if(options.ignoreOldWidth){if(this.$input.width()>this.min_width){this.dropDownWidth=this.$input.width();}else{this.dropDownWidth=this.min_width;}}
else{if(old_width>this.min_width){this.dropDownWidth=old_width;}else{this.dropDownWidth=this.min_width;}}
this.$dropdown.css('width',this.dropDownWidth);if(!options.ignoreOldWidth)
{this.$input.css('width',old_width);}
this.$dropdown.css({'width':this.$dropdown.width(),'top':cb_position.top+cb_height,'left':0});var $selectedOption=cbSelect.children('option:selected'),selectedOptionName=$selectedOption.text()||'',selectedOptionTitle=$selectedOption.attr('title')||'';if(selectedOptionName){this.$input.val(selectedOptionName.trim().replace(/\s+/g,' '));}
if(selectedOptionTitle){this.$input.attr('title',selectedOptionTitle.trim().replace(/\s+/g,' '));}
selectedOptionName=null;if(this.title){this.$input.bind('focus',function(){if(this.value==reference.title){this.value='';}}).bind('blur',function(){if(this.value==''){this.value=reference.title;}}).triggerHandler('blur');}
this.optsHolderWidth=this.dropDownWidth;this.optionHeight=30;this.placeDropDown=function(viewPort){var cb_offset=this.$input.offset();var
cb_top=cb_offset.top,cb_left=cb_offset.left;if(viewPort){cb_top-=viewPort.offset().top;cb_left-=viewPort.offset().left;}else{viewPort=$(window);}
cb_top-=viewPort.scrollTop();var
dd_h=parseInt(this.dropDownHeight),dd_w=parseInt(this.dropDownWidth),cb_h=cb_height,cb_w=old_width,vp_h=viewPort.height(),vp_w=viewPort.width();var o_t=0,o_l=0;if(!options.appendToComboBox){o_t=cb_offset.top;o_l=cb_offset.left;}
if(cb_top+cb_h+dd_h<=vp_h){o_t+=cb_h;}else if(cb_top>=dd_h){o_t+=-dd_h;}else if(vp_h-cb_top-cb_h>cb_top){o_t+=cb_h;}else{o_t+=-dd_h;}
if(cb_left+dd_w<=vp_w){o_l+=0;}else if(cb_left>=dd_w){o_l+=cb_w-dd_w;}else if(vp_w-cb_left-dd_w>cb_left){o_l+=0;}else{o_l+=cb_w-dd_w;}
real_width=this.$dropdown.width();if(o_l+real_width>$(window).width())
{o_l=cb_left+this.$input.width()-real_width+10;}
this.$dropdown.css({left:o_l,top:o_t});};if(this.comboBox.css('position')!='relative')
{this.placeButtons=function(){$('#no-actual-id').add(reference.$button).add(reference.$buttonF).add(reference.$progressBar).css({top:reference.$input.offset().top,left:reference.$input.offset().left
+(options.ignoreOldWidth?this.$input.width()-10:old_width)}).end().css('display','block');};}else{this.placeButtons=function(){};}
function resetPosition(){var r=reference;r.placeDropDown();r.placeButtons();}
$(window).load(resetPosition).resize(resetPosition);if(options.inputCanFloat){$(window).scroll(resetPosition);}
window.setTimeout(resetPosition,1);this.$dropdown.bgiframe();this.opened=false;this.changed=true;this.mousein=false;this.focused=false;this.min_length=options.min_length||3;this.max_height=options.max_height||300;this.dropDownHeight=this.max_height;this.datasource=this.dispatcher.allocateDatasource(this);this.$button.click(function(){reference.$input.focus();if(reference.opened){reference.close();}
else{reference.showFullList();}});this.$buttonF.click(function(){reference.$input.focus();if(reference.opened){reference.close();}
else if(reference.$input.val().length>=reference.min_length){reference.showFilteredList();}});this.$dropdown.mouseenter(function(){reference.mousein=true;}).mouseleave(function(){reference.mousein=false;reference.$input.trigger('focus');});this.$input.blur(function(){reference.blurCancelled=false;window.setTimeout(function(){if(!reference.blurCancelled&&!reference.mousein)
reference.close();},200);reference.onSelect();}).focus(function(){reference.blurCancelled=true;});function keyHandler(keyevent){if(/^13$/.test(keyevent.keyCode)){}
var repeatcount;function goToOrOpen(callback,params){if(repeatcount++<reference.kbSkipFirst)
return;if(!reference.opened){reference.refreshShowTimer(callback,params);}
else{callback.apply(reference,params);}}
function goToHome(){goToOrOpen(reference.highlightFirst,[]);}
function goToEnd(){goToOrOpen(reference.highlightLast,[]);}
function goToNext(){goToOrOpen(reference.highlightAnother,[1,0]);}
function goToPrev(){goToOrOpen(reference.highlightAnother,[-1,0]);}
function goToNextPage(){goToOrOpen(reference.highlightAnother,[1,reference.pageHeight]);}
function goToPrevPage(){goToOrOpen(reference.highlightAnother,[-1,reference.pageHeight]);}
function handleKey(keyName,callback){if(reference.opened){if(keyevent.preventDefault)
keyevent.preventDefault();if(keyevent.stopPropagation)
keyevent.stopPropagation();}
if(reference.kbTimerType!=keyName){callback();if(reference.kbTimer)
window.clearInterval(reference.kbTimer);repeatcount=0;reference.kbTimer=window.setInterval(callback,reference.kbInterval);reference.kbTimerType=keyName;reference.keyUpCancelled=true;}}
switch(keyevent.keyCode){case 9:case 27:reference.close();break;case 13:if(reference.opened){if(keyevent.preventDefault)
keyevent.preventDefault();if(keyevent.stopPropagation)
keyevent.stopPropagation();keyevent.cancelBubble=true;keyevent.returnValue=false;reference.selectCurrent();}
break;case 33:handleKey('pageup',goToPrevPage);break;case 34:handleKey('pagedown',goToNextPage);break;case 36:handleKey('home',goToHome);break;case 35:handleKey('end',goToEnd);break;case 38:handleKey('up',goToPrev);break;case 40:handleKey('down',goToNext);break;case 37:case 39:break;default:window.clearTimeout(reference.kbTimer);reference.kbTimerType='none';reference.changed=true;reference.refreshShowTimer();}};this.$input.bind('focus',function(){var obj=$(this).get(0);if(obj.setSelectionRange)
{obj.setSelectionRange(0,0);}
else if(obj.createTextRange)
{var range=obj.createTextRange();range.collapse(true);range.moveEnd('character',0);range.moveStart('character',0);range.select();}});this.$input.bind('keydown',keyHandler);this.$input.bind('keyup',function(){var r=reference;r.keyUpCancelled=false;if(r.keyUpTimer)
window.clearTimeout(r.keyUpTimer);r.keyUpTimer=window.setTimeout(function(){if(!r.keyUpCancelled){window.clearTimeout(r.kbTimer);r.kbTimerType='none';}},r.keyUpCheckDelay);});this.highlightedOption=false;this.highlightOption=function($option){if(this.highlightedOption){this.highlightedOption.removeClass('selected');}
$option.addClass('selected');this.highlightedOption=$option;};this.selectOption=function($option){this.$input.val(this.generateValueOnSelect($option)).attr('title',this.generateTitleOnSelect($option)).focus();this.selectedIndex=$option.attr('id').substr(this.id.length);if(this.selectedIndex==0)
{this.changed=true;}
this.emitChanged();};this.$dropdown.delegate('.comboBoxOption',{mouseenter:function(){reference.highlightOption($(this));},click:function(){reference.selectOption($(this));reference.close();reference.onSelect();}});cbSelect.remove();cbSelect=null;};ComboBox.prototype={timer:null,close:function(){this.$dropdown.hide();this.opened=false;},open:function(){this.$dropdown.show();this.opened=true;var h;if(this.$opts_holder.height()>this.max_height){h=this.max_height;}else{h=this.$opts_holder.height();}
this.$dropdown.css({'height':h+'px'});this.dropDownHeight=h;this.placeDropDown();this.$dropdown.trigger('scroll');},selectCurrent:function(){if(this.highlightedOption){this.selectOption(this.highlightedOption);this.close();};this.onSelect();},findFirstOption:function(){if(!this.optList||!this.optList.length)
return false;var index=0;while(typeof(this.optList[index].group)!='undefined')
index++;if(!this.drawnOptions[index])
this.drawOption(index);return $('#'+this.id+index);},findLastOption:function(){if(!this.optList||!this.optList.length)
return false;var index=this.optList.length-1;while(typeof(this.optList[index].group)!='undefined')
index--;if(!this.drawnOptions[index])
this.drawOption(index);return $('#'+this.id+index);},highlightFirst:function(){var $option=this.findFirstOption();if($option){this.highlightOption($option);this.scrollTo($option);}},highlightLast:function(){var $option=this.findLastOption();if($option){this.highlightOption($option);this.scrollTo($option);}},highlightAnother:function(direction,offset){var $option;if((!this.highlightedOption)||(this.highlightedOption.length==0)){$option=this.findFirstOption();}else{var optIndex=+this.highlightedOption.attr('id').substr(this.id.length);if(direction>0)
optIndex+=offset;else
optIndex-=offset;do{if(direction>0)
optIndex++;else
optIndex--;if(optIndex<0||optIndex>=this.optList.length)
return;if(!this.drawnOptions[optIndex])
this.drawOption(optIndex);$option=$('#'+this.id+optIndex);}while($option.hasClass('comboBoxOptionGroup'));}
if($option){this.highlightOption($option);this.scrollTo($option);}},scrollTo:function($option){if($option.length){var height=$option.outerHeight();var offset=+$option.css('top').replace('px','');var prev_top=this.$dropdown.scrollTop();var total_height=this.$dropdown.innerHeight();var new_top;if(offset+height>prev_top+total_height){new_top=offset+height-total_height;}
if(offset<prev_top){new_top=offset;}
this.$dropdown.scrollTop(new_top);}},refreshShowTimer:function(callback,params){clearTimeout(this.timer);var reference=this;this.timer=setTimeout(function(){reference.showFilteredList(callback,params);},700);},showFullList:function(){var reference=this;this.getInputEvent();if(this.changed){this.clearDropdown();var list=this.datasource.filter('',reference,this.processResult);}
else{this.open();}
if(this.onShowDropdownList){this.onShowDropdownList();}},processResult:function(list){var res_count=this.populateDropdown(list);this.changed=false;this.$progressBar.hide();if(res_count){this.open();}},showFilteredList:function(callback,params){if(this.changed){this.clearDropdown();this.$progressBar.show();var reference=this;var list=this.datasource.filter(this.$input.val(),reference,this.processResult);this.getInputEvent('filter');}
else{this.open();}
if(callback){callback.apply(this,params);}
if(this.onShowDropdownList){this.onShowDropdownList();}},clearDropdown:function(){this.$opts_holder.empty();this.highlightedOption=false;},drawOption:function(index){this.addOption(this.id,this.$opts_holder,index);},addOption:function(namespace,$hook,index){if(!this.optList)
return;var listItem=this.optList[index];if(!listItem){return 0;}
var $option=$(document.createElement('div'));var classes=[];if(typeof(listItem.group)=='undefined'){classes.push('comboBoxOption');}else{classes.push('comboBoxOptionGroup');}
if(typeof(listItem.classes)=='string'){classes.push(listItem.classes);}else if(typeof(listItem.classes)=='object'){classes.push(listItem.classes.join(' '));}
$option.attr({'class':classes.join(' '),'id':namespace+''+index});$option.append($('<span class="cbOptionTitle" />').html(listItem.title?listItem.title:'&nbsp;').attr('title',listItem.hint));if(listItem.descr!=null){$option.append($(document.createElement('span')).attr({'class':'cbOptionDescr'}).html(listItem.descr?listItem.descr:'&nbsp;'));}
this.drawnOptions[index]=true;$hook.append($option);return $option.css({'top':this.optionHeight*index,'width':this.$dropdown.width()});},populateDropdown:function(list){var reference=this;this.drawnOptions={};this.optList=list;if(!list.length){return 0;}
this.$opts_holder.parent().show();var lastOption=this.addOption(this.id,this.$opts_holder,list.length-1);this.optionHeight=lastOption.height();this.pageHeight=Math.floor(this.dropDownHeight/this.optionHeight)-1;this.$opts_holder.css('height',this.optionHeight*list.length).parent().hide();lastOption.css({'top':this.optionHeight*(list.length-1),'width':this.$dropdown.width()});return list.length;},setInnerText:function(text){this.close();this.$input.val(text);},emitChanged:function(){this.dispatcher.controlChanged(this);},getInputEvent:function(data){if(this.dispatcher.inputCatcher){this.getInputEvent=function(data){this.dispatcher.inputCatcher(this,data);}
this.getInputEvent(data);}}};