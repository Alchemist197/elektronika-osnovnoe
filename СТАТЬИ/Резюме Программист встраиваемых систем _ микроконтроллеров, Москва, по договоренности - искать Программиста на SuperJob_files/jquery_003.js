
(function($){function bindChange(element){return function(){element.bind('keyup change',detectIfValueChanged);};}
function bindTextInput(element){return function(){element.on('input keypress keyup',detectIfValueChanged).on('paste cut',function(){var el=this;window.setTimeout(function(){detectIfValueChanged.apply(el);},0);});};}
function bindInputStateChange(element){return function(){var originalElement=element.get(0),timer;if('onpropertychange'in originalElement){element.bind('propertychange',function(e){var el=this;if(e.originalEvent.propertyName==='checked'){if(timer){window.clearTimeout(timer);}
timer=window.setTimeout(function(){detectIfValueChanged.call(el);},1);}});}else{element.bind('change',detectIfValueChanged);}};}
function initMethod(input){if(!('get'in input)){throw new TypeError('jQuery object expected');}
var
el=input.get(0);if(el.tagName==='SELECT'){return bindChange(input);}else if(el.tagName==='TEXTAREA'){return bindTextInput(input);}else if(el.tagName==='INPUT'){switch(el.getAttribute('type')){case'checkbox':case'radio':return bindInputStateChange(input);case'text':case'search':case'password':return bindTextInput(input);default:return bindChange(input);}}else if(el.tagName=='BUTTON'){return function(){};}}
function elementValueDataKey(element){var name=element.attr('name');return'lastFormValue['+(name===undefined?'':name)+']';}
function detectIfValueChanged(){function _toString(obj){return Object.prototype.toString.apply(obj);}
function areEqual(v1,v2){if(_toString(v1)!==_toString(v2)){return false;}
switch(_toString(v1)){case'[object Array]':return $(v1).not(v2).length+$(v2).not(v1).length===0;default:return v1===v2;}}
if(!('form'in this)){throw new TypeError('Form element expected');}
var
element=$(this),form=$(this.form),dataKey=elementValueDataKey(element),oldValue=form.data(dataKey),newValue=element.formVal(),e;if(!areEqual(newValue,oldValue)){e=$.Event('valuechange',{oldValue:oldValue,newValue:newValue});element.trigger(e);form.data(dataKey,newValue);}}
function initFormElement(input){initMethod(input)();$(input.get(0).form).data(elementValueDataKey(input),input.formVal());}
$.fn.formVal=function(){var
element=this.get(0),formBlock,formBlockId,impl,implName,value;var implementations={phone:function(formBlock){return{country_code:formBlock.find('input.phone-country-code').eq(0).val(),city_code:formBlock.find('input.phone-city-code').eq(0).val(),number:formBlock.find('input.phone-number').eq(0).val(),additional:formBlock.find('input.phone-additional').eq(0).val(),sms:formBlock.find('input.sms').eq(0).formVal(),hour_from:formBlock.find('input.phone-hour_from').eq(0).formVal(),hour_to:formBlock.find('input.phone-hour_to').eq(0).formVal()};},date:function(formBlock){return{year:formBlock.find(':input.year').eq(0).val(),month:formBlock.find(':input.month').eq(0).val(),day:formBlock.find(':input.day').eq(0).val()};},plainArray:function(formBlock){value=[];formBlock.find(':input[name="'+element.name+'[]"]').filter(function(){return this.checked&&!this.disabled;}).each(function(){var element=this;if(!~$.inArray(element.value,value)){value.push(element.value);}});return value;}};if(!element){return undefined;}
if(!('form'in element)){formBlockId=$(element).data('formvalContainer');if(!formBlockId){return undefined;}
formBlock=$('#'+formBlockId);if(!formBlock.length){return undefined;}
implName=$(element).data('formvalImpl');if(!implName){return undefined;}
impl=implementations[implName];if(impl){return impl(formBlock);}else{return undefined;}}
if(element.tagName==='INPUT'){switch(element.getAttribute('type')){case'checkbox':value=[];$(element.form[element.name]).filter(function(){return this.checked&&!this.disabled;}).each(function(){var element=this;if(!~$.inArray(element.value,value)){value.push(element.value);}});break;case'radio':value=undefined;$(element.form[element.name]).filter(function(){return this.checked&&!this.disabled;}).each(function(){var element=this;value=element.value;});break;default:value=$(element).val();}
return value;}else if(element.tagName==='SELECT'||element.tagName==='TEXTAREA'){return $(element).val();}};$.fn.syncFormVal=function(){return this.each(function(){var element=$(this);var form=this.form;if(!form){return;}
$(form).data(elementValueDataKey(element),element.formVal());});};$.fn.registerValuechangeEvent=function(){return this.each(function(){var el=this;if(el.form){initFormElement($(el));}else{$(el).find('.form-block-container').each(function(){var block=$(this);if(!block.hasClass('skip-valuechange-init')){block.find(':input').each(function(){initFormElement($(this));});}});}});};})(jQuery);