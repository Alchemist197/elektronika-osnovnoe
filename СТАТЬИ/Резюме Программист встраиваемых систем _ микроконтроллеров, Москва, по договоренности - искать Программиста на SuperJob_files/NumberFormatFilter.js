
(function(){'use strict';function NumberFormatFilter(){return function(val,thousandSep,showDecimals,decPoint){var formatted,decimals,intPart;if(_.isUndefined(val)){return 0;}
val=val.toString();thousandSep=_.isString(thousandSep)?thousandSep:' ';intPart=val.replace(/(\.\d+)$/,'');formatted=intPart.replace(/(\d)(?=(\d{3})+(?!\d))/g,'$1'+thousandSep);if(showDecimals){decPoint=decPoint?decPoint:'.';decimals=val.indexOf('.')==-1?'':val.replace(/^-?\d+(?=\.)/,'').slice(1);formatted=formatted+decPoint+decimals;}
return formatted;}}
angular.module('hr.common').filter('numberFormat',NumberFormatFilter)})();