
(function(){'use strict';function HrMetaStorage(){var HrMetaStorage,storage;HrMetaStorage={};storage={};HrMetaStorage.get=function(key){return storage[key]?storage[key]:false;};HrMetaStorage.setFromResponse=function(data){_.assign(storage,data);};return HrMetaStorage;}
angular.module('hr.common').service('HrMetaStorage',HrMetaStorage)})();