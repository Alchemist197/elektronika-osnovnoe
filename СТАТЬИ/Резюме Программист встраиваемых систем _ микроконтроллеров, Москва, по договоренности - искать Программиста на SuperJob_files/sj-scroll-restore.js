
(function(){'use strict';var SCROLL_PROPERTY='pagesScroll';angular.module('angie').factory('sjScrollRestore',sjScrollRestore);sjScrollRestore.$inject=['$window'];function sjScrollRestore($window){var localStorage=$window.localStorage;var $currentDocument=angular.element(document);return{storeScroll:storeScroll,restoreAndClearScroll:restoreAndClearScroll};function storeScroll(locationMark){var currentScroll=$currentDocument.scrollTop();var currentScrollStorage={};currentScrollStorage[locationMark]=currentScroll;localStorage.setItem(SCROLL_PROPERTY,JSON.stringify(currentScrollStorage));}
function restoreAndClearScroll(locationMark){restoreScroll(locationMark);clearScroll();}
function restoreScroll(locationMark){var scrollStorage=null;try{scrollStorage=JSON.parse(localStorage.getItem(SCROLL_PROPERTY));}catch(error){scrollStorage=null;}
if(!scrollStorage){return;}
var storedScroll=scrollStorage[locationMark];if(_.isNumber(storedScroll)&&storedScroll>=0){$currentDocument.scrollTop(storedScroll);}}
function clearScroll(){localStorage.removeItem(SCROLL_PROPERTY);}}})();