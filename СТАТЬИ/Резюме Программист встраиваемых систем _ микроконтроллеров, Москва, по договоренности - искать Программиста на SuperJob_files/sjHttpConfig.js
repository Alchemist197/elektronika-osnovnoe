
(function(){'use strict';function StaticRequestInterceptor($q,sjGlobalConfig){return{request:function(config){if(config.url.indexOf(sjGlobalConfig.ST_PATH)==0){delete config.headers['X-Requested-With'];}
return config||$q.when(config);}}}
function sjHttpConfig($httpProvider){$httpProvider.interceptors.push('StaticRequestInterceptor');}
angular.module('sj.utils').factory('StaticRequestInterceptor',StaticRequestInterceptor).config(sjHttpConfig);})();