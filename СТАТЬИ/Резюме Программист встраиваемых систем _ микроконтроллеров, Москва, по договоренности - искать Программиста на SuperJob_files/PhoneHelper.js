
(function(){'use strict';function PhoneHelper($q){var PhoneHelper;PhoneHelper={};PhoneHelper.parse=function(phoneString){var phone,matches;matches=parse(phoneString);if(matches&&matches[1]&&matches[2]&&matches[3]){phone=matches[1]+matches[2]+matches[3].replace(/\D/g,'');}
return phone;};PhoneHelper.toObject=function(phoneString){var phone,matches;matches=parse(phoneString);phone={};if(matches&&matches[1]&&matches[2]&&matches[3]){phone={country_code:matches[1],city_code:matches[2],number:matches[3],additional:phone.additional||''};}
return phone;};PhoneHelper.isUnique=function(){var defer;defer=$q.defer();setTimeout(function(){defer.resolve();},100);return defer.promise;};function parse(string){return string.match(/\+([\d]+)[\s]([\d]+)[\s]([0-9-]{4,})/i);}
return PhoneHelper;}
angular.module('hr.helper').service('PhoneHelper',PhoneHelper)})();