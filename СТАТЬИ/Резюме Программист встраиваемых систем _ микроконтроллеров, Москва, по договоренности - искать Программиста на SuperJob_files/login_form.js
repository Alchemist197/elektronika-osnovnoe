
window.initLoginForm=function(id,callbacks){return function(){$('#'+id).ajaxForm({beforeSubmit:callbacks&&callbacks.beforeSubmit,dataType:'json',success:function(data,status){if('outerRedirect'in data)
{window.location=data.outerRedirect;return;}
if('redirect'in data)
{window.location=data.redirect;return;}
$('.login-popup-top-wrong').html(data.errors.login[0]);if(callbacks&&typeof callbacks.error==='function'){callbacks.error();}},error:callbacks&&callbacks.error});};};$(document).ready(function(){$('a.password-recovery-link').on('click',function(e){var login=$.trim($('#LoginForm_login').val()),pass_recovery_url=$(this).attr('href');if(login!='')
{$('<form/>',{'action':pass_recovery_url,'method':'get'}).append($('<input/>',{'name':'e','type':'text','value':login})).appendTo('body').submit();e.preventDefault();}});});$.fn.passwordToggle=function(options){var init=function(){var $container=$(this),$toggle=$container.find('.js-show-pass'),type='password';$toggle.on('click',function(){var $input=$container.find('.js-input-pass');if($input.val()==''){return;}
type==='password'?type='text':type='password';$toggle.toggleClass('m_active').attr('title',$toggle.data(type+'-title'));id_attr=$input.attr('id');$input.removeAttr('id');var $cloneInput=$("<input>").attr('type',type).attr('name',$input.attr('name')).attr('id',id_attr).attr('placeholder',$input.attr('placeholder')).addClass('js-input-pass').val($input.val())
if($input.hasClass('js-new-sj-input')){$cloneInput.addClass('sj_input js-new-sj-input')}
$cloneInput.insertBefore($input);$input.remove();});};return this.each(init);}
$(function(){$('.js-viewable-password').passwordToggle({});});