
(function(){'use strict';function GeoHelper(GeoType){var GeoHelper;GeoHelper={};GeoHelper.getType=function(str){switch(str){case't':return GeoType.TOWN;case'o':return GeoType.FEDERATION_SUBJECT;case'r':return GeoType.REGION;case'c':return GeoType.COUNTRY;}};return GeoHelper;}
angular.module('hr.common').service('GeoHelper',GeoHelper)})();