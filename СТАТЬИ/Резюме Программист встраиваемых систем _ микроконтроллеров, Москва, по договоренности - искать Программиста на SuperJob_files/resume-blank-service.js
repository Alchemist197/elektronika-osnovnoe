
(function(){'use strict';angular.module('angie').service('ResumeBlankService',ResumeBlankService);ResumeBlankService.$inject=['$q','$http'];function ResumeBlankService($q,$http){var service={data:{activeResumeId:null},resumes:{}};service.setResumeData=setResumeData;service.getResumeData=getResumeData;service.setDataFieldValue=setDataFieldValue;service.getDoubleData=getDoubleData;function setResumeData(resumeId,data){if(!this.resumes[resumeId]){this.resumes[resumeId]={};}
for(var key in data){if(data.hasOwnProperty(key)){this.resumes[resumeId][key]=data[key];}}}
function getResumeData(resumeId,prop){var result=null;if(prop){result=(this.resumes[resumeId]&&this.resumes[resumeId][prop])||null;}else{result=this.resumes[resumeId]||null;}
return result;}
function setDataFieldValue(field,value){this.data[field]=value;}
function getDoubleData(resumeId){var defer=$q.defer();$http.get('/cv/scoring/social/',{params:{id_resume:resumeId}}).then(function(response){defer.resolve(response);}).catch(function(reason){defer.reject(reason);});return defer.promise;}
return service;}})();