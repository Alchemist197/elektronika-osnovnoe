
(function(){'use strict';angular.module('angie').directive('focusOn',focusOn);focusOn.$inject=['$timeout'];function focusOn($timeout){function link(scope,element,attr){scope.$watch(attr.focusOn,function(val){$timeout(function(){if(val){element[0].focus();}else{element[0].blur();}});});}
return{restrict:'A',link:link};}})();