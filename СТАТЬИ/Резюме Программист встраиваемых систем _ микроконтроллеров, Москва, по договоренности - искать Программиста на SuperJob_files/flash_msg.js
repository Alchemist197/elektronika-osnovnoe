
(function(){angular.module('flash_msg',[]);function FlashMsg(){var FlashMsg,renderHelper,queue,queueInterval;FlashMsg={};queue=[];function addToQueue(data){queue.push(data);if(!queueInterval){queueInterval=setInterval(function(){releaseQueue();},500);}}
function releaseQueue(){if(queue.length>0&&renderHelper){renderHelper.render(queue.shift());}
if(queue.length==0){clearInterval(queueInterval);}}
FlashMsg.push=function(text){if(renderHelper){renderHelper.render(text);}else{addToQueue(text);}};FlashMsg.setRenderHelper=function(obj){renderHelper=obj;};return FlashMsg;}
function FlashMsgBlock(FlashMsg){return{link:function(scope,element){var DELAY=5000;var msgs={};function Msg(data){var self=this;var id='msg'+Math.floor((Math.random()*1000)+1);var template='<div class="sj_msg m_black" id='+id+'> \
      <i class="sj_msg_close"></i>'
+data.text+'</div>';var remove=function(){delete msgs[id];self.block.on('transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd',function(){self.block.remove();});self.block.addClass('m_close');};var show=function(){self.block.addClass('m_open');};msgs[id]=this;this.block=$(template);this.block.find('.sj_msg_close').on('click',remove);element.append(this.block);setTimeout(show,0);if(!data.permanent){setTimeout(remove,DELAY);}}
function renderHelper(){this.render=function(data){if(!_.isObject(data)){data={text:data,permanent:false};}
new Msg(data);}}
FlashMsg.setRenderHelper(new renderHelper);}}}
function Run(){$('<div class="sj_msgs" data-flash-msg-block></div>').appendTo('body');}
angular.module('flash_msg').service('FlashMsg',FlashMsg).directive('flashMsgBlock',FlashMsgBlock).run(Run);})();