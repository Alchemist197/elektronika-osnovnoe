
(function(){'use strict';function DeferClick($q){return{scope:{deferClick:'&',deferClickBefore:'&'},link:function(scope,element){var lock=false;element.append('<div class="m_spinner_loader"> '+'<div class="bounce1"></div> '+'<div class="bounce2"></div> '+'<div class="bounce3"></div>'+'</div>');if(!scope.deferClickBefore){scope.deferClickBefore=function(){return true;}}
element.on('click',function(){var before;before=$q.when(scope.deferClickBefore());before.then(executeDeferClick);});function executeDeferClick(){var promise;if(lock){return;}
lock=true;element.addClass('m_spinner');promise=$q.when(scope.deferClick());promise['finally'](function(){element.removeClass('m_spinner');lock=false;});}}}}
angular.module('hr.common').directive('deferClick',DeferClick)})();