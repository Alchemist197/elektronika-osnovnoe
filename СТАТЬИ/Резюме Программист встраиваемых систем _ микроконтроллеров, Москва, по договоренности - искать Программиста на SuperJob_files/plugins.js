
if(!('split'in String.prototype)){String.prototype.split=function(sreg){var text=this,reg=(typeof sreg=='string')?RegExp(sreg,'g'):RegExp(sreg.source,'g'+((sreg.ignoreCase)?'i':'')+((sreg.multiline)?'m':''));if(!reg.source)return[text];var sindex=0,eindex,elms=[];do{reg.exec(text);eindex=reg.lastIndex;elms.push(text.substring(sindex,(eindex)?eindex:text.length).replace(sreg,''));sindex=eindex;}while(sindex);return elms;};}
Number.prototype.toMonetaryString=function(sep){var n=this;if(sep===undefined){sep=' ';}
var sRegExp=new RegExp('(-?[0-9]+)([0-9]{3})'),sValue=n+'';while(sRegExp.test(sValue)){sValue=sValue.replace(sRegExp,'$1'+sep+'$2');}
return sValue;};(function($,SJ){var blockImpl={}
function registerBlock(blockName,impl){blockImpl[blockName]=impl;}
function initBlock($block){var
blockObj,blockName=$block.data('blockName'),options=$block[0].onclick?$block[0].onclick():{};if(!blockImpl[blockName]){throw new Error("Undeclared block name "+blockName);}
blockObj=new blockImpl[blockName]($block,options);blockObj.blockName=blockName;$block.data(blockName+'Obj',blockObj);blockObj._init();return blockObj;}
function initRegion($region){var $blocks,readyBlocks=[];if($region.hasClass('js')){$blocks=$region.add($region.find('.js'));}else{$blocks=$region.find('.js');}
$blocks=$blocks.filter(function(a,b){if(b.getAttribute===undefined){return false;}
return true;});$blocks.sort(function(a,b){var
nname='data-block-nice',an=a.getAttribute(nname),bn=b.getAttribute(nname);return(an||0)-(bn||0);});$blocks.each(function(){var readyBlock=initBlock($(this));if(readyBlock){readyBlocks.push(readyBlock);}});return readyBlocks;}
function modalRequestOptions(options){var
actualOpts,defaults={type:'post',dataType:'json'};actualOpts=$.extend(defaults,options);actualOpts.beforeSend=function(jqXHR,settings){var loaderOptions={lines:13,length:7,width:4,radius:19,corners:0.4,rotate:14,direction:1,color:'#000',speed:1.2,trail:95,shadow:false,hwaccel:true,className:'spinner',zIndex:2e9,top:'auto',left:'auto'};var loaderModalOptions=$.extend({overlayCss:{backgroundColor:'#fff'},escClose:false,overlayClose:false},options);var defaultCallback=function(){$.modal.close();$.modal("<div class='h_loader' spin-preloader/>",loaderModalOptions);new Spinner(loaderOptions).spin(document.querySelector('.h_loader'));return true;};if(options.beforeSend){return options.beforeSend(jqXHR,settings,defaultCallback);}else{return defaultCallback();}};actualOpts.success=function(data,textStatus,jqXHR){var defaultCallback=function(){$.modal.close();return initRegion($(data.html));};var successCallback=function(){if(options.success){options.success(data,textStatus,jqXHR,defaultCallback);}else{defaultCallback();}};if(data.resources){SJ.loadResources(data.resources,successCallback);}else{successCallback();}};actualOpts.error=function(jqXHR,textStatus,errorThrown){var defaultCallback=function(){if(document.cookie.match(/(?: |^)dev_js_on=1;/))
console&&console.log(jqXHR,textStatus,errorThrown);else
window.location.reload();};if(options.error){options.error(jqXHR,textStatus,errorThrown,defaultCallback);}else{defaultCallback();}};return actualOpts;}
function loadModal(options){return $.ajax(modalRequestOptions(options));}
function callBlock($block,callback){if(!$block.hasClass('js'))return;var blockName=$block.data('blockName');function doCall(){var $obj=$block.data(blockName+'Obj');if($obj){callback($obj);}
else{setTimeout(doCall,42);}}
doCall();}
SJ['registerBlock']=registerBlock;SJ['initBlock']=initBlock;SJ['initRegion']=initRegion;SJ['modalRequestOptions']=modalRequestOptions;SJ['loadModal']=loadModal;SJ['callBlock']=callBlock;})(jQuery,SJ);(function(){var request=null,errorMessage=function(message){if(typeof(console.log)==='function'){console.log(message);}};if(!$.sj){$.sj={};}
$.sj.request=function(url,data,method){var $container=this;if(method!=='post'){method='get';}
if(request){request.abort();}
request=$.ajax(url,{data:data,type:method,dataType:'json',success:function(data){if(data==null){return;}
if(data.callback){for(i in data.callback){try{eval(data.callback[i]);}
catch(e){errorMessage('callback error: '+e.message+' at line '+e.lineNumber);}}}
if(data.blocks){for(i in data.blocks){$('#'+i).html(data.blocks[i]);}}
if(data.html&&typeof($container.html)==='function'){$container.html(data.html);}
request=null;},error:function(XHR,textStatus,errorThrown){if(XHR.status){errorMessage(method+' '+url+' '+textStatus+': '+errorThrown);}
request=null;}});};})();function hint1(id){return(window['hints']&&window['hints'][id]&&window['hints'][id].title)?window['hints'][id].title:id;}
function hint(id,show_title){return window['hints']&&window['hints'][id]?(show_title&&window['hints'][id].title?window['hints'][id].title:(window['hints'][id].body?window['hints'][id].body:id)):id;}
function hint_any(id){return window['hints']&&window['hints'][id]?(window['hints'][id].title||window['hints'][id].body||id):id;}
function getLanguage(){var known={'rus':1,'eng':1};var lang=document.cookie.match(/\blang=(\w+);/);if(lang&&lang[1]&&(lang[1]in known)){return lang[1];}else{return'rus';}}
function getPluralForm(number,forms){var lang=getLanguage();var formNo;if(!lang in forms){lang='rus';}
switch(lang){case'eng':if(number==1){formNo=0;}else{formNo=1;}
case'rus':default:if(number%100>4&&number%100<21){formNo=2;}else if(number%10==1){formNo=0;}else if(number%10>1&&number%10<5){formNo=1;}else{formNo=2;}}
return forms[lang][formNo];}
function getAge(year,month,day){if(year==0||month==0||day==0){return undefined;}
var currentDate=globalDate;var thisYearBirthday=new Date(currentDate.getFullYear(),month-1,day);var yearDifference=currentDate.getFullYear()-year;return thisYearBirthday.getTime()>currentDate.getTime()?yearDifference-1:yearDifference;}
(function($){var fix_checked=false;if($.browser.msie){if(typeof(document.documentMode)=='undefined'){fix_checked=true;}else{if(document.documentMode<9){fix_checked=true;}}}
var filterClass;var checkInput=function(){var label=$(this).siblings('label').eq(0);label.find('span.'+filterClass+'-label-unchecked').hide();label.find('span.'+filterClass+'-label-checked').show();this.checked=true;};var uncheckInput=function(){var label=$(this).siblings('label').eq(0);label.find('span.'+filterClass+'-label-checked').hide();label.find('span.'+filterClass+'-label-unchecked').show();this.checked=false;};var invalidateInput=function(){if(this.checked){$(this).each(checkInput);}else{$(this).each(uncheckInput);}};$.fn.updateRadioInputLabels=function(){if(fix_checked){filterClass='radio';this.find('span.radio-button').find('input:radio').each(invalidateInput);}
return this;};$.fn.fixRadioInputLabels=function(){if(fix_checked){this.updateRadioInputLabels();this.on('click','span.radio-button label',function(){var form=$(this).closest('form');var input=$('#'+$(this).attr('for'));if(input.is(':radio')&&!input.attr('disabled')){filterClass='radio';form.find('input:radio[name='+input.attr('name').replace(/\[/g,'\\[').replace(/\]/g,'\\]')+']').each(uncheckInput);input.each(checkInput);}
return true;});}
return this;};$.fn.updateCheckboxInputLabels=function(){if(fix_checked){filterClass='checkbox';this.find('span.checkbox-button').find('input:checkbox').each(invalidateInput);}
return this;};$.fn.fixCheckboxInputLabels=function(){if(fix_checked){this.updateCheckboxInputLabels();this.on('click','span.checkbox-button label',function(){var form=$(this).closest('form');var input=$('#'+$(this).attr('for'));if(input.is(':checkbox')&&!input.attr('disabled')){filterClass='checkbox';if(input.get(0).checked){input.each(uncheckInput);}else{input.each(checkInput);}}
return true;});}
return this;};$.fn.fixMaxLength=function(options){options=$.extend({},options);return this.on('keydown paste keyup',function(){var $obj=$(this);setTimeout(function(){var maxLength=options['maxLength']||$obj.attr('maxLength')||$obj.attr('maxlength');if($obj.val().length>maxLength){$obj.val($obj.val().substr(0,maxLength));}},50);});}
$.fn.attachInputFilter=function(options){options=$.extend({timeout:50,event_color_font:'#b33',default_color_font:'',replace_rgxp:/[^\d]/g,replacement:''},options);if(options.test&&!(options.test instanceof Array)){options.test=[options.test];}
return this.find(options.selector).on('keydown paste',function(e){var element=e.target||e.srcElement;element.style.color=options.event_color_font;setTimeout(function(){var len=element.value.length;element.style.color=options.default_color_font;for(var t=0;t<options.test.length;t++){if(eval(options.test[t])){if(options.replace_rgxp){element.value=element.value.replace(options.replace_rgxp,options.replacement);}
len=element.value.length;if(options['message'+t]){var m=options['message'+t],$selector;if(m.selector){$selector=$(m.selector);}else
if(m.$selector_eval){eval('$selector = '+m.$selector_eval+';');}
if($selector.length){$selector.html(m.text);}}}else{if(options['message'+t]){var m=options['message'+t],$selector;if(m.selector){$selector=$(m.selector);}else
if(m.$selector_eval){eval('$selector = '+m.$selector_eval+';');}
if($selector.length){$selector.html('');}}}}
if(options.maxlength&&len>options.maxlength){element.value=element.value.substr(0,options.maxlength);}},options.timeout);});};})(jQuery);$(document).ready(function(){var unselectRadio=function($radio){var $form=$radio.parents('form');var $group=$form.find("input:radio[name='"+$radio.attr("name")+"']");if($radio.data('isChecked')){$group.removeAttr("checked");$radio.data('isChecked',false);$radio.trigger('change');}else{$group.data('isChecked',false);$radio.data('isChecked',true);$radio.trigger('change');}
$form.updateRadioInputLabels();};$("input.free[type=radio]").live('click',function(){unselectRadio($(this));}).each(function(){$(this).data('isChecked',$(this).attr('checked')!=undefined);});if($.browser.msie&&$.browser.version<9){$('label').live('click',function(){var $input=$('#'+$(this).attr('for'));if($input.attr('type')=='radio'&&$input.hasClass('free')){unselectRadio($input);}});}});$(document).ready(function(){$('body').fixRadioInputLabels().fixCheckboxInputLabels();});