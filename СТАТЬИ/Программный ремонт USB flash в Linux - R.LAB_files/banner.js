$(document).ready(function () {
  debug = 0;
  var curOpac = 1;
  if(window.screen.width > 800) {
				checkwsize();
				$("#fixed").css("opacity","1.0");
  // ставим обработчик
      $(window).resize(function(){
	  checkwsize();
      });
  } // орубили для мобил, шириной меньше 800
    else {
      $("#fixed").remove();
    }
  //----
  checkwsize();
      
function checkwsize()
  {
      var wid = $(window).width();
      var curOffset = parseInt($("#fixed").css("left"));
      var widmin = 1240; //0
      var widmax = widmin+28; //1
      var widminus = parseInt(wid)-1; //2
      var setOffset = 370; // смещение относительно центра страницы
			    // 370 = справа от контента, -650 = слева
      var rightPadding = 260;
      
	if(wid>=widmin && wid<widmax) //0 & 1
	{
	  //curOpac = Math.abs((1239-wid))*0.02; 
	  curOpac = Math.abs((widminus-wid))*0.02; //2
	  curOpac = parseFloat(curOpac.toFixed(1)); 
	}
	if(wid<widmin) //0
	{
	  curOpac = 0.0;  
	}
	if(wid>=widmax) //1
	{
	  curOpac = 1.0;
	}
	//---------------
	// смещение ++		
	//curOffset = parseInt(((wid/2)+setOffset)+2);
	curOffset = parseInt(wid-rightPadding);
	// смещение --
	if(debug==1) 
	    { 
	      $("#du").text(wid
		    +"; op:"+curOpac
		    +"; off:"+curOffset
			   ); 
	    }
	    curOffset = curOffset+"px";
	$("#fixed").css("left", curOffset);
	$("#fixed").css("opacity", curOpac);
	return(curOpac);
  }

  function checkwsizeold()
  {
      var wid = $(window).width();
      
	if(wid<1350 && wid>1299)
	{
	curOpac = Math.abs((1300-wid))*0.02;
	curOpac = parseFloat(curOpac.toFixed(1)); 
	}
	if(wid<1300)
	{
	  curOpac = 0.0;
	}
	if(wid>1350)
	{
	  curOpac = 1.0;
	}
	if(debug==1) 
	    { 
	      $("#du").text(wid+"; op:"+curOpac); 
	    }
	  $("#fixed").css("opacity", curOpac);
	return(curOpac);
  }

      var offset = $('#fixed').offset();
    var topPadding = 0;
    $(window).scroll(function() {
        if ($(window).scrollTop() > offset.top) {
            $('#fixed').stop().animate({marginTop: $(window).scrollTop() - offset.top + topPadding});
        }
        else {
            $('#fixed').stop().animate({marginTop: 0});
        }
    });
});



