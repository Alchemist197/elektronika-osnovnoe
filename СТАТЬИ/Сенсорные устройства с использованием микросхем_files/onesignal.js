function in_array(e, i, t) {
    var n, a = !1,
        t = !!t;
    for (n in i)
        if (t && i[n] === e || !t && i[n] == e) {
            a = !0;
            break
        }
    return a
}

window.QRZPush || (window.QRZPush = []);
var tempQRZPush = null;
"undefined" != typeof QRZPush && (tempQRZPush = QRZPush);

var QRZPush = {
    sdkVersion: '1.2',
    hostUrl: "https://www.qrz.ru/api/message",
    logging: true,
    initParams: null,
    QRZPushDb: null,
    messageListener: null,
    pushMode: null,
    pushKey: null,
    authToken: null,
    webSiteDomain: "https://www.qrz.ru",
    deviceToken: null,
    safariFirstPrompt: null,
    showWidget: false,
    alreadySubscribed: false,
    log: function (e) {
        1 == QRZPush.logging && console.log(e)
    },
    processPushes: function(e) {
        for (var i = 0; i < e.length; i++) QRZPush.push(e[i])
    },
    push: function(e) {
        if ("function" == typeof e) e();
        else {
            var i = e.shift();
            QRZPush[i].apply(null, e)
        }
    },
    init: function(e) {
        QRZPush.checkBrowser() && window.addEventListener("load", function() {
            if (QRZPush.pushMode == 'chrome') {
                if (location.protocol === 'http:' && location.pathname!='/api/message/subscribe') {
                    QRZPush.showWidget = true;
                    QRZPush.getSubscription(function (subscriptionId) {
                        QRZPush.showWidget = subscriptionId ? false : true;
                        QRZPush.alreadySubscribed = subscriptionId ? true : false;
                    });
                    if (QRZPush.showWidget) return void(QRZPush.subscribeWithWidget(!1, !1, !0));
                }
                if (location.protocol === 'https:' || location.hostname == 'localhost') {
                    manifest = document.createElement("link"), manifest.rel = "manifest", manifest.href = QRZPush.webSiteDomain + "/js/manifest.json",
                        document.head.appendChild(manifest), QRZPush.initWorker()
                }
            }
        })
    },
    createOnResultEvent: function(token) {
        QRZPush.deviceToken = token;
        var i = new Event("QrzPushResult");
        document.dispatchEvent(i)
    },
    registerUserForPush: function(e) {
        if (QRZPush.pushMode == 'safari') {
            QRZPush.checkRemotePermission(window.safari.pushNotification.permission("web.web.qrzPush.7284d4679ae256bfd3acc6ea6800d7f8"), function(e) {
                e && QRZPush.safariFirstPrompt && (QRZPush.createOnResultEvent(e), QRZPush.safariFirstPrompt = null)
            })
        } else {
            QRZPush.getSubscription(function(i) {
                i ? QRZPush.isSubscriptionChanged(i) : QRZPush.subscribe(function(i) {
                    QRZPush.putValueToDb("Ids", {
                        type: "SubscriptionId",
                        value: i
                    }), QRZPush.registerNewUser(i, function(t) {
                        return t ? (QRZPush.createOnResultEvent(i), e(t)) : void 0
                    })
                })
            })
        }
    },
    registerNewUser: function(subscriptionId, i) {
        var data = {
                action: "register",
                mode: QRZPush.pushMode,
                device_id: QRZPush.md5(subscriptionId),
                os_v: QRZPush.detectBrowser().version,
                lib_v: QRZPush.sdkVersion,
                screen_w: QRZPush.getScreenWidth(),
                screen_h: QRZPush.getScreenHeight(),
                tz: QRZPush.getTimezone(),
                lang: QRZPush.getBrowserlanguage(),
                token: subscriptionId,
                key: QRZPush.base64(QRZPush.pushKey),
                auth: QRZPush.base64(QRZPush.authToken),
                time: QRZPush.getCurrentTimestamp()
        },
        requestUrl = QRZPush.getGetUrlWithObject(QRZPush.hostUrl, data, !0);
        QRZPush.send(requestUrl, "get", !1, function(e) {
            if (e && e.QrzPushwelcome) {
                var t = e.QrzPushwelcome;
                QRZPush.showWelcomeNotification(t.title, t.body, t.icon, t.redirect_url, t.device_id)
            }
            return i(e)
        })
    },
    addTag: function(tags) {
        QRZPush.getDbValue("Ids", "SubscriptionId", function(i) {
            if (i.target.result) {
                var t = {
                        action: "add_tag",
                        device_id: QRZPush.md5(i.target.result.value),
                        value: tags,
                        time: QRZPush.getCurrentTimestamp()
                    },
                    n = QRZPush.getGetUrlWithObject(QRZPush.hostUrl, t, !0);
                QRZPush.send(n, "get", !1, function() {})
            }
        })
    },
    setTags: function(tags) {
        QRZPush.getDbValue("Ids", "SubscriptionId", function(i) {
            if (i.target.result) {
                var t = {
                        action: "set_tags",
                        device_id: QRZPush.md5(i.target.result.value),
                        value: tags,
                        time: QRZPush.getCurrentTimestamp()
                    },
                    n = QRZPush.getGetUrlWithObject(QRZPush.hostUrl, t, !0);
                QRZPush.send(n, "get", !1, function() {})
            }
        })
    },
    removeTag: function(tags) {
        QRZPush.getDbValue("Ids", "SubscriptionId", function(i) {
            if (i.target.result) {
                var t = {
                        action: "remove_tag",
                        device_id: QRZPush.md5(i.target.result.value),
                        value: tags,
                        time: QRZPush.getCurrentTimestamp()
                    },
                    n = QRZPush.getGetUrlWithObject(QRZPush.hostUrl, t, !0);
                QRZPush.send(n, "get", !1, function() {})
            }
        })
    },
    removeAllTags: function() {
        QRZPush.getDbValue("Ids", "SubscriptionId", function(e) {
            if (e.target.result) {
                var i = {
                        type: "remove_all_tags",
                        device_id: QRZPush.md5(i.target.result.value),
                        time: QRZPush.getCurrentTimestamp()
                    },
                    t = QRZPush.getGetUrlWithObject(QRZPush.hostUrl, i, !0);
                QRZPush.send(t, "get", !1, function() {})
            }
        })
    },
    setAlias: function(e) {
        QRZPush.getDbValue("Ids", "SubscriptionId", function(i) {
            if (i.target.result) {
                var t = {
                        action: "alias",
                        device_id: QRZPush.md5(i.target.result.value),
                        value: e,
                        time: QRZPush.getCurrentTimestamp()
                    },
                    requestUrl = QRZPush.getGetUrlWithObject(QRZPush.hostUrl, t, !0);
                QRZPush.send(requestUrl, "get", !1, function() {})
            }
        })
    },
    afterSubscription: function(event) {
        document.addEventListener("QrzPushResult", function(i) {
            return event(QRZPush.deviceToken)
        }, !1)
    },
    initWorker: function() {
        return "serviceWorker" in navigator && navigator.serviceWorker.register("https://www.qrz.ru/onesignal-worker.js").then(function(reg) {
            QRZPush.log("Worker registered", reg);
        })["catch"](function(error) {
            return QRZPush.log('initWorker '+error), !1
        }), !0
    },
    send: function(requestUrl, method, data, callback) {
        if ("chrome" == QRZPush.pushMode) {
            var request = {
                method: method
            };
            data && (request.body = JSON.stringify(data)), fetch(requestUrl, request).then(function(response) {
                return 200 !== response.status ? void QRZPush.log("Looks like there was a problem. Status Code: " + response.status) : void response.json().then(function(data) {
                    return QRZPush.log("data successfully sent", data), "function" == typeof callback ? callback(data) : void 0
                })
            })["catch"](function(exception) {
                QRZPush.log("Fetch Error :-S", exception)
            })
        } else {
            var o = new XMLHttpRequest;
            o.open(method, requestUrl, !0), o.onreadystatechange = function() {
                if (4 == o.readyState) {
                    if (200 !== o.status) return void QRZPush.log("Looks like there was a problem. Status Code: " + o.status);
                    if (QRZPush.log("data successfully sent"), "function" == typeof callback) return callback(o.responseText)
                }
            }, o.send(null)
        }
    },
    initDb: function(e) {
        if (QRZPush.QRZPushDb) return void e();
        var i = indexedDB.open("QrzPush_sdk_db", 1);
        i.onsuccess = function(i) {
            QRZPush.QRZPushDb = i.target.result, e()
        }, i.onupgradeneeded = function(e) {
            var i = e.target.result;
            i.createObjectStore("Ids", {
                keyPath: "type"
            }), i.createObjectStore("NotificationOpened", {
                keyPath: "key"
            }), i.createObjectStore("Options", {
                keyPath: "key"
            })
        }
    },
    getDbValue: function(dbname, key, val) {
        QRZPush.initDb(function() {
            QRZPush.QRZPushDb.transaction(dbname).objectStore(dbname).get(key).onsuccess = val
        })
    },
    getAllValues: function(e, i) {
        QRZPush.initDb(function() {
            var t = {};
            QRZPush.QRZPushDb.transaction(e).objectStore(e).openCursor().onsuccess = function(e) {
                var n = e.target.result;
                n ? (t[n.key] = n.value.value, n["continue"]()) : i(t)
            }
        })
    },
    putValueToDb: function(e, i) {
        QRZPush.initDb(function() {
            QRZPush.QRZPushDb.transaction([e], "readwrite").objectStore(e).put(i)
        })
    },
    deleteDbValue: function(e, i) {
        QRZPush.initDb(function() {
            QRZPush.QRZPushDb.transaction([e], "readwrite").objectStore(e)["delete"](i)
        })
    },
    isSubscriptionChanged: function(e) {
        QRZPush.getDbValue("Ids", "SubscriptionId", function(i) {
            if (i.target.result) {
                if (i.target.result.value != e) {
                    var t = {
                        action: "token",
                        device_id: QRZPush.md5(i.target.result.value),
                        value: e,
                        time: QRZPush.getCurrentTimestamp()
                    };
                    return url = QRZPush.getGetUrlWithObject(QRZPush.hostUrl, t, !0), void QRZPush.send(url, "get", !1, function(i) {
                        i && QRZPush.createOnResultEvent(e)
                    })
                }
            } else i.target.result || "safari" != QRZPush.pushMode || (QRZPush.putValueToDb("Ids", {
                type: "SubscriptionId",
                value: e
            }), QRZPush.registerNewUser(e, function(i) {
                i && QRZPush.createOnResultEvent(e)
            }))
        })
    },
    subscribeWithWidget: function(e, i, t) {
        if (QRZPush.pushMode == "chrome") {
            QRZPush.getDbValue("Ids", "SubscriptionId", function(n) {
                n.target.result || QRZPush.appendWidgets(e, i, t)
            });
        } else if (QRZPush.pushMode == "safari" &&
            window.safari.pushNotification.permission("web.web.qrzPush.7284d4679ae256bfd3acc6ea6800d7f8").permission == "default")
                QRZPush.appendWidgets(e, i, t);
    },
    appendWidgets: function(e, i, t) {
        if (t && QRZPush.createNativePrompt(), e && QRZPush.createButton(), i && QRZPush.createLine(), null === document.getElementById("qrzpush-button-style")) {
            var n = document.getElementsByTagName("head")[0],
                a = document.createElement("link");
            a.id = "qrzpush-button-style", a.rel = "stylesheet", a.type = "text/css", a.href = window.location.protocol + "//www.qrz.ru/css/pushbutton.css", a.media = "all", n.appendChild(a)
        }
    },
    createNativePrompt: function() {
        if (!QRZPush.checkCookie("dontShowPushPrompt")) {
            var e = " wants to:",
                i = "В В Send notifications",
                t = "Allow",
                n = "Block",
                a = document.createElement("div");
            a.id = "qrzpush-prompt-widget", ("ru" == QRZPush.getBrowserlanguage() || "uk" == QRZPush.getBrowserlanguage()) && (e = " запрашивает разрешение на:", i = " отправку оповещений", t = "Разрешить", n = "Блокировать", a.className = "qrzpush-ru-block");
            var o = document.createElement("div");
            o.id = "qrzpush-prompt-closer";
            var r = document.createElement("span");
            r.id = "qrzpush-prompt-closer-char";
            var p = String.fromCharCode(10761),
                s = document.createTextNode(p);
            r.appendChild(s), o.appendChild(r);
            var c = document.createElement("div");
            c.id = "qrzpush-prompt-domain-name";
            var s = document.createTextNode(document.domain + e);
            c.appendChild(s);
            var u = "f0 9f 94 94",
                d = decodeURIComponent(u.replace(/\s+/g, "").replace(/[0-9a-f]{2}/g, "%$&"));
            d += "  ";
            var l = document.createElement("div");
            l.id = "qrzpush-prompt-bell-text";
            var m = document.createTextNode(d + i);
            l.appendChild(m);
            var f = document.createElement("div");
            f.id = "qrzpush-prompt-buttons", ("ru" == QRZPush.getBrowserlanguage() || "uk" == QRZPush.getBrowserlanguage()) && (f.className = "qrzpush-prompt-buttons-ru", o.className = "qrzpush-prompt-closer-ru");
            var g = document.createElement("div"); g.id = "qrzpush-prompt-block-button", g.className = "qrzpush-prompt-button";
            var J = document.createTextNode(n);
            g.appendChild(J);
            var h = document.createElement("div"); h.id = "qrzpush-prompt-allow-button", h.className = "qrzpush-prompt-button";
            var v = document.createTextNode(t);
            h.appendChild(v), f.appendChild(g), f.appendChild(h), a.appendChild(o), a.appendChild(c), a.appendChild(l), a.appendChild(f),
                document.body.appendChild(a), document.getElementById("qrzpush-prompt-closer-char").addEventListener("click", function(e) {
                e.preventDefault(), QRZPush.removeWidget(["qrzpush-prompt-widget"])
            }), document.getElementById("qrzpush-prompt-block-button").addEventListener("click", function(e) {
                e.preventDefault(), QRZPush.setCookie("dontShowPushPrompt", !0, 30), QRZPush.removeWidget(["qrzpush-prompt-widget"])
            }), document.getElementById("qrzpush-prompt-allow-button").addEventListener("click", function(e) {
                e.preventDefault(), QRZPush.removeWidget(["qrzpush-prompt-widget"]), QRZPush.registerOnWidgetClick()
            })
        }
    },
    getGetUrlWithObject: function(e, i) {
        return e + "?data=" + JSON.stringify(i)
    },
    checkRemotePermission: function(e, i) {
        if ("default" === e.permission) QRZPush.safariFirstPrompt = !0, window.safari.pushNotification.requestPermission("https://www.qrz.ru/safari/7284d4679ae256bfd3acc6ea6800d7f8", "web.web.qrzPush.7284d4679ae256bfd3acc6ea6800d7f8", {}, function(e) {
            QRZPush.checkRemotePermission(e, i)
        });
        else {
            if ("denied" === e.permission) return i();
            if ("granted" === e.permission) return QRZPush.isSubscriptionChanged(e.deviceToken), i(e.deviceToken)
        }
    },
    registerHttp: function() {
        if ("safari" == QRZPush.pushMode) QRZPush.checkRemotePermission(window.safari.pushNotification.permission("web.web.qrzPush.7284d4679ae256bfd3acc6ea6800d7f8"), function(e) {
            e && QRZPush.safariFirstPrompt && (QRZPush.createOnResultEvent(e), QRZPush.safariFirstPrompt = null)
        });
        else {
            if (!QRZPush.showWidget || !QRZPush.isPushManagerSupported()) return;
            var e = void 0 != window.screenLeft ? window.screenLeft : screen.left,
                i = void 0 != window.screenTop ? window.screenTop : screen.top,
                t = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width,
                n = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height,
                a = 600,
                o = 350,
                r = t / 2 - a / 2 + e,
                p = n / 2 - o / 2 + i,
                s = window.open("https://www.qrz.ru/api/message/subscribe", "_blank", "scrollbars=yes, width=" + a + ", height=" + o + ", top=" + p + ", left=" + r);
            s && s.focus(), QRZPush.createMessageListener()
        }
    },
    isSubscribed: function() {
        if ("chrome" != QRZPush.pushMode) {
            var i = window.safari.pushNotification.permission("web.web.qrzPush.7284d4679ae256bfd3acc6ea6800d7f8").permission;
            return "granted" == i ? !0 : !1
        }
        result = false;
        QRZPush.getSubscription(function(i) {
            result = i ? true : false
            return result;
        })
        return result;
    },
    getSubscription: function(e) {
        if (QRZPush.pushMode == 'chrome') {
            if (QRZPush.showWidget) {
                QRZPush.getDbValue("Ids", "SubscriptionId", function(i) {
                    return i.target.result ? i.target.result.value : false;
                });
            } else {
                navigator.serviceWorker.ready.then(function(i) {
                    i.pushManager.getSubscription().then(function(i) {
                        console.log(i);
                        var browser = QRZPush.detectBrowser().name.toLowerCase();
                        if ((browser == 'firefox' || browser == 'chrome') && i!=null) {
                            QRZPush.pushKey = i.getKey('p256dh');
                            QRZPush.authToken = i.getKey('auth');
                            console.log('PublicKey ', QRZPush.base64(QRZPush.pushKey),'AuthToken', QRZPush.base64(QRZPush.authToken));
                        }
                        return i ? (subscriptionId = "subscriptionId" in i ? i.subscriptionId : i.endpoint, subscriptionId = QRZPush.endpointWorkaround(subscriptionId), e(subscriptionId)) : e()
                    })["catch"](function(e) {
                        QRZPush.log(e) //"Error during getSubscription()" +
                    })
                });
                return false;
            }
        } else {
            var i = window.safari.pushNotification.permission("web.web.qrzPush.7284d4679ae256bfd3acc6ea6800d7f8");
            QRZPush.checkRemotePermission(i, function(e) {
                e && QRZPush.safariFirstPrompt && (QRZPush.createOnResultEvent(e), QRZPush.safariFirstPrompt = null)
            })
        }
    },
    registerOnWidgetClick: function() {
        if ("safari" == QRZPush.pushMode) {
            QRZPush.removeWidget(["qrzpush-line-widget", "qrzpush-container", "qrzpush-prompt-widget"]);
            var e = window.safari.pushNotification.permission("web.web.qrzPush.7284d4679ae256bfd3acc6ea6800d7f8");
            QRZPush.checkRemotePermission(e, function(e) {
                e && QRZPush.safariFirstPrompt && (QRZPush.createOnResultEvent(e), QRZPush.safariFirstPrompt = null)
            })
        } else QRZPush.showWidget ? QRZPush.registerHttp() : QRZPush.registerUserForPush(function(e) {
            e && QRZPush.removeWidget(["qrzpush-line-widget", "qrzpush-container", "qrzpush-prompt-widget"])
        })
    },
    createMessageListener: function() {
        function messageReceiver(event) {
            event.data.httpRegistered && (QRZPush.putValueToDb("Ids", {
                type: "SubscriptionId",
                value: event.data.subscriptionId
            }), QRZPush.createOnResultEvent(event.data.subscriptionId), QRZPush.removeWidget(["qrzpush-line-widget", "qrzpush-container", "qrzpush-prompt-widget"]))
        }
        QRZPush.messageListener || (window.addEventListener("message", messageReceiver, !1), QRZPush.messageListener = !0)
    },
    removeWidget: function(e) {
        for (var i in e) {
            var t = e[i],
                n = document.getElementById(t);
            null !== n && n.parentNode.removeChild(n)
        }
    },
    subscribe: function(e) {
        return navigator.serviceWorker.ready.then(function(reg) {
            QRZPush.log(':^)', reg);
            reg.pushManager.subscribe({
                userVisibleOnly: true
            }).then(function(subscription) {
                console.log(subscription);
                var browser = QRZPush.detectBrowser().name.toLowerCase();
                if ((browser == 'firefox' || browser == 'chrome') && subscription!=null) {
                    QRZPush.pushKey = subscription.getKey('p256dh');
                    QRZPush.authToken = subscription.getKey('auth');
                    console.log('PublicKey ', QRZPush.base64(QRZPush.pushKey),'AuthToken', QRZPush.base64(QRZPush.authToken));
                }
                return subscriptionId = "subscriptionId" in subscription ? subscription.subscriptionId : subscription.endpoint,
                    null != subscriptionId ? (subscriptionId = QRZPush.endpointWorkaround(subscriptionId), e(subscriptionId)) : void 0
            })["catch"](function(e) {
                "denied" === Notification.permission ? (QRZPush.log("Permission for Notifications was denied"),
                    QRZPush.removeWidget(["qrzpush-line-widget", "qrzpush-container", "qrzpush-prompt-widget"])) : (QRZPush.log("Unable to subscribe to push.", e), console.log(e))
            })
        }, QRZPush.log('subscribe failed')), !0
    },
    unsubscribe: function(e) {
        return navigator.serviceWorker.ready.then(function(reg) {
            reg.pushManager.unsubscribe().then(function(i) {
                QRZPush.deleteDbValue("Ids","SubscriptionId");
                return true;
            })["catch"](function(e) {
                "denied" === Notification.permission ? (QRZPush.log("Permission for Notifications was denied")) : QRZPush.log("Unable to unsubscribe from push.", e)
            })
        }, QRZPush.log('unsubscribe failed')), !0
    },
    showWelcomeNotification: function(e, i, t, n, a) {
        if ("chrome" == QRZPush.pushMode) {
            var o = {
                    body: i,
                    icon: t,
                    data: {
                        redirect_url: n,
                        device_id: a
                    }
                },
                r = new Notification(e, o);
            r.onclick = function(e) {
                if (e.target && e.target.data && e.target.data.redirect_url && e.target.data.device_id) {
                    var i = {
                            action: "welcome_opened",
                            device_id: e.target.data.device_id,
                            time: Math.floor(Date.now() / 1e3)
                        },
                        t = QRZPush.getGetUrlWithObject(QRZPush.hostUrl, i, !0);
                    QRZPush.send(t, "get", !1, function() {}), window.location = e.target.data.redirect_url
                } else QRZPush.getSubscription(function(e) {
                    if (e) {
                        var i = {
                                action: "welcome_opened",
                                device_id: QRZPush.md5(e),
                                time: Math.floor(Date.now() / 1e3)
                            },
                            t = QRZPush.getGetUrlWithObject(QRZPush.hostUrl, i, !0);
                        QRZPush.send(t, "get", !1, function() {})
                    }
                }), window.location = QRZPush.webSiteDomain
            }
        }
    },
    setCookie: function(e, i, t) {
        var n = new Date;
        n.setTime(n.getTime() + 24 * t * 60 * 60 * 1e3);
        var a = "; expires=" + n.toUTCString();
        document.cookie = encodeURIComponent(e) + "=" + encodeURIComponent(i) + a + "; path=/"
    },
    checkCookie: function(e) {
        for (var i = e + "=", t = document.cookie.split(";"), n = 0; n < t.length; n++) {
            for (var a = t[n];
                 " " == a.charAt(0);) a = a.substring(1);
            if (0 == a.indexOf(i)) return a.substring(i.length, a.length)
        }
        return ""
    },
    checkBrowser: function() {
        if (QRZPush.checkIfSafariNotification()) QRZPush.pushMode = "safari";
        else {
            if (!QRZPush.isPushManagerSupported()) return QRZPush.log("Push messaging isn't supported."), !1;
            if (!QRZPush.isNotificationsSupported()) return QRZPush.log("Notifications aren't supported."), !1;
            if (!QRZPush.isNotificationPermitted()) return QRZPush.log("The user has blocked notifications."), !1;
            QRZPush.pushMode = "chrome"
        }
        return "opera" == QRZPush.detectBrowser().name.toLowerCase() ? !1 : !0
    },
    getScreenWidth: function() {
        return window.screen ? screen.width : 0
    },
    getScreenHeight: function() {
        return window.screen ? screen.height : 0
    },
    isNotificationsSupported: function() {
        return "showNotification" in ServiceWorkerRegistration.prototype
    },
    isNotificationPermitted: function() {
        return "denied" != Notification.permission
    },
    isPushManagerSupported: function() {
        return "PushManager" in window
    },
    getCurrentTimestamp: function() {
        return Math.floor(Date.now() / 1e3)
    },
    getBrowserlanguage: function() {
        return navigator.language.substring(0, 2)
    },
    checkIfSafariNotification: function() {
        return false;//"safari" in window && "pushNotification" in window.safari
    },
    getTimezone: function() {
        return -60 * (new Date).getTimezoneOffset()
    },
    detectBrowser: function() {
        var e, i = navigator.userAgent,
            t = i.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        return /trident/i.test(t[1]) ? (e = /\brv[ :]+(\d+)/g.exec(i) || [], {
            name: "IE",
            version: e[1] || ""
        }) : "Chrome" === t[1] && (e = i.match(/\bOPR\/(\d+)/), null != e) ? {
            name: "Opera",
            version: e[1]
        } : (t = t[2] ? [t[1], t[2]] : [navigator.appName, navigator.appVersion, "-?"], null != (e = i.match(/version\/(\d+)/i)) && t.splice(1, 1, e[1]), {
            name: t[0],
            version: t[1]
        })
    },
    endpointWorkaround: function(e) {
        if (~e.indexOf("https://android.googleapis.com/gcm/send")) {
            var i = e.split("https://android.googleapis.com/gcm/send/");
            return i[1]
        }
        return e
    },
    md5: function(e) {
        var i, t, n, a, o, r, p, s, c, u = function(e) {
                e = e.replace(/\r\n/g, "\n");
                for (var i = "", t = 0; t < e.length; t++) {
                    var n = e.charCodeAt(t);
                    128 > n ? i += String.fromCharCode(n) : n > 127 && 2048 > n ? (i += String.fromCharCode(n >> 6 | 192), i += String.fromCharCode(63 & n | 128)) : (i += String.fromCharCode(n >> 12 | 224), i += String.fromCharCode(n >> 6 & 63 | 128), i += String.fromCharCode(63 & n | 128))
                }
                return i
            },
            d = function(e, i) {
                return e << i | e >>> 32 - i
            },
            l = function(e, i) {
                var t, n, a, o, r;
                return a = 2147483648 & e, o = 2147483648 & i, t = 1073741824 & e, n = 1073741824 & i, r = (1073741823 & e) + (1073741823 & i), t & n ? 2147483648 ^ r ^ a ^ o : t | n ? 1073741824 & r ? 3221225472 ^ r ^ a ^ o : 1073741824 ^ r ^ a ^ o : r ^ a ^ o
            },
            m = function(e, i, t) {
                return e & i | ~e & t
            },
            f = function(e, i, t) {
                return e & t | i & ~t
            },
            g = function(e, i, t) {
                return e ^ i ^ t
            },
            J = function(e, i, t) {
                return i ^ (e | ~t)
            },
            h = function(e, i, t, n, a, o, r) {
                return e = l(e, l(l(m(i, t, n), a), r)), l(d(e, o), i)
            },
            v = function(e, i, t, n, a, o, r) {
                return e = l(e, l(l(f(i, t, n), a), r)), l(d(e, o), i)
            },
            b = function(e, i, t, n, a, o, r) {
                return e = l(e, l(l(g(i, t, n), a), r)), l(d(e, o), i)
            },
            w = function(e, i, t, n, a, o, r) {
                return e = l(e, l(l(J(i, t, n), a), r)), l(d(e, o), i)
            },
            j = function(e) {
                for (var i, t = e.length, n = t + 8, a = (n - n % 64) / 64, o = 16 * (a + 1), r = Array(o - 1), p = 0, s = 0; t > s;) i = (s - s % 4) / 4, p = s % 4 * 8, r[i] = r[i] | e.charCodeAt(s) << p, s++;
                return i = (s - s % 4) / 4, p = s % 4 * 8, r[i] = r[i] | 128 << p, r[o - 2] = t << 3, r[o - 1] = t >>> 29, r
            },
            y = function(e) {
                var i, t, n = "",
                    a = "";
                for (t = 0; 3 >= t; t++) i = e >>> 8 * t & 255, a = "0" + i.toString(16), n += a.substr(a.length - 2, 2);
                return n
            },
            k = Array(),
            P = 7,
            S = 12,
            C = 17,
            I = 22,
            E = 5,
            W = 9,
            N = 14,
            _ = 20,
            D = 4,
            T = 11,
            L = 16,
            O = 23,
            U = 6,
            M = 10,
            B = 15,
            R = 21;
        for (e = u(e), k = j(e), r = 1732584193, p = 4023233417, s = 2562383102, c = 271733878, i = 0; i < k.length; i += 16) t = r, n = p, a = s, o = c, r = h(r, p, s, c, k[i + 0], P, 3614090360), c = h(c, r, p, s, k[i + 1], S, 3905402710), s = h(s, c, r, p, k[i + 2], C, 606105819), p = h(p, s, c, r, k[i + 3], I, 3250441966), r = h(r, p, s, c, k[i + 4], P, 4118548399), c = h(c, r, p, s, k[i + 5], S, 1200080426), s = h(s, c, r, p, k[i + 6], C, 2821735955), p = h(p, s, c, r, k[i + 7], I, 4249261313), r = h(r, p, s, c, k[i + 8], P, 1770035416), c = h(c, r, p, s, k[i + 9], S, 2336552879), s = h(s, c, r, p, k[i + 10], C, 4294925233), p = h(p, s, c, r, k[i + 11], I, 2304563134), r = h(r, p, s, c, k[i + 12], P, 1804603682), c = h(c, r, p, s, k[i + 13], S, 4254626195), s = h(s, c, r, p, k[i + 14], C, 2792965006), p = h(p, s, c, r, k[i + 15], I, 1236535329), r = v(r, p, s, c, k[i + 1], E, 4129170786), c = v(c, r, p, s, k[i + 6], W, 3225465664), s = v(s, c, r, p, k[i + 11], N, 643717713), p = v(p, s, c, r, k[i + 0], _, 3921069994), r = v(r, p, s, c, k[i + 5], E, 3593408605), c = v(c, r, p, s, k[i + 10], W, 38016083), s = v(s, c, r, p, k[i + 15], N, 3634488961), p = v(p, s, c, r, k[i + 4], _, 3889429448), r = v(r, p, s, c, k[i + 9], E, 568446438), c = v(c, r, p, s, k[i + 14], W, 3275163606), s = v(s, c, r, p, k[i + 3], N, 4107603335), p = v(p, s, c, r, k[i + 8], _, 1163531501), r = v(r, p, s, c, k[i + 13], E, 2850285829), c = v(c, r, p, s, k[i + 2], W, 4243563512), s = v(s, c, r, p, k[i + 7], N, 1735328473), p = v(p, s, c, r, k[i + 12], _, 2368359562), r = b(r, p, s, c, k[i + 5], D, 4294588738), c = b(c, r, p, s, k[i + 8], T, 2272392833), s = b(s, c, r, p, k[i + 11], L, 1839030562), p = b(p, s, c, r, k[i + 14], O, 4259657740), r = b(r, p, s, c, k[i + 1], D, 2763975236), c = b(c, r, p, s, k[i + 4], T, 1272893353), s = b(s, c, r, p, k[i + 7], L, 4139469664), p = b(p, s, c, r, k[i + 10], O, 3200236656), r = b(r, p, s, c, k[i + 13], D, 681279174), c = b(c, r, p, s, k[i + 0], T, 3936430074), s = b(s, c, r, p, k[i + 3], L, 3572445317), p = b(p, s, c, r, k[i + 6], O, 76029189), r = b(r, p, s, c, k[i + 9], D, 3654602809), c = b(c, r, p, s, k[i + 12], T, 3873151461), s = b(s, c, r, p, k[i + 15], L, 530742520), p = b(p, s, c, r, k[i + 2], O, 3299628645), r = w(r, p, s, c, k[i + 0], U, 4096336452), c = w(c, r, p, s, k[i + 7], M, 1126891415), s = w(s, c, r, p, k[i + 14], B, 2878612391), p = w(p, s, c, r, k[i + 5], R, 4237533241), r = w(r, p, s, c, k[i + 12], U, 1700485571), c = w(c, r, p, s, k[i + 3], M, 2399980690), s = w(s, c, r, p, k[i + 10], B, 4293915773), p = w(p, s, c, r, k[i + 1], R, 2240044497), r = w(r, p, s, c, k[i + 8], U, 1873313359), c = w(c, r, p, s, k[i + 15], M, 4264355552), s = w(s, c, r, p, k[i + 6], B, 2734768916), p = w(p, s, c, r, k[i + 13], R, 1309151649), r = w(r, p, s, c, k[i + 4], U, 4149444226), c = w(c, r, p, s, k[i + 11], M, 3174756917), s = w(s, c, r, p, k[i + 2], B, 718787259), p = w(p, s, c, r, k[i + 9], R, 3951481745), r = l(r, t), p = l(p, n), s = l(s, a), c = l(c, o);
        var H = y(r) + y(p) + y(s) + y(c);
        return H.toLowerCase()
    },
    base64: function(arrayBuffer) {

        if( typeof arrayBuffer === 'undefined' || arrayBuffer === null ) return '';
        if (!(arrayBuffer instanceof ArrayBuffer) || (arrayBuffer.byteLength === undefined)) return '';

        var base64    = '';
        var encodings = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_';
        var bytes         = new Uint8Array(arrayBuffer);
        var byteLength    = bytes.byteLength;
        var byteRemainder = byteLength % 3;
        var mainLength    = byteLength - byteRemainder;

        var a, b, c, d;
        var chunk;

        for (var i = 0; i < mainLength; i = i + 3) {
            chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2];
            // Use bitmasks to extract 6-bit segments from the triplet
            a = (chunk & 16515072) >> 18; // 16515072 = (2^6 - 1) << 18
            b = (chunk & 258048)   >> 12; // 258048   = (2^6 - 1) << 12
            c = (chunk & 4032)     >>  6; // 4032     = (2^6 - 1) << 6
            d = chunk & 63;               // 63       = 2^6 - 1
            // Convert the raw binary segments to the appropriate ASCII encoding
            base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d]
        }

        if (byteRemainder == 1) {
            chunk = bytes[mainLength];
            a = (chunk & 252) >> 2; // 252 = (2^6 - 1) << 2
            // Set the 4 least significant bits to zero
            b = (chunk & 3)   << 4; // 3   = 2^2 - 1
            base64 += encodings[a] + encodings[b] + '=='
        } else if (byteRemainder == 2) {
            chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1];
            a = (chunk & 64512) >> 10; // 64512 = (2^6 - 1) << 10
            b = (chunk & 1008)  >>  4; // 1008  = (2^6 - 1) << 4
            // Set the 2 least significant bits to zero
            c = (chunk & 15)    <<  2; // 15    = 2^4 - 1
            base64 += encodings[a] + encodings[b] + encodings[c] + '=';
        }

        return base64;
    },
};

// Split a byte array into chunks of size.
function chunkArray(array, size) {
    var start = array.byteOffset || 0;
    array = array.buffer || array;
    var index = 0;
    var result = [];
    while(index + size <= array.byteLength) {
        result.push(new Uint8Array(array, start + index, size));
        index += size;
    }
    if (index <= array.byteLength) {
        result.push(new Uint8Array(array, start + index));
    }
    return result;
}

var base64url = {
    _strmap: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefg' +
    'hijklmnopqrstuvwxyz0123456789-_',
    encode: function(data) {
        //  console.debug('Encoding:', data);
        data = new Uint8Array(data);
        var len = Math.ceil(data.length * 4 / 3);
        return chunkArray(data, 3).map(chunk => [
                chunk[0] >>> 2,
                ((chunk[0] & 0x3) << 4) | (chunk[1] >>> 4),
                ((chunk[1] & 0xf) << 2) | (chunk[2] >>> 6),
                chunk[2] & 0x3f
            ].map(v => base64url._strmap[v]).join('')).join('').slice(0, len);
    },
    _lookup: function(s, i) {
        return base64url._strmap.indexOf(s.charAt(i));
    },
    decode: function(str) {
        var v = new Uint8Array(Math.floor(str.length * 3 / 4));
        var vi = 0;
        for (var si = 0; si < str.length;) {
            var w = base64url._lookup(str, si++);
            var x = base64url._lookup(str, si++);
            var y = base64url._lookup(str, si++);
            var z = base64url._lookup(str, si++);
            v[vi++] = w << 2 | x >>> 4;
            v[vi++] = x << 4 | y >>> 2;
            v[vi++] = y << 6 | z;
        }
        return v;
    }
};


if (tempQRZPush) {
    var isInit = !1,
        e;
    for (item in tempQRZPush) in_array("init", tempQRZPush[item])
        && (isInit = !0, 0 != item && (e = tempQRZPush[0], tempQRZPush[0] = tempQRZPush[item], tempQRZPush[item] = e));
    isInit || QRZPush.push(["init"]), QRZPush.processPushes(tempQRZPush)
}

if ('serviceWorker' in navigator) {
    navigator.serviceWorker.addEventListener('message', function (event) {
        console.debug('Service Worker sent:', JSON.stringify(event.data));
        if (event.data.type == 'QrzPushMessage') {
            var notification = new Notification("Обновление на QRZ.RU", {
                body: event.data.content,
                icon: "http://www.qrz.ru/images/mstile-150x150.png"
            });

            notification.onclick = function(event) {
                event.preventDefault();
                window.open('http://www.qrz.ru' + event.data.url, '_blank');
            }
        }
    });
}
