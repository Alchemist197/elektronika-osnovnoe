// Port addresses for ATA IO adapter 05a

// The ATA IO adapter is the slave device

// Addresses to be loaded into DH register before accessing the port

#define ioport_a    	0x11
#define ioport_b    	0x12
#define ioport_c    	0x13
#define ioport_d    	0x14
#define ioport_e	    0x15
