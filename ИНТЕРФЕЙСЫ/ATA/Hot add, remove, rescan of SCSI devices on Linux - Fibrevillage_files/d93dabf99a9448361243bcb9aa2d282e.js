

window.compojoom = compojoom = window.compojoom || {};
	compojoom.ccomment = {
			user: {"loggedin":false,"avatar":"http:\/\/fibrevillage.com\/media\/com_comment\/images\/noavatar.png"},
					item: {"contentid":279,"component":"com_content","count":0},
					config: {"comments_per_page":5,"sort":0,"tree":1,"tree_depth":5,"form_position":0,"voting":1,"copyright":1,"pagination_position":0,"avatars":0,"gravatar":1,"baseUrl":"http:\/\/fibrevillage.com\/","langCode":"en"}
				};

window.addEvent('domready', function() {
	if (!('ontouchstart' in document.documentElement)) {
	    document.documentElement.className += ' ccomment-no-touch';
	}

	var commentsCollection = new Ccomment.Comments([]),
		hash = location.hash, startPage = 1, comment = 0, params = {};

	var cv = new Ccomment.CommentsOuter({
		collection: commentsCollection,
		template: document.id('comment-outer-template').get('html'),
		element: 'ccomment-content-279'
	});

	if(hash.indexOf('#!/ccomment-page') === 0) {
		startPage = hash.replace('#!/ccomment-page=','');
		params = {start: startPage};
	}

	if(hash.indexOf('#!/ccomment-comment=') === 0) {
		comment = hash.replace('#!/ccomment-comment=','');
		params = {comment: comment};
	}

	commentsCollection.fetch(false, params);
});