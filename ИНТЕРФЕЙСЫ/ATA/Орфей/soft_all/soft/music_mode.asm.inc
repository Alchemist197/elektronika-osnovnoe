;;==========================================================================
;; ������ ������ ������ "������� ������"
;;

.dseg
mmusic_SkipCnt:		.byte	1		; �ޣ���� ��������� ���������� ������
mmusic_SREG:		.byte	1		; SREG, ������� ������ ��������� �� mmusic_E_UserInterface

.eseg
nv_DirEntLabel:		.byte	MaxDirDeep*3	; �������������� ��������

.cseg
;
; ������� ��������� "������� ������"
;
mmusic_Enter:
 sbrs	r16, 0
 rjmp	mmusic_E_GoTo_ModeMenu	; � ������ ������������� �� ����� ��������� � ����� �������� ����

;
; ������� ���� ������������ �����
;
mmusic_E_StartPlay:
 rcall	mmusic_PathPrint	; ������� ������� ����� �� ����� �����
 
 call	file_Idnt		; �������� ����� �� mp3-������
 brcc	mmusic_E_NextFile	; ���� ��� - ������ ���� ����������

 call	file_OpenAndHold	; ������� �������. ���� ���� �������� - ���������m, ���� ������ �����,
 call	lcd_PrintC		; ������ �������� �����ģ� � ������ ��� �������� �� mp3-������
.db	"Open: ", 1
 brcc	mmusic_E_NextFile

 call	vlsi_Open		; NB: ����� �������� �������� VLSI ��� - ���� ��� ������ ����, ����� ���� ��������

 ldi	r16, 1              
 sts	mmusic_SkipCnt, r16

mmusic_E_SP_1:
 ldi	ZL, low(IDEBuf0)	; �������� ��������� ����
 ldi	ZH, high(IDEBuf0)
 call	file_Read
 brcs	mmusic_E_SP_2		; � ������ ������ - ������
 call	lcd_PrintC		; ����� - ���������, ��������� �� ������, ������� � ���������� �����
.db	"Read: ", 1
 rjmp	mmusic_E_NextFile
mmusic_E_SP_2:

 rcall	mmusic_E_KbAnalyse	; ������ ��������� ����������
 breq	mmusic_E_StartPlay	; ������������ �������� ����� �����
 brts	mmusic_E_GoTo_ModeMenu	; ������������ �������� ����� �� ������
 brcs	mmusic_E_NextFile	; ������������ �������� ��������� ����

 lds	r16, kb_Keys		; ���������� ������ ����������� �� �� ������ ������...
 tst	r16
 brne	mmusic_E_SP_5		; ...������ ���� ���� ������ �������...
 lds	r16, mmusic_SkipCnt
 dec	r16
 brne	mmusic_E_SP_3
 ldi	r16, 30			; ...���� �� ������ 30 ������
mmusic_E_SP_3:
 sts	mmusic_SkipCnt, r16
 brne	mmusic_E_SP_4		; ����� �������� �� ���������� "dec"
mmusic_E_SP_5:
 rcall	mmusic_E_RefreshDisplay
mmusic_E_SP_4:

 ldi	ZL, low(IDEBuf0)	; �������� ���� �� VLSI
 ldi	ZH, high(IDEBuf0)
 call	vlsi_SendBuf

 call	file_Step		; ��������� � ���������� �������
 brcs	mmusic_E_SP_1		; C == 0 � ������ ��� ��������� �����, ��� � ������������� ��������
 				; ���������� �� ��� �����������
 call	vlsi_Close		; ��������������� ����������� ����� (������)

mmusic_E_NextFile:		; ������� �������� � ���������� �����
 call	file_Next
 call	lcd_PrintC
.db	"Next: ", 1
 brcc	mmusic_E_GoTo_ModeMenu
 rjmp	mmusic_E_StartPlay

mmusic_E_GoTo_ModeMenu:
 ldi	r16, m_ModeMenu		; � ������ ������������� �� ���� ����� �� �����
 sec
 ret

;
; ���������� �������
;
mmusic_E_RefreshDisplay:
 ldi	r16, 0x00
 call	lcd_SetAttr

 call	vlsi_SetTones

 clr	r17			; ���������
 call	lcd_GoToLine

 lds	dd16uL, vlsi_V_L
 com	dd16uL
 call	DecByteOut
 ldi	r25, '/'
 call	lcd_CharOut
 lds	dd16uL, vlsi_V_R
 com	dd16uL
 call	DecByteOut

 ldi	r25, ' '
 call	lcd_CharOut

 lds	dd16uL, vlsi_Time+0	; �������
 lds	dd16uH, vlsi_Time+1
 call	DecWordOut

 ldi	r25, ' '
 call	lcd_CharOut

 lds	r25, vlsi_Head+0	; �������
 call	lcd_CharOut
 lds	r25, vlsi_Head+1
 call	lcd_CharOut
 lds	r25, vlsi_Head+2
 call	lcd_CharOut
 lds	r25, vlsi_Head+3
 call	lcd_CharOut

 ldi	r25, ' '
 call	lcd_CharOut

 lds	dd16uL, vlsi_BitRate+0
 lds	dd16uH, vlsi_BitRate+1
 call	DecWordOut

 jmp	lcd_ClrEol		; FIN

;
; ������ ������
;
; ���������� � Z � T ���������� ��������:
; Z = 1 - ����� ������� ���� (���� ��� ����Σ�)
; Z = 0, T = 1 - ������� � ModeMenu
; Z = 0, T = 0, C = 1 - ������� � ���������� �����
; Z = 0, T = 0, C = 0 - �������� ������
;
mmusic_E_KbAnalyse:

; ��� F-�������:		��������� ������:
;	NextF	NextD		0	1
;	PrevF	PrevD		2	3
;	Vol+	[F] ?		4	5
;	Vol-	Pause		6	7
; � �������� F:
;	?	pc_Menu
;	Skip	MainMenu
;	Repeat	[F] -
;	Pan-	Pan+

 in	r16, SREG
 cbr	r16, 0x43		; �������� T, Z, C
 sts	mmusic_SREG, r16

 call	ur_KeybRoute
.db	Key0mask, 0x40
.dw	mmusic_E_kr_NextFile

.db	Key2mask, 0x40
.dw	mmusic_E_kr_PrevFile

.db	Key1mask, 0x40
.dw	mmusic_E_kr_NextDir

.db	Key3mask, 0x40
.dw	mmusic_E_kr_PrevDir

.db	Key4mask, 0x00
.dw	mmusic_E_kr_IncVol

.db	Key6mask, 0x00
.dw	mmusic_E_kr_DecVol

.db	Key7mask, 0x40
.dw	mmusic_E_kr_Pause


.db	Key0mask | Key2mask, 0x40
.dw	mmusic_E_kr_SaveLabel

.db	Key1mask | Key3mask, 0x40
.dw	mmusic_E_kr_LoadLabel


.db	Key1mask | Key5mask, 0x40
.dw	mmusic_E_kr_pc_Menu

.db	Key2mask | Key5mask, 0x40
.dw	mmusic_E_kr_Skip

.db	Key3mask | Key5mask, 0x40
.dw	mmusic_E_kr_MainMenu

.db	Key4mask | Key5mask, 0x40
.dw	mmusic_E_kr_Repeat

.db	Key6mask | Key5mask, 0x00
.dw	mmusic_E_kr_DecPan

.db	Key7mask | Key5mask, 0x80
.dw	mmusic_E_kr_IncPan

 lds	r16, mmusic_SREG	; �������� �������� ������ ������ ����������� ��������
 out	SREG, r16
 ret

;
; ������� �� �������� ����������������� ����������
;
mmusic_E_kr_NextFile:		; �� ��������� ����
 lds	r16, mmusic_SREG
 sbr	r16, 0x01		; C = 1
 sts	mmusic_SREG, r16
 ret

mmusic_E_kr_PrevFile:
 rcall	file_Prev
 call	lcd_PrintC
.db	"Prev: ", 1
 brcc	mmusic_E_kr_NextFile	; ���������� ������ ��������-����� - ���� ����� ���� ��� ������ ���������������
 rcall	file_Idnt
 brcc	mmusic_E_kr_PrevFile

mmusic_E_kr_Repeat:
mmusic_E_kr_Send_REOPENFILE:
 lds	r16, mmusic_SREG
 sbr	r16, 0x02		; Z = 1
 sts	mmusic_SREG, r16
 ret

mmusic_E_kr_NextDir:
 rcall	pc_NextDir
 rjmp	mmusic_E_kr_Send_REOPENFILE

mmusic_E_kr_PrevDir:
 rcall	pc_PrevDir
 rjmp	mmusic_E_kr_Send_REOPENFILE

mmusic_E_kr_IncVol:
 ldi	r16, -1
 clr	r17
 jmp	vlsi_ChVol

mmusic_E_kr_DecVol:
 ldi	r16, 1
 clr	r17
 jmp	vlsi_ChVol

mmusic_E_kr_Pause:
 clc
 call	lcd_PrintC
.db	"- PAUSE * ", 1
 ret

mmusic_E_kr_Skip:
 ldi	r17, 1<<vlsi_SM_FFWD
 lds	r16, vlsi_Mode
 eor	r16, r17
 sts	vlsi_Mode, r16
 ret

mmusic_E_kr_MainMenu:
 lds	r16, mmusic_SREG
 sbr	r16, 0x40		; T = 1
 sts	mmusic_SREG, r16
 ret

mmusic_E_kr_DecPan:
 clr	r16    
 ldi	r17, -1
 jmp	vlsi_ChVol

mmusic_E_kr_IncPan:
 clr	r16    
 ldi	r17, 1 
 jmp	vlsi_ChVol

mmusic_E_kr_pc_Menu:
 rcall	pc_Menu
 rjmp	mmusic_E_kr_Send_REOPENFILE

mmusic_E_kr_SaveLabel:
 call	f_IO
.db	f_OWrite, MaxDirDeep*3
.dw	nv_DirEntLabel
.dw	CurrentDirEnt
 clc
 call	lcd_PrintC
.db	"�������� �������� !", 1
 ret

mmusic_E_kr_LoadLabel:
 call	f_IO
.db	f_ORead, MaxDirDeep*3
.dw	nv_DirEntLabel
.dw	CurrentDirEnt
 rjmp	mmusic_E_kr_Send_REOPENFILE

;
; ������� ��� �������� �����:
; ������ 1, I: ������������� �������������
; ������ 2, I: ������������� ���������
; ������ 3, N: ��� ����� (������)
; ������ 4, N: ��� ����� (�����������)
; ������ 5, N: ��� ����� (���������)
;
mmusic_PathPrint:

;; ����� ����

 ldi	r16, 0x80
 call	lcd_SetAttr


 ldi	r17, 1
 call	lcd_GoToLine

 rcall	pc_FindCurDeep
 push	r23
 ldi	r18, lcd_XPosMax
 cpi	r23, 1
 rcall	mmusic_SubPrintPath


 ldi	r17, 2
 call	lcd_GoToLine

 pop	r23
 inc	r23
 ldi	r18, lcd_XPosMax
 cpi	r23, 1
 rcall	mmusic_SubPrintPath

;; ���������� ���

 ldi	r16, 0x00
 call	lcd_SetAttr
 ldi	r17, 3
 call	lcd_GoToLine

 clr	r23
 ldi	r18, lcd_XPosMax*3
 cls				; sign(>=)

mmusic_SubPrintPath:
 brlt	mmusic_SPP_1		; if sign(r1) < sign(r2)

 push	r18
 rcall	file_Name
; push	ZL
; push	ZH
; rcall	lcd_PrintC
;.db	"Get filename: ", 1	; 	��� ������ - � errno !
; pop	ZH
; pop	ZL
 pop	r18
 brcc	mmusic_SPP_1

mmusic_SPP_2:
 ld	r25, Z+
 tst	r25
 breq	mmusic_SPP_1
 call	lcd_CharOutNoLimit
 rjmp	mmusic_SPP_2

mmusic_SPP_1:
 jmp	lcd_ClrEolNoLimit
