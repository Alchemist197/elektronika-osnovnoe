;; ������� ������

.nolist
.include "/usr/local/include/avr/m32def.inc"
.list

;
; ������ � ���
;
.dseg

IDEBuf0:	.byte	512	; ������ IDE
IDEBuf1:	.byte	512	; ����� (��� ��������� FAT12 � ������� ������) ����� ��� ��� ���� �� ������
main_FSState:	.byte	1	; ��������� (�����������) �������� �������
main_Mode:	.byte	1	; ����� ������:

.equ	m_ModeMusic	= 0
.equ	m_ModeSine	= 1
.equ	m_ModeMenu	= 2+0x80
.equ	m_ModeText	= 3

.eseg
nv_main_Mode:	.byte	1	; ����� ����������� ����� ������, �� �� ������������ ������ ���� D7 ������ == 0

.cseg

.org	0x000
 jmp	RESET		; Reset Handler

 jmp	EXT_INT0	; IRQ0 Handler
 jmp	EXT_INT1	; IRQ1 Handler
 jmp	EXT_INT2	; IRQ2 Handler

 jmp	TIM2_COMP	; Timer2 Compare Handler
 jmp	TIM2_OVF	; Timer2 Overflow Handler

 jmp	TIM1_CAPT	; Timer1 Capture Handler
 jmp	TIM1_COMPA	; Timer1 CompareA Handler
 jmp	TIM1_COMPB	; Timer1 CompareB Handler
 jmp	TIM1_OVF	; Timer1 Overflow Handler
 
 jmp	TIM0_COMP	; Timer0 Compare Handler
 jmp	TIM0_OVF	; Timer0 Overflow Handler

 jmp	SPI_STC		; SPI Transfer Complete Handler

 jmp	USART_RXC	; USART RX Complete Handler
 jmp	USART_UDRE	; UDR Empty Handler
 jmp	USART_TXC	; USART TX Complete Handler

 jmp	ADCC		; ADC Conversion Complete Handler
 jmp	EE_RDY		; EEPROM Ready Handler
 jmp	ANA_COMP	; Analog Comparator Handler
 jmp	TWI		; Two-wire Serial Interface
 jmp	SPM_RDY		; Store Program Memory Ready

EXT_INT0:
EXT_INT1:
EXT_INT2:

TIM2_COMP:
TIM2_OVF:

TIM1_CAPT:
;TIM1_COMPA:
TIM1_COMPB:
TIM1_OVF:

TIM0_COMP:
TIM0_OVF:

SPI_STC:

USART_RXC:
USART_UDRE:
USART_TXC:

ADCC:
EE_RDY:
ANA_COMP:
TWI:
SPM_RDY:

;;==========================================================================
;; Main ENTER
;;
RESET:				; Main program start
 ldi	r16, high(RAMEND)
 out	SPH, r16		; Set Stack Pointer to top of RAM
 ldi	r16, low(RAMEND)
 out	SPL, r16

; ������� ���������������� ��������� ���������� �����������

 call	spi_Init		; ������������� SPI
; rcall	rs_Init			; ������������� USART
 rcall	pc_Init			; ������ ������������� ����������
 sbi	PORTB, 2		; ��� ���� �� ������������, ������� ������
 sbi	PORTD, 2		; �� PullUp, ����� ������� �����������������

; ������ ��������� ���������� ����� ������ ���������

 sbi	DDRB, 1			; ���������� ������������� ������������ ���������: VLSI, LCD
 cbi	PORTB, 1		; � ����� �������� ����� ������������� SPI, �����
 call	Wait1mks		; ������������� ���������� ����� � �������������������������
 sbi	PORTB, 1

; ��������� ���������� - ������������ ��� ��������� ��� �������� ������ ��������

; ldi	r16, (1<<SE) | (1<<ISC01) | (1<<ISC00)
; out	MCUCR, r16		; INT0 �� �������������� ������ INT0
; ldi	r16, (1<<INT0)
; out	GICR, r16		; ��������� INT0
; sei

; ������, ����� ������� ���������� ������ � ������ �� �����������
; ������, ����� ��������� �� (����������) � ����� ���������

 call	lcd_LightOn
 call	lcd_Init		; ������������� LCD

 ldi	r17, 2			; � ����� ����������
 call	lcd_GoToLine
 ldi	ZL, low(main_HelloTxt*2)
 ldi	ZH, high(main_HelloTxt*2)
M0:
 lpm	r25, Z+
 tst	r25
 breq	M1
 call	lcd_CharOut
 rjmp	M0
M1:

 call	vlsi_Init		; ������������� VLSI

 call	vlsi_Open		; ������� ���������� � VLSI
 tst	r20
 breq	M4
 ldi	r17, 1
 call	lcd_GoToLine
 mov	dd16uL, r20
 call	DecByteOut
 clc
 call	lcd_PrintC		; ���� �� ��������� - ������ ��������� ��������� �����
.db	"VLSI init: ", 1	; ��� ������ - � r20 !
 rjmp	RESET
M4:

 call	file_Init		; ������������� ��
 call	lcd_PrintC
.db	"File system init: ", 1
 in	r16, SREG		; ����� C ����� ������� ������� �������, ����� ��� ����� ��������� ��
 sts	main_FSState, r16

; ������ ������ ���������� � ����������� �� �����Σ����� ������

 call	f_IO
.db	f_ORead, 1
.dw	nv_main_Mode
.dw	main_Mode

M2:
 ldi	ZL, low(main_RouteTable)
 ldi	ZH, high(main_RouteTable)

 lds	r16, main_Mode
 andi	r16, 0x03
 lsl	r16
 clr	r17

 add	ZL, r16
 adc	ZH, r17
 
 lds	r16, main_FSState	; r16.0 = 0, � ������ ������������� ��
 icall

; ��� �������� ���������� ���������� ������ ������� � r16 ��� ������, �� ������� ����� �������
; ������ ���� C = 0, ���������� ����� �������� ����� RESET
; (������� ���������� ����� VLSI, LCD...), ����� - ����� ������

 in	r2, SREG
 lds	r17, main_Mode
 sts	main_Mode, r16
 cp	r16, r17
 breq	M3			; �� ����� ��� ������������ EEPROM ��-�� ������ �����

 tst	r16			; ���� ��� D7 == 1 - ��� �����-�� ������������� �����, ����� �������
 brmi	M3

 call	f_IO
.db	f_OWrite, 1
.dw	nv_main_Mode
.dw	main_Mode
 
M3:
 sbrs	r2, 0
 rjmp	RESET
 rjmp	M2

main_HelloTxt:
.db	"     ** ���� **", 0

main_RouteTable:		; �������� ������ long jump
 jmp	mmusic_Enter
 jmp	msine_Enter
 jmp	mmenu_Enter
 jmp	mtext_Enter

.include "music_mode.asm.inc"
.include "sine_mode.asm.inc"
.include "menu_mode.asm.inc"
.include "text_mode.asm.inc"

.include "path_changer.asm.inc"

.include "fs.asm.inc"
.include "media.asm.inc"
.include "flash.asm.inc"
.include "LCD.asm.inc"
.include "VLSI.asm.inc"
.include "sleep.asm.inc"
.include "spi.asm.inc"
.include "math.asm.inc"
.include "encode.asm.inc"
.include "user_router.asm.inc"

;
; ���������� �����
;
lcd_T_index:
.include "LCD.font-index.inc"

lcd_T_bmp:
.include "LCD.font-bmp.inc"

;
; In system programm
;
.include "selfprog.asm.inc"
