#include <sys/select.h>
#include <fcntl.h>
#include <stdio.h>
#include <termios.h>

#include "gethex.c"


// ���������� ���������� � ���������������� ������ ������
int	fdSerial;

// ��� �������� ����������������� �����
#define	SIO_NAME	"/dev/cuad0"


// �������� ��������� ���� �� ��������������� ���������� � ���������� 1,
// ���� (���� ��� ����� ������) ������ ���������� ���������� � 0
int recv_byte_from_sio(unsigned char *to) {
  fd_set r;
  struct timeval tv;

  tv.tv_sec = 0; tv.tv_usec = 0;
  FD_ZERO(&r); FD_SET(fdSerial, &r);
  if ( select(fdSerial+1, &r, NULL, NULL, &tv) > 0 ) {
    read(fdSerial, to, 1);
    return 1;
  };
  return 0;
};


// ���������� ���� ����������������� ����������
void send_byte_to_sio(unsigned char v, unsigned char *crc) {
//  printf("-> %02X\n", v);
  usleep(1000);		// ����� ����������� ���������, �� ������� �� �������� ��
  if (write(fdSerial, &v, 1) != 1) { perror("write to serial"); exit(2); };
  *crc += v;
}


// ������������� ����� � TV-set
void init_sio() {
  unsigned char b = 0, crc;
  struct termios ti;

  if ((fdSerial = open(SIO_NAME, O_RDWR | O_FSYNC)) == -1) { perror("open serial"); exit(1); };

  if (tcgetattr(fdSerial, &ti)) { perror("get attributes"); exit(1); };
  cfsetspeed(&ti, B38400);
  cfmakeraw(&ti);
  if (tcsetattr(fdSerial, TCSANOW, &ti)) { perror("set attributes"); exit(1); };

  printf("������� ������ ���� ��������. �������� ��� ������...\n");
  while (b != 'S') {
    send_byte_to_sio('R', &crc);
    recv_byte_from_sio(&b);
  };
  send_byte_to_sio('N', &crc);
  printf("ready.\n");
}


// ��������� ����� � TV-set
void close_sio() {
  close(fdSerial);
}


// ��������� �������� �� ������� ������� ���� != 0xFF
// ���������� 1, ���� �������� ������ ���� �����������������
int test_page(unsigned char *buf, int bufsize) {
  int i;

  for (i = 1; i <= bufsize; i++, buf++) if (*buf != 0xFF) return 1;
  return 0;
}


// ���������� �������� ��� ����������������
void send_page(int addr, unsigned char *buf, int bufsize) {
  unsigned char *p, crc, b;
  int i;

  printf("Page write to 0x%04X: ", addr);
  do {

    p = buf; crc = 0;

    send_byte_to_sio( addr	 & 0xFF, &crc);
    send_byte_to_sio((addr >> 8) & 0xFF, &crc);

    for (i = 1; i <= bufsize; i++) send_byte_to_sio(*p++, &crc);
    send_byte_to_sio(0x100-crc, &crc);

l1:
    read(fdSerial, &b, 1);
    switch (b) {
      case 'F': printf("write error !\n"); break;
//      case 'C': printf("C"); break;
      case 'O': printf("Ok\n"); break;
       default: printf("%c", b); goto l1;
    };

  } while (b != 'O' && b != 'F');
}


#define BYTE_PER_PAGE	(128)
#define NUM_PAGES	(256-4)
main(int argc, char **argv) {
  int i;

  if (argc > 1) {
    hx_fileread_hex(argv[1]);
    init_sio();
    for (i = 0; i < NUM_PAGES; i++) {
      if ( test_page(&(hx_buffer[BYTE_PER_PAGE * i]), BYTE_PER_PAGE) ) {
        send_page(BYTE_PER_PAGE * i, &(hx_buffer[BYTE_PER_PAGE * i]), BYTE_PER_PAGE);
      };
    };
    close_sio();
  };
}
