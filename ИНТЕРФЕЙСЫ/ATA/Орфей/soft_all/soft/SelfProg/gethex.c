// ������ ���������� ���� hex-������� � ������������ ��� �� ������ hx_buffer

#include <stdio.h>

unsigned char *hx_cur, *hx_buf, hx_buffer[0x10000];
long hx_buf_size;
int hx_crc_line;


// %1x
int hx_dig2hex(unsigned char v) {
  if (v >= '0' && v <= '9') { return v - '0'; } else
  if (v >= 'A' && v <= 'F') { return v - 'A' + 10; } else
  if (v >= 'a' && v <= 'f') { return v - 'a' + 10; } else
  return -1;
}


// sscanf(hx_cur, "%02x", &r)
int hx_str2hex() {
  int r1 = hx_dig2hex(*(hx_cur+0));
  int r2 = hx_dig2hex(*(hx_cur+1));

  if (r1 == -1 || r2 == -1) { return -1; } else { return (r1 << 4) | r2; };
}


// ������ ��������� �������� � ���������� �����
// ��������� ������������ ��������� � ���������� ���������:
// 0 - ����������������� �����. ���������� ��� ��������
// 1 - ������� ������
// 2 - ':'
// 3 - ����� �����

int hx_next_simple(int wait_simple) {
  int real_simple = 255, r;

  if (hx_cur >= hx_buf + hx_buf_size)		{ real_simple = 3; } else
  if (*hx_cur == ':')				{ real_simple = 2; hx_cur++; } else
  if (*hx_cur == 13 && *(hx_cur+1) == 10)	{ real_simple = 1; hx_cur += 2; } else
  if (*hx_cur == 10)				{ real_simple = 1; hx_cur++; } else
  if ((r = hx_str2hex()) != -1)			{ real_simple = 0; hx_cur += 2; hx_crc_line += r; };

  if (real_simple != wait_simple) {
    fprintf(stderr, "Wait simple %d != real simple %d !\n", wait_simple, real_simple); exit(10);
  };
  return r;
};



// ����������� ��������� ������ � ���������� �� ���.
int hx_line_analys() {
  int len_line, offset, type, i;

  hx_crc_line = 0;
  hx_next_simple(2);
  len_line = hx_next_simple(0);
  offset = hx_next_simple(0) << 8; offset += hx_next_simple(0);
  type = hx_next_simple(0);
  if (type != 0 && type != 1) { printf("Line type = %d unknow.\n", type); };
  for (i = 0; i < len_line; i++) if (type == 0) { hx_buffer[offset++] = hx_next_simple(0); } else { hx_next_simple(0); };
  hx_next_simple(0);
  if (hx_crc_line & 0xff) { fprintf(stderr, "CRC error !\n"); exit(11); };
  hx_next_simple(1);
  return type;
}


void hx_fileread_hex(char *name) {
  FILE *f;

  f = fopen(name, "r"); if (! f) { perror(name); exit(6); };

  fseek(f, 0, SEEK_END); hx_buf_size = ftell(f); fseek(f, 0, SEEK_SET);
  hx_buf = malloc(hx_buf_size); if (! hx_buf) { perror("memory"); exit(7); };

  fread(hx_buf, hx_buf_size, 1, f);

  fclose(f);

  memset(hx_buffer, 0xff, sizeof(hx_buffer));
  hx_cur = hx_buf;
  while (hx_line_analys() != 1);

  free(hx_buf);
}
