unit mikky;

interface
uses Inferno, alf, x, xlib, xutil, VGAColors, Ini;

  {$i cursorfont.inc}
const
  DefaultMouseCursor	= XC_left_ptr;
  MouseX		: LongInt = 100; // ��� �������� ����� ������ � ��������� ������� ��������� �������� !!!
  MouseY		: LongInt = 100;
  MouseXroot		: LongInt = 100;
  MouseYroot		: LongInt = 100;
  NotExistMouseArea	= 400;
  MouseLmsk		= 1;
  MouseMmsk		= 2;
  MouseRmsk		= 4;
  

type
  RngMouseArea		= 0..400;
  MouseCursorType	= Cardinal;
  TMouseState		= (msFree, msActive, msUnActive);
  TButDrawProc		= procedure(const Wnd: TWindow; var GC: TGC; const Colors: TColors; var dt);
  TButActProc		= procedure(var dt);
  TMouseArea		= array[RngMouseArea] of record { 13 }
    Status	: TMouseState;
    bsWnd, msWnd: TWindow;
    pdt		: Pointer;
    x, y,
    xl, yl	: LongInt;
    colB, colF	: Byte;
    Colors	: TColors;
    DrawProc	: TButDrawProc;
    LActProc, RActProc, MActProc	: TButActProc;
    Curs	: MouseCursorType;
    lab		: String;
  end;

// ���������� ��� �������, �� ������������ �� ����� �� ���������� ��������
procedure SetDefaultCursor(const Wnd: TWindow; const NewCurs: MouseCursorType);

// �������� ����� ������� ����
procedure NewMouseArea(const Wnd: TWindow; var nr: RngMouseArea);

// ���������� ����������� ��� ������� ����
procedure SetMouseRoutines(const n: RngMouseArea; const D: TButDrawProc; var dt; const L, R, M: TButActProc);

// �������� ��������� ������� ����
procedure SetMouseArea(const n: RngMouseArea; const st: TMouseState; const xx, yy, xll, yll: LongInt; const cF, cB: Byte; const l: String; const c: MouseCursorType);
procedure SetMouseArea(const n: RngMouseArea; const cF, cB: Byte; const l: String);
procedure SetMouseArea(const n: RngMouseArea; const xx, yy, xll, yll: LongInt);

// �������� ��������� ������� ����, �� ��� �������� ������� (������ ����������� �������������)
procedure SetMouseArea(const n: RngMouseArea; const xx, yy: LongInt; const cF, cB: Byte; const l: String);

// ���������� ����� ������ ��� ���� [�������� ������� � ���������� �����������]
procedure SetMouseButton(const Wnd: TWindow; var n: RngMouseArea; const xx, yy, xll, yll: LongInt; const cF, cB: Byte; const l: String; const c: MouseCursorType);

// ����������������� ���� ������� ���� ��� ��������
function GetMouseWnd(const n: RngMouseArea): TWindow;

// �������������� ����������� ����������� ������
procedure DrawButton(const n: RngMouseArea);

// ��������� �������� �������� ����
procedure SaveAreaList(var t: TMouseArea);

// ������������ �������� �������� ����
procedure RestoreAreaList(var t: TMouseArea);

// ��������� ���� �� ����������� � ����� �������� �������
function MouseInArea(const n: RngMouseArea): Boolean;
function MouseCurrentArea: RngMouseArea;

// �������� ��������� ������
function TestButton(const n: Cardinal): Boolean;

implementation
const
  ButtonFont	: AnsiString = '-*-helvetica-medium-r-*-*-14-*-*-*-*-*-koi8-*';
  MouseButton	: Cardinal = 0;
  PressMove	= 2;
  CurrentArea	: RngMouseArea = NotExistMouseArea;

var
  MouseArea	: TMouseArea;
  OldFullingTime: TFullingTime;


procedure SetDefaultCursor(const Wnd: TWindow; const NewCurs: MouseCursorType);
var Cursor: TCursor;
begin
  Cursor := XCreateFontCursor(prDisplay, NewCurs);
  XDefineCursor(prDisplay, Wnd, Cursor);
  XFreeCursor(prDisplay, Cursor);
end;


function TestButton(const n: Cardinal): Boolean;
begin TestButton := (MouseButton and n) <> 0 end;


procedure NewMouseArea(const Wnd: TWindow; var nr: RngMouseArea);
var n: RngMouseArea;
begin
  for n:=Low(RngMouseArea) to High(RngMouseArea) do With MouseArea[n] do if MouseArea[n].Status = msFree then begin
    bsWnd := Wnd;
    Status := msActive;
    SetMouseRoutines(n, Nil, MouseArea[n], Nil, Nil, Nil);
    Curs := DefaultMouseCursor;
    NewWindow(Wnd, 0, 0, 10, 10, 10, 10, '', '', msWnd);
    SetVGAPalette(msWnd, Colors);
    nr:=n;
    exit;
  end;
  addlog('Mikky: NewMouseEvent: ��� ��������� ����� ��� ������ ������� ����.')
end;


{
procedure MoveButton(const n: RngMouseArea);
var Hints: TXSizeHints; Supplied: Long; xr, yr: LongInt;
begin With MouseArea[n] do begin
  XGetWMNormalHints(prDisplay, bsWnd, @Hints, @Supplied);
  if x < 0 then xr := Hints.width  + xr - xl else xr := x;
  if y < 0 then yr := Hints.height + yr - yl else xr := x;
  writeln(Hints.width :10, Hints.height:10);
  XMoveResizeWindow(prDisplay, msWnd, xr, yr, xl, yl);
end end;
}


procedure SetMouseRoutines(const n: RngMouseArea; const D: TButDrawProc; var dt; const L, R, M: TButActProc);
begin with MouseArea[n] do begin
  DrawProc := D; pdt := @dt;
  LActProc := L; RActProc := R; MActProc := M;
end end;


procedure SetMouseArea(const n: RngMouseArea; const xx, yy: LongInt; const cF, cB: Byte; const l: String);
const Border = 2;
begin With MouseArea[n] do begin
  SetMouseArea(n, cF, cB, l);

  AlfFakeSetFont(ButtonFont);
  xl := AlfWidthPS(lab) + Border * 2;
  yl := AlfHeightPS(lab) + Border * 2;
  
  x := xx - (xl div 2); y := yy - (yl div 2);
  XMoveResizeWindow(prDisplay, msWnd, x, y, xl, yl);
end end;


procedure SetMouseArea(const n: RngMouseArea; const xx, yy, xll, yll: LongInt);
begin With MouseArea[n] do begin
  x := xx; xl := xll;
  y := yy; yl := yll;
  if xl <= 0 then xl := 1;
  if yl <= 0 then yl := 1;
  XMoveResizeWindow(prDisplay, msWnd, x, y, xl, yl);
end end;


procedure SetMouseArea(const n: RngMouseArea; const cF, cB: Byte; const l: String);
begin With MouseArea[n] do begin
  colF := cF; 
  lab := l;
  if colB <> cB then begin
    colB := cB;
    XSetWindowBackground(prDisplay, msWnd, Colors[colB]);
    XClearWindow(prDisplay, msWnd);
    DrawButton(n);
  end;
end end;


procedure SetMouseArea(const n: RngMouseArea; const st: TMouseState; const xx, yy, xll, yll: LongInt; const cF, cB: Byte; const l: String; const c: MouseCursorType);
var Cursor: TCursor;
begin With MouseArea[n] do begin
  Status := St;
  curs := c;
  SetMouseArea(n, cF, cB, l);
  SetMouseArea(n, xx, yy, xll, yll);
  Cursor := XCreateFontCursor(prDisplay, curs);
  XDefineCursor(prDisplay, msWnd, Cursor);
  XFreeCursor(prDisplay, Cursor);
  case Status of
    msFree	: XDestroyWindow(prDisplay, msWnd);
    msActive	: XMapWindow	(prDisplay, msWnd);
    msUnActive	: XUnMapWindow	(prDisplay, msWnd);
  end;
end end;


procedure DrawButton(const n: RngMouseArea);
var GC: TGC;
begin With MouseArea[n] do begin
  GC := XCreateGC(prDisplay, msWnd, 0, Nil);
  XSetBackGround(prDisplay, GC, Colors[colB]);
  XSetForeGround(prDisplay, GC, Colors[colF]);
  AlfSetFont(GC, ButtonFont);
  if DrawProc = Nil then begin
//  XDrawRectangle(prDisplay, msWnd, GC, 1, 1, xl-2, yl-2);
    AlfCenter := True;
    AlfOutTextXYPS(msWnd, GC, 0, xl, yl div 2, lab);
    AlfCenter := False;
  end else DrawProc(msWnd, GC, Colors, pdt^);
  XFreeGC(prDisplay, GC);
end end;


procedure Press;
begin With MouseArea[CurrentArea] do begin
  if DrawProc = Nil then XMoveWindow(prDisplay, msWnd, x + PressMove, y + PressMove);
end end;

procedure UnPress;
begin With MouseArea[CurrentArea] do begin
  if DrawProc = Nil then XMoveWindow(prDisplay, msWnd, x, y);
end end;


procedure SetMouseButton(const Wnd: TWindow; var n: RngMouseArea; const xx, yy, xll, yll: LongInt; const cF, cB: Byte; const l: String; const c: MouseCursorType);
begin
  NewMouseArea(Wnd, n);
  SetMouseArea(n, msActive, xx, yy, xll, yll, cF, cB, l, c);
end;


function MouseInArea(const n: RngMouseArea): Boolean;
begin
  MouseInArea := n = CurrentArea;
end;


function MouseCurrentArea: RngMouseArea;
begin
  MouseCurrentArea := CurrentArea;
end;


function GetMouseWnd(const n: RngMouseArea): TWindow;
begin GetMouseWnd := MouseArea[n].msWnd end;


procedure SaveAreaList(var t: TMouseArea);
var n: RngMouseArea;
begin
  t := MouseArea;
  for n := Low(RngMouseArea) to High(RngMouseArea) do with MouseArea[n] do
    if Status = msActive then begin Status := msUnActive; XUnMapWindow(prDisplay, msWnd); end;
{ ����� �����������, � �� ��������� �������, ������ ��� �����
  ����� ��������� ����� ��������� �� ��������������� �������
  � ������������ �������� (�.�. ����� ����� ��������� ������ �� ��������������) }
end;


procedure RestoreAreaList(var t: TMouseArea);
var n: RngMouseArea;
begin
  MouseArea:=t;
  for n := Low(RngMouseArea) to High(RngMouseArea) do with MouseArea[n] do
    if Status = msActive then XMapWindow(prDisplay, msWnd);
end;


procedure MouseBG(var Event: TXEvent; const BlockEn: Boolean);
const Mask: Array[1..5] of Byte = ($01, $02, $04, $08, $10);
var n: RngMouseArea;
begin
  OldFullingTime(Event, BlockEn);

  case Event.EventType of

    MotionNotify: with Event.XMotion do begin
      MouseX := x; MouseXroot := x_root;
      MouseY := y; MouseYroot := y_root;
    end;

    ButtonPress, ButtonRelease: With Event.XButton do begin
      MouseX := x; MouseXroot := x_root;
      MouseY := y; MouseYroot := y_root;
      if Event.EventType = ButtonPress then begin
        MouseButton := MouseButton or Mask[Button];
	n := CurrentArea; // ����� ��������� � ActProc CurrentArea ����� ����������
	if n <> NotExistMouseArea then begin
	  Press;
	  with MouseArea[n] do case Button of
	  1: if LActProc <> Nil then begin LActProc(pdt^); DrawButton(n); end;
	  2: if MActProc <> Nil then begin MActProc(pdt^); DrawButton(n); end;
	  3: if RActProc <> Nil then begin RActProc(pdt^); DrawButton(n); end;
	  end;
	end;
      end else begin
        MouseButton := MouseButton and not Mask[Button];
	if CurrentArea <> NotExistMouseArea then UnPress;
      end;
      KeyInState := State;
    end;

    Expose: if Event.XExpose.Count = 0 then begin
      for n:=Low(RngMouseArea) to High(RngMouseArea) do with MouseArea[n] do begin
        if (Event.XExpose.Window = msWnd) and (Status = msActive) then begin DrawButton(n); break end;
      end;
    end;

    EnterNotify: With Event.XCrossing do begin
      for n:=Low(RngMouseArea) to High(RngMouseArea) do with MouseArea[n] do begin
        if (Window = msWnd) and (Status = msActive) then begin CurrentArea := n; if MouseButton <> 0 then Press; break end;
      end;
    end;

    LeaveNotify: With Event.XCrossing do if CurrentArea <> NotExistMouseArea then with MouseArea[CurrentArea] do if msWnd = Window then begin UnPress; CurrentArea := NotExistMouseArea; end;
{
    ConfigureNotify: begin
      for n:=Low(RngMouseArea) to High(RngMouseArea) do with MouseArea[n] do begin
        if (Event.XConfigure.Window = bsWnd) and (Status = msActive) then MoveButton(n);
      end;
    end;
}
  end;
end;


procedure Init;
var n: RngMouseArea;
begin
  OldFullingTime := FullingTime; FullingTime := @MouseBG;
  For n:=Low(RngMouseArea) to High(RngMouseArea) do MouseArea[n].Status := msFree;
  MouseArea[NotExistMouseArea].Status := msUnActive;

  IniOpen('Mikky');
  IniGet('ButtonLabelFont', ButtonFont);
  IniClose;
end;


begin
  Init
end.
