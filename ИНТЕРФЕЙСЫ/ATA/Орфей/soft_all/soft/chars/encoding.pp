Unit encoding;

interface

type UChar = Word;

Function u2k_encode(const s: Array of UChar): AnsiString;
Function a2k_encode(const s: AnsiString): AnsiString;
Function k2a_encode(const s: AnsiString): AnsiString;

implementation

Type
  ttable	= Array[#0..#255] of Char;
  tutable	= Array[0..$3fff] of Char;

Var
  a2k_table, k2a_table	: ttable;
  u2k_table		: tutable;

Function u2k_encode(const s: Array of UChar): AnsiString;
var n: LongInt;
begin
  u2k_encode := '';
  For n:=Low(s) to High(s) do If s[n] > High(tutable) then u2k_encode:=u2k_encode+'?' else If s[n] <> 0 then u2k_encode:=u2k_encode + u2k_table[s[n]] else Break;
end;

Procedure GetUTable(const fn: String);
var f: File of tutable;
begin
  Assign(f, fn); Reset(f);
  Read(f, u2k_table); Close(f);
end;

Procedure GetTable(const fn: String; var tb: ttable);
var f: File of ttable;
begin
  Assign(f, fn); Reset(f);
  Read(f, tb); Close(f);
end;

Function encode(const s: AnsiString; const t: ttable): AnsiString;
var n: LongInt;
begin
  SetLength(encode, Length(s));
  For n:=1 to Length(s) do encode[n]:=t[s[n]];
end;

Function a2k_encode(const s: AnsiString): AnsiString;
begin
  a2k_encode:=encode(s, a2k_table);
end;

Function k2a_encode(const s: AnsiString): AnsiString;
begin
  k2a_encode:=encode(s, k2a_table);
end;

begin
{$ifdef UNIX}
  GetUTable('/usr/home/deka/Source/utf2koi/utf-8.tbl.koi');
  GetTable('/usr/home/deka/Source/inferno/ALT2KOI.TBL', a2k_table);
  GetTable('/usr/home/deka/Source/inferno/KOI2ALT.TBL', k2a_table);
//  GetTable('/ms-dos/BaseTools:/DECODER/ALT2KOI.TBL', a2k_table);
//  GetTable('/ms-dos/BaseTools:/DECODER/KOI2ALT.TBL', k2a_table);
{$endif}
{$ifdef GO32V2}
  GetUTable('d:/DECODER/UTF/UTF-8.TBL');
  GetTable('d:/DECODER/ALT2KOI.TBL', a2k_table);
  GetTable('d:/DECODER/KOI2ALT.TBL', k2a_table);
{$endif}
end.