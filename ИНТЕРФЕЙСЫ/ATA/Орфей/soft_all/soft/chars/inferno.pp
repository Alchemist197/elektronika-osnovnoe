unit Inferno;
{$mode ObjFPC}
{$LinkLib c}
{$define nologfile}	// �� ��������� log-����

interface
uses x, xlib, x11, xutil, Error;

type
  {$PACKRECORDS C}
  T_tm_DateTime = record
          sec, min, hour,	// ������� (0 - 60), ������ (0 - 59), ���� (0 - 23)
	  mday, mon, year,	// ���� ������ (1 - 31), ����� (0 - 11), ��� - 1900
	  wday, yday,		// ���� ������ (Sunday = 0), ���� ���� (0 - 365)
	  isdst,		// ������� ������� �������
	  gmtoff : longint;	// �������� �� UTC � ��������	  
          zone : PChar;		// ������������ ��������� ����
       end;
  TFullingTime	= procedure(var Event: TXEvent; const BlockEn: Boolean);
  TKeys		= (skLShift, skLCtrl, skLAlt, skRAlt, skRCtrl, skRShift);


// ����� ������ ������������ ��� ��������� ���������� ������� � ������:
// FullingTime: � ��� ��������� ������
// FlTmWithPause: �� ������� ������������ - ��� �������� ���������� ^
// � ������������ ������������ ���������������� �����

// ������� ���������� �� ������� ���������� Inferno, ��� ���� � ����������� �� BlockEn
// �������� ������ ��� � ���������� ������� ��� � ��� ���. �� ���� ������� �� ���������,
// Event �� ���������� (� ����� ��������� �������������� !)

// ��������� ������������� �������
procedure InfernoBG(var Event: TXEvent; const BlockEn: Boolean);

// ����� FullingTime � ���������� ������� UserPause (LShift)
procedure FlTmWithPause(var Event: TXEvent; const BlockEn: Boolean);

const
  prgClass	= 'Inferno';
  FullingTime	: TFullingTime = @InfernoBG;
  KeyInResult	: TKeySym = 0;	// ��� ��������� ������������� �������. ��������������� ������������� KeyIn
  monoKeyInRes	: TKeySym = 0;	// ����������, �� ��� ������������ �������������
  monoKeyIn	: Boolean = False;// ��������� ������ ����� �������� � monoKeyInRes, � �� KeyInResult (������������ ��� ������������ �����)
  KeyInReleased	: TKeySym = 0;	// ��� ��������� ���������� �������. ��������������� ������������� KeyIn
  KeyInState	: Cardinal = 0; // ��������� Shift'��, Ctrl'�� � �������. �������������� Inferno � Mikky
  NoEvents	= -2;		// "������� ���". ������������ ��� ������� ����� ��������� �������
  DeleteNotify	= 1001;		// "���� ����� ������� ����" - ������ �� �������� ���������
  CurrentWindow : TWindow = 0;	// ������������� �������� ���� (����� �����). ������������ ��������� � DeleteNotify
  WndBorder	= 2;		// ����� ������ ���� � �������
  MaxCharsFormat= 65535;	// ������������ ������ ���������� format
  PathSpliter	= '/';		// ����������� ������� ����

{$i keysymdef.inc}

var
  prDisplay	: PDisplay;	// ��������� �� ��������� Display
  prScreen	: PScreen;
  GetMaxX, GetMaxY,
  nScreenNum,			// ����� ������
  ColorDepth	: LongInt;	// ������� ������� �����
  UserIDn	: Cardinal;	// ������������� ������������ (��������)
  UserIDs	: String;	// ������������� ������������ (���������)
  HomeDir	: String;	// �������� ���������� ������������
  ProcessIDn	: Cardinal;	// ������������� �������� (��������)
  ProcessIDs	: String;	// ����, �� � ��������� ����
  ProgramName	: String;	// paramstr(0) ��� ����

//  KeyInRes	: Array[0..1] of Byte absolute KeyInResult;

// ����� ����
procedure NewWindow(const pwnd: TWindow; const x0, y0, xl, yl, xm, ym: Integer; const h, i: AnsiString; var wnd: TWindow);
procedure NewRootWindow(const x0, y0, xl, yl, xm, ym: Integer; const h, i: AnsiString; var wnd: TWindow);
procedure NewRootWindow(const x0, y0, xl, yl: Integer; const h: AnsiString; var wnd: TWindow);

{
// ������ ��������� ��� ���������� ����
procedure SetWMHints(
  prDisplay	: PDisplay;	// ��������� �� ��������� Display
  nWnd		: TWindow;	// ������������� ����
  MinXl, MinYl	: Integer;	// ���������
  Title,
  IconTitle	: String);	// ����������
}

// ������� ���� � ���������� � ������� ��� ��������
procedure MessageBox(const s: AnsiString);

// ������������� ���� ��� GC
//procedure SetFont(var gc: TGC; const font: PChar);

// ���������� ���������� ����������
procedure SetKeyBoardLight(const AndData, OrData, XorData: Cardinal);

// �������� ���������� ��������� �� m �����������
procedure Delay(const n: Cardinal);

// �������� � log �������
procedure AddLog(const s: String);

// ��������� ����/������� (������ unix'���� �������)
function GetDataTimeStr: PChar;
function GetDataTimeNum: T_tm_DateTime;
function GetDataTimeSec: LongInt;

// �������������� ����� � ������. Exp-������ � �������� ���������� ����� � ����� �������
function ValToStrE(const v: Extended; const n: LongInt): String;

// �������������� ����� � ������. ������� Str
function ValToStrGExt(const v: Extended; const n1, n2: LongInt): String;
function ValToStrGExt(const v: Extended; const n1: LongInt): String;
function ValToStrGExt(const v: Extended): String;
function ValToStrG(const v: LongInt;  const n1: LongInt): String;
function ValToStrG(const v: LongInt): String;

// �������������� ����� � ������. ������� ������� ����������� � ���������� �������, ���� �� ���������� - exp
function ValToStrS(v: Extended; n: LongInt): String;

// No comments
function snprintf(buf: PChar; const max: LongInt; const fmt: pchar; args: array of const): LongInt; CDecl; External;

// ��������� ������� �������
function TestMKey(const Key: TKeys): Boolean;

// ���� �����
function sign(const v: Extended): Extended;
function sign(const v: LongInt): LongInt;

implementation
const
  DescKeys	: Array[TKeys] of Record
    k: TKeySym; s: Boolean;
  end = (
    (k: XK_Shift_L; s: False),
    (k: XK_Control_L; s: False),
    (k: XK_Alt_L; s: False),
    (k: XK_Alt_R; s: False),
    (k: XK_Control_R; s: False),
    (k: XK_Shift_R; s: False)
  );
  MakeRootWindowEn	: Boolean = False;

var
  OldExitProc			: Pointer;
  WMProtocolDELETE_WINDOW,
  WMProtocols			: TAtom;

function TestMKey(const Key: TKeys): Boolean;
begin TestMKey := DescKeys[Key].s end;


procedure InfernoBG(var Event: TXEvent; const BlockEn: Boolean);
const s: PChar = '0123456789';
var k: TKeys; KeySym: TKeySym;
begin
  if BlockEn or (XPending(prDisplay) <> 0) then begin
    XNextEvent(prDisplay, @Event);
    case Event.EventType of
      KeyPress: begin
        KeyInState := Event.XKey.State;
        KeyInResult := XKeycodeToKeysym(prDisplay, char(Event.XKey.KeyCode), 0 { KeyInState } );
        For k := Low(TKeys) to High(TKeys) do if KeyInResult = DescKeys[k].k then DescKeys[k].s := True;
	if KeyInResult = XK_Break then AddLog('Inferno: User-break signal detected...');
	XLookupString(@Event.XKey, s, SizeOf(s), @KeySym, Nil);
	if (s[0] <= #255) and (s[0] >= #32) and (s[0] <> #127) then KeyInResult := ord(s[0]);
//	writeln('"',s, '", ', KeyInResult, ' ', ord(s[0]));
	if monoKeyIn then begin monoKeyInRes := KeyInResult; KeyInResult := 0; monoKeyIn := False end;
      end;
      KeyRelease: begin
        KeyInState := Event.XKey.State;
        KeyInReleased := XKeycodeToKeysym(prDisplay, char(Event.XKey.KeyCode), 0 {KeyInState});
        For k := Low(TKeys) to High(TKeys) do if KeyInReleased = DescKeys[k].k then DescKeys[k].s := False;
      end;
      ClientMessage:
        if Event.XClient.message_type = WMProtocols then
          if Event.XClient.data.l[0] = WMProtocolDELETE_WINDOW then Event.EventType := DeleteNotify;
      FocusIn: CurrentWindow := Event.XFocus.Window;
//      FocusOut: CurrentWindow := 0; // ��� �������� ����� ��������� ���������� WM �������� �����
    end;
  end else Event.EventType := NoEvents;
end;


procedure FlTmWithPause(var Event: TXEvent; const BlockEn: Boolean);
begin
  FullingTime(Event, BlockEn);
  if KeyInResult = XK_Pause then MessageBox('User-pause detected...');
end;


procedure SetKeyBoardLight(const AndData, OrData, XorData: Cardinal);
const CurState: Cardinal = 0;
var XKeyboardControl: TXKeyboardControl;
begin exit;
  With XKeyboardControl do begin
    CurState:=((CurState and AndData) or OrData) xor XorData;
    led:=CurState xor $ff;
    led_mode:=LedModeOff;
    XChangeKeyboardControl(prDisplay, KBLed or KBLedMode, @XKeyboardControl);
    led:=CurState;
    led_mode:=LedModeOn;
    XChangeKeyboardControl(prDisplay, KBLed or KBLedMode, @XKeyboardControl);
  end;
end;


function time(var tloc: LongInt): LongInt; CDecl; External;
function ctime(var clock: LongInt): PChar; CDecl; External;
function localtime(var clock: LongInt): T_tm_DateTime; CDecl; External;

function GetDataTimeStr: PChar;
var clock: LongInt;
begin
  time(clock); GetDataTimeStr:=ctime(clock);
  GetDataTimeStr[pos(#10, GetDataTimeStr)-1]:=#0;
end;

function GetDataTimeNum: T_tm_DateTime;
var clock: LongInt;
begin
  time(clock); GetDataTimeNum:=localtime(clock);
end;

function GetDataTimeSec: LongInt;
begin time(GetDataTimeSec) end;


function ValToStrE(const v: Extended; const n: LongInt): String;
var i, l: LongInt;
begin
  str(v: n and 127, ValToStrE);
  l := Length(ValToStrE) - 3;
  for i := 1 to 3 do if ValToStrE[l] = '0' then delete(ValToStrE, l, 1);
end;


function ValToStrGExt(const v: Extended; const n1, n2: LongInt): String;
begin str(v: n1: n2, ValToStrGExt) end;

function ValToStrGExt(const v: Extended; const n1: LongInt): String;
begin str(v: n1, ValToStrGExt) end;

function ValToStrGExt(const v: Extended): String;
begin str(v, ValToStrGExt) end;

function ValToStrG(const v: LongInt;  const n1: LongInt): String;
begin str(v: n1, ValToStrG) end;

function ValToStrG(const v: LongInt): String;
begin str(v, ValToStrG) end;

function ValToStrS(v: Extended; n: LongInt): String;
var
  s	: String;
  PorPos,
  Len,
  SIPok	: LongInt;
  STRPok: String[6];

begin
  if abs(v) < 1e-153 then v:=0; // ���� ���������� ���������� (����� �� ������ ������)
  str(v, s);
  PorPos := Pos('E', s) + 1;
  STRPok := copy(s, PorPos, 255); SetLength(s, PorPos - 2);
  val(STRPok, SIPok);
  if v < 0 then begin ValToStrS := '-'; Len := 2 end else begin ValToStrS := ''; Len := 1 end;
  if SIPok < 0 then begin Inc(Len, 2-SIPok-1); ValToStrS := ValToStrS + '0.' end else Inc(Len, SIPok);
  if Len <= n then begin
    // ������� ����������� � ���������� �����
    if SIPok < 0 then begin
      for Len := 1 to -1-SIPok do ValToStrS := ValToStrS + '0';
      if n > PorPos - 2 then n := PorPos - 2;
      ValToStrS := ValToStrS + s[2] + copy(s, 4, n-length(ValToStrS)-1);
    end else begin
      ValToStrS := ValToStrS + s[2] + copy(s, 4, SIPok); Len := Length(ValToStrS);
      if Len + 1 < n then ValToStrS := ValToStrS + '.' + copy(s, 4+SIPok, n-Len-1);
    end;
  end else begin
    // �������� ����������� ������������� � ��������������� �����
    ValToStrS := s[2] + 'e' + STRPok;
    if v < 0 then ValToStrS := '-' + ValToStrS; Len := length(ValToStrS);
    if Len+1 < n then insert(copy(s, 3, n-Len), ValToStrS, pos('e', ValToStrS));
    if Len > n then ValToStrS := '??';      
  end;
end;


function sign(const v: Extended): Extended;	begin if v >= 0 then sign := 1 else sign := -1 end;
function sign(const v: LongInt): LongInt;	begin if v >= 0 then sign := 1 else sign := -1 end;


function usleep(const n: Cardinal): LongInt; CDecl; External;

procedure Delay(const n: Cardinal);
begin usleep(n) end;


procedure AddLog(const s: String);
{$ifndef nologfile}
var ftt: text; io: integer;
{$endif}
begin
{$ifndef nologfile}
{$I-}
  io:=IOResult;
  Assign(ftt, ParamStr(0)+'.log'); Append(ftt);
  io:=IOResult;
{$I+}
  if io<>0 then
    if io=2 then
      rewrite(ftt)
    else begin
      writeln('Error # ', io, ' at time open log file...');
      halt(255);
    end;

  writeln(ftt, GetDataTimeStr, ' : ', s);

  if s[1]='.' then writeln(ftt);
  close(ftt);
{$endif}
  Writeln('AddLog: '+s);
  if s[1]<>':' then begin
    // ���� ExitCode = 0 ������ ��������� �������� ���������������� ������� � ����� ��������� ������
    // ����� ��� ��������, ��� ����� AddLog ��������� �� ��������� ShutDown
    if ExitCode = 0 then halt; { halt ������� exitcode � erroraddr... }
  end;
end;


procedure NewWindow(const pwnd: TWindow; const x0, y0, xl, yl, xm, ym: Integer; const h, i: AnsiString; var wnd: TWindow);
var
  mask		: LongInt;
  rSizeHints	: TXSizeHints;
  rWMHints	: TXWMHints;
  rClassHint	: TXClassHint;
  prWindowName,
  prIconName	: TXTextProperty;
  ph, pi	: PChar;

begin
  wnd := XCreateSimpleWindow(
    prDisplay, pwnd,
    x0, y0, xl, yl, WndBorder,
    XWhitePixel(prDisplay, nScreenNum),
    XBlackPixel(prDisplay, nScreenNum));

  if MakeRootWindowEn then begin
    ph := @(h[1]); pi := @(i[1]);
    if (XStringListToTextProperty(@ph, 1, @prWindowName) = 0) or
       (XStringListToTextProperty(@pi, 1, @prIconName  ) = 0) then begin
      AddLog('. Inferno: No memory in SetWMHints !');
    end;

    With rSizeHints do if (xm = 0) and (ym = 0) then begin
      flags	:= PPosition or PSize or PMinSize or PMaxSize;
      min_width	:= xl;
      min_height:= yl;
      max_width	:= xl;
      max_height:= yl;

    end else begin
      flags	:= PPosition or PSize or PMinSize;
      min_width	:= xm;
      min_height:= ym;
    end;
    With rWMHints do begin
      flags	:= StateHint or IconPixmapHint or InputHint;
      initial_state:=NormalState;
      Input	:= True;
      icon_pixmap:= 0;
    end;
    With rClassHint do begin
      res_name	:= argv[0];
      res_class	:= prgClass;
    end;

    XSetWMProperties(prDisplay, Wnd, @prWindowName, @prIconName, argv, argc, @rSizeHints, @rWMHints, @rClassHint);
  end;

  XMapWindow(prDisplay, Wnd);

  mask := 
    ExposureMask or
    KeyPressMask or KeyReleaseMask or
    ButtonPressMask or ButtonReleaseMask or
    PointerMotionMask or
    EnterWindowMask or LeaveWindowMask;
  if MakeRootWindowEn then mask := mask or StructureNotifyMask or FocusChangeMask;

  XSelectInput(prDisplay, Wnd, mask);    

  if MakeRootWindowEn then XSetWMProtocols(prDisplay, Wnd, @WMProtocolDELETE_WINDOW, 1);

  MakeRootWindowEn := False;
end;

procedure NewRootWindow(const x0, y0, xl, yl: Integer; const h: AnsiString; var wnd: TWindow);
begin
  MakeRootWindowEn := True;
  NewWindow(XRootWindow(prDisplay, nScreenNum), x0, y0, xl, yl, 0, 0, h, h, wnd);    
end;

procedure NewRootWindow(const x0, y0, xl, yl, xm, ym: Integer; const h, i: AnsiString; var wnd: TWindow);
begin
  MakeRootWindowEn := True;
  NewWindow(XRootWindow(prDisplay, nScreenNum), x0, y0, xl, yl, xm, ym, h, i, wnd);
end;

{
procedure SetFont(var gc: TGC; const font: PChar);
var prFontInfo : PXFontStruct;
begin
  prFontInfo := XLoadQueryFont(prDisplay, font);
  if (prFontInfo = NIL) then
    AddLog('. Inferno: Font "' + font + '" not found !')
  else
    XSetFont(prDisplay, gc, prFontInfo^.fid);
end;
}

procedure MessageBox(const s: AnsiString);
const p = '������� ����� �������'; xl = 200; yl = 40;
var Wnd: TWindow; GC: TGC; Event: TXEvent; x, y: LongInt;
begin
  x := (GetMaxX - xl) div 2; y := (GetMaxY - yl) div 2;
  NewRootWindow(x, y, xl, yl, 1, 1, '�����', '�������� ����� ���������', Wnd);
  GC := XCreateGC(prDisplay, Wnd, 0, NIL);
  XSetForeGround(prDisplay, GC, XBlackPixel(prDisplay, nScreenNum));
  XSetBackGround(prDisplay, GC, XWhitePixel(prDisplay, nScreenNum));
  Repeat
    FullingTime(Event, True);
    XDrawImageString(prDisplay, Wnd, GC, 10, 16, @(s[1]), Length(s));
    XDrawImageString(prDisplay, Wnd, GC, 10, 32, p, Length(p));
  Until Event.EventType = KeyPress;
  XFreeGC(prDisplay, GC);
  XDestroyWindow(prDisplay, Wnd);
  KeyInResult := 0;
end;


Procedure ShutDown;
begin
  ExitProc:=OldExitProc;
  if ExitCode=0 then
    AddLog('. Inferno: Halt. Zero ExitCode.')
  else
    AddLog('. Inferno: ' + ErrMsg(ExitCode) + '.');
  XCloseDisplay(prDisplay);
end;


type
  tpasswd = Record
    pw_name, pw_passwd	: PChar;
    pw_uid, pw_gid	: Cardinal;
    pw_change		: Cardinal;
    pw_class, pw_gecos,
    pw_dir, pw_shell	: PChar;
    pw_expire		: Cardinal;
    pw_fields		: LongInt;
  End;
  ppasswd = ^tpasswd;
  
function getuid: Cardinal; CDecl; External;
function getpwuid(uid: Cardinal): ppasswd; CDecl; External;
function getpid: Cardinal; CDecl; External;
function setlocale(category: LongInt; const locale: PChar): PChar; CDecl; External;

procedure Init;
const LC_ALL = 0;
var passwd: ppasswd;
begin
  // ������� ��� XLookup
  setlocale(LC_ALL, '');

  // ��������, ��� �� � ��� ��
  UserIDn := getuid;
  passwd := getpwuid(UserIDn);
  With passwd^ do begin
    UserIDs := pw_name;
    HomeDir := pw_dir;
  end;

  ProcessIDn := getpid;
  ProcessIDs := ValToStrG(ProcessIDn);

  ProgramName := ParamStr(0);
  While Pos(PathSpliter, ProgramName) <> 0 do delete(ProgramName, 1, Pos(PathSpliter, ProgramName));

  // ������������� ����� � ��������
  prDisplay := XOpenDisplay(nil);
  if prDisplay = nil then AddLog('. Inferno: Can not connect to the X server !');

  // �������� ����� ��������� ������
  nScreenNum := XDefaultScreen(prDisplay);
  prScreen := XScreenOfDisplay(prDisplay, nScreenNum);

  // �������� ������� �����
  ColorDepth := XDefaultDepth(prDisplay, nScreenNum);

  GetMaxX := XWidthOfScreen(prScreen); GetMaxY := XHeightOfScreen(prScreen);

  // �������������� � ������� ����������, ����� �� ������� � ������� �������� ����
  WMProtocolDELETE_WINDOW := XInternAtom (prDisplay, 'WM_DELETE_WINDOW', True);
  WMProtocols		  := XInternAtom (prDisplay, 'WM_PROTOCOLS',	 True);

  OldExitProc:=ExitProc; ExitProc:=Addr(ShutDown);

  AddLog(': Inferno: Start program file '+paramstr(0));
end;


begin
  Init
end.
