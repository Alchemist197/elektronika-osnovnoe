unit alf;

interface
uses Inferno, x, xlib, VGAColors, Ini;

Const	
  AlfX		: LongInt = 0;		// ���������� ��� ������ ����������� ������ ����������� �����
  AlfY		: LongInt = 0;
  AlfPrevX	: LongInt = 0;		// ��������� ������ ������. ������������ jkl, ��������������� TextOutIntroFontXY
  AlfCenter	: Boolean = False;	// ����� ������ ���� �� ������
  AlfMaxLen	= 42;			// ������������ ����� �������� ������ ��� EnterString
  AlfLF		= '\';			// ����������� ����� ������
  AlfPT		= #1;			// ��������� ������� ������� ��� EnterString

Type
  AlfTStr	= String[AlfMaxLen];	// ����������, �������� EnterString

// ��������� ������� �����
procedure AlfSetFont(var gc: TGC; const NewName: AnsiString);

// ��������� ������� �����. �� ������������ ����������� ��������, ������������� ����������� ������� Height � Weigth
procedure AlfFakeSetFont(const NewName: AnsiString);

// ������ � ������ ������
function AlfHeight(const s: AnsiString): LongInt;
function AlfHeightPS(const s: AnsiString): LongInt;
function AlfWidth(const s: AnsiString): LongInt;
function AlfWidthPS(const s: AnsiString): LongInt;

// ����� ������ ���������� ������ � ����������� x, y � ������ AlfCenter (TRUE: x ������������� ������ ������)
procedure AlfOutTextXY(const WND: TDrawable; const GC: TGC; const x, y: LongInt; const s: AnsiString);

// ����� ������ ���������� ������ � ����������� x, y � ���������� PS � ������ AlfCenter (TRUE: ����� ���������� ����� x1 .. x2)
procedure AlfOutTextXYPS(const WND: TDrawable; const GC: TGC; x1, x2, y: LongInt; const s: AnsiString);

// ���������� ����� ������
procedure AlfOutText(const WND: TDrawable; const GC: TGC; const s: AnsiString);

// --""-- � ������ ��������� ������
procedure jkl(const WND: TDrawable; const GC: TGC; const s: AnsiString);

// ������� ��������� � ����������� x, y � ���������� �� x � ��������������� ��������
//procedure AlfOutTextCT(x, y: LongInt; s: AnsiString);

// ������� ��������� � ����������� x, y � ���������� �� y � ��������������� ��������
//procedure AlfOutTextLC(x, y: LongInt; s: AnsiString);

// ���� ������
function AlfEnterString(Var GBuf: AlfTStr; Const Prompt: String; x, y: LongInt): Boolean;

// ���� �����
function AlfEnterValue(Var Value: LongInt; Const Prompt: String; Const x, y: LongInt): Boolean;

implementation

const
  FontInfo	: PXFontStruct = Nil;	// ������� ����
  FontName	: AnsiString = '';	// ��� �������� �����
  AlfWD		= '=';			// �� ����� ������� EnterSting ��������� ������ ������ ����
  AlfEFont	: AnsiString = '-*-*-medium-r-*-*-*-120-*-*-*-*-koi8-*'; // ����, ������������ � EnterString

procedure AlfFakeSetFont(const NewName: AnsiString);
begin
  if FontName <> NewName then begin
    if FontInfo <> Nil then XFreeFont(prDisplay, FontInfo);
    FontInfo := XLoadQueryFont(prDisplay, @(NewName[1]));
    if FontInfo = Nil then begin
      addlog(': Font "' + NewName + '" not found ! Attempt "*" font...');
      FontInfo := XLoadQueryFont(prDisplay, '*');
      if FontInfo = Nil then addlog('Default font "*" not found !!!');
    end;
  end;
end;


procedure AlfSetFont(var gc: TGC; const NewName: AnsiString);
begin
  AlfFakeSetFont(NewName);
  XSetFont(prDisplay, gc, FontInfo^.fid);
end;


function AlfHeight(const s: AnsiString): LongInt;
var CharStruct: TXCharStruct; dir, asc, des: LongInt;
begin
  if s = '' then AlfHeight := 0 else begin
    XTextExtents(FontInfo, @(s[1]), Length(s), @dir, @asc, @des, @CharStruct);
    AlfHeight := asc + des; // ��� ������ ��� ����� ������
  end;
  // � ��� ������� ���������� ������ ����� ���������� ������������ CharStruct
end;


function AlfHeightPS(const s: AnsiString): LongInt;
var p1, p2: LongInt;
begin
  p1 := 1; for p2 := 1 to Length(s) do if s[p2] = AlfLF then inc(p1);
  AlfHeightPS := AlfHeight(s) * p1;
end;


function AlfWidth(const s: AnsiString): LongInt;
begin
  if s = '' then AlfWidth := 0 else AlfWidth := XTextWidth(FontInfo, @(s[1]), Length(s));
end;


function AlfWidthPS(const s: AnsiString): LongInt;
var p1, p2, l, w, f: LongInt;
begin
  p1 := 1; l := Length(s); AlfWidthPS := 0;
  while p1 <= l do begin
    p2 := p1;
    while (p2 <= l) and (s[p2] <> AlfLF) do inc(p2);
    f := p2 - p1;
    if f > 0 then begin
      w := XTextWidth(FontInfo, @(s[p1]), f);
      if AlfWidthPS < w then AlfWidthPS := w;
    end;
    p1 := p2 + 1;
  end;
end;


function AlfEnterString(Var GBuf: AlfTStr; Const Prompt: String; x, y: LongInt): Boolean;
const
  Border	= 5;
  Curs		: Byte = AlfMaxLen;
  InExecute	: Boolean = False; // ������������� ��������� ���� ����� ������� ����������

var
  RBuf		: AlfTStr;
  Modif, Ex	: boolean;

  Wnd		: TWindow;
  GC		: TGC;
  Event		: TXEvent; 

  Procedure DrawAll;
  var o, s, xl, yl: LongInt;
  begin
    AlfSetFont(GC, AlfEFont);
    xl := AlfWidth(Prompt + RBuf) + Border * 2;
    yl := AlfHeight(AlfWD) + Border * 2;
    if Modif then begin
      XSetForeGround(prDisplay, GC, XWhitePixel(prDisplay, nScreenNum));
      XSetBackGround(prDisplay, GC, XBlackPixel(prDisplay, nScreenNum));
    end else begin
      XSetForeGround(prDisplay, GC, XBlackPixel(prDisplay, nScreenNum));
      XSetBackGround(prDisplay, GC, XWhitePixel(prDisplay, nScreenNum));
    end;
//    XMoveResizeWindow(prDisplay, Wnd, x - (xl div 2), y - (yl div 2), xl, yl);
    XResizeWindow(prDisplay, Wnd, xl, yl);
    XClearWindow(prDisplay, Wnd);
    AlfOutTextXY(Wnd, GC, Border, Border, Prompt + RBuf);
    o := AlfWidth(Prompt + Copy(RBuf, 1, Curs));
    if Curs >= Length(RBuf) then s := AlfWidth(AlfWD) else s := AlfWidth(RBuf[Curs+1]);
    XDrawRectangle(prDisplay, Wnd, GC, o+Border, yl-Border, s, Border-1);
    XDrawLine(prDisplay, Wnd, GC, o+Border, 0, o+Border, yl);
  end;

begin
  if InExecute then exit(True); // �����������������, �������, ���������, �� �� �� ����� �� ������� :)
  InExecute := True;
  AlfCenter := False;
  NewRootWindow(x, y, 1, 1, 1, 1, Prompt, '������', Wnd);
  GC := XCreateGC(prDisplay, Wnd, 0, NIL);

  RBuf := GBuf;
  if pos(AlfPT, RBuf) <> 0 then begin
    Curs := pos(AlfPT, RBuf);
    Delete(RBuf, Curs, 1);
  end;
  if Curs > Length(RBuf) then Curs := Length(RBuf);

  Modif := False; Ex := False;

  Repeat
    monoKeyIn := True;
    FullingTime(Event, True);
    case Event.EventType of
      Expose: DrawAll;
      DeleteNotify: if Wnd = CurrentWindow then Ex := True;
      KeyPress: begin
	if monoKeyInRes > 255 then case monoKeyInRes of
	  XK_Shift_L,
	  XK_Shift_R: Continue; // ����� �� �������������� Modif
	  XK_Escape,
	  XK_Return: Ex := True;
	  XK_Delete: if Curs < Length(RBuf) then Delete(RBuf, Curs+1, 1);
	  XK_Home  : Curs := 0;
	  XK_End   : Curs := Length(RBuf);
	  XK_Left  : if Curs > 0 then Dec(Curs);
	  XK_Right : if Curs < Length(RBuf) then Inc(Curs);
	  XK_BackSpace: if Curs <> 0 then begin Delete(RBuf, Curs, 1); Dec(Curs); end;
	end else if (Curs <> AlfMaxLen) and (monoKeyInRes > 31) then begin
	  if Modif then begin
	    Inc(Curs); Insert(char(monoKeyInRes), RBuf, Curs);
	  end else begin
	    Curs:=1; RBuf:=char(monoKeyInRes);	    
	  end;
	end;
	Modif := True;
	DrawAll;
      end;
    end;
  Until Ex;

  XFreeGC(prDisplay, GC);
  XDestroyWindow(prDisplay, Wnd);
  AlfEnterString := monoKeyInRes <> XK_Return;
  if AlfEnterString then else GBuf := RBuf;
  monoKeyInRes := 0;
  InExecute := False;
end;


function AlfEnterValue(Var Value: LongInt; Const Prompt: String; Const x, y: LongInt): Boolean;
var s: AlfTStr; i, d: LongInt;
begin
  s := ValToStrG(Value);
  repeat
    AlfEnterValue := AlfEnterString(s, Prompt, x, y);
    if AlfEnterValue then Exit;
    val(s, d, i);
    if i <> 0 then insert(AlfPT, s, i);
  until (i = 0) or AlfEnterValue;
  Value := d;
end;


procedure AlfOutText(const WND: TDrawable; const GC: TGC; const s: AnsiString);
var CharStruct: TXCharStruct; dir, asc, des: LongInt;
begin
  if s <> '' then begin
    XTextExtents(FontInfo, @(s[1]), Length(s), @dir, @asc, @des, @CharStruct);
    XDrawImageString(prDisplay, WND, GC, AlfX, asc + AlfY, @(s[1]), Length(s));
    inc(AlfX, AlfWidth(s));
  end;
end;


procedure AlfOutTextXY(const WND: TDrawable; const GC: TGC; const x, y: LongInt; const s: AnsiString);
begin
  AlfPrevX := x;
  if AlfCenter then AlfX := x - (AlfWidth(s) div 2) else AlfX := x;
  AlfY := y;
  AlfOutText(WND, GC, s);
end;


procedure AlfOutTextXYPS(const WND: TDrawable; const GC: TGC; x1, x2, y: LongInt; const s: AnsiString);
var p1, p2, x, l: LongInt;
begin
  l := Length(s);
  if AlfCenter then begin
    x := ((x2-x1) div 2) + x1;
    dec(y, AlfHeightPS(s) div 2);
  end else x := x1;
  p1 := 1;
  while p1 <= l do begin
    p2 := p1;
    while (p2 <= l) and (s[p2] <> AlfLF) do inc(p2);
    AlfOutTextXY(WND, GC, x, y, Copy(s, p1, p2-p1));
    Inc(y, AlfHeight(s));
    p1 := p2 + 1;
  end;
end;


procedure jkl(const WND: TDrawable; const GC: TGC; const s: AnsiString);
begin
  AlfX:=AlfPrevX;
  Inc(AlfY, AlfHeightPS(s));
  AlfOutText(WND, GC, s);
end;

begin
  IniOpen('Alf');
  IniGet('FontEnterString', AlfEFont);
  IniClose;
end.
