unit Error;

interface

function ErrMsg(const n: Byte): String;

implementation

function ErrMsg(const n: Byte): String;
var c: String[3];
begin
str(n:3, c);
case n of
1..79:	  begin
	    ErrMsg:='DOS error: ';
	    case n of
	    1: ErrMsg:=ErrMsg+'Invalid function number';
	    2: ErrMsg:=ErrMsg+'File not found';
	    3: ErrMsg:=ErrMsg+'Path not found';
	    4: ErrMsg:=ErrMsg+'Too many files';
	    5: ErrMsg:=ErrMsg+'File access denied';
	    6: ErrMsg:=ErrMsg+'Invalid file handle';
	    12: ErrMsg:=ErrMsg+'Invalid file access code';
	    15: ErrMsg:=ErrMsg+'Invalid drive number';
	    16: ErrMsg:=ErrMsg+'Cannot remove current directory';
	    17: ErrMsg:=ErrMsg+'Cannot rename across drivers';
	    18: ErrMsg:=ErrMsg+'No more files';
	     else
	      ErrMsg:=ErrMsg+'unknown # '+c;
	    end;
	  end;
100..139: begin
	    ErrMsg:='I/O error: ';
	    case n of
	    100: ErrMsg:=ErrMsg+'Disk read error';
	    101: ErrMsg:=ErrMsg+'Disk write error. Maybe, no free space';
	    102: ErrMsg:=ErrMsg+'File not assigned';
	    103: ErrMsg:=ErrMsg+'File not open';
	    104: ErrMsg:=ErrMsg+'File not open for input';
	    105: ErrMsg:=ErrMsg+'File not open for output';
	    106: ErrMsg:=ErrMsg+'Invalid numeric format';
	     else
	      ErrMsg:=ErrMsg+'unknown # '+c;
	    end;
	  end;
150..199: begin
	    ErrMsg:='Crytical system error: ';
	    case n of
	    150: ErrMsg:=ErrMsg+'Disk is write protected';
	    151: ErrMsg:=ErrMsg+'Unknown unit';
	    152: ErrMsg:=ErrMsg+'Drive not ready';
	    153: ErrMsg:=ErrMsg+'Unknown command';
	    154: ErrMsg:=ErrMsg+'CRC error in data';
	    155: ErrMsg:=ErrMsg+'Bad drive reguest structure length';
	    156: ErrMsg:=ErrMsg+'Disk seek error';
	    157: ErrMsg:=ErrMsg+'Unknown media type';
	    158: ErrMsg:=ErrMsg+'Sector not found';
	    159: ErrMsg:=ErrMsg+'Printer out of paper';
	    160: ErrMsg:=ErrMsg+'Device write fault';
	    161: ErrMsg:=ErrMsg+'Device read fault';
	    162: ErrMsg:=ErrMsg+'Hardware failure';
	     else
	      ErrMsg:=ErrMsg+'unknown # '+c;
	    end;
	  end;
200..239: begin
	    ErrMsg:='Fatal program error: ';
	    case n of
	    200: ErrMsg:=ErrMsg+'Divizion by zero';
	    201: ErrMsg:=ErrMsg+'Range check';
	    202: ErrMsg:=ErrMsg+'Stack overflow';
	    203: ErrMsg:=ErrMsg+'Heap overflow error';
	    204: ErrMsg:=ErrMsg+'Invalid pointer operation';
	    205: ErrMsg:=ErrMsg+'Floating point operation';
	    206: ErrMsg:=ErrMsg+'Floatiog point underflow';
	    207: ErrMsg:=ErrMsg+'Invalid floating point operation';
	    208: ErrMsg:=ErrMsg+'Overlay manager not installed';
	    209: ErrMsg:=ErrMsg+'Overlay file read error';
	    210..214: ErrMsg:=ErrMsg+'Objects error';
	    215: ErrMsg:=ErrMsg+'Arithmetic overflow';
	    216: ErrMsg:=ErrMsg+'General protection fault';
	     else
	      ErrMsg:=ErrMsg+'unknown # '+c;
	    end;
	  end;
80..99,
140..149,
240..255: ErrMsg:='Unknow # '+c;
0:        ErrMsg:='Your program correct halted';
end;
end;

end.
