{
  $Id: x11.pp,v 1.1 2003/01/20 18:53:01 cc Exp $
}
Unit X11;

{$MODE objfpc}

interface
uses
  x, xlib, XUtil;

{$LinkLib c}
{$LinkLib X11}

type
    PPwchar_t=^Pwchar_t;
    PPPwchar_t=^PPwchar_t;


function XGetTextProperty(display: PDisplay; w: TWindow; 
    text_prop_return: PXTextProperty; prop: TAtom): TStatus; cdecl; external;

function XGetVisualInfo(display: PDisplay; vinfo_mask: longint;
    vinfo_template: PXVisualInfo; nitems_return: PInteger): 
    PXVisualInfo; cdecl; external;

function XGetWMClientMachine(display: PDisplay; w: TWindow; 
    text_prop_return: PXTextProperty): TStatus; cdecl; external;

function XGetWMHints(display: PDisplay; w: TWindow): PXWMHints; cdecl; external;

function XGetWMIconName(display: PDisplay; w: TWindow; 
    text_prop_return: PXTextProperty): TStatus; cdecl; external;

function XGetWMName(display: PDisplay; w: TWindow; 
    text_prop_return: PXTextProperty): TStatus; cdecl; external;

function XGetWMSizeHints(display: PDisplay; w: TWindow; 
    hints_return: PXSizeHints; supplied_return: PLongInt;
    AProperty: TAtom): TStatus; cdecl; external;

function XGetZoomHints(display: PDisplay; w: TWindow; 
    hints_return: PXSizeHints): TStatus; cdecl; external;

procedure XConvertCase(sym: TKeySym; lower, upper: PKeySym); cdecl; external;

function XMatchVisualInfo(display: PDisplay; screen, depth, aclass: integer; 
    vinfo_return: PXVisualInfo): TStatus ; cdecl; external;

function XOffsetRegion(r: TRegion; dx, dy: integer): integer; cdecl; external;

function XPointInRegion(r: TRegion; dx, dy: integer): boolean; cdecl; external;

function XPolygonRegion(points: TXPoint; n, fill_rule: integer): 
    TRegion; cdecl; external;

function XRectInRegion(r: TRegion; x, y: integer;
    width, height: word): longint; cdecl; external;

function XSaveContext(display: PDisplay; rid: TXID; context: TXContext;
    data: PChar): longint; cdecl; external;

function XSetClassHint(display: PDisplay; w: TWindow; 
    class_hints: PXClassHint): integer; cdecl; external;

function XSetIconSizes(display: PDisplay; w: TWindow; 
     size_list: PXIconSize; count: integer): integer; cdecl; external; 

procedure XSetRGBColormaps(display: PDisplay; w: TWindow; 
    stdcmaps: PXStandardColormap; count: integer; 
    AProperty: TAtom); cdecl; external;

function XSetSizeHints(display: PDisplay; w: TWindow; 
    hints: PXSizeHints; AProperty: TAtom): integer; cdecl; external;

procedure XSetWMProperties(display: PDisplay; w: TWindow; 
    window_name, icon_name: PXTextProperty; argv: PPChar;
    argc: integer; normal_hints: PXSizeHints; wm_hints: PXWMHints;
    class_hints: PXClassHint); cdecl; external;

procedure XmbSetWMProperties(display: PDisplay; w: TWindow; 
    window_name, icon_name: PChar; argv: PPChar;
    argc: integer; normal_hints: PXSizeHints; wm_hints: PXWMHints;
    class_hints: PXClassHint); cdecl; external;

procedure XSetWMSizeHints(display: PDisplay; w: TWindow; 
    hints: PXSizeHints; AProperty: TAtom); cdecl; external;

procedure XSetStandardColormap(display: PDisplay; w: TWindow; 
    colormap: PXStandardColormap; AProperty: TAtom); cdecl; external;

function XSetZoomHints(display: PDisplay; w: TWindow; 
    zhints: PXSizeHints): integer; cdecl; external;

function XStringListToTextProperty(list: PPChar; count: integer; 
    text_prop_return: PXTextProperty): TStatus; cdecl; external;

function XmbTextListToTextProperty(display: PDisplay; list: PPChar; 
    count: integer; style: TXICCEncodingStyle; 
    text_prop_return: PXTextProperty): longint; cdecl; external;

function XwcTextListToTextProperty(display: PDisplay; list: PPwchar_t; 
    count: integer; style: TXICCEncodingStyle; 
    text_prop_return: PXTextProperty): longint; cdecl; external;

procedure XwcFreeStringList(list: PPwchar_t); cdecl; external;

function XTextPropertyToStringList(text_prop: PXTextProperty; list_return: PPPChar;
    count_return: PInteger): TStatus; cdecl; external;

function XmbTextPropertyToTextList(display: PDisplay; 
    text_prop: PXTextProperty; list_return: PPPChar;
    count_return: PInteger): longint; cdecl; external;

function XwcTextPropertyToTextList(display: PDisplay; 
    text_prop: PXTextProperty; list_return: PPPwchar_t;
    count_return: PInteger): longint; cdecl; external;

function XWMGeometry(display: PDisplay; screen_number: integer;
    user_geometry, default_geometry: PChar; border_width: word;
    hints: PXSizeHints; x_return, y_return, width_return, height_return,
    gravity_return: PInteger): longint; cdecl; external;



Implementation

end.
{
  $Log: x11.pp,v $
  Revision 1.1  2003/01/20 18:53:01  cc
  + Initial import
  Revision 1.2  2003/02/20 20:32:45  cc
  + Type bugs with TSizeHints/PSizeHisnts removed
}
