unit VGAColors;

interface
uses Inferno, x, x11, xlib, xutil;

type
  TColors	= Array[0..15] of Cardinal;

Procedure SetVGAPalette(var wnd: TWindow; var Colors: TColors);

implementation

const
   VGAPal	: Array[0..15] of Record R, G, B: Byte End=(
     (R:   0; G:   0; B:   0), // 0
     (R:   0; G:   0; B: 168),
     (R:   0; G: 168; B:   0),
     (R:   0; G: 168; B: 168),
     (R: 168; G:   0; B:   0), // 4
     (R: 168; G:   0; B: 168),
     (R: 168; G:  84; B:   0),
     (R: 168; G: 168; B: 168),
     (R:  84; G:  84; B:  84), // 8
     (R:  84; G:  84; B: 252),
     (R:  84; G: 252; B:  84),
     (R:  84; G: 252; B: 252),
     (R: 252; G:  84; B:  84), // 12
     (R: 252; G:  84; B: 252),
     (R: 252; G: 252; B:  84),
     (R: 252; G: 252; B: 252));


Procedure SetVGAPalette(var wnd: TWindow; var Colors: TColors);
var
  default_visual	: PVisual;
  my_colormap		: TColormap;
  Color			: TXColor;
  rc			: TStatus;
  i			: Byte;

begin
  default_visual := XDefaultVisual(prDisplay, XDefaultScreen(prDisplay));
// my_colormap := XDefaultColormap(prDisplay, XDefaultScreen(prDisplay));
  my_colormap := XCreateColormap(prDisplay, wnd, default_visual, AllocNone);
  For i:=0 to 15 do With VGAPal[i] do begin
    Color.red:=R*256;
    Color.green:=G*256;
    Color.blue:=B*256;
    rc:=XAllocColor(prDisplay, my_colormap, @Color);
    if rc=0 then begin
      WriteLn('VGAColors: XAllocColor - ���� # ', i, ' �������� �� �������.');
      Halt(1);
    end;
    Colors[i]:=Color.pixel;
//    writeln(i:2, rc:10, color.pixel:10);
  end;
end;

end.
