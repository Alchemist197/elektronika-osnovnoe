uses Inferno, VGAColors, X, XLib, X11, XUtil;

const
  AnodSize	= 38;
  AnodStep	= 40;
  AnodBorder	= 1;

  PrevBorder	= 4;

  AnodXMaxNum	= 8-1;
  AnodXNum	: Byte = AnodXMaxNum;
  AnodYNum	= 8-1;

  CursX		: LongInt = 0;
  CursY		: LongInt = 0;
  CurC		: LongInt = 0;
  BlB		: LongInt = 0;
  BlK		: LongInt = 0;

  BitMask	: Array[0..7] of Byte = ($01, $02, $04, $08, $10, $20, $40, $80);

type
  tSym		= Record
    bits	: Array[0..AnodXMaxNum] of byte;// ������� ����������� �������
    en		: Boolean;			// ����������� ������� � asm-e
    wd		: Byte;				// ������ �������
    res		: Array[0..3] of byte;  	// ������ ��� ����������
  end;

  tSymBase	= Array[0..255] of tSym;

var
  f		: File of tSymBase;
  SymBase	: tSymBase;
  WndPrev,
  WndMatr	: TWindow;
  Event		: TXEvent;
  Col		: TColors;
  GC		: TGC;

procedure DrawMatrix;
var x, y: LongInt; b: Byte;

begin With SymBase[CurC] do
  For x := 0 to AnodXNum do begin
    b := bits[x];
    For y := 0 to AnodYNum do begin

      XSetForeGround(prDisplay, GC, Col[15]);
      // SetFillStyle(SolidFill, GetColor);
      XFillRectangle(prDisplay, WndMatr, GC, x*AnodStep, y*AnodStep, AnodSize, AnodSize);

      if odd(b) then XSetForeGround(prDisplay, GC, Col[10]) else XSetForeGround(prDisplay, GC, Col[0]);
      XFillRectangle(prDisplay, WndMatr, GC, x*AnodStep+AnodBorder, y*AnodStep+AnodBorder, AnodSize-AnodBorder*2, AnodSize-AnodBorder*2);

      if (x = CursX) and (y = CursY) then begin
        XSetForeGround(prDisplay, GC, Col[12]);
        XDrawLine(prDisplay, WndMatr, GC, x*AnodStep+AnodBorder, y*AnodStep+AnodBorder, x*AnodStep+AnodBorder + AnodSize-AnodBorder*2, y*AnodStep+AnodBorder + AnodSize-AnodBorder*2);
        XDrawLine(prDisplay, WndMatr, GC, x*AnodStep+AnodBorder + AnodSize-AnodBorder*2, y*AnodStep+AnodBorder, x*AnodStep+AnodBorder, y*AnodStep+AnodBorder + AnodSize-AnodBorder*2);	
      end;

      b := b shr 1;
    end;
  end;
end;

procedure DrawPreview;
var x, y, cx, cy: LongInt; b, C: Byte;

begin
  For C := Low(SymBase) to High(SymBase) do With SymBase[C] do
    For x := 0 to AnodXMaxNum do begin
      if x > wd then b := 0 else b := bits[x];
      For y := 0 to AnodYNum do begin

	cx := (c mod 16) * (PrevBorder + AnodXMaxNum);
	cy := (c div 16) * (PrevBorder + AnodYNum);
{
	if C = CurC then begin
	  XSetForeGround(prDisplay, GC, Col[12]);
	  XDrawRectangle(prDisplay, WndPrev, GC, cx, cy, wd, AnodYNum);
	end;
}
        if odd(b) then if (C >= BlB) and (C <= BlK) then XSetForeGround(prDisplay, GC, Col[12]) else XSetForeGround(prDisplay, GC, Col[10]) else if C = CurC then XSetForeGround(prDisplay, GC, Col[7]) else XSetForeGround(prDisplay, GC, Col[0]);
        XDrawPoint(prDisplay, WndPrev, GC, x + cx, y + cy);

        b := b shr 1;
      end;
      if en then else begin
        XSetForeGround(prDisplay, GC, Col[14]);
	XDrawLine(prDisplay, WndPrev, GC, cx, cy, cx+AnodXMaxNum, cy+AnodYNum);
      end;
    end;
end;

procedure ClearSym;
var x: LongInt;
begin With SymBase[CurC] do
  For x := 0 to AnodXNum do bits[x] := 0;
end;

procedure XorSym;
var x: LongInt;
begin With SymBase[CurC] do
  For x := 0 to AnodXNum do bits[x] := not bits[x];
end;

procedure ScrollUp;
var x: LongInt;
begin With SymBase[CurC] do
  For x := 0 to AnodXNum do bits[x] := bits[x] shr 1;
end;

procedure ScrollDown;
var x: LongInt;
begin With SymBase[CurC] do
  For x := 0 to AnodXNum do bits[x] := (bits[x] shl 1) and $FF;
end;

procedure ScrollLeft;
var t: Byte;
begin With SymBase[CurC] do begin
  t	  := bits[0];
  bits[0] := bits[1];
  bits[1] := bits[2];
  bits[2] := bits[3];
  bits[3] := bits[4];
  bits[4] := bits[5];
  bits[5] := bits[6];
  bits[6] := bits[7];
  bits[7] := t;
end end;

procedure ScrollRight;
var t: Byte;
begin With SymBase[CurC] do begin
  t	  := bits[7];
  bits[7] := bits[6];
  bits[6] := bits[5];
  bits[5] := bits[4];
  bits[4] := bits[3];
  bits[3] := bits[2];
  bits[2] := bits[1];
  bits[1] := bits[0];
  bits[0] := t;
end end;

procedure MoveBlock;
var t: tSymBase; C, L: LongInt;
begin
  t := SymBase; L := BlK - BlB;

  For C := BlB to High(SymBase) - L - 1 do SymBase[C] := SymBase[C + L + 1];

  For C := High(SymBase) - L - 1 downto CurC do SymBase[C + L + 1] := SymBase[C];

  For C := BlB to BlK do SymBase[CurC + C - BlB] := t[C];

  BlB := CurC;
  BlK := BlB + L;
end;

procedure BuildAsm;
var C, x, n, o: LongInt; f: Text;
begin
  assign(f, '../LCD.font-index.inc'); rewrite(f);
  n := 0; o := 0;
  For C := Low(SymBase) to High(SymBase) do With SymBase[C] do if en then begin
    WriteLn(f, ' .dw 0x', HexStr(o, 4), '+(lcd_T_bmp*2) ;', C: 4, ' ', chr(C), n: 4);
    inc(o, wd+1);
    inc(n);
  end;
  WriteLn(f, ' .dw 0x', HexStr(o, 4), '+(lcd_T_bmp*2) ; FIN');
  close(f);

  assign(f, '../LCD.font-bmp.inc'); rewrite(f);
  n := 0;
  For C := Low(SymBase) to High(SymBase) do With SymBase[C] do if en then begin
    For x := 0 to AnodXMaxNum do if x <= wd then begin
      if n and 15 = 0 then Write(f, ' .db ');
      Write(f, '0x', HexStr(bits[x], 2): 2);
      inc(n);
      if n and 15 = 0 then Write(f, #10) else Write(f, ', ');
    end;
  end;
  close(f);
end;

begin
  assign(f, 'base.bin'); reset(f); read(f, SymBase); close(f);

  NewRootWindow(405, 235, AnodStep * AnodXNum + AnodSize, AnodStep * AnodYNum + AnodSize, 10, 10, 'CharEdit', 'CharEdit', WndMatr);
  NewRootWindow(200, 200, (PrevBorder + AnodXMaxNum) * 16, (PrevBorder + AnodYNum) * 16, 10, 10, 'CharEdit', 'CharEdit', WndPrev);
  SetVGAPalette(WndMatr, Col);
  GC := XCreateGC(prDisplay, WndMatr, 0, Nil);  

  repeat
    KeyInResult := 0;
    AnodXNum := SymBase[CurC].wd; if AnodXNum > AnodXMaxNum then AnodXNum := AnodXMaxNum;
    XResizeWindow(prDisplay, WndMatr, AnodStep * AnodXNum + AnodSize, AnodStep * AnodYNum + AnodSize);
    DrawMatrix;
    DrawPreview;
    if ((CurC >= 32) and (CurC <= 126)) or (CurC >= $a0) then Write(Chr(CurC)) else Write('.');
    Writeln(' ', CurC);
    FlTmWithPause(Event, True);
    Case Event.EventType of
      Expose: Begin DrawMatrix; DrawPreview; End;
      
    end;
    
    if TestMKey(skLShift) then Case KeyInResult of

      XK_Left		: ScrollLeft;
      XK_Right		: ScrollRight;
      XK_Up		: ScrollUp;
      XK_Down		: ScrollDown;

      XK_Page_Up	: CurC := (CurC - 16) and $FF;
      XK_Page_Down	: CurC := (CurC + 16) and $FF;

    end else Case KeyInResult of

      XK_F2: Begin
        rewrite(f); write(f, SymBase); close(f);
      end;

      XK_F6: if BlB <= BlK then if BlK - BlB <= High(SymBase) - CurC then MoveBlock;

      XK_Home: BlB := CurC;

      XK_End: BlK := CurC;

      XK_F10: BuildAsm;

      XK_BackSpace: With SymBase[CurC] do en := not en;

      Ord(' '): Begin
        With SymBase[CurC] do bits[CursX] := bits[CursX] xor BitMask[CursY];
      end;

      Ord('e'): ClearSym;

      Ord('x'): XorSym;

{
      Ord('n'): Begin
        Write('New pos : ');
	ReadLn(SymBase[CurC].numb);
      end;
}
      XK_Left		: if CursX = 0 then CursX := AnodXNum else dec(CursX);
      XK_Right		: if CursX = AnodXNum then CursX := 0 else inc(CursX);
      XK_Up		: if CursY = 0 then CursY := AnodYNum else dec(CursY);
      XK_Down		: if CursY = AnodYNum then CursY := 0 else inc(CursY);

      XK_Page_Up	: if CurC < High(SymBase) then inc(CurC);
      XK_Page_Down	: if CurC > Low(SymBase) then dec(CurC);

      XK_Insert		: if AnodXNum < AnodXMaxNum then SymBase[CurC].wd := AnodXNum + 1;
      XK_Delete		: if AnodXNum > 0 then SymBase[CurC].wd := AnodXNum - 1;
    end;
  until KeyInResult = XK_Escape;
  
end.
