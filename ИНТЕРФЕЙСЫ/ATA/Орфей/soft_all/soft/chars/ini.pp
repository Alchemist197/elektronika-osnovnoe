unit ini;

interface
uses Inferno;

type
  ini_tPrmName	= string[20];
  ini_tBlkName	= string[30];
  ini_tStrValue	= AnsiString;


// ������� ��� ��������� ������
procedure IniOpen(const sec: ini_tBlkName);

// ���������� ��������� � ����
procedure IniClose;

// ����� �������� ���������� ��������� �� �������
function IniGet(const prm: ini_tPrmName; var value: AnsiString): Boolean;
function IniGet(const prm: ini_tPrmName; var value: String): Boolean;
function IniGet(const prm: ini_tPrmName; var value: LongInt): Boolean;
function IniGet(const prm: ini_tPrmName; var value: Cardinal): Boolean;
function IniGet(const prm: ini_tPrmName; var value: Single): Boolean;
function IniGet(const prm: ini_tPrmName; var value: Boolean): Boolean;

// �������� �������� ��������� ��� ������� �������� � �������� ���������
procedure IniPut(const prm: ini_tPrmName; const value: ini_tStrValue);
procedure IniPut(const prm: ini_tPrmName; const value: LongInt);
procedure IniPut(const prm: ini_tPrmName; const value: Cardinal);
procedure IniPut(const prm: ini_tPrmName; const value: Single);
procedure IniPut(const prm: ini_tPrmName; const value: Boolean);

implementation
const
  MaxParam	= 99; // ������������ ����� ���������� � �������
  BlockName	: ini_tBlkName = '';

var
  IniName	: String;
  IniFD		: Text;
  data		: array[1..MaxParam] of ini_tStrValue;
  ContParam	: LongInt;


procedure err(const s: String);
begin WriteLn('ini: ', s, '.'); halt(255) end;


procedure openTest;
begin if BlockName = '' then err('������ �� ������') end;


procedure IniOpen(const sec: ini_tBlkName);

  procedure GetBlock;
  var s: AnsiString;
  begin
    while not Eof(IniFD) do begin
      ReadLn(IniFD, s);
      if s = BlockName then begin
	while not Eof(IniFD) do begin
	  ReadLn(IniFD, s);
	  if copy(s, 1, 1) = '[' then break;
	  inc(ContParam);
	  data[ContParam] := s;
	end;
	exit;
      end;
    end;
  end;

begin
  ContParam := 0; BlockName := '[' + sec + ']';
  {$I-}
  Reset(IniFD);
  if IOResult = 0 then 
  {$I+}
    GetBlock else ReWrite(IniFD);
  Close(IniFD);
end;


procedure IniClose;
var CopyEn, NeedCreate: Boolean; TmpFD: Text; s: AnsiString;

  procedure PutBlock;
  var c: LongInt;
  begin
    for c := 1 to ContParam do WriteLn(TmpFD, data[c]);
    if ContParam > 0 then if data[ContParam]<>'' then WriteLn(TmpFD); // ������ ����� ��������
  end;

begin
  openTest;
  Reset(IniFD);
  Assign(TmpFD, IniName + ProcessIDs); ReWrite(TmpFD); // ������-�� ���� �� mkstemp

  CopyEn := True; NeedCreate := True;
  while not Eof(IniFD) do begin
    ReadLn(IniFD, s);
    if copy(s, 1, 1) = '[' then CopyEn := True;
    if CopyEn then WriteLn(TmpFD, s);
    if s = BlockName then begin CopyEn := False; NeedCreate := False; PutBlock end;
  end;

  if NeedCreate then begin WriteLn(TmpFD, BlockName); PutBlock end;

  Close(IniFD); Close(TmpFD);

  // "rename(f1,IniName);" ������ ������������, �.�. IniName ����� ���� �������
  ReWrite(IniFD); Reset(TmpFD);
  while not Eof(TmpFD) do begin
    ReadLn(TmpFD, s); WriteLn(IniFD, s);
  end;
  Close(IniFD); Close(TmpFD);
  Erase(TmpFD);

  BlockName := '';
end;


function IniGet(const prm: ini_tPrmName; var value: AnsiString): Boolean;
var c: LongInt;
begin
  openTest; IniGet := False;
  for c := 1 to ContParam do begin
    if Pos(prm + '=', data[c]) = 1 then begin
      value := data[c]; delete(value, 1, Length(prm + '='));
      IniGet := True; break;
    end;
  end;
end;

function IniGet(const prm: ini_tPrmName; var value: String): Boolean;
var s: AnsiString;
begin IniGet := IniGet(prm, s); if IniGet then value := s end;

function IniGet(const prm: ini_tPrmName; var value: LongInt): Boolean;
var s: ini_tStrValue;
begin IniGet := IniGet(prm, s); if IniGet then Val(s, value) end;

function IniGet(const prm: ini_tPrmName; var value: Cardinal): Boolean;
var s: ini_tStrValue;
begin IniGet := IniGet(prm, s); if IniGet then Val(s, value) end;

function IniGet(const prm: ini_tPrmName; var value: Single): Boolean;
var s: ini_tStrValue;
begin IniGet := IniGet(prm, s); if IniGet then Val(s, value) end;

function IniGet(const prm: ini_tPrmName; var value: Boolean): Boolean;
var s: ini_tStrValue;
begin IniGet := IniGet(prm, s); if IniGet then value := LowerCase(s) = 'on' end;


procedure IniPut(const prm: ini_tPrmName; const value: ini_tStrValue);
var c: LongInt;
begin
  openTest;
  for c := 1 to ContParam do if pos(prm + '=', data[c]) = 1 then begin data[c] := prm + '=' + value; exit end;
  if ContParam < MaxParam then begin
    inc(ContParam);
    data[ContParam] := prm + '=' + value;
  end else err('������������ ������');
end;

procedure IniPut(const prm: ini_tPrmName; const value: LongInt);
begin IniPut(prm, ValToStrG(value)) end;

procedure IniPut(const prm: ini_tPrmName; const value: Cardinal);
begin IniPut(prm, ValToStrG(value)) end;

procedure IniPut(const prm: ini_tPrmName; const value: Single);
begin IniPut(prm, ValToStrGExt(value)) end;

procedure IniPut(const prm: ini_tPrmName; const value: Boolean);
begin if value then IniPut(prm, 'on') else IniPut(prm, 'off') end;


procedure Init;
begin
  IniName := HomeDir + PathSpliter + '.' + ProgramName + 'rc';
  Assign(IniFD, IniName);
end;

begin
  Init;
end.