;;==========================================================================
;; ��������� ������������� �������� � ��������
;; ��������� ��� ��������� � sizeof(PC) = 16 � �������� �������� 8 ���
;;

;
; ����� ����� 10, 500 � 250 ��
; ���������� r16, r17
;
Wait10ms:
 ldi	r17, 5
 rjmp	W_2
Wait50ms:
 ldi	r17, 25
 rjmp	W_2
Wait500ms:
 rcall	Wait250ms
Wait250ms:
 ldi	r17, 125
W_2:
 rcall	Wait2ms
 dec	r17
 brne	W_2
 ret
;
; ����� ����� 2 �� ~= 163 ����� * 98
; ���������� r16
;
Wait2ms:
 ldi	r16, 98
 rjmp	W_1
;
Wait265mks:
 ldi	r16, 13
W_1:
 rcall	Wait20mks	; 160
 dec	r16		; 1
 brne	W_1		; 2
 ret
;
Wait20mks:		; 3	8*20 = 160
 rcall	Wait5mks	; 40
 rcall	Wait5mks	; 40
 rcall	Wait5mks	; 40	+ 37
;
Wait5mks:		; 3	40 ������ = 5 ���
 rcall	Wait1mks	; 8
 rcall	Wait1mks	; 8
 rcall	Wait1mks	; 8
 rcall	Wait1mks	; 8	+ 5
;
Wait1mks:		; 3	8 ������ �� ������� 8 ��� = 1 ���
 nop			; 1
 ret			; 4
