;;==========================================================================
;; �������� �� SPI �����������
;; ���������� r16
;;

;
; ������������� SPI
;
spi_Init:
 sbi	PORTB, 4
 sbi	DDRB, 4		; !SS [� ���������� ���� ��� ����� �������������� ��� !CS LCD] ��� ����� ���������� �� ���������� SPI !

 sbi	DDRB, 5		; MOSI
 sbi	DDRB, 7		; SCK

; -SPIE - INT enable
; SPE - SPI enable
; -DORD - LSB first
; MSTR - master mode
; -CPOL - SCK is high when idle
; -CPHA - ���� SCK
; -SPR0, -SPR1, SPI2X - divider [8 / 2 ���]

 ldi	r16, (1<<SPE) | (1<<MSTR)
; ldi	r16, (1<<SPE) | (1<<MSTR) | (1<<SPR0) | (1<<SPR1)
 out	SPCR, r16
 ldi	r16, (1<<SPI2X)
; ldi	r16, 0
 out	SPSR, r16
 ret

;
; ������/������ ����� r16
;
spi_IO:
 out	SPDR, r16
spi_IO_1:
 sbis	SPSR, SPIF
 rjmp	spi_IO_1
 in	r16, SPDR
 ret

;
; ����������� ������� ��������� ��������� � ������ ���������
; ������� ��� _��������_ ���������� � PORTB.4
;
.equ	spi_CSCLK_release	= 0x00	; ���������� ��� r16, �������������� ���������� � SPI ���������� ����������
.equ	spi_CSCLK_LCDKB_Data	= 0x11
.equ	spi_CSCLK_SD		= 0x08	; spi_CSCLK_release - ���������� ���� ��������� (���������
.equ	spi_CSCLK_LCDKB_Ctrl	= 0x01	; ����� ��������� � LCD � ��� ������������� Secure Digital Flash)
.equ	spi_CSCLK_VLSI_SDI	= 0xC0
.equ	spi_CSCLK_VLSI_SCI	= 0x80
;
spi_CSCLK_Select:
 sbi	PORTB, 4	; ���� ��� (!PORTB.4) ������ ���� ������ ������̣�, ���� ����� ���� ����� � IDE,
 sbrc	r16, 0		; �.�. �� Σ� ����� ������ ����������� ����������, ������������ ������ ��������
 cbi	PORTB, 4	; � ���� �� IDE. ������� �� �� ����� ������� � PORTC

 push	r17		; ����������� ��� 3 - �� Σ� ����� !CS ����-�����
 ldi	r17, 0x08
 eor	r16, r17
 pop	r17

 out	PORTC, r16
 ldi	r16, 0xFF
 out	DDRC, r16
 ret
