;;==========================================================================
;; Wrapper ���� ��������. ����������� API ��� ��������� ����
;; ��������� � ��������� ��������
;; ���������� r16, r0, r4..r7
;; r17, r18, X (������ Init)
;; C == 1 � ������ ������ ���������� ��������
;;

; ����� �������� ����������
; ����� ������� (�������� ������ ������� !) ���������� � r4..r7, ����� ������ � ������ � Z+
; ������ �������� ���������� CurrentPart, ���������� (�����) - CurrentDevice

; ���������� ���������:
; media_Init - ������ ���������� ��� ����� CurrentDevice � CurrentPart
; media_Read - ������ 512 ���� �� ���������� �������
; media_ReadNoOffset - ������ 512 ���� � ���������� ������������
; media_Write - ���������� 512 ���� � ��������� ������
; media_WriteNoOffset - ���������� 512 ���� � ���������� ������������

.dseg
; ���������� �������
media_SectorOffset:	.byte	4	; �������� ������� CurrentPart �� ������ �����
media_Enable:		.byte	1	; ���� ����������� �����. ��������������� media_Init

;.include "media.CF.asm.inc"
.include "media.IDE.asm.inc"
.include "media.SD.asm.inc"
;.include "media.MMC.asm.inc"
;.include "media.MS.asm.inc"

.cseg
;
; �������������� ���������� ��� ����� ����� ��� �������
;
media_Init:
 clr	r16			; � ������ ���� �������� ���������������� ����� ��������
 sts	media_Enable, r16

 lds	r16, CurrentDevice
 tst	r16
 brne	media_I_ide

 rcall	ide_PowerDown		; ���� ������������ SD, IDE ������� � ������
 rcall	sd_Init			; NB: ��� ������ � r16 � r0 !
 rcall	lcd_PrintC
.db	"SD init: ", 1
 rjmp	media_I_A

media_I_ide:
 rcall	ide_Init
 rcall	lcd_PrintC
.db	"IDE init: ", 1

media_I_A:
 brcc	media_Fault2

 ldi	r16, 1
 sts	media_Enable, r16

 rcall	media_Init_PT
 rcall	lcd_PrintC
.db	"Partition table: ", 1
 clr	r16
 sbci	r16, 0			; if C = 0 -> r16 = 0, C = 0
 sts	media_Enable, r16	; if C = 1 -> r16 = 255, C = 1
 ret

;

media_Init_PT:			; �� ������ ���� - ����������� ������� ��������
 clr	r4
 clr	r5
 clr	r6
 clr	r7
 rcall	media_I_7

 lds	r18, CurrentPart
 tst	r18
 breq	media_Ok2		; ���� Part = 0 - ����� �� ����� ������� ��������, Offset = 0

; ������ ������� ������� ��������

 ldi	ZL, low(IDEBuf0)
 ldi	ZH, high(IDEBuf0)
 rcall	media_ReadNoOffset
 brcc	media_Fault2

 ldi	ZL, low(IDEBuf0+0x1BE)
 ldi	ZH, high(IDEBuf0+0x1BE)
 rcall	media_TestSign
 ldi	r17, 4

; ���� ������� ������� ������� ��������

media_I_1:
 ldd	r16, Z+4
 tst	r16
 breq	media_Fault2		; ��� � �� ���� ������� ������ ������
 
 cpi	r16, 5
 breq	media_I_2		; ������� extended-������
 cpi	r16, 15
 breq	media_I_2

; ������ ������� ������

media_I_4:
 dec	r18
 breq	media_I_3		; ������� ������ � ������� ������� ��������

media_I_9:
 adiw	Z, 0x10
 dec	r17
 brne	media_I_1
media_Fault2:
 rjmp	media_Fault		; ��� � �� ���� ������� ������ ������

; ������� ������ � ����������� ������� ��������

media_I_6:
 rcall	media_I_7		; ����� �������-���������� -> media_SectorOffset
 rcall	media_GetOffsFromPT	; [0].offs -> r4..r7
 rcall	media_AddSectorOffset	; �������� � ������� + ���������� ���������� ������� ���������� -> r4..r7
 rjmp	media_I_7 		; ���������� �������������� ���������� �������� � ����������� (m_SO := [0].offs + sectext)

; ������� ������ � ������� ������� ��������

media_I_3:
 rcall	media_GetOffsFromPT
media_I_7:
 sts	media_SectorOffset+0, r4
 sts	media_SectorOffset+1, r5
 sts	media_SectorOffset+2, r6
 sts	media_SectorOffset+3, r7
media_Ok2:
 rjmp	media_Ok

; ������ extended-�������

;==============================================
; � ��������-�����������:
; ������� ������ ��������� ������, ������ �������� ���� ����� ������� �������-���������� + sector.[0].offs
; ������  ������ ��������� ������ �����������, ������ �������� ���� (media_SectorOffset) + sector.[1].offs
; ������� M$ �� ���� ���������� ������� !
;==============================================

media_I_2:
 movw	XL, ZL			; � X � r17 ���� ������� ����� ������� ������� ������� ��������
 rcall	media_I_3		; LBA-��������� � ����������� PT ������������� ��
 clr	r4			; ����� ������ extended ������� (�� ������ ������� ����������)
 clr	r5			; media_SectorOffset ����� �������� ������������ ��� ����� ������� ����������
 clr	r6
 clr	r7			; ��������������� �� ������ ������������ �������

media_I_5:			; ���� �� ������� �������� ����������
 ldi	ZL, low(IDEBuf1)
 ldi	ZH, high(IDEBuf1)
 rcall	media_Read
 brcc	media_Fault

 ldi	ZL, low(IDEBuf1+0x1BE)	; ������ ������
 ldi	ZH, high(IDEBuf1+0x1BE)
 rcall	media_TestSign

 ldd	r16, Z+4		; � ������ ������ ����� ���� ������ ������ �� ������
 tst	r16
 breq	media_I_8

 dec	r18			; ������ ������ ������ � ����������� �������
 breq	media_I_6

 adiw	Z, 0x10

 ldd	r16, Z+4		; �� ������ ������ ����� ���� ������ ������ �� ��������� ����������
 tst	r16
 breq	media_I_8

 rcall	media_GetOffsFromPT
 rjmp	media_I_5		; ������� � ���������� ������� ���������� �������

; ������� � ������� ������� ������� ��������

media_I_8:
 movw	ZL, XL
 rjmp	media_I_9

;
; ��������� �������� ������� ������������ ������ �������
; r4..r7 += media_SectorOffset
; � ������ ������������ ���������� C = 0 � ������� �� �������� media*
;
media_AddSectorOffset:
 lds	r16, media_SectorOffset+0
 add	r4, r16
 lds	r16, media_SectorOffset+1
 adc	r5, r16
 lds	r16, media_SectorOffset+2
 adc	r6, r16
 lds	r16, media_SectorOffset+3
 adc	r7, r16
 brcc	media_Ok

media_PopFault:
 pop	r16		; ����� �������� ������ ��� ��������� � SizeOf(PC) == 16
 pop	r16

media_Fault:
 clc
 ret

media_Ok:
 sec
 ret

;
; ��������� ��������� 0x55 0xAA � Z + 0x10*4
; � ������ ������ ���������� C = 0 � ������� �� �������� media*
;
media_TestSign:
 adiw	Z, 0x20
 ldd	r16, Z+0x20
 cpi	r16, 0x55
 brne	media_PopFault
 ldd	r16, Z+0x21
 cpi	r16, 0xAA
 brne	media_PopFault
 sbiw	Z, 0x20
 ret

;
; ����������� � r4..r7 �������� ������� �� �������, �� ������� ��������� Z
;
media_GetOffsFromPT:
 ldd	r4, Z+08
 ldd	r5, Z+09
 ldd	r6, Z+10
 ldd	r7, Z+11
 ret

;
; ��������� ���� ����������� ����� � ������� �� media_* � ������ �������������
;
media_CheckEnable:
 lds	r16, media_Enable
 tst	r16
 breq	media_PopFault
 ret

;
; ������ ������
;
media_Read:
 rcall	media_AddSectorOffset
media_ReadNoOffset:
 rcall	media_CheckEnable

 lds	r16, CurrentDevice
 sbrc	r16, 0
 rjmp	ide_ReadSect
 push	r17
 rcall	sd_ReadSect
 pop	r17
 ret

;
; ���������� ������
;
media_Write:
 rcall	media_AddSectorOffset
media_WriteNoOffset:
 rcall	media_CheckEnable

 lds	r16, CurrentDevice
 sbrc	r16, 0
 rjmp	ide_WriteSect
 push	r17
 rcall	sd_WriteSect
 pop	r17
 ret
