;;==========================================================================
;; ������ ������ ������ "�������� ���������"
;;

.dseg
msine_Freq:	.byte	1
msine_Length:	.byte	1
msine_V_L:	.byte	1
msine_V_R:	.byte	1

msine_MainEn:	.byte	1

.eseg
nv_msine_Freq:	.byte	1
nv_msine_Length:.byte	1
nv_msine_V_L:	.byte	1
nv_msine_V_R:	.byte	1

.cseg
;
; ������� ��������� "�������� ���������"
;
msine_Enter:
 call	f_IO
.db	f_ORead, 4
.dw	nv_msine_Freq
.dw	msine_Freq

 ldi	r16, 'n'
 sts	msine_MainEn, r16

 ldi	r22, low(msine_Desc*2)
 ldi	r23, high(msine_Desc*2)
 call	ur_Menu

 rcall	msine_MainOff
 call	vlsi_Close

 ldi	r16, m_ModeMenu
 sec
 ret

msine_Desc:
 .db	(1<<ur_m_DrawEn) | (1<<ur_m_LeftEn) | (1<<ur_m_OkEn) | (1<<ur_m_RightEn) | (1<<ur_m_SelEn)                                    , 0x00
 .dw	msine_Txt1*2
 .dw	msine_FreqDraw
 .dw	msine_FreqDec
 .dw	msine_SaveTuning
 .dw	msine_FreqInc

 .db	(1<<ur_m_DrawEn) | (1<<ur_m_LeftEn) | (1<<ur_m_OkEn) | (1<<ur_m_RightEn) | (1<<ur_m_SelEn)                                    , 0x00
 .dw	msine_Txt2*2
 .dw	msine_LenDraw
 .dw	msine_LenDec
 .dw	msine_SaveTuning
 .dw	msine_LenInc

 .db	(1<<ur_m_DrawEn) | (1<<ur_m_LeftEn) | (1<<ur_m_OkEn) | (1<<ur_m_RightEn) | (1<<ur_m_SelEn)                                    , 0x00
 .dw	msine_Txt3*2
 .dw	msine_VL_Draw
 .dw	msine_VL_Dec
 .dw	msine_SaveTuning
 .dw	msine_VL_Inc

 .db	(1<<ur_m_DrawEn) | (1<<ur_m_LeftEn) | (1<<ur_m_OkEn) | (1<<ur_m_RightEn) | (1<<ur_m_SelEn)                                    , 0x00
 .dw	msine_Txt4*2
 .dw	msine_VR_Draw
 .dw	msine_VR_Dec
 .dw	msine_SaveTuning
 .dw	msine_VR_Inc

 .db	(1<<ur_m_DrawEn) | (1<<ur_m_LeftEn)                  | (1<<ur_m_RightEn) | (1<<ur_m_SelEn)                                    , 0x00
 .dw	msine_Txt5*2
 .dw	msine_EnDraw
 .dw	msine_MainOff
 .dw	0
 .dw	msine_MainOn

 .db	                                                                           (1<<ur_m_SelEn) | (1<<ur_m_OkFin) | (1<<ur_m_Close), 0x0E
 .dw	msine_Txt6*2
 .dw	0
 .dw	0
 .dw	0
 .dw	0

msine_Txt1:
 .db	"�������  : ", 0
msine_Txt2:
 .db	"������   : ", 0
msine_Txt3:
 .db	"������� �: ", 0
msine_Txt4:
 .db	"������� �: ", 0
msine_Txt5:
 .db	"�������  : ", 0
msine_Txt6:
 .db	"������� ����", 0

;

msine_FreqDraw:
 ldi	ZL, low(msine_FD_Table*2)
 ldi	ZH, high(msine_FD_Table*2)
 lds	r18, msine_Freq
 ldi	r19, 5

msine_FD_2:
 mul	r18, r19
 add	ZL, r0
 add	ZH, r1

msine_FD_1:
 lpm	r25, Z+
 call	lcd_CharOut
 dec	r19
 brne	msine_FD_1

 ret

msine_FD_Table:
.db	"44100", "48000", "32000", "22050", "24000", "16000", "11025", "12000", " 8000"

msine_FreqDec:
 lds	r18, msine_Freq
 dec	r18
 brpl	msine_FI_1
 ldi	r18, 8
 rjmp	msine_FI_1

msine_FreqInc:
 lds	r18, msine_Freq
 inc	r18
 cpi	r18, 9
 brne	msine_FI_1
 clr	r18
msine_FI_1:
 sts	msine_Freq, r18
 rjmp	msine_MainRestart

;

msine_LenDraw:
 ldi	ZL, low(msine_LD_Table*2)
 ldi	ZH, high(msine_LD_Table*2)
 lds	r18, msine_Length
 ldi	r19, 6
 rjmp	msine_FD_2

msine_LD_Table:
.db	"32.000", "16.000", "10.667", " 8.000", " 6.400", " 5.333", " 4.571", " 4.000"

msine_LenDec:
 lds	r18, msine_Length
 dec	r18
 rjmp	msine_LI_1

msine_LenInc:
 lds	r18, msine_Length
 inc	r18
msine_LI_1:
 andi	r18, 0x07
 sts	msine_Length, r18
 rjmp	msine_MainRestart

;

msine_VL_Draw:
 lds	dd16uL, msine_V_L
 com	dd16uL
 jmp	DecByteOut

msine_VL_Dec:
 lds	r16, msine_V_L
 inc	r16
 rjmp	msine_VL_I_1
msine_VL_Inc:
 lds	r16, msine_V_L
 dec	r16
msine_VL_I_1:
 sts	msine_V_L, r16
 rjmp	msine_VR_I_2

;

msine_VR_Draw:
 lds	dd16uL, msine_V_R
 com	dd16uL
 jmp	DecByteOut

msine_VR_Dec:
 lds	r16, msine_V_R
 inc	r16
 rjmp	msine_VR_I_1

msine_VR_Inc:
 lds	r16, msine_V_R
 dec	r16
msine_VR_I_1:
 sts	msine_V_R, r16
msine_VR_I_2:
 rjmp	msine_MainRestart

;

msine_EnDraw:
 lds	r25, msine_MainEn
 jmp	lcd_CharOut

;
; ������������� ���� � ������ �����������
;
msine_MainRestart:
 rcall	msine_MainOff

;
; �������� ����
;
msine_MainOn:
 ldi	r16, 'Y'
 sts	msine_MainEn, r16

 lds	r16, msine_V_L
 sts	vlsi_V_L, r16
 lds	r16, msine_V_R
 sts	vlsi_V_R, r16
 call	vlsi_SetTones

 ldi	r16, spi_CSCLK_VLSI_SDI
 call	spi_CSCLK_Select	; !CS = 1, CLKen = 1

 ldi	r16, 0x53
 call	spi_IO
 ldi	r16, 0xEF
 call	spi_IO
 ldi	r16, 0x6E
 call	spi_IO

 lds	r16, msine_Length
 ldi	r17, 9
 mul	r16, r17		; r0 = Length * 9
 lds	r16, msine_Freq
 adc	r16, r0			; r16 = Length * 9 + Freq
 subi	r16, -(48)		; r16 = (Length * 9) + Freq + 48

 call	spi_IO

 rjmp	msine_MO_1

;
; ��������� ����
;
msine_MainOff:
 ldi	r16, 'N'
 sts	msine_MainEn, r16

 ldi	r16, spi_CSCLK_VLSI_SDI
 call	spi_CSCLK_Select	; !CS = 1, CLKen = 1

 ldi	r16, 0x45
 call	spi_IO
 ldi	r16, 0x78
 call	spi_IO
 ldi	r16, 0x69
 call	spi_IO
 ldi	r16, 0x74
 call	spi_IO

msine_MO_1:
 ldi	r17, 4
msine_MO_2:
 clr	r16
 call	spi_IO
 dec	r17
 brne	msine_MO_2
 ret

;
; ��������� ���������
;
msine_SaveTuning:
 call	f_IO
.db	f_OWrite, 4
.dw	nv_msine_Freq
.dw	msine_Freq

 clc
 call	lcd_PrintC
.db	"Saved to EEPROM", 1
 ret
