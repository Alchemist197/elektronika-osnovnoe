;;==========================================================================
;; ��������� ���������� � LCD-�������
;;

.dseg
lcd_XPos:	.byte	1	; ������� ������������ ������� �� X
lcd_Attr:	.byte	1	; D7 - ��������, D6 - ������ ���������������� ��������� (����������� �� ������)

lcd_LightCnt:	.byte	1	; �ޣ���� �������� ���������

				; ����������:
kb_Accm:	.byte	1	; ���������� ���������� ��������� or
kb_Cntr:	.byte	1	; �ޣ���� ���������� ��������, � ������� ������� ���� ������ ���� �� ���� �������
kb_Keys:	.byte	1	; �������� ������� ���������� ������� (����� ���������)

.equ	lcd_XPosMax	= 84
.equ	lcd_YPosMax	= 6


.cseg
;
; ������������� KB � LCD
;
lcd_Init:
 clr	r17		; ��� ������� ������
 sts	kb_Keys, r17
 sts	kb_Accm, r17

 ldi	r17, 0x21	; �������� �������, ����������� ������� (C) �� ���
 rcall	lcd_SendCtrl
 ldi	r17, 0x13	; bias = 3
 rcall	lcd_SendCtrl
 ldi	r17, 0xC8	; �������� ���������� ��������, Vop = 0x48
 rcall	lcd_SendCtrl	; ������, ���, � ���� ����������� �����������, �� ������� �������� ������� �� ����������� �������
        
 ldi	r17, 0x20	; Power Up
 rcall	lcd_SendCtrl
 ldi	r17, 0x0C	; Set normal mode
 rcall	lcd_SendCtrl

;
; ������� ������
;
lcd_ClrScr:
 clr	r16
 rcall	lcd_SetAttr
 ldi	r17, lcd_YPosMax-1

lcd_CS_1:
 push	r17
 rcall	lcd_GoToLine
 rcall	lcd_ClrEol
 pop	r17
 dec	r17
 brpl	lcd_CS_1
 ret

;
; ������� �� ����� ������
; ������ NoLimit ��������� ������������ (r18) ����� ������ (����� �������� ������� �� ��������� ���������� ������)
;
lcd_ClrEol:
 ldi	r18, lcd_XPosMax
 
lcd_ClrEolNoLimit:
 ldi	r25, ' '
 rcall	lcd_CharOutNoLimit
 lds	r16, lcd_XPos
 cp	r16, r18
 brlo	lcd_ClrEolNoLimit
 ret

;
; �������� ����� r17 � ������� ���������� LCD
;
lcd_SendCtrl:
 ldi	r16, spi_CSCLK_LCDKB_Ctrl
 rcall	spi_CSCLK_Select
 mov	r16, r17
 rcall	spi_IO
 ldi	r16, spi_CSCLK_release	; ����� ��������� !CS �������, 
 rjmp	spi_CSCLK_Select	; ����� ����������� ������� ���������� �� ����� ������ �� IDE

;
; ������� � ������ ��� ������ ����� (r18) �������� � r17 ������
;
lcd_GoToLine:
 clr	r18
lcd_GoToXY:
 ori	r17, 0x40
 rcall	lcd_SendCtrl	; VTAB
 sts	lcd_XPos, r18
 mov	r17, r18
 ori	r17, 0x80
 rjmp	lcd_SendCtrl	; HTAB

;
; ����� ������� r25 � �������, �����Σ���� ������������ lcd
; ������ NoLimit ��������� ������������ (r18) ����� ������ (����� �������� ������� �� ��������� ���������� ������)
; ���������� r16..18, r24, r25
;
lcd_CharOut:
 ldi	r18, lcd_XPosMax

lcd_CharOutNoLimit:
 cpi	r25, '�'
 brne	lcd_CO_3
 ldi	r25, 96
 rjmp	lcd_CO_4

lcd_CO_3:
 cpi	r25, '�'
 brne	lcd_CO_5
 ldi	r25, 97
 rjmp	lcd_CO_4

lcd_CO_5:
 subi	r25, 0x20
 cpi	r25, 0xA0
 brlo	lcd_CO_4
 subi	r25, 0x40-2

lcd_CO_4:
 push	ZL
 push	ZH
 push	XL
 push	XH

 ldi	ZL, low(lcd_T_index*2)
 ldi	ZH, high(lcd_T_index*2)

 clr	r24
 lsl	r25
 rol	r24
 add	ZL, r25
 adc	ZH, r24

 lpm	XL, Z+			; X ��������� �� ���������� ���������� �������
 lpm	XH, Z+

 lpm	r24, Z+			; r24..r25 ��������� �� ���������� ���������� �������

 sub	r24, XL			; r24 - ����� ���������� �����������
 movw	ZL, XL
 lds	r25, lcd_XPos
 lds	r17, lcd_Attr

 ldi	r16, spi_CSCLK_LCDKB_Data
 rcall	spi_CSCLK_Select

lcd_CO_1:			; ���� ������ �������
 cp	r25, r18
 breq	lcd_CO_2
 inc	r25

 lpm	r16, Z+
 sbrc	r17, 7
 com	r16
 rcall	spi_IO

 dec	r24
 brne	lcd_CO_1

 cp	r25, r18		; ����� ����������� ��������
 breq	lcd_CO_2
 inc	r25

 clr	r16
 sbrc	r17, 7
 com	r16
 rcall	spi_IO

lcd_CO_2:
 sts	lcd_XPos, r25

 pop	XH
 pop	XL
 pop	ZH
 pop	ZL

lcd_CO_9:
 ldi	r16, spi_CSCLK_release	; ����� ��������� !CS �������, 
 rjmp	spi_CSCLK_Select	; ����� ����������� ������� ���������� �� ����� ������ �� IDE

;
; ������� ������, ������������� �� �������� ������ ������ ��������� (������������� �������� \001)
; � ������ ������ �������, �������������� ������ "* ".
; �� ������������ ����� ������.
; � ����� ������� ���� "+" � ���������� ����������, ���� �� ����� C = 1
; ��� ���� "-" � ������� ������� ����� �������, ���� C = 0.
; ������������, ������ �������, r1 !
;
lcd_PrintC:
 in	r1, SREG

 clr	r17
 rcall	lcd_GoToLine
 ldi	r16, 0x80
 rcall	lcd_SetAttr

 ldi	r25, '*'
 rcall	lcd_CharOut
 ldi	r25, ' '
 rcall	lcd_CharOut

 pop	ZH
 pop	ZL
 lsl	ZL
 rol	ZH

lcd_PC_2:
 lpm	r25, Z+
 cpi	r25, 1		; � ��� �������� ������������ ����� EOL == 1, ������ ���
 breq	lcd_PC_1	; ����������� .db "������ �������� �����" \\ .db "��� ������"
 rcall	lcd_CharOut	; ���������� ������� ������������� ���� � ����� ������ ������, ��� �������
 rjmp	lcd_PC_2	; ���������� ������ ASCIZ.

lcd_PC_1:
 ldi	r25, ' '
 rcall	lcd_CharOut

 ldi	r25, '-'
 sbrc	r1, 0
 ldi	r25, '+'
 rcall	lcd_CharOut

 rcall	lcd_ClrEol

 sbrs	r1, 0
 rcall	kb_PressAnyKey

 adiw	ZL, 1
 lsr	ZH
 ror	ZL
 out	SREG, r1
 ijmp

;
; ������ ��������� �� r16
;
lcd_SetAttr:
 sts	lcd_Attr, r16
 ret

;
; ��������/��������� ���������
;
lcd_LightOn:
 sbi	DDRB, 3			; "0" - ��������
 ret
;
lcd_LightOff:
 cbi	DDRB, 3			; "Z" - ���������
lcd_ret:
 ret
;
lcd_IntelLight:			; �������� ��������� ��� ����� ������� ������,
 lds	r16, kb_Keys		; ��������� ����� 200 �������� ����� ���������� ������
 tst	r16
 breq	lcd_IL_1

 rcall	lcd_LightOn
 ldi	r16, 200
 rjmp	lcd_IL_2
 
lcd_IL_1:
 lds	r16, lcd_LightCnt
 dec	r16
 breq	lcd_LightOff 

lcd_IL_2:
 sts	lcd_LightCnt, r16
 ret

;
; ������ ����������. ���������� ���� ������ � kb_Keys. ������� ������ ���� "1"
; ������� ���������� ����� ������ PORTA (����� �����) � !CS LCD-�������
;
; ����� ������������ ��� ���� ��������� ������������:
; - ���� �� ����� ������ ���������� �� ���������� ������, ��� ����� ������� �� ������������� � ��������������.
;   � ����� �������� ����� ������� ��� ��������� ��� � ������� ��� ���������� ������ ���� ���������� ���
;   ��������� �� ��������� � ��������� ������.
; - ���� �� ����� ������ ���� �������, ��� ����� ������� ������� �, ��������, ���������� ������ ��������.
;   ��� ��� ��� ������� �������������� ����� ��������, ��� ����������, ��� ����� ��������� ������ ����
;   �� ���� �������� ���������� ��������. ��� ����� ��������� ����������, ���� ��������� ������� ��
;   ������� ������ � ��������� � ������ �������� �� ����������.
;
; �� ����, ��� ��������, ������� ������ ������������ ������ - ��� ����� ��������� ���������� ������
; ���� �� ��� �������� ��������� ��� ������ ���� ���� ������� ��� ������������� ���� ���������� ��������.
; RES: ��� ���� �� ���������. ������ ��������, �������, �����.
;
; ������ �������: ������� ��������� ��������� ���������, ���� ţ ��������� ��� ����� ����������. �� �� ������� �� ����������
; �������� or ���� ��������� � ��� �������� ������ ��� ���������� ��� ���������� ���������.
;
.equ	Key0n	= 5
.equ	Key1n	= 4
.equ	Key2n	= 6
.equ	Key3n	= 0
.equ	Key4n	= 3	   
.equ	Key5n	= 1
.equ	Key6n	= 2   
.equ	Key7n	= 7

.equ	Key0mask= 1<<Key0n
.equ	Key1mask= 1<<Key1n
.equ	Key2mask= 1<<Key2n
.equ	Key3mask= 1<<Key3n
.equ	Key4mask= 1<<Key4n
.equ	Key5mask= 1<<Key5n
.equ	Key6mask= 1<<Key6n
.equ	Key7mask= 1<<Key7n
;
kb_GetKeys:

; ������ ��������� ����������� �����

 clr	r16			; PortA - PullUp
 out	DDRA, r16
 ldi	r16, 0xFF
 out	PORTA, r16

 ldi	r16, spi_CSCLK_LCDKB_Ctrl
 rcall	spi_CSCLK_Select	; !CS = 0

 in	r17, PINA
 com	r17			; ����� ������� ������� ����� "1", � ���������� ������ ���� Zero

; ����������� ���������� ���������

 lds	r16, kb_Accm
 breq	kb_GK_no_key		; [���� Z ����� ������̣� ��������� "com"]

 or	r17, r16		; ���������� ��� � ���������� �������
 
 lds	r16, kb_Cntr		; ��������� �ޣ�����
 inc	r16
 breq	kb_GK_no_inc
 sts	kb_Cntr, r16
kb_GK_no_inc:
 cpi	r16, 8
 brlo	kb_GK_fin		; �ޣ���� ���� ��������

 sts	kb_Keys, r17		; ����� ���������� ��� �������� �������
 rjmp	kb_GK_fin

kb_GK_no_key:			; -- ������� ���������
 sts	kb_Keys, r16		; ���������� � Keys ����� ���������� ����������
 sts	kb_Cntr, r17		; �������� �ޣ����
 				; � ������ ��������� �����������
kb_GK_fin:
 sts	kb_Accm, r17

 lds	r16, lcd_Attr		; ������ ���������������� ���������
 sbrs	r16, 6
 rcall	lcd_IntelLight		; ���������� ���������� � ����������� �� ������� ������

 rjmp	lcd_CO_9		; ��������� ������� ������ (����� �� ������ IDE)

;
; ������� ����� ���������� � ������� ������� ��� ��������� ����� �������, ����� ������� ���������� ���� ������
;
kb_PressAnyKey:
 clr	r16
 sts	kb_Keys, r16
kb_PAK_1:
 rcall	Wait50ms		; ����� ��������� �������� ���������������� ���������
 rcall	kb_GetKeys
 lds	r16, kb_Keys
 tst	r16
 breq	kb_PAK_1

;
; ������� ���������� �������
;
kb_WaitReleaseKeys:
 rcall	Wait50ms		; ����� ��������� �������� ���������������� ���������
 rcall	kb_GetKeys
 lds	r16, kb_Keys
 tst	r16
 brne	kb_WaitReleaseKeys
 ret
