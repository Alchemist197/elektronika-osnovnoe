;;==========================================================================
;; �������� � SD-������
;; ���������� r16, r17, r0, r4..r7
;; ��� �������� ���������� C = 1 - ����� ��������
;; ��������� ������ ������ �� ffff ffff ���� (4 ����... � ����� ������ 2...)
;; � r16 ��� C = 0 ������������ ��� ������ (D7/D6/D5/D4):
;; 1xxx - ������� �� ������� (������ � ������ R1, ��������� ���� �� R1)
;; 0100 - ���������� ����� �� ����� ����������� � ������� ����� (���� D3..D0 ��� � ������ Data Error Token)
;; 0010 - ������������ ��� ���������� ������ (������ 0x20)
;; 011x - ������ ��� ������������� ������ (D4..D0 ��� � ������ Data Response Token)
;; 0000 - ������ ������ R2 (r0 - ����������� ��� ������) [��� ������] ��� �������� ����� �� ������� ������
;;

.cseg
;
; ������������� SD-�����
;
sd_Init:
 ldi	r16, spi_CSCLK_release	; !CS = 1
 rcall	spi_CSCLK_Select
 
 ldi	r17, 10			; ������ �� �������� � MMC, ����� �� ��������,
sd_I_2:				; ���� � ��� ����� ��� ������ ����������
 ldi	r16, 0xFF
 rcall	spi_IO
 dec	r17
 brne	sd_I_2

 ldi	r16, spi_CSCLK_SD	; !CS = 0
 rcall	spi_CSCLK_Select

 ldi	r16, 0xFF		; ��� ����� ����������� ����������
 rcall	spi_IO

 clr	r4
 clr	r5
 clr	r6
 clr	r7

 ldi	r16, 0x40		; ���������� ������� ������������ � SPI (RESET)
 ldi	r17, 0x95
 rcall	sd_CmdSend
 andi	r16, 0x01		; �������� �� in_idle_state = 1
 breq	sd_Fault1

; ldi	r16, 0x48		; ���������� ������� �������� ������� ������� (����������, ...)

; ldi	r16, 0x7A		; ���������� ������� ������ �������� OCR

sd_I_1:
 ldi	r16, 0x77		; ������� APP_CMD - ��������� ������� ����� �� ������������ ������
 rcall	sd_CmdSendWithoutCRC

 ldi	r16, 0x69		; ������������� ����� � ��������� �� � ���, ��� High Cap �� ��������������
 rcall	sd_CmdSendWithoutCRC	; �� ����� ���������� ���� ������� ��� ����������� ��������� CRC, �� �� ��������� - ����� ? ���������� ������������...

 andi	r16, 0x01		; �������� �� in_idle_state = 0
 brne	sd_I_1			; ����� �������� ��� �������, ���� ����� �� ������ �� idle state

 ldi	r16, 2
 mov	r5, r16
 ldi	r16, 0x50		; ��������� ������� ����� � 512 ����
 rcall	sd_CmdSendWithoutCRC

sd_Ok0:
 sec
 ret

;
; ������ ����� ������ r4..r7 � Z+
;
sd_ReadSect:
 ldi	r16, spi_CSCLK_SD	; !CS = 0
 rcall	spi_CSCLK_Select

 rcall	sd_Mul512

 ldi	r16, 0x51
 rcall	sd_CmdSendWithoutCRC

sd_RS_2:
 ldi	r16, 0xFF
 rcall	spi_IO

 cpi	r16, 0xFE		; ����� ������ � �������� ������
 breq	sd_RS_1

 mov	r17, r16
 andi	r17, 0xF0
 brne	sd_RS_2

 ori	r16, 0x40		; ������� ������ ���� = 0 - ��� �������� ������ ������
sd_Fault1:
 rjmp	sd_Fault0

sd_RS_1:
 clr	r17			; ��������� ������ �� �����

sd_RS_3:
 ldi	r16, 0xFF
 rcall	spi_IO
 st	Z+, r16

 ldi	r16, 0xFF
 rcall	spi_IO
 st	Z+, r16

 dec	r17
 brne	sd_RS_3

 ldi	r16, 0xFF		; ���������� ����� ����������� �����
 rcall	spi_IO
 ldi	r16, 0xFF
 rcall	spi_IO

sd_Ok1:
 sec
 ret

;
; ������ ����� ������ r4..r7 �� Z+
;
sd_WriteSect:
 ldi	r16, spi_CSCLK_SD	; !CS = 0
 rcall	spi_CSCLK_Select

 rcall	sd_Mul512

 ldi	r16, 0x58
 rcall	sd_CmdSendWithoutCRC

 ldi	r16, 0xFE
 rcall	spi_IO

 clr	r17			; �������� ������ � �����

sd_WS_3:
 ld	r16, Z+
 rcall	spi_IO

 ld	r16, Z+
 rcall	spi_IO

 dec	r17
 brne	sd_WS_3

 rcall	spi_IO			; ������������ ����� ����������� �����
 rcall	spi_IO

 ldi	r16, 0xFF
 rcall	spi_IO			; ������� ���� �������������
 andi	r16, 0x1F
 cpi	r16, 0x05
 breq	sd_WS_4			; ������ ���� �������

 ori	r16, 0x60		; ������ ��ɣ�� ������
 rjmp	sd_Fault0

sd_WS_4:			; ����� �� �����, �ģ� ���������� ������
 ldi	r16, 0xFF
 rcall	spi_IO
 cpi	r16, 0xFF
 brne	sd_WS_4

 ldi	r16, 0x4D		; �������� ������
 rcall	sd_CmdSendWithoutCRC

 tst	r0
 breq	sd_Ok0

 clr	r16			; ������ R2, ��� ������ � r0
 rjmp	sd_Fault0

;
; �������� ������� � �������� ������ R1 (������������ � r16) ��� R2 (������������ � r0)
; ���������, ��� �� �������� ����� �� ��������� � ��������� �� �������, �� ��� ��� ����� �� �������� !
; � ������ ������ � ������ R1 ���������� ���������� ������, ������������� � sd_*, ��������� C = 0
;
sd_CmdSendWithoutCRC:
 ldi	r17, 0xFF

sd_CmdSend:
 push	r16
 ldi	r16, 0xFF		; ������� ����
 rcall	spi_IO
 pop	r16

 rcall	spi_IO			; ���� ������ � ���� �������

 mov	r16, r7			; �������� �������
 rcall	spi_IO
 mov	r16, r6
 rcall	spi_IO
 mov	r16, r5
 rcall	spi_IO
 mov	r16, r4
 rcall	spi_IO

 mov	r16, r17		; ����������� ����� � ��� ������
 rcall	spi_IO

 ldi	r16, 0xFF		; �������� ���� [��� ���������� ������� ������������� �����
 rcall	spi_IO			; ������������ 3F, ��� ����� ���������� � R1]

sd_CS_1:
 ldi	r16, 0xFF		; �������� ������
 rcall	spi_IO
 tst	r16
 brmi	sd_CS_1

 push	r16			; ���� ����� ������� R2 - � r0 ����� ��� ��������
 ldi	r16, 0xFF
 rcall	spi_IO
 mov	r0, r16
 pop	r16

 mov	r17, r16
 andi	r17, 0x7E
 breq	sd_CS_2

 ori	r16, 0x80		; ��� ������

sd_CS_3:
 pop	r17			; ����������� �� ����� ����� �������� � ������� �� ������ media.SD
 pop	r17

sd_Fault0:
 clc

sd_CS_2:
 ret

;
; ��������� r4..r7 �� 512
; ��� ������������ ������������ �� sd_* � C = 0
;
sd_Mul512:
 ldi	r16, 0x20	; ��� ��������� ������

 tst	r7
 brne	sd_CS_3		; ����� �������� ���� ������
 tst	r6
 brmi	sd_CS_3		; --""--

 mov	r7, r6
 mov	r6, r5
 mov	r5, r4
 clr	r4

 lsl	r5
 rol	r6
 rol	r7
 ret
