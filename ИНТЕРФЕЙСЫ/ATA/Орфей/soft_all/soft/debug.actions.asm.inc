;;==========================================================================
;; ����������� ������ �������
;; �� ���������� ������� Y
;; 

.dseg
;
; ����������
;
DebugVal:	.byte	2


.cseg
;;
;; ��������� SD
;;
cmd_SD_ReadSect:
 rcall	cmd_IDE_IOSect1
 brcc	ret0

 call	sd_ReadSect

cmd_SD_IOSect2:
 ldi	r25, '+'
 brcs	cmd_SD_IOSect2_1
 ldi	r25, '-'
cmd_SD_IOSect2_1:
 rjmp	SendByte

;

cmd_SD_WriteSect:
 rcall	cmd_IDE_IOSect1
 brcc	ret0

 call	sd_WriteSect

 rjmp	cmd_sd_IOSect2

;;
;; ������/������ IDE
;;
cmd_IDE_GetReg:
 ldi	r17, 1

cmd_IGR_1:
 mov	r16, r17
 rcall	ide_GetReg
 mov	r25, r0
 rcall	ByteOut
 ldi	r25, ' '
 rcall	SendByte
 inc	r17
 cpi	r17, 8
 brne	cmd_IGR_1
ret0:
 ret

;

cmd_IDE_PutReg:
 rcall	Str2Val
 brcc	ret0
 push	r24
 adiw	Y, 1
 rcall	Str2Val
 pop	r16
 brcc	ret0
 mov	r0, r24
 rjmp	ide_PutReg

;

cmd_IDE_WriteBuf:
 ldi	ZL, low(IDEBuf0)
 ldi	ZH, high(IDEBuf0)
 rjmp	ide_PutBuf

;

cmd_IDE_ReadBuf:
 ldi	ZL, low(IDEBuf0)
 ldi	ZH, high(IDEBuf0)
 rjmp	ide_GetBuf

;

cmd_IDE_ViewBuf0:
 ldi	ZL, low(IDEBuf0)
 ldi	ZH, high(IDEBuf0)
 rjmp	cmd_IDE_ViewBuf

cmd_IDE_ViewBuf1:
 ldi	ZL, low(IDEBuf1)
 ldi	ZH, high(IDEBuf1)
cmd_IDE_ViewBuf:
 clr	r17

cmd_IVB_3:
 movw	XL, ZL

 ldi	r25, 13
 rcall	SendByte
 mov	r25, r17
 rcall	ByteOut
 ldi	r25, '-'
 rcall	SendByte

 ldi	r18, 16
cmd_IVB_2:
 ldi	r25, ' '
 rcall	SendByte
 ld	r25, Z+
 rcall	ByteOut
 dec	r18
 brne	cmd_IVB_2

 rcall	print
.db	"    ", 1
 movw	ZL, XL

 ldi	r18, 16
cmd_IVB_4:
 ld	r25, Z+
 cpi	r25, 32
 brsh	cmd_IVB_5
 ldi	r25, '.'
cmd_IVB_5:
 rcall	SendByte
 dec	r18
 brne	cmd_IVB_4

 inc	r17
 cpi	r17, 32
 brne	cmd_IVB_3
 ret

;

cmd_IDE_IOSect1:
 rcall	Str2Val
 brcc	ret0
 movw	r6, r24

 adiw	Y, 1

 rcall	Str2Val
 brcc	ret0
 movw	r4, r24

 ldi	ZL, low(IDEBuf0)
 ldi	ZH, high(IDEBuf0)
 sec
ret1:
 ret

;

cmd_IDE_ReadSect:
 rcall	cmd_IDE_IOSect1
 brcc	ret1

 rcall	ide_ReadSect

cmd_IDE_IOSect2:
 ldi	r25, '+'
 brcs	cmd_IDE_IOSect2_1
 ldi	r25, '-'
cmd_IDE_IOSect2_1:
 rjmp	SendByte

;

cmd_IDE_WriteSect:
 rcall	cmd_IDE_IOSect1
 brcc	ret1

 rcall	ide_WriteSect

 rjmp	cmd_IDE_IOSect2

;

cmd_media_PT:
 rcall	Str2Val
 brcc	ret1
 sts	CurrentPart, r24
 sts	CurrentDevice, r25

 rcall	file_DvCh	; �ӣ ����������� - ������ ��� �������

 call	media_Init
 rcall	cmd_IDE_IOSect2

 rcall	fsFAT_Init
 rcall	cmd_IDE_IOSect2
 brcs	cmd_media_PT_2
 
 lds	r25, fs_errno
 rcall	ByteOut
 
cmd_media_PT_2:
 lds	r25, media_SectorOffset+3
 lds	r24, media_SectorOffset+2
 rcall	WordOut
 lds	r25, media_SectorOffset+1
 lds	r24, media_SectorOffset+0
 rcall	WordOut

 ldi	r25, 13
 rcall	SendByte
 ldi	ZL, low(fsFAT_Sector2Cluster)
 ldi	ZH, high(fsFAT_Sector2Cluster)
 ldi	r16, 15
 
cmd_media_PT_1:
 ld	r25, Z+
 rcall	ByteOut
 ldi	r25, ' '
 rcall	SendByte
 dec	r16
 brne	cmd_media_PT_1
 ret

;

cmd_NextFATEntry:
 rcall	Str2Val
 brcc	cmd_NFE_1
 sts	CurrentCluster+0, r24
 sts	CurrentCluster+1, r25
cmd_NFE_1:
 clr	r16
 sts	CurrentCluster+2, r16
 sts	CurrentCluster+3, r16

 rcall	fsFAT_NextFATEntry
 rcall	cmd_IDE_IOSect2

 lds	r25, CurrentCluster+3
 lds	r24, CurrentCluster+2
 rcall	WordOut
 lds	r25, CurrentCluster+1
 lds	r24, CurrentCluster+0
 rjmp	WordOut

;

cmd_MenuPath:
 rjmp	pc_Menu

cmd_SetPath:
 ldi	XL, low(CurrentDirEnt)
 ldi	XH, high(CurrentDirEnt)
 ldi	r22, MaxDirDeep

cmd_SP_1:
 rcall	Str2Val
 brcc	cmd_SP_2

 st	X+, r24
 st	X+, r25
 adiw	X, 1

cmd_SP_2:
 ld	r16, Y
 cpi	r16, ' '
 brne	cmd_SP_3
 adiw	Y, 1
 dec	r22
 brne	cmd_SP_1

cmd_SP_3:
 ldi	XL, low(CurrentDirEnt)
 ldi	XH, high(CurrentDirEnt)
 ldi	r22, MaxDirDeep

 ldi	r25, 13
 rcall	SendByte

cmd_SP_4:
 ld	r24, X+
 ld	r25, X+
 rcall	WordOut
 ldi	r25, ':'
 rcall	SendByte
 ld	r25, X+
 rcall	ByteOut
 ldi	r25, ' '
 rcall	SendByte
 dec	r22
 brne	cmd_SP_4

cmd_SP_ret:
 ret

;

cmd_GetName:
 rcall	file_Idnt
 rcall	cmd_IDE_IOSect2

 clr	r22   

cmd_GN_01:
 ldi	r25, '/'
 rcall	SendByte

 inc	r22
 mov	r23, r22
 push	r22
 rcall	file_Name
 pop	r22
 brcs	cmd_GN_02
 lds	r25, fs_errno
 rjmp	ByteOut

cmd_GN_02:
 ld	r25, Z+
 tst	r25
 breq	cmd_GN_03
 rcall	SendByte
 rjmp	cmd_GN_02

cmd_GN_03:
 tst	r23
 breq	cmd_GN_01

 ldi	r25, 13
 rcall	SendByte

;

cmd_GetName_Lite:
 ldi	r16, 0x80
 call	lcd_SetAttr
 ldi	r17, 1
 call	lcd_GoToLine

 clr	r23
 rcall	file_Name		; ����� �����
 ldi	r18, lcd_XPosMax*3
 brcs	cmd_GN_1
 lds	r25, fs_errno
 rjmp	ByteOut

cmd_GN_1:
 ld	r25, Z+
 tst	r25
 breq	cmd_GN_2
 rcall	SendByte
 call	lcd_CharOutNoLimit
 rjmp	cmd_GN_1

cmd_GN_2:
 jmp	lcd_ClrEolNoLimit

;

cmd_TypeFile:
 rcall	file_Open
 brcc	cmd_SP_ret

cmd_TF_1:
; rcall	print
;.db	13, 13, 13, 13, ">>>>>>>", 1
; lds	r24, CurrentCluster+0
; lds	r25, CurrentCluster+1
; rcall	WordOut
; rcall	print
;.db	"<<<<<<<", 13, 13, 13, 13, 1

 ldi	ZL, low(IDEBuf0)
 ldi	ZH, high(IDEBuf0)
 rcall	file_Read
 brcc	cmd_SP_ret

 ldi	ZL, low(IDEBuf0)
 ldi	ZH, high(IDEBuf0)
 clr	r16

cmd_TF_2:
 ld	r25, Z+
 rcall	SendByte
 ld	r25, Z+
 rcall	SendByte
 dec	r16
 brne	cmd_TF_2

 rcall	file_Step
 brcs	cmd_TF_1
ret5:
 ret

;

cmd_SendFile:
 rcall	cmd_GetName_Lite
 
 rcall	file_Idnt
 brcc	cmd_SF_2

 rcall	file_OpenAndHold
 brcc	ret5

 call	vlsi_Open
 tst	r20
 breq	cmd_SF_1
 ldi	r25, 'V'
 rcall	SendByte
 mov	r25, r20
 rcall	ByteOut
 jmp	vlsi_Init

cmd_SF_1:
 ldi	ZL, low(IDEBuf0)
 ldi	ZH, high(IDEBuf0)
 rcall	file_Read
 brcc	ret5

 rcall	cmd_SF_HI
 
 lds	r19, kb_Keys

 cpi	r19, Key3mask
 breq	cmd_SF_2	; ���������

 cpi	r19, Key7mask
 brne	cmd_SF_3

cmd_SF_4:
 rcall	file_Prev
 rcall	file_Idnt
 brcc	cmd_SF_4
 rjmp	cmd_SendFile

cmd_SF_3:

 cpi	r19, Key3mask | Key7mask
 brne	cmd_SF_5
 rcall	pc_Menu
 rjmp	cmd_SendFile

cmd_SF_5:
 cpi	r19, Key1mask
 brne	cmd_SF_6
 rcall	pc_NextDir
 rjmp	cmd_SendFile
cmd_SF_6:
 cpi	r19, Key5mask
 brne	cmd_SF_7
 rcall	cmd_MenuTest
 rjmp	cmd_SendFile
cmd_SF_7:

 ldi	ZL, low(IDEBuf0)
 ldi	ZH, high(IDEBuf0)
 call	vlsi_SendBuf

 sbic	UCSRA, RXC
ret7:
 ret

 rcall	file_Step
 brcs	cmd_SF_1

 call	vlsi_Close

cmd_SF_2:
 rcall	file_Next
 brcs	cmd_SendFile
 ret

;

cmd_SF_HI:		; ����� �������������� ��� ��������������� �����
 call	kb_GetKeys

; 0 ������ - ��������

 ldi	r16, 0x00
 call	lcd_SetAttr
 clr	r17
 call	lcd_GoToLine

 call	vlsi_SetTones

 lds	r25, vlsi_Head+0
 call	lcd_CharOut
 lds	r25, vlsi_Head+1
 call	lcd_CharOut
 lds	r25, vlsi_Head+2
 call	lcd_CharOut
 lds	r25, vlsi_Head+3
 call	lcd_CharOut

 ldi	r25, ' '
 call	lcd_CharOut

 lds	dd16uL, vlsi_BitRate+0
 lds	dd16uH, vlsi_BitRate+1
 call	DecWordOut

 ldi	r25, ' '
 call	lcd_CharOut

 lds	dd16uL, vlsi_Time+0
 lds	dd16uH, vlsi_Time+1
 call	DecWordOut
 call	lcd_ClrEol

; 4 ������ - ���������

 ldi	r17, 4
 call	lcd_GoToLine

 ldi	r16, 0x80
 call	lcd_SetAttr

 lds	dd16uL, vlsi_Vol
 clr	dd16uH
 call	DecWordOut

 ldi	r25, ' '
 call	lcd_CharOut

 lds	dd16uL, vlsi_Pan
 clr	dd16uH
 call	DecWordOut

 rcall	lcd_ClrEol

; 4 ������ - ������ ������

 lds	r19, kb_Keys
 clr	r16
 clr	r17
 sbrc	r19, Key6n
 ldi	r16,  1
 sbrc	r19, Key2n
 ldi	r16, -1
 call	vlsi_ChVol

; 5-� ������ - ���� � ���������

 ldi	r17, 5
 rcall	lcd_GoToLine
 lds	r19, kb_Keys
 clr	r16
 sbrc	r19, Key4n
 ori	r16, 1<<vlsi_SM_FFWD
 sts	vlsi_Mode, r16

 mov	dd16uL, r16
 clr	dd16uH
 call	DecWordOut
 rcall	lcd_ClrEol

;

 ldi	r25, '.'
 rcall	SendByte

ret8:
 ret

;

cmd_FilePrev:
 rcall	file_Prev
 rjmp	cmd_FN_1

cmd_FileNext:
 rcall	file_Next

cmd_FN_1:
 brcc	cmd_FN_3
 ldi	r25, 13
 rcall	SendByte
 ldi	XL, low(CurrentDirEnt)
 ldi	XH, high(CurrentDirEnt)

cmd_FN_2:
 ld	r24, X+
 ld	r25, X+
 rcall	WordOut
 ldi	r25, '/'
 rcall	SendByte
 ld	r16, X+
 andi	r16, 0x40
 breq	cmd_FN_2

 rjmp	cmd_GetName
;
cmd_FN_3:
 ldi	r25, '?'
 rjmp	SendByte

;

cmd_PrevOnDeep:
 rcall	Str2Val
 brcc	ret8
 mov	r23, r24
 rcall	file_PrvD
 rjmp	cmd_SD_IOSect2

cmd_NextOnDeep:
 rcall	Str2Val
 brcc	ret8
 mov	r23, r24
 rcall	file_NxtD
 rjmp	cmd_SD_IOSect2


;;
;; SPI ���������
;;
cmd_SPI_CLKen:
 rcall	Str2Val
 brcc	ret8
 mov	r16, r24
 jmp	spi_CSCLK_Select

;

cmd_SPI_IO:
 rcall	Str2Val
 brcc	cmd_SPI_IO_1

 mov	r16, r24
 call	spi_IO
 mov	r25, r16
 rcall	ByteOut

cmd_SPI_IO_1:
 ld	r25, Y
 cpi	r25, ' '
 brne	ret8
 rcall	SendByte
 adiw	Y, 1
 rjmp	cmd_SPI_IO

;;
;; ��������� � LCD
;;
cmd_LCD_text:
 ldi	r17, 3
 rcall	lcd_GoToLine
cmd_LCD_t2:
 ld	r25, Y
 cpi	r25, 13
 breq	ret8
 adiw	Y, 1

 mov	r24, r25
 andi	r24, 0xFC
 cpi	r24, 0x30
 brne	cmd_LCD_t3

 ror	r25
 ror	r25
 ror	r25
 mov	r16, r25
 call	lcd_SetAttr
 rjmp	cmd_LCD_t2

cmd_LCD_t3:
 rcall	lcd_CharOut
 rjmp	cmd_LCD_t2

;

cmd_MenuTest:
 ldi	r22, low(cmd_MT_Desc*2)
 ldi	r23, high(cmd_MT_Desc*2)
 jmp	ur_Menu
;
cmd_MT_Desc:
 .db	                   (1<<ur_m_LeftEn) | (1<<ur_m_OkEn) | (1<<ur_m_RightEn) | (1<<ur_m_SelEn) | (1<<ur_m_OkFin), 0x00
 .dw	cmd_MT_Txt1*2
 .dw	0
 .dw	pc_Menu
 .dw	pc_Menu
 .dw	pc_Menu

 .db	(1<<ur_m_DrawEn) | (1<<ur_m_LeftEn) | (1<<ur_m_OkEn) | (1<<ur_m_RightEn) | (1<<ur_m_SelEn) | (1<<ur_m_OkFin), 0x00
 .dw	cmd_MT_Txt2*2
 .dw	cmd_MT_DevDraw
 .dw	cmd_MT_DevSel
 .dw	cmd_MT_DevChg
 .dw	cmd_MT_DevSel

 .db	(1<<ur_m_DrawEn) | (1<<ur_m_LeftEn) | (1<<ur_m_OkEn) | (1<<ur_m_RightEn) | (1<<ur_m_SelEn) | (1<<ur_m_OkFin), 0x00
 .dw	cmd_MT_Txt3*2
 .dw	cmd_MT_PartDraw
 .dw	cmd_MT_PartSelL
 .dw	cmd_MT_PartChg
 .dw	cmd_MT_PartSelR

 .db	                   (1<<ur_m_LeftEn) | (1<<ur_m_OkEn) | (1<<ur_m_RightEn)                                    , 0x00
 .dw	cmd_MT_Txt4*2
 .dw	0
 .dw	0
 .dw	0
 .dw	0

 .db	(1<<ur_m_DrawEn) | (1<<ur_m_LeftEn) |                  (1<<ur_m_RightEn) | (1<<ur_m_SelEn)                  , 0x00
 .dw	cmd_MT_Txt5*2
 .dw	cmd_MT_BassDraw
 .dw	cmd_MT_BassChg
 .dw	0
 .dw	cmd_MT_BassChg

 .db	(1<<ur_m_DrawEn) | (1<<ur_m_LeftEn) |                  (1<<ur_m_RightEn) | (1<<ur_m_SelEn)                   | (1<<ur_m_Close), 0x00
 .dw	cmd_MT_Txt6*2
 .dw	cmd_MT_DiffDraw
 .dw	cmd_MT_DiffChg
 .dw	0        
 .dw	cmd_MT_DiffChg

cmd_MT_Txt1:
 .db	"������� ����...", 0
cmd_MT_Txt2:
 .db	"����������: ", 0
cmd_MT_Txt3:
 .db	"������    : ", 0
cmd_MT_Txt4:
 .db	"�����     : ", 0
cmd_MT_Txt5:
 .db	"������� �������: ", 0
cmd_MT_Txt6:
 .db	"�������� ������ : ", 0

;;

cmd_MT_DevDraw:
 ldi	r25, 'H'
 lds	r16, CurrentDevice
 tst	r16
 brne	cmd_MT_DD_1
 ldi	r25, 'S'
cmd_MT_DD_1:
 rjmp	lcd_CharOut

cmd_MT_DevSel:
 ldi	r17, 0x01
 lds	r16, CurrentDevice
 eor	r16, r17
 sts	CurrentDevice, r16
 ret

cmd_MT_DevChg:
 jmp	file_DvCh
 
;;

cmd_MT_PartDraw:
 lds	dd16uL, CurrentPart
 jmp	DecByteOut
 
cmd_MT_PartSelL:
 lds	r16, CurrentPart
 dec	r16
 sts	CurrentPart, r16
 ret
 
cmd_MT_PartChg:
 jmp	file_DvCh

cmd_MT_PartSelR:
 lds	r16, CurrentPart
 inc	r16
 sts	CurrentPart, r16
 ret

;;

cmd_MT_BassDraw:
 ldi	r25, 'N'
 lds	r16, vlsi_Mode
 sbrc	r16, vlsi_SM_BASS
 ldi	r25, 'Y'
 rjmp	lcd_CharOut

cmd_MT_BassChg:
 ldi	r17, (1<<vlsi_SM_BASS)
 lds	r16, vlsi_Mode
 eor	r16, r17
 sts	vlsi_Mode, r16
 ret

;;

cmd_MT_DiffDraw:
 ldi	r25, 'N'
 lds	r16, vlsi_Mode
 sbrc	r16, vlsi_SM_DIFF
 ldi	r25, 'Y'
 rjmp	lcd_CharOut

cmd_MT_DiffChg:
 ldi	r17, (1<<vlsi_SM_DIFF)
 lds	r16, vlsi_Mode
 eor	r16, r17
 sts	vlsi_Mode, r16
 ret


;;
;; ������/������ ���������� ������ ������ (EEPROM)
;;
cmd_WFlash:
 rcall	f_IO
.db	f_OWrite, 0
.dw	256*0
.dw	IDEBuf0
 rcall	f_IO
.db	f_OWrite, 0
.dw	256*1
.dw	IDEBuf0+256
 rcall	f_IO
.db	f_OWrite, 0
.dw	256*2
.dw	IDEBuf1
 rcall	f_IO
.db	f_OWrite, 0
.dw	256*3
.dw	IDEBuf1+256
 ret

;

cmd_RFlash:
 rcall	f_IO
.db	f_ORead, 0
.dw	256*0
.dw	IDEBuf0
 rcall	f_IO
.db	f_ORead, 0
.dw	256*1
.dw	IDEBuf0+256
 rcall	f_IO
.db	f_ORead, 0
.dw	256*2
.dw	IDEBuf1
 rcall	f_IO
.db	f_ORead, 0
.dw	256*3
.dw	IDEBuf1+256
 ret

;;
;; ����������� ��������
;;
cmd_Debug:
 rcall	Str2Val
 brcc	cmd_D_1
 sts	DebugVal+0, r24
 sts	DebugVal+1, r25
cmd_D_1:
 lds	r24, DebugVal+0
 lds	r25, DebugVal+1
 rjmp	WordOut

;;
;; ��������� ����������� ��������� ����������
;;
cmd_Speed:
 rcall	Str2Val
 brcc	cmd_S_1
 out	OSCCAL, r24
cmd_S_1:
 in	r25, OSCCAL
 rjmp	ByteOut

;;
;; ���������� ������� �� ��������
;;
cmd_Help:
 rcall	print
;.db	"Release date %DAY%.%MONTH%.%YEAR% %HOUR%:%MINUTE%", 13
.db	13
.db	"]    - Get IDE regs", 13
.db	"[A N - Put IDE regs", 13
.db	"<    - Buf from IDE", 13
.db	"|    - View IDE buf0", 13
.db	"-    - View IDE buf1", 13
.db	">    - Buf to IDE", 13
.db	"Rn.m - Read Sector IDE", 13
.db	"Wn.m - Write Sector IDE", 13
.db	"zn.m - Read Sector SD", 13
.db	"xn.m - Write Sector SD", 13
.db	13
.db	"pDP  - media_init(D/P), fs_Init", 13
.db	"nN   - NextFATEntry", 13
.db	"Pn..n- Set path", 13
.db	"m    - Menu path", 13
.db	"(N   - Prev entry on deep N", 13
.db	")N   - Next entry on deep N", 13
.db	"N    - Get name", 13
.db	"t    - Type file", 13
.db	"{    - Prev file", 13
.db	"}    - Next file", 13
.db	13
.db	"Ttext- LCD test", 13
.db	"o    - Menu test", 13
.db	13
.db	"Sn   - Set CLKen byte", 13
.db	"sn   - SPI IO", 13
.db	13
.db	"r   - R Flash to IDEBuf0/1", 13
.db	"w   - W Flash from IDEBuf0/1", 13
.db	13
.db	"kN  - new/read OSCCAL", 13
.db	"lN  - set/get DEBUG", 13
.db	13
.db	"?  - This text", 13, 1
 ret

;;
;; ��������� ������ AnalyseString
;;
cmd_Break:
 pop	r25
 pop	r25
 ret
