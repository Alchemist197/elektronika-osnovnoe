;;==========================================================================
;; ������������� ��������������� ��������� ����������� ����������������� ����������
;;

.dseg
ur_Menu_Flags:		.byte	1	; D7 - ���������� ����������� ������, D6 - ����� ��������� ������

.cseg
;
; ����������� ���������� � �������� ���������� � ����������� �� ������� �������.
;
; ������� ������� ������� ����� �� ������� ���������
; ������: .db [��� ���������� ������], [�����: D7 = 1 - ��������� ������,
; D6 = 1 - ��������� ���������� �������] \\ .dw [���������-����������]
;
ur_KeybRoute:
 rcall	kb_GetKeys

ur_KeybRoute_NoGetKeys:
 pop	ZH		; ����������� ��������� ������
 pop	ZL
 lsl	ZL
 rol	ZH

ur_KR_2:
 lpm	r16, Z+		; ���������� ������
 lpm	r17, Z+		; ����� ������

 lpm	XL, Z+		; ��������� - ����������
 lpm	XH, Z+

 lds	r18, kb_Keys
 cp	r16, r18
 brne	ur_KR_1
 
 movw	r24, ZL		; Z ^= X, X ^= Z: X <-> Z
 movw	ZL, XL
 movw	XL, r24

 push	r17
 push	XL
 push	XH

 sbrs	r17, 6		; ���������� ���������� �������
 rjmp	ur_KR_3
 rcall	kb_WaitReleaseKeys
ur_KR_3:

 icall

 pop	XH
 pop	XL
 pop	r17

 movw	ZL, XL

ur_KR_1:
 sbrs	r17, 7
 rjmp	ur_KR_2

 lsr	ZH		; ����� !
 ror	ZL
 ijmp

;
; ������� ����, ��������� ��������, ����� ������� ���������� � r22.r23:
;  0 .db [����� ����������� ����������� ����� DW, D7 = 1 - ���� ����� ���������,
;         D6 = 1 - ������� ���������� ����� ���������� Ok, D5 = 1 - ����� ����� ���� ������], [������� ������ (X)]
;  2 .dw [   ����� ASCIZ ������]
;  4 .dw [D0 ����� ��������� Draw (����������� ����� ������ ������)]
;  6 .dw [D1 ����� ��������� Left]
;  8 .dw [D2 ����� ��������� Ok]
; 10 .dw [D3 ����� ��������� Right]
; 12
; ����� �� �����, ��� ���������� �� ������ - ��������� ���.
; ������ � ��������� ����� ������ ���� ����������� (Attr.D5 = 1)
;
.def	ur_m_pMenuL	= r22
.def	ur_m_pMenuH	= r23
.def	ur_m_Curs	= r21
.def	ur_m_Attr	= r20
.def	ur_m_Cnt	= r19
			; r16..18 �� ����������������
			; r24..31 �� ����������������

.equ	ur_m_DrawEn	= 0
.equ	ur_m_LeftEn	= 1
.equ	ur_m_OkEn	= 2
.equ	ur_m_RightEn	= 3
.equ	ur_m_SelEn	= 5
.equ	ur_m_OkFin	= 6
.equ	ur_m_Close	= 7

; (1<<ur_m_DrawEn) | (1<<ur_m_LeftEn) | (1<<ur_m_OkEn) | (1<<ur_m_RightEn) | (1<<ur_m_SelEn) | (1<<ur_m_OkFin) | (1<<ur_m_Close)

ur_Menu:
 rcall	lcd_ClrScr
 clr	ur_m_Curs	; ������ � ������ ������

ur_M_2:
 rcall	ur_M_Draw

ur_M_1:
 rcall	ur_KeybRoute
.db	Key1mask, 0x40
.dw	ur_M_Ok

.db	Key0mask, 0x40
.dw	ur_M_Exit

.db	Key2mask, 0x00
.dw	ur_M_Up

.db	Key6mask, 0x00
.dw	ur_M_Left

.db	Key4mask, 0x00
.dw	ur_M_Down

.db	Key7mask, 0x80
.dw	ur_M_Right

 rcall	Wait50ms

 lds	r16, ur_Menu_Flags
 sbrc	r16, 6
 rjmp	kb_WaitReleaseKeys	; ������ ���������� (�� ESC ��� Ok && OkFin)

 sbrc	r16, 7
 rjmp	ur_M_2			; ���� ��������� ����� ���� ������ ���������������� �������� - �������������� ������

 lds	r16, kb_Keys
 tst	r16
 breq	ur_M_1			; ���� ������������� ������ �� �������� - ����� �� ģ����� �����
 rjmp	ur_M_2			; ���� ������������ ����

;; �������� �������� ���������������� ���������, �������� ������� ���������

ur_M_ICall:
 lpm	XL, Z+
 lpm	XH, Z+
 movw	ZL, XL
 push	ur_m_pMenuL
 push	ur_m_pMenuH
 push	ur_m_Curs
 push	ur_m_Attr
 icall
 pop	ur_m_Attr
 pop	ur_m_Curs
 pop	ur_m_pMenuH
 pop	ur_m_pMenuL
 lds	r16, ur_Menu_Flags
 sbr	r16, 0x80
 sts	ur_Menu_Flags, r16
 ret

;; ������� ���� �� �����

ur_M_Draw:
 clr	ur_m_Cnt
ur_M_D_1:
 rcall	ur_M_DrawItem
 inc	ur_m_Cnt

 sbrs	ur_m_Attr, 7
 rjmp	ur_M_D_1

 clr	r16
 sts	ur_Menu_Flags, r16
 ret

;; ������� ���� ������� �� �����

ur_M_DrawItem:
 rcall	ur_M_CalcPointer	; ������ �� �����
 mov	r17, ur_m_Cnt
 adiw	Z, 1
 lpm	r18, Z+
 rcall	lcd_GoToXY

 ldi	r16, 0x00		; ������������� �������� ������ ������
 cp	ur_m_Cnt, ur_m_Curs
 brne	ur_M_DI_1
 ldi	r16, 0x80
ur_M_DI_1:
 rcall	lcd_SetAttr

 lpm	XL, Z+			; ������� �����
 lpm	XH, Z+
 movw	ZL, XL
ur_M_DI_3:
 lpm	r25, Z+
 tst	r25
 breq	ur_M_DI_2
 rcall	lcd_CharOut
 rjmp	ur_M_DI_3
ur_M_DI_2:
 rcall	ur_M_CalcPointer	; ��������������� Z

 push	ur_m_Cnt		; ����� �������������� ��������� ������ ������
 adiw	Z, 4
 sbrc	ur_m_Attr, ur_m_DrawEn
 rcall	ur_M_ICall
 pop	ur_m_Cnt

 ldi	r16, 0x00
 rcall	lcd_SetAttr
 rjmp	lcd_ClrEol

;; ��������� �� ������� ur_m_Cnt � ������ ������� ur_m_pMenu ����� ��������� ������ � ���� ������
;; ��������� � Z � ur_m_Attr

ur_M_CalcPointerCurs:
 mov	ur_m_Cnt, ur_m_Curs

ur_M_CalcPointer:
 movw	ZL, ur_m_pMenuL
 mov	r17, ur_m_Cnt		; r17 = ur_m_Cnt * 3
 lsl	r17
 add	r17, ur_m_Cnt

 lsl	r17			; r17 *= 4 [ �����: r17 = ur_m_Cnt * 3 * 4 ]
 lsl	r17

 clr	r16
 add	ZL, r17
 adc	ZH, r16
 lpm	ur_m_Attr, Z
ur_M_ret0:
 ret

;; ������� �� �������

ur_M_Up:
 tst	ur_m_Curs
 breq	ur_M_ret0
 dec	ur_m_Curs
 rcall	ur_M_CalcPointerCurs
 sbrs	ur_m_Attr, ur_m_SelEn
 rjmp	ur_M_Up
 ret

ur_M_Down:
 rcall	ur_M_CalcPointerCurs
 sbrc	ur_m_Attr, ur_m_Close
 ret
 inc	ur_m_Curs
 rcall	ur_M_CalcPointerCurs
 sbrs	ur_m_Attr, ur_m_SelEn
 rjmp	ur_M_Down
 ret

ur_M_Ok:
 rcall	ur_M_CalcPointerCurs
 adiw	Z, 8
 sbrc	ur_m_Attr, ur_m_OkEn
 rcall	ur_M_ICall
 sbrs	ur_m_Attr, ur_m_OkFin
 ret
ur_M_Exit:
 lds	r16, ur_Menu_Flags
 sbr	r16, 0x40
 sts	ur_Menu_Flags, r16
 ret

ur_M_Left:
 rcall	ur_M_CalcPointerCurs
 adiw	Z, 6
 sbrc	ur_m_Attr, ur_m_LeftEn
 rcall	ur_M_ICall
 ret

ur_M_Right:
 rcall	ur_M_CalcPointerCurs
 adiw	Z, 10
 sbrc	ur_m_Attr, ur_m_RightEn
 rcall	ur_M_ICall
 ret
