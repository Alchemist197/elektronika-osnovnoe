;;==========================================================================
;; ��������� �������� ������ ��������� FAT
;; C == 1 � ������ ������ ���������� ��������
;; ���������� ��������� �� ���������
;;

.dseg
; ��������� ������� �������� �������
fsFAT_Sector2Cluster:		.byte	1	; �������� � ��������
fsFAT_ReservedSectors:		.byte	2	; �������� ������ FAT �� ������ �������
fsFAT_RootOffset:		.byte	4	; �������� ������ Root ��� FAT12, FAT16, ��� FAT32 ������� ������ Root
fsFAT_ZeroClusterOffset:	.byte	4	; �������� �������� (���������������) ��������
fsFAT_Entry2Root:		.byte	2	; ��������� ��������� �������� ��� FAT12, FAT16, ��� FAT32 == 0
fsFAT_Sector2Root:		.byte	1	; �������� ��������� �������� ��� FAT12, FAT16, ��� FAT32 == 0
fsFAT_FAT_Type:			.byte	1	; 0 - FAT16, 1 - FAT12, 2 - FAT32
.equ	fsFAT_LFN_Size		= 63		; ����� ��� ������ �������� �����
fsFAT_LFN:			.byte	fsFAT_LFN_Size+1 ; ����������� ���������� LFN �������� � ����, ��� ���� ����� ����������� � ����� !
fsFAT_LFN_Ptr:			.byte	1	; ��������� �� ��������� ����������� ������ LFN
						; 0 - ���� �� ���� ������ LFN, 1 - ���� ����� ����������
.cseg
;
; ���������� Boot Block � �������������� � ����� � ��� ����������
; errno:
; 0 - ������ ������ Boot Block
; 1 - ������� ����� ���� � �������
; 2 - ������� ����� �������� � ��������
; 3 - ������������ �� ����� ���ޣ�� �������� 2-�� �������� ��� Root
; 4 - ������� ��������� Boot Block
;
fsFAT_Fault1:
 clc
 ret

fsFAT_Init:
 clr	r16
 sts	fs_errno, r16

 clr	r4				; ������ ������
 clr	r5
 clr	r6
 clr	r7
 ldi	ZL, low(IDEBuf0)
 ldi	ZH, high(IDEBuf0)
 rcall	media_Read
 rcall	fsFAT_errtest
;
 ldi	ZL, low(IDEBuf0)
 ldi	ZH, high(IDEBuf0)
;
 ldd	r16, Z+0x0B			; �������� "���� � �������" = 512. � ���������� ���
 tst	r16 				; ��������� ����������
 brne	fsFAT_Fault1
 ldd	r16, Z+0x0C
 cpi	r16, 0x02
 brne	fsFAT_Fault1
 rcall	fsFAT_inc_errno
;
 ldd	r16, Z+0x0D
 sts	fsFAT_Sector2Cluster, r16
 tst	r16				; �������� ��������� � ���������� ���: 1, 2, 4, 8, 16, 32, 64, 128
 breq	fsFAT_Fault1			; ������� �������� �����������
fsFAT_I_1:
 lsl	r16
 brcc	fsFAT_I_1			; ������� ��������� 1 � C
 brne	fsFAT_Fault1			; ���� ��� ���� � ����� ���� �ݣ ���� ������� - ��� ���� ������
 rcall	fsFAT_inc_errno
;
 ldd	r0, Z+0x0E
 sts	fsFAT_ReservedSectors+0, r0	; �������� ������ FAT �� ������ �������
 ldd	r1, Z+0x0F
 sts	fsFAT_ReservedSectors+1, r1
;
 ldd	r18, Z+0x11			; ���������� ������� � Root
 sts	fsFAT_Entry2Root+0, r18		; � FAT32 == 0
 ldd	r19, Z+0x12
 sts	fsFAT_Entry2Root+1, r19
;
 ldd	r2, Z+0x10			; ��������� � ���������� �������� Root
 ldd	r4, Z+0x16			; ���������� �������� FAT, ��� FAT32 == 0
 ldd	r5, Z+0x17
 clr	r6
 clr	r7

 clr	r16
 tst	r5				; ����� ����� ������ ��� FAT
 brne	fsFAT_I_2

 mov	r17, r4
 cpi	r17, 13
 brsh	fsFAT_I_2			; == FAT16

 inc	r16
 tst	r4
 brne	fsFAT_I_2			; == FAT12

 inc	r16				; == FAT32

 ldd	r4, Z+0x24			; ������ ������ FAT ����� ����� � ������ FAT32
 ldd	r5, Z+0x25
 ldd	r6, Z+0x26
 ldd	r7, Z+0x27

fsFAT_I_2:
 sts	fsFAT_FAT_Type, r16

 rcall	mpy32u				; ����� ����� FAT * ����� �������� � FAT...
 rcall	fsFAT_errtest
 add	r8, r0				; ... + �������� ������ FAT = ...
 adc	r9, r1
 clr	r0
 adc	r10, r0
 adc	r11, r0

; ... ������ ��������� �������� ��� FAT12/FAT16 � ������ 2-�� �������� ��� FAT32
; (� FAT32 ������ ��������� �������� ������ ��� 0, ������� ������ ���������� �� ��������)
 rcall	fsFAT_fsFAT_RootOffset_r8
;
; ��� �������� FAT12 �� FAT16 � �� FAT32 ? ��� ������� ��������� � ������������:
; MTools:
; fat12 - ���� ����� ��������� ������ 4085 [�� ���ޣ�� ���������� ������������ / ������ ��������]
; fat32 - ���� ����� �������� �� ������� fat == 0. ���� ������ �� ���� ������ ����� ������� - � ��������� ���.
; ������ �ӣ ���� ������ ���������� ����� ������ 2-�� �������� ������� ��� fat32 root == 0...

; FreeBSD 6.1 kernel:
; * Microsoft Programmer's Reference says if the maximum cluster number in a filesystem is greater
; * than 4078 ((CLUST_RSRVS - CLUST_FIRST) & FAT12_MASK) then we've got a 16 bit fat filesystem.
;[ �������, RSRVS = 0xFF6, � FIRST = 2, �� 0xFF4 = 4084, � ����� �� 4078.
;  ��������� � ���� ���� ������ �� ������� "<= (CLUST_*)", ��� ������������� mtools.
;  ���������, ��� �� ������������� �ݣ ���ͣ� sevilija ���� ����������, ��� 4080 ���������
;  Symante������� ������� ������� �� FAT12 EOF'��. ����� ���� ��������� 4078 ���� ���� �� ������ � ��� ��
;  M$ Programmer's Reference ? ]
; fat32 - ���� ����� ������� root == 0

; �� ��� ��� ������� ��������� �������� � �� �������� � �������� ��� ������ ����� ����,
; � ���������� ����� ������� ���������: ���� ����� ��������, ����ģ���� ��� ������� FAT > 12 -> ��� FAT16
; 12 �������� * 512 ���� / 1.5 �����/������� = 4096 ���������. � ������ ������� ��� ����� ��������� � �������.
; ���� ����� �������� == 0 -> ��� FAT32
;
 lsr	r19 				; ��������� �������� �������� ��������
 ror	r18				; (RootOffset + RootEntry / (EntryPerSector=16)) - (sec2clus * 2)
 lsr	r19
 ror	r18
 lsr	r19
 ror	r18
 lsr	r19
 ror	r18				; r19:r18 = Sector2Root (��� FAT32 == 0)
 sts	fsFAT_Sector2Root, r18

 clr	r16
 add	r8, r18				; r8..r11 = RootOffset
 adc	r9, r19
 adc	r10, r16			; r8..r11 += Sector2Root
 adc	r11, r16			; ������ � r8..r11 �������� ������� ��������

 lds	r18, fsFAT_Sector2Cluster
 clr	r19
 lsl	r18
 rol	r19				; ������ � r19:r18 ����� �������� � �������� * 2

 sub	r8, r18
 sbc	r9, r19
 sbc	r10, r16
 sbc	r11, r16			; m16u -= sec2clus * 2

 sts	fsFAT_ZeroClusterOffset+0, r8
 sts	fsFAT_ZeroClusterOffset+1, r9
 sts	fsFAT_ZeroClusterOffset+2, r10
 sts	fsFAT_ZeroClusterOffset+3, r11	; ��� FAT32 ���� ��� ������ ��������� � fsFAT_RootOffset
;
 lds	r16, fsFAT_FAT_Type
 cpi	r16, 2
 brne	fsFAT_I_3
 ldd	r8, Z+0x2C			; ������� Root ��� FAT32
 ldd	r9, Z+0x2D
 ldd	r10, Z+0x2E
 ldd	r11, Z+0x2F
 rcall	fsFAT_fsFAT_RootOffset_r8
fsFAT_I_3:
;
 subi	ZH, -(2)			; Z += 0x200
 subi	ZL, 2				; Z -= 2	->	Z += 0x1FE
 ld	r16, Z+				; �������� ��������� Boot Block
 cpi	r16, 0x55
 brne	fsFAT_Fault0
 ld	r16, Z+
 cpi	r16, 0xAA
 brne	fsFAT_Fault0
;
fsFAT_OK3:
 sec
 ret

;
; �������� ����� �������� ���� � ���������� ������ �� ��� ������
; errno:
; 0, 1 - fsFAT_GetDir
;
fsFAT_Open:
 rcall	fsFAT_GetDir_MaxDeep
 clr	r16
 sts	CurrentSecInCl, r16
 ret

fsFAT_Fault0:
 clc
 ret

;
; ������ ������� ���������, ���� �����, �������� � CurrentDirEnt, ���������������, ����� ����.
; r23 - ������� ������� �������. r23 = 0 - ������ �� ����� ��� ������, ����� ���������� ������ �� ������� r23
;
; errno:
; 0 - �� �������� ������� (������ �������� ��� ������)
; 1 - ���� �� ������ (��������� ����� �� ��������������� ���������) ���� ��������� ������� ��������
;
; ���� ������ ��� (C = 1), ���������� � Z ��������� �� ��������� ���������� ��������,
; CurrentCluster = r8..r11 = ������ ������� �����, errno = 2
;
fsFAT_GetDir_MaxDeep:
 clr	r23
fsFAT_GetDir:
 ldi	XL, low(CurrentDirEnt)
 ldi	XH, high(CurrentDirEnt)
 ldi	r22, MaxDirDeep

 set				; FAT12/16

 lds	r16, fsFAT_FAT_Type
 cpi	r16, 2
 brne	fsFAT_GD_1

 clt				; FAT32
 rcall	fsFAT_r8_fsFAT_RootOffset
 rcall	fsFAT_CurrentCluster_r8	; CurrentCluster := RootOffset

fsFAT_GD_1:
 clr	r16
 sts	fs_errno, r16

 ld	r20, X+
 ld	r21, X+

 mov	r17, r20
 or	r17, r21
 breq	fsFAT_GD_2		; ���� �� ���� ����� ������ �� ��������...
 ldi	r16, 0x10		; ...�� ţ ����� ���������
fsFAT_GD_2:
 st	X, r16

 rcall	fsFAT_GetDirEntry
 in	r17, SREG
 rcall	fsFAT_errtest

 rcall	fsFAT_CurrentCluster_r8

 rcall	fsFAT_inc_errno

 ld	r16, X
 sbr	r16, 0xE0		; ��� ����� ����������������, ��� ���������, ţ ����� �����������...

 out	SREG, r17
 brts	fsFAT_GD_3
 brne	fsFAT_GD_3
 cbr	r16, 0x20		; ...ţ, �����������, ����� ��������� [T = 0, Z = 1]...

 ld	r19, -X			; ��������� ��������� ����������� ���������� �������� ������ ����� ��� ������� ��������
 ld	r18, -X
 sub	r18, r20
 sbc	r19, r21
 st	X+, r18
 st	X+, r19

fsFAT_GD_3:
 st	X, r16

 out	SREG, r17
 brtc	fsFAT_Fault0		; ������ �� �������� - File not Found
 breq	fsFAT_Ok3		; ������ ���� !

 dec	r23			; r23 ����� ���������� ������� ������, ���� ����� ������ ������
 breq	fsFAT_Ok3		; ��� �������������� �������� (�����������) [��� fsFAT_Name]

 clt
 dec	r22
 breq	fsFAT_Fault0		; ��������� ������� �������

 cbr	r16, 0x40		; ...�ӣ ����, ��� ����� ��������� �� ��������� [T = Z = 1]
 st	X+, r16

 rjmp	fsFAT_GD_1

fsFAT_Fault5:
 clc
 ret

;
; ������ �������, ������� �������� ����� � CurrentCluster (T = 0) ���� Root (T = 1), ���� ��� # r21:r20
; ���������� T = 0, ���� ������� �����������
; ��� T = 1: Z = 0 - ������� �������� ���������, Z = 1 - ������, *Z ��������� �� �������, r8..r11 �������� ��������� �������
; ��� T = 0: Z = 0 - ������� �� �������� ����������� (Volume Label, Deleted, Size=0...), Z = 1 - ����� ��������
; C = 0 � ������ ����� ���� ������� � ������� � ��������
; �������� ��� ������� ��������, ��� ��� ����� ����� ���� ������ �� ��������� ������̣���� �������
;
fsFAT_GetDirEntry:
 clr	r16
 sts	CurrentSecInCl, r16

 clr	r16
 sts	fsFAT_LFN_Ptr, r16

fsFAT_GDE_NextSect:
 ldi	ZL, low(IDEBuf0)
 ldi	ZH, high(IDEBuf0)
 rcall	fsFAT_ReadSect
 brcc	fsFAT_Fault5

 ldi	ZL, low(IDEBuf0)
 ldi	ZH, high(IDEBuf0)
 ldi	r18, 16

fsFAT_GDE_NextRec:
 ld	r16, Z
 tst	r16
 breq	fsFAT_Ok_T0			; ��������� ����� ��������

 mov	r17, r20
 or	r17, r21
 brne	fsFAT_GDE_3			; ���� ��������� ������

; ���������� ����������� ������

 cpi	r16, '.'			; ����������� ���
 breq	fsFAT_GDE_InvalidRecord

 cpi	r16, 0xE5			; ��������� ���
 breq	fsFAT_GDE_InvalidRecord

 ldd	r16, Z+11			; ���������
 andi	r16, 0x08			; "VOL"
 brne	fsFAT_GDE_InvalidRecord

 ldd	r8, Z+0x1A			; Cluster
 ldd	r9, Z+0x1B
 clr	r10
 clr	r11
 lds	r16, fsFAT_FAT_Type
 cpi	r16, 2
 brne	fsFAT_GDE_2
 ldd	r10, Z+0x14
 ldd	r11, Z+0x15
fsFAT_GDE_2:

 ldd	r16, Z+11
 set
 andi	r16, 0x10			; "DIR"
 brne	fsFAT_GDE_ValidRecord		; � ��������� ������ ����� ���� = 0

 ldd	r16, Z+0x1C			; Size
 ldd	r17, Z+0x1D
 ldd	r18, Z+0x1E
 ldd	r19, Z+0x1F
 sts	CurrentSize+0, r16
 sts	CurrentSize+1, r17
 sts	CurrentSize+2, r18
 sts	CurrentSize+3, r19
 or	r16, r17
 or	r16, r18
 or	r16, r19
 breq	fsFAT_GDE_InvalidRecord		; ������� ������
 sez

fsFAT_GDE_ValidRecord:
 rjmp	fsFAT_Ok

fsFAT_GDE_InvalidRecord:
 clz
fsFAT_Ok_T0:
 clt
 rjmp	fsFAT_Ok

; ���� ��������� ������

fsFAT_GDE_3:
 subi	r20, 1
 sbci	r21, 0

 rcall	fsFAT_LFN_Test		; �������� ������ ������ �� ��������� �� LFN

 adiw	Z, 0x20
 dec	r18
 brne	fsFAT_GDE_NextRec

 rcall	fsFAT_IncSec
 brcs	fsFAT_GDE_NextSect
 sez
 rjmp	fsFAT_Ok_T0		; ������������� ����� ����������� �������� ����� ����������� ��� ��� ����������

;
; ������ ��������� ������ �������� ����� � Z+
;
fsFAT_Read:
 clt

;
; ������ ������ �� CurrentCluster/CurrentSecInCl � Z+
; ���� T = 1 -> ������ Root
;
fsFAT_ReadSect:
 brtc	fsFAT_RS_3

 rcall	fsFAT_r8_fsFAT_RootOffset	; ������ ��������� �������� FAT12, FAT16
 movw	r4, r8
 movw	r6, r10

 rjmp	fsFAT_RS_4

fsFAT_Ok:
 sec
 ret

fsFAT_RS_3:
 rcall	fsFAT_r8_CurrentCluster		; ��������� ������ �� ��������
 movw	r4, r8
 movw	r6, r10

 lds	r16, fsFAT_Sector2Cluster

fsFAT_RS_1:
 lsr	r16
 brcs	fsFAT_RS_2
 lsl	r4
 rol	r5
 rol	r6
 rol	r7
 brcc	fsFAT_RS_1			; �������� �� ������������ ��� ���������

fsFAT_Fault2:
 clc
 ret

fsFAT_RS_2:
 lds	r16, fsFAT_ZeroClusterOffset+0
 add	r4, r16
 lds	r16, fsFAT_ZeroClusterOffset+1
 adc	r5, r16
 lds	r16, fsFAT_ZeroClusterOffset+2
 adc	r6, r16
 lds	r16, fsFAT_ZeroClusterOffset+3
 adc	r7, r16

fsFAT_RS_4:
 lds	r16, CurrentSecInCl		; ������ ��������� �������� ������ �������� ��� Root
 add	r4, r16
 clr	r16
 adc	r5, r16
 adc	r6, r16
 adc	r7, r16

 rjmp	media_Read

fsFAT_Ok5:
 sec
 ret

;
; ��������� ������ ����� �� ��������� ������
;
fsFAT_Step:
 lds	r16, CurrentSize+0
 lds	ZL,  CurrentSize+1
 lds	ZH,  CurrentSize+2
 lds	r19, CurrentSize+3
 sbiw	Z, 2				; CurrentSize -= 512
 sbci	r19, 0
 brcs	fsFAT_Fault2			; ��������� < 0. ������ ������ ������ ���
 sts	CurrentSize+0, r16
 sts	CurrentSize+1, ZL
 sts	CurrentSize+2, ZH
 sts	CurrentSize+3, r19
 clt

;
; ������������ CurrentCluster/CurrentSecInCl (��������� � ���������� �������)
; ���� T = 1 -> ��������������� Root
; ���������� C = 0, ���� �������� ���������� (����� �������)
;
fsFAT_IncSec:
 lds	r17, fsFAT_Sector2Cluster
 brtc	fsFAT_IS_1
 lds	r17, fsFAT_Sector2Root
fsFAT_IS_1:
 lds	r16, CurrentSecInCl		; ����� ����������� �������
 inc	r16
 sts	CurrentSecInCl, r16
 cp	r16, r17
 brne	fsFAT_Ok5			; ����� ������� �� ���������
 brts	fsFAT_Fault3			; ���������� ����� Root

 clr	r16				; �������� ��������� ��������� �������
 sts	CurrentSecInCl, r16

;
; �������� CurrentCluster ��������� ��������� �������� �� FAT
; ���������� C = 0 ���� ��������� �������� �����������
; (�� ���������� FAT, EOF, BAD, 0, 1...)
;
fsFAT_NextFATEntry:
 rcall	fsFAT_r8_CurrentCluster

 lds	r4, fsFAT_ReservedSectors+0
 lds	r5, fsFAT_ReservedSectors+1
 clr	r6
 clr	r7

 ldi	ZL, low(IDEBuf0)
 ldi	ZH, high(IDEBuf0)

 lds	r16, fsFAT_FAT_Type
 tst	r16
 breq	fsFAT_NFE16
 dec	r16
 breq	fsFAT_NFE12

; ��������� ������� � FAT32

fsFAT_NFE32:
 lsl	r8			; ��������� ������, � ������� ��������� ������ ���������
 rol	r9
 rol	r10
 rol	r11

 add	r4, r9
 adc	r5, r10
 adc	r6, r11
 adc	r7, r7

 rcall	media_Read		; ���������� ������ ������
 brcc	fsFAT_Fault3
 ldi	ZL, low(IDEBuf0)
 ldi	ZH, high(IDEBuf0)

 clr	r9			; ��������� �������� ������ �������
 lsl	r8
 rol	r9
 add	ZL, r8
 adc	ZH, r9

 ld	r16, Z+			; ������ �������
 ld	r17, Z+
 ld	r18, Z+
 ld	r19, Z+
 cbr	r19, 0xF0		; ������ ������� ���� � FAT32 is reserved
 movw	r8, r16			; r16..r19 - ����������� ��������
 movw	r10, r18
 sbr	r19, 0xF0
 rjmp	fsFAT_NFE_Set

fsFAT_Fault3:
 clc
 ret

; ��������� ������� � FAT16

fsFAT_NFE16:
 add	r4, r9			; ��������� ������, � ������� ��������� ������ ���������
 adc	r5, r10
 adc	r6, r11
 adc	r7, r7

 rcall	media_Read		; ���������� ������ ������
 brcc	fsFAT_Fault3
 ldi	ZL, low(IDEBuf0)
 ldi	ZH, high(IDEBuf0)

 clr	r9			; ��������� �������� ������ �������
 lsl	r8
 rol	r9
 add	ZL, r8
 adc	ZH, r9

 ld	r16, Z+			; ������ �������
 ld	r17, Z+
 movw	r8, r16
 rjmp	fsFAT_NFE12_3

; ��������� ������� � FAT12

fsFAT_NFE12:
 movw	mc16uL, r8
 ldi	r16, 3
 clr	r17
 movw	mp16uL, r16
 rcall	mpy16u			; clust * 3 / 2
 lsr	m16u1
 ror	m16u0			; ������ m16u0..m16u3 �������� �������� ��������� � ������

 movw	r18, m16u0
 andi	r19, 0x01		; ����� ����� �������� ������ �������
 lsr	m16u1			; ����� ����� ������. FAT12 ����� �� ����� 12 �������� �� FAT

 clr	r6
 clr	r7
 lds	r16, fsFAT_ReservedSectors+0
 lds	r17, fsFAT_ReservedSectors+1
 add	r16, m16u1
 adc	r17, r7
 adc	r6, r7
 adc	r7, r7
 movw	r4, r16			; ������ ����� ������� �������

 ldi	ZL, low(IDEBuf0)
 ldi	ZH, high(IDEBuf0)
 rcall	media_Read		; ���������� ������ ������
 brcc	fsFAT_Fault3
 clr	r16
 sec
 adc	r4, r16
 adc	r5, r16
 adc	r6, r16
 adc	r7, r16
 rcall	media_ReadNoOffset	; ���������� ��������� ������ �� ������, ���� ���������� ����� �� ����� ��������
 brcc	fsFAT_Fault3

 ldi	ZL, low(IDEBuf0)
 ldi	ZH, high(IDEBuf0)
 add	ZL, r18
 adc	ZH, r19			; Z ������ ��������� � ������ �� ����������

; MTools:
; *      |    byte n     |   byte n+1    |   byte n+2    |
; *      |7|6|5|4|3|2|1|0|7|6|5|4|3|2|1|0|7|6|5|4|3|2|1|0|
; *      | | | | | | | | | | | | | | | | | | | | | | | | |
; *      | n+0.0 | n+0.5 | n+1.0 | n+1.5 | n+2.0 | n+2.5 |

; *          \_____  \____   \______/________/_____   /
; *            ____\______\________/   _____/  ____\_/
; *           /     \      \          /       /     \

; *      | n+1.5 | n+0.0 | n+0.5 | n+2.0 | n+2.5 | n+1.0 |
; *      |      FAT entry k      |    FAT entry k+1      |

 ld	r16, Z+
 ld	r17, Z+

 lsr	r8
 brcs	fsFAT_NFE12_1
				; ������ ������
 andi	r17, 0x0F		; 1.5 0.0 0.5
 rjmp	fsFAT_NFE12_2

fsFAT_NFE12_1:
 lsr	r17			; �������� ������
 ror	r16
 lsr	r17
 ror	r16
 lsr	r17
 ror	r16
 lsr	r17
 ror	r16			; 2.0 2.5 1.0

fsFAT_NFE12_2:
 movw	r8, r16
 ori	r17, 0xF0

fsFAT_NFE12_3:
 clr	r10
 clr	r11

 ldi	r18, 0xFF		; r16..r19 - ����������� ��������
 ldi	r19, 0xFF

; ���������� �������� � �������� �� ���������� ��������
; r8..r11 �������� ����� ��������, r16..r19 ����, �� �� �������� ���� = 1

fsFAT_NFE_Set:
 rcall	fsFAT_CurrentCluster_r8	; ���������� ��������

 cpi	r16, 2			; �������� �������� �� ���������� ��������
 brsh	fsFAT_NFE_Set_1

 mov	r17, r9
 or	r17, r10		; �������� ������� 8 ��� < 2
 or	r17, r11
 breq	fsFAT_Fault4		; ��������� ���� ��������� ������� 0

fsFAT_NFE_Set_1:
 cpi	r16, 0xF6
 brlo	fsFAT_Ok2

 and	r17, r18		; �������� ������� 8 ��� >= 0xF6
 and	r17, r19
 cpi	r17, 0xFF
 breq	fsFAT_Fault4		; ��������� ���� ��������� ������� 1

fsFAT_Ok2:
 sec
 ret

fsFAT_Fault4:
 clc
 ret

;
; ��������� � ����������� �����
;
fsFAT_Prev:
 rcall	fsFAT_GetDir_MaxDeep

fsFAT_P_2:
;		rcall	debug0
 ldi	ZL, low(CurrentDirEnt)
 ldi	ZH, high(CurrentDirEnt)
 clr	r22

fsFAT_P_1:
 ldd	r16, Z+2
 bst	r16, 7
 brtc	fsFAT_Fault6	; ��� ���� ������� ���� �����

 adiw	Z, 3
 inc	r22
 bst	r16, 6
 brtc	fsFAT_P_1	; ��� �ݣ �� ��������� ����� - ���� �����
 sbiw	Z, 3
 dec	r22

 bst	r16, 4
 brts	fsFAT_P_5

 sbiw	Z, 3		; ��������� ����� �������� ��������. ����� ������ ����������
 dec	r22
 brmi	fsFAT_Fault6

fsFAT_P_5:
 rcall	fsFAT_DecIndir

 ldi	r16, 0xFF
 rcall	fsFAT_FillDirEnt

 rcall	fsFAT_GetDir_MaxDeep
 brcc	fsFAT_P_2
 ret

;

fsFAT_DecIndir:
 ldd	XL, Z+0		; ��������� ������� �����
 ldd	XH, Z+1
 sbiw	X, 1
 std	Z+0, XL
 std	Z+1, XH
fsFAT_DI_ret:
 ret

;

fsFAT_FillDirEnt:	; ��������� ����� ������ ���������� r16
 inc	r22
 cpi	r22, MaxDirDeep
 breq	fsFAT_DI_ret

 adiw	Z, 3
 std	Z+0, r16
 std	Z+1, r16
 rjmp	fsFAT_FillDirEnt

;
; ��������� � ���������� �����
;
fsFAT_Next:
 rcall	fsFAT_GetDir_MaxDeep

fsFAT_N_2:
 ldi	ZL, low(CurrentDirEnt)
 ldi	ZH, high(CurrentDirEnt)
 clr	r22

fsFAT_N_1:
 ldd	r16, Z+2
 bst	r16, 7
 brtc	fsFAT_Fault6	; ��� ���� ������� ���� �����

 adiw	Z, 3
 inc	r22
 bst	r16, 6
 brtc	fsFAT_N_1	; ��� �ݣ �� ��������� ����� - ���� �����
 sbiw	Z, 3
 dec	r22

 bst	r16, 5
 brts	fsFAT_N_5

 sbiw	Z, 3
 dec	r22
 brmi	fsFAT_Fault6

fsFAT_N_5:
 rcall	fsFAT_IncIndir

 clr	r16
 rcall	fsFAT_FillDirEnt 

 rcall	fsFAT_GetDir_MaxDeep
 brcc	fsFAT_N_2
 ret

;

fsFAT_IncIndir:
 ldd	XL, Z+0		; ��������� ������� �����
 ldd	XH, Z+1
 adiw	X, 1
 std	Z+0, XL
 std	Z+1, XH
 ret

fsFAT_Fault6:
 clc
 ret

;
; ���������� ��� �������� ����� ��� ��������� � Z
; r23 �� ����� ��������� ������������ ������� ���������� � �����������
; [0 - /dir/subdir/.../file, 1 - /dir, 2 - /dir1/dir2, 3 - ...]
;
fsFAT_Name:
 rcall	fsFAT_GetDir
 brcc	fsFAT_ret0	; Z ����� ���� ��������̣� � ������ ������...

 clr	r16

 lds	r17, fsFAT_LFN_Ptr
 tst	r17
 breq	fsFAT_NM_1	; �� ������� ����� LFN ��� ������ ������

 ldi	ZL, low(fsFAT_LFN)
 ldi	ZH, high(fsFAT_LFN)
 add	ZL, r17
 adc	ZH, r16

 cpi	r17, 1
 brne	fsFAT_NM_2	; ������ ������ �������
 ldi	r25, '?'
 st	-Z, r25		; ������ ������ ����� �������� '?' ���� ������ �� �������
fsFAT_NM_2:

 sts	fsFAT_LFN+fsFAT_LFN_Size, r16 ; �������� ����� ������

 rjmp	fsFAT_Ok7

fsFAT_NM_1:		; ���ޣ������� �������� 8.3
 std	Z+12, r16

 ldd	r16, Z+10
 std	Z+11, r16
 ldd	r16, Z+09
 std	Z+10, r16
 ldd	r16, Z+08
 std	Z+09, r16

 ldi	r16, '.'
 std	Z+08, r16

 movw	XL, ZL
 ldi	r16, 11		; cp866 -> koi8-r
fsFAT_NM_3:
 ld	r25, X
 rcall	enc_alt2koi
 st	X+, r25
 dec	r16
 brne	fsFAT_NM_3

fsFAT_Ok7:
 sec
fsFAT_ret0:
 ret

;
; ��������� ���������� ������ Z �� ��������� �� LFN
; � �� ����������� ��������� ������ �� �ţ � ����� fsFAT_LFN
;
fsFAT_LFN_Test:
 clr	r17		; ����� ��-LFN ������� ��������� �� ��, ��� ������� LFN �������� � � �������� �����
			; ��������� ��� �� ����� (������� � ������� �������� ������ ���� ��������� �� ����������)
 ldd	r16, Z+0
 cpi	r16, 0xE5
 breq	fsFAT_LFN_3	; ������ �������

 ldd	r19, Z+11
 cpi	r19, 0x0F
 brne	fsFAT_LFN_3	; �� LFN

 lds	r17, fsFAT_LFN_Ptr
 andi	r16, 0x40
 breq	fsFAT_LFN_1	; �� ������ ������� LFN
 ldi	r17, fsFAT_LFN_Size

fsFAT_LFN_1:
 cpi	r17, 2
 brlo	fsFAT_ret0	; ����� �������� ���� LFN �ݣ �� ��������

 ldi	r19, 0x20	; ��������� � ����� ��� ��������� ������ LFN
 ldi	r16, 2
 rcall	fsFAT_LFN_4
 ldi	r19, 0x1A
 ldi	r16, 6
 rcall	fsFAT_LFN_4
 ldi	r19, 0x0B
 ldi	r16, 5
 rcall	fsFAT_LFN_4

fsFAT_LFN_3:
 sts	fsFAT_LFN_Ptr, r17
 ret

; ��������� �������� -(Z+r19) ������� r16

fsFAT_LFN_4:
 push	XL
 push	XH
 push	ZL
 push	ZH

 clr	r25

 movw	XL, ZL
 add	XL, r19
 adc	XH, r25

 ldi	ZL, low(fsFAT_LFN)
 ldi	ZH, high(fsFAT_LFN)
 add	ZL, r17
 adc	ZH, r25

fsFAT_LFN_5:
 cpi	r17, 2
 brlo	fsFAT_LFN_8	; ����� ��������
 
 ld	r25, -X
 ld	r24, -X
 cp	r24, r25
 brne	fsFAT_LFN_6
 cpi	r24, 0xFF
 breq	fsFAT_LFN_7	; ������ 0xFFFF ������������ � �������
 tst	r24
 breq	fsFAT_LFN_7	; ������ 0x0000 ������������ ����������

fsFAT_LFN_6:
 rcall	enc_utf2koi	; ������ ������� ����������� � �������� �����
 dec	r17
 st	-Z, r25

fsFAT_LFN_7:
 dec	r16
 brne	fsFAT_LFN_5
 
fsFAT_LFN_8:
 pop	ZH
 pop	ZL
 pop	XH
 pop	XL
 ret

;
; � ������ C = 0 ��������� ����� �� �������� fs_FAT*,
; ����� ����������� fs_errno
;
fsFAT_errtest:
 brcs	fsFAT_inc_errno

 pop	r16		; ����� �������� ������ ��� ��������� � SizeOf(PC) == 16
 pop	r16
 ret
;
fsFAT_inc_errno:
 lds	r16, fs_errno
 inc	r16
 sts	fs_errno, r16
 ret

;
; ��������� ����������� 32-� �����
;
fsFAT_CurrentCluster_r8:
 sts	CurrentCluster+0, r8
 sts	CurrentCluster+1, r9
 sts	CurrentCluster+2, r10
 sts	CurrentCluster+3, r11
 ret

fsFAT_r8_CurrentCluster:
 lds	r8, CurrentCluster+0		; ��������� ������ �� ��������
 lds	r9, CurrentCluster+1
 lds	r10, CurrentCluster+2
 lds	r11, CurrentCluster+3
 ret

fsFAT_fsFAT_RootOffset_r8:
 sts	fsFAT_RootOffset+0, r8
 sts	fsFAT_RootOffset+1, r9
 sts	fsFAT_RootOffset+2, r10
 sts	fsFAT_RootOffset+3, r11
 ret

fsFAT_r8_fsFAT_RootOffset:
 lds	r8, fsFAT_RootOffset+0
 lds	r9, fsFAT_RootOffset+1
 lds	r10, fsFAT_RootOffset+2
 lds	r11, fsFAT_RootOffset+3
 ret


;!!!!!!!!!!!!!!!!!!!!!!!!
;debug0:
; ldi	XL, low(CurrentDirEnt)
; ldi	XH, high(CurrentDirEnt)

;a:
; ld	r24, X+
; ld	r25, X+
; rcall	WordOut
; ldi	r25, '/'
; rcall	SendByte
; ld	r25, X
; rcall	ByteOut
; ldi	r25, ' '
; rcall	SendByte
; ld	r16, X+
; andi	r16, 0x40
; breq	a

; ldi	r25, 13
; rjmp	SendByte
