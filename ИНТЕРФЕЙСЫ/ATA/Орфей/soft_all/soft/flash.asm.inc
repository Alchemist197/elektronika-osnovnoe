;;==========================================================================
;; �������� � ���������� ������� ������ (EEPROM)
;; ������ ������������ �������, ��������� ������� ���������� ��������� f_IO
;; � ����, ��������� �� �������� ������: �������� [1], ������ ����� [1], ����� � EEPROM [2], ����� � SRAM [2]
;; ����������
;; ���������� ����������� �� ������������� ������ � EEPROM � Sleep Mode
;;

.equ	f_ORead		= 0
.equ	f_OWrite	= 1

.cseg
;
; ����� �����
; ��� ��� ��������� ������ � ������ ������ ��������� ����� ��������������, �� ��ߣ�����,
; ����� ������������ ����� ������� �������� �����, ��� ������ ��� �������� ���� ldi ����� ������� f_*
;
f_IO:
 pop	ZH		; ����������� ��������� ������
 pop	ZL
 lsl	ZL
 rol	ZH

 lpm	r16, Z+		; ��� ��������
 lpm	r17, Z+		; ������ �����

 lpm	XL, Z+		; ����� � EEPROM
 lpm	XH, Z+

 lpm	r0, Z+		; ����� � SRAM
 lpm	r1, Z+

 lsr	ZH		; ���������� ���� �����
 ror	ZL
 push	ZL
 push	ZH

 movw	ZL, r0		; ������ � r16 - ��� ��������, r17 - ������ �����, Z - ����� � SRAM, X - ����� � EEPROM

;

 cpi	r16, f_OWrite
 breq	f_Write

;
; EEPROM -> SRAM
;
f_Read:
 rcall	f_WWait

f_R_1:
 out	EEARL, XL
 out	EEARH, XH
 adiw	X, 1

 sbi	EECR, EERE
 in	r16, EEDR
 st	Z+, r16

 dec	r17
 brne	f_R_1
 ret

;
; SRAM -> EEPROM
;
f_Write:
 rcall	f_WWait

 out	EEARL, XL
 out	EEARH, XH
 adiw	X, 1

 ld	r16, Z+

 sbi	EECR, EERE
 in	r0, EEDR
 cp	r0, r16		; ���� ���������� ������ ��������� � ���, ������� ����� ��������...
 breq	f_W_1		; ... - �� ����� ��� ������������

 out	EEDR, r16
 cli			; ��� ���������� ���������� ����� �������� � ������ ��������� �������...
 sbi	EECR, EEMWE	; ...�� ��� �� ��������
 sbi	EECR, EEWE
 sei

f_W_1:
 dec	r17
 brne	f_Write
 ret

;
; ������� ���������� ������
; ����� ��������� ��� ��������� ����� ������ � ������������� sleep modes !
;
f_WWait:
 sbic	EECR, EEWE
 rjmp	f_WWait
 ret
