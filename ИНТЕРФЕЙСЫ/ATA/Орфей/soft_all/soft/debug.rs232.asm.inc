;;==========================================================================
;; rs232 ����-�����
;; ���������� Z, r24:r25
;;

.dseg
;
; ����������
;
.equ	LineMax	= 32			; ������ ������ ������� ������
LineBuf:	.byte	LineMax		; ����� ������� ������

;
; ��������� ���������� ���������
;
.equ	usartdiv	= 12		; �������� USART ~38.400 b/s ��� 8 ��� ��������

.cseg
;
; ������������� USART
;
rs_Init:
 ldi	r16, high(usartdiv)	; ������ ��������
 out	UBRRH, r16          	; &UBRRH == &UBSRC: ��� ������ ����� ������� �� ������� ����� ������ [URSEL]: D7 = 1 -> UCSRC, D7 = 0 -> UBRRH
 ldi	r16, low(usartdiv)	; Speed
 out	UBRRL, r16

 ldi	r16, (1<<RXEN) | (1<<TXEN)
 out	UCSRB, r16
 
; ldi	r16, (1<<URSEL) | (3<<UCSZ0)	; By default ��� � ��� �����������
; out	UCSRC, r16

 ret

;
; ���������� �� rs232 ascii-������ � ����������� 0x01, ��������� �� �������� ������ ���� ���������
; ������ ��� ��������� � sizeof(PC) == 16 !!!
;
print:
 pop	ZH	; high
 pop	ZL	; low
 lsl	ZL
 rol	ZH

p_2:
 lpm	r25, Z+
 cpi	r25, 1		; � ��� �������� ������������ ����� EOL == 1, ������ ���
 breq	p_1		; ����������� .db "������ �������� �����" \\ .db "��� ������"
 rcall	SendByte	; ���������� ������� ������������� ���� � ����� ������ ������, ��� �������
 rjmp	p_2		; ���������� ������ ASCIZ.

p_1:
 adiw	ZL, 1
 lsr	ZH
 ror	ZL
 ijmp

;
; ������� �� rs232 ����� �� r24:r25
;
WordOut:
 rcall	ByteOut
 mov	r25, r24

;
; ������� �� rs232 ���� �� r25
;
ByteOut:
 push	r25
 swap	r25
 rcall	HexOut
 pop	r25

;
; ������� �� rs232 ����� �� r25
;
HexOut:
 andi	r25, 0x0F
 cpi	r25, 10	; c = 1 if r25 < 10
 brlo	ho_1	; BRanch if LOwer = BR ==== BRanch if C Set
; ldi	r24, 'A'-'0'-10
; add	r25, r24
 subi	r25, -('A'-'0'-10)
ho_1:
; ldi	r24, '0'
; add	r25, r24
 subi	r25, -('0')

;
; r25 -> rs232
;
SendByte:
 sbis	UCSRA, UDRE
 rjmp	SendByte

 tst	r25
 brpl	SB_1
 cpi	r25, 0xC0
 brsh	SB_1
 cpi	r25, '�'
 breq	SB_1
 cpi	r25, '�'
 breq	SB_1

SB_2:
 ldi	r25, '?'

SB_1:
 out	UDR, r25
 ret

;
; ������� ��� '+' ��� '-' �������� C � ��������� ������
; �� ������ �� �������� !!!
;
C_Out:
 push	r16
 push	r25
 in	r16, SREG

 ldi	r25, '-'
 sbrc	r16, 0
 ldi	r25, '+'
 rcall	SendByte
 ldi	r25, 13
 rcall	SendByte

 out	SREG, r16
 pop	r25
 pop	r16
 ret

;
; rs232 -> r25
;
RecvByte:
 sbis	UCSRA, RXC
 rjmp	RecvByte
 in	r25, UDR
 ret

;
; ���� ������ � LineBuf
;
.def	Cursor	= r24
.def	Char	= r25
;
EnterString:
 ldi	Char, ']'
 rcall	SendByte
 ldi	ZL, low(LineBuf)
 ldi	ZH, high(LineBuf)
 clr	Cursor

es_1:
 rcall	RecvByte
 st	Z, Char
 cpi	Char, ' '
 brlo	es_2

; ������� ������
 cpi	Cursor, LineMax-1
 brsh	es_1
 inc	Cursor
 adiw	Z, 1
 rcall	SendByte
 rjmp	es_1

es_2:
 cpi	Char, 8
 brne	es_3

; BackSpace
 tst	Cursor
 breq	es_1

 rcall	SendByte
 ldi	r25, 32
 rcall	SendByte
 ldi	r25, 8
 rcall	SendByte

 dec	Cursor
 sbiw	Z, 1
 rjmp	es_1

es_3:
 cpi	Char, 13
 brne	es_1

; Enter
 rjmp	SendByte
