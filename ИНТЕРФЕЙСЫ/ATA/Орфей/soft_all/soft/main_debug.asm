;; ���������� ������, ���������, ���������������, �� rs232

.nolist
.include "/usr/local/include/avr/m32def.inc"
.list

;
; ������ � ���
;
.dseg

IDEBuf0:	.byte	512	; ������ IDE
IDEBuf1:	.byte	512	; ����� (��� ��������� FAT12) ����� ��� ��� ���� �� ������

.cseg

.org	0x000
 jmp	RESET		; Reset Handler

 jmp	EXT_INT0	; IRQ0 Handler
 jmp	EXT_INT1	; IRQ1 Handler
 jmp	EXT_INT2	; IRQ2 Handler

 jmp	TIM2_COMP	; Timer2 Compare Handler
 jmp	TIM2_OVF	; Timer2 Overflow Handler

 jmp	TIM1_CAPT	; Timer1 Capture Handler
 jmp	TIM1_COMPA	; Timer1 CompareA Handler
 jmp	TIM1_COMPB	; Timer1 CompareB Handler
 jmp	TIM1_OVF	; Timer1 Overflow Handler
 
 jmp	TIM0_COMP	; Timer0 Compare Handler
 jmp	TIM0_OVF	; Timer0 Overflow Handler

 jmp	SPI_STC		; SPI Transfer Complete Handler

 jmp	USART_RXC	; USART RX Complete Handler
 jmp	USART_UDRE	; UDR Empty Handler
 jmp	USART_TXC	; USART TX Complete Handler

 jmp	ADCC		; ADC Conversion Complete Handler
 jmp	EE_RDY		; EEPROM Ready Handler
 jmp	ANA_COMP	; Analog Comparator Handler
 jmp	TWI		; Two-wire Serial Interface
 jmp	SPM_RDY		; Store Program Memory Ready

EXT_INT0:		; ��� ���������� ������ ����� ���� �� sleep
EXT_INT1:
EXT_INT2:

TIM2_COMP:
TIM2_OVF:

TIM1_CAPT:
TIM1_COMPA:
TIM1_COMPB:
TIM1_OVF:

TIM0_COMP:
TIM0_OVF:

SPI_STC:

USART_RXC:
USART_UDRE:
USART_TXC:

ADCC:
EE_RDY:
ANA_COMP:
TWI:
SPM_RDY:

;;==========================================================================
;; Main ENTER
;;
RESET:				; Main program start
 ldi	r16, high(RAMEND)
 out	SPH, r16		; Set Stack Pointer to top of RAM
 ldi	r16, low(RAMEND)
 out	SPL, r16

; ������� ���������������� ��������� ���������� �����������

 call	spi_Init		; ������������� SPI
 rcall	rs_Init			; ������������� USART
 sbi	PORTB, 2		; ��� ���� ���� �� ������������, ������� ������
 sbi	PORTD, 2		; �� PullUp, ����� ������� �����������������

; ������ ��������� ���������� ����� ������ ���������

M2:
 rcall	print
.db	"HReset", 13, 1
 sbi	DDRB, 1			; ���������� ������������� ������������ ���������: VLSI, LCD
 cbi	PORTB, 1		; � ����� �������� ����� ������������� SPI, �����
 call	Wait1mks		; ������������� ���������� ����� � �������������������������
 sbi	PORTB, 1

; ��������� ���������� - ������������ ��� ��������� ��� �������� ������ ��������

; ldi	r16, (1<<SE) | (1<<ISC01) | (1<<ISC00)
; out	MCUCR, r16		; INT0 �� �������������� ������ INT0
; ldi	r16, (1<<INT0)
; out	GICR, r16		; ��������� INT0
; sei

; ������, ����� ������� ���������� ������ � ������ �� �����������
; ������, ����� ��������� �� (����������) � ����� ���������

 call	lcd_Init		; ������������� LCD
 call	vlsi_Init		; ������������� VLSI
 call	lcd_LightOn
 rcall	print
.db	"FS Init...", 1
 call	file_Init		; ������������� ��
 rcall	C_Out
 
 brcc	Main			; ���� �� �� ������� ���������������� - �� ����� ��������� ���������������

 rcall	print
.db	"Try VLSI open and play file...", 13, 1

 call	vlsi_Open		; ������� ���������� � VLSI
 tst	r20
 brne	M2			; ���� �� ��������� - ������ ��������� ��������� �����

 call	lcd_LightOff

 rcall	cmd_SendFile		; ������ ������ ������

;
;
;

Main:
 rcall	print
.db	12, "rs232 <-> IDE bridge ready !", 13, 1

MainCycle:
 rcall	EnterString
 rcall	AnalyseString
 rjmp	MainCycle


.include "debug.rs232.asm.inc"
.include "debug.syntax_analyser.asm.inc"
.include "debug.actions.asm.inc"

.include "path_changer.asm.inc"

.include "fs.asm.inc"
.include "media.asm.inc"
.include "flash.asm.inc"
.include "LCD.asm.inc"
.include "VLSI.asm.inc"
.include "sleep.asm.inc"
.include "spi.asm.inc"
.include "math.asm.inc"
.include "encode.asm.inc"
.include "user_router.asm.inc"

;
; ���������� �����
;
lcd_T_index:
.include "LCD.font-index.inc"

lcd_T_bmp:
.include "LCD.font-bmp.inc"

;
; In system programm
;
.include "selfprog.asm.inc"
