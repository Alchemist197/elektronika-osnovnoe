;;==========================================================================
;; ������ ������ ������ "������� ����"
;;

.dseg
mmenu_MainMode:		.byte	1
mmenu_NoNeedRestart:	.byte	1	; D0 ���������� � �������� ����� C �������� ������

.cseg
;
;
;
mmenu_Enter:
 call	f_IO
.db	f_ORead, 1
.dw	nv_main_Mode
.dw	mmenu_MainMode

 ldi	r16, 0xFF
 sts	mmenu_NoNeedRestart, r16

 ldi	r22, low(mmenu_Desc*2)
 ldi	r23, high(mmenu_Desc*2)
 call	ur_Menu
 lds	r16, mmenu_NoNeedRestart
 lsr	r16
 lds	r16, mmenu_MainMode
 ret

;
mmenu_Desc:
 .db	                   (1<<ur_m_LeftEn) | (1<<ur_m_OkEn) | (1<<ur_m_RightEn) | (1<<ur_m_SelEn) | (1<<ur_m_OkFin), 0x00
 .dw	mmenu_Txt1*2
 .dw	0
 .dw	pc_Menu
 .dw	pc_Menu
 .dw	pc_Menu

 .db	(1<<ur_m_DrawEn) | (1<<ur_m_LeftEn) | (1<<ur_m_OkEn) | (1<<ur_m_RightEn) | (1<<ur_m_SelEn) | (1<<ur_m_OkFin), 0x00
 .dw	mmenu_Txt2*2
 .dw	mmenu_DevDraw
 .dw	mmenu_DevSel
 .dw	mmenu_DevChg
 .dw	mmenu_DevSel

 .db	(1<<ur_m_DrawEn) | (1<<ur_m_LeftEn) | (1<<ur_m_OkEn) | (1<<ur_m_RightEn) | (1<<ur_m_SelEn) | (1<<ur_m_OkFin), 0x00
 .dw	mmenu_Txt3*2
 .dw	mmenu_PartDraw
 .dw	mmenu_PartSelL
 .dw	mmenu_PartChg
 .dw	mmenu_PartSelR

 .db	(1<<ur_m_DrawEn) | (1<<ur_m_LeftEn)                  | (1<<ur_m_RightEn) | (1<<ur_m_SelEn) | (1<<ur_m_OkFin), 0x00
 .dw	mmenu_Txt4*2
 .dw	mmenu_ModeDraw
 .dw	mmenu_ModeDec
 .dw	0
 .dw	mmenu_ModeInc

 .db	(1<<ur_m_DrawEn) | (1<<ur_m_LeftEn) |                  (1<<ur_m_RightEn) | (1<<ur_m_SelEn)                  , 0x00
 .dw	mmenu_Txt5*2
 .dw	mmenu_BassDraw
 .dw	mmenu_BassChg
 .dw	0
 .dw	mmenu_BassChg

 .db	(1<<ur_m_DrawEn) | (1<<ur_m_LeftEn) |                  (1<<ur_m_RightEn) | (1<<ur_m_SelEn)                   | (1<<ur_m_Close), 0x00
 .dw	mmenu_Txt6*2
 .dw	mmenu_DiffDraw
 .dw	mmenu_DiffChg
 .dw	0        
 .dw	mmenu_DiffChg

mmenu_Txt1:
 .db	"������� ����...", 0
mmenu_Txt2:
 .db	"����������: ", 0
mmenu_Txt3:
 .db	"������    : ", 0
mmenu_Txt4:
 .db	"�����     : ", 0
mmenu_Txt5:
 .db	"������� �������: ", 0
mmenu_Txt6:
 .db	"�������� ������ : ", 0

;;

mmenu_DevDraw:
 ldi	r25, 'H'
 lds	r16, CurrentDevice
 tst	r16
 brne	mmenu_DD_1
 ldi	r25, 'S'
mmenu_DD_1:
 jmp	lcd_CharOut

mmenu_DevSel:
 lds	r16, CurrentDevice
 ldi	r17, 0xFF
 sbrc	r16, 0
 ldi	r17, 0x00
 sts	CurrentDevice, r17
 ret

mmenu_DevChg:
 lds	r16, 0x00
 sts	mmenu_NoNeedRestart, r16
 jmp	file_DvCh
 
;;

mmenu_PartDraw:
 lds	dd16uL, CurrentPart
 jmp	DecByteOut
 
mmenu_PartSelL:
 lds	r16, CurrentPart
 dec	r16
 sts	CurrentPart, r16
 ret
 
mmenu_PartChg:
 jmp	file_DvCh

mmenu_PartSelR:
 lds	r16, CurrentPart
 inc	r16
 sts	CurrentPart, r16
 ret

;;

mmenu_ModeDraw:
 lds	r16, mmenu_MainMode
 clr	r17
 ldi	ZL, low(mmenu_MD_Table*2)
 ldi	ZH, high(mmenu_MD_Table*2)
 add	ZL, r16
 adc	ZH, r17
 lpm	r25, Z
 jmp	lcd_CharOut

mmenu_MD_Table:
.db	"MOmT"

mmenu_ModeDec:
 lds	r16, mmenu_MainMode
 dec	r16
 rjmp	mmenu_MI_1

mmenu_ModeInc:
 lds	r16, mmenu_MainMode
 inc	r16
mmenu_MI_1:
 andi	r16, 0x03
 sts	mmenu_MainMode, r16
 ret

;;

mmenu_BassDraw:
 ldi	r25, 'N'
 lds	r16, vlsi_Mode
 sbrc	r16, vlsi_SM_BASS
 ldi	r25, 'Y'
 rjmp	lcd_CharOut

mmenu_BassChg:
 ldi	r17, (1<<vlsi_SM_BASS)
 lds	r16, vlsi_Mode
 eor	r16, r17
 sts	vlsi_Mode, r16
 ret

;;

mmenu_DiffDraw:
 ldi	r25, 'N'
 lds	r16, vlsi_Mode
 sbrc	r16, vlsi_SM_DIFF
 ldi	r25, 'Y'
 rjmp	lcd_CharOut

mmenu_DiffChg:
 ldi	r17, (1<<vlsi_SM_DIFF)
 lds	r16, vlsi_Mode
 eor	r16, r17
 sts	vlsi_Mode, r16
 ret
