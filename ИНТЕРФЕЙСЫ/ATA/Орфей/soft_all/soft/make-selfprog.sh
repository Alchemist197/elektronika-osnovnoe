#!/bin/sh

# ������������ ��� � �������� ������ ��� ����� com-���� ��������� ����������� �������������������� ����� boot-block

if [ $1 ]
then
  n=$1
else
  n=main
fi

clear
cd SelfProg
gcc -ggdb -o sprog prg.c || { sleep 3 ; exit ; }

[ "$USER" = "root" ] && { {
  #Linux:
  #stty -F /dev/ttyS0 38400 raw

  #FreeBSD 6.1:
  stty -f /dev/cuad0.init 38400 raw; echo $?
  stty -f /dev/cuad0.lock 38400 raw; echo $?
  stty -f /dev/cuad0 ; echo $?
} || { sleep 2 ; exit ; } ; }

cd ..

tavrasm $n.asm -e $n.lst && SelfProg/sprog $n.hex || { sleep 3 ; exit ; }

rm $n.hex
