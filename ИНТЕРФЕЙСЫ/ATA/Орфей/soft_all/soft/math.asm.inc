;;==========================================================================
;; ����������
;; ���������� r0..r7, r16, r25
;;

.cseg
;* Title:		Multiply and Divide Routines
;* Version:		1.1
;* Last updated:	97.07.04

;
; ��������� ���������� ��������� 8-��� �� 32-����
; r8..r11 = r2 * r4..r7
; ���������� C = 0 ��� ������������
;
mpy32u:
 clr	r8
 clr	r9
 clr	r10
 clr	r11

m32u_1:
 sec
 tst	r2
 breq	m32u_2
 dec	r2

 add	r8, r4
 adc	r9, r5
 adc	r10, r6
 adc	r11, r7
 brcc	m32u_1
 clc

m32u_2:
 ret

;***************************************************************************
;*
;* "mpy16u" - 16x16 Bit Unsigned Multiplication
;*
;* This subroutine multiplies the two 16-bit register variables 
;* mp16uH:mp16uL and mc16uH:mc16uL.
;* The result is placed in m16u3:m16u2:m16u1:m16u0.
;*  
;***************************************************************************

;***** Subroutine Register Variables

.def	mc16uL	=r2		;multiplicand low byte
.def	mc16uH	=r3		;multiplicand high byte
.def	mp16uL	=r4		;multiplier low byte
.def	mp16uH	=r5		;multiplier high byte

.def	m16u0	=r4		;result byte 0 (LSB)
.def	m16u1	=r5		;result byte 1
.def	m16u2	=r6		;result byte 2
.def	m16u3	=r7		;result byte 3 (MSB)

.def	mcnt16u	=r16		;loop counter

;***** Code

mpy16u:	clr	m16u3		;clear 2 highest bytes of result
	clr	m16u2
	ldi	mcnt16u, 16	;init loop counter
	lsr	mp16uH
	ror	mp16uL

m16u_1:	brcc	noad8		;if bit 0 of multiplier set
	add	m16u2, mc16uL	;add multiplicand Low to byte 2 of res
	adc	m16u3, mc16uH	;add multiplicand high to byte 3 of res
noad8:	ror	m16u3		;shift right result byte 3
	ror	m16u2		;rotate right result byte 2
	ror	m16u1		;rotate result byte 1 and multiplier High
	ror	m16u0		;rotate result byte 0 and multiplier Low
	dec	mcnt16u		;decrement loop counter
	brne	m16u_1		;if not done, loop more
	ret

;***************************************************************************
;*
;* "div16u" - 16/16 Bit Unsigned Division
;*
;* This subroutine divides the two 16-bit numbers
;* "dd8uH:dd8uL" (dividend) and "dv16uH:dv16uL" (divisor).
;* The result is placed in "dres16uH:dres16uL" and the remainder in
;* "drem16uH:drem16uL".
;*
;***************************************************************************
; input:	r1, r0 = Dividend X
; 		r3, r2 = Divisor Y
;
; output:	r1, r0 = quotient Q of division
;		r7, r6 = remainder
;
; r1:r0 / r3:r2 = r1:r0 [r7:r6]		Q = X / Y
;
;***** Subroutine Register Variables

.def	dd16uL	= r0
.def	dd16uH	= r1
.def	dv16uL	= r2
.def	dv16uH	= r3

.def	dres16uL= r0
.def	dres16uH= r1
.def	drem16uL= r6
.def	drem16uH= r7

.def	dcnt16u	= r16

;***** Code

div16u:	clr	drem16uL		;clear remainder Low byte
	sub	drem16uH, drem16uH	;clear remainder High byte and carry
	ldi	dcnt16u, 17		;init loop counter
d16u_1:	rol	dd16uL			;shift left dividend
	rol	dd16uH
	dec	dcnt16u			;decrement counter
	brne	d16u_2			;if done
	ret				; return
d16u_2:	rol	drem16uL		;shift dividend into remainder
	rol	drem16uH
	sub	drem16uL, dv16uL	;remainder = remainder - divisor
	sbc	drem16uH, dv16uH
	brcc	d16u_3			;if result negative
	add	drem16uL, dv16uL	; restore remainder
	adc	drem16uH, dv16uH
	clc				; clear carry to be shifted into result
	rjmp	d16u_1			;else
d16u_3:	sec				; set carry to be shifted into result
	rjmp	d16u_1

;
; ������� ����� � ���������� ������� �� dd16uL:dd16uH
;
DecByteOut:
 clr	dd16uH
DecWordOut:
 ldi	r16, 10
 mov	dv16uL, r16
 clr	r16
 push	r16
 mov	dv16uH, r16

dwo_1:
 rcall	div16u

 ldi	r16, '0'
 add	drem16uL, r16
 push	drem16uL

; movw	dd16uL, dres16uL

 mov	r16, dd16uL
 or	r16, dd16uH
 brne	dwo_1

dwo_2:
 pop	r25
 tst	r25
 breq	m32u_2
 rcall	lcd_CharOut	; SendByte
 rjmp	dwo_2
