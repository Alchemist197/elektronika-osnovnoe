;;==========================================================================
;; ������ ������ ������ "������ ������"
;;

.equ	mtext_rtctop	= 12500-1		; ���������� �� ������� 1 ����� ���� 10 ��� � ������� ��� prescaler == 64 � �������� 8 ���

.dseg
mtext_pFirstLine:	.byte	2		; ����� ������, ����������� � ������ ������ [��� ��������� ����� �� ������]
mtext_pSecondLine:	.byte	2		; ����� ������, ����������� �� ������ ������ [��� ��������� ������ �� ������]
mtext_pLastLine:	.byte	2		; ����� ������, ����������� � ��������� ������ [��� ��������� ������ �� ��������]
mtext_GetCharState:	.byte	1		; ��������� GetChar
mtext_PrintLineState:	.byte	1		; ��������� PrintLine
mtext_SREG:		.byte	1		; SREG, ����� ������� ���������� ��������� ������������ ������ �������� ������
mtext_NEOS:		.byte	1		; �� ����� �������

; ��� ���������� ����� ����� � � EEPROM ��������� !
mtext_nSector:		.byte	2		; ����� �������� �������. ����� ��� ��������� ��������
mtext_RedLine:		.byte	1		; ����� �������� ������� ������ [0..3]

; ��� ���������� ����� ����� � � EEPROM ��������� !
mtext_LightMode:	.byte	1		; ��������� (D7.D6): x0 - ���������, 11 - ��������, 01 - ���������
mtext_LShift:		.byte	1		; ����� ������ ������ ������� � �������� [0..15]

.eseg
nv_mtext_LightMode:	.byte	1		; ����� ���������
nv_mtext_LShift:	.byte	1		; ����� ������ ������ ������� � ��������

; �������� # 1
nv_mtext_CurrDirEnt1:	.byte	MaxDirDeep*3	; ������� ���
nv_mtext_nSector1:	.byte	2		; ����� �������
nv_mtext_RedLine1:	.byte	1		; ����� �������� ������� ������

; �������� # 2
nv_mtext_CurrDirEnt2:	.byte	MaxDirDeep*3	; ������� ���
nv_mtext_nSector2:	.byte	2		; ����� �������
nv_mtext_RedLine2:	.byte	1		; ����� �������� ������� ������


.cseg
;
; ������� ��������� "������ ������"
;
mtext_Enter:
 call	vlsi_Sleep

 rcall	f_IO
.db	f_ORead, 2
.dw	nv_mtext_LightMode
.dw	mtext_LightMode

 lds	r16, mtext_LShift
 andi	r16, 0x0F
 sts	mtext_LShift, r16

 rcall	mtext_Load1
 rjmp	mtext_E_read

mtext_E_open:
 rcall	mtext_FOpen
 brcc	mtext_E_ret

mtext_E_read:
 rcall	mtext_FRead
 brcc	mtext_E_nodraw

mtext_E_draw:
 rcall	lcd_ClrScr
 rcall	mtext_PrintScreen

mtext_E_nodraw:
 rcall	mtext_IdleWhileNoKeys

 in	r16, SREG
 cbr	r16, 0x43		; �������� T, Z, C
 sts	mtext_SREG, r16

 call	ur_KeybRoute_NoGetKeys
.db	Key2mask, 0x00
.dw	mtext_E_LineUp

.db	Key4mask, 0x00
.dw	mtext_E_LineDown

.db	Key6mask, 0x00
.dw	mtext_E_PageDown

.db	Key0mask, 0x40
.dw	mtext_LShiftDec

.db	Key1mask, 0x40
.dw	mtext_LShiftInc

.db	Key5mask, 0xC0
.dw	mtext_Tune

 lds	r16, mtext_SREG
 out	SREG, r16
 breq	mtext_E_read
 brcs	mtext_E_open
 brtc	mtext_E_draw

mtext_E_ret:
 rcall	file_Init		; ���� ����� ��������������� DirEnt �� �������� ����� (�� ������ ��������)
 ldi	r16, m_ModeMenu
 sec
 ret

;
; ����������� ������
;

;; �� 20 �������� �����

mtext_E_LineUp:
 ldi	r16, low(IDEBuf0)
 ldi	r17, high(IDEBuf0)

 lds	ZL, mtext_pFirstLine+0
 lds	ZH, mtext_pFirstLine+1

 sbiw	Z, 20

 cp	ZL, r16
 cpc	ZH, r17
 brsh	mtext_E_LU_1
 movw	ZL, r16
mtext_E_LU_1:

 sts	mtext_pFirstLine+0, ZL
 sts	mtext_pFirstLine+1, ZH

mtext_E_LU_ret:
 ret

;; �� ����� ����

mtext_E_PageDown:
 lds	ZL, mtext_pLastLine+0
 lds	ZH, mtext_pLastLine+1
 rjmp	mtext_E_LD_1

;; �� ������ ����

mtext_E_LineDown:
 lds	ZL, mtext_pSecondLine+0
 lds	ZH, mtext_pSecondLine+1

mtext_E_LD_1:
 sts	mtext_pFirstLine+0, ZL
 sts	mtext_pFirstLine+1, ZH

 lds	r16, mtext_NEOS
 tst	r16
 brne	mtext_E_LU_ret

;; ������� � ���������� �������

 lds	XL, mtext_nSector+0
 lds	XH, mtext_nSector+1
 adiw	XL, 1
 sts	mtext_nSector+0, XL
 sts	mtext_nSector+1, XH

 rcall	file_Step
 rcall	lcd_PrintC
.db	"Step: ", 1

 rjmp	mtext_FS_ReadRequest

;; ���������� ��������

mtext_LShiftDec:
 lds	r16, mtext_LShift
 dec	r16
 rjmp	mtext_LS_1

mtext_LShiftInc:
 lds	r16, mtext_LShift
 inc	r16

mtext_LS_1:
 andi	r16, 0x0F
 sts	mtext_LShift, r16
mtext_LS_ret:
 ret

;
; ������� ���� ��������� � ������
;
mtext_AllSleep:

;
; ����������� ��������� (���� ?)
;
mtext_WakeUp:

;
; ����� ����� ������ �� mtext_pFirstLine; ��������� mtext_pSecondLine, mtext_pLastLine
;
mtext_PrintScreen:
 lds	r16, mtext_LightMode		; D7.D6: x0 - ���������, 11 - ��������, 01 - ���������
 sbrs	r16, 6
 rjmp	mtext_PS_2

 rcall	lcd_LightOff
 sbrc	r16, 7
 rcall	lcd_LightOn

mtext_PS_2:
 andi	r16, 0x40
 rcall	lcd_SetAttr

 lds	r16, mtext_RedLine
 sts	mtext_GetCharState, r16

 lds	ZL, mtext_pFirstLine+0
 lds	ZH, mtext_pFirstLine+1

 clr	r17
 rcall	mtext_PrintLine

 sts	mtext_pSecondLine+0, ZL
 sts	mtext_pSecondLine+1, ZH

 ldi	r17, 1

mtext_PS_1:
 push	r17
 rcall	mtext_PrintLine
 pop	r17
 inc	r17
 cpi	r17, 5
 brne	mtext_PS_1

 sts	mtext_pLastLine+0, ZL
 sts	mtext_pLastLine+1, ZH

;
; ����� ��������� ������ r17 �� Z+
;
; mtext_PrintLineState: ����� ������� � ������, D7 = 1 ���� ��������� �������� ��� ������
;
mtext_PrintLine:
 lds	r18, mtext_LShift
 rcall	lcd_GoToXY
 clr	r16
 sts	mtext_PrintLineState, r16
 
mtext_PL_1:
 lds	r16, lcd_XPos
 cpi	r16, lcd_XPosMax-4	; ����������� ����� ���������� ������, ���� ��� ������� ����� ������� '-'
 brsh	mtext_PL_dash		; ��������� ��������� ������� ��������� - ����� ���� �� ����� ��������� ����� ������

mtext_PL_4:
 rcall	mtext_GetChar
 tst	r25
 breq	mtext_LS_ret		; #0 - ����� ������
 cpi	r25, 10
 breq	mtext_LS_ret		; CR - ���� ���������� ������ ������

 lds	r16, mtext_PrintLineState
 cbr	r16, 0x80
 
 cpi	r25, ' '
 brne	mtext_PL_2

 tst	r16
 breq	mtext_PL_4		; ������� � ������ ���������� ������ ���������

 sbr	r16, 0x80		; ������� � ���, ��� ��������� ������ ��� ��������
 
mtext_PL_2:
 inc	r16
 sts	mtext_PrintLineState, r16

 cpi	r25, 31			; �� ��������� � ������ ������ ������ (������� ������)
 brne	mtext_PL_3
 ldi	r25, ' '
mtext_PL_3:

 rcall	lcd_CharOut
 rjmp	mtext_PL_1

mtext_PL_dash:
 lds	r16, mtext_PrintLineState
 tst	r16
 brmi	mtext_GC_ret		; ��������� �������� ��� ������, '-' � ����� ������ �� ����

 ld	r16, Z
 cpi	r16, 33
 brlo	mtext_GC_ret		; ��������� �������� ����� ������/tab/cr/lf/Null - '-' � ����� ������ �� ����

 ldi	r25, '-'
 rjmp	lcd_CharOut

;
; ����� ���������� ������� �� Z+, ����������� ������������� �������� ����� � �������, ��� ���� ���������
; ����� �������� � #10 + ��� ������� 31, ���� � #32, ������� ������� ����� ��������� �����
;
; mtext_GetCharState:
; 0001 0000 - ������� ������
; 0000 1000 - ������ [��������� ����������� �������]
; 0000 0100 - ������� ������, �� ��� ������� ������� ������ ��� ��������
; 0000 00xx - ������� ������ [��������� ����������� ������� � �������� �����]
;       (xx - ������� �������� �ݣ ����� ������� ��� ������� ������)
;
mtext_GetChar:
 lds	r17, mtext_GetCharState
 cpi	r17, 4
 brlo	mtext_GC_fake_char

mtext_GC_fetch:
 ld	r25, Z+

 cpi	r25, 9
 brne	mtext_GC_1
 ldi	r25, ' '
mtext_GC_1:

 cpi	r25, 13
 brne	mtext_GC_2
 ldi	r25, 10
mtext_GC_2:

 cpi	r25, 10
 breq	mtext_GC_CR
 cpi	r25, ' '
 breq	mtext_GC_space

 ldi	r17, 0x10		; ����� ������ ����� ������� � �������� ������ ��������� ��������� � 0x10
 tst	r25
 brne	mtext_GC_save_state
 sbiw	Z, 1			; ���� �������� #0 - ���������� ��������� ������ ������,
 rjmp	mtext_GC_save_state	; ���������� �� ����������� ������� ����� ����� #0

mtext_GC_space:
 sbrs	r17, 4
 rjmp	mtext_GC_fetch		; ������ ����� ������� ������ � ��������� 0x10
 ldi	r17, 0x08		; ��� ���� ���������� ������� � ��������� 0x08
 rjmp	mtext_GC_save_state

mtext_GC_CR:
 cpi	r17, 0x04
 breq	mtext_GC_fetch		; ��������� 0x04 - ��� ��������� ��������� ������� ������ - ������ �� �����������
 lds	r17, mtext_RedLine	; �������� ������� ������, ��������� � ��������� 3..0
 rjmp	mtext_GC_save_state

mtext_GC_fake_char:
 ldi	r25, 31			; �� ��������� ��������� ������� ������
 inc	r17

mtext_GC_save_state:
 sts	mtext_GetCharState, r17
 sts	mtext_NEOS, r25		; ��������� ������ = 0 - End Of Sector

mtext_GC_ret:
 ret
 
;
; �������� �������� 1
;
mtext_Save1:
 rcall	f_IO
.db	f_OWrite, MaxDirDeep*3
.dw	nv_mtext_CurrDirEnt1
.dw	CurrentDirEnt
 rcall	f_IO
.db	f_OWrite, 3
.dw	nv_mtext_nSector1
.dw	mtext_nSector

 rjmp	mtext_S2_1

;
; �������� �������� 2
;
mtext_Save2:
 rcall	f_IO
.db	f_OWrite, MaxDirDeep*3
.dw	nv_mtext_CurrDirEnt2
.dw	CurrentDirEnt
 rcall	f_IO
.db	f_OWrite, 3
.dw	nv_mtext_nSector2
.dw	mtext_nSector

mtext_S2_1:
 clc
 rcall	lcd_PrintC
.db	"�������� ��������", 1
 ret

;
; ������� � �������� 1
;
mtext_Load1:
 rcall	f_IO
.db	f_ORead, MaxDirDeep*3
.dw	nv_mtext_CurrDirEnt1
.dw	CurrentDirEnt

 rcall	f_IO
.db	f_ORead, 3
.dw	nv_mtext_nSector1
.dw	mtext_nSector

 rjmp	mtext_FOpenAndSeek

;
; ������� � �������� 2
;
mtext_Load2:
 rcall	f_IO
.db	f_ORead, MaxDirDeep*3
.dw	nv_mtext_CurrDirEnt2
.dw	CurrentDirEnt

 rcall	f_IO
.db	f_ORead, 3
.dw	nv_mtext_nSector2
.dw	mtext_nSector

;
; ��������� ���� � ��������� � ������� nSector, nSector �� ����������
;
mtext_FOpenAndSeek:
 rcall	mtext_FOpen1			; ��������� ����

 lds	XL, mtext_nSector+0		; ��������� nSector
 lds	XH, mtext_nSector+1
 push	XL
 push	XH

mtext_FS_2:
 lds	XL, mtext_nSector+0
 lds	XH, mtext_nSector+1

 mov	r16, XL
 or	r16, XH
 breq	mtext_FS_1			; ���� nSector = 0 - �����

 sbiw	X, 1
 sts	mtext_nSector+0, XL		; nSector--
 sts	mtext_nSector+1, XH

 rcall	file_Step
 rcall	lcd_PrintC
.db	"�� ���� ����� ��������", 1
 brcs	mtext_FS_2

mtext_FS_1:
 pop	XH				; ��������������� nSector
 pop	XL
 sts	mtext_nSector+0, XL
 sts	mtext_nSector+1, XH

mtext_FS_ReadRequest:
 lds	r16, mtext_SREG			; ����� �������� - ��������� ������
 sbr	r16, 0x02			; Z = 1 - Read Sector
 sts	mtext_SREG, r16
 ret

;
; ��������� ����, ���������� �ޣ���� �������� �� 0
;
mtext_FOpen:
 clr	r16
 sts	mtext_nSector+0, r16
 sts	mtext_nSector+1, r16

mtext_FOpen1: 
 rcall	file_Open
 rcall	lcd_PrintC
.db	"Open: ", 1
 ret

;
; ������ ������, ������������� Top �� ������ �������, ��������� #0 � �����
;
mtext_FRead:
 ldi	ZL, low(IDEBuf0)
 ldi	ZH, high(IDEBuf0)
 sts	mtext_pFirstLine+0, ZL
 sts	mtext_pFirstLine+1, ZH

 rcall	file_Read
 rcall	lcd_PrintC
.db	"Read: ", 1

 ldi	r16, '\'
 sts	IDEBuf0+512, r16
 clr	r16
 sts	IDEBuf0+513, r16	; ����������� '\' � #0, �������� �� IDEBuf1
 ret

;
; ��������� ���������� �� ������� 10 ��, �� ���� ���������� ����������, ������� �������� kb_GetKeys.
; ���� ������ � ������.
; ���� ����� ����������� kb_Keys == 0, ������� �����������.
; � ��������� ������ ���������� �� ������� �����������, ������ ���������������,
; SREG ������������ �� ��������, ���������� ������������ ��������� ���������
;
mtext_IdleWhileNoKeys:

;; ������� ���������� EEPROM/FLASH ������� � ��������� SREG

 rcall	f_WWait			; �� ���
 in	r0, SREG

;; ������������� ������� 0

; out TCCR1A, 0x00		; By default

; ldi	r16, (0<<WGM13) | (1<<WGM12) | (0<<CS12) | (1<<CS11) | (1<<CS10)
 ldi	r16, (0<<CTC11) | (1<<CTC10) | (0<<CS12) | (1<<CS11) | (1<<CS10)
 out	TCCR1B, r16

 ldi	r16, high(mtext_rtctop)
 out	OCR1AH, r16
 ldi	r16, low(mtext_rtctop)
 out	OCR1AL, r16

 ldi	r16, (1<<OCIE1A)
 out	TIMSK, r16

;; ���-���

 sei
mtext_IWNK_1:
 ldi	r16, (1<<SE) | (0<<SM2) | (0<<SM1) | (0<<SM0)
 out	MCUCR, r16
 sleep
 lds	r16, kb_Keys
 tst	r16
 breq	mtext_IWNK_1

;; ��������� �������

 clr	r16
 out	TIMSK, r16		; ������ ���������� �� �������
 out	TCCR1B, r16		; ������ ������ �������

;; �������������� SREG

 out	SREG, r0		; ����� ���� I
 ret

;
; ���������� ���������� 10 ��
;
TIM1_COMPA:
 rcall	kb_GetKeys
 reti

;
; ���� �������� ������
;
mtext_Tune:
 ldi	r22, low(mtext_TuneMenu*2)
 ldi	r23, high(mtext_TuneMenu*2)
 jmp	ur_Menu

;

mtext_TuneMenu:
 .db	                   (1<<ur_m_LeftEn) | (1<<ur_m_OkEn) | (1<<ur_m_RightEn) | (1<<ur_m_SelEn) | (1<<ur_m_OkFin)                  , 0x00
 .dw	mtext_TM_Txt1*2
 .dw	0
 .dw	mtext_E_NewFile
 .dw	mtext_E_NewFile
 .dw	mtext_E_NewFile

 .db	(1<<ur_m_DrawEn) | (1<<ur_m_LeftEn)                  | (1<<ur_m_RightEn) | (1<<ur_m_SelEn) | (1<<ur_m_OkFin)                  , 0x00
 .dw	mtext_TM_Txt2*2
 .dw	mtext_TM_RL_Draw
 .dw	mtext_TM_RL_Dec
 .dw	0
 .dw	mtext_TM_RL_Inc

 .db	(1<<ur_m_DrawEn) | (1<<ur_m_LeftEn) | (1<<ur_m_OkEn) | (1<<ur_m_RightEn) | (1<<ur_m_SelEn) | (1<<ur_m_OkFin)                  , 0x00
 .dw	mtext_TM_Txt3*2
 .dw	mtext_TM_LightDraw
 .dw	mtext_TM_LightDec
 .dw	mtext_TM_LightSave
 .dw	mtext_TM_LightInc

 .db	                   (1<<ur_m_LeftEn)                  | (1<<ur_m_RightEn) | (1<<ur_m_SelEn)                                    , 0x00
 .dw	mtext_TM_Txt4*2
 .dw	0
 .dw	mtext_Save1
 .dw	0
 .dw	mtext_Load1

 .db	                   (1<<ur_m_LeftEn)                  | (1<<ur_m_RightEn) | (1<<ur_m_SelEn)                                    , 0x00
 .dw	mtext_TM_Txt5*2
 .dw	0
 .dw	mtext_Save2
 .dw	0
 .dw	mtext_Load2

 .db	                                      (1<<ur_m_OkEn) |                     (1<<ur_m_SelEn) | (1<<ur_m_OkFin) | (1<<ur_m_Close), 0x0E
 .dw	mtext_TM_Txt6*2
 .dw	0
 .dw	0
 .dw	mtext_Quit
 .dw	0

;;

mtext_TM_Txt1:
 .db	"����� �����", 0
mtext_TM_Txt2:
 .db	"������� ������: ", 0
mtext_TM_Txt3:
 .db	"���������      : ", 0
mtext_TM_Txt4:
 .db	"��������1 �< >�", 0
mtext_TM_Txt5:
 .db	"��������2 �< >�", 0
mtext_TM_Txt6:
 .db	"������� ����", 0

;; ����� ������ �����

mtext_E_NewFile:
 lds	r16, mtext_SREG		; ����� ������� �����
 sbr	r16, 0x01		; C = 1 - Open file
 sts	mtext_SREG, r16
 rjmp	pc_Menu

;; ���������� ������� �������

mtext_TM_RL_Draw:
 lds	dd16uL, mtext_RedLine
 ldi	r17, 4
 sub	r17, dd16uL
 mov	dd16uL, r17
 jmp	DecByteOut

mtext_TM_RL_Inc:
 lds	r16, mtext_RedLine
 dec	r16
 dec	r16
 rjmp	mtext_TM_RL_1

mtext_TM_RL_Dec:
 lds	r16, mtext_RedLine

mtext_TM_RL_1:
 andi	r16, 0x03
 inc	r16
 sts	mtext_RedLine, r16
 ret

;; ���������� ����������

mtext_TM_LightDraw:
 lds	r16, mtext_LightMode	; D7.D6: x0 - ���������, 11 - ��������, 01 - ���������
 ldi	r25, '0'
 sbrc	r16, 7
 ldi	r25, '1'
 sbrs	r16, 6
 ldi	r25, 'i'
 rjmp	lcd_CharOut

mtext_TM_LightDec:
 lds	r16, mtext_LightMode
 subi	r16, 0x40
 sts	mtext_LightMode, r16
 ret

mtext_TM_LightSave:
 rcall	f_IO
.db	f_OWrite, 2
.dw	nv_mtext_LightMode
.dw	mtext_LightMode
 clc
 rcall	lcd_PrintC
.db	"Light & LShift saved", 1
 ret
 
mtext_TM_LightInc:
 lds	r16, mtext_LightMode
 subi	r16, -0x40
 sts	mtext_LightMode, r16
 ret

;; ��������� ������ ������

mtext_Quit:
 lds	r16, mtext_SREG
 sbr	r16, 0x40		; T = 1 - exit from modile
 sts	mtext_SREG, r16
 ret
