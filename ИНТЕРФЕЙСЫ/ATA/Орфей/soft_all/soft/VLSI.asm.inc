;;==========================================================================
;; �������� � VLSI
;;

.dseg
vlsi_Time:	.byte	2	; ����� ��������������� �����
vlsi_Head:	.byte	4	; ��������� ����� (ASCII): ��������� � ����� �������
vlsi_BitRate:	.byte	2	; �������
vlsi_V_L:	.byte	1	; ��������� �� (����������� �� Vol � Pan)
vlsi_V_R:	.byte	1	; ��������� �� (����������� �� Vol � Pan)
vlsi_Dirt:	.byte	1	; ���� ������������� ������ ���������� � EEPROM

vlsi_Vol:	.byte	1	; ���������	[Vol, Pan, Mode ���� ������ ��� f_IO]
vlsi_Pan:	.byte	1	; �������
vlsi_Mode:	.byte	1	; ������ ���������� ���������

.equ		vlsi_SM_DIFF	= 0
.equ		vlsi_SM_FFWD	= 1
.equ		vlsi_SM_BASS	= 7

.eseg
nv_vlsi_VolMod:	.byte	3	; �������� �������� � EEPROM, �������� vlsi_Init, ������� vlsi_Close ��� vlsi_Dirt = 1

.cseg
;
; ����� ��� ������ ��/� r18..r19 ������� r17 VLSI
;
vlsi_CRead:
 ldi	r16, spi_CSCLK_VLSI_SCI	; !CS = 0, CLKen = 1
 rcall	spi_CSCLK_Select

 ldi	r16, 0x03		; ������� ��� ��������
 rcall	spi_IO

 mov	r16, r17
 rcall	spi_IO			; ������ ����� ��������

 rcall	spi_IO
 mov	r19, r16
 rcall	spi_IO
 mov	r18, r16
 rjmp	vlsi_CIO_1

vlsi_CWrite:
 ldi	r16, spi_CSCLK_VLSI_SCI	; !CS = 0, CLKen = 1
 rcall	spi_CSCLK_Select

 ldi	r16, 0x02
 rcall	spi_IO

 mov	r16, r17
 rcall	spi_IO			; ������ ����� ��������

 mov	r16, r19
 rcall	spi_IO
 mov	r16, r18
 rcall	spi_IO

vlsi_CIO_1:
 ldi	r16, spi_CSCLK_VLSI_SDI	; !CS = 1 - ��� ���������� ��� ���������� ���������� (�� ��� �� VLSI)
 rjmp	spi_CSCLK_Select

;
; ������������� �������� VLSI [��� ��������� �������]
;
vlsi_Init:
 rcall	f_IO			; ������ ���������, ������� � ���� BASS & DIFF
.db	f_ORead, 3
.dw	nv_vlsi_VolMod
.dw	vlsi_Vol

 clr	r16			; ��������� ������ ��� ��������, ��������� �� ����
 sts	vlsi_Dirt, r16

 rcall	vlsi_CalcVol		; Vol, Pan -> V_L, V_R

 lds	r16, vlsi_Mode
 andi	r16, (1<<vlsi_SM_BASS) | (1<<vlsi_SM_DIFF)
 sts	vlsi_Mode, r16

 rjmp	Wait2ms			; � FAQ - 1 ms (...����� ����������� RESET ����� ���������� � CBUS � DBUS)

;
; ��������� VLSI � power down
;
vlsi_Sleep:
 ldi	r16, 0xFF
 sts	vlsi_V_L, r16
 sts	vlsi_V_R, r16
 rjmp	vlsi_SetTones

;
; ���������� ����� VLSI [����� ������� ���������������]
; ���������� � r20 ���������:
; 0 - Ok
; 1 - DREQ �� �������� ����� ������
; 2 - DREQ �� ���� ����� ������
; 3 - ��� SW_RESET �� ���� ����� ������� ������
;
vlsi_Open:
 clr	r17
 clr	r19
 ldi	r18, 0x04
 rcall	vlsi_CWrite

 rcall	Wait5mks		; � datasheet ������� 2 mks, � � FAQ - 5 mks

 ldi	r20, 2
 sbic	PINB, 0			; ���������� VLSI
 ret

 rcall	Wait2ms			; � datasheet ������� 250 mks, � � FAQ - 1000 mks

 ldi	r20, 1
 sbis	PINB, 0			; ���������� VLSI
vlsi_O_1:
 ret

 ldi	r20, 3
 clr	r17
 rcall	vlsi_CRead
 andi	r18, 0x04
 brne	vlsi_O_1

 clr	r20
 ldi	r16, spi_CSCLK_VLSI_SDI	; !CS = 1, CLKen = 1
 rcall	spi_CSCLK_Select

 clr	r16
 rcall	spi_IO
 clr	r16
 rcall	spi_IO			; � datasheet ������� ���� �����, � FAQ - 2

;
; ��������� ���������� VLSI (���������, �����, ...) � ���������� ��� ��������� [�� ����� ���������������]
;
vlsi_SetTones:
 clr	r17			; Mode
 clr	r19
 lds	r18, vlsi_Mode
 andi	r18, (1<<vlsi_SM_DIFF) | (1<<vlsi_SM_FFWD) | (1<<vlsi_SM_BASS)
 rcall	vlsi_CWrite


 ldi	r17, 0x0b		; Volume
 lds	r19, vlsi_V_L
 lds	r18, vlsi_V_R
 rcall	vlsi_CWrite


 ldi	r17, 0x04		; Time
 rcall	vlsi_CRead
 sts	vlsi_Time+0, r18
 sts	vlsi_Time+1, r19


 ldi	r17, 0x05		; AUDATA
 rcall	vlsi_CRead

 mov	r16, r19		; BitRate
 andi	r16, 0x01
 sts	vlsi_BitRate+0, r18
 sts	vlsi_BitRate+1, r16

 ldi	r16, 'm'		; Mono/Stereo
 sbrc	r19, 7
 ldi	r16, 's'
 sts	vlsi_Head+0, r16

 lsl	r19			; Sample rate
 andi	r19, 0b111100
 clr	r18

 ldi	ZL, low(vlsi_ST_Table*2)
 ldi	ZH, high(vlsi_ST_Table*2)

 add	ZL, r19
 adc	ZH, r18

 lpm	r17, Z+
 lpm	r18, Z+
 lpm	r19, Z+
 sts	vlsi_Head+1, r17
 sts	vlsi_Head+2, r18
 sts	vlsi_Head+3, r19
 ret

vlsi_ST_Table:
.db	" 0? "
.db	"44k1"	; 44100
.db	"48k0"	; 48000
.db	"32k0"	; 32000

.db	"22k1"	; 22050
.db	"24k0"	; 24000
.db	"16k0"	; 16000
.db	"11k3"	; 11025

.db	"12k0"	; 12000
.db	" 8k0"	;  8000
.db	" A? "
.db	" B? "

.db	" C? "
.db	" D? "
.db	" E? "
.db	" F? "

;
; ��������� ��������� �� r16 � �������� �� r17
; ����� ������������ ���� ����������� � ���������� vlsi_CalcVol
;
vlsi_ChVol:
 mov	r18, r16
 or	r18, r17
 breq	vlsi_ret

 lds	r18, vlsi_Vol
 lds	r19, vlsi_Pan

 tst	r16
 brmi	vlsi_CV_3

 add	r18, r16		; ���������� ���������
 brcc	vlsi_CV_4
 ldi	r18, 0xFF		; ��������� �������
 rjmp	vlsi_CV_4

vlsi_CV_3:
 add	r18, r16		; ���������� ���������
 brcs	vlsi_CV_4
 ldi	r18, 0x00		; ��������� ��������

vlsi_CV_4:
 add	r19, r17		; ����� ������������ �� ������������

 sts	vlsi_Vol, r18
 sts	vlsi_Pan, r19

 ldi	r16, 1			; ��������� ������ ��������, ����� ��������� � EEPROM
 sts	vlsi_Dirt, r16

;
; ������������� V_L, V_R �� Vol � Pan
; 
vlsi_CalcVol:
 lds	r18, vlsi_Vol
 mov	r19, r18

 lds	r16, vlsi_Pan
 tst	r16
 breq	vlsi_CV_1

 brmi	vlsi_CV_2

 add	r18, r16		; ������� ������, ��������� �����
 brcc	vlsi_CV_1
 ldi	r18, 0xFF		; ������� � �����
 rjmp	vlsi_CV_1

vlsi_CV_2:
 sub	r19, r16		; ������� �����, ��������� ������
 brcs	vlsi_CV_1
 ldi	r19, 0xFF		; ������� � ������

vlsi_CV_1:
 sts	vlsi_V_L, r18
 sts	vlsi_V_R, r19
vlsi_ret:
 ret

;
; �������� ������ Z+ � SDI
;
vlsi_SendBuf:
 ldi	r16, spi_CSCLK_VLSI_SDI
 rcall	spi_CSCLK_Select	; !CS = 1, CLKen = 1
 clr	r17

vlsi_SB_1:
 sbis	PINB, 0			; ��������� ���������� VLSI
 rjmp	vlsi_SB_1

	; ldi	r25, 0xc2
	; sleep
	; call	SendByte

 ld	r16, Z+
 rcall	spi_IO

 ld	r16, Z+
 rcall	spi_IO

 dec	r17
 brne	vlsi_SB_1
 ret

;
; ���������� ��������������� �����
; [���������� 2048 ����� � SDI � ���� 50 ms]
; �������������� ��������� � EEPROM ���������/��������, ���� ����
;
vlsi_Close:
 lds	r16, vlsi_Dirt
 tst	r16
 breq	vlsi_C_2
 
 rcall	f_IO			; ��������� ���������, ������� � ���� BASS & DIFF
.db	f_OWrite, 3
.dw	nv_vlsi_VolMod
.dw	vlsi_Vol

 clr	r16			; ��������� ������ ��� ���������, ����������� �� ����
 sts	vlsi_Dirt, r16

 sec
 rcall	lcd_PrintC
.db	"Volume saved ", 1

vlsi_C_2:
 ldi	r16, spi_CSCLK_VLSI_SDI
 rcall	spi_CSCLK_Select	; !CS = 1, CLKen = 1
 ldi	r17, 4
 clr	r18

vlsi_C_1:
 sbis	PINB, 0			; ��������� ���������� VLSI
 rjmp	vlsi_C_1

 clr	r16
 rcall	spi_IO

 dec	r18
 brne	vlsi_C_1
 dec	r17
 brne	vlsi_C_1
 rjmp	Wait50ms
