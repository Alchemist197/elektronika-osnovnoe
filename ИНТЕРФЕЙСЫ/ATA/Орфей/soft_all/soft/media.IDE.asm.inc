;;==========================================================================
;; �������� � IDE-����������� � �����������
;; ���������� r16, r17 [Init], r0, r4..r7
;; ���� ��� CS0 == 0, CS1 == 1 ���������, � �������� (������� IORdy) �� �������������
;;

; ���� ����� ���� ��������� IORdy:  Future Reg: 0x03, Sector Count Reg: 0x01, Command Reg: 0xef [Set Features]

.dseg
;
; IDE-��������
;
.equ	DatReg	= 0x0
.equ	ErrReg	= 0x1
.equ	FetReg	= 0x1
.equ	CntReg	= 0x2
.equ	Ad0Reg	= 0x3
.equ	Ad1Reg	= 0x4
.equ	Ad2Reg	= 0x5
.equ	DevReg	= 0x6
.equ	CmdReg	= 0x7
.equ	SttReg	= 0x7

.cseg
;
; ������������� IDE ���������� � ��������� IO
; C = 1 ��� ������
;
ide_Init:
 rcall	ide_InitPorts

 ldi	r17, 20		; �ģ� ���������� ����� 10 ������
ide_I_1:
 ldi	r16, SttReg
 rcall	ide_GetReg
 tst	r0
 brpl	ide_Ok

 push	r17
 rcall	Wait500ms
 pop	r17

 dec	r17
 brne	ide_I_1

 clc
 ret

ide_Ok:
 sec
 ret

;
; ������������� ��������� IO
;
ide_InitPorts:
 sbi	PORTC, 7	; ������������� ��� D7 pullup, ����� BUSY � ���������� ����� ������� ��� "1"
			; ��� ������� ����� ��������� ����� �������, ����� ������ ��������� ���������� �������

 sbi	DDRD, 5		; A1
 sbi	DDRD, 6		; A2
 sbi	DDRD, 7		; A0

 sbi	DDRD, 3		; !IOW
 sbi	PORTD, 3
 sbi	DDRD, 4		; !IOR
 sbi	PORTD, 4

 ret

;
; ������� ������� r16 IDE � r0
;
ide_GetReg:
 rcall	ide_SetAddr
 clr	r16
 out	DDRA, r16
 out	DDRC, r16

 cbi	PORTD, 4
 nop			; ������� ������ � ���� � ����� ������� ���������. �� ���
 in	r0, PINC
 sbi	PORTD, 4
 ret

;
; ���������� �������� �����
;
ide_PowerDown:
;{0xE1} Idle - ������� �����, �� ��������� ������� �������� ����� ����������� ���������. ���������� ����� ���� ���������� � ���� �����
;{0xE0} StandBy - ��� idle, �� �������� ������� � �������� ������ ������ ����� ��������, ��� � Idle
;{0xE6} Sleep - ����� �������� ���, ����� ������ �� RESET, ������� ��������� �� ��������

; clr	r7		; ����� ��������� ����������� SetSect
; rcall	ide_SetSect	; ������� ���������� � �������� ���������� � DevReg

 rcall	ide_InitPorts

; ���������� IDE �� �ģ�, �.�. ���� ����� ���� �� ���������,
; � ��������� ����

 ldi	r16, 0xE6
 mov	r0, r16
 ldi	r16, CmdReg

;
; �������� ������� r16 IDE �� r0
;
ide_PutReg:
 rcall	ide_SetAddr
 ldi	r16, 0xFF
 out	DDRA, r16
 out	DDRC, r16

 clr	r16
 out	PORTA, r16
 out	PORTC, r0

 cbi	PORTD, 3
; nop
 sbi	PORTD, 3
 ret

;
; ������� ����� � Z+
;
ide_GetBuf:
 clr	r16
 out	DDRA, r16
 out	DDRC, r16
 rcall	ide_SetAddr

ide_GB_1:
 cbi	PORTD, 4	; 2
 nop			; 1 ������� ������ � ���� � ����� ������� ���������. �� ���

 in	r0, PINC	; 1
 st	Z+, r0		; 2
 in	r0, PINA	; 1
 st	Z+, r0		; 2

 sbi	PORTD, 4	; 2
 inc	r16		; 1
 brne	ide_GB_1	; 2	= 14
 ret

;
; �������� ����� �� Z+
;
ide_PutBuf:
 ldi	r16, 0xFF
 out	DDRA, r16
 out	DDRC, r16
 clr	r16
 rcall	ide_SetAddr

ide_PB_1:
 ld	r0, Z+
 out	PORTC, r0
 ld	r0, Z+
 out	PORTA, r0

 cbi	PORTD, 3	; 2
; nop
 sbi	PORTD, 3	; 2

 inc	r16		; 1
 brne	ide_PB_1	; 2
 ret

;
; ������� ���������� ��� ������� � ���������� �����
; ���������� � r0 ������� ���������
;
ide_WaitBusy:
 ldi	r16, SttReg
 rcall	ide_GetReg
 tst	r0
 brmi	ide_WaitBusy
 ret

;
; ���������� �������� IDE �� LBA-������ # R4..R7
; C = 1 ��� ������ (# ������� ������������ � ����������� ���������)
; [ ���� ��� ����������� ������ 28 ���, ����� ����� ����� ��������� �� 32 ]
;
ide_SetSect:
 rcall	ide_WaitBusy

 ldi	r16, 1		; Sector count
 mov	r0, r16
 ldi	r16, CntReg
 rcall	ide_PutReg

 ldi	r16, Ad0Reg	; Low nsec  0.. 7
 mov	r0, r4
 rcall	ide_PutReg

 ldi	r16, Ad1Reg	; Mid nsec  8..15
 mov	r0, r5
 rcall	ide_PutReg

 ldi	r16, Ad2Reg	; Hgh nsec 16..24
 mov	r0, r6
 rcall	ide_PutReg

 mov	r16, r7		; Mode + Device + nsec 28..25
 cpi	r16, 0x10
 brsh	ide_SS_Fault
 ori	r16, 0xE0
 mov	r0, r16
 ldi	r16, DevReg
 rcall	ide_PutReg

ide_SS_Ok:
 sec
 ret

ide_SS_Fault:
 clc
 ret

;
; ������������� �� ���������� ����� IDE-�������� [r16] ��� ������
;
ide_SetAddr:
 cbi	PORTD, 5
 cbi	PORTD, 6
 cbi	PORTD, 7
 sbrc	r16, 0
 sbi	PORTD, 7
 sbrc	r16, 1
 sbi	PORTD, 5
 sbrc	r16, 2
 sbi	PORTD, 6
 ret

;
; ������ ������ r4..r7 � Z+
; C = 1 ��� ������
;
ide_ReadSect:
 rcall	ide_SetSect
 brcc	ide_SS_Fault

 ldi	r16, 0x20
 mov	r0, r16
 ldi	r16, CmdReg
 rcall	ide_PutReg

 rcall	ide_WaitBusy
 mov	r16, r0
 andi	r16, 0x09
 cpi	r16, 0x08	; DRQ = 1, ERR = 0
 brne	ide_SS_Fault

 rcall	ide_GetBuf
 rjmp	ide_SS_Ok

;
; ���������� ������ r4..r7 �� Z+
;
ide_WriteSect:
 rcall	ide_SetSect
 brcc	ide_SS_Fault

 ldi	r16, 0x30
 mov	r0, r16
 ldi	r16, CmdReg
 rcall	ide_PutReg

 rcall	ide_WaitBusy
 mov	r16, r0
 andi	r16, 0x09
 cpi	r16, 0x08	; DRQ = 1, ERR = 0
 brne	ide_SS_Fault

 rcall	ide_PutBuf	; ���������, ����� �� ��������� ��������� ����������
 rjmp	ide_SS_Ok	; ������, �� �� ��� �������������. �ӣ �����, ��� �����
			; ����������� ��������� ���������� device ����� �����������
			; � ���� ������ �� ������� - ������ ���� ����� ������
