;;==========================================================================
;; �������� � ������� ������� ������: ����� ����� ����,
;; � ����� ��� ���ң�/����� �� ������, �����������, �������...

.dseg

.equ	pc_MinLine	= 0				; ������ ������, ��������� ��� ������
.equ	pc_MaxLine	= lcd_YPosMax			; ��������� ������, ��������� ��� ������ + 1
.equ	pc_NumLine	= pc_MaxLine - pc_MinLine - 1	; ����� �����, ��������� ��� ������

; ��� ���������� ��������� �� 0, �� �����, �� ����� ��������� � file_Name, � �� ��������� ����������� �������
pc_Top:			.byte	1			; ����� �������� ����, ������� ��������� � pc_MinLine
pc_Curs:		.byte	1			; ����� �������� ����, �� ������� ��������� ������
pc_NumPath:		.byte	1			; ������ ����� ��������� ����

.cseg
;
; ������, � �������
;
pc_Init:
 clr	r16
 sts	pc_Top, r16
 ret

;
; ����������� ������ pc_Menu
;
pc_M_Up:
 lds	r16, pc_Curs
 tst	r16
 breq	pc_M_Up_1
 dec	r16
 sts	pc_Curs, r16
pc_M_Up_1:
 ret

;
 
pc_M_Down:
 lds	r16, pc_Curs
 inc	r16
 sts	pc_Curs, r16
pc_M_ret0:
 ret

;

pc_M_Left:
 lds	r23, pc_Curs
 rjmp	file_PrvD

pc_M_Right:
 lds	r23, pc_Curs
 rjmp	file_NxtD

;
; ����� ���� ����� ����
;
pc_Menu:

; ���������� ������

 rcall	lcd_ClrScr

; ��������� ��������� CurrentDirEnt

pc_M_1:
 clr	r23
 rcall	file_Name		; ���� � ������ �� �����������, �.�. �������� ��������� �� �� ���������� ����

; ������ ���������� ��������� � ����

 clr	r16
 ldi	XL, low(CurrentDirEnt)
 ldi	XH, high(CurrentDirEnt)
 ldi	r17, MaxDirDeep

pc_M_2:
 adiw	X, 2
 ld	r18, X+
 tst	r18
 brpl	pc_M_3			; ���� ������� ������� �� �������������� - ����� �ޣ��

 inc	r16

 dec	r17
 breq	pc_M_3			; �� ������ ����� ������� - ����� �ޣ��

 sbrs	r18, 6			; ������� ����� ����������� ����� �������
 rjmp	pc_M_2

pc_M_3:
 tst	r16			; � ������ ������� ���� ��� ������ ������ ����� ��������������
 breq	pc_M_ret0

; �������� ����������� ��������� ��� pc_Top � pc_Curs

.def	pc_r_NumPath	= r16
.def	pc_r_Top	= r17
.def	pc_r_Curs	= r18

 lds	pc_r_Top, pc_Top
 lds	pc_r_Curs, pc_Curs

 dec	pc_r_NumPath		; NumPath--;

 cp	pc_r_Curs, pc_r_NumPath	; if Curs >= NumPath then Curs := NumPath;
 brlo	pc_M_4
 mov	pc_r_Curs, pc_r_NumPath
pc_M_4:

 cp	pc_r_Curs, pc_r_Top	; if Curs < Top then Top := Curs;
 brsh	pc_M_5
 mov	pc_r_Top, pc_r_Curs
pc_M_5:

 mov	r19, pc_r_Curs
 subi	r19, pc_NumLine		; t := Curs - NumLine;

 cp	pc_r_Top, r19		; if Top < t then Top := t;
 brge	pc_M_6			; t - �������� !
 mov	pc_r_Top, r19
pc_M_6:

 tst	pc_r_Top		; if Top < 0 then Top := 0;
 brpl	pc_M_7
 clr	pc_r_Top
pc_M_7:

 sts	pc_NumPath, pc_r_NumPath
 sts	pc_Top, pc_r_Top
 sts	pc_Curs, pc_r_Curs

; ����� ��������� ����

 ldi	r17, pc_MinLine		; ����� �������� ������
 lds	r18, pc_Top		; ����� ������ � ����

pc_M_A:
 inc	r18

 push	r17
 push	r18

 rcall	lcd_GoToLine		; ������� � ��������� ������

 pop	r18
 push	r18

 ldi	r16, 0x00		; ��������� ���������� ������
 lds	r19, pc_Curs
 inc	r19
 cp	r18, r19
 brne	pc_M_E
 ldi	r16, 0x80
pc_M_E:
 rcall	lcd_SetAttr

 lds	r16, pc_NumPath
 subi	r16, -(2)
 cp	r18, r16
 brsh	pc_M_C			; ������ �ͣ� ��� (����������, ����� ��������� �� ��� ���������� �����)
 
 mov	r23, r18
 rcall	file_Name		; ����������� ��� �����
 brcs	pc_M_B

 ldi	r25, '?'		; � ������ ������ ������� ����� ������� '?'
 rcall	lcd_CharOut
 rjmp	pc_M_C

pc_M_B:
 ld	r25, Z+			; � ������ ����������� ������� - ����� �����
 tst	r25
 breq	pc_M_C
 rcall	lcd_CharOut
 rjmp	pc_M_B

pc_M_C:
 rcall	lcd_ClrEol		; ������� ����� ������

 pop	r18
 pop	r17

 inc	r17
 cpi	r17, pc_MaxLine
 brne	pc_M_A

; ������� �� �������

pc_M_D:
 call	ur_KeybRoute
.db	Key2mask, 0x00
.dw	pc_M_Up

.db	Key6mask, 0x00
.dw	pc_M_Left

.db	Key4mask, 0x00
.dw	pc_M_Down

.db	Key7mask, 0x80
.dw	pc_M_Right

 rcall	Wait50ms

 lds	r16, kb_Keys
 tst	r16
 breq	pc_M_D

 cpi	r16, Key0mask
 brne	pc_M_11
 rjmp	kb_WaitReleaseKeys	; ������� ������� ���������� �������

pc_M_11:
 rjmp	pc_M_1

;
; ����� �������� �������� ������/�����
; ���� �� ������� ������� ������� ������� ���� ������� ���� � root - ���������� file_Prev / file_Next
;
pc_PrevDir:
 rcall	pc_FindCurDeep
 rjmp	file_PrvD

pc_NextDir:
 rcall	pc_FindCurDeep
 rjmp	file_NxtD

;
; ���������� � r23 ������� �������� �������� -1 (C == 0) ���� 0xFF (C == 1) - ���� ������� - root
;
pc_FindCurDeep:
 clr	r23
 rcall	file_Name		; �� ����� �����������, ������ �� ������ ������, ����� DirEnt ���������

 ldi	XL, low(CurrentDirEnt)
 ldi	XH, high(CurrentDirEnt)
 ldi	r16, MaxDirDeep

pc_FCD_1:
 adiw	X, 2
 ld	r17, X+
 andi	r17, 0xC0
 cpi	r17, 0x80
 brne	pc_FCD_2
 dec	r16
 brne	pc_FCD_1

pc_FCD_2:
 ldi	r23, MaxDirDeep-1
 sub	r23, r16
 ret
