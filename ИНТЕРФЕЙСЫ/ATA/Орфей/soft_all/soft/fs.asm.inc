;;==========================================================================
;; Wrapper �������� ������. ����������� API ��� ��������� ��
;; C == 1 � ������ ������ ���������� ��������
;;

; ���� �������� ����������. ���������� ���������:
; file_Init - ������ ���������� ������ ��� ��������� ����������
; file_DvCh - ������ ���������� ��� ����� CurrentDevice ��� CurrentPart
; file_Open - ��������� ���� �� Current Dev/Part/DE, ������������� ������ �� ������ �����
; file_OpenAndHold - ��� � file_Open, �� ������� ��������� DirEnt � EEPROM
; file_Read - ������ ������
; file_Step - ��������� ������ �� ��������� ������
; file_PrvD - �������� ����� ���������� ������� ��� ���� �� ������ r23
; file_NxtD - �������� ����� ��������� ������� ��� ���� �� ������ r23
; file_Prev - �������� ����� ���������� ����
; file_Next - �������� ����� ��������� ����
; file_Idnt - ���������� C = 1, ���� ������ � ������ ����� �� ������ mp3-�����
; file_Name - ���������� ��� ����� (r23 == 0) ��� ������ �� ���������, �� ������� ��������� Current. �������� ������� �� open !!! ���������: koi8-r

; ���� �������� �� ������ ����������, ������ � ������� �������� �
; ������� ������� � ���������. ������ ������ ��������� �� �����������,
; ��������� ������ ������� �� ����, ����� file_Open ���������� C = 0 (� ����� � ������,
; ���� ������ ����� == 0)

; file_Open, file_Read � file_Name ��������� Z � �������� ������ ������

; file_Prev � file_Next, � ������ ��������� ���������� �������� ������, ����������
; � �������� ������ �������� �������, � ����� ��������� - ��� ����������� �������� ����� ��������� � ������������
; (�.�. �� ������� ��������� ����� �������� ������� �, ��������, ���������� ����� ��������)

; file_Idnt ���������� ������ �������� � �� ����� ��-��������� ����������


.dseg
; ���� � ��������� �����
.equ	MaxDirDeep	= 10
CurrentDevice:	.byte	1		; ���������� (1 - HDD, 0 - SD). ����������� ������ ��������� �����
CurrentPart:	.byte	1		; ���� � ������� �������� (0 - ���� ����� boot block). ����������� ������ ��������� �����

; ������ ��������.
; ������ ����� - ����� 8.3entry � ���������. ����������� ���������� ��������
; ������ ���� - ��������� ������ � �������:
;  D7 - ��� ���� ����� ������� ������� ����������������: ������ ���� � ���� ������ � ��� ����������� ������ �����������
;  D6 - ��� ����� - ��������� � �������, ����������� ������ �� ���������
; !D5 - ����� ��������� (����� �� ������� �������) [� ���� ������ ����� ����� ����� ��������������]
;  D4 - ����� ��������� (�� ���������� ������ �������)
; ����������� ���������� ������ � �������
CurrentDirEnt:	.byte	MaxDirDeep*3

; ������� ������� � �����
CurrentCluster:	.byte	4		; ����� �������� �� ��������
CurrentSecInCl:	.byte	1		; ����� ������� ������ ��������

CurrentSize:	.byte	4		; ��ߣ� �����: ��������������� Open-�� (�������� � Name-��), ���������� � ����������� Step-��

; ���������� �� �������
fs_errno:	.byte	1		; ����� ������ ����� ���� ������ ��� ��������� ������

;.include "fs.EXT.asm.inc"
.include "fs.FAT.asm.inc"
;.include "fs.UFS.asm.inc"

.eseg
; ��� ���� � EEPROM. �� ����������������� ������� file_Init � �����������...
nv_CurrentDevPrt:	.byte	2		; ...file_DvCh
nv_CurrentDirEnt:	.byte	MaxDirDeep*3	; ...file_OpenAndHold

.cseg
;
; ������������� ����������. ����������� ��������� �� EEPROM,
; ����� �������� � ������������ �ӣ ��������� (file_DC_1)
;
file_Init:
 rcall	f_IO
.db	f_ORead, MaxDirDeep*3
.dw	nv_CurrentDirEnt
.dw	CurrentDirEnt

 rcall	f_IO
.db	f_ORead, 2
.dw	nv_CurrentDevPrt
.dw	CurrentDevice

 rjmp	file_DC_1

;
; ���������� ��� ����� ���������� ��� �������
; ��������� ��������� � EEPROM, ����� ��������� ��� ���������
;
file_DvCh:
 rcall	f_IO
.db	f_OWrite, 2
.dw	nv_CurrentDevPrt
.dw	CurrentDevice

file_DC_1:
 rcall	media_Init
 rcall	lcd_PrintC
.db	"Media init: ", 1
 brcc	file_Fault0

 rcall	fsFAT_Init
 rcall	lcd_PrintC
.db	"FS init: ", 1
 ret

;
; ��������� CurrentDirEnt � EEPROM, ����� ��������� ����
;
file_OpenAndHold:
 rcall	f_IO
.db	f_OWrite, MaxDirDeep*3
.dw	nv_CurrentDirEnt
.dw	CurrentDirEnt

;
; ��������� ����
;
file_Open:
 rjmp	fsFAT_Open

;
; ������ ��������� ������ �� �����
;
file_Read:
 rjmp	fsFAT_Read

;
; ��������� ������ �� ��������� ������
;
file_Step:
 rjmp	fsFAT_Step

;
; ��������� � ����������� ����� ��� �������� ������ r23
; ���� �� ���� ������ ������ ��� ��������� - ��������� �� ����� ������� ������
;
file_PrvD:
 ldi	r17, 0x00
 rcall	file_ND_FillDirEnt
 rjmp	file_Prev

;
; ��������� � ���������� ����� ��� �������� ������ r23
; ���� �� ���� ������ ������ ��� ��������� - ��������� �� ����� ������� ������
;
file_NxtD:
 ldi	r17, 0xFF
 rcall	file_ND_FillDirEnt
 rjmp	file_Next

; ��������� DirEnt �� ������� r23 (�������� ţ) ���������� r17

file_ND_FillDirEnt:
 ldi	ZL, low(CurrentDirEnt)
 ldi	ZH, high(CurrentDirEnt)
 clr	r16

file_ND_CDE_2:
 cp	r23, r16
 brsh	file_ND_CDE_1
 st	Z+0, r17
 st	Z+1, r17
file_ND_CDE_1:
 adiw	Z, 3
 inc	r16
 cpi	r16, MaxDirDeep
 brne	file_ND_CDE_2
 ret

;
; ��������� � ����������� �����
;
file_Prev:
 rjmp	fsFAT_Prev

;
; ��������� � ���������� �����
;
file_Next:
 rjmp	fsFAT_Next

;
; ��������� �������������� ����� � MP3
; �������� ������� �� open !!!
;
file_Idnt:
 rcall	file_Open
 brcc	file_Fault0

 ldi	ZL, low(IDEBuf0)
 ldi	ZH, high(IDEBuf0)
 rcall	file_Read
 brcc	file_Fault0

 lds	r16, IDEBuf0+0
 lds	r17, IDEBuf0+1
 lds	r18, IDEBuf0+2

 cpi	r16, 'I'		; �������� �� ������ id3-����
 brne	file_I_0
 cpi	r17, 'D'
 brne	file_I_0
 cpi	r18, '3'
 breq	file_Ok0

file_I_0:
 cpi	r16, 0xFF		; �������� �� ������ mpeg-������
 brne	file_Fault0
 cbr	r17, 0x1F
 cpi	r17, 0xE0
 brne	file_Fault0

file_Ok0:
 sec
 ret

file_Fault0:
 clc
 ret

;
; ���������� ��� �������� ����� (r23 == 0) ��� ������ �� ��������� (r23 > 0),
; � ������� ����� ����, ��� ��������� �� ASCIIZ � Z.
; � r23 ���Σ��� 0 (��� C = 1), ���� ����������� ���������� ������� �� ����������
; �������� ������� �� open !!!
;
file_Name:
 rjmp	fsFAT_Name
