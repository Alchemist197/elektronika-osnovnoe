;;==========================================================================
;; �������������� ����������
;; ���������� r23, r24:r25, Y
;;

.cseg
;
; ������� ������. ������� ���������� �� (Y)
; ��������� ascii.hex -> r24:25. ���� ����� ������� -> C = 1, ����� r24:r25 �� ��������
;
Str2Val:
 rcall	Char2Val
 brcc	cv_ret
 mov	r24, r23
 clr	r25

sv_1:
 rcall	Char2Val
 brcc	cv_sec_ret
 lsl	r24
 rol	r25
 lsl	r24
 rol	r25
 lsl	r24
 rol	r25
 lsl	r24
 rol	r25
 or	r24, r23
 rjmp	sv_1

;
; ���� ������ (Y) - hex-����� -> C = 1, Y++, r23 - value
;
Char2Val:
 ld	r23, Y
 subi	r23, '0'
 brlo	cv_1
 cpi	r23, 10
 brlo	cv_2
 subi	r23, 'A'-'0'
 cbr	r23, 0x20
 brlo	cv_1
 cpi	r23, 6
 brlo	cv_3

cv_1:
 clc
cv_ret:
 ret

cv_3:
 subi	r23, -(10)
cv_2:
 adiw	Y, 1
cv_sec_ret:
 sec
 ret

;
; ��������� ������. ��������� ������� ������ � ������� � ���������� ���������� �� ��������������� ������������
; � ��� ����� ������������ ��� ��������, ����� r28:r29 [Y] - �� ������ ��������� �� ��������� ������������� ������
;
as_Table:
.db	']', 0
.dw	cmd_IDE_GetReg
.db	'[', 0
.dw	cmd_IDE_PutReg
.db	'<', 0
.dw	cmd_IDE_ReadBuf
.db	'|', 0
.dw	cmd_IDE_ViewBuf0
.db	'-', 0
.dw	cmd_IDE_ViewBuf1
.db	'>', 0
.dw	cmd_IDE_WriteBuf
.db	'R', 0
.dw	cmd_IDE_ReadSect
.db	'W', 0
.dw	cmd_IDE_WriteSect
.db	'z', 0
.dw	cmd_SD_ReadSect
.db	'x', 0
.dw	cmd_SD_WriteSect

.db	'i', 0
.dw	cmd_SendFile
.db	'p', 0
.dw	cmd_media_PT
.db	'n', 0
.dw	cmd_NextFATEntry
.db	'P', 0
.dw	cmd_SetPath
.db	'm', 0
.dw	cmd_MenuPath
.db	'(', 0
.dw	cmd_PrevOnDeep
.db	')', 0
.dw	cmd_NextOnDeep
.db	'N', 0
.dw	cmd_GetName
.db	't', 0
.dw	cmd_TypeFile
.db	'{', 0
.dw	cmd_FilePrev
.db	'}', 0
.dw	cmd_FileNext

.db	'T', 0
.dw	cmd_LCD_text
.db	'o', 0
.dw	cmd_MenuTest

.db	'S', 0
.dw	cmd_SPI_CLKen
.db	's', 0
.dw	cmd_SPI_IO

.db	'w', 0
.dw	cmd_WFlash
.db	'r', 0
.dw	cmd_RFlash

.db	'k', 0
.dw	cmd_Speed
.db	'l', 0
.dw	cmd_Debug

.db	'?', 0
.dw	cmd_Help
.db	13, 0
.dw	cmd_Break

.db	0
;
AnalyseString:
 ldi	YL, low(LineBuf)
 ldi	YH, high(LineBuf)

as_1:
 ldi	ZL, low(as_Table*2)
 ldi	ZH, high(as_Table*2)
 ld	r25, Y+
 rcall	SendByte

as_2_next:
 lpm	r24, Z
 tst	r24
 breq	as_4_se

 cp	r24, r25	; cpse may be ?
 breq	as_3_find
 adiw	Z, 4
 rjmp	as_2_next

; ������ �� ������ � �������

as_4_se:
 ldi	r25, '?'
 rcall	SendByte
 rjmp	as_1

; ������ ������ ! ������� �� ���������-����������

as_3_find:
 adiw	Z, 2
 lpm	r24, Z+
 lpm	r25, Z+
 movw	Z, r24
 icall
 rjmp	as_1
