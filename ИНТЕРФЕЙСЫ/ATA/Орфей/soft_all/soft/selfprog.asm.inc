; BOOTSZ0,1 = 1, ���������� ��� ����� � ������ RWW/NRWW, ����������� - � NRWW
; ���� ������� ����������� fuse'��� �� ���������� �����, ����� ��� ������ ���� �������� �������
.cseg
 .org	0x3F00
;;==========================================================================
;; Boot Loader - Self prog
;;
.def	rACC	= r16
.def	rSPM_ARG= r17
.def	rCNT	= r18
.def	rCRC	= r19
.def	rDEB_CHR= r21

BOOTRES:			; ���� ���������� ���������� ��� ������

 ldi	rACC, high(RAMEND)
 out	SPH, rACC		; Set Stack Pointer to top of RAM
 ldi	rACC, low(RAMEND)
 out	SPL, rACC

;
; ������������� USART
;
 ldi	rACC, high(12)		; �������� USART ~38.400 b/s ��� 8 ��� ��������
 out	UBRRH, rACC
 ldi	rACC, low(12)
 out	UBRRL, rACC

 ldi	rACC, (1<<RXEN) | (1<<TXEN)
 out	UCSRB, rACC		; ������������, ��� ������ ����� �������� ����� ��ɣ�����
 
; ldi	rACC, (1<<URSEL) | (3<<UCSZ0)	; By default ��� � ��� �����������
; out	UCSRC, rACC

;
; ������ ����� 0.5 �
;
 ldi	rACC, 15
 clr	ZL
 clr	ZH

bl_1:			; 4 * 65536 = 262144 ~= .032 �
 adiw	Z, 1		; 2
 brne	bl_1		; 2

 dec	rACC
 brne	bl_1

;
; ��������� ������� ������� 'R' � ������ ��ɣ�� USART
;
 in	rACC, UDR	; ���������� �� ��������� - ���� ���ģ� ������ ������ ��� � ��� ����� ����
 cpi	rACC, 'R'
 breq	bl_2
 jmp	0x0000		; ���� 'R' �� ������� - ��������� � ���������� �������� ���������
; clr	ZL
; clr	ZH
; ijmp
bl_2:

;
; ������������ rs232
;
 ldi	rACC, 'S'	; ����� ���������� ����� CPU, ������������ ������ ����� ����������� ������� ���� 'R'.
 rcall	bl_SendByte	; ����� ������ ��� ��� ������������ (���� ������������), �� ���������� 'S'

bl_Sync:		; � ���� ������� ������������� � ���� ����� 'N'. ��� ������ ���� ���������� ������������,
 rcall	bl_RecvByte	; ����� ���� ������� ��������� ���������������� � ������ ���� ������� �����
 cpi	rACC, 'N'	; ��������� � ������������ �� ����������.
 brne	bl_Sync

;
; ���� ��������������������
; ���� ������ ����������, �� RESET'�.
;
bl_Main:
 rcall	bl_RecvPage	; ������������ ������ ��������� ����� ��������, ������ � CRC
 ldi	rACC, 'C'
 tst	rCRC
 brne	bl_SendState	; ������ ����������� ����� ��� ��ɣ�� ��������

 rcall	bl_CWPage
 ldi	rACC, 'O'
 breq	bl_SendState	; �������� ���������������� ��������
 ldi	rACC, 'F'	; ������ �������� ������

bl_SendState:
 rcall	bl_SendByte	; ����� ���� �� ������ ��������� ���� ���������
 rjmp	bl_Main

;
; ������� � ������ ��������
; ��������/�������� ���������� � X, ����� ������ � SPM_Buffer
; ���������� Z = 1 � ������ ������
;
bl_CWPage:
 rcall	bl_SetBufferPtr
 movw	ZL, XL

 ldi	rSPM_ARG, (1<<PGERS) | (1<<SPMEN)	; �������
 ldi	rDEB_CHR, '1'
 rcall	bl_SPM

 ldi	rSPM_ARG, (1<<RWWSRE) | (1<<SPMEN)	; ���������� ������� � rww-section
 ldi	rDEB_CHR, '2'
 rcall	bl_SPM

bl_CWP_1:
 ld	r0, Y+
 ld	r1, Y+
 ldi	rSPM_ARG, (1<<SPMEN)			; ���������� ������ ����� ������
 ldi	rDEB_CHR, '3'
 rcall	bl_SPM
 adiw	Z, 2
 dec	rCNT
 dec	rCNT
 brne	bl_CWP_1

 movw	ZL, XL					; �������� ������ ���� ����� 0, �������� - ��� ��������, � ������� �������������� ������
 ldi	rSPM_ARG, (1<<PGWRT) | (1<<SPMEN)	; ������ ��������
 ldi	rDEB_CHR, '4'
 rcall	bl_SPM

 ldi	rSPM_ARG, (1<<RWWSRE) | (1<<SPMEN)	; ���������� ������� � rww-������
 ldi	rDEB_CHR, '5'				; NB: RWWSRE �� ��������� � ������ ������ m32def.inc !
 rcall	bl_SPM

 rcall	bl_SetBufferPtr
 movw	ZL, XL

bl_CWP_2:					; �������� ���������� ��������
 lpm	r0, Z+
 ld	r1, Y+
 cpse	r0, r1
 rjmp	bl_CWP_3
 dec	rCNT
 brne	bl_CWP_2
 ret						; Z = 1

bl_CWP_3:
 clz
 ret

;
; ��������� Y �� ������ ������ � ���������� � �������� rCNT � ������ ������ � ������
;
bl_SetBufferPtr:
 ldi	YL, low(IDEBuf0)
 ldi	YH, high(IDEBuf0)
 ldi	rCNT, 128
 ret

;
; ��������� spm, ���������� ����������. �������������� rSPM_ARG -> SPMCR
;
bl_SPM:
 out	UDR, rDEB_CHR
 in	rACC, SPMCR
 sbrc	rACC, SPMEN
 rjmp	bl_SPM
 out	SPMCR, rSPM_ARG
 spm
 ret

;
; ��������� ��������: �����, ������, CRC
; ���������� � rACC ��������, � � rCRC - ����������� CRC
;
bl_RecvPage:
 clr	rCRC
 rcall	bl_RecvByte	; �����
 mov	XL, rACC
 rcall	bl_RecvByte
 mov	XH, rACC

 rcall	bl_SetBufferPtr
bl_3:
 rcall	bl_RecvByte	; ������
 st	Y+, rACC
 dec	rCNT
 brne	bl_3
			; CRC
;
; rs232 -> rACC, rCRC += rACC
;
bl_RecvByte:
 sbis	UCSRA, RXC	; Wait for data to be received [ControlStatusRegisterA]
 rjmp	bl_RecvByte
 in	rACC, UDR
 add	rCRC, rACC
 ret

;
; rACC -> rs232
;
bl_SendByte:		; Wait for empty transmit buffer
 sbis	UCSRA, UDRE	; ������� �������, ���� ��� ���������� [DataRegisterEmpty, TransmitComplete]
 rjmp	bl_SendByte
 out	UDR, rACC
 ret
