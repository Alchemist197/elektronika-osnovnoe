{ $define READONLY}

Uses Encoding;
{$PACKRECORDS 1}
{$Link ylib}
{$LinkLib c}
{$i dir.inc}

type
  tSec0 = Record
    jmpBoot	: Array[0..2] of Byte;
    OEM_name	: Array[0..7] of Char;
    Byte2Sec	: Word;
    Sec2Clust	: Byte;
    ReservSec	: Word;
    NumFAT	: Byte;
    RootEntCnt	: Word;
    TotSec16	: Word;
    Media	: Byte;
    FATSz16	: Word;
    Sec2Track	: Word;
    Heads	: Word;
    OffsetSlice	: Cardinal;
    TotSec32	: Cardinal;
    // ������ ����� FAT16 � FAT32 ���������� ��������
    FATSz32	: Cardinal;
    ExtFlag	: Word;		// �������� FAT
    FSVersion	: Word;
    RootClust	: Cardinal;
    FSInfoOfs	: Word;
    BackBootSec	: Word;
    R0		: Array[1..12] of Byte;
    DrvNum	: Byte;
    R1		: Byte;
    Sign29h	: Byte;
    VolID	: Cardinal;
    VolLab	: Array[1..11] of Char;
    FSType	: Array[1..8] of Char;
    R2		: Array[$5A..$1FD] of Byte;
    SignAA55h	: Word;
  End;

  tDirEntDesc	= Record
    desc83	: tDirRecord;
    CountLFN	: Cardinal;
    descLFN	: Array[0..31] of tLngRecord;
    Name	: AnsiString;
  end;

//  tSec = Array[0..511] of Byte;

  tAction	= (aRead, aWrite);

var
  fl		: File;
  pFAT32	: ^Cardinal;
  pFAT16	: ^Word absolute pFAT32;
  ClustTwoOfs,
  FATSz		: Cardinal;
  TypeFAT	: (FAT12, FAT16, FAT32);
  BootSec	: tSec0;

{$i myfilepos.inc}

Procedure GetBoot;
begin
//  Read(fl, tSec(BootSec));
  BlockRead(fl, BootSec, 512);
  With BootSec do Begin
    if (RootEntCnt = 0) xor (FATSz16 = 0) then begin
      WriteLn('������ ����������� ���� FAT'); Halt(1);
    end;

    if (RootEntCnt = 0) and (FATSz16 = 0) then begin
      FATSz := FATSz32;
      TypeFAT := FAT32;
    end else if FATSz16 <= 12 then begin
      WriteLn('FAT12 �� ��������������'); Halt(2);
    end else begin
      FATSz := FATSz16;
      TypeFAT := FAT16;
    end;
    ClustTwoOfs := ReservSec + (NumFAT * FATSz) + ((RootEntCnt+15) div 16);

    WriteLn('������� # 2 ���������� � ������� ', ClustTwoOfs);
    Write  ('FAT type    : ');
    case TypeFAT of
      FAT12: WriteLn('FAT12');
      FAT16: WriteLn('FAT16');
      FAT32: WriteLn('FAT32');
    end;
    WriteLn('Jmp         : $', HexStr(jmpBoot[0], 2), ' ', HexStr(jmpBoot[1], 2), ' ', HexStr(jmpBoot[2], 2));
    WriteLn('OEM name    : ', OEM_name);
    WriteLn('Byte2Sec    : ', Byte2Sec);
    WriteLn('Sec2Clust   : ', Sec2Clust);
    WriteLn('ReservSec   : ', ReservSec);
    WriteLn('NumFAT      : ', NumFAT);
    WriteLn('RootEntCnt  : ', RootEntCnt);
    WriteLn('TotSec16    : ', TotSec16);
    WriteLn('Media = $F8 : $', HexStr(Media, 2));
    WriteLn('FATSz16     : ', FATSz16);
    WriteLn('Sec2Track   : ', Sec2Track);
    WriteLn('Heads       : ', Heads);
    WriteLn('OffsetSlice : ', OffsetSlice);
    WriteLn('TotSec32    : ', TotSec32);
    // ������ ����� FAT16 � FAT32 ���������� ��������
    if TypeFAT = FAT32 then begin
      WriteLn('FATSz32     : ', FATSz32);
      WriteLn('Act FAT     : ', ExtFlag);
      WriteLn('FSVersion   : ', FSVersion);
      WriteLn('RootClust   : ', RootClust);
      WriteLn('FSInfoOfs   : ', FSInfoOfs);
      WriteLn('BackBootSec : ', BackBootSec);
      WriteLn('DrvNum      : ', DrvNum);
      WriteLn('Sign = $29  : $', HexStr(Sign29h, 2));
      WriteLn('VolID       : $', HexStr(VolID, 8));
      WriteLn('VolLab      : ', VolLab);
      WriteLn('FSType      : ', FSType);
      WriteLn('Sign = $AA55: $', HexStr(SignAA55h, 4));
    end;
  End;
end;


Procedure GetFAT;
begin
  GetMem(pFAT16, FATSz * 512);
  SetSec(BootSec.ReservSec);
  BlockRead(fl, pFAT16^, FATSz * 512);
end;


Const
  ResCluster = $FFFFFF6;
//  FreCluster = $0000000;
//  BadCluster = $FFFFFF7;
  EofCluster = $FFFFFFF;

Function GetFATElement(Const Clust: Cardinal): Cardinal;
begin
  if TypeFAT = FAT16 then begin
    GetFATElement := (pFAT16 + Clust)^;
    if GetFATElement = $FFFF then GetFATElement := EofCluster else
    if (GetFATElement >= $FFF6) and (GetFATElement < $FFFF) then GetFATElement := ResCluster;
  end else begin
    GetFATElement := (pFAT32 + Clust)^;
    if (GetFATElement >= ResCluster) and (GetFATElement < EofCluster) then GetFATElement := ResCluster;
  end;

  if (GetFATElement = ResCluster) or (GetFATElement < 2) then begin
    WriteLn('�������� ������� � ������� FAT'); Halt(3);
  end;
end;

{
Procedure BlockView(var Buf);
var n: Cardinal; p: ^Byte;
begin
  p := @Buf;
  For n := 0 to 511 do begin
    if n and $0F = 0 then Write(#10, HexStr(n, 4), ' - ');
    Write(' ', HexStr((p+n)^, 2));
    if (p+n)^ in [32..126] then Write(chr((p+n)^)) else Write('.');
  end;
  WriteLn;
end;
}

Function NextSector(Var Clust, Count: Cardinal; Const Act: tAction; Var Buf): boolean; // Clust = 0 ��� ��������� ��������
var t: Cardinal;
begin
//  WriteLn('CountSect: ', Count);
  if Clust = 0 then begin
    t := Count;
    inc(t, BootSec.ReservSec);
    inc(t, FATSz * BootSec.NumFAT);
    NextSector := Count < ((BootSec.RootEntCnt+15) div 16);
    if not NextSector then Exit;

    SetSec(t);

    inc(Count);
  end else begin
    NextSector := Clust <> EofCluster;
    if not NextSector then Exit;

    t := Count;
    inc(t, ClustTwoOfs);
    inc(t, BootSec.Sec2Clust * (Clust - 2));
    SetSec(t);

    inc(Count);
    if Count >= BootSec.Sec2Clust then begin
      Count := 0;
      Clust := GetFATElement(Clust);
    end;
  end;
{$ifdef READONLY}
  if Act = aRead then BlockRead(fl, Buf, 512) else BlockView(Buf);
{$else}
  if Act = aRead then BlockRead(fl, Buf, 512) else BlockWrite(fl, Buf, 512);
{$endif}
end;


Function CutRightSpace(Const s: String): AnsiString;
var n: Cardinal;
Begin
  CutRightSpace := s;
  For n := Length(s) downto 1 do If s[n] <> ' ' then Break;
  SetLength(CutRightSpace, n);
End;


Procedure ReBuildLFN(Var DirEnt: tDirEntDesc);
var c, LFNIndex: Cardinal; LFN: Array[0..999] of UChar;

  Procedure AddUChars(const s: Array of UChar);
  Var n: Cardinal;
  Begin For n := Low(s) to High(s) do begin LFN[LFNIndex] := s[n]; inc(LFNIndex) end End;

begin With DirEnt do begin
  if descLFN[0].Cnt and $40 = 0 then begin
    WriteLn('!!! Like FreeBSD LFN bug. Fixed.');
    descLFN[0].Cnt := descLFN[0].Cnt or $40;
  end;

  LFNIndex := 0;

  For c := CountLFN-1 DownTo 0 do With DirEnt.descLFN[c] do begin
    AddUChars(nm1);
    AddUChars(nm2);
    AddUChars(nm3);
  end;

  LFN[LFNIndex] := 0;
  Name := u2k_encode(LFN);
end end;


function cmp(const d1, d2: tDirEntDesc): Boolean;
begin
  cmp := k2a_encode(d1.Name) < k2a_encode(d2.Name);
end;


Procedure ReSortDir(const Clust: Cardinal); // Clust = 0 ��� ��������� ��������
var
  Dir		: Array[0..3999] of tDirEntDesc;
  SecCount	: Cardinal;
  Buf		: tDirSector;
  SName		: String[15];

// ==============================================================================

  Procedure Clean;
  var cd: Cardinal;
  begin
    For cd := Low(Dir) to High(Dir) do With Dir[cd] do begin Name := ''; CountLFN := 0 end;
  end;

// ==============================================================================

  Procedure ReadDir(Clust: Cardinal);
  var c, cd: Cardinal;
  begin
    SecCount := 0; cd := Low(Dir);

    While NextSector(Clust, SecCount, aRead, Buf) do begin
      For c := 0 to 15 do With Dir[cd] do begin

	case Buf[c].Name[0] of
          #0:	Exit;
	  #$e5:	Begin CountLFN := 0; Continue; End;
	end;

	if Buf[c].Attr = $0F then begin
          Move(Buf[c], descLFN[CountLFN], 32);
	  inc(CountLFN);
	end else begin
	  Name := '+';
	  Move(Buf[c], desc83, 32);
	  inc(cd);
	end;

      end;
    end;
  end;

// ==============================================================================

  Procedure Sort;
  
    Procedure InSort(l, r: LongInt);
    Var i, j: LongInt; x, y: tDirEntDesc;
    Begin
      i := l; j := r; x := Dir[(l+r) DIV 2];
      Repeat
	while cmp(Dir[i], x) do i := i + 1;
	while cmp(x, Dir[j]) do j := j - 1;
	If i <= j then
	Begin
	  y := Dir[i]; Dir[i] := Dir[j]; Dir[j] := y;
	  i := i + 1; j := j - 1;
	End;
      Until i > j;
      If l < j then InSort(l, j);
      If i < r then InSort(i, r);
    End;

  Begin
    if Dir[Low(Dir)].desc83.Name[0] = '.' then InSort(Low(Dir)+2, High(Dir)) else InSort(Low(Dir), High(Dir));
  End;

// ==============================================================================

  Procedure View;
  var cd: Cardinal;
  begin
    For cd := Low(Dir) to High(Dir) do With Dir[cd] do if Name <> '' then begin
      SName := a2k_encode(CutRightSpace(desc83.Name) + '.' + CutRightSpace(desc83.Ext));
      if CountLFN = 0 then begin
	Name := SName;
	WriteLn('S [', cd, ']: ', Name);
      end else begin
	ReBuildLFN(Dir[cd]);
	WriteLn('L [', cd, ']: ', SName, ' == ', Name);
      end;
    end;
  end;

// ==============================================================================

  Procedure WriteDir(Clust: Cardinal);
  var cd, lcd, c: Cardinal;
  
    procedure NextDir;
    begin while cd <= High(Dir) do if Dir[cd].Name = '' then inc(cd) else break end;

  begin
    SecCount := 0; cd := Low(Dir); NextDir; lcd := 0;

    Repeat
      For c := 0 to 15 do if cd > High(Dir) then begin
        FillChar(Buf[c], 32, 0)
      end else With Dir[cd] do begin
	if CountLFN = 0 then begin
	  Move(desc83, Buf[c], 32);
	  lcd := 0;
	  inc(cd); NextDir;
	end else begin
	  Move(descLFN[lcd], Buf[c], 32);
	  dec(CountLFN); inc(lcd);
	end;
     end;
    Until not NextSector(Clust, SecCount, aWrite, Buf);
  end;

// ==============================================================================

  Procedure SubDirs;
  var cd, c: Cardinal;
  begin
    For cd := Low(Dir) to High(Dir) do With Dir[cd] do if Name <> '' then begin
      if (desc83.Attr and $10 <> 0) and (Name[1] <> '.') then begin
	c := desc83.cH;
	c := (c shl 16) or desc83.cL;
	WriteLn('RC: ', Name, ' [', c, ']');
	ReSortDir(c);
      end;
    end;
  end;

// ==============================================================================

begin
  Clean;
  ReadDir(Clust);
  View;
  Sort;
  View;
  WriteDir(Clust);
  SubDirs;
end;


begin
  WriteLn('tSec size = ', SizeOf(tSec0));
  if SizeOf(tSec0) <> 512 then RunError;
{$ifdef READONLY}
  FileMode := 0;
{$endif}
//Assign(fl, '/dev/da2s1'); Reset(fl, 1);
//    Assign(fl, '/dev/ad0s5'); Reset(fl, 1);
//    Assign(fl, '/dev/ad2s5'); Reset(fl, 1);
  Assign(fl, ParamStr(1)); Reset(fl, 1);
  GetBoot;
  GetFAT;

  if TypeFAT = FAT32 then ReSortDir(BootSec.RootClust) else ReSortDir(0);

  Close(fl);
end.
