#include <stdio.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>

void MyFilePos(unsigned int fd, unsigned int sec) {
  off_t ofs;

  ofs = sec;
  ofs *= 512;
  if (lseek(fd, ofs, SEEK_SET) != ofs) {
    perror("lseek");
    exit(2);
  };
}
