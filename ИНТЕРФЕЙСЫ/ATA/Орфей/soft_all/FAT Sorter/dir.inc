{
Const
  InvalidChars	= '?*"/\';	// ��� ������� � ������ ������ ����� �������� �� '_'
}

Type

tDirRecord = Record
  Name	: Array[0..7] of Char;
  Ext	: Array[0..2] of Char;
  Attr	: Byte;			// file if & $18 = 0
  r0	: Array[$c..$13] of Byte;
  cH	: Word;
  r1	: Cardinal;
  cL	: Word;
  sz	: Cardinal;
end;

tLngRecord = Record
  Cnt	: Byte;
  nm1	: Array[0..4] of UChar;
  Attr	: Byte;
  Typ	: Byte;
  Hash	: Byte;
  nm2	: Array[0..5] of UChar;
  r1	: Word;
  nm3	: Array[0..1] of UChar;
end;

(*
���� hash ����p��� ��� �� ����������� ���� ��p������������� ��p������
����� (DOS 8+3). ��� ��������� ��������� ��p����:

unsigned char calchash(unsigned char name[11])
{
   unsigned char s = 0; int k = 0;
   do s = ( ((s&1)<<7) | ((s>>1)&0x7F) ) + nm[k]; while(k < 11);
}
*)

tDirSector = Array[0..15] of tDirRecord;

{
Function DeSpecial(const a: String): String;
Var n: Cardinal;
Begin
  DeSpecial := a;
  For n := 1 to Length(DeSpecial) do If (pos(DeSpecial[n], InvalidChars) <> 0) or (DeSpecial[n] < #32) then DeSpecial[n] := '_';
End;

Function GetCluster(Const dr: tDirRecord): Cardinal;
Begin
  GetCluster:=dr.cH;
  GetCluster:=GetCluster shl 16;
  GetCluster:=GetCluster or dr.cL;
End;
}
