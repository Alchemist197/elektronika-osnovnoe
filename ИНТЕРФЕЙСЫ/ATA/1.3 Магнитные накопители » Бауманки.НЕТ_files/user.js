var last_word = '';
var r_key_search = 0;

$(function() {

    if(dle_skin != 'smartphone' && dle_skin != 'admin')
        {
        setTimeout(function() {
            //������� ��� alt � title �� ������ ������� tooltip
            $('[title], [alt]').each(function(indx, element){
                var el = $(element);

                var alt = el.attr('alt');
                var title = el.attr('title');

                if (void 0 === el.attr('title'))
                    {
                    if(alt != '')
                        el.removeAttr('alt').attr('tooltip', alt);
                    }
                else if (void 0 === el.attr('alt'))
                    {
                    if(title != '')
                        el.removeAttr('title').attr('tooltip', title);
                    }
                else
                    {
                    if(title != '')
                        el.removeAttr('title').removeAttr('alt').attr('tooltip', title);
                    else if(alt != '')
                        el.removeAttr('title').removeAttr('alt').attr('tooltip', alt);
                    }
                });


            $("body").prepend('<div id="tooltip-div" class="tooltip-div" style="display:none;"></div>');
            $('[tooltip]').mousemove(function(e){
        		var tooltip = $(this).attr('tooltip');
        		$('#tooltip-div').html(tooltip).css({'left': String(e.clientX + 10)+'px', 'top': String(parseInt(e.clientY) + 10 + parseInt($(window).scrollTop()))+'px'}).show();;

        	}).mouseout(function(){
        		$('#tooltip-div').hide();
        	});
            }, 100);
        }





    $('#search_words').keyup(function(eventObject){
        fast_search($('#search_words').val().trim());
        });

        if($("div").is(".definition-relink-block"))
            {
            page_size = getPageSize();
            var def_dialog_w = parseInt(page_size[2]);
            if(def_dialog_w > 600)
                def_dialog_w = 600;

            $('.definition-relink-block').dialog({
                autoOpen: false,
                width: def_dialog_w,

                modal: true,
                resizable: false,
                buttons: {
                    "���������": function() {
                        location = $(this).find('.definition-dialog-link').val();
                    },
                    "�������": function() {
                        $( this ).dialog( "close" );
                    }}
                });
            }

        if($("div").is(".short-document-block"))
            {
            page_size = getPageSize();
            var def_dialog_w = parseInt(page_size[2]);
            if(def_dialog_w > 600)
                def_dialog_w = 600;

            $('.short-document-block').dialog({
                autoOpen: false,
                width: def_dialog_w,

                modal: true,
                resizable: false,
                open: function( event, ui ) {
                    if($(this).find('.short-document-dialog-form').is('form'))
                        {
                        $(this).dialog( "option", "buttons",
                            [{
                                text: "���������",
                                click: function() {
                                    $(this).find('.short-document-dialog-form').submit();
                                    $( this ).dialog( "close" );
                                    }
                            },
                            {
                                text: "�������",
                                click: function() {
                                    $( this ).dialog( "close" );
                                    }
                            }]);
                        }
                    else
                        {
                        $(this).dialog( "option", "buttons",
                            [{
                                text: "�������",
                                click: function() {
                                    $( this ).dialog( "close" );
                                    }
                            }]);
                        }

                },
                buttons: {
                    "���������": function() {
                        $(this).find('.short-document-dialog-form').submit();
                        $( this ).dialog( "close" );
                    },
                    "�������": function() {
                        $( this ).dialog( "close" );
                    }}
                });
            }
    });

function show_definition_relink(id)
    {
    $('#definition-dialog-'+id).dialog("open");
    };

function show_short_document(id)
    {
    $('#short-document-dialog-'+id).dialog("open");
    };

function add_high_school()
    {
    var name = $('#hs_name').val().trim();
    var abb = $('#hs_abbreviation').val().trim();
    var add_sem = 0;

    if($("#create_12s").prop("checked"))
        add_sem = 1;

    if(abb != '' && name != '')
        {
        $('#add_new_high_school').dialog("close");
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {name:name, abb:abb, add_sem:add_sem, action:'add_high_school'}, function( data ){
            HideLoading("");

            var data_t = parseInt(data);

            if($("#open_high_school_page").prop("checked") && data_t != 'NaN')
                {
                location = '/filearray/high-school-'+data_t+'.html';
                }
            else if($("#open_high_school_page").prop("checked") && data_t == 'NaN')
                DLEalert(data, '���������� ����');
            else if(!$("#open_high_school_page").prop("checked") && data_t != 'NaN')
                DLEalert('��� '+abb+' ������� ��������', '���������� ����');
            else
                DLEalert(data, '���������� ����');

            $('#hs_name').val('');
            $('#hs_abbreviation').val('');
            $("#create_12s").attr('checked', 'checked');
            //$("#open_high_school_page").attr('checked', 'checked');
            $('#info_add_hs').css('display', 'none');
            });
        }
    else
        {
        if(abb == '')
            $('#info_add_hs td').html('<b>���������� ������� ������������ ����</b>');
        else
            $('#info_add_hs td').html('<b>���������� ������� ������������ ����</b>');
        $('#info_add_hs').css('display', 'table-row');
        }

    };

function show_filearray_page(page)
    {
    var text = $('#search_text').val();
    ShowLoading("");

    $.post("/engine/modules/filearray/ajax/user_ajax.php", {text:text, page:page, action:'show_filearray_page'}, function( data ){
        HideLoading("");
        $('#search_result').html(data);
        $('#page_id').val(page);
        });
    };

function show_high_schools_page(page)
    {
    var text = $('#search_text').val();
    ShowLoading("");

    $.post("/engine/modules/filearray/ajax/user_ajax.php", {text:text, page:page, action:'show_high_schools_page'}, function( data ){
        HideLoading("");
        $('#search_result').html(data);
        $('#page_id').val(page);
        });
    };


function show_album_of_hs_page(page, hs_id)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {hs_id:hs_id, page:page, action:'show_album_of_hs_page'}, function( data ){
        HideLoading("");
        $('#albums_list_block').html(data);
        });
    };

function show_news_of_hs_page(page, cat_id, text)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {cat_id:cat_id, page:page, text:text, action:'show_news_of_hs_page'}, function( data ){
        HideLoading("");
        $('#news_of_hs_block').html(data);
        });
    };

function show_events_of_hs_page(page, id, text, date_start, date_finish)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, page:page, text:text, date_start:date_start, date_finish:date_finish, action:'show_events_of_hs_page'}, function( data ){
        HideLoading("");
        $('#events_of_hs_block').html(data);
        });
    };

function other_color_table(id)
    {
    $('#'+id+' tbody td').css('background-color', 'transparent');
    $('#'+id+' tr:odd td').css('background-color', '#FCFCD9');
    };

function fast_search_df(text, min)
    {
    var min_len = parseInt();
    text = text.trim();
    var t_l = parseInt(text.length);

    if(last_word != text)
        {
        if(t_l >= min || t_l == 0)
            {
            var dt = $('#edit_type').val();
            var action = 'show_filearray_page';

            if(dt == 'dossier')
                action = 'show_dossier_page';

            if(dt == 'high_school')
                action = 'show_high_schools_page';

            ShowLoading("");
            $.post("/engine/modules/filearray/ajax/user_ajax.php", {text:text, page:'1', action:action, type:dt}, function( data ){
                HideLoading("");
                $('#search_result').html(data);
                select_text_in_table('search_result', text);
                last_word = text;
                });
            }
        }
    };

function fast_search_df_mp(text, min)
    {
    var min_len = parseInt();
    text = text.trim();
    var t_l = parseInt(text.length);

    if(last_word != text)
        {
        if(t_l >= min || t_l == 0)
            {
            var dt = $('#edit_type').val();
            var action = 'show_filearray_page';

            if(dt == 'dossier')
                action = 'show_dossier_page';

            if(dt == 'high_school')
                action = 'show_high_schools_page';

            ShowLoading("");
            $.post("/engine/modules/filearray/ajax/user_ajax.php", {text:text, page:'1', action:action, aviable:'main', type:dt}, function( data ){
                HideLoading("");
                $('#search_result').html(data);
                //select_text_in_main_slider(text);
                select_text_in_table('search_result', text);
                last_word = text;
                });
            }
        }
    };

function delete_high_school_fa(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ ���? ��� ����� � ����� ����� �������.','�������� ����',function(){
        ShowLoading("");
        var dt = $('#edit_type').val();
        var page = $('#page_id').val();
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, page:page, type:dt, text:$('#search_text').val(), action:'delete_high_school_fa'}, function( data ){
            HideLoading("");
            $('#search_result').html(data);
            });
        });
    };

function edit_high_school_fa(id, abb, name)
    {
    $('#edit_hs_id').val(id);
    $('#hs_name_edit').val(name);
    $('#hs_abbreviation_edit').val(abb);
    $('#edit_high_school').dialog('open');
    };

function save_hs_edit()
    {
    var name = $('#hs_name_edit').val().trim();
    var abb = $('#hs_abbreviation_edit').val().trim();
    var id = $('#edit_hs_id').val();

    if(abb != '' && name != '')
        {
        $('#edit_high_school').dialog( "close" );
        var dt = $('#edit_type').val();
        var page = $('#page_id').val();

        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {type:dt, page:page, id:id, text:$('#search_text').val(), name:name, abb:abb, action:'save_hs_edit'}, function( data ){
            HideLoading("");
            $('#search_result').html(data);
            DLEalert('��������� ������� ���������', '�������������� ����');
            });

        $('#info_add_hs_edit').css('display', 'none');
        }
    else
        {
        if(abb == '')
            $('#info_add_hs_edit td').html('<b>���������� ������� ������������ ����</b>');
        else
            $('#info_add_hs_edit td').html('<b>���������� ������� ������������ ����</b>');
        $('#info_add_hs_edit').css('display', 'table-row');
        }
    };

function show_add_semester()
    {
    $('#type_semester').val('add');
    $('#add_new_semester').dialog('open');
    };

function add_or_edit_semestr(hs_id)
    {
    var name = $('#semester_name').val().trim();
    var type = $('#type_semester').val();

    var id = '0';
    if(type == 'edit')
        id = $('#semester_id').val();

    if(name != '')
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, hs_id:hs_id, name:name, type:type, action:'add_or_edit_semestr'}, function( data ){
            HideLoading("");
            $('#info_add_sem').css('display', 'none');
            if(data == 'ok')
                location = '/filearray/high-school-'+hs_id+'.html';
            else
                DLEalert(data, '����������/�������������� ��������');
            });
        }
    else
        {
        $('#info_add_sem td').html('<b>���������� ������� ������������ ��������</b>');
        $('#info_add_sem').css('display', 'table-row');
        }
    };

function edit_semester(id, name)
    {
    $('#semester_name').val(name);
    $('#type_semester').val('edit');
    $('#semester_id').val(id);
    $('#add_new_semester').dialog('open');
    };

function delete_semester(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ �������? ��� ����� �������� ����� �������.','�������� ��������',function(){
        var hs_id = $('#hs_id').val();
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_semester'}, function( data ){
            HideLoading("");
            if(data == 'ok')
                location = '/filearray/high-school-'+hs_id+'.html';
            else
                DLEalert(data, '�������� ��������');
            });
        });
    };

function show_add_subject_form()
    {
    $('#type_subject').val('add');
    $('.add_new_subj').css('display', 'table-row');
    $('#add_new_subject').dialog('open');
    };

function add_or_edit_subject(hs_id, sem_id)
    {
    var name = $('#subject_name').val().trim();
    var type = $('#type_subject').val();
    var desc = $('#subject_desc').val();
    var auto_cat = $('#auto_create_cat').val();

    var id = '0';
    if(type == 'edit')
        id = $('#subject_id').val();

    if(name != '')
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, hs_id:hs_id, sem_id:sem_id, name:name, text:desc, type:type, auto_cat:auto_cat, action:'add_or_edit_subject'}, function( data ){
            HideLoading("");
            $('#info_add_sub').css('display', 'none');
            if(data == 'ok')
                location = '/filearray/high-school/semester-'+sem_id+'.html';
            else
                DLEalert(data, '����������/�������������� ��������');
            });
        }
    else
        {
        $('#info_add_sub td').html('<b>���������� ������� ������������ ��������</b>');
        $('#info_add_sub').css('display', 'table-row');
        }
    };

function edit_subject(id, name, desc)
    {
    $('#subject_name').val(name);
    $('#subject_desc').val(desc);
    $('#type_subject').val('edit');
    $('#subject_id').val(id);
    $('.add_new_subj').css('display', 'none');
    $('#add_new_subject').dialog('open');
    };

function delete_subject(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ �������? ��� ����� �������� ����� �������.','�������� ��������',function(){
        var semester_id = $('#semester_id').val();
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_subject'}, function( data ){
            HideLoading("");
            if(data == 'ok')
                location = '/filearray/high-school/semester-'+semester_id+'.html';
            else
                DLEalert(data, '�������� ��������');
            });
        });
    };

function show_add_category_form()
    {
    $('#type_cat').val('add');
    $('#add_new_cat').dialog('open');
    };

function add_or_edit_cat(hs_id, sem_id, subj_id)
    {
    var name = $('#cat_name').val().trim();
    var type = $('#type_cat').val();
    var desc = $('#cat_desc').val();

    var id = '0';
    if(type == 'edit')
        id = $('#cat_id').val();

    if(name != '')
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, hs_id:hs_id, sem_id:sem_id, subj_id:subj_id, name:name, text:desc, type:type, action:'add_or_edit_cat'}, function( data ){
            HideLoading("");
            $('#info_add_cat').css('display', 'none');
            if(data == 'ok')
                location = '/filearray/high-school/subject-'+subj_id+'.html';
            else
                DLEalert(data, '����������/�������������� ���������');
            });
        }
    else
        {
        $('#info_add_cat td').html('<b>���������� ������� ������������ ���������</b>');
        $('#info_add_cat').css('display', 'table-row');
        }
    };

function edit_cat(id, name, desc)
    {
    $('#cat_name').val(name);
    $('#cat_desc').val(desc);
    $('#type_cat').val('edit');
    $('#cat_id').val(id);
    $('#add_new_cat').dialog('open');
    };

function delete_cat(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ ���������? ��� ����� ��������� ����� �������.','�������� ���������',function(){
        var subject_id = $('#subject_id').val();
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_cat'}, function( data ){
            HideLoading("");
            if(data == 'ok')
                location = '/filearray/high-school/subject-'+subject_id+'.html';
            else
                DLEalert(data, '�������� ���������');
            });
        });
    };

function is_checked(type, id)
    {
    var counter = parseInt($('#'+type+'_counter').html());
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, type:type, action:'is_checked'}, function( data ){
        HideLoading("");
        if(data == 'ok')
            {
            $('.r_'+type+id).remove();
            $('#'+type+'_counter').html(counter-1);
            }
        else
            DLEalert(data, '����������');
        });
    };

function show_edit_form_subject(id)
    {
    $('#row_subj'+id).css('display', 'none');
    $('#edit_subj'+id).css('display', 'table-row');
    };

function stop_edit_subj(id)
    {
    $('#edit_subj'+id).css('display', 'none');
    $('#row_subj'+id).css('display', 'table-row');
    };

function save_edit_row(type, id)
    {
    var name = $('#'+type+'_name'+id).val().trim();
    var desc = $('#'+type+'_desc'+id).val();
    var counter = parseInt($('#'+type+'_counter').html());

    if(name != '')
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, type:type, name:name, text:desc, action:'save_edit_row'}, function( data ){
            HideLoading("");
            if(data == 'ok')
                {
                $('.r_'+type+id).remove();
                $('#'+type+'_counter').html(counter-1);
                }
            else
                DLEalert(data, '����������');
            });
        }
    else
        DLEalert('������������ ������ ���� �������� �����������!', '����������');
    };

function delete_subject_row_ap(id, type)
    {
    var counter = parseInt($('#'+type+'_counter').html());

    DLEconfirm('�� ������������� ������ ������� ������ �������? ��� ����� �������� ����� �������.','�������� ��������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_subject'}, function( data ){
            HideLoading("");
            if(data == 'ok')
                {
                $('.r_'+type+id).remove();
                $('#'+type+'_counter').html(counter-1);
                }
            else
                DLEalert(data, '�������� ��������');
            });
        });
    };

function check_all_cb()
    {
    var all_cb = '';

    if($("#check_all").prop("checked"))
        {
        $(".auto_create_cat_cb").prop("checked", true);
        $(".auto_create_cat_cb").each(function(indx, element){
            if(all_cb == '')
                all_cb = $(element).attr('text');
            else
                all_cb = all_cb+'|||'+$(element).attr('text');
            });
        $('#auto_create_cat').val(all_cb);
        }
    else
        {
        $(".auto_create_cat_cb").prop("checked", false);
        $('#auto_create_cat').val('');
        }
    };

function create_str_cb()
    {
    var all_cb = '';

    $(".auto_create_cat_cb:checked").each(function(indx, element){
            if(all_cb == '')
                all_cb = $(element).attr('text');
            else
                all_cb = all_cb+'|||'+$(element).attr('text');
            });
    $('#auto_create_cat').val(all_cb);
    $("#check_all").prop("checked", false);
    };

function show_edit_form_category(id)
    {
    $('#row_category'+id).css('display', 'none');
    $('#edit_category'+id).css('display', 'table-row');
    };

function stop_edit_category(id)
    {
    $('#edit_category'+id).css('display', 'none');
    $('#row_category'+id).css('display', 'table-row');
    };

function delete_cat_row_ap(id)
    {
    var counter = parseInt($('#category_counter').html());

    DLEconfirm('�� ������������� ������ ������� ������ ���������? ��� ����� ��������� ����� �������.','�������� ���������',function(){

        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_cat'}, function( data ){
            HideLoading("");
            if(data == 'ok')
                {
                $('.r_category'+id).remove();
                $('#category_counter').html(counter-1);
                }
            else
                DLEalert(data, '�������� ���������');
            });
        });
    };

function delete_temp_file(id)
    {
    name = $('#name_on_server_'+id).val();
    DLEconfirm('�� ������������� ������ ������� ������ ����', '�������� �����',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {name:name, action:'delete_temp_file'}, function( data ){
            HideLoading("");
            $('#div_'+id).remove();
            check_f_n_onclick();
            });
        });
    };

function check_all_file_name()
    {
    var x = true;
    var i = 0;
    $('#names_and_descr .show_file_name').each(function(indx, element){
        i = i+1;
        if($(element).val().trim() == '')
            x = false;
        });

    if(i == 0)
        x = false;
    return x;
    };

function check_f_n_onclick()
    {
    if(check_all_file_name() && $('#upload_file').val() == 1)
        {
        $('#send_button').css('display', 'block');
        $('#hide_send_button').css('display', 'none');
        }
    else
        {
        $('#send_button').css('display', 'none');
        $('#hide_send_button').css('display', 'block');
        }
    };

function save_upload_file()
    {
    var name_arr = $("#names_and_descr .show_file_name").map(function(indx, element){return $(element).val();}).get();
    var descr_arr = $("#names_and_descr .show_file_descr").map(function(indx, element){return $(element).val();}).get();
    var server_name_arr = $("#names_and_descr .hide_name_on_server").map(function(indx, element){return $(element).val();}).get();
    var cat_id = $("#file_category").val();

    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {cat_id:cat_id, name_arr:name_arr, descr_arr:descr_arr, server_name_arr:server_name_arr, action:'save_upload_file'}, function( data ){
        HideLoading("");
        alert(data);
        $('#names_and_descr').html('');
        $('#send_button').hide();
        $('#hide_send_button').hide();
        $('#file_upload_box').hide();

        location = '/filearray/high-school/category-'+cat_id+'.html';
        });
    };

function show_file_page(id, cat_id, text)
    {
    var hs_id = $('#hs_id').val();
    var sem_id = $('#semestr_id').val();

    text = text.trim();
    var tl = parseInt(text.length);

    if(tl == 0 || tl > 1)
        {
        $('#page').val(id);
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, hs_id:hs_id, sem_id:sem_id, cat_id:cat_id, text:text, action:'show_file_page'}, function( data ){
            HideLoading("");
            $('#table_with_file_to_download').html(data);
            });
        }
    };

function edit_file_info(id, name)
    {
    $('#file_id').val(id);
    $('#file_name').val(name);
    $('#file_desc').val($('#file_descr_'+id).html());

    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'edit_cat_block'}, function( data ){
        HideLoading("");
        $('#edit_cat_block').html(data);
        });

    $('#file_edit_form').dialog('open');
    };

function save_file_info()
    {
    var id = $('#file_id').val();
    var name = $('#file_name').val();
    var desc = $('#file_desc').val();
    var new_cat_id = $('#file_cat_id').val();

    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, name:name, page:$('#page').val(), cat_id:$('#cat_id').val(), new_cat_id:new_cat_id, text:desc, action:'save_file_info'}, function( data ){
        HideLoading("");
        $('#table_with_file_to_download').html(data);
        $('#file_edit_form').dialog("close");
        });
    };

function delete_file(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ ����?','�������� �����',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, page:$('#page').val(), cat_id:$('#cat_id').val(), action:'delete_file'}, function( data ){
            HideLoading("");
            $('#table_with_file_to_download').html(data);
            });
        });
    };

function show_edit_form_file(id)
    {
    $('#row_file'+id).css('display', 'none');
    $('#edit_file'+id).css('display', 'table-row');
    };

function stop_edit_file(id)
    {
    $('#edit_file'+id).css('display', 'none');
    $('#row_file'+id).css('display', 'table-row');
    };

function delete_file_row_ap(id)
    {
    var counter = parseInt($('#file_counter').html());

    DLEconfirm('�� ������������� ������ ������� ������ ����?','�������� �����',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_file_row_ap'}, function( data ){
            HideLoading("");
            $('.r_file'+id).remove();
            $('#file_counter').html(counter-1);
            });
        });
    };

function comm_reply(name)
    {
    $('#comments').val($('#comments').val()+'[b]'+name+'[/b], ');
    };

function delete_comment(id, type)
    {
    DLEconfirm('�� ������������� ������ ������� ������ �����������?', '�������� �����������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, type:type, action:'delete_comment'}, function( data ){
            HideLoading("");
            $("#comm"+id).remove();
            });
        });
    };

function file_vote(id, type)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, type:type, action:'file_vote'}, function( data ){
        HideLoading("");
        if(data == 'ok')
            {
            var r = parseInt($('#rate').html());

            if(type == 'up')
                r = r + 1;
            else
                r = r -1;

            if(r > 0)
                $('#rate').css('color', 'green');
            else if(r < 0)
                $('#rate').css('color', 'red');
            else
                $('#rate').css('color', 'black');

            $('#rate').html(r);
            $('#vote_num').html(parseInt($('#vote_num').html())+1);
            DLEalert('�������, ��� ����� �����.', '����������� �������� �����');
            }
        else if(data == 'you_file')
            DLEalert('�� �� ������ ���������� �� ���� ���������� ����', '����������� �������� �����');
        else if(data == 'vote_exists')
            DLEalert('�� ��� ���������� �� ������ ����', '����������� �������� �����');
        else if(data == 'auth')
            DLEalert('��� ���� ����� ������������� �������������', '����������� �������� �����');
        else
            DLEalert(data, '����������� �������� �����');
        });
    };

function send_complaint(id)
    {
    var type = $("#type_compl").val();
    var text = $('#descr_compl').val();

    if(type != '0')
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, type:type, text:text, action:'send_complaint'}, function( data ){
            HideLoading("");
            $('#complaint_form').dialog( "close" );
            DLEalert('���� ������ ������� ����������.', '������ �� ����');
            });
        }
    else
        DLEalert('�� �� ������� ��� ������.', '������ �� ����');
    };

function is_checked_compl(id)
    {
    var counter = parseInt($('#compl_counter').html());

    DLEconfirm('�� ������������� ������ �������� ������ ������ ��� ����������.', '�������� ������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'is_checked_compl'}, function( data ){
            HideLoading("");
            $(".r_compl"+id).remove();
            $('#compl_counter').html(counter-1);
            });
        });
    };

function edit_file_info_from_compl_ap(id)
    {
    $('#row_compl'+id).css('display', 'none');
    $('#edit_compl'+id).css('display', 'table-row');
    };

function stop_edit_file_info_from_compl_ap(id)
    {
    $('#edit_compl'+id).css('display', 'none');
    $('#row_compl'+id).css('display', 'table-row');
    };

function save_edit_compl_row(id, compl_id)
    {
    var counter = parseInt($('#compl_counter').html());
    var name = $('#file_compl_name'+compl_id).val();
    var text = $('#file_compl_desc'+compl_id).val();

    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, compl_id:compl_id, name:name, text:text, action:'save_edit_compl_row'}, function( data ){
        HideLoading("");
        $(".r_compl"+compl_id).remove();
        $('#compl_counter').html(counter-1);
        });
    };

function delete_file_row_compl_ap(id)
    {
    var counter = parseInt($('#compl_counter').html());

    DLEconfirm('�� ������������� ������ ������� ���� ��������� � ���� �������?', '�������� �����',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_file_row_compl_ap'}, function( data ){
            HideLoading("");
            $(".r_compl"+id).remove();
            $('#compl_counter').html(counter-1);
            });
        });
    };

function delete_file_comment(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ �����������?', '�������� �����������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_file_comment'}, function( data ){
            HideLoading("");
            $('#file_comm'+id).remove();
            });
        });
    };

function comm_vote(id, type, type_comm)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, type:type, action:'comm_vote', type_comm:type_comm}, function( data ){
        HideLoading("");
        if(data == 'ok')
            {
            $('#rate_comm'+id+' .masha_index').remove();
            var r = parseInt($('#rate_comm'+id).html());

            if(type == 'up')
                r = r + 1;
            else
                r = r - 1;

            if(r > 0)
                $('#rate_comm'+id).css('color', 'green');
            else if(r < 0)
                $('#rate_comm'+id).css('color', 'red');
            else
                $('#rate_comm'+id).css('color', 'black');

            if(r > 0)
                $('#rate_comm'+id).html('+'+r);
            else
                $('#rate_comm'+id).html(r);
            DLEalert('�������, ��� ����� �����.', '����������� �������� �����������');
            }
        else if(data == 'vote_exists')
            DLEalert('�� ��� ���������� �� ������ �����������', '����������� �������� �����������');
        else if(data == 'is_my_comm')
            DLEalert('�� �� ������ ���������� ������� ����� �� ������������', '����������� �������� �����������');
        else if(data == 'auth')
            DLEalert('��� ���� ����� ������������� �������������', '����������� �������� �����������');
        else
            DLEalert(data, '����������� �������� �����������');
        });
    };

function add_banner()
    {
    var type = parseInt($('#banner_type').val());
    var code_type = parseInt($('#banner_add_type').val());

    var text = $('#banner_text').val().trim();

    var max_len = parseInt($('#max_lenght_banner_text').val());
    var text_len = parseInt(text.length);
    var form_type = $('#type_form').val();
    var id = 0;

    var link = $('#banner_link').val();
    var b_img = $('#b_is_load_img').val();

    if(form_type == 'edit')
        id = $('#banner_id').val();

    if(code_type == 0)
        {
        if(text_len <= max_len)
            {
            if(type > 0)
                {
                if(text == '')
                    {
                    $('#info_add_banner td').html('�� �� ��������� ��� ������������ ����');
                    $('#info_add_banner').css('display', 'table-row');
                    }
                else
                    {
                    $('#info_add_banner').css('display', 'none');
                    ShowLoading("");
                    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, type:type, text_banner:text, form_type:form_type, code_type:code_type, action:'add_banner'}, function( data ){
                        HideLoading("");
                        $('#add_banner_form').dialog('close');
                        if(data == 'ok')
                            location = '/filearray/banners.html';
                        else if(data == 'iframe_exists')
                            DLEalert('��������� ������������ iframe', '���������� �������');
                        else if(data == 'max_lenght')
                            DLEalert('��������� ������������ ���������� �������� � ��������� �����', '���������� �������');
                        else
                            DLEalert(data, '���������� �������');
                        });
                    }
                }
            else
                {
                $('#info_add_banner td').html('�� �� ������� ��� �������');
                $('#info_add_banner').css('display', 'table-row');
                }
            }
        else
            {
            $('#info_add_banner td').html('����� ������� ��������� ���������� ���������� ���������� �������� � ��������� �����');
            $('#info_add_banner').css('display', 'table-row');
            }
        }
    else
        {
        if(type > 0)
            {
            if((type == 4 && parseInt(link.length) > 7) || (type < 4 && parseInt(link.length) > 7 && b_img != ''))
                {
                $('#info_add_banner').css('display', 'none');
                ShowLoading("");
                $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, type:type, form_type:form_type, code_type:code_type, link:link, b_img:b_img, action:'add_banner'}, function( data ){
                    HideLoading("");
                    $('#add_banner_form').dialog('close');
                    if(data == 'ok')
                        location = '/filearray/banners.html';
                    else if(data == 'iframe_exists')
                        DLEalert('��������� ������������ iframe', '���������� �������');
                    else
                        DLEalert(data, '���������� �������');
                    });
                }
            else
                {
                $('#info_add_banner td').html('�� �� ��������� ��� ������������ ����');
                $('#info_add_banner').css('display', 'table-row');
                }
            }
        else
            {
            $('#info_add_banner td').html('�� �� ������� ��� �������');
            $('#info_add_banner').css('display', 'table-row');
            }
        }
    };

function show_add_banner_form()
    {
    $('#banner_img_prev').html('');
    $('#b_is_load_img').val('');
    $('#type_form').val('add');
    $('#info_add_banner').css('display', 'none');
    $('#add_banner_form').dialog('open');
    $('#banner_text').val('');
    $('#banner_type').val('0');
    $('#banner_add_type').val(0).change();
    show_hint();
    };

function edit_banner(id, type)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'edit_banner'}, function( data ){
        HideLoading("");
        $('.hint_bt').css('display', 'none');
        $('#banner_text').val(data);
        $('#type_form').val('edit');
        $('#info_add_banner').css('display', 'none');
        $('#banner_type').val(type);
        $('#banner_id').val(id);
        show_hint();
        $('#add_banner_form').dialog('open');
        });
    };

function delete_banner(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ ������?', '�������� �������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_banner'}, function( data ){
            HideLoading("");
            $('#banner_row'+id).remove();
            });
        });
    };

function delete_banner_ap(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ ������?', '�������� �������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_banner'}, function( data ){
            HideLoading("");
            $('#banner_counter').html(parseInt($('#banner_counter').html())-1);
            $('.banner_row'+id).remove();
            });
        });
    };

function save_banner_ap(id)
    {
    var text = $('#banner_text'+id).val();
    var type = $('#banner_type'+id).val();

    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, type:type, text_banner:text, form_type:'edit_ap', action:'add_banner'}, function( data ){
        HideLoading("");
        if(data == 'ok')
            {
            $('#banner_counter').html(parseInt($('#banner_counter').html())-1);
            $('.banner_row'+id).remove();
            }
        else if(data == 'iframe_exists')
            DLEalert('��������� ������������ iframe', '���������� �������');
        else if(data == 'max_lenght')
            DLEalert('��������� ������������ ���������� �������� � ��������� �����', '���������� �������');
        else
            DLEalert(data, '���������� �������');
        });
    };

function show_hint()
    {
    var id = $('#banner_type').val();

    $('.hint_bt').css('display', 'none');
    $('#type_'+id).css('display', 'table-row');
    };

function show_dossier_page(page)
    {
    var text = $('#search_text').val();
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {text:text, page:page, action:'show_dossier_page'}, function( data ){
        HideLoading("");
        $('#search_result').html(data);
        $('#page_id').val(page);
        });
    };

function show_add_cat_dosier_form()
    {
    $('#add_new_cat_dosier').dialog('open');
    $('#type_cat').val('add');
    $('#info_add_cat').css('display', 'none');
    $('#cat_name').val('');
    $('#cat_desc').val('');
    };

function add_or_edit_cat_dosier(hs_id)
    {
    var name = $('#cat_name').val().trim();
    var text = $('#cat_desc').val().trim();
    var type = $('#type_cat').val();

    var id = '0';
    if(type == 'edit')
        id = $('#cat_id').val();

    if(name != '')
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, hs_id:hs_id, text:text, name:name, type:type, action:'add_or_edit_cat_dosier'}, function( data ){
            HideLoading("");
            $('#info_add_cat').css('display', 'none');
            if(data == 'ok')
                location = '/dossier/high-school-'+hs_id+'.html';
            else
                DLEalert(data, '����������/�������������� ���������');
            });
        }
    else
        {
        $('#info_add_cat td').html('<b>���������� ������� ������������ ���������</b>');
        $('#info_add_cat').css('display', 'table-row');
        }
    };


function edit_cat_dossier(id, name, desc)
    {
    $('#cat_id').val(id);
    $('#cat_name').val(name);
    $('#cat_desc').val(desc);
    $('#type_cat').val('edit');
    $('#add_new_cat_dosier').dialog('open');
    };

function delete_cat_dossier(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ ���������? ��� ����� �� �������������� ����� �������.','�������� ���������',function(){
    var hs_id = $('#hs_id').val();
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_cat_dossier'}, function( data ){
            HideLoading("");
            if(data == 'ok')
                location = '/dossier/high-school-'+hs_id+'.html';
            else
                DLEalert(data, '�������� ��������');
            });
        });
    };

function show_edit_form_dossier_cat(id)
    {
    $('#row_dossier_cat'+id).css('display', 'none');
    $('#edit_dossier_cat'+id).css('display', 'table-row');
    };
function stop_edit_dossier_cat(id)
    {
    $('#edit_dossier_cat'+id).css('display', 'none');
    $('#row_dossier_cat'+id).css('display', 'table-row');
    };

function delete_dossier_cat_row_ap(id)
    {
    var counter = parseInt($('#dossier_cat_counter').html());

    DLEconfirm('�� ������������� ������ ������� ������ ��������� �����? ��� ����� �� �������������� ����� �������.','�������� ���������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_cat_dossier'}, function( data ){
            HideLoading("");
            if(data == 'ok')
                {
                $('.r_dossier_cat'+id).remove();
                $('#dossier_cat_counter').html(counter-1);
                }
            else
                DLEalert(data, '�������� ���������');
            });
        });
    };

function add_new_dossier_form()
    {
    $('#add_new_dosier').dialog('open');
    $('#type_dossier').val('add');
    $('#info_add_dossier').css('display', 'none');
    $('#surname').val('');
    $('#name').val('');
    };

function add_or_edit_dosier(hs_id, cat_id)
    {
    var cat_id = $('#cat_id').val();
    var surname = $('#surname').val().trim();
    var name = $('#name').val().trim();
    var type = $('#type_dossier').val();
    var text = $('#dossier_descr').val().trim();

    var id = '0';
    if(type == 'edit')
        id = $('#dossier_id').val();

    if(name != '' && surname != '')
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, hs_id:hs_id, cat_id:cat_id, surname:surname, name:name, type:type, text:text, action:'add_or_edit_dosier'}, function( data ){
            HideLoading("");
            $('#info_add_dossier').css('display', 'none');
            if(data == 'ok')
                location = '/dossier/high-school/category-'+cat_id+'.html';
            else
                DLEalert(data, '����������/�������������� �����');
            });
        }
    else
        {
        $('#info_add_dossier td').html('<b>���������� ��������� ��� ���� ���</b>');
        $('#info_add_dossier').css('display', 'table-row');
        }
    };

function edit_dossier(id, surname, name, fathername, cat_id)
    {
    var descr = $('#descr_edit_'+id).html();

    $('#dossier_id').val(id);
    $('#surname').val(surname);
    $('#name').val(name);
    $('#dossier_descr').val(descr);
    $('#type_dossier').val('edit');
    $('#add_new_dosier').dialog('open');
    $('#cat_id').val(cat_id);

    };

function delete_dossier(id)
    {
    DLEconfirm('�� ������������� ������ ������� ����� �� �������������?','�������� ����� �� �������������',function(){
    var cat_id = $('#cat_id').val();
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_dossier'}, function( data ){
            HideLoading("");
            if(data == 'ok')
                location = '/dossier/high-school/category-'+cat_id+'.html';
            else
                DLEalert(data, '�������� ����� �� �������������');
            });
        });
    };

function show_edit_form_dossier(id)
    {
    $('#row_dossier'+id).css('display', 'none');
    $('#edit_dossier'+id).css('display', 'table-row');
    };
function stop_edit_dossier(id)
    {
    $('#edit_dossier'+id).css('display', 'none');
    $('#row_dossier'+id).css('display', 'table-row');
    };

function save_edit_row_dossier(id)
    {
    var surname = $('#dossier_surname'+id).val().trim();
    var name = $('#dossier_name'+id).val().trim();

    var counter = parseInt($('#dossier_counter').html());

    if(name != '' && surname != '')
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, surname:surname, name:name, action:'save_edit_row_dossier'}, function( data ){
            HideLoading("");
            if(data == 'ok')
                {
                $('.r_dossier'+id).remove();
                $('#dossier_counter').html(counter-1);
                }
            else
                DLEalert(data, '����������');
            });
        }
    else
        DLEalert('��� ������ ���� �������� �����������!', '����������');
    };

function delete_dossier_row_ap(id)
    {
    var counter = parseInt($('#dossier_counter').html());

    DLEconfirm('�� ������������� ������ ������� ����� �� �������������?','�������� �����',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_dossier'}, function( data ){
            HideLoading("");
            if(data == 'ok')
                {
                $('.r_dossier'+id).remove();
                $('#dossier_counter').html(counter-1);
                }
            else
                DLEalert(data, '�������� �����');
            });
        });
    };

function show_add_photo_form()
    {
    $('#add_new_photo').dialog('open');
    };

function show_all_photo_form(id, page)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, page:page, action:'show_all_photo_form'}, function( data ){
        HideLoading("");
        $('#all_photo').html(data);
        });
    };

function photo_vote(id, type)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, type:type, action:'photo_vote'}, function( data ){
        HideLoading("");
        if(data == 'ok')
            {
            var r = parseInt($('#photo-rate'+id).html());

            if(type == 'up')
                r = r + 1;
            else
                r = r -1;

            if(r > 0)
                $('#photo-rate'+id).css('color', 'green');
            else if(r < 0)
                $('#photo-rate'+id).css('color', 'red');
            else
                $('#photo-rate'+id).css('color', 'black');

            $('#photo-rate'+id).html(r);

            DLEalert('�������, ��� ����� �����.', '����������� �������� ����');
            }
        else if(data == 'you_photo')
            DLEalert('�� �� ������ ���������� ������� ����������� ������� ��������� ����.', '����������� �������� ����');
        else if(data == 'vote_exists')
            DLEalert('�� ��� ���������� �� ������ ����������', '����������� �������� ����');
        else if(data == 'auth')
            DLEalert('����� ������������� �������������', '����������� �������� ����');
        else
            DLEalert(data, '����������� �������� ����');
        });
    };

function photo_complaint(id, type)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, type:type, action:'photo_complaint'}, function( data ){
        HideLoading("");

        if(type == 'no')
            {
            $('#send_complaint'+id).css('display', 'inline');
            $('#no_complaint'+id).css('display', 'none');
            $('.compl_count'+id).html(parseInt($('.compl_count'+id).html())-1);
            }
        else
            {
            $('#send_complaint'+id).css('display', 'none');
            $('#no_complaint'+id).css('display', 'inline');
            $('.compl_count'+id).html(parseInt($('.compl_count'+id).html())+1);
            }

        });
    };

function show_edit_form_dossier_photo(id)
    {
    $('#row_dossier_photo'+id).css('display', 'none');
    $('#edit_dossier_photo'+id).css('display', 'table-row');
    };

function stop_edit_dossier_photo(id)
    {
    $('#edit_dossier_photo'+id).css('display', 'none');
    $('#row_dossier_photo'+id).css('display', 'table-row');
    };

function save_edit_row_dossier_photo(id)
    {
    var desc = $('#dossier_photo_desc'+id).val();

    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, text:desc, action:'save_edit_row_dossier_photo'}, function( data ){
        HideLoading("");
        if(data == 'ok')
            {
            $('.r_dossier_photo'+id).remove();
            $('#dossier_photo_counter').html(parseInt($('#dossier_photo_counter').html())-1);
            }
        else
            DLEalert(data, '����������');
        });
    };

function delete_dossier_photo_row_ap(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ ����?','�������� ����',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_dossier_photo_row_ap'}, function( data ){
            HideLoading("");
            if(data == 'ok')
                {
                $('.r_dossier_photo'+id).remove();
                $('#dossier_photo_counter').html(parseInt($('#dossier_photo_counter').html())-1);
                }
            else
                DLEalert(data, '�������� ����');
            });
        });
    };

function send_vote_explanations(id)
    {
    var val = 0;

    if(dle_skin != 'smartphone')
        val = $('#slider_quality_explanations').slider("value");
    else
        val = $('#slider_quality_explanations').val();

    if(parseInt(val) > 0)
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, val:val, action:'send_vote_explanations'}, function( data ){
            HideLoading("");
            if(data == 'auth')
                DLEalert('����� ������������� �������������', '�����������');
            else
                location = '/dossier/dossier-'+id+'.html'
            });
        }
    else
        DLEalert('�� �� ������ ��������� ������ ������ 1', '�����������');
    };

function send_vote_valuation(id)
    {
    var val = 0;

    if(dle_skin != 'smartphone')
        val = $('#slider_valuation').slider("value");
    else
        val = $('#slider_valuation').val();

    if(parseInt(val) != 0)
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, val:val, action:'send_vote_valuation'}, function( data ){
            HideLoading("");
            if(data == 'auth')
                DLEalert('����� ������������� �������������', '�����������');
            else
                location = '/dossier/dossier-'+id+'.html'
            });
        }
    else
        DLEalert('�� �� ������ ��������� ������ ������ 1', '�����������');
    };

function send_evil_or_good(id, type)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, type:type, action:'send_evil_or_good'}, function( data ){
        HideLoading("");
        if(data == 'auth')
            DLEalert('����� ������������� �������������', '�����������');
        else
            location = '/dossier/dossier-'+id+'.html'
        });
    };

function send_comm(id, type)
    {
    var text = $('#comments').val().trim();

    if(text != '')
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, type:type, text:text, action:'send_comm'}, function( data ){
            HideLoading("");
            $('#comments').val('');
            $('#comm_block').html(data+$('#comm_block').html());
            });
        }
    else
        DLEalert('����������� �� ����� ���� ������', '�������� �����������');
    };

function upload_photo_to_dossier()
    {
    if($('#filename').val() != '')
        $('#photo_upload').submit();
    else
        DLEalert('�� �� ������� ���� ��� ��������', '�������� ����');
    }

function domain_vote(id, type)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, type:type, action:'domain_vote'}, function( data ){
        HideLoading("");
        if(data == 'ok')
            {
            var r = parseInt($('#rate_link'+id).html());

            if(type == 'up')
                r = r + 1;
            else
                r = r - 1;

            if(r > 0)
                $('#rate_link'+id).css('color', 'green');
            else if(r < 0)
                $('#rate_link'+id).css('color', 'red');
            else
                $('#rate_link'+id).css('color', 'black');

            if(r > 0)
                $('#rate_link'+id).html('+'+r);
            else
                $('#rate_link'+id).html(r);
            DLEalert('�������, ��� ����� �����.', '���������� ������� ������');
            }
        else if(data == 'vote_exists')
            DLEalert('�� ��� ���������� �� ������ �����', '���������� ������� ������');
        else
            DLEalert(data, '���������� ������� ������');
        });
    };

function no_recomend(id)
    {
    var counter = parseInt($('#links_domains_counter').html());
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'no_recomend'}, function( data ){
        HideLoading("");
        if(data == 'ok')
            {
            $('.r_links_domains'+id).remove();
            $('#links_domains_counter').html(counter-1);
            }
        else
            DLEalert(data, '����������');
        });
    };

function show_link_compl_form(id, type)
    {
    if(type != '0')
        {
        $('#descr_compl').val('');
        $('#type_compl').val(type);
        $("#link_compl_form").dialog('open');
        }
    };

function send_link_complaint(id)
    {
    var type = $("#type_compl").val();
    var text = $('#descr_compl').val();

    if(type != '0')
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, type:type, text:text, action:'send_link_complaint'}, function( data ){
            HideLoading("");
            $('#link_compl_form').dialog( "close" );
            DLEalert('���� ������ ������� ����������.', '������ �� �����');
            });
        }
    else
        DLEalert('�� �� ������� ��� ������.', '������ �� �����');
    };

function domain_compl_result(id, type)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, type:type, action:'domain_compl_result'}, function( data ){
        HideLoading("");
        if(data == 'ok')
            {
            $('.r_links_domains'+id).remove();
            $('#links_domains_counter').html(parseInt($('#links_domains_counter').html())-1);
            }
        else
            DLEalert(data, '������������ ������');
        });
    };

function show_comm_compl_form(id, type, type_comm)
    {
    if(type != '0')
        {
        $('#descr_comm_compl').val('');
        $('#comm_id').val(id);
        $('#type_comm_compl').val(type);
        $("#comm_compl_form").dialog('open');
        $('#comm_type').val(type_comm);
        }
    };

function send_comm_complaint()
    {
    var id = $('#comm_id').val();
    var type_comm = $('#comm_type').val();
    var type = $("#type_comm_compl").val();
    var text = $('#descr_comm_compl').val();

    if(type != '0')
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, type:type, type_comm:type_comm, text:text, action:'send_comm_complaint'}, function( data ){
            HideLoading("");
            $('#comm_compl_form').dialog( "close" );
            $('#compl_comm'+id).val('0');
            DLEalert('���� ������ ������� ����������.', '������ �� �����������');
            });
        }
    else
        DLEalert('�� �� ������� ��� ������.', '������ �� �����������');
    };

function delete_comm_ap(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ �����������?','�������� �����������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_comm_ap'}, function( data ){
            HideLoading("");
            if(data == 'ok')
                {
                $('.r_comm_compl'+id).remove();
                $('#comm_compl_counter').html(parseInt($('#comm_compl_counter').html())-1);
                }
            else
                DLEalert(data, '�������� �����������');
            });
        });
    };

function sort_rate_domain(sort)
    {
    document.cookie="domains_sort="+sort;
    location='/rate-external-links.html';
    };

function vk_profile_add()
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'vk_profile_add'}, function( data ){
        HideLoading("");
        if(data == 'check_pass')
            {
            $('#vk_id_add').dialog('open');
            }
        else if(data == 'ok')
            location = 'https://oauth.vk.com/authorize?client_id=3732900&scope=&redirect_uri=http://baumanki.net/engine/modules/filearray/vk_add.php&response_type=code';
        });
    };

function auth_website()
    {
    var pass = $('#password_vk_auth').val().trim();

    if(pass != '')
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'auth_website', pass:pass}, function( data ){
            HideLoading("");
            if(data == 'no_pass')
                DLEalert('�� ����� �� ������ ������, ��������� �����.', '����������� �� �����');
            else if(data == 'ok')
                location = 'https://oauth.vk.com/authorize?client_id=3732900&scope=&redirect_uri=http://baumanki.net/engine/modules/filearray/vk_add.php&response_type=code';
            });
        }
    else
        DLEalert('�� �� ����� ������', '����������� �� �����');
    };

function save_new_login()
    {
    var new_login = $('#new_login').val().trim();

    if(new_login != '')
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'save_new_login', new_login:new_login}, function( data ){
            HideLoading("");
            if(data == 'ok')
                {
                $('#change_login').dialog('close');
                $('#my_login').html(new_login);
                DLEalert('����� ������� �������', '��������� ������');
                }
            else
                DLEalert(data, '��������� ������');
            });
        }
    else
        DLEalert('�� �� ������� ����� �����.', '��������� ������');
    };

function add_to_my_lecturer(id)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'add_to_my_lecturer', id:id}, function( data ){
        HideLoading("");
        if(data == 'ok')
            {
            $('#add_to_my_lecturer').css('display', 'none');
            $('#delete_from_my_lecturer').css('display', 'inline-block');
            DLEalert('������������� ������� �������� � ������ ����� ��������������', '������ ��������������');
            }
        else if(data == 'exists')
            DLEalert('���� ������������� ��� ���� � ����� ������ ��������������', '������ ��������������');
        else
            alert(data);
        });
    };

function delete_from_my_lecturer(id)
    {
    DLEconfirm('�� ������������� ������ ������� ����� ������������� �� ������ ������?', '������ ��������������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'delete_from_my_lecturer', id:id}, function( data ){
            HideLoading("");
            $('#add_to_my_lecturer').css('display', 'inline-block');
            $('#delete_from_my_lecturer').css('display', 'none');
            $('#l'+id).remove();
            DLEalert('������������� ������� ������ �� ������ ������ ��������������', '������ ��������������');
            });
        });
    };

function add_to_my_hs(id)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'add_to_my_hs', id:id}, function( data ){
        HideLoading("");
        if(data == 'ok')
            {
            $('#add_to_my_hs').css('display', 'none');
            $('#delete_from_my_hs').css('display', 'inline-block');
            DLEalert('��� ������� ��������', '���������� ����');
            }
        else if(data == 'exists')
            DLEalert('�� ��� �������� ��� ������� � ������ ����', '���������� ����');
        });
    };

function delete_from_my_hs(id)
    {
    DLEconfirm('�� ������������� ������ ������� ���� ��� �� ������ ������?', '�������� ����',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'delete_from_my_hs', id:id}, function( data ){
            HideLoading("");
            $('#add_to_my_hs').css('display', 'inline-block');
            $('#delete_from_my_hs').css('display', 'none');
            });
        });
    };

function send_pm(id)
    {
    var text = $('#comments').val().trim();

    if(text != '')
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'send_pm', id:id, text:text}, function( data ){
            HideLoading("");
            $('#pm_block').html(data+$('#pm_block').html());
            $('#comments').val('');
            });
        }
    else
        DLEalert('�� �� ������ ��������� ������ ���������', '�������� ���������');
    };
function delete_dialog(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ ������. ��� ��������� ����� ������� � ������������ �� �� ��������.','�������� �������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'delete_dialog', id:id}, function( data ){
            HideLoading("");
            $('#dialog'+id).remove();
            });
        });
    };

function delete_pm_message(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ ���������? ������, ��� ��������� ����� ������� ������ �� ����� ���������, � ������ ����������� ��� ���������. ������������ ��������� ��������� ����������.','�������� ���������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'delete_pm_message', id:id}, function( data ){
            HideLoading("");
            $('#pm_message_'+id).remove();
            });
        });
    };

function carma_vote(id, type, carma_type)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, type:type, carma_type:carma_type, action:'carma_vote'}, function( data ){
        HideLoading("");
        if(data == 'ok')
            {
            var r = parseInt($('#karma_'+carma_type).html());

            if(type == 'up')
                r = r + 1;
            else
                r = r - 1;

            if(r > 0)
                $('#karma_'+carma_type).css('color', 'green');
            else if(r < 0)
                $('#karma_'+carma_type).css('color', 'red');
            else
                $('#karma_'+carma_type).css('color', 'black');

            if(r > 0)
                $('#karma_'+carma_type).html('+'+r);
            else
                $('#karma_'+carma_type).html(r);
            DLEalert('�������, ��� ����� �����.', '�����');
            }
        else if(data == 'vote_exists')
            DLEalert('�� ��� ���������� ������ ����� ����� �������������', '�����');
        else if(data == 'is_my_comm')
            DLEalert('�� �� ������ ���������� ����� ������ ����', '�����');
        else if(data == 'auth')
            DLEalert('����� ������������� �������������', '�����');
        else
            DLEalert(data, '�����');
        });
    };

function delete_subj_name(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ �������?', '�������� ��������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'delete_subj_name', id:id}, function( data ){
            HideLoading("");
            $('.sn'+id).remove();
            });
        });
    };

function send_order()
    {
    var title = $('#title').val().trim();
    var subject = parseInt($('#subject').val());
    var �ategory = parseInt($('#�ategory').val());
    var descr = $('#description').val().trim();
    var file = $('#add_file').val();
    var id = $('#for_user').val();
    var payment = $('#payment').val();
    var subject_name = $('#subject_name').val().trim();

    if(title == '')
        DLEalert('�� �� ������� ���������', '�������� �������');
    else if(subject == 0)
        DLEalert('�������� �������', '�������� �������');
    else if(subject == -1 && subject_name == '')
        DLEalert('�� �� ������� �������', '�������� �������');
    else if(�ategory == 0)
        DLEalert('�������� ���������', '�������� �������');
    else if(descr == '')
        DLEalert('���������� ������� ���� �������', '�������� �������');
    else
        {
        if(subject == -1)
            subject = 0;

        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, payment:payment, title:title, subject:subject, cat_id:�ategory, descr:descr, file:file, subject_name:subject_name, action:'send_order'}, function( data ){
            HideLoading("");
            location = '/my-orders.html';
            });
        }
    };

function request_task(id)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'request_task'}, function( data ){
        HideLoading("");
        if(data == 'no_task')
            DLEalert('� ��� ��� ��������� �������, �������� �����.', '���������� �������');
        else
            {
            $('#sel_r').html(data);
            $("#sel_request_task").dialog('open');
            }
        });
    };

function send_request(id)
    {
    var task_id = $('#task_id').val();
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, task_id:task_id, action:'send_request'}, function( data ){
        HideLoading("");
        DLEalert('���� ����������� ������� ����������.', '���������� �������');
        });
    };

function start_task(id)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'start_task'}, function( data ){
        HideLoading("");
        $('#only_for_me_begin_task').remove();
        DLEalert('��������� ���������� ����������� ��� �� ���������� � �������. �������.', '�������');
        setTimeout("location='/order-"+id+".html'", 3000);
        });
    };

function executor_finish_task(id)
    {
    DLEconfirm('�� ������������� ������ ��������� �������? ������ �������� �������� �� ��������. �� ���������� ������� ���� �� ��� ��� �� ���������, ��� ����� ��������� �� ����� ��������.', '���������� �������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'executor_finish_task', id:id}, function( data ){
            HideLoading("");
            $("#send_recall_executor").dialog('open');
            });
        });
    };

function send_recall_executor(id)
    {
    var rate = $('#customer_rate').val();
    var text = $('#recall').val();

    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, rate:rate, text:text, action:'send_recall_executor'}, function( data ){
        HideLoading("");
        location = '/order-'+id+'.html';
        });
    };

function send_recall_customer(id)
    {
    var rate = $('#executor_rate').val();
    var text = $('#recall').val();

    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, rate:rate, text:text, action:'send_recall_customer'}, function( data ){
        HideLoading("");
        location = '/order-'+id+'.html';
        });
    };

function customer_finish_task(id)
    {
    DLEconfirm('�� ������������� ������ ��������� �������? ������ �������� �������� �� ��������. �� ���������� ������� ���� ����������� ��� ��� �� ��������, ��� ����� ��������� �� ����� ��������.', '���������� �������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'customer_finish_task', id:id}, function( data ){
            HideLoading("");
            $("#send_recall_customer").dialog('open');
            });
        });
    };

function send_bid(id)
    {
    var text = $('#bid_text').val().trim();

    if(text != '')
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'send_bid', id:id, text:text}, function( data ){
            HideLoading("");
            location = '/order-'+id+'.html';
            });
        }
    else
        DLEalert('�� �� �������� ����� ������', '���������� �����������');
    };

function set_executor(task_id, id)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'set_executor', id:id, task_id:task_id}, function( data ){
        HideLoading("");
        location = '/order-'+task_id+'.html';
        });
    };

function deny_task(id)
    {
    DLEconfirm('�� ������������� ������ ���������� �� �������? ������ �������� �������� �� ��������.', '����� �� �������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'deny_task', id:id}, function( data ){
            HideLoading("");
            location = '/order-'+id+'.html';
            });
        });
    };

function delete_task(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ �������?', '�������� �������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'delete_task', id:id}, function( data ){
            HideLoading("");
            $('#task_id'+id).remove();
            });
        });
    };

function delete_ex()
    {
    $('#tr_for_user').remove();
    $('#for_user').val('0');
    };

function show_freelance_page(id)
    {
    $('#page').val(id);
    $('#start_search_freelance').submit();
    };

function show_exe_page(id)
    {
    $('#page').val(id);
    $('#start_search_executor').submit();
    };

function show_add_subject_to_profile(id)
    {
    $('#cat_subject').val(id);
    $('#add_subject_to_profile').dialog('open');
    };

function subject_change()
    {
    var subject = $('#subject').val();
    if(subject == '0' || subject == '-1')
        {
        $('#subject_name_tr').css('display', 'table-row');
        $('#subject_name').val('');
        }
    else
        $('#subject_name_tr').css('display', 'none');

    };

function subject_change2()
    {
    var subject = $('#subject').val();
    if(subject == '-1')
        {
        $('#subject_name_tr').css('display', 'table-row');
        $('#subject_name').val('');
        }
    else
        $('#subject_name_tr').css('display', 'none');

    };

function add_subject_to_profile()
    {
    var id = $('#cat_subject').val();
    var subject = $('#subject').val();
    var text = $('#text_subject').val();
    var name = $('#subject_name').val().trim();

    if(subject == '')
        DLEalert('�� �� ������� �������', '���������� ��������');
    else if(subject == '0' && name == '')
        DLEalert('�� �� ������� �������', '���������� ��������');
    else
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'add_subject_to_profile', id:id, subject:subject, text:text, name:name}, function( data ){
            HideLoading("");
            $('#objects_'+id).html(data);
            $('#subject').val('');
            $('#subject_name').val('');
            $('#text_subject').val('');
            subject_change();

            $('#add_subject_to_profile').dialog('close');
            DLEalert('������� ������� ��������', '���������� ��������');
            });
        }
    };

function delete_my_subject(id, type)
    {
    DLEconfirm('�� ������������� ������ ������� ������ �������?', '�������� ��������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'delete_my_subject', id:id, type:type}, function( data ){
            HideLoading("");
            $('#li_'+type+'_'+id).remove();
            });
        });
    };

function add_to_nano_blog()
    {
    var title = $('#title').val().trim();
    var text = $('#comments').val().trim();
    var img = $('#img_nano_blog').val();
    var file = $('#file_nano_blog').val();
    var file_name = $('#file_name_nano_blog').val();
    var show_in_nb_list = 0;
    var cat_nb = $('#cat_nano_blog').val();

    if($("#show_in_nb_list").prop("checked"))
        show_in_nb_list = 1;

    if(title == '')
        DLEalert('���������� ������� ���������', '������ � ����-�����');
    else
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {title:title, text:text, img:img, file:file, file_name:file_name, show_in_nb_list:show_in_nb_list, cat_nb:cat_nb, action:'add_to_nano_blog'}, function( data ){
            HideLoading("");

            $('#title').val('');
            $('#comments').val('');
            $('#img_nano_blog').val('');
            $('#file_nano_blog').val('');
            $('#file_name_nano_blog').val('');
            $('#cat_nano_blog').val('0');
            $('#img_tr').css('display', 'none');
            $('#file_tr').css('display', 'none');
            $("#show_in_nb_list").prop("checked", true);
            $('#nano_blog_messages').html(data + $('#nano_blog_messages').html());
            $('#no_nano_blog_messages').remove();
            });
        }
    };

function add_to_nano_blog_from_feed()
    {
    var title = $('#title').val().trim();
    var text = $('#comments').val().trim();
    var img = $('#img_nano_blog').val();
    var file = $('#file_nano_blog').val();
    var file_name = $('#file_name_nano_blog').val();
    var show_in_nb_list = 0;
    var cat_nb = $('#cat_nano_blog').val();

    if($("#show_in_nb_list").prop("checked"))
        show_in_nb_list = 1;

    if(title == '')
        DLEalert('���������� ������� ���������', '������ � ����-�����');
    else
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {title:title, text:text, img:img, file:file, file_name:file_name, show_in_nb_list:show_in_nb_list, cat_nb:cat_nb, action:'add_to_nano_blog'}, function( data ){
            HideLoading("");

            $('#title').val('');
            $('#comments').val('');
            $('#img_nano_blog').val('');
            $('#file_nano_blog').val('');
            $('#file_name_nano_blog').val('');
            $('#cat_nano_blog').val('0');
            $('#img_tr').css('display', 'none');
            $('#file_tr').css('display', 'none');
            $("#show_in_nb_list").prop("checked", true);

            var my_loc = window.location;
            location = my_loc;
            });
        }
    };

function nano_blog_vote(id, type)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, type:type, action:'nano_blog_vote'}, function( data ){
        HideLoading("");
        if(data == 'ok')
            {
            var r = parseInt($('#mess_rate'+id).html());

            if(type == 'up')
                r = r + 1;
            else
                r = r -1;

            if(r > 0)
                $('#mess_rate'+id).css('color', 'green');
            else if(r < 0)
                $('#mess_rate'+id).css('color', 'red');
            else
                $('#mess_rate'+id).css('color', 'black');

            if(r > 0)
                $('#mess_rate'+id).html('+'+r);
            else
                $('#mess_rate'+id).html(r);

            DLEalert('�������, ��� ����� �����.', '����-����');
            }
        else if(data == 'you_blog')
            DLEalert('�� �� ������ ���������� � ����� ����-�����', '����-����');
        else if(data == 'auth')
            DLEalert('����� ������������� �������������', '����-����');
        else if(data == 'vote_exists')
            DLEalert('�� ��� ���������� �� ������ ���������', '����-����');
        });
    };

function update_pm(id)
    {
    var time = 20;
    var a = setInterval(function() {
        time--;
        if(time == 0)
            {
            clearInterval(a);
            var last_id = $('.one-mess:first').attr('mess_id');

            ShowLoading("");
            $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'update_pm', id:id, last_id:last_id}, function( data ){
                HideLoading("");
                $('.one-mess:first').before(data);
                update_pm(id);
                });
            }
        $('#sec_i').html(time);
        }, 1000);
    };

function load_all_pm(id)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'update_pm', id:id}, function( data ){
        HideLoading("");
        $('#pm_block').html(data);
        });
    };

function load_comm(type, page)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'load_comm', type:type, id:page}, function( data ){
        HideLoading("");
        $('#'+type+'_div').html(data);
        $("html,body").animate({scrollTop: $('#'+type+'_div').offset().top-50}, 800);
        });
    };

function start_post_search()
    {
    var text = $('#search_words_post').val().trim();
    var sort = $('#post_sort').val();

    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'start_post_search', text:text, sort:sort}, function( data ){
        HideLoading("");
        $('#post_result').html(data);
        });
    };

function start_file_search()
    {
    var text = $('#search_words_file').val().trim();
    var sort = $('#file_sort').val();

    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'start_file_search', text:text, sort:sort}, function( data ){
        HideLoading("");
        $("#file_result").html(data);
        });
    };

function start_lecture_search()
    {
    var text = $('#search_words_lecture').val().trim();
    var subject_id = $('#lecture_subject').val();

    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'start_lecture_search', text:text, subject_id:subject_id}, function( data ){
        HideLoading("");
        $("#lecture_result").html(data);
        });
    };

function start_dossier_search()
    {
    var text = $('#search_words_dossier').val().trim();
    var sort = $('#dossier_sort').val();

    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'start_dossier_search', text:text, sort:sort}, function( data ){
        HideLoading("");
        $('#dossier_result').html(data);
        });
    };

function get_random_num(min_random, max_random) {
    var range = max_random - min_random + 1;
    return Math.floor(Math.random()*range) + min_random;
};

function fast_search(text)
    {
    var l = parseInt(text.length);

    if(l >= 3)
        {
        var r = parseInt(get_random_num(0, 30000));
        r_key_search = r;

        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'fast_search', text:text}, function( data ){
            HideLoading("");
            $('#auto_search_result').html(data);
            if(data != '' && r_key_search == r)
                $('#auto_search_result').css('display', 'block');
            else
                $('#auto_search_result').css('display', 'none');
            });
        }
    else
        $('#auto_search_result').css('display', 'none');
    };

function search_blur()
    {
    if($('#search_words').val() == '')
        $('#search_words').val('�����...');

    setTimeout("$('#auto_search_result').css('display', 'none');", 150);
    };

function insert_matrix(pref)
    {
    DLEprompt('������� ����������� ������� �������� �� ��������. �������� &quot;2,3&quot;, ����� ������� ������� � 2-� �������� � 3-� ���������', '', '����������� �������', function(data){

        var rows = parseInt(data.substr(0, data.indexOf(',')));
        var col = parseInt(data.substr(data.indexOf(',')+1));
        var insert = '\\begin{'+pref+'matrix}\n';

        for(var i=0; i<rows; i++)
            {
            for(var j=1; j<col; j++)
                {
                insert = insert+' & ';
                }

            if(i+1 < rows)
                insert = insert + '\\\\\n';
            else
                insert = insert + '\n';
            }
        insert = insert+'\\end{'+pref+'matrix}';

        doInsert(insert, '', false);
        update_eqn_prev();
        });
    };

function insert_table()
    {
    DLEprompt('������� ����������� ������� �������� �� ��������. �������� &quot;2,3&quot;, ����� ������� ������� � 2-� �������� � 3-� ���������', '', '����������� �������', function(data){

        var rows = parseInt(data.substr(0, data.indexOf(',')));
        var col = parseInt(data.substr(data.indexOf(',')+1));
        var insert = '[table]\n';

        for(var i=1; i<=rows; i++)
            {
            insert = insert + '[tr]';
            for(var j=1; j<=col; j++)
                {
                /*DLEprompt('������� �������� ������ �������: '+i+' ������ '+j+' �������', '', '�������� �������', function(table_data){
                    insert = insert+'[td]'+i+'-'+j+'[/td]';
                    });*/
                insert = insert+'[td]'+i+'-'+j+'[/td]';
                }
            insert = insert + '[/tr]\n';
            }
        insert = insert+'[/table]';

        doInsert(insert, '', false);
        create_previev();
        });
    };

function insert_cases()
    {
    DLEprompt('������� ���������� ����� �������', '', '�������', function(data){

        var rows = parseInt(data);
        var insert = '\\begin{cases}\n';

        for(var i=0; i<rows; i++)
            {
            if(i+1 < rows)
                insert = insert + '\\\\\n';
            else
                insert = insert + '\n';
            }
        insert = insert+'\\end{cases}';

        doInsert(insert, '', false);
        update_eqn_prev();
        });
    };

function update_eqn_prev()
    {
    $('#MathPreview').attr('title', $('#eqn_title').val());
    $('#MathBuffer').attr('title', $('#eqn_title').val());
    Preview.Update();
    };

function ins_greek_alph()
    {
    document.getElementById(selField).focus();
    if ( is_ie )
        ie_range_cache = document.selection.createRange();

    $("#greek_alp").remove();

    $("body").append("<div id='greek_alp' title='��������� �������' style='display:none'>"+ document.getElementById('greek_alps').innerHTML +"</div>");

    var w = '250';
    var h = 'auto';

    $('#greek_alp').dialog({
        autoOpen: true,
        dialogClass: "modalfixed",
        width: w,
        height: h
    });

	$('.modalfixed.ui-dialog').css({position:"fixed"});
	$('#greek_alp').dialog( "option", "position", ['0','0'] );

    sleep(500);
    };
function sleep(ms) {
ms += new Date().getTime();
while (new Date() < ms){}
}
function create_previev()
    {
    var preview = parseInt($('#preview').val());
    var text = $('#task_text').val();
    var form = $('#preview_form').html();
    form = form.replace('{see_count}', '0');

    if(preview == 0)
        return false;

    if(text.indexOf('source_code=') > -1)
        {
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'get_source_code_prev', text_p_task:text}, function( data ){
            text = replace_text_prev_p_task(data);
            $('#preview_t_e').html(form.replace('{text_p_task}', text));
            });
        }
    else
        {
        text = replace_text_prev_p_task(text);
        $('#preview_t_e').html(form.replace('{text_p_task}', text));
        }


    };

function replace_equation_1(str, p1, offset, s)
    {
    p1 = p1.replace(new RegExp("\\n", 'gm'), "");
    return "<div style=\"text-align:center; padding:5px;\"><img src=\"http://latex.codecogs.com/png.latex?"+p1+"\"></div>";
    };

function replace_equation_2(str, p1, p2, offset, s)
    {
    p1 = p1.replace(new RegExp("\\n", 'gm'), "");
    p2 = p2.replace(new RegExp("\\n", 'gm'), "");
    return "<div style=\"text-align:center; padding:5px;\"><img title=\""+p1+"\" src=\"http://latex.codecogs.com/png.latex?"+p2+"\"></div>";
    };

function replace_table(str, p1, offset, s)
    {
    p1 = p1.replace(/\[td\]([\s\S]*?)\[\/td\]/gim, replace_td)
    p1 = p1.replace(new RegExp("\\n", 'gm'), "");
    p1 = p1.replace(new RegExp("\\[tr\\](.*?)\\[\\/tr\\]", 'gim'), "<tr>$1<//tr>");

    return "<div style=\"text-align:center;\"><table class=\"task-table\">"+p1+"</table></div>";
    };

function replace_td(str, p1, offset, s)
    {
    p1 = p1.replace(new RegExp("\\n", 'gm'), "<br />");
    return "<td>"+p1+"</td>";
    };

function replace_text_prev_p_task(text)
    {
    text = text.replace(/\[equation]([\s\S]*?)\[\/equation\]/gim, replace_equation_1)
    text = text.replace(/\[equation=(.*?)\]([\s\S]*?)\[\/equation\]/gim, replace_equation_2)

    text = text.replace(/\[table\]([\s\S]*?)\[\/table\]/gim, replace_table)

    text = text.replace(new RegExp("\\n", 'gm'), "<br />");

    text = text.replace(new RegExp("\\[sup\\]\\[greek_alph\\]", 'gm'), '<sup><span class="one_greek_simv_ind">');
    text = text.replace(new RegExp("\\[\\/greek_alph\\]\\[\\/sup\\]", 'gm'), '</span></sup>');
    text = text.replace(new RegExp("\\[sub\\]\\[greek_alph\\]", 'gm'), '<sub><span class="one_greek_simv_ind">');
    text = text.replace(new RegExp("\\[\\/greek_alph\\]\\[\\/sub\\]", 'gm'), '</span></sub>');

    text = text.replace(new RegExp("\\[greek_alph\\]", 'gm'), '<span class="one_greek_simv">');
    text = text.replace(new RegExp("\\[\\/greek_alph\\]", 'gm'), '</span>');
    text = text.replace(new RegExp("\\[sub\\]", 'gm'), '<sub>');
    text = text.replace(new RegExp("\\[\\/sub\\]", 'gm'), '</sub>');
    text = text.replace(new RegExp("\\[sup\\]", 'gm'), '<sup>');
    text = text.replace(new RegExp("\\[\\/sup\\]", 'gm'), '</sup>');

    text = text.replace(new RegExp("\\[b\\]", 'gm'), '<b>');
    text = text.replace(new RegExp("\\[\\/b\\]", 'gm'), '</b>');
    text = text.replace(new RegExp("\\[i\\]", 'gm'), '<i>');
    text = text.replace(new RegExp("\\[\\/i\\]", 'gm'), '</i>');
    text = text.replace(new RegExp("\\[u\\]", 'gm'), '<u>');
    text = text.replace(new RegExp("\\[\\/u\\]", 'gm'), '</u>');

    text = text.replace(new RegExp("\\[center\\]", 'gm'), '<div style="text-align:center;">');
    text = text.replace(new RegExp("\\[\\/center\\]", 'gm'), '</div>');

    text = text.replace(new RegExp("\\[center\\]\\[thumb\\](.*?)\\/p_task\\/(.*?)\\/(.*?)\\[\\/thumb\\]\\[\\/center\\]", 'gim'), "<div style=\"text-align:center;\"><a href=\"$1/p_task/$2/$3\" onclick=\"return hs.expand(this)\" ><img src=\"$1/p_task/$2/thumbs/$3\" /></a></div>");
    text = text.replace(new RegExp("\\[thumb\\](.*?)\\/p_task\\/(.*?)\\/(.*?)\\[\\/thumb\\]", 'gim'), "<a href=\"$1/p_task/$2/$3\" onclick=\"return hs.expand(this)\" ><img src=\"$1/p_task/$2/thumbs/$3\" /></a>");
    text = text.replace(new RegExp("\\[thumb=(.*?)\\](.*?)\\/p_task\\/(.*?)\\/(.*?)\\[\\/thumb\\]", 'gim'), "<a href=\"$2/p_task/$3/$4\" onclick=\"return hs.expand(this)\" ><img src=\"$2/p_task/$3/thumbs/$4\" style=\"float:$1;\" /></a>");

    text = text.replace(new RegExp("\\[center\\]\\[img\\](.*?)\\/p_task\\/(.*?)\\/(.*?)\\[\\/img\\]\\[\\/center\\]", 'gim'), "<div style=\"text-align:center;\"><img src=\"$1/p_task/$2/$3\" /></div>");
    text = text.replace(new RegExp("\\[img\\](.*?)\\/p_task\\/(.*?)\\/(.*?)\\[\\/img\\]", 'gim'), "<img src=\"$1/p_task/$2/$3\" />");
    text = text.replace(new RegExp("\\[img=(.*?)\\](.*?)\\/p_task\\/(.*?)\\/(.*?)\\[\\/img\\]", 'gim'), "<img src=\"$2/p_task/$3/$4\" style=\"float:$1;\" />");

    text = text.replace(new RegExp("\\[center\\]\\[thumb\\](.*?)\\/p_task_solution\\/(.*?)\\/(.*?)\\[\\/thumb\\]\\[\\/center\\]", 'gim'), "<div style=\"text-align:center;\"><a href=\"$1/p_task_solution/$2/$3\" onclick=\"return hs.expand(this)\" ><img src=\"$1/p_task_solution/$2/thumbs/$3\" /></a></div>");
    text = text.replace(new RegExp("\\[thumb\\](.*?)\\/p_task_solution\\/(.*?)\\/(.*?)\\[\\/thumb\\]", 'gim'), "<a href=\"$1/p_task_solution/$2/$3\" onclick=\"return hs.expand(this)\" ><img src=\"$1/p_task_solution/$2/thumbs/$3\" /></a>");
    text = text.replace(new RegExp("\\[thumb=(.*?)\\](.*?)\\/p_task_solution\\/(.*?)\\/(.*?)\\[\\/thumb\\]", 'gim'), "<a href=\"$2/p_task_solution/$3/$4\" onclick=\"return hs.expand(this)\" ><img src=\"$2/p_task_solution/$3/thumbs/$4\" style=\"float:$1;\" /></a>");

    text = text.replace(new RegExp("\\[center\\]\\[img\\](.*?)\\/p_task_solution\\/(.*?)\\/(.*?)\\[\\/img\\]\\[\\/center\\]", 'gim'), "<div style=\"text-align:center;\"><img src=\"$1/p_task_solution/$2/$3\" /></div>");
    text = text.replace(new RegExp("\\[img\\](.*?)\\/p_task_solution\\/(.*?)\\/(.*?)\\[\\/img\\]", 'gim'), "<img src=\"$1/p_task_solution/$2/$3\" />");
    text = text.replace(new RegExp("\\[img=(.*?)\\](.*?)\\/p_task_solution\\/(.*?)\\/(.*?)\\[\\/img\\]", 'gim'), "<img src=\"$2/p_task_solution/$3/$4\" style=\"float:$1;\" />");

    return text;
    }

function delete_p_task_cat(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ ���������?', '�������� ���������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'delete_p_task_cat', id:id}, function( data ){
            HideLoading("");
            $('.ptc'+id).remove();
            });
        });
    };

function delete_nano_blog_messgae(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ ��������� �� ����-�����?', '�������� ��������� �� ����-�����',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'delete_nano_blog_messgae', id:id}, function( data ){
            HideLoading("");
            $('#nano_blog_mess'+id).remove();
            });
        });
    };

function delete_nano_blog_cat(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ ��������� ����-�����?', '�������� ��������� ����-�����',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'delete_nano_blog_cat', id:id}, function( data ){
            HideLoading("");
            $('.nano_blog_cat'+id).remove();
            });
        });
    };

function sel_p_task_cat_id(el)
    {
    id = parseInt($(el).val());
    if(id <= 0)
        id = parseInt($(el).attr('parrent_id'));

    $('#p_task_cat_id').val(id);

    if(id > 0)
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'sel_p_task_cat_id', id:id}, function( data ){
            HideLoading("");
            $('#cat_select_td').html(data);
            });
        }
    else
        $('.child_cat').remove();
    };

function show_ceate_p_task_cat_dialog(id)
    {
    DLEprompt('������� �������� ����������� ���������', '', '�������� ���������', function(data){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'ceate_p_task_cat_dialog', name:data, id:id}, function( data ){
            HideLoading("");
            $('#cat_select_td').html(data);
            });
        });
    };

function show_img_upload_form(name, id)
    {
	document.getElementById(selField).focus();

	if(is_ie)
        ie_range_cache = document.selection.createRange();

	$("#dle_emo").remove();
    $("#greek_alp").remove();
	$("#cp").remove();
	$("#dlepopup").remove();

	p_task_upload ( selField, name, id, 'no');
    };

function p_task_img_upload(name, id)
    {
	document.getElementById(selField).focus();

	if(is_ie)
        ie_range_cache = document.selection.createRange();

	$("#dle_emo").remove();
    $("#greek_alp").remove();
	$("#cp").remove();
	$("#dlepopup").remove();

	p_task_upload ( selField, name, id, 'no');
    };

function p_task_upload ( area, author, news_id, wysiwyg){

		var rndval = new Date().getTime();
		var shadow = 'none';
        var ac = $('#action_img_upload').val();

		$('#mediaupload').remove();
		$('body').append("<div id='mediaupload' title='�������� � ������� �����������' style='display:none'></div>");

		$('#mediaupload').dialog({
			autoOpen: true,
			width: 680,
			height: 450,
			dialogClass: "modalfixed",
			open: function(event, ui) {
				$("#mediaupload").html("<iframe name='mediauploadframe' id='mediauploadframe' width='100%' height='400' src='"+dle_root+"engine/modules/filearray/ajax/upload.php?area=" + area + "&action="+ac+"&author=" + author + "&news_id=" + news_id + "&wysiwyg=" + wysiwyg + "&skin=" + dle_skin + "&rndval=" + rndval + "' frameborder='0' marginwidth='0' marginheight='0' allowtransparency='true'></iframe>");
				$( ".ui-dialog" ).draggable( "option", "containment", "" );
			},
			dragStart: function(event, ui) {
				shadow = $(".modalfixed").css('box-shadow');
				$(".modalfixed").fadeTo(0, 0.6).css('box-shadow', 'none');
				$("#mediaupload").hide();
			},
			dragStop: function(event, ui) {
				$(".modalfixed").fadeTo(0, 1).css('box-shadow', shadow);
				$("#mediaupload").show();
			},
			beforeClose: function(event, ui) {
				$("#mediaupload").html("");
			}
		});

		if ($(window).width() > 830 && $(window).height() > 530 ) {
			$('.modalfixed.ui-dialog').css({position:"fixed"});
			$('#mediaupload').dialog( "option", "position", ['0','0'] );
		}
		return false;
};

function add_p_task(id)
    {
    var cat_id = parseInt($('#p_task_cat_id').val());
    var text = $('#task_text').val().trim();

    if(cat_id > 0 && text != '')
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'add_p_task', cat_id:cat_id, id:id, text_p_task:text}, function( data ){
            HideLoading("");
            if(data == 'no_cat')
                DLEalert('��������� ��������� �� �������', '���������� ������');
            else if(data == 'no_parrent_cat')
                DLEalert('������ ����� ��������� ������ � �������������, � �������� ��������� ���������� ����� ���������!', '���������� ������');
            else if(parseInt(data) > 0)
                {
                if(parseInt(id) == 0)
                    {
                    $('#p_task_id').val(data);
                    $("#do_you_wont").dialog('open');
                    }
                else
                    location = '/show-p-task-'+id+'.html';
                }
            });
        }
    else if(cat_id == 0)
        DLEalert('��������� ��� ������ �� �������', '���������� ������');
    else if(text == '')
        DLEalert('����� ������ �� ������', '���������� ������');
    };

function set_check_p_task(id)
    {
    DLEconfirm('�� ������������� ������ �������� ������ ������?', '��������� ������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'set_check_p_task', id:id}, function( data ){
            HideLoading("");
            $('#no_check_'+id).remove();
            });
        });
    };

function set_check_p_task_a(id)
    {
    DLEconfirm('�� ������������� ������ �������� ������ ������?', '��������� ������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'set_check_p_task', id:id}, function( data ){
            HideLoading("");
            $('.tr_p_t_'+id).remove();

            var i = parseInt($('#p_task_counter').html());
            $('#p_task_counter').html(i-1);
            });
        });
    };

function delete_p_task(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ ������?', '�������� ������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'delete_p_task', id:id}, function( data ){
            HideLoading("");
            $('#p_task_block_'+id).remove();
            });
        });
    };

function delete_p_task_a(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ ������?', '�������� ������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'delete_p_task', id:id}, function( data ){
            HideLoading("");
            $('.tr_p_t_'+id).remove();

            var i = parseInt($('#p_task_counter').html());
            $('#p_task_counter').html(i-1);
            });
        });
    };

function select_type_solution(id)
    {
    $('.solution').hide();
    $('.solution_type'+id).show();
    };

function check_solution_price()
    {
    var pr = $('#solution_price').val();
    pr = pr.replace(',', '.');
    $('#solution_price').val(pr);
    };

function add_p_task_solution(type_s)
    {
    var t = parseInt($('#type_solution').val());
    var pr = parseFloat($('#solution_price').val());

    if(type_s == 'edit')
        {
        var p_task_id = parseInt($('#solution_p_task_id').val());
        if(p_task_id > 0)
            {}
        else
            {
            DLEalert('ID ������ �� ������ ��� ������ �������', '�������������� �������');
            return false;
            }
        }

    if(pr >= 0)
        {}
    else
        {
        DLEalert('��������� ������� �� �������� ��� �������� �� �����. ����� ������������ ������ ����� � ������� ����� � ����� ������� ����� �������.', '���������� �������');
        return false;
        }

    if(t == 1)
        {
        var f = $('#solution_file').val();

        if(type_s == 'edit' && f == '' && $("input").is("#exists_sol_file"))
            f = $('#exists_sol_file').val();

        var parts, ext = ( parts = f.split("/").pop().split(".") ).length > 1 ? parts.pop() : "";
        ext = ext.toLowerCase();

        if(f == '')
            {
            DLEalert('�� �� ������� ���� � ��������. ���� � ��� ��� ����� � ��������, �� �� ������ ��� ������ ������, �������� ���� ������� �������, ������ � ����� "��� �������" �������� "�������� ������� �������"', '���������� �������');
            return false;
            }
        else if(ext != 'jpg' && ext != 'jpeg' && ext != 'png' && ext != 'gif' && ext != 'doc' && ext != 'docx' && ext != 'pdf' && ext != 'txt' && ext != 'zip' && ext != 'rar')
            {
            DLEalert('� �������� ����������� �����: *.jpg; *.jpeg; *.png; *.gif; *.doc; *.docx; *.pdf; *.txt; *.zip; *.rar;<br />�������� ������ ����', '���������� �������');
            return false;
            }

        $('#task-form').attr('enctype', 'multipart/form-data');
        }
    else if(t == 2)
        {
        var text = $('#task_text').val();
        if(text == '')
            {
            DLEalert('�� �� �������� ������� ��� ������', '���������� �������');
            return false;
            }
        $('#task-form').removeAttr('enctype');
        }
    else
        return false;

    $('#task-form').submit();
    };

function show_p_task_page(pg, tab_id)
    {
    $('#page'+tab_id).val(pg);
    $('#tab_active').val(tab_id);
    $('#form_my_p_task').submit();
    };

function set_check_p_task_solution(id)
    {
    DLEconfirm('�� ������������� ������ �������� ������ �������?', '��������� �������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'set_check_p_task_solution', id:id}, function( data ){
            HideLoading("");
            $('#no_check_'+id).remove();
            });
        });
    };

function delete_p_task_solution(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ �������?', '�������� �������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'delete_p_task_solution', id:id}, function( data ){
            HideLoading("");
            $('#one_p_task_solution'+id).remove();
            });
        });
    };

function delete_p_task_solution_adm(id)
    {
    var counter = parseInt($('#p_task_solution_counter').html());

    DLEconfirm('�� ������������� ������ ������� ������ �������?', '�������� �������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'delete_p_task_solution', id:id}, function( data ){
            HideLoading("");
            $('.r_p_task_solution'+id).remove();
            $('#p_task_solution_counter').html(counter-1);
            });
        });
    };

function buy_p_task_solution(id)
    {
    DLEconfirm('�� ������������� ������ ������ ������ �������?', '������� �������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'buy_p_task_solution', id:id}, function( data ){
            HideLoading("");
            if(data == 'no_money')
                {
                DLEconfirm('�� ����� ����� ������������ �������. ������ ��������� ��� ����?', '������� �������',function(){
                    location = '/add-money-to-balance.html';
                    });
                }
            else if(data == 'ok')
                location = '/show-solution-'+id+'.html';
            else
                alert(data);
            });
        });
    };

function solution_vote(id, type)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, type:type, action:'solution_vote'}, function( data ){
        HideLoading("");

        if(data == 'you_solution')
            DLEalert('�� �� ������ ���������� �� ���� �������', '����������� �������� �������');
        else if(data == 'vote_exists')
            DLEalert('�� ��� ���������� �� ������ �������', '����������� �������� �������');
        else if(data == 'auth')
            DLEalert('��� ���� ����� ������������� �������������', '����������� �������� �������');
        else if(data == 'not_see')
            DLEalert('�� �� ������ ������������� �� ������ �������, ��������� �� �� ������������ � ���', '����������� �������� �������');
        else
            {
            $('#sol_rate').html(data);
            DLEalert('�������, ��� ����� �����.', '����������� �������� �������');
            }
        });
    };

function show_payment_form(type)
    {
    var price = parseInt($('#price_add').val().trim());

    if(price > 0)
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {type:type, price:price, action:'show_payment_form'}, function( data ){
            HideLoading("");

            $('#show_payment_form').html(data);
            $("html,body").animate({scrollTop: $("#show_payment_form").offset().top-90}, 500);

            });
        }
    else
        DLEalert('����� ��� ���������� ����� �������� �� �����', '���������� �������');
    };

function get_my_balance_log(id)
    {
    var date_start = $('#date_start').val();
    var date_finish = $('#date_finish').val();

    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, date_start:date_start, date_finish:date_finish, action:'get_my_balance_log'}, function( data ){
        HideLoading("");
        $('#table_log').html(data);
        });
    };

function buy_vip_access()
    {
    var time = $('#vip_access_time').val();
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:time, action:'buy_vip_access'}, function( data ){
        HideLoading("");
        if(data == 'no_money')
            DLEalert('�� ����� ������� �� ���������� �������', '������� VIP �������');
        else if(data == 'ok')
            {
            $("#buy_vip_access").dialog('close');
            DLEalert('VIP ������ ������� ������, �������� ��������', '������� VIP �������');
            }
        });
    };

function load_bill_table(start, end)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {start:start, end:end, action:'load_bill_table'}, function( data ){
        HideLoading("");
        $('#bill_table').html(data);
        });
    };

function get_bill_table()
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'get_bill_table'}, function( data ){
        HideLoading("");
        $('#bill_table').html(data);
        });
    };

function ins_mark()
    {
    document.getElementById(selField).focus();

    if ( is_ie )
        ie_range_cache = document.selection.createRange();

    $("#mark_div").remove();

    $("body").append("<div id='mark_div' title='�������������� ��������� � �����������' style='display:none'>"+ document.getElementById('mark_div_').innerHTML +"</div>");

    var w = '350';
    var h = 'auto';


    $('#mark_div').dialog({
        autoOpen: true,
        dialogClass: "modalfixed",
        width: w,
        height: h
    });

	$('.modalfixed.ui-dialog').css({position:"fixed"});
	$('#mark_div').dialog( "option", "position", ['0','0'] );
    };

function p_task_navigate(id)
    {
    $('#page').val(id);
    $('#submit_bt').click();
    };

function show_p_task_cat(id)
    {
    ShowLoading();
    document.cookie="p_task_cat_cookie="+id;
    location='/task-archive/';
    };

function edit_cat_p_task(name, par_cat, id)
    {
    $('#p_task_cat_name').val(name);
    $('#parentid_p_task_cat').val(par_cat);
    $('#edit_p_task_cat').val(id);
    $('#bt_add_edit_p_task_cat').val('��������� ���������');
    $('#action_p_task_cat').val('edit');

    $("html,body").animate({scrollTop: $("#p_task_cat_name").offset().top-50}, 100);
    };

function edit_nano_blog_cat(name, id)
    {
    $('#nano_blog_cat_name').val(name);
    $('#edit_nano_blog_cat').val(id);
    $('#bt_add_edit_nano_blog_cat').val('��������� ���������');
    $('#action_nano_blog_cat').val('edit');

    $("html,body").animate({scrollTop: $("#nano_blog_cat_name").offset().top-50}, 100);
    };

function get_my_money_send()
    {
    var price = parseInt($('#get_sum_my_money').val().trim());

    if(price > 0)
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {price:price, action:'get_my_money_send'}, function( data ){
            HideLoading("");
            if(data == 'no_money')
                DLEalert('�� ����� ������� ������������ �������', '����� �������');
            else if(data == 'wmr_not_exists')
                DLEalert('� ����� ������� �� ������ WMR �������. �������������� ���� �������, ������� ��� WMR ������� � ���������� �����.', '����� �������');
            else if(data == 'no_min_money')
                DLEalert('�� �� ������ �������� ������� ������ ����������� �����.', '����� �������');
            else if(data == 'payment_exists')
                DLEalert('�� ������ �� ����� ���������� ����� ����� �������. �������� ���� ����� ����������� ���������� �������.', '����� �������');
            else
                {
                DLEalert('������� ������� ��������, �������� ����������� �������', '����� �������');
                $("#get_my_money_dialog").dialog('close');
                }
            });
        }
    else
        DLEalert('�� ������� ������� ����� ��� ������ �������', '����� �������');
    };

function select_row(id)
    {
    $('.payment'+id).css('background-color', '#FCFCD9');
    };

function payment_ok(id)
    {
    DLEconfirm('�� ������������� ������ �������� ������� �'+id+' ��� ���������? ����� ������� ����� ������� �� ����� ������������ ������������.','�������',function(){
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'payment_ok'}, function( data ){
            $('.payment'+id).remove();
            });
        });
    };

function show_payment_form_no(id)
    {
    $('#hide'+id).css('display', 'table-row');
    $('#no_bt'+id).remove();
    };

function payment_no(id)
    {
    DLEconfirm('�� ������������� ������ �������� � ������� �'+id+'? ����� ������� ����� ���������� �� ���� ������������.', '�������',function(){
        var text = $('#text'+id).val();
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, text:text, action:'payment_no'}, function( data ){
            $('.payment'+id).remove();
            });
        });
    };

function cancel_payment(id)
    {
    DLEconfirm('�� ������������� ������ �������� ������� �'+id+'?','���������� ���������',function(){
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'cancel_payment'}, function( data ){
            if(data == 'ok')
                location = '/payment-log.html';
            });
        });
    };

function load_payment_table(start, end)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {start:start, end:end, action:'load_payment_table'}, function( data ){
        HideLoading("");
        $('#payment_table').html(data);
        });
    };

function load_site_balance_table(start, end)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {start:start, end:end, action:'load_site_balance_table'}, function( data ){
        HideLoading("");
        $('#site_balance_table').html(data);
        });
    };
function get_payment_table()
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'get_payment_table'}, function( data ){
        HideLoading("");
        $('#payment_table').html(data);
        });
    };

function get_site_balance_table()
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'get_site_balance_table'}, function( data ){
        HideLoading("");
        $('#site_balance_table').html(data);
        });
    };

function change_solution_price(id)
    {
    DLEprompt('������� ����� ��������� �������', '0.00', '��������� ��������� �������', function(data){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'change_solution_price', price:data, id:id}, function( data ){
            HideLoading("");
            DLEalert('��������� ������� ������� ��������, �������� ��������', '��������� ��������� �������');
            });
        });
    };

function replace_video_iframe(text, id)
    {
    if(id != '')
        {
        var scroll_pos_start = document.getElementById(id).selectionStart;
        var scroll_pos_end = document.getElementById(id).selectionEnd;
        var t = $("#"+id);

        for (var i=0; i<t.length; i++)
            {
            t[i].value=t[i].value.replace(/<iframe src=".*?(vk.com|vkontakte.ru)(.*?)".*?<\/iframe>/gim, 'http://vk.com$2');
            t[i].value=t[i].value.replace(/<iframe.*?src=".*?rutube.ru(.*?)".*?<\/iframe>/gim, 'http://rutube.ru$1');
            t[i].value=t[i].value.replace(/<iframe.*?src=".*?(youtube.com|youtu.be).*?\/embed\/(.*?)".*?<\/iframe>/gim, 'http://www.youtube.com/watch?v=$2');
            }

        setSelectionRange(document.getElementById(id), scroll_pos_start, scroll_pos_end);
        }
    else
        {
        text = text.replace(/<iframe src=".*?(vk.com|vkontakte.ru)(.*?)".*?<\/iframe>/gim, 'http://vk.com$2');
        text = text.replace(/<iframe.*?src=".*?rutube.ru(.*?)".*?<\/iframe>/gim, 'http://rutube.ru$1');
        text = text.replace(/<iframe.*?src=".*?(youtube.com|youtu.be).*?\/embed\/(.*?)".*?<\/iframe>/gim, 'http://www.youtube.com/watch?v=$2');
        }

    return '[media='+text+']';
    };

function setSelectionRange(input, selectionStart, selectionEnd) {
  if (input.setSelectionRange) {
    input.setSelectionRange(selectionStart, selectionEnd);
  }
  else if (input.createTextRange) {
    var range = input.createTextRange();
    range.collapse(true);
    range.moveEnd('character', selectionEnd);
    range.moveStart('character', selectionStart);
    range.select();
  }
};

function add_video_for_album(name)
    {
    $('#text-add-video').html('������� ��� ����� � '+name);

    $("#add-video-for-album-dialog").dialog('open');
    }

function insert_video_to_album(video_code, vname)
    {
    video_code = replace_video_iframe(video_code, '');

    if(video_code != '')
        {
        var tmp_tpl = $('#video_tpl').html();
        tmp_tpl = tmp_tpl.replace(new RegExp("\{video_code\}", 'gm'), video_code.trim());
        tmp_tpl = tmp_tpl.replace(new RegExp("\{video_name\}", 'gm'), vname.trim());

        $('#video_box').append(tmp_tpl);
        }
    };

function tag_vkontakte()
    {
	var thesel = get_sel(eval('fombj.'+ selField));
    if (!thesel)
        thesel = '';

	DLEprompt('������� ��� ������� ����� � ���������', thesel, dle_prompt, function (r) {
		doInsert(replace_video_iframe(r, ''), "", false);
		ie_range_cache = null;
        });
    };

function tag_rutube()
    {
	var thesel = get_sel(eval('fombj.'+ selField));
    if (!thesel)
        thesel = '';

	DLEprompt('������� ��� ������� ����� � RuTube', thesel, dle_prompt, function (r) {
		doInsert(replace_video_iframe(r, ''), "", false);
		ie_range_cache = null;
        });
    };

function go_nano_blog_feed()
    {
    var nb = parseInt($('#nano_blog_cat').val());

    if(nb > 0)
        location = '/nano-blog-'+nb+'-feed/';
    else
        location = '/nano-blog-feed/';
    };

function take_solution_please(id)
    {
    DLEprompt('����� �� ������ ��������� ������ ������, ��������� ��������� �������������� ������ �������. �� ���� � ��� ���� � ��������, �� ������ �������� ���� �� ��������� 0, ��� �� ��� ������ �����. ����� ���� ��� ������ ����� ������, � ������� �������� �������������� �����, ��� � ������ ��������� ������ ���������� �� ����.', '0', dle_prompt, function (p) {
		p = parseInt(p);
        if(p >= 0)
            {
            ShowLoading("");
            $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'take_solution_please', id:id, price:p}, function( data ){
                HideLoading("");
                DLEalert('������� � ������� ������ ������� ����������', '������ �� ������� ������');
                });
            }
        else
            DLEalert('�������������� �������� �������, ����� ������� ������ ����� �����', '������ �� ������� ������');
        });
    };

function show_take_solution(id)
    {
    $('#take_solution_'+id).dialog('open');
    };

function send_my_money_for_user(name, price)
    {
    if(name == '')
        name = $('#send_money_uname').val().trim();

    if(price == '')
        price = parseInt($('#send_sum_money').val().trim());

    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'send_my_money_for_user', name:name, price:price}, function( data ){
        HideLoading("");
        if(data == 'error_name')
            DLEalert('��������� ������������ �� ������ ��� �� �� ������� ��� ������������ ��� ��������.', '������� ������� ������������');
        else if(data == 'error_price')
            DLEalert('����� ��� �������� �� �������� ��� �������� �� �����', '������� ������� ������������');
        else if(data == 'no_ballance')
            DLEalert('�� ����� ����� �� ���������� ������� ��� ��������', '������� ������� ������������');
        else if(data == 'ok')
            {
            DLEalert('������� ������� ��������', '������� ������� ������������');
            $( "#send_money_for_user" ).dialog('close');
            }
        });
    };

function add_definition_descr()
    {
    var element_text = $("#default-tr-definition-descr").clone();
    element_text.removeAttr('id');

    var rand_name = 'definition_descr_'+getRandomInt(1, 999999);

    $(element_text).find('textarea').val('');
    $(element_text).find('textarea').attr('name', rand_name).attr('id', rand_name);;
    $(element_text).find('td > div').first().attr('onmouseenter', 'selField = \''+rand_name+'\'; get_sel(eval(\'fombj.\'+ selField));');
    $(element_text).find('td').first().append('<br />[<a href="#" onclick="delete_definition_descr($(this)); return false;">�������</a>]');

    $(".definition-descr").last().after(element_text);
    };

function delete_definition_descr(el)
    {
    el.parent('td').parent('tr').remove();
    };
function getRandomInt(min, max)
{
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function add_definition(id)
    {
    var definition = $('#definition').val().trim();
    var def_desc_arr = [];
    var no_descr = true;
    var full_descr = $('#full_descr').val().trim();
    var relinks = $('#relinks').val().trim();

    $(".definition-descr-text").each(function(indx){
        var txt_descr = $(this).val().trim();
        def_desc_arr.push(txt_descr);
        if(txt_descr != '')
            no_descr = false;
        });

    if(no_descr == false && definition != '')
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'add_definition', definition:definition, def_desc_arr:def_desc_arr, full_descr_definition:full_descr, relinks:relinks, id:id}, function( data ){
            HideLoading("");

            if(parseInt(data) > 0)
                {
                location = '/definition-'+data+'.html';
                }
            else
                DLEalert('�� �� ��������� ��� ������������ ����.', '���������� �����������');
            });
        }
    else
        DLEalert('�� �� ��������� ��� ������������ ����.', '���������� �����������');
    };

function delete_definition(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ �����������', '�������� �����������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_definition'}, function( data ){
            HideLoading("");
            location = '/definitions/';
            });
        });
    };

function show_definitions_page(page)
    {
    var nav_type = $('#navigation_type').val();

    if(nav_type == 'ajax')
        {
        var rand_val = Math.floor(Math.random( ) * (99998+1));
        $('#ajax_key').val(rand_val);

        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:page, text:$('#search_text').val().trim(), action:'show_definitions_page'}, function( data ){
            HideLoading("");
            if(String(rand_val) == $('#ajax_key').val())
                {
                $('#search_result').html(data);
                }
            });

        return false;
        }
    else
        return true;
    };

function fast_search_definitions(text)
    {
    var rand_val = Math.floor(Math.random( ) * (99998+1));
    $('#ajax_key').val(rand_val);

    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:'1', text:text, action:'show_definitions_page'}, function( data ){
        HideLoading("");
        if(String(rand_val) == $('#ajax_key').val())
            $('#search_result').html(data);
        });
    };

function delete_lecture(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ ������?', '�������� ������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_lecture'}, function( data ){
            HideLoading("");
            $('#lecture_row_'+id).remove();
            DLEalert('������ ������� �������', '�������� ������');
            });
        });
    };

function delete_lecture_theme(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ ���� �� ����� �� ��������?', '�������� ����',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_lecture_theme'}, function( data ){
            HideLoading("");
            $('#lecture_theme_'+id).remove();
            DLEalert('���� ������� �������', '�������� ����');
            });
        });
    };

function delete_lecture_subject(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ ������� �� ����� ��������?', '�������� ��������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_lecture_subject'}, function( data ){
            HideLoading("");
            $('#lecture-subject-'+id).remove();
            DLEalert('������� ������� ������', '�������� ��������');
            });
        });
    };



function delete_solution_file(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ ���� �������?', '�������� ����� �������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_solution_file'}, function( data ){
            HideLoading("");
            $('#loaded-sol-file').remove();
            DLEalert('���� ������� ������', '�������� ����� �������');
            });
        });
    };
function delete_rough_copy(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ ��������?', '�������� ���������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_rough_copy'}, function( data ){
            HideLoading("");
            $('#rough_copy_r'+id).remove();
            DLEalert('�������� ������� ������', '�������� ���������');
            });
        });
    };
function get_data_post_field()
    {
    var rough_copy = {};

    rough_copy.title = $('#title').val().trim();
    rough_copy.category = $('#category').val();
    rough_copy.short_story = $('#short_story').val().trim();
    rough_copy.full_story = $('#full_story').val();
    rough_copy.tags = $('#tags').val().trim();
    rough_copy.allow_main = 0;
    rough_copy.vote_title = $('#vote_title').val().trim();
    rough_copy.frage = $('#frage').val().trim();
    rough_copy.vote_body = $('#vote_body').val();
    rough_copy.allow_m_vote = 0;

    if($("#allow_main").prop("checked"))
        rough_copy.allow_main = 1;
    if($("#allow_m_vote").prop("checked"))
        rough_copy.allow_m_vote = 1;

    return rough_copy;
    }

function save_rough_copy_post(show_save_process)
    {
    if(show_save_process)
        ShowLoading("");

    var rough_copy = get_data_post_field();
    var rough_copy_id = $('#rough_copy_id').val();

    if(rough_copy.title != '' || rough_copy.category != null || rough_copy.short_story != '' || rough_copy.full_story != ''
        || rough_copy.tags != '' || rough_copy.allow_main != 0 || rough_copy.vote_title != '' || rough_copy.frage != ''
        || rough_copy.vote_body != '' || rough_copy.allow_m_vote != 0 || show_save_process)
        {
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:rough_copy_id, content_arr:rough_copy, action:'save_rough_copy_post'}, function( data ){

            if(parseInt(data) > 0)
                $('#rough_copy_id').val(data);

            if(show_save_process)
                {
                HideLoading("");
                DLEalert('���� ������ ������� ��������� � ����������', '���������� ������');
                }
            });
        }
    };

function load_rough_copy(id)
    {
    $('#rough_copy_load_id').val(id);
    $('#rough_copy_load_form').submit();
    };

function check_user_post()
    {
    var post_content = get_data_post_field();

    var error_arr = [];
    if(post_content.title == '')
        error_arr.push('��������� �� ������');
    if(post_content.category == null)
        error_arr.push('��������� �� ��������');
    if(post_content.short_story == '')
        error_arr.push('������� ���������� �� ��������');
    if(post_content.tags == '')
        error_arr.push('�� �� ������� ����');
    else
        {
        var x = post_content.tags.split(',');

        if(x.length < 5 || x.length > 20)
            error_arr.push('������ ���� �������� �� ����� 5-�� � �� ����� 20-�� �����');
        }

    if(error_arr.length == 0)
        {
        //����� ������� �������
        var payment_arr = [];

        var count_p_l = parseInt(substr_count(post_content.full_story, '[url'))+parseInt(substr_count(post_content.short_story, '[url'));

        var l_p = parseFloat($('#post_show_link_price').val());
        var m_p = parseFloat($('#post_show_on_main_page').val());
        var price = l_p*count_p_l;

        if(count_p_l > 0)
            payment_arr.push('������ ������ ('+l_p+' ���. � '+count_p_l+' = '+price+' ���.)');
        if(post_content.allow_main == 1)
            {
            payment_arr.push('���������� ������ �� ������� �������� '+m_p+' ���.');
            price = price + m_p;
            }

        if(payment_arr.length > 0)
            {
            DLEconfirm('���� ������ �������� ������� �����:<br /><b>'+payment_arr.join('<br />')+'</b><br />� ������ ���� ����� ������� '+price+' ���. �� ������������� ������ �������� ���� ������?', '���������� ������',function(){
                var u_balance = $('#balance').val();

                if(u_balance-price < 0)
                    DLEalert('�� ����� ����� �� ���������� �������, ��������� ������ � ���������� � ��������� ������.', '���������� ������');
                else
                    {
                    $('#entryform').submit();
                    }
                });
            }
        else
            {
            $('#entryform').submit();
            }
        }
    else
        {
        DLEalert('��� ���������� ������ �� ��������� ��������� ������:<br /><b>'+error_arr.join('<br />')+'</b>', '���������� ������');
        }
    };

function substr_count( haystack, needle, offset, length ) {

	var pos = 0, cnt = 0;

	if(isNaN(offset)) offset = 0;
	if(isNaN(length)) length = 0;
	offset--;

	while( (offset = haystack.indexOf(needle, offset+1)) != -1 ){
		if(length > 0 && (offset+needle.length) > length){
			return false;
		} else{
			cnt++;
		}
	}

	return cnt;
};

function set_check_user_post(id)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'set_check_user_post'}, function( data ){
        HideLoading("");
        $('#no_check_post_'+id).remove();
        });
    };

function post_rate_vote(id, type)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, type:type, action:'post_rate_vote'}, function( data ){
        HideLoading("");
        if(data == 'ok')
            {
            $('#b_rate_'+id+' .masha_index').remove();
            var r = parseInt($('#b_rate_'+id).html());

            if(type == 'up')
                r = r + 1;
            else
                r = r - 1;

            if(r > 0)
                $('#b_rate_'+id).css('color', 'green');
            else if(r < 0)
                $('#b_rate_'+id).css('color', 'red');
            else
                $('#b_rate_'+id).css('color', 'black');

            if(r > 0)
                $('#b_rate_'+id).html('+'+r);
            else
                $('#b_rate_'+id).html(r);
            DLEalert('�������, ��� ����� �����.', '����������� �������� ������');
            }
        else if(data == 'vote_exists')
            DLEalert('�� ��� ���������� �� ������ ������', '����������� �������� ������');
        else if(data == 'is_my_comm')
            DLEalert('�� �� ������ ���������� ������� ����� �� �������', '����������� �������� ������');
        else if(data == 'auth')
            DLEalert('��� ���� ����� ������������� �������������', '����������� �������� ������');
        else
            DLEalert(data, '����������� �������� ������');
        });
    };

function album_rate_vote(id, type)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, type:type, action:'album_rate_vote'}, function( data ){
        HideLoading("");
        if(data == 'ok')
            {
            $('#b_rate_'+id+' .masha_index').remove();
            var r = parseInt($('#b_rate_'+id).html());

            if(type == 'up')
                r = r + 1;
            else
                r = r - 1;

            if(r > 0)
                $('#b_rate_'+id).css('color', 'green');
            else if(r < 0)
                $('#b_rate_'+id).css('color', 'red');
            else
                $('#b_rate_'+id).css('color', 'black');

            if(r > 0)
                $('#b_rate_'+id).html('+'+r);
            else
                $('#b_rate_'+id).html(r);
            DLEalert('�������, ��� ����� �����.', '����������� �������� �������');
            }
        else if(data == 'vote_exists')
            DLEalert('�� ��� ���������� �� ������ ������', '����������� �������� �������');
        else if(data == 'is_my_comm')
            DLEalert('�� �� ������ ���������� ������� ����� �� �������', '����������� �������� �������');
        else if(data == 'auth')
            DLEalert('��� ���� ����� ������������� �������������', '����������� �������� �������');
        else
            DLEalert(data, '����������� �������� �������');
        });
    };

function show_hint_advertising_banner(id, value)
    {
    $('.hint_'+id).hide();
    $('#hint_'+id+'_'+value).show();
    };

function delete_advertising_banner(id)
    {
    var p = $('#system_percent_del_adv_banner').val();
    DLEconfirm('�� ������������� ������ ������� ������ ������? ������� ������� ����� ��������� �� ��� ������, �� ������� ��������� �������: '+p+'%', '�������� �������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_advertising_banner'}, function( data ){
            HideLoading("");
            $('#one_tr_adv_b_'+id).remove();
            DLEalert('������ ������� ������', '�������� �������');
            });
        });
    }

function add_advertising_banner()
    {
    var name = $('#banner_name').val().trim();
    var type = parseInt($('#banner_type').val());
    var text = $('#banner_text').val().trim();
    var ip_show = $('#ip_show').val();
    var place = $('#banner_place').val();

    var add_type = parseInt($('#banner_add_type').val());
    var banner_link = $('#banner_link').val().trim();
    var b_is_load_img = $('#b_is_load_img').val();
    var can_add = false;



    if(add_type == 1)
        {
        if(type == 1 || type == 2 || type == 3)
            {
            if(b_is_load_img != '')
                can_add = true;
            }
        else if(type == 4)
            can_add = true;
        }


    if(add_type == 0 && name != '' && type != 0 && text != '')
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {name:name, type:type, banner_text:text, ip_show:ip_show, place:place, action:'add_advertising_banner'}, function( data ){
            HideLoading("");
            if(data == 'ok')
                location = '/advertising/banners.html';
            else if(data == 'iframe_exists')
                DLEalert('��������� ������������ iframe', '���������� �������');
            else
                DLEalert('�� ����� ���������� ��������� ������, ��������� ������������ ���������� �����', '���������� �������');
            });
        }
    else if(add_type == 1 && name != '' && type != 0 && parseInt(banner_link.length) > 7 && can_add)
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {name:name, add_type:add_type, banner_link:banner_link, b_is_load_img:b_is_load_img, type:type, banner_text:text, ip_show:ip_show, place:place, action:'add_advertising_banner'}, function( data ){
            HideLoading("");
            if(data == 'ok')
                location = '/advertising/banners.html';
            else if(data == 'iframe_exists')
                DLEalert('��������� ������������ iframe', '���������� �������');
            else
                DLEalert('�� ����� ���������� ��������� ������, ��������� ������������ ���������� ����� = '+data, '���������� �������');
            });
        }
    else
        DLEalert('�� �� ��������� ��� ������������ ����', '���������� �������');
    };

function save_edit_adv_banner(id)
    {
    var name = $('#banner_name').val().trim();
    var type = parseInt($('#banner_type').val());
    var text = $('#banner_text').val().trim();
    var ip_show = $('#ip_show').val();
    var place = $('#banner_place').val();

    if(name != '' && type != 0 && text != '')
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, name:name, type:type, banner_text:text, ip_show:ip_show, place:place, action:'save_edit_adv_banner'}, function( data ){
            HideLoading("");

            if(data == 'ok')
                location = '/advertising/banners.html';
            else if(data == 'iframe_exists')
                DLEalert('��������� ������������ iframe', '���������� �������');
            else
                DLEalert('�� ����� ���������� ��������� ������, ��������� ������������ ���������� �����', '���������� �������');
            });
        }
    else
        DLEalert('�� �� ��������� ��� ������������ ����', '���������� �������');
    };

function start_show_banner(id)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'start_show_banner'}, function( data ){
        HideLoading("");
            if(data == 'ok')
                location = '/advertising/banners.html';
            else if(data == 'no_money')
                DLEalert('����� ��������� ������ ��������� ��� �������', '������ �������');
        });
    };

function stop_show_banner(id)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'stop_show_banner'}, function( data ){
        HideLoading("");
        location = '/advertising/banners.html';
        });
    };

function add_to_adv_banner_ballance(id, type)
    {
    var one_p = $('#banner_price_'+type).val();

    $('#one_b_pr').val(one_p);
    $('#price_one_banner_show').html(''+one_p+' ���.');
    $('#banner_id').val(id);
    $('#banner_payment_count').val('100');

    get_adv_banner_price();

    $( "#add_adv_banner_ballance" ).dialog("open");
    };

function get_adv_banner_price()
    {
    var one_p = parseFloat($('#one_b_pr').val());
    var count = parseInt($('#banner_payment_count').val());

    if(count > 0)
        {}
    else
        count = 0;

    $('#sum_banner_show').html(String(number_format(count*one_p, 2, '.', ''))+' ���.')
    };

function number_format( number, decimals, dec_point, thousands_sep ) {	// Format a number with grouped thousands
	//
	// +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
	// +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +	 bugfix by: Michael White (http://crestidg.com)

	var i, j, kw, kd, km;

	// input sanitation & defaults
	if( isNaN(decimals = Math.abs(decimals)) ){
		decimals = 2;
	}
	if( dec_point == undefined ){
		dec_point = ",";
	}
	if( thousands_sep == undefined ){
		thousands_sep = ".";
	}

	i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

	if( (j = i.length) > 3 ){
		j = j % 3;
	} else{
		j = 0;
	}

	km = (j ? i.substr(0, j) + thousands_sep : "");
	kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
	//kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
	kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");


	return km + kw + kd;
}

function send_money_to_adv_banner()
    {
    var id = $('#banner_id').val();
    var count = parseInt($('#banner_payment_count').val());

    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, count:count, action:'send_money_to_adv_banner'}, function( data ){
        HideLoading("");
        if(data == 'no_money')
            DLEalert('�� ����� ������� ������������ �������, ��������� ��� ������', '���������� ������� �������');
        else
            location = '/advertising/banners.html';
        });
    }

function save_adv_banner_ap(id)
    {
    var text = $('#adv_banner_text'+id).val();
    var type = $('#adv_banner_type'+id).val();
    var place = $('#adv_banner_place'+id).val();

    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, type:type, banner_text:text, place:place, action:'ap_banner_is_check'}, function( data ){
        HideLoading("");
        if(data == 'ok')
            {
            $('#adv_banner_counter').html(parseInt($('#adv_banner_counter').html())-1);
            $('.adv_banner_row'+id).remove();
            }
        else if(data == 'iframe_exists')
            DLEalert('��������� ������������ iframe', '���������� �������');
        });
    };

function disable_adv_banner(id)
    {
    DLEprompt('������� ������� ���������� �������, ������ ������� ����� ���������� ������������. �� ������ ���� ��������������� ������ ���� ������� ���.', '', '������� ���������� �������', function(data){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, text:data, action:'disable_adv_banner'}, function( data ){
            HideLoading("");
            $('#adv_banner_counter').html(parseInt($('#adv_banner_counter').html())-1);
            $('.adv_banner_row'+id).remove();
            });
        });
    };

function get_my_adv_banner_log(id)
    {
    var date_start = $('#date_start').val();
    var date_finish = $('#date_finish').val();
    var type = $('#banner_type').val();
    var b_id = $('#adv_id').val();

    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, date_start:date_start, date_finish:date_finish, type:type, b_id:b_id, action:'get_my_adv_banner_log'}, function( data ){
        HideLoading("");
        $('#table_log').html(data);
        });
    };

function block_banner_place(id)
    {
    $("#banner_place option").removeAttr('disabled').removeAttr('selected');
    $("#banner_place").val('0');

    if(parseInt(id) == 2 || parseInt(id) == 4)
        {
        $("#banner_place option[value='0']").attr("disabled", "disabled");
        $("#banner_place option[value='1']").attr("disabled", "disabled");
        $("#banner_place option[value='2']").attr("disabled", "disabled");
        $("#banner_place option[value='4']").attr("disabled", "disabled");
        $("#banner_place option[value='5']").attr("disabled", "disabled");
        $("#banner_place option[value='6']").attr("disabled", "disabled");
        $("#banner_place").val('3');
        }
    };

function sel_banner_add_type(id)
    {
    $('.banner_add_type_0, .banner_add_type_1').hide();
    $('.banner_add_type_'+id).show();

    check_banner_type();
    };

function check_banner_type()
    {
    $('.b_load_img').hide();
    var banner_add_type = $('#banner_add_type').val();
    var id = parseInt($('#banner_type').val());
    if(id != 0 && id != 4 && banner_add_type == 1)
        $('.b_load_img').show();
    };

function i_click_to_clickunder(key_1, key_2, key_3)
    {
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {key_1:key_1, key_2:key_2, key_3:key_3, action:'i_click_to_clickunder'}, function( data ){

        });
    };

//����������� �������� ��������
function  getPageSize()
    {
    var xScroll, yScroll;

           if (window.innerHeight && window.scrollMaxY) {
                   xScroll = document.body.scrollWidth;
                   yScroll = window.innerHeight + window.scrollMaxY;
           } else if (document.body.scrollHeight > document.body.offsetHeight){
                   xScroll = document.body.scrollWidth;
                   yScroll = document.body.scrollHeight;
           } else if (document.documentElement && document.documentElement.scrollHeight > document.documentElement.offsetHeight){
                   xScroll = document.documentElement.scrollWidth;
                   yScroll = document.documentElement.scrollHeight;
           } else {
                   xScroll = document.body.offsetWidth;
                   yScroll = document.body.offsetHeight;
           }

           var windowWidth, windowHeight;
           if (self.innerHeight) {
                   windowWidth = self.innerWidth;
                   windowHeight = self.innerHeight;
           } else if (document.documentElement && document.documentElement.clientHeight) {
                   windowWidth = document.documentElement.clientWidth;
                   windowHeight = document.documentElement.clientHeight;
           } else if (document.body) {
                   windowWidth = document.body.clientWidth;
                   windowHeight = document.body.clientHeight;
           }

            if(yScroll < windowHeight){
                   pageHeight = windowHeight;
           } else {
                   pageHeight = yScroll;
           }

           if(xScroll < windowWidth){
                   pageWidth = windowWidth;
           } else {
                   pageWidth = xScroll;
           }

           return [pageWidth,pageHeight,windowWidth,windowHeight];
    };

function check_event_data()
    {
    var name = $('#name').val().trim();
    var event_date = $('#event_date').val().trim();
    var event_descr = $('#event_descr').val().trim();
    var event_coordinates = $('#event_coordinates').val().trim();
    var event_address = $('#event_address').val().trim();
    var tags = $('#tags').val().trim();
    var time_start = $('#time_start').val().trim();

    if(tags == '')
        {
        DLEalert('�� �� ������� ����', '���������� �������');
        return false;
        }
    else if(time_start == '')
        {
        DLEalert('�� �� ������� ����� ������ �������', '���������� �������');
        return false;
        }
    else
        {
        var x = tags.split(',');

        if(x.length < 3 || x.length > 20)
            {
            DLEalert('������ ���� �������� �� ����� 3-� � �� ����� 20-�� �����', '���������� �������');
            return false;
            }
        }

    if(name != '' && event_date != '' && event_descr != '')
        return true;
    else
        DLEalert('���������� ��������� ��� ����', '���������� �������');

    return false;
    };

function i_go_to_event(id)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'i_go_to_event'}, function( data ){
        HideLoading("");
        $('#i_go_to_event_bt').hide();
        $('#i_not_go_to_event_bt').show();
        });
    };

function i_not_go_to_event(id)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'i_not_go_to_event'}, function( data ){
        HideLoading("");
        $('#i_go_to_event_bt').show();
        $('#i_not_go_to_event_bt').hide();
        });
    };

function add_inform_for_event(id, report_exists, album_exists)
    {
    $('#event_id').val(id);
    $('#album_url').val('');
    $('#report_url').val('');
    $('#update_text').val('');

    if(parseInt(report_exists) == 1)
        $('#report_row').hide();
    else
        $('#report_row').show();

    if(parseInt(album_exists) == 1)
        $('#album_row').hide();
    else
        $('#album_row').show();

    $('#add_info_for_event').dialog('open');
    };

function save_event_info()
    {
    var id = $('#event_id').val();
    var album_url = $('#album_url').val();
    var report_url = $('#report_url').val();
    var update_text = $('#update_text').val();

    if(album_url != '' || report_url != '' || update_text != '')
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, album_url:album_url, report_url:report_url, update_text:update_text, action:'save_event_info'}, function( data ){
            HideLoading("");

            $('#add_info_for_event').dialog('close');
            DLEalert('������� ������� ���������', '���������� �������');
            });
        }
    else
        DLEalert('��������� ������ ���� ����', '���������� �������');
    };

function preview_add_hs_event()
    {
    if(check_event_data())
        {
        dd = window.open('', 'prv', 'height=600,width=770,resizable=0,scrollbars=1');
        document.entryform.action='/engine/modules/filearray/ajax/user_ajax.php?action=preview_add_hs_event';
        document.entryform.target='prv';
        document.entryform.submit();
        dd.focus()

        setTimeout("document.entryform.action='';document.entryform.target='_self'", 500);
        }
    };

function check_album_data()
    {
    var name = $('#name').val().trim();
    var description = $('#description').val().trim();

    if(name != '' && description != '')
        return true;
    else
        DLEalert('���������� ��������� ��� ������������ ����', '���������� �������');

    return false;
    };

function show_hide_block(id, link_id, show_test, hide_text)
    {
    var is_show = parseInt($('#'+id).attr('show'));

    if(is_show == 1)
        {
        $('#'+link_id).html(hide_text);
        $('#'+id).attr('show', '0').slideToggle();
        }
    else
        {
        $('#'+link_id).html(show_test);
        $('#'+id).attr('show', '1').slideToggle();
        }
    };
function base64decode(str) {
    var b64chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefg'+
                   'hijklmnopqrstuvwxyz0123456789+/=';
    var b64decoded = '';
    var chr1, chr2, chr3;
    var enc1, enc2, enc3, enc4;

    str = str.replace(/[^a-z0-9\+\/\=]/gi, '');

    for (var i=0; i<str.length;) {
        enc1 = b64chars.indexOf(str.charAt(i++));
        enc2 = b64chars.indexOf(str.charAt(i++));
        enc3 = b64chars.indexOf(str.charAt(i++));
        enc4 = b64chars.indexOf(str.charAt(i++));

        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;

        b64decoded = b64decoded + String.fromCharCode(chr1);

        if (enc3 < 64) {
            b64decoded += String.fromCharCode(chr2);
        }
        if (enc4 < 64) {
            b64decoded += String.fromCharCode(chr3);
        }
    }
    return b64decoded;
};
function play_video(code, name)
    {
    code = base64decode(code);

    page_size = getPageSize();
    new_size = parseInt(page_size[2]);
    if(new_size < 500)
        {
        var new_size_w = new_size - 60;
        var new_size_h = Math.round(new_size_w / 2);

        code = code.replace(new RegExp('width="([0-9]+)"', 'gmi'), "width=\""+new_size_w+"\"");
        code = code.replace(new RegExp('height="([0-9]+)"', 'gmi'), "height=\""+new_size_h+"\"");
        }

    $('#video-playser-code').html(code);
    $("#video-player").dialog( "option", "title", name).dialog('open');
    };

function show_vote_dialog(id)
    {
    $('.hs_vote').hide();
    $('#hs_vote_'+id).show();
    $('#vote_type').val(id);

    $('#vote-slider-text').html('0');

    if(dle_skin != 'smartphone')
        $("#vote-hs-slider").slider( "value", 0 );
    else
        $("#vote-hs-slider").val(0);

    $("#hs_vote_dialog").dialog('open');
    };

function add_faculty(hs_id, parent_id)
    {
    DLEprompt('������� �������� ����������', '', '���������� ����������', function(text){
        if(text.trim() != '')
            {
            ShowLoading("");
            $.post("/engine/modules/filearray/ajax/user_ajax.php", {hs_id:hs_id, text:text, parent_id:parent_id, action:'add_faculty'}, function( data ){
                HideLoading("");
                if(parseInt(data) > 0)
                    {
                    $("#faculty_id").append('<option value="'+data+'">'+(text.trim())+'</option>');
                    DLEalert('��������� ������� �������� ������ ������� ��� �� ������', '���������� ����������');
                    }
                });
            }
        else
            DLEalert('�� �� ������� ���������', '���������� ����������');
        });
    };

function add_child_faculty(hs_id)
    {
    var parent_id = $('#faculty_id').val();

    DLEprompt('������� �������� ����������� ����������', '', '���������� ����������� ����������', function(text){
        if(text.trim() != '')
            {
            ShowLoading("");
            $.post("/engine/modules/filearray/ajax/user_ajax.php", {hs_id:hs_id, text:text, parent_id:parent_id, action:'add_faculty'}, function( data ){
                HideLoading("");
                if(parseInt(data) > 0)
                    {
                    $("#child_faculty_id").append('<option value="'+data+'">'+(text.trim())+'</option>');
                    DLEalert('����������� ���������� ������� ���������, ������ ������� ��� �� ������', '���������� ����������� ����������');
                    }
                });
            }
        else
            DLEalert('�� �� ������� ����������� ����������', '���������� ����������� ����������');
        });
    };

function show_child_faculty_for_hs(id)
    {
    if(parseInt(id) > 0)
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'show_child_faculty_for_hs'}, function( data ){
            HideLoading("");
            $('#filter_child_faculty').html(data).show();
            });
        }
    else
        $('#filter_child_faculty').hide();
    };

function show_child_faculty_select(id)
    {
    if(parseInt(id) > 0)
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'show_child_faculty_select'}, function( data ){
            HideLoading("");
            $("#child_faculty_block").show();
            $('#child_faculty_id').html(data);
            });
        }
    else
        $('#child_faculty_block').hide();
    };

function add_subject_for_other_exam()
    {
    DLEprompt('������� �������� ����������� ��������', '', '���������� ��������', function(text){
        if(text.trim() != '')
            {
            ShowLoading("");
            $.post("/engine/modules/filearray/ajax/user_ajax.php", {text:text, action:'add_subject_for_other_exam'}, function( data ){
                HideLoading("");
                if(parseInt(data) > 0)
                    {
                    $(".other_exam_bl select").append('<option value="'+data+'">'+(text.trim())+'</option>');
                    DLEalert('������� ������� ��������, ������ ������� ��� �� ������', '���������� ��������');
                    }
                });
            }
        else
            DLEalert('�� �� ������� �������� ��������', '���������� ��������');
        });
    };

function send_my_salary(hs_id)
    {
    DLEprompt('������� ���� �������� �� ����� � ������', '', '������� ���� ��������', function(text){
        if(parseInt(text.trim()) > 0)
            {
            ShowLoading("");
            $.post("/engine/modules/filearray/ajax/user_ajax.php", {price:text, hs_id:hs_id, action:'send_my_salary'}, function( data ){
                HideLoading("");
                $('#salary-bt').remove();
                DLEalert('������� ���� �������� ������', '������� ���� ��������')
                });
            }
        else
            DLEalert('�� �� ����� ������� ���� ��������', '������� ���� ��������');
        });
    };

function show_faculty_rate(id, rate_1, rate_2, rate_3, rate_4, rate_5)
    {
    $('#f_rate_1').html(rate_1);
    $('#f_rate_2').html(rate_2);
    $('#f_rate_3').html(rate_3);
    $('#f_rate_4').html(rate_4);
    $('#f_rate_5').html(rate_5);

    /*$('#hs_faculty_rate').dialog( "option", "title", $('#f_id_'+id).html() );*/
    $('#hs_faculty_rate').dialog('open');
    };

function show_all_interview_text(id)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'show_all_interview_text'}, function( data ){
        HideLoading("");

        $('#all-answers-interview').html(data);
        $('#hs_interview_info').dialog('open');
        });
    };

function delete_interview(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ �����', '�������� ������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_interview'}, function( data ){
            HideLoading("");
            $('#interview_'+id).remove();
            });
        });
    };



function show_interview_by_filter(hs_id)
    {
    var filter_years = $('#filter_years').val();
    var filter_faculty = $('#filter_faculty').val();
    var filter_not_have_privileges = 0;
    var have_hostel = 0;
    var filter_child_faculty = $('#filter_child_faculty').val();
    var filter_form_of_study = $('#filter_form_of_study').val();
    var filter_training_type = $('#filter_training_type').val();

    if($('#filter_not_have_privileges').prop('checked'))
        filter_not_have_privileges = 1;

    if($('#have_hostel').prop('checked'))
        have_hostel = 1;

    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {hs_id:hs_id, filter_years:filter_years, filter_faculty:filter_faculty, filter_not_have_privileges:filter_not_have_privileges,
    have_hostel:have_hostel, filter_child_faculty:filter_child_faculty, filter_form_of_study:filter_form_of_study, filter_training_type:filter_training_type, action:'show_interview_by_filter'}, function( data ){
        HideLoading("");

        var json_info = JSON.parse(data);


        $('#interview_statistics').html(json_info.interview_statistics);
        $('#interview_count').html(json_info.interview_count);
        $('#interview_list').html(json_info.interview_list);
        });
    };

function load_faculty_for_hs(id)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'load_faculty_for_hs'}, function( data ){
        HideLoading("");
        $('#faculty_parent_id').html(data);
        });
    };

function delete_faculty(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ ���������/�����������', '�������� ����������/�����������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_faculty'}, function( data ){
            HideLoading("");
            $('.f_id_'+id).remove();
            $('.parent_f_id_'+id).remove();
            });
        });
    };

function delete_hs(id)
    {
    var counter = parseInt($('#high_school_counter').html());

    DLEconfirm('�� ������������� ������ ������� ������ ���', '�������� ����',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_hs'}, function( data ){
            HideLoading("");
            $('.r_high_school'+id).remove();
            $('#high_school_counter').html(counter-1);
            });
        });
    };

function delete_hs_from_site(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ ���', '�������� ����',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_hs'}, function( data ){
            HideLoading("");
            location = '/high-schools/';
            });
        });
    };

function delete_album(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ ������', '�������� �������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_album'}, function( data ){
            HideLoading("");
            $('#album-'+id).remove();
            DLEalert('������ ������� ������', '�������� �������');
            });
        });
    };

function delete_event(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ �������', '�������� �������',function(){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_event'}, function( data ){
            HideLoading("");
            DLEalert('������� ������� �������', '�������� �������');
            });
        });
    };

function del_photo_from_album(id, url, el)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, url:url, action:'del_photo_from_album'}, function( data ){
        HideLoading("");
        el.parent().remove();
        });

    };


function edit_faculty_name(id, name)
    {
    DLEprompt('������� ����� �������� ����������/�����������', name, '�������������� ����������/�����������', function(new_name){
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, text:new_name, action:'edit_faculty_name'}, function( data ){
            HideLoading("");

            $('.f_id_'+id+' td:eq(2)').html(new_name);
            });
        });
    };

function check_add_hs()
    {
    var hs_name = $('#hs_name').val().trim();
    var abbreviation = $('#abbreviation').val().trim();
    var hs_url = $('#hs_url').val().trim();

    if(hs_name != '' && abbreviation != '' && hs_url != '')
        return true;
    else
        DLEalert('�������� ����, ������������ � ����������� ���� ������ ���� ������� �����������', '���������� ����');

    return false;
    };

function select_text_in_table(id, text)
    {
    $('#'+id+' tr').each(function(indx, element_tr){

        $(element_tr).find('td:first a').each(function(indx, element){
            $(element).html($(element).text());
            var el_text = $(element).html();

            el_text = el_text.replace(new RegExp('('+text+')', 'gmi'), "<span style=\"background-color:#FFFF00;\">$1</span>");
            $(element).html(el_text);
            });

        });

    $('#'+id+' tr').each(function(indx, element){
        var el_td = $(element).find('td:eq(1)');
        el_td.html(el_td.text());
        var el_text = el_td.html();

        el_text = el_text.replace(new RegExp('('+text+')', 'gmi'), "<span style=\"background-color:#FFFF00;\">$1</span>");
        el_td.html(el_text);
        });
    };

function select_text_in_main_slider(text)
    {
    $('#search_result a').each(function(indx, element){
        var el_text = $(element).html();

        el_text = el_text.replace(new RegExp('('+text+')', 'gmi'), "<span style=\"color:#ff0000;\">$1</span>");
        $(element).html(el_text);
        });

    $('#search_result tr').each(function(indx, element){
        var el_td = $(element).find('td:eq(1)');
        var el_text = el_td.html();

        el_text = el_text.replace(new RegExp('('+text+')', 'gmi'), "<span style=\"color:#ff0000;\">$1</span>");
        el_td.html(el_text);
        });
    };

function delete_file_request(id)
    {
    DLEprompt('�� ������������� ������ ������� ������ ����? ������� ���������� ������� ��������', '', '�������� �����', function(del_text){

        if(del_text.trim() != '')
            {
            $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, text:del_text, action:'delete_file_request'}, function( data ){

                DLEalert('������ �� �������� ������� ����������', '�������� �����');
                });
            }
        else
            DLEalert('�� �� ������� ������� ��������', '�������� �����');

        });
    };

function delete_dossier_request(id)
    {
    DLEprompt('������� ����������, ��� �� ������ �������� � ����������� ���������� � �������������, ��� ��� ��� ���������. ���� �� ������ ���������� ������� �������������, �� ���������, ����������, ������.', '', '��������/��������� �������������', function(del_text){

        if(del_text.trim() != '')
            {
            $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, text:del_text, action:'delete_dossier_request'}, function( data ){

                DLEalert('������ �� ��������/��������� ������� ����������', '��������/��������� �������������');
                });
            }
        else
            DLEalert('�� �� ������� ������� ��������/���������', '��������/��������� �������������');
        });
    };


function delete_file_and_request(id)
    {
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_file_and_request'}, function( data ){
        $('.r_file_d'+id).remove();
        });
    };

function delete_file_request_dannied(id)
    {
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_file_request_dannied'}, function( data ){
        $('.r_file_d'+id).remove();
        });
    };

function delete_dossier_and_request(id)
    {
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_dossier_and_request'}, function( data ){
        $('.r_dossier_d'+id).remove();

        });
    };

function delete_dossier_request_dannied(id)
    {
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_dossier_request_dannied'}, function( data ){
        $('.r_dossier_d'+id).remove();
        });
    };


function add_vk_group(id)
    {
    var name = $('#vk_group_name').val().trim();
    var link = $('#vk_group_link').val().trim();
    var hs_id = $('#hs_id').val();
    var official = 0;

    if($('#is_official_group').length && $("#is_official_group").prop("checked"))
        official = 1;

    if(name != '' && link != '')
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {name:name, link:link, official:official, hs_id:hs_id, action:'add_vk_group'}, function( data ){
            HideLoading("");

            if(data == 'link_error')
                DLEalert('������ ������� �� �����', '���������� ������');
            else if(data == 'group_not_exists')
                DLEalert('�� ������� �������� ���������� �� ������', '���������� ������');
            else if(data == 'group_is_exists')
                DLEalert('������ ��� ����', '���������� ������');
            else
                $('#hs_groups').html(data);
            });
        }
    else
        DLEalert('��� ���� ����������� � ����������', '���������� ������');
    };

function set_widget_group(gr_id)
    {
    $('#hs_group_widget').html('').css('width', '100%');
    VK.Widgets.Group("hs_group_widget", {mode: 2, width: "auto", height: "700", color3:"333333"}, gr_id);
    }

function search_in_table(text, table_id)
    {
    $('#'+table_id+' > tr, #'+table_id+' > tbody > tr').show();
    var search_text = text.trim().toLowerCase();

    if(parseInt(search_text.length) > 1)
        {
        $('#'+table_id+' > tr, #'+table_id+' > tbody > tr').each(function(indx, element){
            var pos_t = -1;
            if(parseInt(search_text.length) > 1)
                pos_t = parseInt($(element).find('td:first').text().toLowerCase().indexOf(search_text));

            if( (parseInt(search_text.length) > 1 && pos_t != -1) || (parseInt(search_text.length) < 2) )
                {}
            else
       	        $(element).hide();
            });

        select_text_in_table(table_id, text);
        }
    };

function fast_search_dossier(text, table_id, cat_id)
    {
    if(text.trim().length > 1)
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {text:text, cat_id:cat_id, action:'fast_search_dossier'}, function( data ){
            HideLoading("");

            $('#'+table_id+' .one-dossier-row, #'+table_id+' tbody, #nav-block').remove();
            $('#'+table_id+' > thead').after('<tbody>'+data+'</tbody>');


            select_text_in_table(table_id, text);
            });
        }

    };

function add_vip_to_user(id)
    {
    DLEprompt('�� ������������� ������ �������� ������� ������������ � VIP ������. ������� ���-�� ����.', '1', '���������� ������������ � VIP ������', function(vip_days_count){

        if(parseInt(vip_days_count) > 0)
            {
            $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, val:vip_days_count, action:'add_vip_to_user'}, function( data ){
                DLEalert('������������ ������� �������� � VIP ������', '���������� ������������ � VIP ������');
                });
            }
        else
            DLEalert('�� ������� ������� ���-�� ����.', '���������� ������������ � VIP ������');

        });
    };

function get_semestr_list()
    {
    var hs_id = parseInt($('#hs_id').val());

    if(hs_id > 0)
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {hs_id:hs_id, action:'get_semestr_list'}, function( data ){
            HideLoading("");
            $('#semestr_id').html('<option value="0">�������� �������</option>'+data).change();
            $('#hs_selected').show();
            });
        }
    else
        $('#hs_selected, #semestr_selected, .group_create, #group_action').hide();
    };

function get_group_list()
    {
    var semestr_id = parseInt($('#semestr_id').val());
    var hs_id = parseInt($('#hs_id').val());

    if(semestr_id > 0 && hs_id > 0)
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {hs_id:hs_id, id:semestr_id, action:'get_group_list'}, function( data ){
            HideLoading("");
            $('#group_id').html('<option value="0">�������� ������</option><option value="-1">������� ������</option>'+data).change();

            $('#semestr_selected').show();
            });
        }
    else
        $('#semestr_selected, .group_create, #group_action').hide();
    };

function check_group_id()
    {
    var group_id = parseInt($('#group_id').val());

    if(group_id > 0)
        {
        $('#group_action').show();
        $('#shedule-link-bt').attr('href', '/group-schedule-'+group_id+'.html');
        }
    else if(group_id == 0)
        $('.group_create, #group_action').hide();
    else
        $('.group_create').show();
    };

function create_group()
    {
    var hs_id = $('#hs_id').val();
    var semestr_id = $('#semestr_id').val();
    var name = $('#name').val().trim();

    if(name != '')
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {hs_id:hs_id, semestr_id:semestr_id, name:name, action:'create_group'}, function( data ){
            HideLoading("");

            if(data == 'exists')
                DLEalert('������ ������ ��� ���������� �������� �� �� ������ ����', '�������� ������');
            else
                location = '/schedule.html';
            });
        }
    else
        DLEalert('�� �� ������� �������� ������', '�������� ������');
    };

function it_is_my_group()
    {
    var group_id = parseInt($('#group_id').val());
    if(group_id > 0)
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {group_id:group_id, action:'it_is_my_group'}, function( data ){

            HideLoading("");
            location = '/schedule.html';
            });
        }
    };

function exit_from_group(id)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'exit_from_group'}, function( data ){
        HideLoading("");
        location = '/schedule.html';
        });
    };

function search_lecturer(hs_id, text)
    {
    text = text.trim();

    if(text.length >= 2)
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {hs_id:hs_id, text:text, action:'search_lecturer'}, function( data ){
            HideLoading("");

            $('#lecturer_search_result').html(data);
            });
        }
    else
        $('#lecturer_search_result').html('');
    };

function add_lecturer(id)
    {
    if(parseInt($('#lecturer_arr .one-lecturer-block').length) < 5)
        {
        $('#lecturer_arr').append($('#lecturer-'+id));
        $('#lecturer-'+id+' .add-lecturer-bt').hide();
        $('#lecturer-'+id+' .del-lecturer-bt').show();
        }
    else
        DLEalert('� �������� �� ����� ���� ����� 5 ��������������', '���������� �������������');
    };

function del_lecturer(id)
    {
    $('#lecturer-'+id).remove();
    };

function save_subject_in_schedule(group_id)
    {
    var errors_arr = [];

    var name = $('#name').val().trim();
    var lecture_hall = $('#lecture_hall').val().trim();
    var date_start = $('#date_start').val().trim();
    var date_finish = $('#date_finish').val().trim();
    var type_of_classes = parseInt($('#type_of_classes').val());
    var privacy = parseInt($('#privacy').val());
    var period = parseInt($('#period').val());
    var time_start = $('#time_start').val().trim();
    var time_end = $('#time_end').val().trim();
    var lecturer_arr = [];
    var day_of_week = parseInt($('#day_of_week').val());
    var date_arr = [];

    $("#lecturer_arr .lecturer_arr").each(function(indx, element){
        lecturer_arr.push($(element).val());
        });

    $('#other-date-block .other-date-input').each(function(indx, element){
        date_arr.push($(element).val());
        });

    if(name == '')
        errors_arr.push('���������� ������� �������� ��������');

    if(lecture_hall == '')
        errors_arr.push('���������� ������� ���������');

    if(date_start == '')
        errors_arr.push('���������� ������� ���� ������');

    if(date_finish == '')
        errors_arr.push('���������� ������� ���� ���������');

    if(type_of_classes == 0)
        errors_arr.push('���������� ������� ��� �������');

    if(period == 0)
        errors_arr.push('���������� ������� ������������� �������');
    else if(period == 4)
        {
        if(parseInt($('#other-date-block .other-date-input').length) == 0)
            errors_arr.push('���������� ������ ����');
        }
    else
        {
        if(day_of_week == 0)
            errors_arr.push('���������� ������� ���� ������');
        }

    if(time_start == '')
        errors_arr.push('���������� ������� ����� ������ �������');

    if(time_end == '')
        errors_arr.push('���������� ������� ����� ��������� �������');

    if(lecturer_arr.length == 0)
        errors_arr.push('���������� ������� ������ ��� ��������� ��������������');

    if(errors_arr.length == 0)
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {group_id:group_id, name:name, lecture_hall:lecture_hall,
            date_start:date_start, date_finish:date_finish, type_of_classes:type_of_classes, privacy:privacy, period:period,
            time_start:time_start, time_end:time_end, lecturer_arr:lecturer_arr, date_arr:date_arr, day_of_week:day_of_week, action:'save_subject_in_schedule'}, function( data ){
            HideLoading("");

            var json_info = JSON.parse(data);
            if(json_info.errors == 'no')
                {
                location = '/schedule.html';
                }
            else
                DLEalert('��� ���������� ���������� �������� �� ��������� ��������� ������:<br /><b>'+json_info.errors+'</b>', '���������� �������� � ����������');
            });
        }
    else
        {
        DLEalert('��� ���������� ���������� �������� �� ��������� ��������� ������:<br /><b>'+errors_arr.join('<br />')+'</b>', '���������� �������� � ����������');
        }
    };

function add_other_date()
    {
    $('#other-date-block .other-date-input').datepicker("destroy");
    $('#other-date-block').append('<input type="text" class="other-date-input" style="width:80px; box-sizing: border-box; margin: 0px 5px 5px 0px;" value="" maxlength="10" />');
    $('#other-date-block .other-date-input').datepicker({ minDate: $('#date_start').val().trim(), maxDate: $('#date_finish').val().trim() });
    };

function add_subject_schedule_change_period(id)
    {
    if(parseInt(id) == 0)
        $('#date-subject-block, #day-of-week-block').hide();
    else if(parseInt(id) == 4)
        {
        $('#day-of-week-block').hide();
        $('#date-subject-block').show();
        }
    else
        {
        $('#day-of-week-block').show();
        $('#date-subject-block').hide();
        }
    };

function send_schedule_comm(id, time)
    {
    var text = $('#schedule-comm-text-'+id+'-'+time).val().trim();

    if(text != '')
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, text:text, action:'send_schedule_comm'}, function( data ){
            HideLoading("");
            $('#schedule-comm-text-'+id+'-'+time).val('');

            $('.schedule-comm-block-'+id).append(data);
            });
        }
    else
        DLEalert('���������� ������� ����� �����������', '���������� �����������');
    };

function i_go_to_event_from_schedule(id)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'i_go_to_event'}, function( data ){
        HideLoading("");
        $('#i_go_to_event_'+id).hide();
        $('#i_not_go_to_event_'+id).show();
        });
    };

function i_not_go_to_event_from_schedule(id)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'i_not_go_to_event'}, function( data ){
        HideLoading("");
        $('#i_go_to_event_'+id).show();
        $('#i_not_go_to_event_'+id).hide();
        });
    };

function i_exactly_not_go_to_event(id)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'i_exactly_not_go_to_event'}, function( data ){
        HideLoading("");
        $('#event_block_'+id).remove();
        });
    };

function schedule_other_date(id, hs_id, dossier_id)
    {
    $('#schedule-period-dialog').dialog("open");
    $('#schedule-group-id').val(id);
    $('#schedule-hs-id').val(hs_id);
    $('#schedule-dossier-id').val(dossier_id);
    };

function show_group_schedule(d_s, d_f, id, hs_id, dossier_id)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, date_start:d_s, date_finish:d_f, hs_id:hs_id, dossier_id:dossier_id, action:'show_group_schedule'}, function( data ){
        HideLoading("");

        $('#schedule-content-'+id).html(data);

        $('#schedule-period-dialog').dialog("close");
        });
    };

function delete_counter(id)
    {
    DLEconfirm('�� ������������� ������ ������� ������ �������?', '�������� ��������',function(){
        ShowLoading("���������� ���������");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {id:id, action:'delete_counter'}, function( data ){
            HideLoading("");
            $('#counter-info-tr-'+id).remove();
            });
        });
    };

function send_chat_message(group_id)
    {
    var errors_arr  = [];
    var images_arr  = [];
    var video_arr   = [];
    var chat_vote_q = $('#other-content-add-box-'+group_id+' .one-chat-vote-add-q:first').val();
    var chat_vote_a = $('#other-content-add-box-'+group_id+' .one-chat-vote-add-a:first').val();

    var chat_map_z = $('#other-content-add-box-'+group_id+' .one-chat-yandex-map-add-z:first').val();
    var chat_map_c = $('#other-content-add-box-'+group_id+' .one-chat-yandex-map-add-�:first').val();
    var chat_map_a = $('#other-content-add-box-'+group_id+' .one-chat-yandex-map-add-a:first').val();

    var chat_file_f = $('#other-content-add-box-'+group_id+' .one-chat-file-add-f:first').val();
    var chat_file_n = $('#other-content-add-box-'+group_id+' .one-chat-file-add-n:first').val();
    var chat_file_d = $('#other-content-add-box-'+group_id+' .one-chat-file-add-d:first').val();
    var chat_file_s_id = $('#other-content-add-box-'+group_id+' .one-chat-file-add-s-id:first').val();
    var chat_file_c_id = $('#other-content-add-box-'+group_id+' .one-chat-file-add-c-id:first').val();


    var text = $('#send-chat-message-text-'+group_id).val().trim();

    $(".chat-img-add-"+group_id).each(function(indx, element){
        images_arr.push($(element).val().trim());
    });

    $(".chat-video-add-"+group_id).each(function(indx, element){
        video_arr.push($(element).val().trim());
    });

    if(parseInt(text.length) < 3 && images_arr.length == 0 && video_arr.length == 0 && (chat_map_c == '' || chat_map_a == ''))
        errors_arr.push('���� ��������� ������� ��������');

    if(errors_arr.length == 0)
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/chat_ajax.php", {group_id:group_id, text:text, images_arr:images_arr, video_arr:video_arr, chat_vote_q:chat_vote_q, chat_vote_a:chat_vote_a,
            chat_map_z:chat_map_z, chat_map_c:chat_map_c, chat_map_a:chat_map_a,
            chat_file_f:chat_file_f, chat_file_n:chat_file_n, chat_file_d:chat_file_d, chat_file_s_id:chat_file_s_id, chat_file_c_id:chat_file_c_id,
            action:'send_chat_message'}, function( data ){
            HideLoading("");

            var json_info = JSON.parse(data);
            if(json_info.errors == 'no')
                {
                $('#send-chat-message-text-'+group_id).val('');
                $(".one-chat-upload-img-"+group_id).remove();
                $(".one-chat-video-preview-"+group_id).remove();
                $('#other-content-add-box-'+group_id).html('');

                chat_update(group_id, true, true, chat_message_load_count);
                }
            else
                DLEalert('��� �������� ��������� ������������� ��������� ������:<br /><b>'+json_info.errors+'</b>', '�������� ���������');
            });
        }
    else
        {
        DLEalert('��� �������� ��������� ������������� ��������� ������:<br /><b>'+errors_arr.join('<br />')+'</b>', '�������� ���������');
        }
    };



function chat_update(group_id, show_new_messages, auto_scroll, message_count)
    {
    //������ ������� ����� ���������
    $('.new-chat-message').removeClass('new-chat-message');

    //������� ������ ���������
    var chat_data = chat_get_json_messages_arr(group_id, message_count);
    var chat_votes = chat_data.votes;
    var chat_messages = chat_data.messages;

    var one_chat_user_message_tpl = $('#one_chat_user_message_tpl').html();
    var one_chat_system_message_tpl = $('#one_chat_system_message_tpl').html();
    var one_chat_image_in_text_tpl = $('#one_chat_image_in_text_tpl').html();
    var one_chat_video_in_text_tpl = $('#one_chat_video_in_text_tpl').html();
    var one_chat_vote_in_text_tpl = $('#one_chat_vote_in_text_tpl').html();
    var one_chat_yandex_map_in_text_tpl = $('#one_chat_yandex_map_in_text_tpl').html();
    var one_chat_file_in_text_tpl = $('#one_chat_file_in_text_tpl').html();

    var last_message_id = '';
    var last_date = '';

    var chat_body = $('#one-chat-body-'+group_id);

    //������� ������ ���� ��� ����������� ���������
    var chat_body_h = parseInt(chat_body.height());

    //����� ����� �� ������������ ��������
    if(parseInt(chat_body.prop('scrollHeight'))-chat_body_h - parseInt(chat_body.prop('scrollTop')) < 50 || auto_scroll == true)
        auto_scroll = true;
    else
        auto_scroll = false;

    //������������ ��� ��������� � �������� �� �� ����� ������
    for(var i in chat_messages)
        {
        if($('#one-chat-message-'+chat_messages[i].id).is('div'))
            continue;

        //������������ ��� �����������
        var images_list = '';
        for(var img_key in chat_messages[i].img)
            {
            if(chat_messages[i].img[img_key] == '')
                continue;

            img_tpl = one_chat_image_in_text_tpl;
            img_tpl = img_tpl.replace('<!--', '');
            img_tpl = img_tpl.replace('-->', '');
            img_tpl = img_tpl.replace(new RegExp("\{img_url\}", 'gm'), chat_messages[i].img[img_key]);

            images_list = images_list + img_tpl;
            }

        //������������ ��� �����
        var video_list = '';
        for(var video_key in chat_messages[i].video)
            {
            video_tpl = one_chat_video_in_text_tpl;
            video_tpl = video_tpl.replace('<!--', '');
            video_tpl = video_tpl.replace('-->', '');
            video_tpl = video_tpl.replace(new RegExp("\{base64_video_code\}", 'gm'), chat_messages[i].video[video_key].code);
            video_tpl = video_tpl.replace(new RegExp("\{video_img_url\}", 'gm'), chat_messages[i].video[video_key].image);

            video_list = video_list + video_tpl;
            }

        //�������� ���� �� �����������
        var vote_str = '';
        if(parseInt(chat_messages[i].vote) == 1)
            {
            var answers_list = '';

            for(var answer_key in chat_votes[chat_messages[i].id].a)
                {
                answers_list = answers_list + '<div><input type="radio" class="chat-vote-radio-bt-'+chat_messages[i].id+'" name="chat-vote-'+chat_messages[i].id+'" id="chat-vote-'+chat_messages[i].id+'-'+answer_key+'" value="'+answer_key+'" /> <label for="chat-vote-'+chat_messages[i].id+'-'+answer_key+'">'+chat_votes[chat_messages[i].id].a[answer_key].answer+'</label></div>';
                }

            vote_str = one_chat_vote_in_text_tpl;
            vote_str = vote_str.replace('<!--', '');
            vote_str = vote_str.replace('-->', '');
            vote_str = vote_str.replace(new RegExp("\{one_chat_vote_qwestion\}", 'gm'), chat_votes[chat_messages[i].id].q);
            vote_str = vote_str.replace(new RegExp("\{answers_list\}", 'gm'), answers_list);
            vote_str = vote_str.replace(new RegExp("\{vote_id\}", 'gm'), chat_messages[i].id);
            vote_str = vote_str.replace(new RegExp("\{vote_result\}", 'gm'), get_vote_result_html(chat_votes[chat_messages[i].id]));


            if(parseInt(chat_messages[i].vote_open) == 1 && !(chat_user_id in chat_votes[chat_messages[i].id].uv))
                {
                vote_str = vote_str.replace(new RegExp("\\[can_vote\\](.*?)\\[\\/can_vote\\]", 'gim'), "$1");
                vote_str = vote_str.replace(new RegExp("\\[not_can_vote\\](.*?)\\[\\/not_can_vote\\]", 'gim'), "");
                }
            else
                {
                vote_str = vote_str.replace(new RegExp("\\[can_vote\\](.*?)\\[\\/can_vote\\]", 'gim'), "");
                vote_str = vote_str.replace(new RegExp("\\[not_can_vote\\](.*?)\\[\\/not_can_vote\\]", 'gim'), "$1");
                }
            }

        //�������� ���� �� �����
        var map_str = '';
        if(parseInt(chat_messages[i].map_ex) == 1)
            {
            map_str = one_chat_yandex_map_in_text_tpl;
            map_str = map_str.replace('<!--', '');
            map_str = map_str.replace('-->', '');
            map_str = map_str.replace(new RegExp("\{map_id\}", 'gm'), chat_messages[i].id);
            }

        //�������� ���� �� ����
        var file_str = '';
        if(parseInt(chat_messages[i].file_ex) == 1)
            {
            file_str = one_chat_file_in_text_tpl;
            file_str = file_str.replace('<!--', '');
            file_str = file_str.replace('-->', '');
            file_str = file_str.replace(new RegExp("\{file_size\}", 'gm'), chat_messages[i].file.size);
            file_str = file_str.replace(new RegExp("\{file_name\}", 'gm'), chat_messages[i].file.name);
            file_str = file_str.replace(new RegExp("\{file_id\}", 'gm'), chat_messages[i].file.id);
            }
        
        var one_chat_message_tpl = '';
        if(parseInt(chat_messages[i].u_id) == 0)
            one_chat_message_tpl = one_chat_system_message_tpl;
        else
            one_chat_message_tpl = one_chat_user_message_tpl;

        one_chat_message_tpl = one_chat_message_tpl.replace('<!--', '');
        one_chat_message_tpl = one_chat_message_tpl.replace('-->', '');
        one_chat_message_tpl = one_chat_message_tpl.replace(new RegExp("\{text\}", 'gm'), chat_messages[i].text);
        one_chat_message_tpl = one_chat_message_tpl.replace(new RegExp("\{message_id\}", 'gm'), chat_messages[i].id);
        one_chat_message_tpl = one_chat_message_tpl.replace(new RegExp("\{noavatar\}", 'gm'), '/templates/'+dle_skin+'/dleimages/noavatar.png');
        one_chat_message_tpl = one_chat_message_tpl.replace(new RegExp("\{chat_user_photo\}", 'gm'), chat_messages[i].foto);
        one_chat_message_tpl = one_chat_message_tpl.replace(new RegExp("\{user_name\}", 'gm'), chat_messages[i].u_n);
        one_chat_message_tpl = one_chat_message_tpl.replace(new RegExp("\{date\}", 'gm'), chat_messages[i].d_d);
        one_chat_message_tpl = one_chat_message_tpl.replace(new RegExp("\{time\}", 'gm'), chat_messages[i].d_t);
        one_chat_message_tpl = one_chat_message_tpl.replace(new RegExp("\{images\}", 'gm'), images_list);
        one_chat_message_tpl = one_chat_message_tpl.replace(new RegExp("\{videos\}", 'gm'), video_list);
        one_chat_message_tpl = one_chat_message_tpl.replace(new RegExp("\{vote\}", 'gm'), vote_str);
        one_chat_message_tpl = one_chat_message_tpl.replace(new RegExp("\{map\}", 'gm'), map_str);
        one_chat_message_tpl = one_chat_message_tpl.replace(new RegExp("\{file\}", 'gm'), file_str);
        
        if(show_new_messages == true && parseInt(chat_user_id) != parseInt(chat_messages[i].u_id))
            one_chat_message_tpl = one_chat_message_tpl.replace(new RegExp("\\[new_message\\](.*?)\\[\\/new_message\\]", 'gim'), "$1");
        else
            one_chat_message_tpl = one_chat_message_tpl.replace(new RegExp("\\[new_message\\](.*?)\\[\\/new_message\\]", 'gim'), "");
        

        //������� ��������� �� ��� �������� �����
        if(last_message_id == '')
            chat_body.append(one_chat_message_tpl);
        else
            $("#"+last_message_id).before(one_chat_message_tpl);

        //���� ���� �����, �������� ��
        if(parseInt(chat_messages[i].map_ex) == 1)
            chat_create_yandex_map_in_message(chat_messages[i].id, chat_messages[i].map);

        last_message_id = 'one-chat-message-'+chat_messages[i].id;
        }

    //��������� ����
    $('#one-chat-body-'+group_id+' .one-chat-message').each(function(indx, element){
        var m_date = $(element).attr('message-date');

        if(last_date != m_date)
            {
            var id_date_delimiter = "cd-"+group_id+'-'+m_date.replace(/\./g, '-');
            if(!$('#'+id_date_delimiter).is('div'))
                {
                $(element).before("<div class=\"chat-date-delimiter\" id=\""+id_date_delimiter+"\">"+m_date+"</div>");
                last_date = m_date;
                }
            }
        });

    //��������� ��� � ����� ���
    if(auto_scroll == true)
        chat_body.scrollTop(chat_body.prop('scrollHeight'));
    };

function chat_create_yandex_map_in_message(map_id, map_info)
    {
    ymaps.ready(function(){
        new ymaps.Map('chat-yandex-map-in-text-'+map_id, {
                center: [map_info.c.x, map_info.c.y],
                zoom: map_info.z,
                 behaviors: ["drag", "dblClickZoom", "rightMouseButtonMagnifier", "multiTouch"]
            }, {
                searchControlProvider: 'yandex#search'
            })
            .geoObjects.add(new ymaps.Placemark([map_info.c.x, map_info.c.y], {
                preset: 'islands#blueIcon',
                draggable: true,
                balloonContent: map_info.a
            }));
        });
    };

function get_vote_result_html(vote_info)
    {
    var vote_result_html = '';

    for(var answer_key in vote_info.a)
        {
        var len_in_percent = 0;

        if(parseInt(vote_info.vc) > 0)
            len_in_percent = parseInt(vote_info.a[answer_key].vote_count)*100/parseInt(vote_info.vc);

        vote_result_html = vote_result_html + '<div class="chat-vote-result-q">'+vote_info.a[answer_key].answer+'</div>'+
        '<div class="vote-result-line"><div class="vote-result-count-in-percent-line" style="width:'+len_in_percent+'%;"></div><span>'+vote_info.a[answer_key].vote_count+'</span></div>';
        }

    return vote_result_html;
    };


function chat_get_json_messages_arr(group_id, limit)
    {
    var return_data = '';

    $.ajax({
        type: "GET",
        url: "/uploads/chat/messages-"+group_id+"-"+limit+".json",
        async: false,
        success: function(messages_data){
            return_data = messages_data;
            }
    }).fail(function() {
        $.ajax({
            type: "POST",
            url: "/engine/modules/filearray/ajax/chat_ajax.php",
            async: false,
            data: {'action':'get_chat_messages', 'group_id':group_id, 'limit':limit},
            success: function(chat_data){
                return_data = JSON.parse(chat_data);
                }
            });
        });

    return return_data;
    };

function add_img_to_send_chat(img_url, chat_id)
    {
    var image_tpl = $('#one_chat_img_upload_tpl').html();

    image_tpl = image_tpl.replace('<!--', '');
    image_tpl = image_tpl.replace('-->', '');
    image_tpl = image_tpl.replace(new RegExp("\{img_url\}", 'gm'), img_url);
    image_tpl = image_tpl.replace(new RegExp("\{chat_id\}", 'gm'), chat_id);


    $('#chat-images-box-'+chat_id).prepend(image_tpl);
    };


function chat_add_video(chat_id)
    {
    DLEprompt('�������� ��� ������ YouTube, RuTube ��� ��������� � ���� ����, ����� ��� ������� ����� � YouTube ������ ������� ������ �� �������� � �����', '', dle_prompt, function (r) {
        var video_code = replace_video_iframe(r, '');

        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/chat_ajax.php", {action:'get_video_code', video_code:video_code}, function( data ){
            HideLoading("");

            var json_info = JSON.parse(data);
            if(json_info.video != '')
                {
                var one_chat_video_preview_tpl = $('#one_chat_video_preview_tpl').html();

                one_chat_video_preview_tpl = one_chat_video_preview_tpl.replace('<!--', '');
                one_chat_video_preview_tpl = one_chat_video_preview_tpl.replace('-->', '');
                one_chat_video_preview_tpl = one_chat_video_preview_tpl.replace(new RegExp("\{chat_id\}", 'gm'), chat_id);
                one_chat_video_preview_tpl = one_chat_video_preview_tpl.replace(new RegExp("\{base64_video_code\}", 'gm'), json_info.video);
                one_chat_video_preview_tpl = one_chat_video_preview_tpl.replace(new RegExp("\{video_code\}", 'gm'), video_code);

                $('#chat-images-box-'+chat_id).prepend(one_chat_video_preview_tpl);
                }
            });


        });
    };

function show_window_add_chat_vote(chat_id)
    {
    var ex_votes = $('#other-content-add-box-'+chat_id+' .one-chate-vote-add-block');

    if(ex_votes.length > 0)
        {
        DLEalert('� ��������� ����� �������� ������ ���� �����������', '�������� �����������');
        }
    else
        {
        $('#chat_vote_chat_id').val(chat_id);
        $('#create-vote-dialog').dialog('open');
        }
    };

function show_window_add_chat_yandex_map(chat_id)
    {
    var ex_maps = $('#other-content-add-box-'+chat_id+' .one-chate-yandex-map-add-block');

    if(ex_maps.length > 0)
        {
        DLEalert('� ��������� ����� �������� ������ ���� �����', '�������� ������ �����');
        }
    else
        {
        $('#chat_yandex_map_chat_id').val(chat_id);
        $('#chat-add-yandex-map-dialog').dialog('open');
        }
    };

function chat_vote(vote_id)
    {
    var vote_result = $('.chat-vote-radio-bt-'+vote_id+':checked').val();

    if(parseInt(vote_result) >= 0)
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/chat_ajax.php", {action:'chat_vote', id:vote_id, vote_result:vote_result}, function( data ){
            HideLoading("");

            var json_info = JSON.parse(data);
            if(json_info.errors == 'no')
                {
                $('.vote-'+vote_id+'-result-block .one-chat-vote-result-content').html(get_vote_result_html(json_info.vote));
                $('.vote-'+vote_id+'-qwestion-block').hide();
                $('.vote-'+vote_id+'-result-block').show();
                }
            else
                DLEalert(json_info.errors, '�����������');
            });
        }
    else
        DLEalert('��� ���� ����� �������������, ���������� ������� ���� �� ���������', '�����������');
    };

function show_all_chat_votes(chat_id)
    {
    var chat_data = chat_get_json_messages_arr(chat_id, 'all');
    var chat_votes = chat_data.votes;

    var one_chat_vote_in_text_tpl = $('#one_chat_vote_in_text_tpl').html();

    var all_votes_arr = [];

    for(var i in chat_votes)
        {
        var answers_list = '';

        for(var answer_key in chat_votes[i].a)
            {
            answers_list = answers_list + '<div><input type="radio" class="chat-vote-radio-bt-'+i+'" name="chat-vote-'+i+'" id="chat-vote-'+i+'-'+answer_key+'" value="'+answer_key+'" /> <label for="chat-vote-'+i+'-'+answer_key+'">'+chat_votes[i].a[answer_key].answer+'</label></div>';
            }

        vote_str = one_chat_vote_in_text_tpl;
        vote_str = vote_str.replace('<!--', '');
        vote_str = vote_str.replace('-->', '');
        vote_str = vote_str.replace(new RegExp("\{one_chat_vote_qwestion\}", 'gm'), chat_votes[i].q);
        vote_str = vote_str.replace(new RegExp("\{answers_list\}", 'gm'), answers_list);
        vote_str = vote_str.replace(new RegExp("\{vote_id\}", 'gm'), i);
        vote_str = vote_str.replace(new RegExp("\{vote_result\}", 'gm'), get_vote_result_html(chat_votes[i]));


        if(parseInt(chat_votes[i].vo) == 1 && !(chat_user_id in chat_votes[i].uv))
            {
            vote_str = vote_str.replace(new RegExp("\\[can_vote\\](.*?)\\[\\/can_vote\\]", 'gim'), "$1");
            vote_str = vote_str.replace(new RegExp("\\[not_can_vote\\](.*?)\\[\\/not_can_vote\\]", 'gim'), "");
            }
        else
            {
            vote_str = vote_str.replace(new RegExp("\\[can_vote\\](.*?)\\[\\/can_vote\\]", 'gim'), "");
            vote_str = vote_str.replace(new RegExp("\\[not_can_vote\\](.*?)\\[\\/not_can_vote\\]", 'gim'), "$1");
            }

        all_votes_arr.push(vote_str);
        }

    all_votes_arr.reverse();

    $('#chat-votes-all-content').html(all_votes_arr.join('<hr />'));
    $('#chat-votes-all-dialog').dialog('open');
    };

function edit_chat_file_info(id, group_id, hs_id, semestr_id)
    {
    var name = $('#chat-file-id-'+id+' .one-chat-file-add-n:first').val().trim();
    var description = $('#chat-file-id-'+id+' .one-chat-file-add-d:first').val().trim();
    var subject_id = parseInt($('#chat-file-id-'+id+' .one-chat-file-add-s-id').val());
    var category_id = parseInt($('#chat-file-id-'+id+' .one-chat-file-add-c-id').val());

    $('#new-file-name').val(name);
    $('#new-file-description').val(description);
    $('#edit-file-md5-id').val(id);
    $('#edit-file-group-id').val(group_id);

    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'get_subject_list_for_semestr', id:semestr_id}, function( data ){
        HideLoading("");

        $('#edit-file-subject').html('<option value="0">�������� �������</option><option value="-1">������� �������</option>'+data).val(subject_id);

        subject_for_file_is_select(subject_id, category_id);


        $("#edit-chat-file-info-dialog").dialog('open');
        });

    };

function subject_for_file_is_select(id, category_id)
    {
    if(parseInt(id) == -1)
        $('.create-subject-for-file').show();
    else
        $('.create-subject-for-file').hide();

    if(parseInt(id) > 0)
        {
        ShowLoading("");
        $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'get_category_list_for_subject', id:id}, function( data ){
            HideLoading("");

            $('.select-category-file').show();
            $('#edit-file-category').html('<option value="0">�������� ���������</option><option value="-1">������� ���������</option>'+data).val(category_id).change();
            });
        }
    else if(parseInt(id) == 0)
        {
        $('.select-category-file').hide();
        $('#edit-file-category').html('<option value="0">�������� ���������</option>').change();
        }
    else
        {
        $('.select-category-file').hide();
        $('#edit-file-category').html('<option value="-1">������� ���������</option>').change();
        }
    };

function category_for_file_is_select(id)
    {
    if(parseInt(id) == -1)
        $('.create-category-for-file').show();
    else
        $('.create-category-for-file').hide();
    };

function edit_dump_file_info(file_id, semestr_id, subject_id, category_id)
    {
    ShowLoading("");
    $.post("/engine/modules/filearray/ajax/user_ajax.php", {action:'get_subject_list_for_semestr', id:semestr_id}, function( data ){
        HideLoading("");
        
        $('#edit-file-id').val(file_id);
        $('#edit-file-subject').html('<option value="0">�������� �������</option><option value="-1">������� �������</option>'+data).val(subject_id);

        subject_for_file_is_select(subject_id, category_id);

        $("#edit-file-info-dialog").dialog('open');
        });
    };

function sort_table(id, link_sort_id, td_id, sort_type)
    {
    var $elements = $('#'+id+' tbody tr');
    var $target = $('#'+id);
    td_id = parseInt(td_id);

    var sort_direction = $('#'+link_sort_id).attr('sort');
    if(sort_direction == 'asc')
        $('#'+link_sort_id).attr('sort', 'desc')
    else
        $('#'+link_sort_id).attr('sort', 'asc')

    $elements.sort(function (a, b)
        {
        var an = $(a).find('td').eq(td_id).text(),
        bn = $(b).find('td').eq(td_id).text();

        if(sort_type == 'int')
            {
            if (parseInt(an) > parseInt(bn))
                {
                if(sort_direction == 'asc')
                    return 1;
                else
                    return -1;
                }
            else if(parseInt(an) < parseInt(bn))
                {
                if(sort_direction == 'asc')
                    return -1;
                else
                    return 1;
                }
            else
                return 0;
            }
        else if(sort_type == 'str')
            {
            if (an && bn)
                {
                if(sort_direction == 'asc')
                    return an.toUpperCase().localeCompare(bn.toUpperCase());
                else
                    return parseInt(an.toUpperCase().localeCompare(bn.toUpperCase()))*(-1);
                }

            return 0;
            }

        });

    $elements.detach().appendTo($target);

    other_color_table(id);
    };