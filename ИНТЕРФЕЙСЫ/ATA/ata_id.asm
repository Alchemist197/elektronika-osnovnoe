.486


stk segment stack use16
 db 256 dup (?)
stk ends

base equ 1f0h

data segment use16
 msg0 db "Disk capacity: ",'$'
 msg1 db " Sectors",'$'
 buffer dw 256 dup (0)
 string db 10  dup (?)
data ends

code segment use16
assume cs:code,ss:stk,ds:data

ByteToStr       PROC
                xor     ah,ah
WordToStr:      xor     dx,dx
DWordToStr:     push    si
                push    di
                mov     si,10
                xor     cx,cx
Next_Digit:     push    ax
                mov     ax,dx
                xor     dx,dx
                div     si
                mov     bx,ax
                pop     ax
                div     si
                push    dx
                mov     dx,bx
                inc     cx
                or      bx,ax
                jnz     Next_Digit
                cld
Store_Digit_Loop:
                pop     ax
                add     al,'0'
                stosb
                loop    Store_Digit_Loop
                mov     BYTE PTR es:[di],'$'
                pop     di
                pop     si
                ret
ByteToStr       ENDP

start:  ; ENTRY POINT

mov ax,data
mov ds,ax
mov es,ax

mov dx,base+7
m1:
in   al,dx
test al,80h
jnz  m1

mov dx,base+6
mov al,0E0h
out dx,al

mov  dx,base+7
m2:
in   al,dx
test al,80h
jnz  m2
test al,40h
jz   m2

mov dx,base+7
mov al,0ECh
out dx,al

mov  dx,base+206h
m3:
in   al,dx
test al,80h
jnz  m3

mov  dx,base+7
m4:
in   al,dx
test al,08h
jz   m4

cld
mov di,offset buffer
mov dx,base
mov cx,256
rep insw

mov  di,offset string
mov  ax,word ptr [buffer+60*2]
mov  dx,word ptr [buffer+61*2]
call DWordToStr


mov ah,9
mov dx,offset msg0
int 21h
mov dx,offset string
int 21h
mov dx,offset msg1
int 21h

exit:
mov ax,4c00h
int 21h
code ends
end start
