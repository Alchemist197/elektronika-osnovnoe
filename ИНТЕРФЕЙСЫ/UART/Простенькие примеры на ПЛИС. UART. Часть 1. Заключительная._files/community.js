$(document).ready(function() {

    $("#moderator_name_search").keyup(function() {

          var user_name = $(this).val();

          if(user_name.length>3) {
            $.ajax({
                type: "POST",
                url: "ajax/user_search.php",
                data: "user_name="+escape(user_name),
                dataType: "json",
                beforeSend: function() {
                  $("#mnsr").hide();
                  $("#mnsr_loading").show();
                },
                success: function(res){
                  if(res.user_id) {
                      $("#mnsr_user_id").val(res.user_id);
                      $("#mnsr_img").attr("src", res.avatar);
                      $("#mnsr_name").html(user_name);
                      $("#mnsr_rating").html(res.rating);
                      $("#mnsr_reg_ago").html(res.reg_ago);
                      $("#mnsr").slideDown();
                  }
                },
                complete: function(data) {
                  $("#mnsr_loading").hide();
                }
            });
          }

      });


      $("#add_moderator").click(function(e){
          e.preventDefault();

          cat_id = $("#cat_id").val();
          user_id = $("#mnsr_user_id").val();

            $.ajax({
                type: "POST",
                url: "ajax/community_moderators_manage.php",
                data: "mode=add&cat_id="+cat_id+"&user_id="+user_id,
                beforeSend: function() {
                    $("#add_moderator_loading").show();
                },
                success: function(res){
                  if(res==1) {
                        $("#cat_moderators2").append('<div class="minfo"><img src="'+$("#mnsr_img").attr("src")+'" style="padding: 5px; width: 30px; height: 30px; float: left;" />&nbsp;'+$("#mnsr_name").html()+'<a class="delete_moderator" href="" attr="'+user_id+'"><img src="images/bin_ico.gif" style="float: right; padding: 12px 12px 0 0;" /></a></div>');

                        moderators_count = Math.round($("#moderators_count").html())+1;
                        $("#moderators_count").html(moderators_count);

                        $("#moderator_name_search").val("");
                        $("#mnsr").hide();
                  }
                  else {
                    alert(res);
                  }
                },
                complete: function(data) {
                    $("#add_moderator_loading").hide();
                }
            });



      });


      $(document).on("click", ".delete_moderator", function(e){
          e.preventDefault();

          cat_id = $("#cat_id").val();
          user_id = $(this).attr("attr");

          f_out = $(this).parent();

            $.ajax({
                type: "POST",
                url: "ajax/community_moderators_manage.php",
                data: "mode=delete&cat_id="+cat_id+"&user_id="+user_id,
                beforeSend: function() {
                    $("#add_moderator_loading").show();
                },
                success: function(res){
                  if(res==1) {
                    moderators_count = Math.round($("#moderators_count").html())-1;
                    $("#moderators_count").html(moderators_count);

                    f_out.fadeOut();
                  }
                },
                complete: function(data) {
                    $("#add_moderator_loading").hide();
                }
            });



      });


      $(document).on("change", "#br_upl" ,function(event) {

             var loading_img = $("#br_upl_loading");


             $("#br_upl_form").ajaxSubmit({
                url: "/ajax_upload_br.php?cat_id="+$("#cat_id").val(),
                type: "POST",
                contestType: "multipart/form-data",
                dataType: "text",
                beforeSubmit: function()
                {
                    loading_img.show();
                },
                success: function(data)
                {
                     if(data) {
                        data += '?'+Math.random();
                        $('#br_upl_result').html("<div style=\"clear: both;\"><img src=\"/images/cats_brovastik/"+data+"\" /><br /><div ><a id=\"br_upl_img_remove\" class=\"dotted\" href=\"\" style=\"text-decoration: none;\"  attr=\""+$("#cat_id").val()+"\">�������</a></div></div>").fadeIn();
                     }

                },
                complete: function() {
                    loading_img.hide();
                }
             });
      });


      $(document).on("click", "#br_upl_img_remove" ,function(e) {
        e.preventDefault();
        $('#br_upl_result').fadeOut("normal", function() {$('#br_upl_result').html("")});
        $.ajax({url: "/ajax_upload_br.php?cat_id="+$("#cat_id").val()+"&mode=delete"});
      });



      $(".sub_btn a").click(function(e) {
        e.preventDefault();

        if(logined) {
            link_t = $(this);
            state = $(this).attr("attr");
            cat_id = $(this).attr("abbr");

            loading_img = $(this).parent().parent().children(".sub_loading");
            count_subs =  $(this).parent().parent().find(".count_subs");
            count_subs_v = Math.round(count_subs.html());


            $.ajax({
                type: "POST",
                url: site_url+"ajax/user_sub.php",
                data: "state="+state+"&cat_id="+cat_id,
                beforeSend: function() {
                  loading_img.show();
                },
                success: function(data){
                  if(data) {
                      if(state==0) {
                          link_t.html("<span class=\"dotted\">����������</span>").attr("attr", "1");
                          count_subs.html(count_subs_v+1);

                      }
                      else {
                          link_t.html("<img src=\""+site_url+"images/communities_btn_sub.gif\" />").attr("attr", "0"); ;
                          count_subs.html(count_subs_v-1);
                      }
                  }
                },
                complete: function(data) {
                  loading_img.hide();
                }
            });
        }
        else {
            $("#unreg_message").show();
            setTimeout(function() {$("#unreg_message").fadeOut();}, 2000);
        }


      });


});