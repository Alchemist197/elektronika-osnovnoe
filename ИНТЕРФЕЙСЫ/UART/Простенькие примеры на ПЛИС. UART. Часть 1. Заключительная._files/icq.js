var story_id_g;
function show_icq(story_id)
{
    if(!logined)
    {
        $('#over_login').show();
    }
    story_id_g = story_id;
    input_id = 'icq_input_'+story_id;
    $('#icq_story_id').val(story_id);
    if($('#'+input_id).html()!=''&&$('#icq_form').css('display')!='none')
    {
        hide_icq();
    }
    $('#icq_form').appendTo('#'+input_id);
    $('#icq_form').slideDown();
    return false;
}

function show_login_icq()
{
    $('#over').show();
}

function hide_icq()
{
    $('#icq_form').slideUp();
    return false;
}

function send_icq()
{
    $('#icq_form').ajaxSubmit({
       type: 'POST',
       dataType: 'json',
       beforeSubmit: function(mas,mun,op)
       {
           $('#icq_loading').show();
           $('#icq_send_b').attr('disabled','disabled');
       },
       success: function(data,succ,ob)
       {
    	   $('#icq_loading').hide();     
           $('#icq_send_b').removeAttr('disabled');
           if(data.status==0)
           {
               alert(data.message);
           }else
           {
               $('#icq_success_'+story_id_g).show();
               if(phpSelf=='/view_page.php')
               {
                   $('#login_f').hide();
               }
               else
            	   hide_icq();
           }
       },
       error:function(a,b,c)
       {
           alert(b);
       }
       
       
    });
    return false;
}

function refresh_captcha(starturl)
{
    $('#captcha').attr('src',starturl+'?'+Math.floor(Math.random()*1000));
}