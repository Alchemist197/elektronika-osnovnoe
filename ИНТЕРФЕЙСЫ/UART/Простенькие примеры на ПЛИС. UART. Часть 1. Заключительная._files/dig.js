var lastDig = [];
lastDig['ztype'] = '';
lastDig['num'] = '';
function clear_temp_dig()
{
	$.ajax({
		url: "/ajax/temp_digs.php", 
		data: {
			act: 'clear'
		}, 
		type: "GET", 
		dataType: "html", 
		success: function(){}
	});
}
function save_temp_dig(id, vote, type)
{ 
	if(id > 0)
	{
		if(type == 'dig')
		{
			$.ajax({
				url: "/ajax/temp_digs.php", 
				data: { 
					act:  'push',
					id:   id,
					vote: vote,
					type: type
				}, 
				type: "GET", 
				dataType: "html",
				success: function(){}			
			});
		}
		else
		{
			$.ajax({
				url: "/ajax/temp_digs.php", 
				data: { 
					act:  'push',
					id:   id,
					vote: vote['vote'],					
					story: vote['story'],
					dir: vote['dir'],					
					type: type
				}, 
				type: "GET", 
				dataType: "html",
				success: function(){}			
			});
		}
	}
}


var dig_attempts = new Array();
var up_active = new Array();  ;
var down_active = new Array();  ;
var baloon_rate_open = false;

function my_dig(Ztype,id)
{  
    if(!logined)
    {
        if(phpSelf=='view_page.php')
        {
            $('#login_f').toggle();
        }
        else
        {
            t = $('#up_b_'+id).offset().top + 70;
            l = $('#up_b_'+id).offset().left - 25;


            $("#unreg_message").show();
            setTimeout(function() {$("#unreg_message").fadeOut();}, 2000);

            //$("#baloon_rate").css("top", t+"px").css("left", l+"px").show();
            //baloon_rate_open = true;

        	//show_icq(0);
        }
        return;
    }




    var do_vote = false;
    if(!dig_attempts[id]) {
      dig_attempts[id] = 1;
    }

    if(dig_attempts[id]==1) {
          up_active[id] = $('#up_b_'+id).hasClass('up_active');
          down_active[id] = $('#down_b_'+id).hasClass('down_active');
    }
    dig_attempts[id]++;
    //alert(dig_attempts[id]);


    if( Ztype=='-')
    {


      	str = Math.round($('#num_digs'+id).html()) - 1;
        if(!str) str = '0';

       if(up_active[id])
       {
           do_vote = true;
    	   $('#up_b_'+id).removeClass('up_active').addClass('up');
           $('#pb_up_b_'+id).removeClass('com_up_active').addClass('com_up');
           $('#num_digs'+id).html(str);
           $('#dot_digs'+id+' img').attr("src", "http://s.pikabu.ru/images/dot.png");
           up_active[id] = false;
       }
       else if(!down_active[id])
       {
           do_vote = true;
           $('#down_b_'+id).removeClass('down').addClass('down_active');
           $('#pb_down_b_'+id).removeClass('com_down').addClass('com_down_active');
           $('#num_digs'+id).html(str);
           $('#dot_digs'+id+' img').attr("src", "http://s.pikabu.ru/images/dot-orange.png");
           down_active[id] = true;
       }       
    }
    else
    {
      	str = Math.round($('#num_digs'+id).html()) + 1;
        if(!str) str = '0';

       if(down_active[id])
       {
          do_vote = true;
     	  $('#down_b_'+id).removeClass('down_active').addClass('down');
          $('#pb_down_b_'+id).removeClass('com_down_active').addClass('com_down');
          $('#num_digs'+id).html(str);
          $('#dot_digs'+id+' img').attr("src", "http://s.pikabu.ru/images/dot.png");
          down_active[id] = false;
       }
       else if(!up_active[id])
       {
           do_vote = true;
           $('#up_b_'+id).removeClass('up').addClass('up_active');
           $('#pb_up_b_'+id).removeClass('com_up').addClass('com_up_active');
           $('#num_digs'+id).html(str);
           $('#dot_digs'+id+' img').attr("src", "http://s.pikabu.ru/images/dot-green.png"); 
           up_active[id] = true;
       } 
    }




    if(do_vote) {

    	 $.ajax({
    	   url: '/ajax/dig.php',
    	   data:{
    	       i: id,
    	       type: Ztype
    	       },
    	   type: 'POST',
    	   dataType: "json",
    	    success: function(data,code,n)
    	    {
                //alert(data.digs);

/*  wtf?
    	        if(data.status==='0')
{
    //location.href = '/login.php';
    var cur_msg = $("#unreg_message")[0].innerHTML;
    $("#unreg_message")[0].innerHTML = '��������� ������. ��� ����������� ���������� �������� ��������';
    $("#unreg_message").show();
    setTimeout(function() {$("#unreg_message").fadeOut();}, 2000);
    setTimeout(function() {$("#unreg_message")[0].innerHTML = cur_msg; }, 4000);                                    
    return;
}
*/

                if( (Ztype=='-')&&data.status==3)
                {
                    $('#up_b_'+id).removeClass('up_active').addClass('up');
                    $('#down_b_'+id).addClass('down_active');

                    $('#pb_up_b_'+id).removeClass('com_up_active').addClass('up');
                    $('#pb_down_b_'+id).addClass('com_down_active');
                }
                else if(data.status==5)
                {
                    $('#down_b_'+id).removeClass('down_active').addClass('down');
                    $('#up_b_'+id).addClass('up_active');

                    $('#pb_down_b_'+id).removeClass('com_down_active').addClass('down');
                    $('#pb_up_b_'+id).addClass('com_up_active');
                }
                else
                {
                   $('#down_b_'+id).removeClass('down_active').addClass('down');
                   $('#up_b_'+id).removeClass('up_active').addClass('up');

                   $('#pb_down_b_'+id).removeClass('com_down_active').addClass('down');
                   $('#pb_up_b_'+id).removeClass('com_up_active').addClass('up');
                }
            }
            //$('#num_digs'+id).html(data.digs);


    	 });
    }
 
}

function add_podpiska(cat_id,img_id)
{
 if(!logined)
 {
     show_icq();
     return;
 }   
 $.ajax({
   url: '/ajax/podpiska.php',
   data:{
       i: cat_id
   },
   type: 'GET',
   dataType: "json",  
    success: function(data,code)
    {                                    
        if(data.status=='0')
            alert('���������� �����������. ���������� � ��������������');
        else if(data.status=='1')
        {
            
            $('#a_'+cat_id).attr('my_title','���������� �� ��������');    
            $('#'+img_id).removeClass('add_rem_add').addClass('add_rem_delete');
              
            
        }
        else if(data.status=='2')
        {
            
            $('#a_'+cat_id).attr('my_title','�����������');
            
            $('#'+img_id).removeClass('add_rem_delete').addClass('add_rem_add');  
        }
    }
     
 });    
}

/*
$(function(){
 k = Get_Cookie('last_click'); 
  if(phpSelf!='story.php')
  {
  $('table[rel=table]').each(function()
  {
  
      if(k==$(this).attr('lang'))
        {
             
            $('table[rel=table]').removeClass('table_dashed');
            $(this).addClass('table_dashed');
            Set_Cookie('last_click','');
        }
  });  
  }
  else Set_Cookie('last_click','');
  
  $('a[rel=story_url]').each(function()
  {
     this.onclick = function()
     {
         Set_Cookie('last_click',$(this).attr('href')); 
         id = $(this).attr('id');                           
         location.href = '/view_page.php?story_id='+id.substr(8,20);
         return false;
     } 
  });
});
*/

function Set_Cookie( name, value)  
{
var today = new Date();
today.setTime( today.getTime() );
expires = 10 * 1000 * 60 * 60 * 24;
var expires_date = new Date( today.getTime() + (expires) );
document.cookie = name + "=" +escape( value ) +
( ( expires ) ? ";expires=" + expires_date.toGMTString() : "" );
}
 
function Get_Cookie( name ) {
var start = document.cookie.indexOf( name + "=" );
var len = start + name.length + 1;
if ( ( !start ) &&
( name != document.cookie.substring( 0, name.length ) ) )
{return null;}
if ( start == -1 ) return null;
var end = document.cookie.indexOf( ";", len );
if ( end == -1 ) end = document.cookie.length;
return unescape( document.cookie.substring( len, end ) );
} 

function windowHeight() {
var de = document.documentElement;

return self.innerHeight || ( de && de.clientHeight ) || document.body.clientHeight;
}

// ����������� ������ ������� ����� ��������
function windowWidth() {
var de = document.documentElement;

return self.innerWidth || ( de && de.clientWidth ) || document.body.clientWidth;
} 