////////////////////////////////////////////////////////////////////////////////
// Optical mouse interface for grabbing images from an MCS-12085 sensor
// Copyright (C) 2014 Roger Rowland. All rights reserved.
//
// Based on sketches by Martijn The and Benoit Rousseau.
////////////////////////////////////////////////////////////////////////////////

#include "MouseInterface.h"

////////////////////////////////////////////////////////////////////////////////
// CMouseInterface implementation
////////////////////////////////////////////////////////////////////////////////

CMouseInterface::CMouseInterface(int nClockPin, int nIOPin) :
  m_nClockPin(nClockPin), m_nIOPin(nIOPin)
{
  // initialising constructor - set initial pin modes
  pinMode(m_nClockPin, OUTPUT);
  pinMode(m_nIOPin, INPUT);
}

void CMouseInterface::SetWakeMode(bool bForceAwake) const
{
  // true = force awake, false = normal: configuration
  // address is 0x00, LSB is set to 1 for forced awake
  Write(0x00, bForceAwake ? 0x01 : 0x00);
}

int CMouseInterface::Grab()
{
  // grab frame and return success/fail
  int nResult = GRAB_OK;
  
  // to begin grabbing we write anything to register 0x08
  // to reset the pixel hardware so the next read is the
  // first pixel (see datasheet)
  Write(0x08, 0x00);
  
  // now read pixels one at a time
  int nNumPixels = Width() * Height(), nIndex = 0, nRetries = 0;
  while (nIndex < nNumPixels)
  {
    // read a pixel
    byte nPixel = Read(0x08);
    
    // check for data valid
    if (!(nPixel & 0x40))
    {
      // bad read - check retries
      if (++nRetries > 10)
      {
        // too many retires - abort
        nResult = GRAB_BAD_DATA;
        break;
      }
    }
    else
    {
      // good read, so reset retries
      nRetries = 0;
    
      // if this is the first pixel, check SOF
      if (nIndex == 0 && !(nPixel & 0x80))
      {
        // missing SOF - abort
        nResult = GRAB_BAD_START;
        break;
      }
    
      // only 6 bits are used for data (0..63)
      m_pFrame[nIndex++] = nPixel & 0x3F;
    }
  }
  
  // return success/fail
  return nResult;
}

void CMouseInterface::Write(byte nAddress, byte nData) const
{
  // write data to given address - MSB needs to be set
  // to 1 to indicate a write operation (see datasheet)
  nAddress |= 0x80;

  // prepare io pin for writing
  pinMode(m_nIOPin, OUTPUT);
  
  // write address followed by data
  Write(nAddress);
  Write(nData);
}

void CMouseInterface::Write(byte nData) const
{
  // write a byte to SDIO (assumes OUTPUT mode)
  for (int i = 7; i >= 0; --i)
  {
    // tick, write, tock
    digitalWrite(m_nClockPin, LOW);
    digitalWrite(m_nIOPin, nData & (1 << i));
    digitalWrite(m_nClockPin, HIGH);
  }
}

byte CMouseInterface::Read(byte nAddress) const
{
  // read byte from given address - ensure MSB is set
  // to 0 to indicate a read operation (see datasheet)
  nAddress &= 0x7F;

  // prepare io pin for writing
  pinMode(m_nIOPin, OUTPUT);

  // write the address that we want to read
  Write(nAddress);

  // prepare io pin for reading
  pinMode(m_nIOPin, INPUT);
  
  // wait to ensure MCS12085 has a response
  // (must be at least 100us - see datasheet)
  delayMicroseconds(100);
  
  // read the data
  byte nData = 0;
  for (int i = 7; i >= 0; --i)
  {
    // tick, tock, read
    digitalWrite(m_nClockPin, LOW);
    digitalWrite(m_nClockPin, HIGH);
    nData |= (digitalRead(m_nIOPin) << i);
  }
  
  // another delay is required before next io
  // (must be at least 100us - see datasheet)
  delayMicroseconds(100);
  
  // return the data
  return nData;
}

