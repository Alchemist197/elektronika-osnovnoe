////////////////////////////////////////////////////////////////////////////////
// Optical mouse interface for grabbing images from an MCS-12085 sensor
// Copyright (C) 2014 Roger Rowland. All rights reserved.
//
// Based on sketches by Martijn The and Benoit Rousseau.
////////////////////////////////////////////////////////////////////////////////

#include "MouseInterface.h"

// define digital io pins based on physical connection to Arduino
#define SCK_PIN    4
#define SDIO_PIN   7

// helper function prototypes
void SendRawFrame();                                                           // grab frame and write to serial

// create a global mouse interface instance
CMouseInterface g_pMouse(SCK_PIN, SDIO_PIN);

// true when streaming mouse pixel data to serial
bool g_bStreaming = false;

////////////////////////////////////////////////////////////////////////////////
// initialisation
////////////////////////////////////////////////////////////////////////////////

void setup()
{
  // initialise serial comms for PC control
  Serial.begin(115200);
}

////////////////////////////////////////////////////////////////////////////////
// run loop
////////////////////////////////////////////////////////////////////////////////

void loop()
{
  // if we have some incoming serial data ...
  if (Serial.available() > 0)
  {
    // ... read the oldest byte from the buffer
    byte nCommand = Serial.read();
    
    // "S" starts streaming, "Q" stops it
    if (nCommand == 'S' && !g_bStreaming)
    {
      // start streaming - so force awake now
      g_pMouse.SetWakeMode(true);
      g_bStreaming = true;
    }
    else if (nCommand == 'Q' && g_bStreaming)
    {
      // stop streaming - so reset wake mode
      g_pMouse.SetWakeMode(false);
      g_bStreaming = false;
    }
  }
  
  // if we're streaming ...
  if (g_bStreaming)
  {
    // ... send another frame
    SendRawFrame();
  }
}

////////////////////////////////////////////////////////////////////////////////
// helper functions
////////////////////////////////////////////////////////////////////////////////

void SendRawFrame()
{
  // grab frame and write to serial
  static const byte nDelimiter = 255;
  
  // grab a frame (skip errors)
  if (g_pMouse.Grab() == GRAB_OK)
  {
    // send frame in raw format
    Serial.write(g_pMouse.Frame(), g_pMouse.Width() * g_pMouse.Height());
  
    // frame data is 0..63 so delimiter 255 is safe
    Serial.write(&nDelimiter, 1);
  }
}

