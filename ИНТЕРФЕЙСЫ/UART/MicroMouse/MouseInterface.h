////////////////////////////////////////////////////////////////////////////////
// Optical mouse interface for grabbing images from an MCS-12085 sensor
// Copyright (C) 2014 Roger Rowland. All rights reserved.
//
// Based on sketches by Martijn The and Benoit Rousseau.
////////////////////////////////////////////////////////////////////////////////

#ifndef MOUSEINTERFACE_H
#define MOUSEINTERFACE_H

// include dependencies
#include <arduino.h>

// definitions of pixel array for MCS-12085 (amend for other sensors)
#define ARRAY_WIDTH    18
#define ARRAY_HEIGHT   18

// return codes from GrabFrame()
#define GRAB_OK              0                                                  // frame grab successful
#define GRAB_BAD_START       1                                                  // first pixel has no SOF
#define GRAB_BAD_DATA        2                                                  // data may be invalid

////////////////////////////////////////////////////////////////////////////////
// CMouseInterface declaration
////////////////////////////////////////////////////////////////////////////////

class CMouseInterface
{
public:
  // construction
  CMouseInterface(int nClockPin, int nIOPin);                                    // initialising constructor

public:
  // public interface
  void SetWakeMode(bool bForceAwake) const;                                      // true = force awake, false = normal
  int Width() const { return ARRAY_WIDTH; }                                      // return pixel array width
  int Height() const { return ARRAY_HEIGHT; }                                    // return pixel array height
  byte* Frame() { return m_pFrame; }                                             // return pointer to frame
  int Grab();                                                                    // grab frame and return success/fail
  
private:
  // private functions
  void Write(byte nAddress, byte nData) const;                                   // write data to given address
  void Write(byte nData) const;                                                  // write a byte to SDIO
  byte Read(byte nAddress) const;                                                // read byte from given address
  
private:
  // private data
  int m_nClockPin;                                                               // digital pin for SCK
  int m_nIOPin;                                                                  // digital pin for SDIO
  byte m_pFrame[ARRAY_WIDTH * ARRAY_HEIGHT];                                     // cached grabbed frame
};

#endif

