////////////////////////////////////////////////////////////////////////////////
// Optical mouse interface for grabbing images from an MCS-12085 sensor
// Copyright (C) 2014 Roger Rowland. All rights reserved.
//
// Based on sketches by Martijn The and Benoit Rousseau.
////////////////////////////////////////////////////////////////////////////////

#include "MouseInterface.h"

// define digital io pins based on physical connection to Arduino
#define SCK_PIN    4
#define SDIO_PIN   7

// helper function prototypes
void DumpFrame();                                                              // grab frame and dump to serial
void SendFrame();                                                              // grab frame and write to serial

// create a global mouse interface instance
CMouseInterface g_pMouse(SCK_PIN, SDIO_PIN);

////////////////////////////////////////////////////////////////////////////////
// initialisation
////////////////////////////////////////////////////////////////////////////////

void setup()
{
  // initialise serial comms for PC control
  Serial.begin(38400);
}

////////////////////////////////////////////////////////////////////////////////
// run loop
////////////////////////////////////////////////////////////////////////////////

void loop()
{
  // if we have some incoming serial data ...
  if (Serial.available() > 0)
  {
    // ... read the oldest byte from the buffer
    byte nCommand = Serial.read();
    
    // process the command - uppercase for debugging,
    // lowercase for interop with PC application
    switch (nCommand)
    {
    case 'W':
      // force mouse to be always awake
      g_pMouse.SetWakeMode(true);
      Serial.println("Forced awake mode is ON");
      break;
    case 'S':
      // set wake back to normal
      g_pMouse.SetWakeMode(false);
      Serial.println("Forced awake mode is OFF");
      break;
    case 'G':
      // grab a frame and dump it to serial
      Serial.println("Starting frame grab:");
      DumpFrame();
      break;
    }
  }
}

////////////////////////////////////////////////////////////////////////////////
// helper functions
////////////////////////////////////////////////////////////////////////////////

void DumpFrame()
{
  // grab frame and dump to serial
  int nIndex = 0;
  
  // grab a frame
  int nResult = g_pMouse.Grab();
  
  // check for errors
  if (nResult == GRAB_OK)
  {
    // ok - dump one row at a time
    byte *pFrame = g_pMouse.Frame();
    for (int y = 0; y < g_pMouse.Height(); ++y)
    {
      // dump one row
      for (int x = 0; x < g_pMouse.Width(); ++x)
      {
        // format nicely
        if (pFrame[nIndex] < 10) Serial.print("0");
        Serial.print(pFrame[nIndex++]);
        Serial.print(" ");
      }
    
      // next row
      Serial.println(" ");
    }
  }
  else if (nResult == GRAB_BAD_START)
  {
    // no SOF found
    Serial.println("Frame grab failed : no SOF found!");
  }
  else
  {
    // bad frame
    Serial.println("Frame grab failed : too many retries!");
  }
}

