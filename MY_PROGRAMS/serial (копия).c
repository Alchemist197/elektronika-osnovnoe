#include <stdio.h>   /* Стандартные объявления ввода/вывода */
#include <string.h>  /* Объявления строковых функций */
#include <unistd.h>  /* Объявления стандартных функций UNIX */
#include <fcntl.h>   /* Объявления управления файлами */
#include <errno.h>   /* Объявления кодов ошибок */
#include <termios.h> /* Объявления управления POSIX-терминалом */
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/io.h>

#define PORT 0x3f8	//SERIAL0

#define TIME 1

//SERIAL 			//адрес 	DLAB	чтение/запись	Название регистра
#define SER_THR (PORT + 0x000)		//00h	0	WR	THR(Transmit Holding Register)-регистр данных ожидающих передачи
#define SER_RBR (PORT + 0x000)		//00h	0	RD	RBR(Receiver Buffer Register)- буферный регистр приемника
#define SER_DLL (PORT + 0x000)		//00h	1	RD/WR	DLL(Divisor Latch LSB)-младший байт делителя частоты
#define SER_DIM (PORT + 0x001)		//01h	1	RD/WR	DIM(Divisor Latch MSB)-старший байт делителя частоты
#define SER_IER (PORT + 0x001)		//01h	0	RD/WR	IER(Interrupt Enable Register)-регистр разрешения прерывания
#define SER_IIR (PORT + 0x002)		//02h	х	RD	IIR(Interrupt Identification Register)-регистр идентифицирующий прерывания
#define SER_FRC (PORT + 0x002)		//02h	х	WR	FCR(FIFO Control Register)-регистр управления режимом FIFO
#define SER_LCR (PORT + 0x003)		//03h	x	RD/WR	LCR(Line Control Register)-регистр управления линией связи
#define SER_MCR (PORT + 0x004)		//04h	x	RD/WR	MCR(Modem Control Register)-регистр управления модемом
#define SER_LSR (PORT + 0x005)		//05h	x	RD/WR	LSR(Line Status Register)-регистр состояния линии связи
#define SER_MSR (PORT + 0x006)		//06h	x	RD/WR	MSR(Modem Status Register)-регистр состояния модема
#define SER_SCR (PORT + 0x007)		//07h	x	RD/WR	SCR(Scratch Pad Register)-регистр временного хранения 

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0') 
//printf("Leading text "BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(byte));
//printf("m: "BYTE_TO_BINARY_PATTERN" "BYTE_TO_BINARY_PATTERN"\n",  BYTE_TO_BINARY(m>>8), BYTE_TO_BINARY(m));


//Print the least significant bit and shift it out on the right. Doing this until the integer becomes zero prints the binary representation without leading zeros but in reversed order. Using recursion, the order can be corrected quite easily.
void print_binary(int number)
{
    if (number) {
        print_binary(number >> 1);
        putc((number & 1) ? '1' : '0', stdout);
    }
} 


//	As itoa() is indeed non-standard, as mentioned by several helpful commenters, 
//	it is best to use 
//	sprintf(target_string,"%d",source_int)
//	or (better yet, because it's safe from buffer overflows) 
//	snprintf(target_string, size_of_target_string_in_bytes, "%d", source_int). 
//	I know it's not quite as concise or cool as itoa(), but at least you can Write Once, Run Everywhere (tm) ;-)

	/* itoa:  конвертируем n в символы в s */
	void itoa(int n, char s[])
	{
		int i, sign;
		
		if ((sign = n) < 0)  /* записываем знак */
			n = -n;          /* делаем n положительным числом */
			
		i = 0;
		do {       /* генерируем цифры в обратном порядке */
		s[i++] = n % 10 + '0';   /* берем следующую цифру */
		} while ((n /= 10) > 0);     /* удаляем */
		
		if (sign < 0)
			s[i++] = '-';
		s[i] = '\0';
		reverse(s);
	}

//Функция reverse реализована двумя страницами ранее:

	#include <string.h>
 
	/* reverse:  переворачиваем строку s на месте */
	void reverse(char s[])
	{
		int i, j;
		char c;
		
		for (i = 0, j = strlen(s)-1; i<j; i++, j--) {
			c = s[i];
			s[i] = s[j];
			s[j] = c;
		}
	}



int main(int argc, char* argv[]) { 
	
	unsigned char data = 0x00000000;
	int timesleep = 0;
	if (argc != 3) {
		printf("Usage: %s 0x<value> time_usleep\n", argv[0]);
//		printf("Usage: %s 1(serial)-0(once) directive(1,2,3) time(ns)\n", argv[0]);
		return 1;
	} else {
		//unsigned int port;
		//unsigned int value;
		//sscanf(argv[1], "%x", &port);
		//sscanf(argv[2], "%x", &value);
		sscanf(argv[1], "%x", &data);
		sscanf(argv[2], "%i", &timesleep);

//		sscanf(argv[1], "%d", &b);
//		sscanf(argv[2], "%d", &dir);
//		sscanf(argv[3], "%d", &tim);
		//printf("dir %d, tim %d", dir, tim);

//		int ret;
//		ret = iopl(3);
//		if ( ret ) { perror("iopl"); return 1;


	int fd;		// Файловый дескриптор для порта 
	port_set:
	//"/dev/ttyS0"
		fd = open("/dev/ttyS0", O_RDWR | O_NOCTTY | O_NDELAY); //'open_port()' - Открывает последовательный порт
		if (fd == -1) {
			//Возвращает файловый дескриптор при успехе или -1 при ошибке.
			printf("error port\n");
			perror("open_port: Unable to open /dev/ttyS0 - ");
		}
		else {
			struct termios options; 	//структура для установки порта
			tcgetattr(fd, &options); 	//читает параметры порта
			
//			cfsetispeed(&options, B50);	//установка скорости порта
//			cfsetospeed(&options, B50);	//установка скорости порта
			cfsetispeed(&options, B2400);	//установка скорости порта
			cfsetospeed(&options, B2400);	//установка скорости порта
			
			options.c_cflag &= ~PARENB;	//выкл проверка четности
			options.c_cflag &= ~CSTOPB;	//выкл 2-х стобит, вкл 1 стопбит
			options.c_cflag &= ~CSIZE;	//выкл битовой маски
			
			options.c_oflag = 0;			//Raw output.
//			options.c_cflag &= ISTRIP;
//			options.c_cflag &= ~XON;
//			options.c_cflag &= ~XOFF;
			
			options.c_cflag |= CS8;		//вкл 8бит
//			options.c_cflag |= CS7;		//вкл 7бит
			tcsetattr(fd, TCSANOW, &options);	//сохранения параметров порта
		}



char buf[512];	//размер зависит от размера строки принимаемых данных
int outa=0; 
int iIn;
int bytes_written = 0;
int i = 0;
 
//int open_port(void);


	int ret;
	ret = iopl(3);
	if ( ret ) { perror("iopl"); return 1; }

	//int x = 0x00000000;
	//int y = 0x00000000;
	//int* result[TIME];
	
	//bytes_written = write( fd, &data, 1 );

	char c; // которое надо вывести
	read_port:
		bytes_written = write( fd, &data, 1 );

//		outb_p ((unsigned char) data, (unsigned short int) SER_THR);
		usleep(timesleep); //1000 = 1мс		
//		insb((unsigned short int) SER_RBR, result, 1);
//		if (result) printf("Result: %s\n", result);

//		c = inb((unsigned short int) SER_MSR);
//		if (c) {
	//		Convert char to bits		
//			char str[9]; // 8 разрядов + нуль-символ в конце
//			snprintf(str, 9, "%d", (unsigned char) c);


	//		Подскажите пожалуйста как можно вывести биты одного байта (символа типа char)?
	//		char c; // которое надо вывести

	//		char str[9]; // 8 разрядов + нуль-символ в конце
	//		itoa( // варианты: _itoa, _ltoa, _ultoa
	//			(unsigned char)c,	// если не сделать такое приведение типа,
									// то диапазон 128..255 будет интерпретироваться
									// как (-128..-1) или 0xFFFFFF00 .. 0xFFFFFFFF
	//			str);
				//,2);
	//		естественно, будет старшими разрядами вперед
//			printf("Result('%c') is %s\n", c, str);


	//		Очень просто.
	//		std::bitset<8> bit_set('Z');
	//		std::cout<<bit_set;

			//printf("Result: %x\n", x);	
	//		printf("Result: ");
	//		print_binary(x);
	//		printf("\n");
			iIn = read(fd, buf, 1);		//чтения приходящих данных из порта
			
	//		int n = foo();
//			printf("%x _ ", (int)buf & 0xFF);

		i++;
		printf("%i _ ",buf);
		if (i = 9) { printf ("\n"); i = 0; }
		
//			printf("%x",buf);
//		}
	goto read_port;

//	while (1) goto read_port;
	
//	printf("stop1\n");
	}
}


/*
 * snprintf example
#include <stdio.h>

int main ()
{
  char buffer [100];
  int cx;

  cx = snprintf ( buffer, 100, "The half of %d is %d", 60, 60/2 );

  if (cx>=0 && cx<100)      // check returned value

    snprintf ( buffer+cx, 100-cx, ", and the half of that is %d.", 60/2/2 );

  puts (buffer);

  return 0;
}
*/
