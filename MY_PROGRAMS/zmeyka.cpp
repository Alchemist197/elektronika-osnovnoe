//---------------------------------------------------------------------------

#include <vcl.h>
#include <iostream>
#include <conio.h>
#include <windows.h>
#include <vector>

#pragma hdrstop

using namespace std;

//---------------------------------------------------------------------------

enum Way{
        UP,
        DOWN,
        RIGHT,
        LEFT
};

//--------settings-----------------------------------------------------------

HANDLE hStdOut=GetStdHandle(STD_OUTPUT_HANDLE);  //--handle console
char sp='0';    //--symbol piton
char sa='.';    //--symbol area
char sw='H';    //--symbol wall
int ps=7;        //--start piton size
int delay=100;   //--delay step
int width=30;    //--width Area
int height=20;   //--height Area

//--------prototype----------------------------------------------------------

void gotoxy(int x, int y);
void showArea();
void setWalls();
void validWalls(COORD &temp);
void newMouse();
bool ifMouseEating();
void move(Way way);
void init();

//--------end prototype------------------------------------------------------

void gotoxy(int x, int y)
{
        COORD temp={x,y};
        SetConsoleCursorPosition(hStdOut,temp);
}

//---------------------------------------------------------------------------
void showArea()
{
        int i,j;

        for(i=1; i<width+1; i++){
                for(j=1; j<height+1; j++){
                        gotoxy(i,j);
                        cout<<sa;
                }
        }
}

//---------------------------------------------------------------------------

void setWalls()
{
        int i,j;

        for(i=0; i<width+2; i++){
                for(j=0; j<height+2; j++){
                        if(j==0 || j==height+1 || i==0 ||i==width+1){
                                gotoxy(i,j);
                                cout<<sw;
                        }
                }
        }
}

//---------------------------------------------------------------------------

class Piton{

private:

        vector<COORD> pit;
        Way way;
        COORD head;
        COORD mouse;
        int i;



void validWalls(COORD &temp)
{
        switch(temp.X){
                case 31:{temp.X=1; break;}
                case 0:{temp.X=width; break;}
        }
        switch(temp.Y){
                case 0:{temp.Y=height; break;}
                case 21:{temp.Y=1; break;}
        }
}

bool validPiton()
{
        for(i=1; i<pit.size()-2; i++){

                if(pit.front().X==pit[i].X && pit.front().Y==pit[i].Y){

                        return true;
                }

        }

        return false;

}


void gameOver()
{
        exit(0);
        //cout<<"rfrfrf"<<endl;

}

void newMouse()
{
        mouse.X=rand()%30+1;
        mouse.Y=rand()%20+1;

        for(int i=0; i<pit.size(); i++) {

                if(pit[i].X==mouse.X && pit[i].Y==mouse.Y) {
                        newMouse();
                }
        }

        SetConsoleCursorPosition(hStdOut,mouse);
        cout<<"*";
}

bool ifMouseEating()
{
        return (pit.front().X==mouse.X && pit.front().Y==mouse.Y);
}


public:

Piton(){
                COORD temp={15,10};

                for(i=0; i<ps;i ++){
                        temp.Y++;
                        pit.push_back(temp);
                        SetConsoleCursorPosition(hStdOut,pit[i]);
                        cout<<sp;
                } //--end for

                newMouse();

}//--- end Piton

void move(Way way)
{



        head=pit.front();

        switch(way){
                        case UP:{head.Y--; break;}
                        case DOWN:{head.Y++; break;}
                        case RIGHT:{head.X++; break;}
                        case LEFT:{head.X--; break;}
        }//--end switch

        pit.insert(pit.begin(),head);

        SetConsoleCursorPosition(hStdOut,pit.back());
        cout<<".";

        pit.erase(pit.end());

        for(i=0; i<ps; i++){

                validWalls(pit[i]);

                SetConsoleCursorPosition(hStdOut,pit[i]);
                cout<<"0";
        }

        if(ifMouseEating()){

                pit.insert(pit.end(),mouse);
                newMouse();
        }

        if(!validPiton()){

                gameOver();
        }


} //-- end move ()

};//---end class

void init()
{
        showArea();
        setWalls();
        _setcursortype(_NOCURSOR);
}

#pragma argsused
void main()
{
        int i,j,a,m,n,x,y;
        int key;

        srand((unsigned)time(NULL));

        init();

        Piton pit;

        Way way=UP;

        while(1){
                if(kbhit()){
                        key=getch();
                        if(key==0){
                                key=getch();
                                switch(key){
                                        case 72:{if(way!=DOWN){way=UP;}
                                                break;}
                                        case 80:{if(way!=UP){way=DOWN;}
                                                break;}
                                        case 75:{if(way!=RIGHT){way=LEFT;}
                                                break;}
                                        case 77:{if(way!=LEFT){way=RIGHT;}
                                                break;}
                                } //--end switch
                        }//---end if 0
                        else{
                                switch(key){
                                        case 27:{exit(0); break;}
                                }//--- end switch key
                        }//-- end else

                }  //-- end if

                pit.move(way);
                Sleep(delay);

        } //-- end while


}
//---------------------------------------------------------------------------
