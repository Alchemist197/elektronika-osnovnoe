/*
	Попытаемся сложить две матрицы и сохранить результат в третьей результирующей матрице. 
	Для решения данной задачи уже необходимо подумать о правильном распределении данных между потоками. 
	Я реализовал простой алгоритм — сколько строк в матрице, столько потоков. 
	Каждый поток складывает элементы строки первой матрицы с элементами строки второй матрицы 
	и сохраняет результат в строку третей матрицы. 
	Получается, что каждый поток работает ровно со своими данными 
	и таким образом исключается доступ одного потока к данным другого потока. 
	Код программы выгляди следующим образом:
*/
//gcc -std=gnu99 main.c -o main.out -lpthread

#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/io.h>
 
//размеры матриц
#define N 5
#define M 5

#define PORT 0x2f8	//	SERIAL1
#define TIME 10000	//	Фиксированная задержка 0 сигнала в микросекундах

//SERIAL 			//адрес 	DLAB	чтение/запись	Название регистра
#define SER_THR (PORT + 0x000)		//00h	0	WR	THR(Transmit Holding Register)-регистр данных ожидающих передачи
#define SER_RBR (PORT + 0x000)		//00h	0	RD	RBR(Receiver Buffer Register)- буферный регистр приемника
#define SER_DLL (PORT + 0x000)		//00h	1	RD/WR	DLL(Divisor Latch LSB)-младший байт делителя частоты
#define SER_DIM (PORT + 0x001)		//01h	1	RD/WR	DIM(Divisor Latch MSB)-старший байт делителя частоты
#define SER_IER (PORT + 0x001)		//01h	0	RD/WR	IER(Interrupt Enable Register)-регистр разрешения прерывания
#define SER_IIR (PORT + 0x002)		//02h	х	RD	IIR(Interrupt Identification Register)-регистр идентифицирующий прерывания
#define SER_FRC (PORT + 0x002)		//02h	х	WR	FCR(FIFO Control Register)-регистр управления режимом FIFO
#define SER_LCR (PORT + 0x003)		//03h	x	RD/WR	LCR(Line Control Register)-регистр управления линией связи
#define SER_MCR (PORT + 0x004)		//04h	x	RD/WR	MCR(Modem Control Register)-регистр управления модемом
#define SER_LSR (PORT + 0x005)		//05h	x	RD/WR	LSR(Line Status Register)-регистр состояния линии связи
#define SER_MSR (PORT + 0x006)		//06h	x	RD/WR	MSR(Modem Status Register)-регистр состояния модема
#define SER_SCR (PORT + 0x007)		//07h	x	RD/WR	SCR(Scratch Pad Register)-регистр временного хранения 
 
//специальная структура для данных потока
/*
typedef struct {
	int rowN; //номер обрабатываемой строки
	int rowSize; //размер строки
	//указатели на матрицы
	int** array1; 
	int** array2;
	int** resArr;
} pthrData;
*/
typedef struct {	//
	int dir;	//	Направление вращения
	int tim;	//	Задержка положительного импульса
} ShimData;

typedef struct {	//
	int 
} SignData;



//	Функция подачи сигнала на привод двигателя 
void* shimsignal(void* thread_data)
{
	//	Эта функция только читает данные из структуры ShimData, применяя их для управления импульсами
	//получаем структуру с данными
	pthrData *data = (pthrData*) thread_data;
 	int dir = 0;
 	int tim = 0;
 	
	//	Модулируем широтно-импульсный сигнал на порт: outb_p ((unsigned char) value, (unsigned short int) port);
	while ( 1 ) {	
		//	dir должна находиться в пределах допустимых значений:
		//		0,1 - остановка, 2 - движение против часовой, 3 - движение по часовой
		//	Считываем dir и tim из структуры, если она не заблокирована в данный момент
		//	Если структура заблокирована, пользуемся старыми значениями
		dir = data->dir;
		tim = data->tim;
				
		//	1 с выставленной задержкой
		outb_p ((unsigned char) dir, (unsigned short int) SER_MCR); 
		usleep(tim); //1000 = 1мс
		//	0 с фиксированной задержкой
		outb_p ((unsigned char) dir-2, (unsigned short int) SER_MCR);
		usleep(10000); //1000 = 1мс
		//outb_p ((unsigned char) 0x01, (unsigned short int) SER_MCR); usleep(1000); //1мс
		//usleep(100000); //100мс
	}
 
	//складываем элементы строк матриц и сохраняем результат
//	for(int i = 0; i < data->rowSize; i++)
//		data->resArr[data->rowN][i] = data->array1[data->rowN][i] + data->array2[data->rowN][i];
 
	return NULL;
}
 
//	Функция считывания оборотов
void* optodatchik(void* thread_data)
{
	//	Эта функция только пишет данные в структуру SignData, изменяя их при каждом пройденном лепестке и круге 
//	time_t bgnround;	
//	time_t endround;
	struct timeval bgnround;
	struct timeval endround;
	struct timeval lepestok;
	struct timeval probel;
	double seconds = 0.0;
	//	Отметки лепестков
	int tic = 0;	//	1 - Пробел, 0 - Лепесток
	int toc = 0;	//	Счетчик лепестков по кругу

	time(&bgnround);
	while ( 1 ) {
		x = inb((unsigned short int) SER_MSR);
		if ( tic == 1 && !x ) {	//Zaschityvaem lepestok i ozhidaem 10
			time(&lepestok);
			tic = 0;
			toc++;
		}
		if ( x >= 10 ) {	//Probel; Ozhidaem 0
			time(&probel);
			tic = 1;
		}
		if ( toc > TIME ) {	//Proyden krug
			toc = 0;
			gettimeofday(&endround, 0);
			seconds = timedifference_msec(bgnround, endround) / 1000;
			printf("Round is gone through %f seconds; FREQ ~ %f ob/min;\n", seconds, 60/seconds);
			gettimeofday(&bgnround, 0);
		}
		//printf("Result: %x\n", x);
		usleep(100); //1000 = 1мс
	}
 
	return NULL;
}
 
int main (int argc, char* argv[])
{
    int x = 0x00;
    int b = 0;
	int dir = 0;
	int tim = 1;

	//	Отрабатываем входные аргументы
	if (argc != 4) {
		//printf("Usage: %s 0x<port> 0x<value>\n", argv[0]);
		//return 1;
	} else {
		//unsigned int port;
		//unsigned int value;
		//sscanf(argv[1], "%x", &port);
		//sscanf(argv[2], "%x", &value);
	}

	//	Получаем доступ до портов
	int ret;
	ret = iopl(3);
	if ( ret ) { perror("iopl"); return 1; }	



	//выделяем память под двумерные массивы
	int** matrix1 = (int**) malloc(N * sizeof(int*));
	int** matrix2 = (int**) malloc(N * sizeof(int*));
	int** resultMatrix = (int**) malloc(N * sizeof(int*));
 
	//выделяем память под элементы матриц
	for(int i = 0; i < M; i++){
		matrix1[i] = (int*) malloc(M * sizeof(int));
		matrix2[i] = (int*) malloc(M * sizeof(int));
		resultMatrix[i] = (int*) malloc(M * sizeof(int));
	}
 
	//инициализируем начальными значениями
	for(int i = 0; i < N; i++){
		for(int j = 0; j < M; j++){
			matrix1[i][j] = i;
			matrix2[i][j] = j;
			resultMatrix[i][j] = 0;
		}
	}
 
	//выделяем память под массив идентификаторов потоков
	pthread_t* threads = (pthread_t*) malloc(N * sizeof(pthread_t));
	//сколько потоков - столько и структур с потоковых данных
	pthrData* threadData = (pthrData*) malloc(N * sizeof(pthrData));
 
	//инициализируем структуры потоков
	for(int i = 0; i < N; i++){
		threadData[i].rowN = i;
		threadData[i].rowSize = M;
		threadData[i].array1 = matrix1;
		threadData[i].array2 = matrix2;
		threadData[i].resArr = resultMatrix;
 
		//запускаем поток
		pthread_create(&(threads[i]), NULL, shim, &threadData[i]);
	}
 
	//ожидаем выполнение всех потоков
	for(int i = 0; i < N; i++)
		pthread_join(threads[i], NULL);
 
	//освобождаем память
	free(threads);
	free(threadData);
	for(int i = 0; i < N; i++){
		free(matrix1[i]);
		free(matrix2[i]);
		free(resultMatrix[i]);
	}
	free(matrix1);
	free(matrix2);
	free(resultMatrix);
	return 0;
}
