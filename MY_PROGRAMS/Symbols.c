//Как вариант, это вывести таблицу символов:
#include <stdio.h>

int main(int argc, char *argv[]) {
   // выводим таблицу символов
   for (unsigned int count = 0; count < 256; count++) {
      printf("%d means %c\n", count, count);
   }
   getchar();
   return 0;
}
