#include <stdio.h>
#include <sys/io.h>
//#include <stdlib.h>
#include <unistd.h>
//LPT
//lp0 0x3bc; lp1 0x378; lp2 0x278;
#define PORT 0x378
#define TIME 1

//ATAPI
/* IO ports for channel 1*/
#define ATA_DR		(0x170)		/* Data Register */
#define ATA_ER		(0x171)		/* Error Register */
#define ATA_INT		(0x172)		/* Interrupt Register */
#define ATA_ADR0	(0x173)		/* Addr0 Register */
#define ATA_ADR1	(0x174)		/* Addr1 Register */
#define ATA_ADR2	(0x175)		/* Addr2 Register */
#define ATA_SEL		(0x176)		/* Device Select Register */
#define ATA_CMD		(0x177)		/* Status/Command Register */
#define ATA_ADD		(0x376)		/* Additional Status/Command Register */

int main (int argc, char* argv[])
{
    int x = 0x00;
    int b = 0;
/*
	if (ioperm (PORT, 1, 1))
	{
		perror ("ioperm()");
		exit;
	}
	outb(x, PORT);
*/ 
	if (argc != 2) {
		//printf("Usage: %s 0x<port> 0x<value>\n", argv[0]);
		printf("Usage: %s 1(on) 0(off)\n", argv[0]);
		return 1;
	} else {
		//unsigned int port;
		//unsigned int value;
		int ret;
		//sscanf(argv[1], "%x", &port);
		//sscanf(argv[2], "%x", &value);
		sscanf(argv[1], "%d", &b);
		ret = iopl(3);
		if ( ret ) { perror("iopl"); return 1; }
		//outb_p ((unsigned char) value, (unsigned short int) port);
		if ( b ) {	//Enable all
			while ( 1 ) {
				for (x = 0x00; x < 0xFF; x++) {
					outw_p ((unsigned char) x, (unsigned short int) ATA_DR); usleep(1000); //1мс
/*					
					outb_p ((unsigned char) x, (unsigned short int) ATA_ER); usleep(1000); //1мс
					outb_p ((unsigned char) x, (unsigned short int) ATA_INT); usleep(1000); //1мс			
					outb_p ((unsigned char) x, (unsigned short int) ATA_ADR0); usleep(1000); //1мс
					outb_p ((unsigned char) x, (unsigned short int) ATA_ADR1); usleep(1000); //1мс
					outb_p ((unsigned char) x, (unsigned short int) ATA_ADR2); usleep(1000); //1мс			
					outb_p ((unsigned char) x, (unsigned short int) ATA_SEL); usleep(1000); //1мс
					outb_p ((unsigned char) x, (unsigned short int) ATA_CMD); usleep(1000); //1мс
					outb_p ((unsigned char) x, (unsigned short int) ATA_ADD); usleep(1000); //1мс
*/					 
					usleep(10000); //10мс
				}
//				sleep(1);
			}
		} else {	//Disable all
			outb_p ((unsigned char) 0x00, (unsigned short int) ATA_DR);			
			outb_p ((unsigned char) 0x00, (unsigned short int) ATA_ER);
			outb_p ((unsigned char) 0x00, (unsigned short int) ATA_INT);			
			outb_p ((unsigned char) 0x00, (unsigned short int) ATA_ADR0);
			outb_p ((unsigned char) 0x00, (unsigned short int) ATA_ADR1);
			outb_p ((unsigned char) 0x00, (unsigned short int) ATA_ADR2);
			outb_p ((unsigned char) 0x00, (unsigned short int) ATA_SEL);			
			outb_p ((unsigned char) 0x00, (unsigned short int) ATA_CMD);			
			outb_p ((unsigned char) 0x00, (unsigned short int) ATA_ADD);			
		}
	}
	return 0;
}

