#include <stdio.h>
#include <sys/io.h>
//#include <stdlib.h>
#include <unistd.h>
//LPT
//lp0 0x3bc; lp1 0x378; lp2 0x278;
//#define PORT 0x378	//LPT
#define PORT 0x2f8	//SERIAL0

#define TIME 1

//SERIAL 			//адрес 	DLAB	чтение/запись	Название регистра
#define SER_THR (PORT + 0x000)		//00h	0	WR	THR(Transmit Holding Register)-регистр данных ожидающих передачи
#define SER_RBR (PORT + 0x000)		//00h	0	RD	RBR(Receiver Buffer Register)- буферный регистр приемника
#define SER_DLL (PORT + 0x000)		//00h	1	RD/WR	DLL(Divisor Latch LSB)-младший байт делителя частоты
#define SER_DIM (PORT + 0x001)		//01h	1	RD/WR	DIM(Divisor Latch MSB)-старший байт делителя частоты
#define SER_IER (PORT + 0x001)		//01h	0	RD/WR	IER(Interrupt Enable Register)-регистр разрешения прерывания
#define SER_IIR (PORT + 0x002)		//02h	х	RD	IIR(Interrupt Identification Register)-регистр идентифицирующий прерывания
#define SER_FRC (PORT + 0x002)		//02h	х	WR	FCR(FIFO Control Register)-регистр управления режимом FIFO
#define SER_LCR (PORT + 0x003)		//03h	x	RD/WR	LCR(Line Control Register)-регистр управления линией связи
#define SER_MCR (PORT + 0x004)		//04h	x	RD/WR	MCR(Modem Control Register)-регистр управления модемом
#define SER_LSR (PORT + 0x005)		//05h	x	RD/WR	LSR(Line Status Register)-регистр состояния линии связи
#define SER_MSR (PORT + 0x006)		//06h	x	RD/WR	MSR(Modem Status Register)-регистр состояния модема
#define SER_SCR (PORT + 0x007)		//07h	x	RD/WR	SCR(Scratch Pad Register)-регистр временного хранения 

//FLOPPY
/* IO ports */
#define FDC_SRB  (0x3f1)	/* Дополнительный регистр состояния */
#define FDC_DOR  (0x3f2)	/* Digital Output Register */
#define FDC_TDR  (0x3f3)	/* Регистр управления */
#define FDC_MSR  (0x3f4)	/* Main Status Register (input) */
#define FDC_DRS  (0x3f4)	/* Data Rate Select Register (output) */
#define FDC_DATA (0x3f5)	/* Data Register */
#define FDC_RESV (0x3f6)	/* Reserved */
#define FDC_DIR  (0x3f7)	/* Digital Input Register (input) */
#define FDC_CCR  (0x3f7)	/* Configuration Control Register (output) */

/* command unsigned chars (these are 765 commands + options such as MFM, etc) */
#define CMD_SPECIFY (0x03)	/* specify drive timings */
#define CMD_WRITE   (0xc5)	/* write data (+ MT,MFM) */
#define CMD_READ    (0xe6)	/* read data (+ MT,MFM,SK) */
#define CMD_RECAL   (0x07)	/* recalibrate */
#define CMD_SENSEI  (0x08)	/* sense interrupt status */
#define CMD_FORMAT  (0x4d)	/* format track (+ MFM) */
#define CMD_SEEK    (0x0f)	/* seek track */
#define CMD_VERSION (0x10)	/* FDC version */
#define CMD_MOTOR1	(0x1c)	/* Motor1 enable */

int main (int argc, char* argv[])
{
    int x = 0x00;
    int b = 0;
	int dir = 0;
	int tim = 1;
/*
	if (ioperm (PORT, 1, 1))
	{
		perror ("ioperm()");
		exit;
	}
	outb(x, PORT);
*/ 
	if (argc != 4) {
		//printf("Usage: %s 0x<port> 0x<value>\n", argv[0]);
		printf("Usage: %s 1(serial)-0(once) directive(1,2,3) time(ns)\n", argv[0]);
		return 1;
	} else {
		//unsigned int port;
		//unsigned int value;
		//sscanf(argv[1], "%x", &port);
		//sscanf(argv[2], "%x", &value);
		sscanf(argv[1], "%d", &b);
		sscanf(argv[2], "%d", &dir);
		sscanf(argv[3], "%d", &tim);
		//printf("dir %d, tim %d", dir, tim);

		int ret;
		ret = iopl(3);
		if ( ret ) { perror("iopl"); return 1; }
/*
asm( ""
"'записываем  в LCR режим работы сом порта:"
"'8 бит всимволе,1 стоп бит, проверка паритета на четность,выдавать 0 в случае обрыва, DLAB=1"
"	    movl dx,%0x2fb	'адрес регистра"
"	movl al,%0xDB		'записываем в AL значения для регистра LCR=DBh"
"	out dx,al		'записываем данные в регистр UART LCR"
"'задаем скорость обмена  115 000 бит/сек  DIM=00h, DLL=01h"
"        movl dx,%0x2f8	'адрес регистра"
"	movl al,%0x01"
"	out dx,al	 	'запись регистра DLL=01h"
"        movl dx,%0x2f9	'адрес регистра"
"	movl al,%0x00"
"	out dx,al		'запись регистра DIM=00h"
"'снимаем бит DLAB=1"
"        movl dx,%0x2fb 'адрес регистра"
"	movl al,%0x5B		'DLAB=0"
"	out dx,al"
"'послать байт 03h в линию связи"
"        movl dx,%0x2f8	'адрес регистра"
"	movl al,%0x03"
"	out dx,al		'посылает байт 03h на скорости 115 000 бит/сек"
);
*/

		//outb_p ((unsigned char) value, (unsigned short int) port);
		if ( b ) {	//Enable all
			//outb_p ((unsigned char) x, (unsigned short int) SER_MCR);
			while ( 1 ) {
				//for (x = 0x00; x < 0xFF; x++) {
					//outb_p ((unsigned char) dir, (unsigned short int) SER_MCR); //usleep(1000); //1мс
					outb_p ((unsigned char) dir, (unsigned short int) SER_MCR); 
					usleep(tim); //1000 = 1мс
					outb_p ((unsigned char) dir-2, (unsigned short int) SER_MCR);
					usleep(10000); //1000 = 1мс
					//outb_p ((unsigned char) 0x01, (unsigned short int) SER_MCR); usleep(1000); //1мс
					//usleep(100000); //100мс
				//}
				//sleep(1);
			}
		} else {	//Disable all
			//outb_p ((unsigned char) 0x01, (unsigned short int) SER_THR);			
			outb_p ((unsigned char) dir, (unsigned short int) SER_MCR);		
		}
	}
	return 0;
}

