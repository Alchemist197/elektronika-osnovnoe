	.file	"rtl.c"
	.section	.rodata
.LC0:
	.string	"REALTIME PROJECT"
.LC1:
	.string	"iopl"
	.text
.globl thread_func
	.type	thread_func, @function
thread_func:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$56, %esp
	movl	$.LC0, (%esp)
	call	puts
	movl	8(%ebp), %eax
	movl	%eax, -36(%ebp)
	movl	$3, (%esp)
	call	iopl
	movl	%eax, -32(%ebp)
	cmpl	$0, -32(%ebp)
	je	.L2
	movl	$.LC1, (%esp)
	call	perror
	leave
	ret
.L2:
	movl	$0, -28(%ebp)
	movl	$1, -24(%ebp)
	movl	$1, -20(%ebp)
	movl	$0, -16(%ebp)
	movl	-36(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, -12(%ebp)
	movl	-36(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, -8(%ebp)
	movl	-36(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -4(%ebp)
	
	
.L7:
	movl	$1, -24(%ebp)
	jmp	.L3
.L6:
	movl	$0, -28(%ebp)
	jmp	.L4
	
.L5:
	movl	-24(%ebp), %eax
	movl	%eax, %ecx
	movl	-8(%ebp), %eax
	movl	%eax, %edx
	movl	%ecx, %eax
#APP
# 93 "rtl.c" 1
	outw %ax, %dx
# 0 "" 2
#NO_APP
	addl	$1, -28(%ebp)
.L4:
	movl	-12(%ebp), %eax
	cmpl	-28(%ebp), %eax
	ja	.L5
	
	movl	-8(%ebp), %eax
	movl	%eax, %edx
#APP
# 96 "rtl.c" 1
	inw %dx, %ax
# 0 "" 2
#NO_APP
	movl	%eax, -16(%ebp)
	sall	-24(%ebp)
.L3:
	cmpl	$65534, -24(%ebp)
	jbe	.L6
	jmp	.L7
	.size	thread_func, .-thread_func
	
	
	
	
	
	
	
	
	.section	.rodata
	.align 4
.LC2:
	.string	"Usage: %s 0x<port> num_cycles intercycle_timeout \n"
.LC3:
	.string	"%x"
.LC4:
	.string	"%d"
.LC5:
	.string	"PORT: %x;\n"
.LC6:
	.string	"mlockall failed: %m\n"
	.align 4
.LC7:
	.string	"init pthread attributes failed"
.LC8:
	.string	"pthread setstacksize failed"
.LC9:
	.string	"pthread setschedpolicy failed"
.LC10:
	.string	"pthread setschedparam failed"
	.align 4
.LC11:
	.string	"pthread setinheritsched failed"
.LC12:
	.string	"create pthread failed"
.LC13:
	.string	"join pthread failed: %m\n"
	.text
.globl main
	.type	main, @function
main:
	leal	4(%esp), %ecx
	andl	$-16, %esp
	pushl	-4(%ecx)
	pushl	%ebp
	movl	%esp, %ebp
	pushl	%ecx
	subl	$100, %esp
	movl	%ecx, -76(%ebp)
	movl	-76(%ebp), %eax
	cmpl	$4, (%eax)
	je	.L10
	movl	-76(%ebp), %edx
	movl	4(%edx), %eax
	movl	(%eax), %eax
	movl	%eax, 4(%esp)
	movl	$.LC2, (%esp)
	call	printf
	movl	$1, -72(%ebp)
	jmp	.L11
.L10:
	movl	-76(%ebp), %edx
	movl	4(%edx), %eax
	addl	$4, %eax
	movl	(%eax), %edx
	leal	-68(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$.LC3, 4(%esp)
	movl	%edx, (%esp)
	call	sscanf
	movl	-76(%ebp), %edx
	movl	4(%edx), %eax
	addl	$8, %eax
	movl	(%eax), %edx
	leal	-68(%ebp), %eax
	addl	$4, %eax
	movl	%eax, 8(%esp)
	movl	$.LC4, 4(%esp)
	movl	%edx, (%esp)
	call	sscanf
	movl	-76(%ebp), %edx
	movl	4(%edx), %eax
	addl	$12, %eax
	movl	(%eax), %edx
	leal	-68(%ebp), %eax
	addl	$8, %eax
	movl	%eax, 8(%esp)
	movl	$.LC4, 4(%esp)
	movl	%edx, (%esp)
	call	sscanf
	movl	-68(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	$.LC5, (%esp)
	call	printf
	movl	$3, (%esp)
	call	mlockall
	cmpl	$-1, %eax
	jne	.L12
	movl	$.LC6, (%esp)
	call	printf
	movl	$-2, (%esp)
	call	exit
.L12:
	leal	-52(%ebp), %eax
	movl	%eax, (%esp)
	call	pthread_attr_init
	movl	%eax, -8(%ebp)
	cmpl	$0, -8(%ebp)
	je	.L13
	movl	$.LC7, (%esp)
	call	puts
	jmp	.L14
.L13:
	movl	$16384, 4(%esp)
	leal	-52(%ebp), %eax
	movl	%eax, (%esp)
	call	pthread_attr_setstacksize
	movl	%eax, -8(%ebp)
	cmpl	$0, -8(%ebp)
	je	.L15
	movl	$.LC8, (%esp)
	call	puts
	jmp	.L14
.L15:
	movl	$1, 4(%esp)
	leal	-52(%ebp), %eax
	movl	%eax, (%esp)
	call	pthread_attr_setschedpolicy
	movl	%eax, -8(%ebp)
	cmpl	$0, -8(%ebp)
	je	.L16
	movl	$.LC9, (%esp)
	call	puts
	jmp	.L14
.L16:
	movl	$80, -16(%ebp)
	leal	-16(%ebp), %eax
	movl	%eax, 4(%esp)
	leal	-52(%ebp), %eax
	movl	%eax, (%esp)
	call	pthread_attr_setschedparam
	movl	%eax, -8(%ebp)
	cmpl	$0, -8(%ebp)
	je	.L17
	movl	$.LC10, (%esp)
	call	puts
	jmp	.L14
.L17:
	movl	$1, 4(%esp)
	leal	-52(%ebp), %eax
	movl	%eax, (%esp)
	call	pthread_attr_setinheritsched
	movl	%eax, -8(%ebp)
	cmpl	$0, -8(%ebp)
	je	.L18
	movl	$.LC11, (%esp)
	call	puts
	jmp	.L14
.L18:
	leal	-68(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	$thread_func, 8(%esp)
	leal	-52(%ebp), %eax
	movl	%eax, 4(%esp)
	leal	-56(%ebp), %eax
	movl	%eax, (%esp)
	call	pthread_create
	movl	%eax, -8(%ebp)
	cmpl	$0, -8(%ebp)
	je	.L19
	movl	$.LC12, (%esp)
	call	puts
	jmp	.L14
.L19:
	movl	-56(%ebp), %eax
	movl	$0, 4(%esp)
	movl	%eax, (%esp)
	call	pthread_join
	movl	%eax, -8(%ebp)
	cmpl	$0, -8(%ebp)
	je	.L14
	movl	$.LC13, (%esp)
	call	printf
.L14:
	movl	-8(%ebp), %eax
	movl	%eax, -72(%ebp)
.L11:
	movl	-72(%ebp), %eax
	addl	$100, %esp
	popl	%ecx
	popl	%ebp
	leal	-4(%ecx), %esp
	ret
	.size	main, .-main
	.ident	"GCC: (Debian 4.3.2-1.1) 4.3.2"
	.section	.note.GNU-stack,"",@progbits
