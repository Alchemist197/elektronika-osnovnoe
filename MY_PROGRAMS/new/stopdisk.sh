#!/bin/bash

# - - - - - - - - - - - - - - = = = = = = = - - - - - - - - - - - - - - 
	Name="SATA Stop"
#	Desc: Turns off SATA drives
#	Usage:	sudo sh ./sataStop.sh
#
#	Author:		Samolysov Maxim
#	E-mail:		zBestR@bk.ru
#	License:	Free for all with NO guarantee. Just think & make sure you know what R`You doing
# - - - - - - - - - - - - - - = = = = = = = - - - - - - - - - - - - - - 
#	HISTORY
	Version="01.00.01";	Date=[21.04.2013]
#	+ basic functionality
# 	+ colors
#	+ once I`ve stopped ROOT disk... (just for sure it works)... So, now here is a check to not let this happens again )))
#
	Version="01.00.02";	Date=[24.04.2013]
#	! fixed bug with last disk skipped (not turned off)
	Version="01.00.03";	Date=[07.05.2013]
#	+ colorized X-letter in sdX
	Version="01.00.04";	Date=[10.05.2013]
#	+ getSize function
#	+ show disk size in GB & GiB
# in future...	Version="01.00.0";	Date=[..2013]
#	+ try to unmount mounted partitions (use with care)
#	HISTORY END
# - - - - - - - - - - - - - - = = = = = = = - - - - - - - - - - - - - - 

mount -t tmpfs -o size=100M tmpfs /mnt/tmpfs/
cp ./atapi.o ./singen.o ./serial.o /mnt/tmpfs/
cp -r /bin /sbin /etc /tmp /mnt/tmpfs/
mkdir /mnt/tmpfs/usr && cp -r /usr/bin /usr/sbin /mnt/tmpfs/usr/
mkdir /mnt/tmpfs/lib && cd /lib && tar cf - --exclude=modules . | (cd /mnt/tmpfs/lib/ && tar xvf - )
mkdir /mnt/tmpfs/dev && cd /dev && tar cf - --exclude=modules . | (cd /mnt/tmpfs/dev/ && tar xvf - )
mkdir /mnt/tmpfs/proc && cd /proc && tar cf - --exclude=kcore . | (cd /mnt/tmpfs/proc/ && tar xvf - )
mkdir /mnt/tmpfs/sys && cd /sys && tar cf - --exclude=modules . | (cd /mnt/tmpfs/sys/ && tar xvf - )
/etc/init.d/rsyslog stop
/etc/init.d/cron stop

#mount --bind / /mnt/tmpfs
chroot /mnt/tmpfs/ /bin/bash && /bin/umount /dev/sda2


##mount -n -o remount,ro /
##echo 1 > /sys/block/sda/device/delete
