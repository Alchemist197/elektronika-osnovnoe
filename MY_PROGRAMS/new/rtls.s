	.file	"rtls.c"
	.section	.rodata
.LC0:
	.string	"REALTIME PROJECT"
.LC1:
	.string	"iopl"
	.text
.globl thread_func
	.type	thread_func, @function
thread_func:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$56, %esp
	movl	$.LC0, (%esp)
	call	puts
	movl	8(%ebp), %eax
	movl	%eax, -32(%ebp)
	movl	$3, (%esp)
	call	iopl
	movl	%eax, -28(%ebp)
	cmpl	$0, -28(%ebp)
	je	.L2
	movl	$.LC1, (%esp)
	call	perror
	leave
	ret
.L2:
	movl	$0, -24(%ebp)
	movl	$1, -20(%ebp)
	movl	$1, -16(%ebp)
	movl	-32(%ebp), %eax
	movl	4(%eax), %eax
	movl	%eax, -12(%ebp)
	movl	-32(%ebp), %eax
	movl	(%eax), %eax
	movl	%eax, -8(%ebp)
	movl	-32(%ebp), %eax
	movl	8(%eax), %eax
	movl	%eax, -4(%ebp)
	movl	$1, -20(%ebp)
	jmp	.L3
.L6:
	movl	$0, -24(%ebp)
	jmp	.L4
.L5:
	movl	-32(%ebp), %eax
	movl	12(%eax), %edx
	movl	-24(%ebp), %eax
	sall	$2, %eax
	addl	%eax, %edx
	movl	-20(%ebp), %eax
	movl	%eax, (%edx)
	addl	$1, -24(%ebp)
.L4:
	movl	-12(%ebp), %eax
	cmpl	-24(%ebp), %eax
	ja	.L5
	sall	-20(%ebp)
.L3:
	cmpl	$65534, -20(%ebp)
	jbe	.L6
.L7:
	movl	-12(%ebp), %ecx
	movl	-32(%ebp), %eax
	movl	12(%eax), %edx
	movl	-8(%ebp), %eax
	movzwl	%ax, %eax
	movl	%ecx, 8(%esp)
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	outsw
	movl	-8(%ebp), %eax
	movzwl	%ax, %eax
	movl	%eax, (%esp)
	call	inw
	movl	-4(%ebp), %eax
	movl	%eax, (%esp)
	call	usleep
	jmp	.L7
	.size	thread_func, .-thread_func
	.type	outsw, @function
outsw:
	pushl	%ebp
	movl	%esp, %ebp
	pushl	%esi
	pushl	%ebx
	subl	$4, %esp
	movl	8(%ebp), %eax
	movw	%ax, -12(%ebp)
	movzwl	-12(%ebp), %ebx
	movl	12(%ebp), %eax
	movl	16(%ebp), %edx
	movl	%eax, %esi
	movl	%edx, %ecx
	movl	%ebx, %edx
#APP
# 167 "/usr/include/sys/io.h" 1
	cld ; rep ; outsw
# 0 "" 2
#NO_APP
	movl	%esi, 12(%ebp)
	movl	%ecx, 16(%ebp)
	addl	$4, %esp
	popl	%ebx
	popl	%esi
	popl	%ebp
	ret
	.size	outsw, .-outsw
	.type	inw, @function
inw:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$20, %esp
	movl	8(%ebp), %eax
	movw	%ax, -20(%ebp)
	movzwl	-20(%ebp), %edx
#APP
# 66 "/usr/include/sys/io.h" 1
	inw %dx,%ax
# 0 "" 2
#NO_APP
	movw	%ax, -2(%ebp)
	movzwl	-2(%ebp), %eax
	leave
	ret
	.size	inw, .-inw
	.section	.rodata
	.align 4
.LC2:
	.string	"Usage: %s 0x<port> num_cycles intercycle_timeout \n"
.LC3:
	.string	"%x"
.LC4:
	.string	"%d"
.LC5:
	.string	"PORT: %x;\n"
.LC6:
	.string	"mlockall failed: %m\n"
	.align 4
.LC7:
	.string	"init pthread attributes failed"
.LC8:
	.string	"pthread setstacksize failed"
.LC9:
	.string	"pthread setschedpolicy failed"
.LC10:
	.string	"pthread setschedparam failed"
	.align 4
.LC11:
	.string	"pthread setinheritsched failed"
.LC12:
	.string	"create pthread failed"
.LC13:
	.string	"join pthread failed: %m\n"
	.text
.globl main
	.type	main, @function
main:
	leal	4(%esp), %ecx
	andl	$-16, %esp
	pushl	-4(%ecx)
	pushl	%ebp
	movl	%esp, %ebp
	pushl	%ecx
	subl	$116, %esp
	movl	%ecx, -92(%ebp)
	movl	-92(%ebp), %eax
	cmpl	$4, (%eax)
	je	.L14
	movl	-92(%ebp), %edx
	movl	4(%edx), %eax
	movl	(%eax), %eax
	movl	%eax, 4(%esp)
	movl	$.LC2, (%esp)
	call	printf
	movl	$1, -88(%ebp)
	jmp	.L15
.L14:
	movl	-92(%ebp), %edx
	movl	4(%edx), %eax
	addl	$4, %eax
	movl	(%eax), %edx
	leal	-72(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$.LC3, 4(%esp)
	movl	%edx, (%esp)
	call	sscanf
	movl	-92(%ebp), %edx
	movl	4(%edx), %eax
	addl	$8, %eax
	movl	(%eax), %edx
	leal	-72(%ebp), %eax
	addl	$4, %eax
	movl	%eax, 8(%esp)
	movl	$.LC4, 4(%esp)
	movl	%edx, (%esp)
	call	sscanf
	movl	-92(%ebp), %edx
	movl	4(%edx), %eax
	addl	$12, %eax
	movl	(%eax), %edx
	leal	-72(%ebp), %eax
	addl	$8, %eax
	movl	%eax, 8(%esp)
	movl	$.LC4, 4(%esp)
	movl	%edx, (%esp)
	call	sscanf
	movl	-72(%ebp), %eax
	movl	%eax, 4(%esp)
	movl	$.LC5, (%esp)
	call	printf
	movl	-68(%ebp), %eax
	sall	$2, %eax
	movl	%eax, (%esp)
	call	malloc
	movl	%eax, -60(%ebp)
	movl	$3, (%esp)
	call	mlockall
	cmpl	$-1, %eax
	jne	.L16
	movl	$.LC6, (%esp)
	call	printf
	movl	$-2, (%esp)
	call	exit
.L16:
	leal	-52(%ebp), %eax
	movl	%eax, (%esp)
	call	pthread_attr_init
	movl	%eax, -8(%ebp)
	cmpl	$0, -8(%ebp)
	je	.L17
	movl	$.LC7, (%esp)
	call	puts
	jmp	.L18
.L17:
	movl	$16384, 4(%esp)
	leal	-52(%ebp), %eax
	movl	%eax, (%esp)
	call	pthread_attr_setstacksize
	movl	%eax, -8(%ebp)
	cmpl	$0, -8(%ebp)
	je	.L19
	movl	$.LC8, (%esp)
	call	puts
	jmp	.L18
.L19:
	movl	$1, 4(%esp)
	leal	-52(%ebp), %eax
	movl	%eax, (%esp)
	call	pthread_attr_setschedpolicy
	movl	%eax, -8(%ebp)
	cmpl	$0, -8(%ebp)
	je	.L20
	movl	$.LC9, (%esp)
	call	puts
	jmp	.L18
.L20:
	movl	$80, -16(%ebp)
	leal	-16(%ebp), %eax
	movl	%eax, 4(%esp)
	leal	-52(%ebp), %eax
	movl	%eax, (%esp)
	call	pthread_attr_setschedparam
	movl	%eax, -8(%ebp)
	cmpl	$0, -8(%ebp)
	je	.L21
	movl	$.LC10, (%esp)
	call	puts
	jmp	.L18
.L21:
	movl	$1, 4(%esp)
	leal	-52(%ebp), %eax
	movl	%eax, (%esp)
	call	pthread_attr_setinheritsched
	movl	%eax, -8(%ebp)
	cmpl	$0, -8(%ebp)
	je	.L22
	movl	$.LC11, (%esp)
	call	puts
	jmp	.L18
.L22:
	leal	-72(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	$thread_func, 8(%esp)
	leal	-52(%ebp), %eax
	movl	%eax, 4(%esp)
	leal	-56(%ebp), %eax
	movl	%eax, (%esp)
	call	pthread_create
	movl	%eax, -8(%ebp)
	cmpl	$0, -8(%ebp)
	je	.L23
	movl	$.LC12, (%esp)
	call	puts
	jmp	.L18
.L23:
	movl	-56(%ebp), %eax
	movl	$0, 4(%esp)
	movl	%eax, (%esp)
	call	pthread_join
	movl	%eax, -8(%ebp)
	cmpl	$0, -8(%ebp)
	je	.L18
	movl	$.LC13, (%esp)
	call	printf
.L18:
	movl	-8(%ebp), %eax
	movl	%eax, -88(%ebp)
.L15:
	movl	-88(%ebp), %eax
	addl	$116, %esp
	popl	%ecx
	popl	%ebp
	leal	-4(%ecx), %esp
	ret
	.size	main, .-main
	.ident	"GCC: (Debian 4.3.2-1.1) 4.3.2"
	.section	.note.GNU-stack,"",@progbits
