

/*                                                                  
 * POSIX Real Time Example
 * using a single pthread as RT thread
 */
 
#include <limits.h>
#include <pthread.h>
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>

#include <sys/io.h>
#include <unistd.h>

# define NUM_THREADS 1

#define OUT_P_B(val,port)	\
    asm(			\
	"outb %%al, %%dx"	\
	::"a"(val),"d"(port)	\
    )
    
#define OUT_P_W(val,port)	\
    asm(			\
	"outw %%ax, %%dx"	\
	::"a"(val),"d"(port)	\
    )
    
#define IN_P_B(val,port)	\
    asm(			\
	"inb %%dx, %%al"	\
	:"=a"(val)		\
	:"d"(port)		\
    )


#define IN_P_W(val,port)	\
    asm(			\
	"inw %%dx, %%ax"	\
	:"=a"(val)		\
	:"d"(port)		\
    )
    
typedef struct someArgs_tag {
    int port;
    //const char *msg;
    int count;
    int timeout;
} someArgs_t;

//gcc rtl.c -o rtl.o -lpthread -O2
//./rtl.o 0x170 500000 500000

void *thread_func(void *args)
{
        //Do RT specific stuff here
        printf("REALTIME PROJECT\n");
        
		someArgs_t *arg = (someArgs_t*) args;
    
    /*
    int len;
    if (arg->msg == NULL) {
        return "PASKUDNO";
    }
    len = strlen(arg->msg);
    printf("%s\n", arg->msg);
    arg->out = len;
    return "OTLICHNO";        
    */     
		//int port = 0x170;
		
		int ret = iopl(3);
		if ( ret ) { perror("iopl"); return; }

	//local_irq_disable();
	asm("cli");
	// прерывания запрещены ... 
	
		unsigned int x = 0, y = 0x0001, z = 0x0001, data = 0;     
		int count = arg->count, port = arg->port, timeout = arg->timeout;		
		while ( 1 ) {
//			for (y = 0x0001; y < 0xffff; y = y++) {		//Электронный счетчик
			for (y = 0x0001; y < 0xffff; y = y * 2) {	//Бегущий огонь
//			for (z = 0x0001, y = 0x0001; y <= 0xffff, z <= 0x10000;  z = z * 2, y = z - 1) {	//Наплывающий огонь
				for (x = 0; x < count; x++) {	//c = 100000 for visual STROBE control
						//usleep between writes kills the time!
						//suffix _p kills the time and dirtied signal
					//outw ((unsigned short int) y, (unsigned short int) port); //usleep(t);
					OUT_P_W ((unsigned short int) y, (unsigned short int) port);	//Одинаково с С функцией
				}
				//inw_p ((unsigned short int) port);
				IN_P_W (data, (unsigned short int) port);
			}
			//usleep(timeout); //500000 = 500мс
		}				   
        
	//local_irq_enable();
	asm("sti");
	// прерывания разрешены        
        
        return NULL;
}
 
int main(int argc, char* argv[])
{
        struct sched_param param;
        pthread_attr_t attr;
        pthread_t thread;
        int ret;
        
        someArgs_t args[NUM_THREADS];
        
        if (argc != 4) {
			printf("Usage: %s 0x<port> num_cycles intercycle_timeout \n", argv[0]);
			//printf("Usage: %s 1(on) 0(off)\n", argv[0]);
			return 1;
		} else {
			//unsigned int port;
			//unsigned int value;
			int ret;
			//sscanf(argv[1], "%x", &port);
			//sscanf(argv[2], "%x", &value);
			sscanf(argv[1], "%x", &args[0].port);
			//sscanf(argv[2], "%x", &data);
			sscanf(argv[2], "%d", &args[0].count);
			//sscanf(argv[4], "%d", &t);
			sscanf(argv[3], "%d", &args[0].timeout);
			//sscanf(argv[6], "%d", &b);
			printf("PORT: %x;\n", args[0].port);
		
			//args[0].id = 123456;
			//args[0].msg = "MESSAGA\n";        
			
	 
			/* Lock memory */
			if(mlockall(MCL_CURRENT|MCL_FUTURE) == -1) {
					printf("mlockall failed: %m\n");
					exit(-2);
			}
	 
			/* Initialize pthread attributes (default values) */
			ret = pthread_attr_init(&attr);
			if (ret) {
					printf("init pthread attributes failed\n");
					goto out;
			}
	 
			/* Set a specific stack size  */
			ret = pthread_attr_setstacksize(&attr, PTHREAD_STACK_MIN);
			if (ret) {
				printf("pthread setstacksize failed\n");
				goto out;
			}
	 
			/* Set scheduler policy and priority of pthread */
			ret = pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
			if (ret) {
					printf("pthread setschedpolicy failed\n");
					goto out;
			}
			param.sched_priority = 80;
			ret = pthread_attr_setschedparam(&attr, &param);
			if (ret) {
					printf("pthread setschedparam failed\n");
					goto out;
			}
			/* Use scheduling parameters of attr */
			ret = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
			if (ret) {
					printf("pthread setinheritsched failed\n");
					goto out;
			}
	 
			/* Create a pthread with specified attributes */
			ret = pthread_create(&thread, &attr, thread_func, (void*) &args[0]);
			if (ret) {
					printf("create pthread failed\n");
					goto out;
			}
	 
			/* Join the thread and wait until it is done */
			ret = pthread_join(thread, NULL);
			if (ret)
					printf("join pthread failed: %m\n");
					
	out:
			return ret;
		}
}
