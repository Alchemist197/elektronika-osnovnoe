#include <stdio.h>
#include <sys/io.h>
//#include <stdlib.h>
#include <unistd.h>
//LPT
//lp0 0x3bc; lp1 0x378; lp2 0x278;
#define PORT 0x378
#define TIME 1

//RECOMMEND FOR VISUAL CONTROL: ./wr_ata.o 0x170 0x1111 10000 0 50000 1

//ATAPI
/* IO ports for channel 2*/
#define ATA_DR		(0x1f0)		/* Data Register */
#define ATA_ER		(0x1f1)		/* Error Register */
#define ATA_INT		(0x1f2)		/* Interrupt Register */
#define ATA_ADR0	(0x1f3)		/* Addr0 Register */
#define ATA_ADR1	(0x1f4)		/* Addr1 Register */
#define ATA_ADR2	(0x1f5)		/* Addr2 Register */
#define ATA_SEL		(0x1f6)		/* Device Select Register */
#define ATA_CMD		(0x1f7)		/* Status/Command Register */
#define ATA_ADD		(0x3f6)		/* Additional Status/Command Register */

#define OUT_P_B(val,port)	\
    asm(			\
	"outb %%al, %%dx"	\
	::"a"(val),"d"(port)	\
    )


#define IN_P_B(val,port)	\
    asm(			\
	"inb %%dx, %%al"	\
	:"=a"(val)		\
	:"d"(port)		\
    )


#define IN_P_W(val,port)	\
    asm(			\
	"inw %%dx, %%ax"	\
	:"=a"(val)		\
	:"d"(port)		\
    )
    

int main (int argc, char* argv[])
{
	int port = 0x000;
	unsigned int data = 0x0000;
//    int hidata = 0x00;
//    int lodata = 0x00;
    int x = 0, b = 0, c = 0, t = 0, to = 0;
/*
	if (ioperm (PORT, 1, 1))
	{
		perror ("ioperm()");
		exit;
	}
	outb(x, PORT);
*/ 
	if (argc != 7) {
		printf("Usage: %s 0x<port> 0x<word> num_cycles time_out intercycle_timeout continue\n", argv[0]);
		//printf("Usage: %s 1(on) 0(off)\n", argv[0]);
		return 1;
	} else {
		//unsigned int port;
		//unsigned int value;
		int ret;
		//sscanf(argv[1], "%x", &port);
		//sscanf(argv[2], "%x", &value);
		sscanf(argv[1], "%x", &port);
		sscanf(argv[2], "%x", &data);
		sscanf(argv[3], "%d", &c);
		sscanf(argv[4], "%d", &t);
		sscanf(argv[5], "%d", &to);
		sscanf(argv[6], "%d", &b);
		printf("PORT: %x; DATA: %x;\n", port, data);
		
		ret = iopl(3);
		if ( ret ) { perror("iopl"); return 1; }
		
		if ( b ) {
			while ( 1 ) {
				for (x = 0; x < c; x++) {	//c = 100000 for visual STROBE control
					//usleep between writes kills the time!
					outw ((unsigned short int) data, (unsigned short int) port); //usleep(t);
//					OUT_P_B((unsigned char) data, (unsigned short int) port); //usleep(t);
//					outw_p ((unsigned char) 0xff, (unsigned short int) 0x1f7); usleep(t);
					//OUT_P_B((unsigned char) 0x00, (unsigned short int) port); usleep(t);
				}
				inw_p ((unsigned short int) port);
//				usleep(500000); //500мс
				usleep(to);
			}		
		} else {
			for (x = 0; x < c; x++) {
					outw ((unsigned short int) data, (unsigned short int) port); //usleep(t);
//					OUT_P_B((unsigned char) data, (unsigned short int) port); //usleep(t);
//					outw_p ((unsigned char) 0xff, (unsigned short int) 0x1f7); usleep(t);
					//OUT_P_B((unsigned char) 0x00, (unsigned short int) port); usleep(t);
			}
			inw_p ((unsigned short int) port);
//			usleep(500000); //500мс			
			usleep(to);
		}
/*		
		if ( b ) {	//Enable all
			while ( 1 ) {
				for (x = 0x00; x < 0xFF; x++) {
					outb_p ((unsigned char) x, (unsigned short int) ATA_DR); usleep(1000); //1мс					
					outb_p ((unsigned char) x, (unsigned short int) ATA_ER); usleep(1000); //1мс
					outb_p ((unsigned char) x, (unsigned short int) ATA_INT); usleep(1000); //1мс			
					outb_p ((unsigned char) x, (unsigned short int) ATA_ADR0); usleep(1000); //1мс
					outb_p ((unsigned char) x, (unsigned short int) ATA_ADR1); usleep(1000); //1мс
					outb_p ((unsigned char) x, (unsigned short int) ATA_ADR2); usleep(1000); //1мс			
					outb_p ((unsigned char) x, (unsigned short int) ATA_SEL); usleep(1000); //1мс
					outb_p ((unsigned char) x, (unsigned short int) ATA_CMD); usleep(1000); //1мс
					outb_p ((unsigned char) x, (unsigned short int) ATA_ADD); usleep(1000); //1мс
					 
					usleep(100000); //100мс
				}
				sleep(1);
			}
		} else {	//Disable all
			outw_p ((unsigned char) 0x00, (unsigned short int) ATA_DR);			
			outw_p ((unsigned char) 0x00, (unsigned short int) ATA_ER);
			outw_p ((unsigned char) 0x00, (unsigned short int) ATA_INT);			
			outw_p ((unsigned char) 0x00, (unsigned short int) ATA_ADR0);
			outw_p ((unsigned char) 0x00, (unsigned short int) ATA_ADR1);
			outw_p ((unsigned char) 0x00, (unsigned short int) ATA_ADR2);
			outw_p ((unsigned char) 0x00, (unsigned short int) ATA_SEL);			
			outw_p ((unsigned char) 0x00, (unsigned short int) ATA_CMD);			
			outw_p ((unsigned char) 0x00, (unsigned short int) ATA_ADD);			
		}
*/		
	}
	return 0;
}

