#include <stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/soundcard.h>
#include <errno.h>

#define BUF_SIZE 2048
#define MIXER "/dev/mixer"

int audio_fd, out_fd;

void onexit() {
	close(audio_fd);
	close(out_fd);
}


int main (int argc, char *argv[]) {
	//Setting up mixer parameters
	int mixer_fd, recmask, recsrc, vol;

	if ((mixer_fd = open(MIXER, O_RDWR)) == -1) {
		perror(MIXER);
		return EXIT_FAILURE;
	}
	if (ioctl(mixer_fd, SOUND_MIXER_READ_RECMASK, &recmask) == -1) {
		printf("Inactive mixr, try other device\n");
		return EXIT_FAILURE;
	}
	if ((recmask & SOUND_MASK_MIC) == 0 ) {
		printf("Microphone record does not supported\n");
		exit(1);
	}

	ioctl(mixer_fd, SOUND_MIXER_READ_RECSRC, &recsrc);
	if ((recsrc & SOUND_MASK_MIC) == 0 ) {
		recsrc = SOUND_MASK_MIC;
		ioctl(mixer_fd, SOUND_MIXER_WRITE_RECSRC, &recsrc);
	}
	
	ioctl(mixer_fd, SOUND_MIXER_READ_MIC, &vol);
	if (vol < 90) vol+=10;
	ioctl(mixer_fd, SOUND_MIXER_WRITE_MIC, &vol);
	
	close(mixer_fd);

	//Work with audio device
	int format, nchans, rate;
	int actlen, count;
	unsigned char buf[BUF_SIZE];
	
	if (argc < 2) {
		printf("Command: %s filename [rate]\n", argv[0]);
		return EXIT_SUCCESS;
	}
	
	out_fd = open(argv[1], O_CREAT|O_WRONLY, 0777);
	audio_fd = open("/dev/dsp", O_RDONLY, 0);
	atexit(onexit);
	format = AFMT_U8;
	
	ioctl(audio_fd, SNDCTL_DSP_SETFMT, &format);
	if (format != AFMT_U8) {
		printf("Error: requested format not supported");
		return EXIT_FAILURE;
	}
	
	nchans = 1;
	ioctl(audio_fd, SNDCTL_DSP_CHANNELS, &nchans);
	if (argc == 3) rate = atoi(argv[2]);
	else rate = 8000;
	
	ioctl(audio_fd, SNDCTL_DSP_SPEED, &rate);
	printf("Discretisation frequency is: %i Hz\n", rate);
	
	for (count = 0; count <= (rate*30*nchans); count += actlen) {
		actlen = read(audio_fd, buf, BUF_SIZE);
		write(out_fd, buf, actlen);
	}

	return EXIT_SUCCESS;

}
