/*
 * 
 * 
 *	singen.c
 *	A simple audio playback program that plays continuous 1 kHz sine wave.
 * 
 *	Description
 *	Copyright (C) 4Front Technologies, 2002-2004. Released under GPLv2/CDDL.
 *
 * 	This minimalistic program shows how to play audio with OSS. It outputs 1000 Hz sinewave signal (based on a 48 step lookup table).
 * 
 *	This is pretty much the simpliest possible audio playback program one can imagine. 
 *	It could be possible to make it even simplier by removing all error checking but that is in no way recommended. 
 */
 
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include "soundcard.h"
#include "allocarr.h"


#define BUF_SIZE 2048
#define MIXER "/dev/mixer"

int fd_out;
int sample_rate = 48000;

static int *sinebuf;

//This routine is a typical example of application routine that produces audio signal using synthesis. 
//This is actually a very basic "wave table" algorithm (btw). 
//It uses precomputed sine function values for a complete cycle of a sine function. 
//This is much faster than calling the sin() function once for each sample.
//In other applications this routine can simply be replaced by whatever the application needs to do.
static void
write_sinewave (int sine, int szbuf)
{
	static unsigned int phase = 0;	// Phase of the sine wave
	unsigned int p;
	int i;
	short buf[1024];		// 1024 samples/write is a safe choice 

	int outsz = sizeof (buf) / 2;

/*
	static int sinebuf[960] = { //50 Hz meander
		
		32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, //100
		32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, //100
		32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, //100
		32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, //100
		
		32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767, //80
		
		-32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, //100
		-32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, //100
		-32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, //100
		-32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, //100
		
		-32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, -32767, //80
		
	};
*/

	static int sinebuf_khz[48] = {

		0, 4276, 8480, 12539, 16383, 19947, 23169, 25995,
		28377, 30272, 31650, 32486, 32767, 32486, 31650, 30272,
		28377, 25995, 23169, 19947, 16383, 12539, 8480, 4276,
		0, -4276, -8480, -12539, -16383, -19947, -23169, -25995,
		-28377, -30272, -31650, -32486, -32767, -32486, -31650, -30272,
		-28377, -25995, -23169, -19947, -16383, -12539, -8480, -4276
	};

//The sinebuf[] table was computed for 48000 Hz. We will use simple sample rate compensation.
//We must prevent the phase variable from groving too large because that would cause cause arihmetic overflows after certain time. 
//This kind of error posibilities must be identified when writing audio programs 
//that could be running for hours or even months or years without interruption. 
//When computing (say) 192000 samples each second the 32 bit integer range may get overflown very quickly. 
//The number of samples played at 192 kHz will cause an overflow after about 6 hours.


	for (i = 0; i < outsz; i++)
	{
//	В исходном коде состояний 48, значит, p%48 = [0..47]
//	В 48000 раз в секунду 1000 герц укладываются 48 раз
//	Значит, каждое значение синусоиды должно продолжаться 24 раза, затем сменяться следующим (или начальным, если оно конечное)
//	Снимок отправляется частями в виде массива buf, чтобы не перегружать возможное число элементов для записи;
//	Затем действо продолжается с момента окончания записи предыдущего буф (куска фазы с прошлого)
		if (sine) {
			p = (phase * sample_rate) / 48000;		//p === phase
			phase = (phase + 1) % 4800;				//phase = [0..4799]
			buf[i] = sinebuf_khz[p % 48];				//p % 48= [0..47]
		}
		else if (szbuf) {
//	У нас всего два состояния, +ампл и -ампл, значит, p%2 = [0..1] - net
//	В 48000 раз в секунду 50 герц укладываются 960 раз.
//	Значит, 480 раз укладывается 0, затем 480 раз укладывается единица - и это, в результате, повторяется 50 раз за "снимок". 
//	Снимок отправляется частями в виде массива buf, чтобы не перегружать возможное число элементов для записи;
//	Затем действо продолжается с момента окончания записи предыдущего буф (куска фазы с прошлого)
			p = (phase * sample_rate) / 48000;	//Это просто для вжатия в 48000 герц передачи, всегда = прошлой фазе
			phase = (phase + 1) % 4800;
			buf[i] = sinebuf[p % szbuf];
		}
	}



//Proper error checking must be done when using write. It's also important to reporte the error code returned by the system.
	if (write ( fd_out, buf, sizeof (buf) ) != sizeof (buf))
	{
		perror ("Audio write");
		exit (-1);
	}
}


//The open_audio_device opens the audio device and initializes it for the required mode.
static int
open_audio_device (char *name, int mode)
{
  int tmp, fd;

  if ((fd = open (name, mode, 0)) == -1)
    {
      perror (name);
      exit (-1);
    }

//Setup the device. Note that it's important to set the sample format, number of channels and sample rate exactly in this order. Some devices depend on the order.

//Set the sample format
	tmp = AFMT_S16_NE;		/* Native 16 bits */
	if (ioctl (fd, SNDCTL_DSP_SETFMT, &tmp) == -1)
	{
		perror ("SNDCTL_DSP_SETFMT");
		exit (-1);
	}

	if (tmp != AFMT_S16_NE)
	{
		fprintf (stderr,
		   "The device doesn't support the 16 bit sample format.\n");
		exit (-1);
	}

//Set the number of channels
	tmp = 1;
	if (ioctl (fd, SNDCTL_DSP_CHANNELS, &tmp) == -1)
	{
		perror ("SNDCTL_DSP_CHANNELS");
		exit (-1);
	}

	if (tmp != 1)
	{
		fprintf (stderr, "The device doesn't support mono mode.\n");
		exit (-1);
	}


//Set the sample rate
	sample_rate = 48000;
	if (ioctl (fd, SNDCTL_DSP_SPEED, &sample_rate) == -1)
	{
		perror ("SNDCTL_DSP_SPEED");
		exit (-1);
	}
	
//Set playback volume
	//int level = (left)|(right<<8);
/*
	int level = (100)|(100);
	if (ioctl(fd, 
				SNDCTL_DSP_SETPLAYVOL, 
				&level
			) == -1)
	{
		perror ("SNDCTL_DSP_SETPLAYVOL");
		exit (-1);
	}
*/
//No need for error checking because we will automatically adjust the signal based on the actual sample rate. However most application must check the value of sample_rate and compare it to the requested rate.
//Small differences between the rates (10% or less) are normal and the applications should usually tolerate them. However larger differences may cause annoying pitch problems (Mickey Mouse).
  return fd;
}


int
main (int argc, char *argv[]) {
	
	//Setting up OSS driver
	//#whereis modprobe
	if (system("/sbin/modprobe snd-pcm-oss")) {
		printf("Cannot open OSS driver, exit\n");
		return EXIT_FAILURE;		
	}
	usleep(1000000);
	
	
		//Setting up mixer parameters
	int mixer_fd, recmask, recsrc, vol;

	if ((mixer_fd = open(MIXER, O_RDWR)) == -1) {
		perror(MIXER);
		return EXIT_FAILURE;
	}
	if (ioctl(mixer_fd, SOUND_MIXER_READ_RECMASK, &recmask) == -1) {
		printf("Inactive mixer, try other device\n");
		return EXIT_FAILURE;
	}
	if ((recmask & SOUND_MASK_MIC) == 0 ) {
		printf("Microphone record does not supported\n");
		exit(1);
	}

	ioctl(mixer_fd, SOUND_MIXER_READ_RECSRC, &recsrc);
	if ((recsrc & SOUND_MASK_MIC) == 0 ) {
		recsrc = SOUND_MASK_MIC;
		ioctl(mixer_fd, SOUND_MIXER_WRITE_RECSRC, &recsrc);
	}
	
	ioctl(mixer_fd, SOUND_MIXER_READ_MIC, &vol);
	if (vol < 90) vol+=10;
	ioctl(mixer_fd, SOUND_MIXER_WRITE_MIC, &vol);
	
	close(mixer_fd);

//Use /dev/dsp as the default device because the system administrator may select the device using the Bad xlink 'ossctl' program or some other methods

	char *name_out = "/dev/dsp";


//It's recommended to provide some method for selecting some other device than the default. We use command line argument but in some cases an environment variable or some configuration file setting may be better.

	int sine = 0;
	int szbuf = 0;
	int i = 0;
	int freq = 0;
//You can use the function int atoi (const char * str);.
//You need to include #include <stdlib.h> and use the function in this way:
//int x = atoi(argv[1]);	
	
	if (argc > 1) {		//Generiruem tablitzu samplov, iskhodya iz chastoty; esli ee net - vydaem 1000Hz sinewave
//		name_out = argv[1];
		//int freq = argv[1];

		freq = strtol(argv[1], NULL, 10);
		// string to long(string, endptr, base)
		
		printf ("%d\n", freq);
		if ((freq == 0)||(freq > 48000)) {
			printf ("Frequency is out of range!"); exit(1);
		}
		
		//Size of sinebuf = 48000 / freq
		
		szbuf = 48000 / freq;
		sinebuf = b_allocate( sizeof( int ), szbuf ); //new int[szbuf]; 
		
		for (i = 0; i < szbuf/2; i++) {	//FREQ Hz meander: half = 32767
			sinebuf[i] = 32767;
		}
		for (i; i < szbuf; i++) {	//FREQ Hz meander: half = -32767
			sinebuf[i] = -32767;
		}
				
	}
	else sine = 1;

//It's mandatory to use O_WRONLY in programs that do only playback. Other modes may cause increased resource (memory) usage in the driver. It may also prevent other applications from using the same device for recording at the same time.

	fd_out = open_audio_device (name_out, O_WRONLY);
	
	while (1) write_sinewave (sine, szbuf);

    if (sinebuf) b_deallocate( sinebuf );

	exit (0);
}
