<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<html><head>
<meta http-equiv="content-type" content="text/html; charset=windows-1252">
 <meta name="GENERATOR" content="SGML-Tools 1.0.9">
 <title>Linux I/O port programming mini-HOWTO: Using I/O ports in C programs</title>
 <link href="https://www.tldp.org/HOWTO/IO-Port-Programming-3.html" rel="next">
 <link href="https://www.tldp.org/HOWTO/IO-Port-Programming-1.html" rel="previous">
 <link href="https://www.tldp.org/HOWTO/IO-Port-Programming.html#toc2" rel="contents">
</head>
<body>
<a href="https://www.tldp.org/HOWTO/IO-Port-Programming-3.html">Next</a>
<a href="https://www.tldp.org/HOWTO/IO-Port-Programming-1.html">Previous</a>
<a href="https://www.tldp.org/HOWTO/IO-Port-Programming.html#toc2">Contents</a>
<hr>
<h2><a name="s2">2. Using I/O ports in C programs</a></h2>

<h2><a name="ss2.1">2.1 The normal method</a>
</h2>

<p>Routines for accessing I/O ports are in <code>/usr/include/asm/io.h</code>
(or <code>linux/include/asm-i386/io.h</code> in the kernel source
distribution). The routines there are inline macros, so it is enough
to <code>#include &lt;asm/io.h&gt;</code>; you do not need any additional
libraries.
</p><p>Because of a limitation in gcc (present in all versions I know of,
including egcs), you <em>have to</em> compile any source code that uses
these routines with optimisation turned on (<code>gcc -O1</code> or higher),
or alternatively use <code>#define extern static</code> before you
<code>#include &lt;asm/io.h&gt;</code> (remember to <code>#undef
extern</code>afterwards).
</p><p>For debugging, you can use <code>gcc -g -O</code> (at least with modern
versions of gcc), though optimisation can sometimes make the debugger
behave a bit strangely. If this bothers you, put the routines that use
I/O port access in a separate source file and compile only that with
optimisation turned on.
</p><p>
</p><h3>Permissions</h3>

<p>Before you access any ports, you must give your program permission to
do so. This is done by calling the <code>ioperm()</code> function (declared
in <code>unistd.h</code>, and defined in the kernel) somewhere near the start
of your program (before any I/O port accesses). The syntax is
<code>ioperm(from, num, turn_on)</code>, where <code>from</code> is the first port
number to give access to, and <code>num</code> the number of consecutive ports
to give access to. For example, <code>ioperm(0x300, 5, 1)</code> would give
access to ports 0x300 through 0x304 (a total of 5 ports). The last
argument is a Boolean value specifying whether to give access to the
program to the ports (true (1)) or to remove access (false (0)). You
can call <code>ioperm()</code> multiple times to enable multiple
non-consecutive ports. See the <code>ioperm(2)</code> manual page for details
on the syntax.
</p><p>The <code>ioperm()</code> call requires your program to have root privileges;
thus you need to either run it as the root user, or make it setuid
root. You can drop the root privileges after you have called
<code>ioperm()</code> to enable the ports you want to use. You are not
required to explicitly drop your port access privileges with
<code>ioperm(..., 0)</code> at the end of your program; this is done
automatically as the process exits.
</p><p>A <code>setuid()</code> to a non-root user does not disable the port access
granted by <code>ioperm()</code>, but a <code>fork()</code> does (the child process
does not get access, but the parent retains it).
</p><p><code>ioperm()</code> can only give access to ports 0x000 through 0x3ff; for
higher ports, you need to use <code>iopl()</code> (which gives you access to
all ports at once). Use the level argument 3 (i.e., <code>iopl(3)</code>) to
give your program access to <em>all</em> I/O ports (so be careful ---
accessing the wrong ports can do all sorts of nasty things to your
computer). Again, you need root privileges to call <code>iopl()</code>. See
the <code>iopl(2)</code> manual page for details.
</p><p>
</p><h3>Accessing the ports</h3>

<p>
</p><p>To input a byte (8 bits) from a port, call <code>inb(port)</code>, it returns
the byte it got. To output a byte, call <code>outb(value, port)</code> (please
note the order of the parameters). To input a word (16 bits) from
ports <code>x</code> and <code>x+1</code> (one byte from each to form the word, using
the assembler instruction <code>inw</code>), call <code>inw(x)</code>. To output a
word to the two ports, use <code>outw(value, x)</code>. If you're unsure of
which port instructions (byte or word) to use, you probably want
<code>inb()</code> and <code>outb()</code> --- most devices are designed for bytewise
port access. Note that all port access instructions take at least
about a microsecond to execute.
</p><p>The <code>inb_p()</code>, <code>outb_p()</code>, <code>inw_p()</code>, and <code>outw_p()</code>
macros work otherwise identically to the ones above, but they do an
additional short (about one microsecond) delay after the port access;
you can make the delay about four microseconds with <code>#define
REALLY_SLOW_IO</code> before you <code>#include &lt;asm/io.h&gt;</code>. These
macros normally (unless you <code>#define SLOW_IO_BY_JUMPING</code>, which
is probably less accurate) use a port output to port 0x80 for their
delay, so you need to give access to port 0x80 with <code>ioperm()</code>
first (outputs to port 0x80 should not affect any part of the
system). For more versatile methods of delaying, read on.
</p><p>There are manual pages for <code>ioperm(2)</code>, <code>iopl(2)</code>, and the above
macros in reasonably recent releases of the Linux manual page
collection.
</p><p>
</p><p>
</p><h2><a name="ss2.2">2.2 An alternate method: <code>/dev/port</code></a>
</h2>

<p>Another way to access I/O ports is to <code>open()</code> <code>/dev/port</code> (a
character device, major number 1, minor 4) for reading and/or writing
(the stdio <code>f*()</code> functions have internal buffering, so avoid
them). Then <code>lseek()</code> to the appropriate byte in the file (file
position 0 = port 0x00, file position 1 = port 0x01, and so on), and
<code>read()</code> or <code>write()</code> a byte or word from or to it.
</p><p>Naturally, for this to work your program needs read/write access to
<code>/dev/port</code>. This method is probably slower than the normal
method above, but does not need compiler optimisation nor
<code>ioperm()</code>. It doesn't need root access either, if you give a
non-root user or group access to <code>/dev/port</code> --- but this is a
very bad thing to do in terms of system security, since it is possible
to hurt the system, perhaps even gain root access, by using
<code>/dev/port</code> to access hard disks, network cards, etc. directly.
</p><p>You cannot use <code>select(2)</code> or <code>poll(2)</code> to read /dev/port,
because the hardware does not have a facility for notifying the CPU
when a value in an input port changes.
</p><p>
</p><p>
</p><hr>
<a href="https://www.tldp.org/HOWTO/IO-Port-Programming-3.html">Next</a>
<a href="https://www.tldp.org/HOWTO/IO-Port-Programming-1.html">Previous</a>
<a href="https://www.tldp.org/HOWTO/IO-Port-Programming.html#toc2">Contents</a>


</body></html>