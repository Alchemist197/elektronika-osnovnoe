#include <stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/soundcard.h>
#include <errno.h>

#define BUF_SIZE 2048
#define MIXER "/dev/mixer"

int audio_fd, out_fd;

void onexit() {
	close(audio_fd);
	close(out_fd);
}


int main (int argc, char *argv[]) {
	//Setting up mixer parameters
	int mixer_fd, recmask, recsrc, vol;

	if ((mixer_fd = open(MIXER, O_RDWR)) == -1) {
		perror(MIXER);
		return EXIT_FAILURE;
	}
	if (ioctl(mixer_fd, SOUND_MIXER_READ_RECMASK, &recmask) == -1) {
		printf("Inactive mixer, try other device\n");
		return EXIT_FAILURE;
	}
	if ((recmask & SOUND_MASK_MIC) == 0 ) {
		printf("Microphone record does not supported\n");
		exit(1);
	}

	ioctl(mixer_fd, SOUND_MIXER_READ_RECSRC, &recsrc);
	if ((recsrc & SOUND_MASK_MIC) == 0 ) {
		recsrc = SOUND_MASK_MIC;
		ioctl(mixer_fd, SOUND_MIXER_WRITE_RECSRC, &recsrc);
	}
	
	ioctl(mixer_fd, SOUND_MIXER_READ_MIC, &vol);
	if (vol < 90) vol+=10;
	ioctl(mixer_fd, SOUND_MIXER_WRITE_MIC, &vol);
	
	close(mixer_fd);

	//Work with audio device
	/*
	//Ниже приводится текст программы micrec.c, осуществляющей запись с микрофона (с целью сокращения листинга в программе опущена проверка ошибок):
	int format, nchans, rate;
	int actlen, count;
	unsigned char buf[BUF_SIZE];
	
	if (argc < 2) {
		printf("Command: %s filename [rate]\n", argv[0]);
		return EXIT_SUCCESS;
	}
	
	out_fd = open(argv[1], O_CREAT|O_WRONLY, 0777);
	audio_fd = open("/dev/dsp", O_RDONLY, 0);
	atexit(onexit);
	format = AFMT_U8;
	
	ioctl(audio_fd, SNDCTL_DSP_SETFMT, &format);
	if (format != AFMT_U8) {
		printf("Error: requested format not supported\n");
		return EXIT_FAILURE;
	}
	
	nchans = 1;
	ioctl(audio_fd, SNDCTL_DSP_CHANNELS, &nchans);
	if (argc == 3) rate = atoi(argv[2]);
	else rate = 8000;
	
	ioctl(audio_fd, SNDCTL_DSP_SPEED, &rate);
	printf("Discretisation frequency is: %i Hz\n", rate);
	
	for (count = 0; count <= (rate*30*nchans); count += actlen) {
		actlen = read(audio_fd, buf, BUF_SIZE);
		write(out_fd, buf, actlen);
	}

	return EXIT_SUCCESS;
	//Перед запуском программы необходимо с помощью программы-микшера выбрать микрофон в качестве источника записи
	// и установить значение уровней записи. Если у вас нет микрофона, вы можете записывать данные с аудио-CD. 
	//В этом случае необходимо запустить компакт-диск до запуска программы micrec. 
	//При записи с CD, можно установить значение nchans равным 2, что соответствует стерео-записи 
	//(такие же изменения следует сделать в приводимой ниже программе micplay). 
	//Учтите, что если вы попытаетесь записывать данные с неустановленного источника 
	//(например с входа Line In, к которому ничего не подключено), устройство dev/dsp может оказаться заблокированным,
	// и вам, возможно, придется перезагрузить систему.
	//Запуск программы micrec выполняется следующей командой: ./micrec outputfile [rate]
	//где outputfile – имя файла, в котором сохраняется запись, а необязательный параметр rate – частота дискретизации (значение по умолчанию – 8000). Программа micrec выполняет запись в течение 30 секунд. 
	*/
	
	//Далее следует текст программы micplay.c, предназначенной для воспроизведения файлов, созданных программой micrec.
	int audio_fd, in_fd, format;
	int nchans, rate, actlen;
	unsigned char buf[BUF_SIZE];
	
	if (argc < 2) {
		printf("Command: %s filename [rate]\n", argv[0]);
		return EXIT_SUCCESS;
	}
	
	in_fd = open(argv[1], O_RDONLY);
	audio_fd = open(DEVICE, O_WRONLY, 0);
	
	format = AFMT_U8;
	ioctl(audio_fd, SNDCTL_DSP_SETFMT, &format);
	if (format != AFMT_U8) {
		printf("Error: requested format not supported\n");
		return EXIT_FAILURE;
	}
	
	nchans = 1;
	ioctl(audio_fd, SNDCTL_DSP_CHANNELS, &nchans);
	if (argc == 3) rate = atoi(argv[2]);
	else rate = 8000;
	
	ioctl(audio_fd, SNDCTL_DSP_SPEED, &rate);
	printf("Discretisation frequency is: %i Hz\n", rate);

	while ((actlen = read(in_fd, buf, BUF_SIZE)) > 0)
		write(audio_fd, buf, actlen);
		
	close(audio_fd);
	close(in_fd);
	return EXIT_SUCCESS;
	//Программа micplay запускается командой ./micplay inputfile [rate], где inputfile – файл, созданный программой micplay, 
	// а rate – частота дискретизации (по умолчанию – 8000). 
	//Для получение эффекта ускорения или замедления попробуйте воспроизвести файл аудиоданных, 
	//указав частоту дискретизации, отличающуюся от той, с которой он был записан.


/*
Сохранение аудиоданных в wav-файле.

Программа micrec записывает данные на диск в “сыром” формате. Фактически, результат выполнения программы micrec – это просто оцифрованный звук. Файл не содержит информации ни о частоте дискретизации, ни о формате кодирования, ни о числе каналов. Для того, чтобы файлы, созданные программой micrec, могли воспроизводиться стандартными программами-проигрывателями, нам необходимо сохранять данные в одном из стандартных форматов. Самым простым форматом хранения цифрового аудио является формат Microsoft RIFF (wav-файлы). От нашего “сырого” формата wav-файлы отличаются только наличием заголовка, в котором указываются основные параметры аудиозаписи. Структура заголовка wav-файла соответствует следующей структуре, определенной на языке C:

typedef struct WAVHEADER_t {
// идентификатор формата файла
char riff[4];
// общий размер файла
long filesize;
// тип данных riff
char rifftype[4];
// идентификатор блока описания формата
char chunk_id1[4];
// размер блока описания формата
long chunksize1;
// идентификатор формата данных
short wFormatTag;
// число каналов
short nChannels;
// число сэмплов в секунду
long nSamplesPerSec;
// среднее число байт/сек
long nAvgBytesPerSec;
// размер одного блока (число каналов)*(число байтов на канал)
short nBlockAlign;
// число битов на один сэмпл
short wBitsPerSample;
// идентификатор области аудиоданных
char chunk_id2[4];
// длина области аудиоданных
long chunksize2;
} WAVHEADER_t;

Непосредственно за заголовком следуют аудиоданные. Определим процедуру fillheader, заполняющую заголовок wav-файла в соответствии с переданными ей параметрами:

void fillheader(WAVHEADER_t *header,
short channels,
long samplerate,
short databits,
long rawsize)
{
memcpy(header->riff , (const void *) "RIFF", 4);
memcpy(header->rifftype, (const void *) "WAVE", 4);
memcpy(header->chunk_id1, (const void *) "fmt ", 4);
header->chunksize1 = 16;
header->wFormatTag = WAVE_FORMAT_PCM;
memcpy(header->chunk_id2, (const void *) "data", 4);
header->nChannels = channels;
header->nSamplesPerSec = samplerate;
header->nBlockAlign = databits * channels / 8;
header->nAvgBytesPerSec = samplerate * header->nBlockAlign;
header->wBitsPerSample = databits;
header->chunksize2 = rawsize;
header->filesize = header->chunksize2 + 44;
};

Функции fillheader передаются следующие параметры: header – указатель на структуру WAVHEAER_t, которую следует заполнить; channels – число каналов (1 – моно, 2 – стерео); samplerate – частота дискретизации в Герцах; databits – число битов, кодирующих сэмпл (8, 16 или 32); rawsize – длина блока аудиоданных в байтах.

Разместив описание структуры WAVHEAER_t и объявление функции fillheader в файле wav.h, а текст функции fillheader в файле wav.c, мы можем написать следующую программу, преобразующую данные из формата программы micrec в формат wav:

#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include "wav.h"
#define BUF_SIZE 8192
int main(int argc, char * argv[]) {
struct WAVHEADER_t header;
struct stat fs;
int in_fd, out_fd, actlen, rate, nchans, fmt, aopt;
char buf[BUF_SIZE];
if (argc < 3) {
printf("команда: %s rawfile wavfile [-rxx] [-fxx] [-cxx]\n", argv[0]);
return EXIT_FAILURE;
}
rate = 8000;
nchans = 1;
fmt = 8;
if (stat(argv[1], &fs) == -1) {
perror(argv[1]);
return EXIT_FAILURE;
}
if ((in_fd = open(argv[1], O_RDONLY)) == -1) {
perror(argv[1]);
return EXIT_FAILURE;
}
if ((out_fd = open(argv[2], O_WRONLY|O_CREAT, 0777)) == -1) {
perror(argv[2]);
return EXIT_FAILURE;
}
while ((aopt = getopt (argc, argv, "r:f:c")) != -1)
switch (aopt) {
case 'r':
if (optarg != NULL) rate = atoi(optarg);
break;
case 'f':
if (optarg != NULL) fmt = atoi(optarg);
break;
case 'c':
if (optarg != NULL) nchans = atoi(optarg);
break;
}
fillheader(&header, nchans, rate, fmt, fs.st_size);
write(out_fd, &header, sizeof(header));
while ((actlen = read(in_fd, buf, BUF_SIZE)) > 0)
write(out_fd, buf, actlen);
close(out_fd);
close(in_fd);
}

Назовем нашу программу raw2wav. Эта программа позволяет преобразовывать любые данные, прочитанные из устройства /dev/dsp в формат wav. Программа запускается следующей командой:

./raw2wav rawfile wavfile [ -r rate ] [ -f format ] [ -c channels ]

где rawfile – имя входного файла, wavfile – имя результирующего файла в формате wav, rate – частота дискретизации, format – число бит, кодирующих сэмпл, а channels – количество каналов. Значения этих параметров по умолчанию соответствуют параметрам программы micrec.

Например, для того, чтобы преобразовать файл с записью 16-битного стереозвука с частотой дискретизации 24 кГц в wav-файл, следует дать команду:

./raw2wav myfile.raw myfile.wav –r24000 –f16 –c2

Небольшое замечание: программа-проигрыватель noatun, версии 1.0.1 поставляемая с Mandrake 8.0 и используемая по умолчанию в KDE, не всегда корректно воспроизводит полученные wav-файлы. По-видимому, проблема связана с самой neotune, так как другие проигрыватели, в том числе xmms и Windows Media Player, воспроизводят наши файлы без накладок.

Формат цифрового аудио позволяет записывать звуковой сигнал из внешнего источника и воспроизводить звук с достаточно высоким качеством, однако, если мы хоти создавать музыку на компьютере, более подходящим средством для нас является протокол MIDI, который будет рассмотрен в следующей статье.
*/
}
