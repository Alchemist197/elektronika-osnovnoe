#include <stdio.h>
#include <sys/io.h>
//#include <stdlib.h>
#include <unistd.h>
//LPT
//lp0 0x3bc; lp1 0x378; lp2 0x278;
#define PORT 0x378
#define TIME 1

//FLOPPY
/* IO ports */
#define FDC_SRB  (0x3f1)	/* Дополнительный регистр состояния */
#define FDC_DOR  (0x3f2)	/* Digital Output Register */
#define FDC_TDR  (0x3f3)	/* Регистр управления */
#define FDC_MSR  (0x3f4)	/* Main Status Register (input) */
#define FDC_DRS  (0x3f4)	/* Data Rate Select Register (output) */
#define FDC_DATA (0x3f5)	/* Data Register */
#define FDC_RESV (0x3f6)	/* Reserved */
#define FDC_DIR  (0x3f7)	/* Digital Input Register (input) */
#define FDC_CCR  (0x3f7)	/* Configuration Control Register (output) */

/* command unsigned chars (these are 765 commands + options such as MFM, etc) */
#define CMD_SPECIFY (0x03)	/* specify drive timings */
#define CMD_WRITE   (0xc5)	/* write data (+ MT,MFM) */
#define CMD_READ    (0xe6)	/* read data (+ MT,MFM,SK) */
#define CMD_RECAL   (0x07)	/* recalibrate */
#define CMD_SENSEI  (0x08)	/* sense interrupt status */
#define CMD_FORMAT  (0x4d)	/* format track (+ MFM) */
#define CMD_SEEK    (0x0f)	/* seek track */
#define CMD_VERSION (0x10)	/* FDC version */
#define CMD_MOTOR1	(0x1c)	/* Motor1 enable */

int main (int argc, char* argv[])
{
    int x = 0x00;
    int b = 0;
/*
	if (ioperm (PORT, 1, 1))
	{
		perror ("ioperm()");
		exit;
	}
	outb(x, PORT);
*/ 
	if (argc != 2) {
		//printf("Usage: %s 0x<port> 0x<value>\n", argv[0]);
		printf("Usage: %s 1(on) 0(off)\n", argv[0]);
		return 1;
	} else {
		//unsigned int port;
		//unsigned int value;
		int ret;
		//sscanf(argv[1], "%x", &port);
		//sscanf(argv[2], "%x", &value);
		sscanf(argv[1], "%d", &b);
		ret = iopl(3);
		if ( ret ) { perror("iopl"); return 1; }
		//outb_p ((unsigned char) value, (unsigned short int) port);
		if ( b ) {	//Enable all
			while ( 1 ) {
				for (x = 0x00; x < 0xFF; x++) {
					outb_p ((unsigned char) x, (unsigned short int) FDC_SRB); usleep(1000); //1мс
					outb_p ((unsigned char) x, (unsigned short int) FDC_DOR); usleep(1000); //1мс
					outb_p ((unsigned char) x, (unsigned short int) FDC_TDR); usleep(1000); //1мс			
					outb_p ((unsigned char) x, (unsigned short int) FDC_DRS); usleep(1000); //1мс
					outb_p ((unsigned char) x, (unsigned short int) FDC_DATA); usleep(1000); //1мс
					outb_p ((unsigned char) x, (unsigned short int) FDC_RESV); usleep(1000); //1мс			
					outb_p ((unsigned char) x, (unsigned short int) FDC_CCR); usleep(1000); //1мс
					usleep(100000); //100мс
				}
//				sleep(1);
			}
		} else {	//Disable all
			outb_p ((unsigned char) 0x00, (unsigned short int) FDC_SRB);			
			outb_p ((unsigned char) 0x00, (unsigned short int) FDC_DOR);
			outb_p ((unsigned char) 0x00, (unsigned short int) FDC_TDR);			
			outb_p ((unsigned char) 0x00, (unsigned short int) FDC_DRS);
			outb_p ((unsigned char) 0x00, (unsigned short int) FDC_DATA);
			outb_p ((unsigned char) 0x00, (unsigned short int) FDC_RESV);
			outb_p ((unsigned char) 0x00, (unsigned short int) FDC_CCR);			
		}
	}
	return 0;
}

