#include <stdio.h>   /* Стандартные объявления ввода/вывода */
#include <string.h>  /* Объявления строковых функций */
#include <unistd.h>  /* Объявления стандартных функций UNIX */
#include <fcntl.h>   /* Объявления управления файлами */
#include <errno.h>   /* Объявления кодов ошибок */
#include <termios.h> /* Объявления управления POSIX-терминалом */
#include <sys/types.h>
#include <sys/stat.h>

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0') 
//printf("Leading text "BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(byte));
//printf("m: "BYTE_TO_BINARY_PATTERN" "BYTE_TO_BINARY_PATTERN"\n",  BYTE_TO_BINARY(m>>8), BYTE_TO_BINARY(m));


/*
//Print the least significant bit and shift it out on the right. Doing this until the integer becomes zero prints the binary representation without leading zeros but in reversed order. Using recursion, the order can be corrected quite easily.
*/
void print_binary(int number)
{
    if (number) {
        print_binary(number >> 1);
        putc((number & 1) ? '1' : '0', stdout);
    }
} 


int fd; /* Файловый дескриптор для порта */
char buf[512];/*размер зависит от размера строки принимаемых данных*/
int outa=0; 
int iIn;
 
int open_port(void);
int main(void) { 
	port_set:
		fd = open("/dev/ttyS0", O_RDWR | O_NOCTTY | O_NDELAY); /*'open_port()' - Открывает последовательный порт */
		if (fd == -1) {
		/*
		* Возвращает файловый дескриптор при успехе или -1 при ошибке.
		*/
			printf("error port\n");
			perror("open_port: Unable to open /dev/ttyS0 - ");
		}
		else {
			struct termios options; /*структура для установки порта*/
			tcgetattr(fd, &options); /*читает параметры порта*/
			
			cfsetispeed(&options, B115200); /*установка скорости порта*/
			cfsetospeed(&options, B115200); /*установка скорости порта*/
			
			options.c_cflag &= ~PARENB; /*выкл проверка четности*/
			options.c_cflag &= ~CSTOPB; /*выкл 2-х стобит, вкл 1 стопбит*/
			options.c_cflag &= ~CSIZE; /*выкл битовой маски*/
			options.c_cflag |= CS8; /*вкл 8бит*/
//			options.c_cflag |= CS7; /*вкл 7бит*/
			tcsetattr(fd, TCSANOW, &options); /*сохранения параметров порта*/
		}
	read_port:
		iIn=read(fd,buf,8); /*чтения приходящих данных из порта*/
		
		//int n = foo();
		//printf("%x\n", n & 0xFF);
		//printf("%s",buf);
		print_binary(buf);
		printf("\n");
		//printf("%x\n",buf);
		
	//goto read_port;
	while (1) goto read_port;
	
	printf("stop1\n");
}
