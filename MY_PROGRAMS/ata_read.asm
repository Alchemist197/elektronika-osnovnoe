.586

stk segment stack use16
 db 1024 dup (?)
stk ends

base equ 1f0h

data segment use16
 buffer dw 256 dup (?)
 string db 20  dup (?)
data ends

code segment use16
assume cs:code,ss:stk,ds:data

ByteToStr       PROC
                xor     ah,ah
WordToStr:      xor     dx,dx
DWordToStr:     push    si
                push    di
                mov     si,10
                xor     cx,cx
Next_Digit:     push    ax
                mov     ax,dx
                xor     dx,dx
                div     si
                mov     bx,ax
                pop     ax
                div     si
                push    dx
                mov     dx,bx
                inc     cx
                or      bx,ax
                jnz     Next_Digit
                cld
Store_Digit_Loop:
                pop     ax
                add     al,'0'
                stosb
                loop    Store_Digit_Loop
                mov     BYTE PTR es:[di],'$'
                pop     di
                pop     si
                ret
ByteToStr       ENDP

ATA_READ PROC
mov dx,base+206h
mov al,2
out dx,al

mov dx,base+7
m1:
in   al,dx
test al,80h
jnz  m1

mov ecx,esi
shr ecx,24
or  cl,0E0h

mov dx,base+6
mov al,cl
out dx,al

mov  dx,base+7
m2:
in   al,dx
test al,80h
jnz  m2
test al,40h
jz   m2

mov eax,esi
mov dx,base+3
out dx,al          ;[0:7]
mov dx,base+4
shr eax,8
out dx,al          ;[8:15]
mov dx,base+5
shr eax,8
out dx,al          ;[16:23]

mov dx,base+7
mov al,020h
out dx,al

mov  dx,base+206h
m3:
in   al,dx
test al,80h
jnz  m3

mov  dx,base+7
m4:
in   al,dx
test al,08h
jz   m4

cld
mov dx,base
mov cx,256
rep insw

mov dx,base+206h
mov al,0
out dx,al
ret
ATA_READ ENDP

start:  ; ENTRY POINT

mov ax,data
mov ds,ax
mov es,ax
lea di,buffer
mov esi,0
                   ;ES:DI - buffer, ESI[0:27] - LBA Address, ESI[28] - Device; PIO mode
call ATA_READ

mov  di,offset string
mov  al,byte ptr [buffer+511]
call ByteToStr

mov ah,9
mov dx,offset string
int 21h

mov ax,4c00h
int 21h
code ends
end start
