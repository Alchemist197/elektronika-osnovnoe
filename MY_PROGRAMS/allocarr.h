#include <stdlib.h>
 
size_t* b_element_size_ptr_( void* buf )
{
    return ( size_t* )( ( char* )buf - sizeof( size_t ) );
}
 
size_t* b_length_ptr_( void* buf )
{
    return ( size_t* )( ( char* )buf - ( sizeof( size_t ) << 1 ) );
}
 
void* b_allocate( size_t siz, size_t len )
{
    char*
        buf = malloc( siz * len + ( sizeof( size_t ) << 1 ) );
    if( buf )
    {
        buf += sizeof( size_t ) << 1;
        *b_element_size_ptr_( buf ) = siz;
        *b_length_ptr_( buf ) = len;
    }
    return buf;
}
 
void b_deallocate( void* buf )
{
    if( buf )
        free( b_length_ptr_( buf ) );
}
 
size_t b_length( void* buf )
{
    return *b_length_ptr_( buf );
}
 
size_t b_bytes( void* buf )
{
    return b_length( buf ) * *b_element_size_ptr_( buf );
}


/* Usage:
#include <stdio.h>
 
int main( void )
{
    int*
        data = b_allocate( sizeof( int ), 1024 );
    if( data )
    {
        printf( "Length of data: %d\n", b_length( data ) );
        printf( "Size (in bytes): %d\n", b_bytes( data ) );
    }
    b_deallocate( data );
    return 0;
}
*/
