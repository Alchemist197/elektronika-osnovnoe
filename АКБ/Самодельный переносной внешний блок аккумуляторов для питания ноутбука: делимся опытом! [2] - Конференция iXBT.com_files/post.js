// Copyright (c) 2000-2013 mn@ixbt.com, http://forum.iXBT.com

var mode  = 2
var offset_y = 0
var timeout_id = 0

if (getCookie("setup")!=null){
	if (getCookie("setup").indexOf("I") != -1) var no_instruments=true
	if (getCookie("setup").indexOf("S") != -1) var no_smilies=true
	if (getCookie("setup").indexOf("Q") != -1) var no_quoteform=true
}

function insertcodes1(show_always, hide_attach){
if (show_always){
	no_instruments=""
	no_smilies=""
}
if ((no_instruments == true) && (no_smilies == true)) return
document.writeln('<table border=0 cellpadding=0 cellspacing=1 width=100%>');
if (no_instruments != true) {
	var attach_text = (hide_attach==null) ? '<area shape="rect" coords="247,0 262,18" href="javascript:;" onclick="if (forums_array[forum_number][4]!=\'\') {document.getElementById(\'attach\').style.display=\'\'} else {alert(language_text[201])}" title="'+language_text[202]+'">' : '';
	var paste_text = ((document.selection && document.selection.createRange) || document.getSelection || window.getSelection) ? '<a id="idpaste" href="javascript:p2(selection)" onMouseOver="get_selection()" onMouseUp="float_paste.style.top=-50;"><img border=0 src="/paste.gif" title="'+language_text[205]+'"></a>' : '';
	document.writeln(
		'<map name="codes1">'+
		'<area shape="rect" coords="0,0,18,18"    href="javascript:do_preview()" title="'+language_text[200]+'">'+
		'<area shape="rect" coords="23,0,40,18"   href="javascript:c1(0)"  title="'+codes_array[0][1]+'">'+
		'<area shape="rect" coords="41,0,58,18"   href="javascript:c1(1)"  title="'+codes_array[1][1]+'">'+
		'<area shape="rect" coords="59,0,76,18"   href="javascript:c1(2)"  title="'+codes_array[2][1]+'">'+
		'<area shape="rect" coords="77,0,100,18"  href="javascript:c1(3)"  title="'+codes_array[3][1]+'">'+
		'<area shape="rect" coords="101,0,122,18" href="javascript:c1(4)"  title="'+codes_array[4][1]+'">'+
		'<area shape="rect" coords="123,0,135,18" href="javascript:c1(5)"  title="'+codes_array[5][1]+'">'+
		'<area shape="rect" coords="136,0,152,18" href="javascript:c1(6)"  title="'+codes_array[6][1]+'">'+
		'<area shape="rect" coords="153,0,169,18" href="javascript:c1(7)"  title="'+codes_array[7][1]+'">'+
		'<area shape="rect" coords="170,0,191,18" href="javascript:c1(8)"  title="'+codes_array[8][1]+'">'+
		'<area shape="rect" coords="192,0,210,18" href="javascript:c1(9)"  title="'+codes_array[9][1]+'">'+
		'<area shape="rect" coords="211,0,233,18" href="javascript:c1(10)" title="'+codes_array[10][1]+'">'+
		'<area shape="rect" coords="234,0,254,18" href="javascript:c1(11)" title="'+codes_array[11][1]+'">'+
		'<area shape="rect" coords="255,0,275,18" href="javascript:c1(12)" title="'+codes_array[12][1]+'">'+
		'<area shape="rect" coords="276,0 294,18" href="javascript:c1(13)" title="'+codes_array[13][1]+'">'+
		'</map><map name="codes2">'+
		'<area shape="rect" coords="0,0 20,18" href="javascript:c2(14)" title="'+codes_array[14][1]+'">'+
		'<area shape="rect" coords="21,0 40,18" href="javascript:c2(15)" title="'+codes_array[15][1]+'">'+
		'<area shape="rect" coords="50,0 68,18" href="javascript:c1(16)" title="'+codes_array[16][1]+'">'+
		'<area shape="rect" coords="69,0 87,18" href="javascript:c1(17)" title="'+codes_array[17][1]+'">'+
		'<area shape="rect" coords="88,0 106,18" href="javascript:c1(18)" title="'+codes_array[18][1]+'">'+
		'<area shape="rect" coords="107,0 125,18" href="javascript:c1(19)" title="'+codes_array[19][1]+'">'+
		'<area shape="rect" coords="126,0 144,18" href="javascript:c1(20)" title="'+codes_array[20][1]+'">'+
		'<area shape="rect" coords="145,0 162,18" href="javascript:c1(21)" title="'+codes_array[21][1]+'">'+
		'<area shape="rect" coords="163,0 181,18" href="javascript:c1(22)" title="'+codes_array[22][1]+'">'+
		'<area shape="rect" coords="182,0 200,18" href="javascript:c1(23)" title="'+codes_array[23][1]+'">'+
		'<area shape="rect" coords="201,0 214,18" href="javascript:c1(24)" title="'+codes_array[24][1]+'">'+
		'<area shape="rect" coords="222,0 239,18" href="javascript:c1(25)" title="'+codes_array[25][1]+'">'+
		attach_text+
		'<area shape="rect" coords="263,0 278,18" href="javascript:;" onclick="if (document.getElementById(\'smiliesmap\').style.display==\'none\') {document.getElementById(\'smiliesmap\').style.display=\'\'} else {document.getElementById(\'smiliesmap\').style.display=\'none\'}" title="'+language_text[203]+'">'+
		'<area shape="rect" coords="279,0 298,18" href="javascript:c1(26)" title="'+codes_array[26][1]+'">'+
		'<area shape="rect" coords="299,0 315,18" href="javascript:do_help()" title="'+language_text[204]+'">'+
		'</map>'+
		'<tr><td class=s valign=center>'+paste_text+'<img border=0 src="/codesmap1.png" usemap="#codes1"> <img border=0 src="/codesmap2.png" usemap="#codes2"> <select name="select" class=s onChange="select_onchange(this.options[this.selectedIndex].value)"><option value="1">'+language_text[206]+'<option value="2" selected>'+language_text[207]+'<option value="3">'+language_text[208]+'</select></td></tr>'
	);
	var testform_cookie = getCookie('setup2')
	if ((testform_cookie!=null)){ 
		form_cookie=testform_cookie.split("|")
		if (form_cookie[8]!=null) {
			document.postform.select.selectedIndex=form_cookie[8]
			mode=document.postform.select.selectedIndex+1;
		}
	}
}
document.writeln('<tr><td class=s>')
if (typeof document.activeElement == "undefined") document.addEventListener("focus", function(event) {document.activeElement = (event.target.nodeType == Node.TEXT_NODE) ? event.target.parentNode : event.target;}, false);
}


function select_onchange(number){
mode=number
var testform_cookie = getCookie('setup2')
if (testform_cookie!=null) {form_cookie=testform_cookie.split("|")} else {form_cookie = new Array()}
form_cookie[8]=mode-1
document.cookie="setup2="+form_cookie.join("|")+"; expires=Sun, 01-Jan-2034 00:00:00 GMT; path=/;";
}

function do_help(){ 
window.open("/help/code.html", "_blank")
}

function p3(text){ 
if (text!="") paste(text, 1)
}

function p4(text1, text2){
if (document.selection) {
	document.postform.message.focus();
	document.postform.document.selection.createRange().text = text1+document.postform.document.selection.createRange().text+text2
} else if (typeof eval(document.postform.message.selectionStart) != "undefined") {
	d = document.postform.message;
	d.focus();
	pos = d.selectionEnd+text1.length;
	if (d.selectionStart!=d.selectionEnd) pos+= text2.length
	d.value = d.value.substring(0, d.selectionStart) + text1 + d.value.substring(d.selectionStart,d.selectionEnd) + text2 + d.value.substring(d.selectionEnd,d.value.length)
	d.selectionEnd = pos;
} else document.postform.message.value += text1+text2
}

function c1(num) {
line1 = "["+codes_array[num][0]+"]"
line2 = "[/"+codes_array[num][0]+"]"
if (num==22) {
	line1 = "[table][tr][td]"
	line2 = "[/td][/tr][/table]"
}
if (num==24) line2 = ""
if (mode==1) alert(codes_array[num][1]+"\n\n\n����������. ����� ������ ������� � ����� ��������� ����� ������� ��� ���������� �����, � ������ ����� ������ ������������ �������� ��������������� �����.")
	else if (mode==3) p4(line1, line2)
	else {
		if (codes_array[num][2] == null) p3(line1)
		else if (codes_array[num][3] == null) {
			txt=prompt(codes_array[num][2],"")
			if (txt!=null) p3(line1+txt+line2)
		} else {
			txt2=prompt(codes_array[num][3],"")
			if (txt2!=null) {
				txt=prompt(codes_array[num][2],"")
				if (txt!=null) {				
					if (txt2=="") p3(line1+txt+line2) 
						else 	if (codes_array[num][4] != null) p3("["+codes_array[num][0]+"="+txt+"]"+txt2+"[/"+codes_array[num][0]+"]")
											else p3("["+codes_array[num][0]+"="+txt2+"]"+txt+"[/"+codes_array[num][0]+"]")
				}			
			}
		}
	}
}

function c2(num) {
if (num == 13) line1 = "[list=1]" 
    else line1 = "[list][*]"
line2 = "\n[/list]"

if (mode==1) alert(codes_array[num][1])
	else if (mode==3) p4(line1+"[*]", line2)
	else {
		txt="1"
		list=""
		while ((txt!="") && (txt!=null)) {
			txt=prompt(codes_array[num][2],"")
			if (txt!="") list+="[*]"+txt+"\r"
			if (txt==null) return 
		} 
		p3(line1+list+"[/list]")
	}
}

function insert_quote(){
if ((no_instruments) && (navigator.userAgent.indexOf("Opera") == -1)) document.writeln('<br><br><a class=small3 href="javascript:p2(selection)" onMouseDown="get_selection()">'+language_text[205]+'</a>')
if (IE5 || Opera7 || Gecko){
	document.writeln('<br><br><div id=float_link><a class=small3 href="javascript:do_float(1)">'+language_text[209]+'</a></div>');
	window.onscroll = on_scroll;
	document.onmouseup = function (evt) {on_mouseup (evt)};
}
}

function on_mouseup(evt){
event2 = (document.all) ? window.event : evt;
if (!no_quoteform) setTimeout("paste_window("+event2.clientX+","+event2.clientY+")", 50);
}

function paste_window(X, Y){
if (window.getSelection) text = window.getSelection().toString();      
else if (document.getSelection) text = document.getSelection();                
else if (document.selection) text = document.selection.createRange().text;  
id = (document.activeElement) ? document.activeElement.id : "";
if ((text!="") && (id!='idpaste') && (id!='message') && (id!='username')){
	paste_user_name=selected_name;
	selected_name="";
	float_paste.style.display="";
	float_paste.style.left=(X-50)+"px";
	var scrollTop=document.body.scrollTop || document.documentElement.scrollTop;
	scrollTop+=Y-60;
	float_paste.style.top=scrollTop+"px";
	document.onmouseup = null;
	timeout_id=setTimeout("hide_paste_window()", 2500);
} else {
	hide_paste_window()
}
}

function hide_paste_window(){
clearTimeout(timeout_id);
document.onmouseup = (IE5 || Opera7 || Gecko) ? function (evt) {on_mouseup (evt)} : null;
float_paste.style.display='none';
}

function do_float(isfloat){
if (isfloat) {
	post.style.position="fixed";
	post.style.boxShadow="1px 1px 7px #000";
	post.style.backgroundColor="white";
	post.style.bottom="0px";
	post.style.left="0px";
	post.style.width="100%";
	float_link.innerHTML='<a class=small3 href="javascript:do_float(0)">'+language_text[210]+'</a>'
} else {
	post.style.position=""
	post.style.boxShadow=""
	post.style.backgroundColor="white"
	float_link.innerHTML='<a class=small3 href="javascript:do_float(1)">'+language_text[209]+'</a>'
}
}

function on_scroll(evt){
height = (document.all) ? document.body.offsetHeight : window.innerHeight;
if (document.postform != null) 
	if (post.style.position != "") setTimeout('document.getElementById("post").style.top=document.body.scrollTop+height-post.offsetHeight-10', 50)
}

function message_resize_onmousemove(evt) {
pageY = (document.all) ? window.event.clientY : evt.pageY;
newheigth=document.postform.message.offsetHeight-6-offset_y+pageY;
if (newheigth>50) {
	document.postform.message.style.height=newheigth+"px"
	offset_y=pageY
}
return false
}

function message_resize_onmousedown(evt) {
pageY = (document.all) ? window.event.clientY : evt.pageY;
document.onmousemove = (Gecko)? function (evt) {message_resize_onmousemove (evt)} : message_resize_onmousemove;
document.onmouseup = message_resize_onmouseup
offset_y = pageY
return false
}

function message_resize_onmouseup() {
document.onmousemove = null;
document.onmouseup = (IE5 || Opera7 || Gecko) ? function (evt) {on_mouseup (evt)} : null;
var testform_cookie = getCookie('setup2');
if (testform_cookie!=null) {form_cookie=testform_cookie.split("|")} else {form_cookie = new Array()};
form_cookie[7]=document.postform.message.offsetHeight-6;
document.cookie="setup2="+form_cookie.join("|")+"; expires=Sun, 01-Jan-2034 00:00:00 GMT; path=/;";
return false
}

function insertcodes2(hide_attach){
if ((IE5) || (Opera7)) {document.writeln('<img src="/blank.gif" onmousedown="message_resize_onmousedown(event)" width=100% height=7 style="cursor: n-resize"><br>')} else {document.writeln('<div onmousedown="message_resize_onmousedown(event)" style="width:100%; height:7px; cursor: n-resize"></div>')}
if ((no_instruments == true) && (no_smilies == true)) return;
var tempstyle = (no_smilies) ? ' style="display:none"' : '';
var text = '</td></tr><map name=smilies>';
for (i=0; i<smilies_array.length; i++) {
	text += '<area shape="rect" coords="'+smilies_array[i][0]+'"    href="javascript:p3(\''+smilies_array[i][1]+'\')" title="'+smilies_array[i][2]+'">';
}
text += '</map><tr id=smiliesmap'+tempstyle+'><td class=s><img src="/smileymap.gif" border=0 usemap="#smilies"></td></tr></table>';
document.writeln(text);

if (hide_attach==null){
	if (forums_array[forum_number][4]!=''){
		document.writeln('</td></tr><tr id=attach style="display:none" class='+table_class+'><td class=n id="t18" align=right width=18%>'+language_text[211]+':<div class=small3>('+forums_array[forum_number][4]+' '+language_text[212]+' '+ forums_array[forum_number][5]+' '+language_text[213]+')</div></td><td class=s valign=top>');
		var event = (Opera7) ? "onChange" : "onClick";
		for (i=1; i<=attach_max; i++) {
			var tmp1 = (i==1) ? '': 'style="display:none"';
			var tmp2 = (i==attach_max) ? '': event+'="document.getElementById(\'attach'+(i+1)+'\').style.display=\'\';"'
			document.writeln('<div id=attach'+i+' '+tmp1+'>'+i+': <input type="file" name="attachfile'+i+'" value="" size=40 maxlength=200 class=s '+tmp2+'></div>');
		}
	}
}
}

function do_preview(){
if (document.postform.id.value.indexOf("preview")==-1) old_action=document.postform.id.value;
id_array=document.postform.id.value.split(":");
document.postform.id.value='preview:'+id_array[1]+':'+id_array[2];
window.open("", "preview", "toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes, resizable=yes, top=0, left=0");   
document.postform.target="preview";   
document.postform.submit();  
}

function pip(ipn){
document.ip.elements[0].value=ipn
document.ip.elements[0].focus()
}
