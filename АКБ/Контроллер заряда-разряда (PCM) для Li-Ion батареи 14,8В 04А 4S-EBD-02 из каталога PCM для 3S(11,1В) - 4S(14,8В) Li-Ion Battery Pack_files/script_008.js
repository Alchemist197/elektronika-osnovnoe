var flushTimeout;

	$(function(){
		var offerID = getUrlVars()["offerID"];
		if(offerID){
			$.getJSON(elementAjaxPath + "?act=getOfferByID&id=" + offerID, function(data) {
				$.each(data[0], function(elementIndex, elementValue){
					var $searchCurrentProperty = $(".elementSkuPropertyValue[data-name='" + elementIndex + "'][data-value='" + elementValue + "']");
					$searchCurrentProperty.find(".elementSkuPropertyLink").trigger("click");
				});
			});
		}

		function getUrlVars() {
		    var vars = {};
		    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
		        vars[key] = value;
		    });
	   		return vars;
		}

	});

	$(function(){
		$(document).on("click", ".question", function(e) {
			e.preventDefault();
			$("#hint").remove();
			$("#catalogElement").append(
				$('<div id="hint">').html("<span>" + $(this).siblings().text() + "</span><ins></ins><p>" + $(this).data("description") + "</p>").css({
					"top": ($(this).offset().top - 20) + "px",
					"left": ($(this).offset().left + 40) + "px"
				})
			);
		});
		$(document).on("click", "#hint ins", function(e) {
			$("#hint").remove();
		});
	});

	$(function(){
	
		var $catalogElement = $("#catalogElement");
		var $elementNavigation = $("#elementNavigation");
		var $elementTools = $("#elementTools");

		if($elementNavigation.find(".tabs").height() > $elementTools.find(".fixContainer").height()){
			var maxScroll = $catalogElement.offset().top - $elementNavigation.find(".tabs").outerHeight();
		}else{
			var maxScroll = $catalogElement.offset().top - $elementTools.find(".fixContainer").outerHeight() - 36;
		}
		var navOffset = $elementTools.offset().top;

		var scrollControl = function(event){
			var curScrollValueY = (event.currentTarget.scrollY) ? event.currentTarget.scrollY : $(window).scrollTop()
			if(curScrollValueY <= maxScroll + $catalogElement.height()){
				if(navOffset <= curScrollValueY){
					$elementNavigation.addClass("fixed").find(".tabs").removeClass("maxScroll");
					$elementTools.addClass("fixed").find(".fixContainer").removeClass("maxScroll");	;
				}else{
					$elementNavigation.removeClass("fixed").find(".tabs").removeClass("maxScroll");
					$elementTools.removeClass("fixed").find(".fixContainer").removeClass("maxScroll");
				}
			}else{
				$elementNavigation.removeClass("fixed").find(".tabs").addClass("maxScroll");
				$elementTools.removeClass("fixed").find(".fixContainer").addClass("maxScroll");
			}

		};

		$(window).on("load scroll resize", scrollControl);

		var scrollToPropertyList = function(event){
			
			$("html, body").animate({
				scrollTop: $("#elementProperties").offset().top + "px"
			}, 250);
			
			return event.preventDefault();
		
		};
		
		$(document).on("click", ".morePropertiesLink", scrollToPropertyList);
	
	});

	$(function(){

		var $_this 	  = {},
			$_parn    = {},
			$_addCart = {};


		var _tmpPrice = null,
			_sumPrice = null,
			_tmpDisnt = null,
			_sumDisnt = null,
			_prs = null,
			_prd = null;


		var lsClick = function(event){

			var __priceID = event.data.lsWindow === true ? "#setWPrice" : "#setPrice",
				__priceDS = event.data.lsWindow === true ? "#setWDisnt" : "#setDisnt",
				__addCart = event.data.lsWindow === true ? "#setWindowPrice .addCart" : ".rt .addCart",
				__prodCls = ".setElement",
				__disbCLs = "disabled",
				__priceDT = "price",
				__priceDI = "discount",
				__textDat = "text";

			var $_setPrice = $(__priceID),
				$_setDisnt = $(__priceDS);


			$_this = $(this);
			$_parn = $_this.parents(__prodCls);

			$_this.toggleClass(__disbCLs);
			$_parn.toggleClass(__disbCLs);

			_prs = $_this.hasClass(__disbCLs) ? -parseInt($_parn.data(__priceDT)) : parseInt($_parn.data(__priceDT));
			_prd = $_this.hasClass(__disbCLs) ? -(parseInt($_parn.data(__priceDT)) + Math.ceil($_parn.data(__priceDI))) : (parseInt($_parn.data(__priceDT)) + Math.ceil($_parn.data(__priceDI)));

			_tmpPrice = $_setPrice.html().replace(/[0-9]/g, '');
			_sumPrice = parseInt($_setPrice.html().replace(/[^0-9]/g, '')) + _prs;

			_tmpDisnt = $_setDisnt.html().replace(/[0-9]/g, '');
			_sumDisnt = parseInt($_setDisnt.html().replace(/[^0-9]/g, '')) + _prd;

			$_setPrice.html(
				formatPrice(_sumPrice) + _tmpPrice
			);

			$_setDisnt.html(
				formatPrice(_sumDisnt) + _tmpDisnt
			);

			_sumPrice == _sumDisnt ? $_setDisnt.hide() : $_setDisnt.show();

			$_addCart = $(__addCart);
			$_addCart.text($_addCart.data(__textDat)).attr("href", "#").removeClass("added");

		};

		var oSetWindow = function(event){
			$("#setWindow").toggle();
			event.preventDefault();
		};

		$(document).on("click", ".sCheck", {lsWindow : false}, lsClick);
		$(document).on("click", ".sWindowCheck", {lsWindow: true}, lsClick);
		$(document).on("click", "#setWindow .close, #catalogElement .addSet, #setWindow .closeWindow", oSetWindow);

	});

	$(function(){

		var sendRating = function(event) {
			var $this = $(this);
			var $win = $("#elementError");
			var trig = event.data.dest == "good" ? true : false;

			$.getJSON(ajaxPath + "?act=rating&id=" + $this.data("id") + "&trig=" + trig, function(data) {
				if (data["result"]) {
					$this.find("span").html(
						parseInt($this.find("span").html()) + 1
					);
				} else {
					$win.show().find("p").text(data["error"]).parent().find(".heading").text(data["heading"]);
				}
			});
			event.preventDefault();
		};

		var calcRating = function(event) {
			var $this = $(this);
			var $mover = $this.find(".m");
			var $ratingInput = $("#ratingInput");
			var position = $this.offset().left;
			var curWidth = $this.width() / 5;
			var value = Math.ceil((event.pageX - position) / curWidth);

			$mover.stop().css({
				"width": (value * 20) + "%"
			});

			if (event.data.action) {
				$ratingInput.val(value);
			};

		};

		var callRating = function(event) {
			var $this = $(this);
			var $ratingInput = $("#ratingInput");
			var value = $ratingInput.val() != "" ? parseInt($ratingInput.val()) : 0;

			clearTimeout(flushTimeout);
			flushTimeout = setTimeout(function() {
				$this.find(".m").css({
					"width": (value * 20) + "%"
				})
			}, 500);
		};

		var usedSelect = function(event) {
			var $this = $(this);
			var $ul = $(".usedSelect");
			var usedInput = $("#usedInput");

			$ul.find("a").removeClass("selected");
			$this.addClass("selected");
			$("#usedInput").val($this.data("id"));

			event.preventDefault();
		};

		var reviewSubmit = function(event) {
			var $this = $(this);
			var $form = $(this).parents("form");
			var formData = $form.serialize();
			var $win = $("#elementError");

			$.getJSON(ajaxPath + "?act=newReview&" + formData + "&iblock_id=" + $this.data("id"), function(data) {
				$win.show().find("p").text(data["message"]).parent().find(".heading").text(data["heading"]);
				data["reload"] ? $win.data("reload", 1) : void 0;
			});

			event.preventDefault();
		};

		var windowClose = function(event) {
			var $win = $("#elementError");
			$win.data("reload") ? document.location.reload() : $("#elementError").hide();
			event.preventDefault();
		};

		var showReview = function(event) {
			var $this = $(this);
			var $reviews = $("#reviews");
			if ($this.data("open") == "N") {
				$reviews.children("li").removeClass("hide");
				$this.data("open", "Y").html(CATALOG_LANG["REVIEWS_HIDE"]);
			} else {
				$reviews.children("li").slice(3).addClass("hide")
				$this.data("open", "N").html(CATALOG_LANG["REVIEWS_SHOW"]);
			}
			event.preventDefault();
		};

		var showReviewForm = function(event){
			var $newReview = $("#newReview");
			$newReview.show();
			$("html, body").animate({
				scrollTop: $newReview.offset().top + "px"
			}, 250);
			return event.preventDefault();
		};

		var scrollToReviews = function(event){
			var $newReview = $("#catalogReviews");
			$("html, body").animate({
				scrollTop: $newReview.offset().top + "px"
			}, 250);
			return event.preventDefault();
		};

		$(document).on("click", ".showReviewDetail", function(event) {
			var $this = $(this);
			var $reviewContainer = $("#reviews");

			scrollElement(
				$reviewContainer.children("li").eq(
					$this.data("cnt")
				).offset().top
			);
			event.preventDefault();
		});

		$(document).on("click", ".good", {
			dest: "good"
		}, sendRating);
		$(document).on("click", ".bad", {
			dest: "bad"
		}, sendRating);

		//rating review
		$(document).on("mousemove", "#newRating .rating", {action: false}, calcRating);
		$(document).on("mouseleave", "#newRating .rating", callRating)
		$(document).on("click", "#newRating .rating", {action: true}, calcRating);
		$(document).on("click", ".usedSelect a", usedSelect);
		$(document).on("click", "#showallReviews", showReview);
		$(document).on("click", "#newReview .submit", reviewSubmit);
		$(document).on("click", "#elementErrorClose, #elementError .close", windowClose);
		$(document).on("click", ".reviewAddButton", showReviewForm);
		$(document).on("click", ".countReviewsTools", scrollToReviews);
	});



/**
 * Контроллер для карточки товара
 *
 */
var itemControl = (function($) {
    var params = {
            debug: false, //режим отладки
        },
        $countInput = null, //инпут смены количества
        $countIncTrigger = null, //плюс количества
        $countDecTrigger = null, //минус количества
        $priceVariantLines = null, //линии с ценовыми параметрами
        $totalPriceView = null, //отображение общей цены
        $totalDiscountView = null, //отображение общей скидки
        isInit = false; //был ли ранее уже вызван init()

    //выводим интерфейс
    return{
        init: init,
        checkCountInputRange: checkCountInputRange,
        updatePriceTable: updatePriceTable
    };

    /**
     * Инициализация всех вызовов
     *
     */

    function init(){

        $countInput = $('[data-count-input]');
        $countIncTrigger = $('[data-count-inc-trigger]');
        $countDecTrigger = $('[data-count-dec-trigger]');
        $priceVariantLines = $('[data-price-variant-line]');
        $totalPriceView = $('[data-total-price]'); //вывод цены
        $totalDiscountView = $('[data-total-discount]'); //вывод скидки
        updatePriceTable();
        _initDecCounter();
        _initIncCounter();
        if (isInit)
        {
            return;
        }
        _initCounter();



        isInit = true;
    }

    /**
     * Посылает собщение об ошибке в консоль
     *
     */
    function _sendError(msg){
        if (params.debug)
        {
            console.error('[itemControl] ' + msg);
        }
    };

    /**
     * Посылает собщение в консоль
     *
     */
    function _sendInfo(msg){
        if (params.debug)
        {
            console.log('[itemControl] ' + msg);
        }
    };

    /**
     * Провешиваем логику на "минус"
     *
     */
    function _initDecCounter(){
        if ($countDecTrigger.length)
        {
            $countDecTrigger.on('click', function(e){
                _counterDecClickEventHandler(e);

                return false;
            });
        } else {
            _sendError('Не определен плюс увеличения количества');
        }
    }

    /**
     * Провешиваем логику на "плюс"
     *
     */
    function _initIncCounter(){
        if ($countIncTrigger.length)
        {
            $countIncTrigger.on('click', function(e){
                _counterIncClickEventHandler(e);

                return false;
            });
        } else {
            _sendError('Не определен минус увеличения количества');
        }
    }

    /**
     * Провешиваем логику на инпут с количеством
     *
     */
    function _initCounter(){
        if ($countInput.length)
        {
            $countInput.on('change', function(e){
                _counterChangeEventHandler(e);
            });
        } else {
            _sendError('Не определен инпут количества');
        }
    }

    /**
     * Возвращает число с отделенными тысячями
     *
     */
    function _priceFormat(nStr) {
        var _nStr = nStr || 0;

        _nStr = String(_nStr);

        var remainder = _nStr.length % 3;
        return (_nStr.substr(0, remainder) + _nStr.substr(remainder).replace(/(\d{3})/g, ' $1')).trim();
    }

    /**
     * Проверяем не выходит ли значение инпута за диапозоны
     *
     */
    function checkCountInputRange(){

        if ($countInput.length)
        {
            $.each($countInput, function(){
                var $this = $(this);

                if ($this.val()<=0)
                {
                    $this.val(1);
                }
                else if ($this.val()>999)
                {
                    $this.val(999);
                }
                $("#addbutton").data("quantity",parseInt($this.val()));
            });

        } else {
            _sendError('Не определен инпут количества');
        }
    }

    /**
     * Обновляем вывод цены
     *
     */
    function _updateTotalPriceView(price){
        var _price = price || 0;

        if ($totalPriceView.length)
        {
            $.each($totalPriceView, function(){
                $(this).empty().html(_priceFormat(_price));
            });
        } else {
            _sendError('Не определен view для отображения цены');
        }
    }

    /**
     * Обновляем вывод скидки
     *
     */
    function _updateTotalDiscountView(disc){
        var _disc = disc || 0;

        if ($totalDiscountView.length)
        {
            $.each($totalDiscountView, function(){
                $(this).html(_priceFormat(_disc));
            });
        } else {
            _sendError('Не определен view для отображения скидки');
        }
    }

    /**
     * Обновляем актуальные линии с ценами,
     * а также обновляем общую цену и общую скидку
     *
     */
    function updatePriceTable(){
        if ($priceVariantLines.length)
        {
            $.each($priceVariantLines, function(){
                var $line = $(this);

                if ($countInput.length)
                {
                    /*	возможно стоит переписать, т.к. у нас два инпута:
                     для адаптива и обычный. могут быть сложно
                     отловимые баги с их не-синхронизацией.
                     в текущей реалзации предполагаем, что первый
                     взятый инпут имеет актуальное значение
                     */

                    if( parseInt($countInput.val()) >= parseInt($line.data("min")) && parseInt($countInput.val()) <= parseInt($line.data("max")) )
                    {
                        /*parseInt($('.fixContainer .changePrice').text($line.data('price')));*/
                        $line.addClass("is-active");

                        _updateTotalPriceView(
							$line.data('price') * $countInput.val()
                        );

                        _updateTotalDiscountView(
                            /*(parseInt(parseInt($('.fixContainer .changePrice').text())) * parseInt($countInput.val()) - ($line.data("price")) * parseInt($countInput.val()))*/
                        );
                        if (window.price) {
                            $('.fixContainer .counter-price').text(' x ' + window.price);
                        } else {
                            $('.fixContainer .counter-price').text(' x ' + formatPrice($line.data('price')) + ' руб.');
						}

                    }
                    else{
                        $line.removeClass("is-active");
                    }
                } else {
                    _sendError('Не определен инпут количества');

                    return false;
                }

            });
        } else {
            _sendError('Не найдеры линиии с ценами');
        }

    }

    $('.changePrice').on('change', function(){alert('Price changed');})

    /**
     * Handler изменения инпута
     */
    function _counterChangeEventHandler(e){
        var value = e.target.value;

        checkCountInputRange();
        $("#addbutton").data("quantity",parseInt(value));
        //кандидат на переписывание,
        // синхронизация инпутов в адаптиве и десктопе
        $.each($countInput, function(){
            $(this).val(value);
        });

        updatePriceTable();
    }

    /**
     * Handler клика по увеличениею количества
     */
    function _counterIncClickEventHandler(e){

        if ($countIncTrigger.length)
        {
            if ($countInput.length)
            {
                $.each($countInput, function(){
                    var $this = $(this);
                    $this.val(parseInt($this.val())+1 );
                    checkCountInputRange();
                    updatePriceTable();
                    $("#addbutton.addCart").attr('data-quantity', $this.val());
                    $("input.qty").attr('value', $this.val());
                });

            } else {
                _sendError('Не определен инпут количества');
            }

        } else {
            _sendError('Не определен триггер увеличения количества');
        }
    }

    /**
     * Handler клика по уменьшению количества
     */
    function _counterDecClickEventHandler(e){

        if ($countDecTrigger.length)
        {
            if ($countInput.length)
            {
                $.each($countInput, function(){
                    var $this = $(this);

                    $this.val( parseInt($this.val()) - 1 );

                    checkCountInputRange();
                    updatePriceTable();
                    $("#addbutton.addCart").attr('data-quantity', $this.val());
                    $("input.qty").attr('value', $this.val());
                });

            } else {
                _sendError('Не определен инпут количества');
            }

        } else {
            _sendError('Не определен триггер увеличения количества');
        }
    }

}($));

$(document).ready(function() {
    itemControl.init();
    $('.b-count-control__input').change(function(){
        $("#addbutton.addCart").attr('data-quantity', $(this).val());
        $("input.qty").attr('value', $(this).val());
	})
});