//*********************************************************************************
// PIC18F252
#include <pic18.h>
//*********************************************************************************
__CONFIG(1,ECRA6 & OSCSDIS);
__CONFIG(2,BORDIS & PWRTEN & WDTPS128);
__CONFIG(3,CCP2RC1);
__CONFIG(4,DEBUGDIS & LVPDIS & STVRDIS);
__CONFIG(5,UNPROTECT);
__CONFIG(6,WRTEN);
__CONFIG(7,TRU);
//*********************************************************************************
#include "macros.h"
#include "var.h"
#include "usart.h"
//********************************************************************************
//*********************************************************************************
//*********************************************************************************
#define _CPLD_COUNT_RESET {_CPLD_RESET = 1;\
						   _NOP;_NOP;_NOP;_NOP;_NOP;_NOP;\
						   _CPLD_RESET = 0;}
//*********************************************************************************
//������ ������ CPLD ��� ������ � RAM
void StartRAM(void)
{
	//CLK �� "0"
	_CPLD_CLK = 0;	
	//����� ��������
	_CPLD_COUNT_RESET;
	//������ ������ ��� ������ � RAM
	_CPLD_RUN = 1;	
}
//*********************************************************************************
//���������� ������ CPLD...
void StopRAM(void)
{
	//���������� ������
	_CPLD_RUN = 0;
	//��������� ������� CPLD
	_CPLD_COUNT_RESET;
}
//*********************************************************************************
//�������� ������ � CPLD �� ����������������� ����������
void SendSerialCPLD(UINT data, UCHAR len)
{
	while(len--)
	{	
		//��������� ��� ������
		if (data & 0x1) _CPLD_SHIFT_DATA = 1;
		else _CPLD_SHIFT_DATA = 0;
		data >>= 1;
		//����
		_CPLD_SHIFT_CLK = 1;
		_NOP;_NOP;_NOP;_NOP;_NOP;_NOP;
		_CPLD_SHIFT_CLK = 0;
	}
	//�������...
	_CPLD_SHIFT_LATCH = 1;
	_NOP;_NOP;_NOP;_NOP;_NOP;_NOP;
	_CPLD_SHIFT_LATCH = 0;
}
//*********************************************************************************
//�������� ������ �� RAM � ������� � ��
UCHAR SendData(void)
{
	UCHAR ok = _TRUE;
	UCHAR dbr,dbw = 0;
	UINT index,bcnt;
	ULONG timeOut = 400000;
	//��������� RAM � ADC
	StartRAM();
	//��������, ����������� �� ������ � RAM...
	while((!_CPLD_STOP) && (timeOut--)) _WDT_RESET;
	//���������� RAM � ADC
	StopRAM();
	//...��� �������...
	if (!timeOut) ok = _FALSE;
	if (ok)
	{
		//�������� ������ �� RAM
		for (index = 0; index < _BUFFL; index++) 
		{
			//�������� ������� RAM...
			_CPLD_CLK = 1;
			//��������...
			_NOP;_NOP;_NOP;_NOP;_NOP;_NOP;
			dbr = PORTB;		
			//������������� ����...
			for (bcnt = 7; bcnt; bcnt--) 
			{
				if (dbr & 0x1) dbw |= 0x1;
				dbr >>= 1;
				dbw <<= 1;
			}
			//�������� ������� RAM...
			_CPLD_CLK = 0;			
			//� �����...
			buf[index] = dbw;
			//
			_WDT_RESET;
		}				
	}
	//������� ����� � ������� + CRC32
	ok = USART_SendFrame((UCHAR *)&buf[0], _BUFFL + 4);
	return(ok);
}
//*********************************************************************************
//���
void PWMCCP_Set(UCHAR squarLTRIG, UCHAR squarYPOS)
{
	CCPR1L = squarLTRIG;	//���������� 1
	CCPR2L = squarYPOS;		//���������� 2
	CCP1CON = 0xF; 	//�������� ��� 1
	CCP2CON = 0xF; 	//�������� ��� 2
	PR2 = 0xFF;		//������� ��� = 39,06 kHz
	T2CON = 0x4;	//�������� TMR2	(1:1)
}
//*********************************************************************************
//�������� ���������
void SetupChange(void)
{
	UINT setup = 0;
	//�������� ������ �������
    buffsize = s.adc.buffer_size;
	//������� ���./����.
	if (s.capture.flags & 0x1) setup |= _TRIGGON;
	//������� �����
	if (s.capture.flags & 0x2) setup |= _TRIGGFALL;
	//������� ���
	if (s.capture.flags & 0x4) setup |= _TRIGEXTON;
	//������� ���������� (0..1)
	//�������������� �������� �� 10 (2)
	setup |= (s.mask & 0x7);
	//AC/DC (3)
	setup |= (s.capture.flags & 0x8);
	//�������� ������� �������������
	setup |= ((s.adc.delay & 0xF) << 7);
	//��������� CPLD
	SendSerialCPLD(setup, 11);
	//��� (������� ��������)
	PWMCCP_Set(_CENTERT + (SCHAR)((FLOAT)(s.capture.level - _CENTERT) / (FLOAT)_TRIGPMW),\
			  (_CENTERY + (SCHAR)((FLOAT)(s.capture.delay - _CENTERY) / (FLOAT)_YPOSPWM)));	
}
//*********************************************************************************
//�������� ����
void main(void)
{
	di();
	//��������� ������������		
	PORTA = 0x0;
	LATA = 0x0;
	PORTB = 0x0;
	LATB = 0x0;
	PORTC = 0x0;
	LATC = 0x0;
	ADCON1 = 0xF;	//��� RA ��� ��������
	TRISA = 0b00000000;
	TRISB = 0b11111111;
	TRISC = 0b10100000;
	RBPU = 1; //���.���������
	//------------------------	
	USART_Init();
	PWMCCP_Set(0x0,0x0);
	//����...
	while(1)
	{
		//���������� ��������� �����
		while (!RCIF) _WDT_RESET;
		//���� ������� �� ��...
		if (USART_GetFrame((UCHAR *)&cmd, SIZEOF_SCOPE_COMMAND))
		{
			//������� �� �������� ������ ������������ � ��...
			if (cmd.command == _OSC_CMD_GET) SendData();
			//������� ��������� ������������...
			if (cmd.command == _OSC_CMD_SETTINGS)
			{
				//������� ���������...
				if (USART_GetFrame((UCHAR *)&s, SIZEOF_SCOPE_SETTINGS))
				{
					//�������� ���������
					SetupChange();	
				}
			}
		}
		_WDT_RESET;
	}
}
//*********************************************************************************
