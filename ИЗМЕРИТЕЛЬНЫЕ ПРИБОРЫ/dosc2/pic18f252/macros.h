//*********************************************************************************
#define UCHAR		unsigned char
#define SCHAR		signed char
#define UINT		unsigned int
#define SINT		signed int
#define ULONG		unsigned long
#define SLONG		signed long
#define FLOAT		float
#define DOUBLE		double
//
#define _NOP		asm("nop")
#define _WDT_RESET	asm("clrwdt")
//*********************************************************************************
#define _FALSE 0
#define _TRUE  1
#define _CRC_MASK 0xFFFFFFFFUL
#define _CRC_POLY 0xEDB88320UL
#define _OSC_CMD_GET 'g'
#define _OSC_CMD_SETTINGS 's'
#define _RTS_CHAR 'r'	/* Request To Send. */
#define _CTS_CHAR 'c'	/* Clear To Send. */
#define _USART_BLOCK 14
//���������� ������� CPLD
#define _CPLD_RUN	RC0
#define _CPLD_STOP	RC5
#define _CPLD_CLK	RC4
#define _CPLD_RESET	RC3
//���������������� ��������� CPLD
#define _CPLD_SHIFT_DATA	RA0
#define _CPLD_SHIFT_LATCH	RA1
#define _CPLD_SHIFT_CLK		RA2
//����������� ��������� ������ �������� ��� ���
#define _TRIGPMW	2.886
//����������� ��������� ��������� Y ��� ���
#define _YPOSPWM	2.5
#define _CENTERT	125//114
#define _CENTERY	140//127
//*********************************************************************************
//���� ��������� ������� ������ 
//0..1 - ������� ����������: 00,01,10,11
//2 - ������� �������� 1:10: 0 - v/1; 1 - v/10
//3 - AC/DC: 0 - DC; 1 - AC
//4 - ����� ������ ��������: 0 - �� ������, 1 - �� �����
//5 - ���./����. �������: 0 - ������ ��������, 1 - ������ �������
//6 - ��� ��������: 0 - ����������, 1 - �������
//7..10 - �������� Fosc:
//0000 - fosc/1    	= 40 MHz
//0001 - fosc/2		= 20 MHz
//0010 - fosc/5		= 8 MHz
//0011 - fosc/10 	= 4 MHz
//0100 - fosc/20	= 2 MHz
//0101 - fosc/50	= 0.8 MHz
//0110 - fosc/100	= 0.4 MHz
//0111 - fosc/200	= 0.2 MHz
//1000 - fosc/500	= 80 kHz
//1001 - fosc/1000	= 40 kHz
//1010 - fosc/2000	= 20 kHz
//1011 - fosc/5000	= 8 kHz
//1100 - fosc/10000 = 4 kHz
//*********************************************************************************
#define _TRIGGFALL	0b00000010000
#define _TRIGGRISE	0b00000000000
#define _TRIGGON	0b00000100000
#define _TRIGOFF	0b00000000000
#define _TRIGEXTON	0b00001000000
#define _TRIGEXTOFF	0b00000000000
//*********************************************************************************

