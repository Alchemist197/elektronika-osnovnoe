#ifndef __PRECOMP_H
#define __PRECOMP_H

#pragma warning(disable:4786)

#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>
#include <limits.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <process.h>
#include <mmsystem.h>
#include <vector>
#include <queue>
#include <map>
#include <algorithm>

using namespace std;

#endif /* #ifndef __PRECOMP_H */