//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by osc.rc
//
#define IDS_HARDWARE_SHUTDOWN_FAILED    1
#define IDS_HARDWARE_SETTINGS_FAILED    2
#define IDS_HARDWARE_INIT_FAILED        3
#define IDS_DATA_TRANSFER_FAILED        4
#define IDS_PROGRAM_RUNNED              5
#define IDS_EMULATOR                    6
#define IDS_HARDWARE_SETTINGS_RETRY     7
#define IDS_OUT_OF_MEMORY               8
#define IDS_CREATE_FILE_FAILED          9
#define IDS_WH_OUT_OF_RANGE             10
#define IDS_SCOPE_INI_FILE_NOT_FOUND    11
#define IDS_WRITE_ERROR                 12
#define IDS_READ_ERROR                  13
#define IDS_OPEN_FILE_FAILED            14
#define IDS_FSEEK_FAILED                15
#define IDS_FTELL_FAILED                16
#define IDS_CLOSE_FILE_FAILED           17
#define IDS_SCOPE_INI_FILE_BAD_SYNTAX   18
#define IDS_COM_PORT_INIT_FAILED        19
#define IDS_NOTHING_TO_SAVE             20
#define IDS_CAPTION                     21
#define IDS_CAPTION_WITH_BACKGROUND     22
#define IDS_CONTROLS_CAPTION            23
#define IDS_DCONTROLS_CAPTION           24
#define IDS_STROBE_CAPTION              25
#define IDS_NAN                         26
#define IDR_MAINWND_MENU                101
#define IDD_CONTROLS                    102
#define IDD_INFOPAN                     103
#define IDD_ABOUT                       104
#define IDD_EXPORT_BMP                  105
#define IDD_DCON                        106
#define IDC_LINE_WIDTH                  1001
#define IDC_MINMAX_TIMEOUT              1002
#define IDC_DEVICES                     1003
#define IDC_RESOLUTIONS                 1004
#define IDC_SAMPLE_TIMES                1005
#define IDC_VOLTAGE_RANGES              1006
#define IDC_BUFFER_SIZES                1007
#define IDC_CAPTURE_LEVEL               1008
#define IDC_CAPTURE_DELAY               1009
#define IDC_CAPTURE_ENABLED             1010
#define IDC_SLOPE                       1011
#define IDC_LOG                         1012
#define IDC_SLOPE2                      1012
#define IDC_CALC_FFT_MAX                1013
#define IDC_SLOPE3                      1013
#define IDC_SHOW_DATA_POINTS            1015
#define IDC_MEMORY                      1019
#define IDC_BMP_WIDTH                   1020
#define IDC_TIMEOUT                     1020
#define IDC_BMP_HEIGHT                  1021
#define IDC_SELECT_FILE                 1022
#define IDC_ENABLE_MIN                  1022
#define IDC_BMP_FILE                    1023
#define IDC_ENABLE_MAX                  1023
#define IDC_RLE                         1024
#define IDC_STROBE_ENABLE               1024
#define IDC_STROBE_FREQ_EDIT            1025
#define IDC_STROBE_FREQ_ENTER           1026
#define IDC_STROBE_FREQ_DISPLAY         1027
#define IDC_SAMPLE                      1032
#define IDM_START                       40001
#define IDM_STOP                        40002
#define IDM_EXIT                        40003
#define IDM_GET                         40004
#define IDM_ABOUT                       40005
#define IDM_CURSORS                     40006
#define IDM_EXPORT_BMP                  40007
#define IDM_FILE_SAVE                   40008
#define IDM_FILE_LOAD_BACKGROUND        40009
#define IDM_FILE_CLOSE_BACKGROUND       40010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        108
#define _APS_NEXT_COMMAND_VALUE         40011
#define _APS_NEXT_CONTROL_VALUE         1030
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
