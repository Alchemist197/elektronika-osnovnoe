#ifndef __COMMON_H
#define __COMMON_H

#ifndef __AVR_C
#pragma pack(push)
#pragma pack(1)
#endif /* #ifndef __AVR_C */

/*------------------------------------------------------------------*/
#ifdef __AVR_C

#ifdef __IAR_SYSTEMS_ICC__
#define ENABLE_BIT_DEFINITIONS
#include <iom8515.h>
#else /* GCC */
#include <avr\io.h>
#endif

typedef unsigned char BOOL;
typedef unsigned long CRC;

#define FALSE 0
#define TRUE (!FALSE)

#ifndef NULL
#define NULL 0
#endif /* #ifndef NULL */

#endif /* __AVR_C */
/*------------------------------------------------------------------*/

#define ADC_BITS_PER_SAMPLE 8

typedef unsigned char UCHAR;
typedef unsigned short USHORT;

#define OSC_CMD_GET 'g'
#define OSC_CMD_SETTINGS 's'

/*--------------------------------------------------------------------
SCOPE_COMMAND
--------------------------------------------------------------------*/
typedef struct tagSCOPE_COMMAND
{
	UCHAR command;
	CRC crc;
} SCOPE_COMMAND;
#define SIZEOF_SCOPE_COMMAND (1 + sizeof(CRC))

/* ��������� ������� ���������. */
#define CAPTURE_ENABLED 0x01
#define CAPTURE_POSEDGE 0x02

#define CAPTURE_DELAY_MAX (0xFF - 1)
#define CAPTURE_LEVEL_MAX 0xFF

typedef struct tagSCOPE_CAPTURE
{
	/* ������� (0..255). */
	UCHAR level;
	UCHAR delay;
	UCHAR flags;
} SCOPE_CAPTURE;

/* ��������� ���. */
#define ADC_MODE0 0x00
#define ADC_MODE0_BUFSZ_MAX 32
#define ADC_MODE0_BASE_DIV 1

#define ADC_MODE1 0x01
#define ADC_MODE1_BUFSZ_MAX SCOPE_BUFFER_SIZE
#define ADC_MODE1_BASE_DIV 3

#define ADC_MODE2 0x02
#define ADC_MODE2_BUFSZ_MAX SCOPE_BUFFER_SIZE
#define ADC_MODE2_BASE_DIV 7

#define ADC_MODE3 0x03
#define ADC_MODE3_BUFSZ_MAX SCOPE_BUFFER_SIZE
#define ADC_MODE3_BASE_DIV 10
#define ADC_MODE3_STEP_DIV 3
#define ADC_MODE3_DELAY_MAX (0xFF - 1) /* ����. ��������� ��������. */

#define ADC_MODE4 0x04
#define ADC_MODE4_BUFSZ_MAX SCOPE_BUFFER_SIZE
#define ADC_MODE4_BASE_DIV 10
#define ADC_MODE4_STEP_DIV 30
#define ADC_MODE4_DELAY_MAX (0xFF - 1) /* ����. ��������� ��������. */

typedef struct tagSCOPE_ADC
{
	UCHAR mode;
	UCHAR delay;	/* ������������ ������ � ADC_MODE3. */
	USHORT buffer_size;
} SCOPE_ADC;

/*--------------------------------------------------------------------
SCOPE_SETTINGS
--------------------------------------------------------------------*/
typedef struct tagSCOPE_SETTINGS
{
	SCOPE_CAPTURE capture;	/* 3 �����. */
	SCOPE_ADC adc;			/* 4 �����. */
	UCHAR mask;				/* 1 ����. */
	CRC crc;				/* 4 �����. */
} SCOPE_SETTINGS;
#define SIZEOF_SCOPE_SETTINGS (8 + sizeof(CRC))

#include "adc.h"

#ifndef __AVR_C
#pragma pack(pop)
#endif /* #ifndef __AVR_C */

#endif /* #ifndef __COMMON_H */
