#include "precomp.h"
#include "global.h"
#include "view.h"
#include "units.h"
#include "settings.h"
#include "fft.h"

/*--------------------------------------------------------------------
View::View()
--------------------------------------------------------------------*/
View::View() 
{
	wndclass.lpszClassName = "OSC_VIEW_CLASS";
	prop.style = WS_CHILD;

	ZeroMemory(&vs, sizeof(SETUP));
	vs.line_width = 1;
	vs.flags = VF_CALC_FFT_MAX;
	vs.memory = GetLimit();

	cursors.AddCursor(new CursorV('1', 0));
	cursors.AddCursor(new CursorV('2', 1));
	cursors.AddCursor(new CursorH('3', 1));
	cursors.AddCursor(new CursorH('4', 0));

	hmetrics_font = GetMetricsFont();
	hscale_font = GetScaleFont();
	autoupdate_timer = 0;
}

/*--------------------------------------------------------------------
View::~View()
--------------------------------------------------------------------*/
View::~View()
{
	DeleteObject(hmetrics_font);
	DeleteObject(hscale_font);
}

/*--------------------------------------------------------------------
View::SetVS()
--------------------------------------------------------------------*/
void View::SetVS(const SETUP& vs, BOOL update /* = TRUE */)
{
	ASSERT(vs.line_width >= 1);
	ASSERT(vs.memory >= 1);

	SetLimit(1);
	SetLimit(vs.memory);

	ResetMinMax(
		vs.flags & VF_MINY_ENABLED ^
			View::vs.flags & VF_MINY_ENABLED,
		vs.flags & VF_MAXY_ENABLED ^
			View::vs.flags & VF_MAXY_ENABLED);

	if (vs.flags & VF_AUTOUPDATE_ENABLED)
	{
		if (!(View::vs.flags & VF_AUTOUPDATE_ENABLED))
		{
			autoupdate_timer = SetTimer(hwnd, AUTOUPDATE_TIMER_ID,
				0, NULL);
			ASSERT(autoupdate_timer != 0);
		}
	}
	else
	{
		if (View::vs.flags & VF_AUTOUPDATE_ENABLED)
		{
			VERIFY(KillTimer(hwnd, AUTOUPDATE_TIMER_ID));
			autoupdate_timer = 0;
		}
	}

	View::vs = vs;

	if (update) UpdateClient();
}

/*--------------------------------------------------------------------
View::ResetCursors()
--------------------------------------------------------------------*/
void View::ResetCursors()
{
	VERIFY(cursors.SetPosByName('1', 0));
	VERIFY(cursors.SetPosByName('2', 1));
	VERIFY(cursors.SetPosByName('3', 1));
	VERIFY(cursors.SetPosByName('4', 0));
}

/*--------------------------------------------------------------------
View::Create()
--------------------------------------------------------------------*/
BOOL View::Create(HWND parent) 
{
	return Window::Create(parent);
}

/*--------------------------------------------------------------------
View::Redraw()
--------------------------------------------------------------------*/
void View::Redraw(HDC dc)
{
	BOOL is_dc_null = dc == NULL;
	if (dc == NULL)
		dc = GetDC(hwnd);

	RECT rect;
	GetClientRect(hwnd, &rect);

	HDC memdc = CreateCompatibleDC(dc);
	HBITMAP old_bmp = (HBITMAP)SelectObject(memdc,
		CreateCompatibleBitmap(dc, rect.right, rect.bottom));

	Draw(memdc, rect.right, rect.bottom);
	
	BitBlt(dc, 0, 0, rect.right, rect.bottom, memdc, 0, 0, SRCCOPY);
	DeleteObject(SelectObject(memdc, old_bmp));
	DeleteDC(memdc);

	if (is_dc_null)
		ReleaseDC(hwnd, dc);
}

/*--------------------------------------------------------------------
View::OnTimer()
--------------------------------------------------------------------*/
void View::OnTimer(HWND hwnd, UINT id)
{
	if (id == AUTOUPDATE_TIMER_ID)
	{
		if (vs.flags & VF_MINY_ENABLED ||
			vs.flags & VF_MAXY_ENABLED)
			UpdateClient();
	}
}

/*--------------------------------------------------------------------
View::OnDestroy()
--------------------------------------------------------------------*/
void View::OnDestroy(HWND hwnd)
{
	if (autoupdate_timer != 0)
	{
		VERIFY(KillTimer(hwnd, AUTOUPDATE_TIMER_ID));
		autoupdate_timer = 0;
	}
}

/*--------------------------------------------------------------------
View::OnLButtonDown()
--------------------------------------------------------------------*/
void View::OnLButtonDown(HWND hwnd, BOOL fDoubleClick,
	int x, int y, UINT keyFlags)
{
	POINT p = { x, y };
	if (cursors.OnMouseLButtonDown(hwnd, p)) Redraw(NULL);
}

/*--------------------------------------------------------------------
View::OnLButtonUp()
--------------------------------------------------------------------*/
void View::OnLButtonUp(HWND hwnd,
	int x, int y, UINT keyFlags) 
{
	POINT p = { x, y };
	if (cursors.OnMouseLButtonUp(p)) Redraw(NULL);
}

/*--------------------------------------------------------------------
View::OnMouseMove()
--------------------------------------------------------------------*/
void View::OnMouseMove(HWND hwnd,
	int x, int y, UINT keyFlags) 
{
	POINT p = { x, y };
	if (cursors.OnMouseMove(p)) Redraw(NULL);
}

/*--------------------------------------------------------------------
View::GetScaleFont()
--------------------------------------------------------------------*/
HFONT View::GetScaleFont()
{
	LOGFONT lf;
	ZeroMemory(&lf, sizeof(LOGFONT));

	lf.lfHeight = 15;
	lf.lfCharSet = ANSI_CHARSET;
	lf.lfOutPrecision = OUT_DEFAULT_PRECIS;
	lf.lfClipPrecision = CLIP_DEFAULT_PRECIS;
	lf.lfQuality = DEFAULT_QUALITY;
	lf.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	lf.lfWeight = FW_THIN;

	HFONT font = CreateFontIndirect(&lf);
	ASSERT(font != NULL);
	return font;
}

/*--------------------------------------------------------------------
View::GetMetricsFont()
--------------------------------------------------------------------*/
HFONT View::GetMetricsFont()
{
	LOGFONT lf;
	ZeroMemory(&lf, sizeof(LOGFONT));

	lf.lfHeight = 14;
	lf.lfCharSet = ANSI_CHARSET;
	lf.lfOutPrecision = OUT_DEFAULT_PRECIS;
	lf.lfClipPrecision = CLIP_DEFAULT_PRECIS;
	lf.lfQuality = DEFAULT_QUALITY;
	lf.lfPitchAndFamily = FIXED_PITCH | FF_SCRIPT;
	lf.lfWeight = FW_THIN;

	HFONT font = CreateFontIndirect(&lf);
	ASSERT(font != NULL);
	return font;
}

/* ��������� ������������ �����, ������, ������ � ����� �� �������. */
const int L_SPACE = 80;
const int R_SPACE = 35;
const int T_SPACE = 65;
const int B_SPACE = 25;

#define GRAPH_W(w) ((w) - L_SPACE - R_SPACE)
#define GRAPH_H(h) ((h) - T_SPACE - B_SPACE)
/*#define GRAPH_W 400
#define GRAPH_H 256*/

/*--------------------------------------------------------------------
View::ResetMinMax()
--------------------------------------------------------------------*/
void View::ResetMinMax(BOOL reset_min, BOOL reset_max)
{
	for (UINT i = 0; i < miny.size(); i++)
	{
		if (reset_min)
		{
			miny[i].y = COORD_MAX;
			miny[i].last_update_time_ms = -1;
			miny[i].last_shift_time_ms = -1;
		}
		if (reset_max)
		{
			maxy[i].y = COORD_MIN;
			maxy[i].last_update_time_ms = -1;
			maxy[i].last_shift_time_ms = -1;
		}
	}
}

/*--------------------------------------------------------------------
View::OnSize()
--------------------------------------------------------------------*/
void View::OnSize(HWND hwnd, UINT state, int cx, int cy)
{
	if (GRAPH_W(cx) > 0)
	{
		miny.resize(GRAPH_W(cx));
		maxy.resize(miny.size());

		ResetMinMax(TRUE, TRUE);
	}
	else
	{
		miny.clear();
		maxy.clear();
	}
}

/*--------------------------------------------------------------------
View::DrawGrid()
--------------------------------------------------------------------*/
void View::DrawGrid(HDC dc, const VIEW_PROP& vp, int w, int h)
{
	const COLORREF GRID_COLOR = RGB(0x80, 0x80, 0x80);
	const COLORREF TEXT_COLOR = RGB(0xFF, 0xFF, 0xFF);
	/* ��� ����� �� ��� X. */
	const int X_GRID_SIZE = 80;
	/* ��� ����� �� ��� Y. */
	const int Y_GRID_SIZE = 35;

	int i;
	char buffer[256];
	UINT old_text_align;

	HPEN old_pen = (HPEN)SelectObject(dc,
		CreatePen(PS_SOLID, 0, GRID_COLOR));
	HFONT old_font = (HFONT)SelectObject(dc, hscale_font);
	
	/* ����� ������ ���� �������. */
	POINT lb = mapper.GetLB();	
	/* ������ ������� ���� �������. */
	POINT rt = mapper.GetRT();	
	
	/* ������ �����. */
	MoveToEx(dc, lb.x, lb.y, NULL);
	LineTo(dc, lb.x, rt.y);
	LineTo(dc, rt.x, rt.y);
	LineTo(dc, rt.x, lb.y);
	LineTo(dc, lb.x, lb.y);

	int x_grid_lines = 1 + GRAPH_W(w)/X_GRID_SIZE;
	int y_grid_lines = 1 + GRAPH_H(h)/Y_GRID_SIZE;

	int old_bk_mode = SetBkMode(dc, TRANSPARENT);
	COLORREF old_text_color = SetTextColor(dc, TEXT_COLOR);

	Units units;
	units.Setup(
		MAX(ABS(vp.min_voltage), ABS(vp.max_voltage)), 
		"V");
	old_text_align = SetTextAlign(dc, TA_RIGHT | TA_BOTTOM);
	/* ������ �������������� �����. */
	for (i = 0; i < y_grid_lines && y_grid_lines > 1; i++)
	{
		REAL y = vp.min_voltage + (vp.max_voltage - vp.min_voltage)*
			i/(y_grid_lines - 1);
		
		POINT p = mapper.Map(0, y);;
		MoveToEx(dc, p.x, p.y, NULL);
		p = mapper.Map(vp.time, y);
		LineTo(dc, p.x, p.y);

		/* �����. */
		units.Translate(buffer, y);
		SIZE text_extent;
		VERIFY(GetTextExtentPoint32(dc, buffer, 
			strlen(buffer), &text_extent));
		TextOut(dc, lb.x - (Cursor::GetRSize() >> 1), 
			p.y + (text_extent.cy >> 1),
			buffer, strlen(buffer));
	}

	/* ������ ������������ �����. */
	units.Setup(vp.time, "s");
	SetTextAlign(dc, TA_CENTER | TA_TOP);
	for (i = 0; i < x_grid_lines && x_grid_lines > 1; i++)
	{
		REAL x = vp.time*i/(x_grid_lines - 1);

		POINT p = mapper.Map(x, vp.min_voltage);
		MoveToEx(dc, p.x, p.y, NULL);
		p = mapper.Map(x, vp.max_voltage);
		LineTo(dc, p.x, p.y);

		/* �����. */
		units.Translate(buffer, x);
		TextOut(dc, p.x, lb.y + (Cursor::GetRSize() >> 1), 
			buffer, strlen(buffer));
	}
	SetTextAlign(dc, old_text_align);

	/* ����������� �������. */
	SetBkMode(dc, old_bk_mode);
	SetTextColor(dc, old_text_color);
	DeleteObject(SelectObject(dc, old_pen));
	SelectObject(dc, old_font);
}

/*--------------------------------------------------------------------
View::DrawMetrics()
--------------------------------------------------------------------*/
void View::DrawMetrics(HDC dc, const VIEW_PROP& vp, int w, int h, 
					   const GRAPH_VECTOR& graphs)
{
	const COLORREF TEXT_COLOR = RGB(0xFF, 0xFF, 0xFF);
	const int START_X = 120;
	const int START_Y = 5;
	const int STEP_X = 120;
	const int STEP_Y = 20;

	HFONT old_font = (HFONT)SelectObject(dc, hmetrics_font);
	int old_text_color = SetTextColor(dc, TEXT_COLOR);
	int old_bk_mode = SetBkMode(dc, TRANSPARENT);
	int old_text_align = SetTextAlign(dc, TA_LEFT | TA_TOP);

	int x = START_X, y = START_Y;

	REAL pos_a, pos_b, pos_c, pos_d;
	VERIFY(cursors.GetPosByName('1', pos_a));
	VERIFY(cursors.GetPosByName('2', pos_b));
	VERIFY(cursors.GetPosByName('3', pos_c));
	VERIFY(cursors.GetPosByName('4', pos_d));

	REAL time_a = pos_a*vp.time;
	REAL time_b = pos_b*vp.time;
	REAL v_c = vp.max_voltage - 
		pos_c*(vp.max_voltage - vp.min_voltage);
	REAL v_d = vp.max_voltage - 
		pos_d*(vp.max_voltage - vp.min_voltage);

	char buffer[256], text[256];
	Units units;

#define PRINT(p, v, u) {\
	units.Setup((v), (u));\
	strcpy(text, (p));\
	strcat(text, units.Translate(buffer, (v)));\
	TextOut(dc, x, y, text, strlen(text));\
	x += STEP_X; }
#define PRINT_S(p) {\
	TextOut(dc, x, y, (p), strlen(p));\
	x += STEP_X; }

	PRINT("T1: ", time_a, "s");
	PRINT("T2: ", time_b, "s");
	PRINT("T2-T1: ", time_b - time_a, "s");
	REAL period = ABS(time_a - time_b);
	if (period > 1E-18) {
		PRINT("freq: ", 1/period, "Hz"); }
	else {
		PRINT_S("f:"); }
	
	x = START_X;
	y += STEP_Y;

	PRINT("V3: ", v_c, "V");
	PRINT("V4: ", v_d, "V");
	PRINT("V4-V3: ", v_d - v_c, "V");

	if (vs.flags & VF_CALC_FFT_MAX)
		if (graphs.size() != 0) {
			PRINT("FFTmax: ", graphs[0].data.GetFFTMaxFreq(), "Hz"); }
		else {
			PRINT_S("FFTmax:"); }

	SetTextAlign(dc, old_text_align);
	SetBkMode(dc, old_bk_mode);
	SetTextColor(dc, old_text_color);
	SelectObject(dc, old_font);
}

/*--------------------------------------------------------------------
View::UpdateMinMax()
--------------------------------------------------------------------*/
void View::UpdateMinMax(const POINT_VECTOR& points, int graph_w)
{
	/* ����� "�������" ����� �������� ��� ���������
	�� 1 �������. */
	const long SHIFT_PERIOD_MS = 25;

	if (points.size() == 0 || graph_w <= 0 ||
		!(vs.flags & VF_MINY_ENABLED || vs.flags & VF_MAXY_ENABLED))
		return;

	POINT lb = mapper.GetLB();

	POINT_VECTOR ipoints;
	
	POINT p1 = points[0];
	for (size_t i = 0; i < points.size(); i++)
	{
		POINT p2 = points[i];
		if (p2.x == p1.x)
			ipoints.push_back(p2);
		else
		{
			/* ������ ����� (p1, p2]. */
			POINT p;
			/* �����. ������������. */
			int a = p2.y - p1.y;
			int b = p2.x - p1.x;
			for (p.x = p1.x + 1; p.x <= p2.x; p.x++)
			{
				p.y = p1.y + a*(p.x - p1.x)/b;
				ipoints.push_back(p);
			}
		}
		p1 = p2;
	}

	ASSERT(graph_w == miny.size());
	long current_minmax_time = (long)minmax_timer.GetElapsedTimeMs();
	long timeout_ms = vs.minmax_timeout*1000;
	/*---*/
	for (size_t i = 0; i < ipoints.size(); i++)
	{
		POINT p = ipoints[i];
		int n = p.x - lb.x;
		if (n >= 0 && n < (int)miny.size())
		{
			MINMAX* pmm = NULL;

/*--- MACRO --------------------------------------------------------*/
#define UPDATE_MINMAX {\
	pmm->y = p.y;\
	pmm->last_update_time_ms = current_minmax_time;\
	pmm->last_shift_time_ms = -1; }
/*--- ENDM ---------------------------------------------------------*/

/*--- MACRO --------------------------------------------------------*/
#define SHIFT_MINMAX(sign) { long shift;\
	if (pmm->last_shift_time_ms == -1)\
		shift = (current_minmax_time - pmm->last_update_time_ms -\
			timeout_ms)/SHIFT_PERIOD_MS;\
	else\
		shift = (current_minmax_time - pmm->last_shift_time_ms)\
		/SHIFT_PERIOD_MS;\
	ASSERT(shift >= 0);\
	if (shift > 0) pmm->last_shift_time_ms = current_minmax_time;\
	if ((sign) > 0) {\
		if ((pmm->y += shift) > p.y) pmm->y = p.y; }\
	else {\
		if ((pmm->y -= shift) < p.y) pmm->y = p.y; } }
/*--- ENDM ---------------------------------------------------------*/

			/* ��������. */
			if (vs.flags & VF_MINY_ENABLED)
			{
				pmm = &miny[n];
				if (p.y < pmm->y ||
					pmm->last_update_time_ms == -1)
					UPDATE_MINMAX
				else if (current_minmax_time -
					pmm->last_update_time_ms > timeout_ms
					&& pmm->y < COORD_MAX)
					SHIFT_MINMAX(+1);
			}

			/* ���������. */
			if (vs.flags & VF_MAXY_ENABLED)
			{
				pmm = &maxy[n];
				if (p.y > pmm->y ||
					pmm->last_update_time_ms == -1)
					UPDATE_MINMAX
				else if (current_minmax_time -
					pmm->last_update_time_ms > timeout_ms
					&& pmm->y > COORD_MIN)
					SHIFT_MINMAX(-1);
			}
			/*---*/
		}
	}

	/* ������ �� ������ �������� ��� ����, ��� ��� ��������
	����������� ������������� ������������ (����� � ������). */
	int l_free_space = 0;
	int r_free_space = miny.size();
	if (ipoints.size() != 0)
	{
		l_free_space = ipoints.front().x - lb.x;
		if (l_free_space < 0) l_free_space = 0;
		r_free_space = (miny.size() - 1) - (ipoints.back().x - lb.x);
		if (r_free_space < 0) r_free_space = 0;
	}
	/*---*/
	BOOL left = TRUE;
	int j = 0;
	for (;;)
	{
		if (left && j == l_free_space)
		{
			left = FALSE;
			j = 0;
		}
		if (!left && j == r_free_space) break;

		int n = left ? j : miny.size() - 1 - j;

		MINMAX* pmm;
		POINT p;
		p.x = 0;
		/* ��������. */
		if (vs.flags & VF_MINY_ENABLED)
		{
			pmm = &miny[n];
			p.y = COORD_MAX;
			if (current_minmax_time -
				pmm->last_update_time_ms > timeout_ms
				&& pmm->y < COORD_MAX)
				SHIFT_MINMAX(+1);
		}
		/* ���������. */
		if (vs.flags & VF_MAXY_ENABLED)
		{
			pmm = &maxy[n];
			p.y = COORD_MIN;
			if (current_minmax_time -
				pmm->last_update_time_ms > timeout_ms
				&& pmm->y > COORD_MIN)
				SHIFT_MINMAX(-1);
		}

		++j;
	}
}

/*--------------------------------------------------------------------
View::PlotGraph()
--------------------------------------------------------------------*/
void View::PlotGraph(HDC dc, const VIEW_PROP& vp,
					 int w, int h, const GRAPH& graph,
					 REAL color_mult, REAL width_mult)
{
	const COLORREF POINTS_PEN_COLOR = RGB(0xFF, 0xFF, 0xFF);
	const COLORREF POINTS_BRUSH_COLOR = RGB(0x00, 0x00, 0x00);
	const int POINT_SIZE = 2;

	const Data& data = graph.data;

	/* ������ ����� ���������� �����������, ����� ��������
	������ �������� ���������/������������ ������ � ����. */
	static POINT_VECTOR points;
	if (points.size() < data.Size()) points.resize(data.Size());

	for (size_t i = 0; i < data.Size(); i++)
	{
		/* �������������� ���������. */
		points[i] = mapper.Map(data.SampleTime(i),
			data.SampleVoltage(i));
	}
	UpdateMinMax(points, GRAPH_W(w));

	UCHAR red =
		(UCHAR)Globals::Round(color_mult*(graph.color & 0xFF));
	UCHAR green =
		(UCHAR)Globals::Round(color_mult*((graph.color >> 8) & 0xFF));
	UCHAR blue =
		(UCHAR)Globals::Round(color_mult*((graph.color >> 16) & 0xFF));
	COLORREF color = RGB(red, green, blue);

	HPEN old_pen = (HPEN)SelectObject(dc,
		CreatePen(PS_SOLID,
			Globals::Round(vs.line_width*width_mult), color));
	/* ������ ������. */
	Polyline(dc, &*points.begin(), data.Size());

	if (vs.flags & VF_SHOW_POINTS)
	{
		DeleteObject(SelectObject(dc,
			CreatePen(PS_SOLID, 1, POINTS_PEN_COLOR)));
		HBRUSH old_brush = (HBRUSH)SelectObject(dc,
			CreateSolidBrush(POINTS_BRUSH_COLOR));
		int old_bk_mode = SetBkMode(dc, OPAQUE);

		for (size_t i = 0; i < data.Size(); i++)
		{
			POINT& p = points[i];
			Rectangle(dc,
				p.x - POINT_SIZE, p.y - POINT_SIZE,
				p.x + POINT_SIZE, p.y + POINT_SIZE);
		}

		SetBkMode(dc, old_bk_mode);
		DeleteObject(SelectObject(dc, old_brush));
	}

	/* ����������� �������. */
	DeleteObject(SelectObject(dc, old_pen));
}

/*--------------------------------------------------------------------
View::PlotMinMax()
--------------------------------------------------------------------*/
void View::PlotMinMax(HDC dc, const VIEW_PROP& vp, int w, int h)
{
	const COLORREF MINMAX_COLOR = RGB(0x80, 0x80, 0x80);
	if (!(vs.flags & VF_MINY_ENABLED || vs.flags & VF_MAXY_ENABLED))
		return;

	HPEN old_pen = (HPEN)SelectObject(dc,
		CreatePen(PS_SOLID, 0, MINMAX_COLOR));
	int old_r2_mode = SetROP2(dc, R2_WHITE);

	/* ����� ������ ���� �������. */
	POINT lb = mapper.GetLB();	
#ifdef _DEBUG
	/* ������ ������� ���� �������. */
	POINT rt = mapper.GetRT();	
	ASSERT(miny.size() == rt.x - lb.x + 1);
#endif /* #ifdef _DEBUG */

	POINT_VECTOR points;
	points.resize(miny.size());
	ASSERT(miny.size() == maxy.size());

	/* ��������. */
	if (vs.flags & VF_MINY_ENABLED)
	{
		for (size_t i = 0; i < miny.size(); i++)
		{
			POINT p = { lb.x + i, miny[i].y };
			points[i] = p;
		}
		Polyline(dc, &*points.begin(), points.size());
	}

	/* ���������. */
	if (vs.flags & VF_MAXY_ENABLED)
	{
		for (size_t i = 0; i < maxy.size(); i++)
		{
			POINT p = { lb.x + i, maxy[i].y };
			points[i] = p;
		}
		Polyline(dc, &*points.begin(), points.size());
	}
	
	/* ����������� �������. */
	SetROP2(dc, old_r2_mode);
	DeleteObject(SelectObject(dc, old_pen));
}

/*--------------------------------------------------------------------
View::Draw()
--------------------------------------------------------------------*/
void View::Draw(HDC dc, int w, int h)
{
/*#define SHOW_STAT*/

	const COLORREF BK_COLOR = RGB(0x00, 0x00, 0x00);
	const COLORREF CAPTURE_COLOR = RGB(0xFF, 0x00, 0x00);

	/* �������� ��������� ���������� � ���������
	� ����. ���������, ��� ������������ ������� � ���. */
	VIEW_PROP vp;
	SETTINGS s;
	_settings.Get(s);
	/*---*/
	Hardware& hw = *_hardware[s.selected_device_index];
	HARDWARE_CAPS caps;
	hw.GetCaps(caps);
	HARDWARE_SETTINGS hws;
	hw.GetHWSBuffer(hws);
	Resolution& res = caps.resolutions[hws.resolution_index];
	/*---*/
	vp.max_sample_value = (1 << caps.bits_per_sample) - 1;
	vp.min_voltage = caps.ranges[hws.range_index].GetMin();
	vp.max_voltage = caps.ranges[hws.range_index].GetMax();
	vp.capture_enabled = hws.flags & HWS_CAPTURE_ENABLED;
	vp.capture_level = hws.capture_level;
	vp.time = res.GetBufferSize(hws.buffer_size_index)*
		res.GetTime(hws.resolution_scale);

	GRAPH_VECTOR_VECTOR q;
	GetQueue(q);
	if (!q.empty() && !q.back().empty())
		vp.time = q.back().front().data.TimeLength();

#ifdef SHOW_STAT
	Timer rt_timer;
	rt_timer.Reset();

	static Timer fps_timer;
	static UINT fps_counter = 0;
	static REAL fps = 0;

	fps_counter++;
#endif /* #ifdef SHOW_STAT */

	Mapper::SETUP ms;
	ZeroMemory(&ms, sizeof(Mapper::SETUP));
	ms.lb.x = L_SPACE;
	ms.lb.y = h - 1 - B_SPACE;
	ms.rt.x = ms.lb.x + GRAPH_W(w) - 1;
	ms.rt.y = ms.lb.y - GRAPH_H(h) - 1;
	ms.min_voltage = vp.min_voltage;
	ms.max_voltage = vp.max_voltage;
	ms.min_time = 0;
	ms.max_time = vp.time;
	mapper.Setup(ms);

	HBRUSH old_brush = (HBRUSH)SelectObject(dc,
		CreateSolidBrush(BK_COLOR));
	HPEN old_pen = (HPEN)SelectObject(dc,
		CreatePen(PS_DOT, 0, CAPTURE_COLOR));

	/* ������� ����� ���� ������ ����. */
	PatBlt(dc, 0, 0, w, h, PATCOPY);

	if (GRAPH_W(w) > 0 && GRAPH_H(h) > 0 && vp.time > 0)
	{
		/* ������ �����. */
		DrawGrid(dc, vp, w, h);

		/* ����� ������ ���� �������. */
		POINT lb = mapper.GetLB();	
		/* ������ ������� ���� �������. */
		POINT rt = mapper.GetRT();	
		/*---*/
		RECT rect;
		rect.left = lb.x;
		rect.top = rt.y;
		rect.right = rt.x;
		rect.bottom = lb.y;
		cursors.SetRect(rect);
		/*---*/
		IntersectClipRect(dc, lb.x, rt.y, rt.x + 1, lb.y + 1);

		/* ������ �������. */
		for (int i =
			(vs.flags & VF_MEMORY_ENABLED || q.size() == 0) ?
			q.size() - 1 : 0;
			i >= 0; i--)
		{
			for (int j = q[i].size() - 1; j >= 0; j--)
			{
				REAL color_mult = q.size() == 1 ?
					1 : 1 - 0.8*i/(q.size() - 1.0);
				REAL width_mult = 1 + i;
				PlotGraph(dc, vp,
					w, h, q[i][j], color_mult, width_mult);
			}
		}

		PlotMinMax(dc, vp, w, h);

		if (vp.capture_enabled)
		{
			/* ������ �������������� ����� �� ������ �������. */
			REAL capture_voltage = vp.min_voltage + 
				vp.capture_level*(vp.max_voltage - vp.min_voltage)/
				vp.max_sample_value;
			POINT p = mapper.Map(0, capture_voltage);
			MoveToEx(dc, p.x, p.y, NULL);
			p = mapper.Map(vp.time, capture_voltage);

			int old_bk_mode = SetBkMode(dc, TRANSPARENT);
			LineTo(dc, p.x, p.y);
			SetBkMode(dc, old_bk_mode);
		}

		/* ���������� ������� �������. */
		SelectClipRgn(dc, NULL);
		/* �������. */
		cursors.Draw(dc);
		/* �������. */
		if (cursors.IsEnabled())
		{
			DrawMetrics(dc, vp, w, h, q.size() != 0 ?
				q[0] : GRAPH_VECTOR());
		}
	}

	/* ����������� �������. */
	DeleteObject(SelectObject(dc, old_brush));
	DeleteObject(SelectObject(dc, old_pen));

#ifdef SHOW_STAT
	if (fps_timer.GetElapsedTime() >= 1)
	{
		fps = fps_counter/fps_timer.GetElapsedTime();
		fps_timer.Reset();	
		fps_counter = 0;
	}

	char stat[256];
	sprintf(stat, "Redraw time = %.2fms FPS = %.2f",
		rt_timer.GetElapsedTime()*1000, fps);
	TextOut(dc, 0, 0, stat, strlen(stat));
#endif /* #ifdef SHOW_STAT */
}

/*--------------------------------------------------------------------
View::OnPaint()
--------------------------------------------------------------------*/
void View::OnPaint(HWND hwnd)
{
	PAINTSTRUCT ps;
	BeginPaint(hwnd, &ps);
	Redraw(ps.hdc);
	EndPaint(hwnd, &ps);
}

/*--------------------------------------------------------------------
View::WindowProc()
--------------------------------------------------------------------*/
LPARAM CALLBACK View::WindowProc(HWND hwnd, UINT msg, 
		WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	HANDLE_MSG(hwnd, WM_PAINT, OnPaint);
	}

	return Window::WindowProc(hwnd, msg, wParam, lParam);
}
