#ifndef __EMULATOR_H
#define __EMULATOR_H

#include "hwthread.h"

/*--------------------------------------------------------------------
Emulator
--------------------------------------------------------------------*/
class Emulator: public HardwareTh
{
public:

	virtual BOOL Init(HARDWARE_CALLBACK callback, void* pparam);

	virtual char* GetName(char* name) {
		return strcpy(name, "��������"); }

	virtual void IO_Thread_OnDataRequest();
	virtual BOOL IO_Thread_OnUpdateSettingsRequest(
		const HARDWARE_SETTINGS& hws);

protected:

	enum { RES_LOW, RES_NORM, RES_HIGH };
};

#endif /* #ifndef __EMULATOR_H */
