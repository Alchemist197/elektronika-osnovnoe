#include "precomp.h"
#include "global.h"
#include "fft.h"

FFT _fft;

/*--------------------------------------------------------------------
���� ������ � ���������.
--------------------------------------------------------------------*/

/* ��������������� ������ ���� ������ � ������. */
const ULONG CACHE_SIZE = 1024*16;

REAL FFT::pi;

/*--------------------------------------------------------------------
FFT::RFFT()
����������� ���������� ��� ������� v. ��������� ������������
�� ����� �������� ������. ������� ������ ������ ���� ��������������
��������� �������� FFT::Reorder().
--------------------------------------------------------------------*/
void FFT::RFFT(COMPLEX* v, ULONG n)
{
	if (n <= (CACHE_SIZE/2)/sizeof(COMPLEX))
	{
		/* ���� ������ ������ n ��������� ������������� ������
		� ��� ������, �������� ������� ������������ ����������
		��� (��� ����������� �� ��������). */
		IFFT(v, n);
		return;
	}

	ULONG half_n = n >> 1;

	if (half_n > 1)
	{
		RFFT(v, half_n);
		RFFT(v + half_n, half_n);
	}

	/* ���������� �������� ����� �� ������� n-�� �������. */
	COMPLEX w_main;
	RootOfUnity(w_main, 1, n);

	COMPLEX w;
	w.re = 1;
	w.im = 0;

	/* ����������� ����������� ����������� �������. */
	ULONG i = 0;
	while (i < half_n)
	{
		COMPLEX a = v[i];
		COMPLEX b = v[i + half_n];

		/* ��������� b �� w. */
		COMPLEX t = b;
		b.re = t.re*w.re - t.im*w.im;
		b.im = t.re*w.im + t.im*w.re;

		v[i].re = a.re + b.re;
		v[i].im = a.im + b.im;

		v[i + half_n].re = a.re - b.re;
		v[i + half_n].im = a.im - b.im;

		if (++i < half_n)
			NextRoot(w, w_main, i, n);
	}
}

/*--------------------------------------------------------------------
FFT::IFFT()
����������� ���������� ��� ������� v. ��������� ������������
�� ����� �������� ������. ������� ������ ������ ���� ��������������
��������� �������� FFT::Reorder().
--------------------------------------------------------------------*/
void FFT::IFFT(COMPLEX* v, ULONG n)
{
	ULONG step = 1;

	while (step < n)
	{
		ULONG half_step = step;
		step <<= 1;

		/* ���������� �������� ����� �� ������� step-�������. */
		COMPLEX w_main;
		RootOfUnity(w_main, 1, step);
		
		COMPLEX w;
		w.re = 1;
		w.im = 0;
		
		ULONG i = 0; 
		while (i < half_step)
		{
			for (ULONG j = i; j < n; j += step)
			{
				COMPLEX a = v[j];
				COMPLEX b = v[j + half_step];

				/* ��������� b �� w. */
				COMPLEX t = b;
				b.re = t.re*w.re - t.im*w.im;
				b.im = t.re*w.im + t.im*w.re;

				v[j].re = a.re + b.re;
				v[j].im = a.im + b.im;

				v[j + half_step].re = a.re - b.re;
				v[j + half_step].im = a.im - b.im;
			}

			if (++i < half_step)
				NextRoot(w, w_main, i, step);
		}
	}
}

/*--------------------------------------------------------------------
FFT::Reorder()
��������������� ������������ ��������� �������� ������� ���
������������� �������������� ������� ��� "�� �����".
--------------------------------------------------------------------*/
void FFT::Reorder(COMPLEX* v, ULONG n)
{
	/* ������� ������ ������������. */
	ULONG reorder_index = 0;

	for (ULONG i = 1; i < n; i++)
	{
		/* �������� ��������� ���������� ������ �� ��������
		����������� ���������� ������� � �������� ������� �
		������ ���������. */
		ULONG t = n;
		do {
			reorder_index ^= (t >>= 1);
		} while ((reorder_index & t) == 0);

		/* ��� ������� ��������� �������� ��������� ������������. */
		if (reorder_index > i)
		{
			COMPLEX t = v[i];
			v[i] = v[reorder_index];
			v[reorder_index] = t;
		}
	}
}
