#include "precomp.h"
#include "global.h"
#include "comm.h"

const CRC CRC_MASK = 0xFFFFFFFFUL;
const CRC CRC_POLY = 0xEDB88320UL;

const DWORD READ_TIMEOUT_CONSTANT = 1000;	/* ms. */
const UINT ERROR_RECOVERY_TIME = 1500;		/* ms. */

const char RTS_CHAR = 'r';	/* Request To Send. */
const char CTS_CHAR = 'c';	/* Clear To Send. */
const USHORT USART_BLOCK = 14;

/*--------------------------------------------------------------------
Rs232::Rs232()
--------------------------------------------------------------------*/
Rs232::Rs232()
{
	hcomm = INVALID_HANDLE_VALUE;

	/* ��������� ������� ��� CRC32. */
	for (int i = 0; i < 256; i++)
		crc32_table[i] = ByteCRC32((UCHAR)i);

	ZeroMemory(&dcb, sizeof(dcb));
	dcb.DCBlength = sizeof(DCB);
	dcb.fBinary = TRUE;		/* �������� �����, �.�. 
							��� �������� �� ������ EOF */
	dcb.fParity = FALSE;	/* ��� �������� ��������. */
	dcb.ByteSize = 8;		/* 8 ��� ������. */
	dcb.StopBits = ONESTOPBIT;

	/* ������/������ ���������� ����������� ��� ������������� ������.
	����������� ������ �������� ������ ����� ������
	ClearCommError(). */
	dcb.fAbortOnError = TRUE;
}

/*--------------------------------------------------------------------
Rs232::GetCRC32()
--------------------------------------------------------------------*/
CRC Rs232::CalcCRC32(UCHAR* p, DWORD buffer_size)
{
	CRC crc = CRC_MASK;
	UCHAR c;

	while (buffer_size--)
	{
		c = (UCHAR)crc ^ *p++;
		crc = crc32_table[c] ^ (crc >> 8);
	}

	return ~crc;
}

/*--------------------------------------------------------------------
Rs232::ByteCRC32()
--------------------------------------------------------------------*/
CRC Rs232::ByteCRC32(UCHAR byte)
{
	UCHAR nbit;
	CRC sr = (CRC)byte;

	for (nbit = 0; nbit < 8; nbit++)
	{
		if (sr & 1)
			sr = (sr >> 1) ^ CRC_POLY;
		else
			sr >>= 1;
	}

	return sr;
}

/*--------------------------------------------------------------------
Rs232::Init()
--------------------------------------------------------------------*/
BOOL Rs232::Init(UINT port_number, UINT baud_rate)
{
	dcb.BaudRate = baud_rate;

	char com_port_name[16];
	sprintf(com_port_name, "COM%d", port_number);
	hcomm = CreateFile(com_port_name,
		GENERIC_READ | GENERIC_WRITE,
		0,
		0,
		OPEN_EXISTING,
		0,
		NULL);

	if (hcomm == INVALID_HANDLE_VALUE)
		THROW;
	
	if (!SetCommState(hcomm, &dcb))
		THROW;

	/* ����-����. */
	COMMTIMEOUTS ct;
	ZeroMemory(&ct, sizeof(COMMTIMEOUTS));
	ct.ReadTotalTimeoutConstant = READ_TIMEOUT_CONSTANT;
	ct.ReadIntervalTimeout = ct.ReadTotalTimeoutConstant;
	ct.ReadTotalTimeoutMultiplier = 0;

	if (!SetCommTimeouts(hcomm, &ct))
		THROW;

	if (!EscapeCommFunction(hcomm, SETDTR))
		THROW;

	if (!PurgeComm(hcomm, PURGE_RXCLEAR | PURGE_TXCLEAR |
		PURGE_TXABORT | PURGE_RXABORT))
		THROW;

	return TRUE;

CATCH:

	Shutdown();

	return FALSE;
}

/*--------------------------------------------------------------------
Rs232::Shutdown()
--------------------------------------------------------------------*/
BOOL Rs232::Shutdown()
{
	BOOL ok = TRUE;

	if (hcomm != INVALID_HANDLE_VALUE)
	{
		if (!PurgeComm(hcomm, PURGE_RXCLEAR | PURGE_TXCLEAR |
			PURGE_TXABORT | PURGE_RXABORT))
			ok = FALSE;

		if (!EscapeCommFunction(hcomm, CLRDTR))
			ok = FALSE;

		if (!CloseHandle(hcomm))
			ok = FALSE;
		hcomm = INVALID_HANDLE_VALUE;
	}

	return ok;
}

/*--------------------------------------------------------------------
Rs232::LogCommError()
--------------------------------------------------------------------*/
#ifdef _DEBUG
void Rs232::LogCommError(DWORD errors, const COMSTAT& comstat)
{
	if (errors == 0) ERRLOG("CE_NO_ERROR");
	if (errors & CE_BREAK) ERRLOG("CE_BREAK");
	if (errors & CE_DNS) ERRLOG("CE_DNS");
	if (errors & CE_FRAME) ERRLOG("CE_FRAME");
	if (errors & CE_IOE) ERRLOG("CE_IOE");
	if (errors & CE_MODE) ERRLOG("CE_MODE");
	if (errors & CE_OOP) ERRLOG("CE_OOP");
	if (errors & CE_OVERRUN) ERRLOG("CE_OVERRUN");
	if (errors & CE_PTO) ERRLOG("CE_PTO");
	if (errors & CE_RXOVER) ERRLOG("CE_RXOVER");
	if (errors & CE_RXPARITY) ERRLOG("CE_RXPARITY");
	if (errors & CE_TXFULL) ERRLOG("CE_TXFULL");
}
#endif /* #ifdef _DEBUG */

/*--------------------------------------------------------------------
Rs232::Read()
--------------------------------------------------------------------*/
BOOL Rs232::Read(UCHAR* p, DWORD size)
{
	ASSERT(hcomm != INVALID_HANDLE_VALUE);

	DWORD number_of_bytes_read = 0;

	BOOL ok = ReadFile(hcomm, p, size, 
		&number_of_bytes_read, NULL);
	if (ok)
	{
		ok = number_of_bytes_read == size;
#ifdef _DEBUG
		if (!ok)
		{
			char error[256];
			sprintf(error,
				"Rs232::Read(): "
				"number_of_bytes_read(%lu) != size(%lu).",
				number_of_bytes_read, size);
			ERRLOG(error);
		}
#endif /* #ifdef _DEBUG */
	}
	else
		ERRLOG("Rs232::Read(): ReadFile() failed.");

	if (!ok)
	{
		DWORD errors = 0;
		COMSTAT comstat;
		if (ClearCommError(hcomm, &errors, &comstat))
		{
			ERRLOG("Rs232::Read() CommError:");
			LogCommError(errors, comstat);
		}
	}

	return ok;
}

/*--------------------------------------------------------------------
Rs232::Write()
--------------------------------------------------------------------*/
BOOL Rs232::Write(UCHAR* p, DWORD size)
{
	ASSERT(hcomm != INVALID_HANDLE_VALUE);

	DWORD number_of_bytes_written = 0;

	BOOL ok = WriteFile(hcomm, p, size,
		&number_of_bytes_written, NULL);
	if (ok)
	{
		ok = number_of_bytes_written == size;
#ifdef _DEBUG
		if (!ok)
		{
			char error[256];
			sprintf(error,
				"Rs232::Write(): "
				"number_of_bytes_written(%lu) != size(%lu).",
				number_of_bytes_written, size);
			ERRLOG(error);
		}
#endif /* #ifdef _DEBUG */
	}
	else
		ERRLOG("Rs232::Write(): WriteFile() failed.");

	if (!ok)
	{
		DWORD errors = 0;
		COMSTAT comstat;
		if (ClearCommError(hcomm, &errors, &comstat))
		{
			ERRLOG("Rs232::Write() CommError:");
			LogCommError(errors, comstat);
		}
	}

	return ok;
}

/*--------------------------------------------------------------------
Rs232::GetFrame()
� ����� ����� ������ ������ ���� CRC.
--------------------------------------------------------------------*/
BOOL Rs232::GetFrame(UCHAR* p, USHORT size)
{
	CRC crc;				/* ����� ��� �������� CRC. */
	CRC* pcrc = (CRC*)(p + size - sizeof(CRC));	/* ��������� ��
												CRC � �����. */
	BOOL ok;				/* ������. */
	UCHAR c;				/* ����� ��� ������ RTS �
							�������� CTS. */
	USHORT bytes_read = 0;	/* ������� �������� ����. */
	USHORT block;			/* ������ �����. */
	
	/* ��������� RTS. */
	if (!(ok = Read(&c, sizeof(UCHAR)) && c == RTS_CHAR))
		ERRLOG("Rs232::GetFrame(): RTS read error.");
		
	/* ��������� �� ��� ����, ���� �� �������� �����-����
	������ � ����� �������� ���� ������ ������� �����. */
	c = CTS_CHAR;
	while (ok && bytes_read < size)
		/* ���������� CTS. */
		if (ok = Write(&c, sizeof(UCHAR)))
		{
			/* ��������� ������ ���������� ������������ �����. */
			if ((block = size - bytes_read) > USART_BLOCK)
				block = USART_BLOCK;
			/* ��������� ��������� ����. */
			if (!(ok = Read(p + bytes_read, block)))
				ERRLOG("Rs232::GetFrame(): block read error.");
			/* ����������� ������� �������� ����. */
			bytes_read += block;
		}
		else
			ERRLOG("Rs232::GetFrame(): CTS write error.");
		
	if (ok)
	{
		/* ��������� CRC � ���������� ��� ��� ������������� 
		������. */
		crc = CalcCRC32(p, size - sizeof(CRC));
		if (ok = Write((UCHAR*)&crc, sizeof(CRC)))
		{
			/* ���������� ����������� CRC � ��������. */
			if (!(ok = crc == *pcrc))
				ERRLOG("Rs232::GetFrame(): CRC error.");
		}
		else
			ERRLOG("Rs232::GetFrame(): CRC write error.");
	}

	if (!ok)
		PurgeComm(hcomm, PURGE_RXCLEAR | PURGE_TXCLEAR |
			PURGE_TXABORT | PURGE_RXABORT);

	return ok;
}

/*--------------------------------------------------------------------
Rs232::SendFrame()
� ����� ����� ������ ������ ���� ����� ��� CRC.
--------------------------------------------------------------------*/
BOOL Rs232::SendFrame(UCHAR* p, USHORT size)
{
	CRC crc;					/* ����� ��� ������ CRC. */
	CRC* pcrc = (CRC*)(p + size - sizeof(CRC)); /* ��������� ��
												CRC � �����. */
	BOOL ok;					/* ������. */
	UCHAR c;					/* ����� ��� �������� RTS �
								������ CTS. */
	USHORT bytes_written = 0;	/* ������� ������������ ����. */
	USHORT block;				/* ������ �����. */
	
	/* ��������� CRC. */
	*pcrc = CalcCRC32(p, size - sizeof(CRC));
	
	/* ���������� RTS. */
	c = RTS_CHAR;
	if (!(ok = Write(&c, sizeof(UCHAR))))
		ERRLOG("Rs232::SendFrame(): RTS write error.");
	
	/* ��������� �� ��� ����, ���� �� �������� �����-����
	������ � ����� ����������� ���� ������ ������� �����. */
	while (ok && bytes_written < size)
		/* ���� CTS. */
		if (ok = (Read(&c, sizeof(UCHAR)) &&
			c == CTS_CHAR))
		{
			/* ��������� ������ ���������� ������������� �����. */
			if ((block = size - bytes_written) > USART_BLOCK)
				block = USART_BLOCK;
			/* ���������� ��������� ����. */
			if (!(ok = Write(p + bytes_written, block)))
				ERRLOG("Rs232::SendFrame(): block write error.");
			/* ����������� ������� ������������ ����. */
			bytes_written += block;
		}
		else
			ERRLOG("Rs232::SendFrame(): CTS read error.");
	
	if (ok)
	{
		/* �������� ������ ��������� ����������� �� CRC32
		��� ����������� ������. */
		if (ok = Read((UCHAR*)&crc, sizeof(CRC)))
		{
			if (!(ok = crc == *pcrc))
				ERRLOG("Rs232::SendFrame(): CRC error.");
		}
		else
			ERRLOG("Rs232::SendFrame(): CRC read error.");
	}
	
	if (!ok)
		PurgeComm(hcomm, PURGE_RXCLEAR | PURGE_TXCLEAR |
			PURGE_TXABORT | PURGE_RXABORT);
		
	return ok;
}
