#include "precomp.h"
#include "global.h"
#include "expbmp.h"

/*--------------------------------------------------------------------
ExpBmpDialog::ExpBmpDialog()
--------------------------------------------------------------------*/
ExpBmpDialog::ExpBmpDialog()
{
	ZeroMemory(&expbmp, sizeof(EXPORT_BMP));
	expbmp.w = 512;
	expbmp.h = 256;
	expbmp.rle = TRUE;
}

/*--------------------------------------------------------------------
ExpBmpDialog::OnInitDialog()
--------------------------------------------------------------------*/
BOOL ExpBmpDialog::OnInitDialog(HWND hwnd, HWND hwndFocus,
								LPARAM lParam) 
{
	VERIFY(edit_w = GetDlgItem(hwnd, IDC_BMP_WIDTH));
	VERIFY(edit_h = GetDlgItem(hwnd, IDC_BMP_HEIGHT));
	VERIFY(static_filename = GetDlgItem(hwnd, IDC_BMP_FILE));
	VERIFY(check_rle = GetDlgItem(hwnd, IDC_RLE));

	ShowWindow(check_rle, SW_HIDE);

	Button_SetCheck(check_rle,
		expbmp.rle ? BST_CHECKED : BST_UNCHECKED);

	char buffer[16];
	sprintf(buffer, "%d", expbmp.w);
	Edit_SetText(edit_w, buffer);
	sprintf(buffer, "%d", expbmp.h);
	Edit_SetText(edit_h, buffer);

	return TRUE;
}

/*--------------------------------------------------------------------
ExpBmpDialog::OnCommand()
--------------------------------------------------------------------*/
void ExpBmpDialog::OnCommand(HWND hwnd, int id, HWND hwnd_ctl,
		UINT code_notify)
{
	EXPORT_BMP copy = expbmp;
	BOOL fault = FALSE;

	if (id == IDOK)
	{
		Static_GetText(static_filename, copy.file_name, 
			sizeof(copy.file_name));

		copy.rle = Button_GetCheck(check_rle) == BST_CHECKED;

		char buffer[16];
		Edit_GetText(edit_w, buffer, sizeof(buffer));
		copy.w = atoi(buffer);
		Edit_GetText(edit_h, buffer, sizeof(buffer));
		copy.h = atoi(buffer);

		if (copy.w < 512 || copy.w > 2000 ||
			copy.h < 256 || copy.h > 2000)
		{
			char msg[256];
			VERIFY(LoadString(Globals::GetInstanceHandle(),
				IDS_WH_OUT_OF_RANGE, msg, sizeof(msg)));
			MessageBox(hwnd, msg, "", MB_OK | MB_ICONEXCLAMATION);
			fault = TRUE;
		}

		if (!fault)
			expbmp = copy;
	}
	else if (id == IDC_SELECT_FILE)
	{
		char module_path[MAX_PATH];
		char file_name[MAX_PATH];

		Globals::GetModulePath(module_path, sizeof(module_path));
		*file_name = '\0';

		OPENFILENAME ofn;
		ZeroMemory(&ofn, sizeof(OPENFILENAME));
		ofn.lStructSize = sizeof(OPENFILENAME);
		ofn.hwndOwner = hwnd;
		ofn.lpstrFile = file_name;
		ofn.lpstrDefExt = "bmp";
		ofn.nMaxFile = sizeof(file_name);
		ofn.lpstrFilter =
			"(*.gif)\0*.gif\0(*.png)\0*.png\0(*.bmp)\0*.bmp\0\0";
		ofn.nFilterIndex = 1;
		ofn.lpstrFileTitle = NULL;
		ofn.nMaxFileTitle = 0;
		ofn.lpstrInitialDir = module_path;
		ofn.Flags = OFN_PATHMUSTEXIST | OFN_OVERWRITEPROMPT;

		if (GetSaveFileName(&ofn))
		{
			Static_SetText(static_filename, file_name);
			EnableWindow(GetDlgItem(hwnd, IDOK), TRUE);

			char ext[MAX_PATH];
			Globals::GetFileExt(ext, file_name);
			CharUpper(ext);
			ShowWindow(check_rle,
				strcmp(ext, "BMP") == 0 ? SW_SHOWNORMAL : SW_HIDE);
		}
	}

	if ((id == IDOK || id == IDCANCEL) && !fault)
		EndDialog(hwnd, id);
}
