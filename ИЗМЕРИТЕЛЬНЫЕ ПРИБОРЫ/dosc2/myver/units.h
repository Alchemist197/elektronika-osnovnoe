#ifndef __UNITS_H
#define __UNITS_H

/*--------------------------------------------------------------------
Units
--------------------------------------------------------------------*/
class Units
{
public:

	Units(): divisor(1) {
		strcpy(units, ""); }

	void Setup(REAL x, const char* units_name);

	char* Translate(char* buffer, REAL x) {
		sprintf(buffer, "%.2f%s", x/divisor, units); 
		return buffer; }

	static BOOL atof(const char* text, REAL* pvalue = NULL);

protected:
	
	/*--- ������ ---*/

	char units[16];
	REAL divisor;
};

#endif /* #ifndef __UNITS_H */