#ifndef __GLOBAL_H
#define __GLOBAL_H

/*--------------------------------------------------------------------
���� � ���������.
--------------------------------------------------------------------*/

typedef int BOOL;
typedef double REAL;
typedef short int SHORT;
typedef unsigned long ULONG;

#ifndef FALSE
#define FALSE 0
#define TRUE (!FALSE)
#endif /* #ifndef FALSE */

typedef vector<UCHAR> UCHAR_VECTOR;
typedef vector<USHORT> USHORT_VECTOR;
typedef vector<UINT> UINT_VECTOR;
typedef vector<int> INT_VECTOR;
typedef vector<REAL> REAL_VECTOR;
typedef vector<HWND> HWND_VECTOR;
typedef vector<POINT> POINT_VECTOR;

const int COORD_MIN = -10000;
const int COORD_MAX = +10000;

/*--------------------------------------------------------------------
�������.
--------------------------------------------------------------------*/

#ifdef _DEBUG
#define ASSERT(exp) assert(exp)
#define VERIFY(exp) assert(exp)
#else
#define ASSERT(exp)
#define VERIFY(exp) exp
#endif /* #ifdef _DEBUG */

#define ABS(x) ((x) < 0 ? -(x) : (x))
#define MAX(x, y) ((x) >= (y) ? (x) : (y))

#define THROW goto CATCH;

/*--------------------------------------------------------------------
Event
--------------------------------------------------------------------*/
class Event
{
public:
	Event(BOOL manual_reset = TRUE, BOOL initial_state = FALSE) {
		event_handle = CreateEvent(NULL, manual_reset,
			initial_state, NULL); 
		ASSERT(event_handle != NULL); }

	~Event() {
		VERIFY(CloseHandle(event_handle)); }

	HANDLE GetHandle() {
		return event_handle; } 

	void Set() {
		VERIFY(SetEvent(event_handle)); }

	void Reset() {
		VERIFY(ResetEvent(event_handle)); }

	void Wait(DWORD milliseconds = INFINITE) {
		WaitForSingleObject(event_handle, milliseconds); }

	BOOL IsSet() {
		return WaitForSingleObject(event_handle, 0) == 
			WAIT_OBJECT_0; }

protected:

	HANDLE event_handle;
};

/*--------------------------------------------------------------------
CriticalSection
--------------------------------------------------------------------*/
class CriticalSection
{
public:

	CriticalSection() {
		counter = 0;
		ZeroMemory(&cs, sizeof(CRITICAL_SECTION));
		InitializeCriticalSection(&cs); }

	~CriticalSection() {
		ASSERT(counter == 0);
		DeleteCriticalSection(&cs); }

	int GetCounter() {
		return counter; }

	void Enter() {		
		EnterCriticalSection(&cs); 
		++counter; }

	void Leave() {
		--counter;
		LeaveCriticalSection(&cs); }

protected:

	CRITICAL_SECTION cs;
	int counter;
};

#define ERRLOG(error) Globals::ErrLog(error)

/*--------------------------------------------------------------------
Globals
--------------------------------------------------------------------*/
class Globals
{
public:

	Globals();
	~Globals();

#ifdef _DEBUG	
	static void ErrLog(const char* error);
#else
	static void ErrLog(const char* error) {}
#endif

	static int Round(REAL x);
	static DWORD Align(DWORD dw, DWORD align) {
		return dw + (dw % align); }

	static void GetModulePath(char* buffer, UINT buffer_size);
	static void GetFileExt(char* buffer, const char* file_name);
	static void Log(const char* msg);
	static void Log(UINT string_id, const char* param = NULL);

	static void SetInstanceHandle(HINSTANCE hinstance) {
		Globals::hinstance = hinstance; }
	static void SetMainWindowPointer(void* pmainwnd) {
		Globals::pmainwnd = pmainwnd; }
	static void* GetMainWindowPointer() {
		return pmainwnd; }

	static HINSTANCE GetInstanceHandle() {
		ASSERT(hinstance != NULL);
		return hinstance; }

	static BOOL IsUint(const char* p);
	static BOOL IsFloat(const char* p);

protected:

	static HINSTANCE hinstance;
	static void* pmainwnd;
	
#ifdef _DEBUG
	static FILE* ferrlog;
#endif /* #ifdef _DEBUG */
};

#endif /* #ifndef __GLOBAL_H */
