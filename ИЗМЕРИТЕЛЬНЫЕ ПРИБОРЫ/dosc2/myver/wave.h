#ifndef __WAVE_H
#define __WAVE_H

#include "hwthread.h"

/*--------------------------------------------------------------------
Wave
--------------------------------------------------------------------*/
class Wave: public HardwareTh
{
public:

	Wave();

	virtual BOOL Init(HARDWARE_CALLBACK callback, void* pparam);
	virtual BOOL Shutdown();

	virtual char* GetName(char* name) {
		return strcpy(name, "�������� �����"); }

protected:

	virtual void IO_Thread_OnDataRequest();
	virtual BOOL IO_Thread_OnUpdateSettingsRequest(
		const HARDWARE_SETTINGS& hws);

	BOOL GetWave(USHORT* buffer, UINT nsamples);

	void ProcessData(USHORT* buffer, UINT nsamples,
		UINT_VECTOR& v, UINT buffer_size,
		const HARDWARE_SETTINGS& hws);

	HWAVEIN hwavein;
	Event event_data_ready;
	BOOL wavein_started;
};


#endif /* #ifndef __WAVE_H */
