#include "precomp.h"
#include "global.h"
#include "hwthread.h"

/*--------------------------------------------------------------------
HardwareTh::HardwareTh()
--------------------------------------------------------------------*/
HardwareTh::HardwareTh()
{
	io_thread_handle = NULL;

	/* ������� ������ ������� ��� ���������� ������� 
	�����-������. */
	for (int i = 0; i < NUMBER_OF_EVENTS; i++)
	{
		events[i] = CreateEvent(NULL, 
			TRUE	/* manual_reset. */, 
			FALSE	/* initial_state. */, 
			NULL);
		ASSERT(events[i] != NULL);
	}
}

/*--------------------------------------------------------------------
HardwareTh::~HardwareTh()
--------------------------------------------------------------------*/
HardwareTh::~HardwareTh()
{
	for (int i = 0; i < NUMBER_OF_EVENTS; i++)
		VERIFY(CloseHandle(events[i]));
}

/*--------------------------------------------------------------------
HardwareTh::Init()
--------------------------------------------------------------------*/
BOOL HardwareTh::Init(HARDWARE_CALLBACK callback, void* pparam)
{
	ASSERT(io_thread_handle == NULL);
	
	BOOL ok;
	if (!(ok = Hardware::Init(callback, pparam)))
		Shutdown();

	return ok;
}

/*--------------------------------------------------------------------
HardwareTh::CreateIOThread()
--------------------------------------------------------------------*/
BOOL HardwareTh::CreateIOThread(int priority)
{
	/* ������� �����, ���������� �� �����/�������� ������. */
	BOOL ok = (io_thread_handle = (HANDLE)_beginthreadex(NULL,
		0,
		IO_ThreadProc,
		(void*)this,
		0,
		NULL)) != NULL;

	if (ok)
	{
		ok = SetThreadPriority(io_thread_handle,
			priority);
	}

	return ok;
}

/*--------------------------------------------------------------------
HardwareTh::TerminateIOThread()
--------------------------------------------------------------------*/
BOOL HardwareTh::TerminateIOThread()
{
	BOOL ok = TRUE;

	if (io_thread_handle != NULL)
	{
		VERIFY(SetEvent(events[EVENT_TERMINATE_IO_THREAD_REQUEST]));
		WaitForSingleObject(io_thread_handle, INFINITE);

		if (!CloseHandle(io_thread_handle))
			ok = FALSE;

		io_thread_handle = NULL;
	}

	return ok;
}

/*--------------------------------------------------------------------
HardwareTh::Shutdown()
--------------------------------------------------------------------*/
BOOL HardwareTh::Shutdown()
{
	BOOL ok = TerminateIOThread();
	if (!Hardware::Shutdown())
		ok = FALSE;
	return ok;
}

/*--------------------------------------------------------------------
HardwareTh::SetHWS()
--------------------------------------------------------------------*/
void HardwareTh::UpdateHWS()
{
	VERIFY(SetEvent(events[EVENT_UPDATE_SETTINGS_REQUEST]));
}

/*--------------------------------------------------------------------
HardwareTh::RequestSample()
--------------------------------------------------------------------*/
void HardwareTh::RequestSample()
{
	VERIFY(SetEvent(events[EVENT_DATA_REQUEST]));
}

/*--------------------------------------------------------------------
HardwareTh::IO_ThreadProc()
--------------------------------------------------------------------*/
unsigned __stdcall HardwareTh::IO_ThreadProc(void* pparam)
{
	HardwareTh& hw = *(HardwareTh*)pparam;
	DWORD index = 0;
	BOOL exit_do = FALSE;

	do {
		WaitForMultipleObjects(NUMBER_OF_EVENTS,
			hw.events,
			FALSE /* wait_all. */,
			INFINITE);

		/* ������� ���������, ����� ��� ���������. */
		if (WaitForSingleObject(
			hw.events[EVENT_UPDATE_SETTINGS_REQUEST], 0) == 
			WAIT_OBJECT_0)
		{
			VERIFY(ResetEvent(
				hw.events[EVENT_UPDATE_SETTINGS_REQUEST]));
			HARDWARE_SETTINGS hws;
			hw.GetHWSBuffer(hws);
			if (hw.IO_Thread_OnUpdateSettingsRequest(hws))
				hw.SetHWS(hws);
		}

		/*---*/
		if (WaitForSingleObject(
			hw.events[EVENT_DATA_REQUEST], 0) == WAIT_OBJECT_0)
		{
			VERIFY(ResetEvent(hw.events[EVENT_DATA_REQUEST]));
			hw.IO_Thread_OnDataRequest();
		}

		/*---*/
		if (WaitForSingleObject(
			hw.events[EVENT_TERMINATE_IO_THREAD_REQUEST], 0) == 
			WAIT_OBJECT_0)
		{
			VERIFY(ResetEvent(
				hw.events[EVENT_TERMINATE_IO_THREAD_REQUEST]));
			exit_do = TRUE;
		}

	} while (!exit_do);

	return 0;
}
