#ifndef __COMM_H
#define __COMM_H

#include "hrtimer.h"

typedef unsigned long CRC;

/*--------------------------------------------------------------------
Rs232
��������� ����� �� �������������� ����-��������� ������ 
(TXD, RXD, GND). DTR  (Data Terminal Ready) ������ ���� ��������� 
�������� � DSR (Data Set Ready) � CD (Carrier Detect), 
� RTS (Request To Send) - c CTS (Clear To Send).

�������������� ������ ������ 8-N-1, �.�. 8 ���, 
��� �������� ��������, 1 �������� ���.
--------------------------------------------------------------------*/
class Rs232
{
public:

	Rs232();
	~Rs232() {
		Shutdown(); }

	BOOL Init(UINT port_number, UINT baud_rate);
	BOOL Shutdown();

	HANDLE GetHandle() {
		return hcomm; }

	BOOL SendFrame(UCHAR* p, USHORT size);
	BOOL GetFrame(UCHAR* p, USHORT size);

protected:

	CRC ByteCRC32(UCHAR byte);
	CRC CalcCRC32(UCHAR* p, DWORD buffer_size);

#ifdef _DEBUG
	void LogCommError(DWORD errors, const COMSTAT& comstat);
#else
	void LogCommError(DWORD errors, const COMSTAT& comstat) {}
#endif /* #ifdef _DEBUG */

	BOOL Read(UCHAR* p, DWORD size);
	BOOL Write(UCHAR* p, DWORD size);

	void WaitForErrorRecovery();
	void SetErrorState();

	/*--- ������. ---*/

	DCB dcb;
	HANDLE hcomm;
	CRC crc32_table[256];
};

#endif /* #ifndef __COMM_H */