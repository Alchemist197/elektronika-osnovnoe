#include "precomp.h"
#include "global.h"
#include "hrtimer.h"

/*--------------------------------------------------------------------
Timer::Timer()
--------------------------------------------------------------------*/
Timer::Timer()
{
	VERIFY(QueryPerformanceFrequency(&m_freq));
	Reset();
}
