#ifndef __HRTIMER_H
#define __HRTIMER_H

typedef __int64 INT64;

/*--------------------------------------------------------------------
������ � ������� ����������� (����� 1 ���.)
--------------------------------------------------------------------*/
class Timer
{
public:

	Timer();

	/* ��������� � ���������� �������. */
	void Reset() {
		VERIFY(QueryPerformanceCounter(&m_start)); }

	/* ��������� ������� (� ���.), ���������� � ������� 
	���������� ������ Reset(). */
	double GetElapsedTime() {
		return GetElapsedPeriods()/(double)m_freq.QuadPart; }

	/* ��������� ����� ������ �������, ��������� � �������
	���������� ������ Reset(). */
	INT64 GetElapsedPeriods() {
		LARGE_INTEGER pc;
		VERIFY(QueryPerformanceCounter(&pc));
		return (INT64)(pc.QuadPart - m_start.QuadPart); }

	/* ��������� ������� ������ �������. */
	INT64 GetFrequency() {
		return (INT64)m_freq.QuadPart; }

	INT64 GetElapsedTimeMs() {
		return GetElapsedPeriods()*1000/GetFrequency(); }

protected:

	LARGE_INTEGER m_freq;
	LARGE_INTEGER m_start;
};

#endif /* #ifndef __HRTIMER_H */
