#ifndef __MAINWND_H
#define __MAINWND_H

#include "window.h"
#include "view.h"
#include "controls.h"
#include "dcon.h"
#include "strobe.h"
#include "infopan.h"
#include "hardware.h"

/* ����� ��� MainWnd::flags. */
const UINT F_STARTED			= 0x0001;
const UINT F_DATA_REQUEST		= 0x0002;
const UINT F_UPDATE_SETTINGS	= 0x0004;

/*--------------------------------------------------------------------
MainWnd
--------------------------------------------------------------------*/
class MainWnd: public Window
{
public:

	MainWnd();
	~MainWnd();

	virtual BOOL Create();

	void Log(const char* msg) {
		if (infopan.GetHwnd() != NULL) infopan.Log(msg); }

	View& GetView() {
		return view; }

	void SetFlags(UINT flags) {
		MainWnd::flags |= flags; }

	/* ��� ������� ���������� �� WinMain. */
	void OnIdle();

	static void Hardware_Callback(const HARDWARE_NOTIFY& hwn);

protected:

	void CmdGet();
	void CmdStart();
	void CmdStop();
	void CmdAbout();
	void CmdCursors();
	void CmdExportBmp();
	void CmdFileSave();
	void CmdFileLoadBackground();
	void CmdFileCloseBackground();

	void HandleHardwareNotify(const HARDWARE_NOTIFY& hwn);

	virtual LPARAM CALLBACK WindowProc(HWND hwnd, UINT msg, 
		WPARAM wParam, LPARAM lParam);

	MSG_HANDLER void OnCommand(HWND hwnd, int id, HWND hwnd_ctl,
		UINT code_notify);
	MSG_HANDLER void OnDestroy(HWND hwnd);
	MSG_HANDLER void OnSize(HWND hwnd, UINT state, int cx, int cy);
	MSG_HANDLER void OnClose(HWND hwnd);

	/*--- ������. ---*/

	View view;
	Controls controls;
	DControls dcon;
//	Strobe strobe;
	InfoPan infopan;
	Tab tab;
	Data background;
	char caption[256];

	UINT flags;
	HardwareNotifyQueue hwn_queue;
};

#endif /* #ifndef __MAINWND_H */
