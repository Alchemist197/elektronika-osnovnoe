#include "precomp.h"
#include "global.h"
#include "controls.h"
#include "mainwnd.h"
#include "units.h"
#include "resource.h"

const UINT TRACK_MAX = 1023;

/*--------------------------------------------------------------------
Controls::UpdateControls()
--------------------------------------------------------------------*/
void Controls::UpdateControls()
{
	char buffer[256];
	
	SETTINGS s;
	_settings.Get(s);

	Hardware& hw = *_hardware[s.selected_device_index];

	HARDWARE_SETTINGS hws;
	hw.GetHWSBuffer(hws);

	HARDWARE_CAPS caps;
	hw.GetCaps(caps);

	Resolution& res = caps.resolutions[hws.resolution_index];


	/* combo_devices. */
	ComboBox_ResetContent(combo_devices);
	for (size_t i = 0; i < _hardware.size(); i++)
	{
		char name[256];
		ComboBox_AddString(combo_devices,
			_hardware[i]->GetName(name));
	}
	ComboBox_SetCurSel(combo_devices,
		s.selected_device_index);

	/* combo_resolutions. */
	ComboBox_ResetContent(combo_resolutions);
	for (size_t i = 0; i < caps.resolutions.size(); i++)
	{
		caps.resolutions[i].GetName(buffer);
		ComboBox_AddString(combo_resolutions,
			buffer);
	}
	ComboBox_SetCurSel(combo_resolutions,
		hws.resolution_index);

	/* combo_buffer_sizes. */
	UINT_VECTOR sizes;
	res.GetBufferSizes(sizes);

	ComboBox_ResetContent(combo_buffer_sizes);
	for (size_t i = 0; i < sizes.size(); i++)
	{
		sprintf(buffer, "%d", sizes[i]);
		ComboBox_AddString(combo_buffer_sizes, buffer);
	}
	ComboBox_SetCurSel(combo_buffer_sizes,
		hws.buffer_size_index);
		
	/* combo_sample_times. */ //���������� ComboBox ���������� �������������
	ComboBox_ResetContent(combo_sample_times);
	for (UINT i = 0; i <= res.GetMaxScale(); i++)
	{
		REAL time = res.GetTime((UINT)i);

		Units units;
		units.Setup(time, "s");
			
		ComboBox_AddString(combo_sample_times, 
			units.Translate(buffer, time));
	}
	ComboBox_SetCurSel(combo_sample_times,
		hws.resolution_scale);

	/* combo_voltage_ranges */
	ComboBox_ResetContent(combo_voltage_ranges);
	for (size_t i = 0; i < caps.ranges.size(); i++)
	{
		Range& range = caps.ranges[i];
		char range_l[16];
		char range_h[16];

		Units units;
		units.Setup(range.GetMin(), "V");
		units.Translate(range_l, range.GetMin());
		units.Setup(range.GetMax(), "V");
		units.Translate(range_h, range.GetMax());

		sprintf(buffer, "%s / %s", range_l, range_h);
		ComboBox_AddString(combo_voltage_ranges, buffer);
	}
	ComboBox_SetCurSel(combo_voltage_ranges,
		hws.range_index);

	/* track_capture_level. */
	SendMessage(track_capture_level, TBM_SETRANGE, (WPARAM)FALSE,
		MAKELPARAM(0, TRACK_MAX));
	SendMessage(track_capture_level, TBM_SETPOS, (WPARAM)TRUE,
		(LPARAM)(TRACK_MAX - (ULONG)hws.capture_level*TRACK_MAX/
		caps.max_capture_level));

	/* track_capture_delay. */
	SendMessage(track_capture_delay, TBM_SETRANGE, (WPARAM)FALSE,
		MAKELPARAM(0, TRACK_MAX));
	SendMessage(track_capture_delay, TBM_SETPOS, (WPARAM)TRUE,
		(LPARAM)(TRACK_MAX - (ULONG)hws.capture_delay*TRACK_MAX/
		caps.max_capture_delay));

	/* check_capture_enabled. */
	Button_SetCheck(check_capture_enabled,
		(hws.flags & HWS_CAPTURE_ENABLED) ?
		BST_CHECKED : BST_UNCHECKED);

	/* combo_slope. */
	ComboBox_SetCurSel(combo_slope,
		(hws.flags & HWS_SLOPE_POSEDGE) ?
		1 : 0);

	/* combo_slope2. int/ext */
	ComboBox_SetCurSel(combo_slope2,
		(hws.flags & HWS_CAPTURE_TYPE) ?
		1 : 0);
	/* combo_slope3. AC/DC */
	ComboBox_SetCurSel(combo_slope3,
		(hws.flags & HWS_INPUT_ACDC) ?
		1 : 0);

	/* check_tiemout. */
/*	Button_SetCheck(check_timeout,
		(hws.flags & HWS_TIMEOUT_ENABLED) ?
		BST_CHECKED : BST_UNCHECKED);
	EnableWindow(check_timeout,
		caps.flags & HWC_TIMEOUT_CONTROL);
	ShowWindow(check_timeout,
		(caps.flags & HWC_TIMEOUT_CONTROL) ?
		SW_SHOWNORMAL : SW_HIDE);*/

	EnableWindow(track_capture_level,
		hws.flags & HWS_CAPTURE_ENABLED);
	EnableWindow(track_capture_delay, 1);	//��������/��������� "������" Y-pos
		//hws.flags & HWS_CAPTURE_ENABLED);
	EnableWindow(combo_slope,
		hws.flags & HWS_CAPTURE_ENABLED);
	EnableWindow(combo_slope2,
		hws.flags & HWS_CAPTURE_ENABLED);
	EnableWindow(combo_slope3,
		TRUE);
}

/*--------------------------------------------------------------------
Controls::OnInitDialog()
--------------------------------------------------------------------*/
BOOL Controls::OnInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam) 
{
	VERIFY(combo_devices = GetDlgItem(hwnd, IDC_DEVICES));
	VERIFY(combo_resolutions = GetDlgItem(hwnd, IDC_RESOLUTIONS));
	VERIFY(combo_buffer_sizes = GetDlgItem(hwnd, IDC_BUFFER_SIZES));
	VERIFY(combo_sample_times = GetDlgItem(hwnd, IDC_SAMPLE_TIMES));
	VERIFY(combo_voltage_ranges = 
		GetDlgItem(hwnd, IDC_VOLTAGE_RANGES));
	VERIFY(combo_slope = GetDlgItem(hwnd, IDC_SLOPE));
	VERIFY(combo_slope2 = GetDlgItem(hwnd, IDC_SLOPE2));
	VERIFY(combo_slope3 = GetDlgItem(hwnd, IDC_SLOPE3));

//	VERIFY(butt_start = GetDlgItem(hwnd, IDC_STARTBUTT));
//	VERIFY(butt_stop = GetDlgItem(hwnd, IDC_STOPBUTT));
//	VERIFY(butt_sample = GetDlgItem(hwnd, IDC_SAMPLE));

	VERIFY(track_capture_level = GetDlgItem(hwnd, IDC_CAPTURE_LEVEL));
	VERIFY(track_capture_delay = GetDlgItem(hwnd, IDC_CAPTURE_DELAY));

	VERIFY(check_capture_enabled =
		GetDlgItem(hwnd, IDC_CAPTURE_ENABLED));
	//VERIFY(check_timeout = GetDlgItem(hwnd, IDC_TIMEOUT));

	ComboBox_AddString(combo_slope, "rise");
	ComboBox_AddString(combo_slope, "fall");

	ComboBox_AddString(combo_slope2, "internal");
	ComboBox_AddString(combo_slope2, "external");

	ComboBox_AddString(combo_slope3, "DC");
	ComboBox_AddString(combo_slope3, "AC");

	UpdateControls();

	return TRUE; 
}
/*--------------------------------------------------------------------
Controls::OnCommand()
--------------------------------------------------------------------*/
void Controls::OnCommand(HWND hwnd, int id, HWND hwnd_ctl,
	UINT code_notify)
{
	SETTINGS s;
	_settings.Get(s);
	SETTINGS backup = s;

	HARDWARE_SETTINGS hws, backup_hws;
	Hardware* phw = _hardware[s.selected_device_index];
	phw->GetHWSBuffer(hws);
	backup_hws = hws;

	if (hwnd_ctl == check_capture_enabled)
	{
		if (Button_GetCheck(check_capture_enabled) == BST_CHECKED)
			hws.flags |= HWS_CAPTURE_ENABLED;
		else
			hws.flags &= ~HWS_CAPTURE_ENABLED;
	}
	else /*if (hwnd_ctl == check_timeout)
	{
		if (Button_GetCheck(check_timeout) == BST_CHECKED)
			hws.flags |= HWS_TIMEOUT_ENABLED;
		else
			hws.flags &= ~HWS_TIMEOUT_ENABLED;
	}*/

	if (code_notify == CBN_SELENDOK)
	{
		if (hwnd_ctl == combo_devices)
		{
			s.selected_device_index = 
				ComboBox_GetCurSel(combo_devices);
		}
		else if (hwnd_ctl == combo_resolutions)
		{
			hws.resolution_index = ComboBox_GetCurSel(
				combo_resolutions);
		}
		else if (hwnd_ctl == combo_buffer_sizes)
		{
			hws.buffer_size_index = 
				ComboBox_GetCurSel(combo_buffer_sizes);
		}
		else if (hwnd_ctl == combo_sample_times)
		{
			hws.resolution_scale = 
				ComboBox_GetCurSel(combo_sample_times);
		}
		else if (hwnd_ctl == combo_voltage_ranges)
		{
			hws.range_index = 
				ComboBox_GetCurSel(combo_voltage_ranges);
		}
		else if (hwnd_ctl == combo_slope)
		{
			if (ComboBox_GetCurSel(combo_slope) == 1)
				hws.flags |= HWS_SLOPE_POSEDGE;
			else
				hws.flags &= ~HWS_SLOPE_POSEDGE;
		}
		else if (hwnd_ctl == combo_slope2)
		{
			if (ComboBox_GetCurSel(combo_slope2) == 1)
				hws.flags |= HWS_CAPTURE_TYPE;
			else
				hws.flags &= ~HWS_CAPTURE_TYPE;
		}
		else if (hwnd_ctl == combo_slope3)
		{
			if (ComboBox_GetCurSel(combo_slope3) == 1)
				hws.flags |= HWS_INPUT_ACDC;
			else
				hws.flags &= ~HWS_INPUT_ACDC;
		}
	}

	MainWnd& mainwnd = *(MainWnd*)Globals::GetMainWindowPointer();
	View& view = mainwnd.GetView();
	BOOL update_controls = FALSE;
	BOOL update_view = FALSE;

	/* ������� ������ ����������? */
	if (s.selected_device_index != backup.selected_device_index)
	{
		/* ��������� ������ � �������� �����������. */
		if (!phw->Shutdown())
		{
			char name[256];
			Globals::Log(IDS_HARDWARE_SHUTDOWN_FAILED,
				phw->GetName(name));
		}

		/* �������������� ����� ����������. */
		BOOL retry;
		do {
			phw = _hardware[s.selected_device_index];
			if (phw->Init(MainWnd::Hardware_Callback, &mainwnd))
			{
				/* ����������� ��������� ���������� �
				����������� ������ ������. */
				mainwnd.SetFlags(F_UPDATE_SETTINGS | F_DATA_REQUEST);
				retry = FALSE;
			}
			else
			{
				char name[256];
				Globals::Log(IDS_HARDWARE_INIT_FAILED,
					phw->GetName(name));
				Globals::Log(IDS_EMULATOR);

				/* ������������� �� ��������. */
				s.selected_device_index = 0;
				retry = TRUE;
			}
		} while (retry);

		phw->GetHWSBuffer(hws);
		backup_hws = hws;

		view.ClearGraphs();
		update_view = update_controls = TRUE;
	}

	/* ������� ������ ����������? */
	if (hws.resolution_index != backup_hws.resolution_index)
	{
		hws.buffer_size_index = 0;
		hws.resolution_scale = 0;
		update_controls = TRUE;
	}

	/* ��������/��������� �������������? */
	if (hws.flags & HWS_CAPTURE_ENABLED ^
		backup_hws.flags & HWS_CAPTURE_ENABLED)
	{
		update_controls = TRUE;
	}

	/* ���������� ��������� ����������? */
	if (memcmp(&backup_hws, &hws, sizeof(HARDWARE_SETTINGS)) != 0)
	{
		phw->SetHWSBuffer(hws);
		mainwnd.SetFlags(F_UPDATE_SETTINGS | F_DATA_REQUEST);
		view.ResetMemory();
		update_view = TRUE;
	}

	/* ���������� ���������? */
	if (memcmp(&backup, &s, sizeof(SETTINGS)) != 0)
	{
		update_view = TRUE;
		_settings.Set(s);
	}

	if (update_view) view.UpdateClient();
	if (update_controls) UpdateControls();
};

/*--------------------------------------------------------------------
Controls::OnTrack()
--------------------------------------------------------------------*/
void Controls::OnTrack(HWND hwnd, HWND hwnd_track,
		UINT notify_code, UINT position)
{
	if (notify_code == TB_ENDTRACK ||
		notify_code == TB_THUMBTRACK)
	{
		SETTINGS s;
		_settings.Get(s);

		Hardware& hw = *_hardware[s.selected_device_index];
		HARDWARE_CAPS caps;
		hw.GetCaps(caps);
		HARDWARE_SETTINGS hws, backup_hws;
		hw.GetHWSBuffer(hws);
		backup_hws = hws;

		UINT pos = (UINT)SendMessage(hwnd_track, TBM_GETPOS, 0, 0);

		if (hwnd_track == track_capture_level)
		{
			hws.capture_level = (UINT)((ULONG)(TRACK_MAX - pos)*
				caps.max_capture_level/TRACK_MAX);
		}
		else if (hwnd_track == track_capture_delay)
		{
			hws.capture_delay = (UINT)((ULONG)(TRACK_MAX - pos)*
				caps.max_capture_delay/TRACK_MAX);
		}

		/* ���-�� ����������? */
		if (memcmp(&backup_hws, &hws, sizeof(HARDWARE_SETTINGS)) != 0)
		{
			MainWnd& mainwnd =
				*(MainWnd*)Globals::GetMainWindowPointer();
			View& view = mainwnd.GetView();
			
			hw.SetHWSBuffer(hws);
			mainwnd.SetFlags(F_UPDATE_SETTINGS | F_DATA_REQUEST);
			view.ResetMemory();
			view.UpdateClient();
		}
	}
}

/*--------------------------------------------------------------------
Controls::WindowProc()
--------------------------------------------------------------------*/
LPARAM CALLBACK Controls::WindowProc(HWND hwnd, UINT msg, 
		WPARAM wParam, LPARAM lParam)
{
	if (msg == WM_VSCROLL)
	{
		OnTrack(hwnd, (HWND)lParam, LOWORD(wParam), HIWORD(wParam));
		return (LPARAM)TRUE;
	}

	return Panel::WindowProc(hwnd, msg, wParam, lParam);
}
