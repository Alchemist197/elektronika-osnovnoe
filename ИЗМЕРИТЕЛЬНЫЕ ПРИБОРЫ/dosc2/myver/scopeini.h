#ifndef __SCOPEINI_H
#define __SCOPEINI_H

#include "hardware.h"

/*--------------------------------------------------------------------
ScopeIniFile
--------------------------------------------------------------------*/
class ScopeIniFile
{
public:

	struct INI_DATA
	{
		ULONG freq;
		UINT port;
		UINT baud_rate;
		UINT buffer_size;
		RANGE_VECTOR ranges;
	};

	/* ������ ������ �� ����� �������������. ���� ���� �� ����������,
	�� ����� ������������ ������������� (� ������� �� ���������).
	� ������ ������������� �����-���� ������ ������� ���������� FALSE,
	��� ���� ���������� ini_data �� ����������. */
	static BOOL Read(INI_DATA& ini_data);

	/* ��������� ������ �� ���������. */
	static void GetDefault(INI_DATA& ini_data);

protected:

	static BOOL Translate(INI_DATA& ini_data,
		char* buffer, long size);
	static void NextWord(const char*& p, const char* pend,
		char* word, UINT sizeof_word, BOOL& ok);
	static BOOL Create(const char* file_name);
};

#endif /* #ifndef __SCOPEINI_H */
