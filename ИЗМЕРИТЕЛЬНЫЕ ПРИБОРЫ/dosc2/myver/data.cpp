#include "precomp.h"
#include "global.h"
#include "data.h"
#include "fft.h"

const ULONG SIGNATURE = 0x61746164; /* "data" */

/*--------------------------------------------------------------------
Data::Save()
--------------------------------------------------------------------*/
BOOL Data::Save(FILE* f) const
{
	ASSERT(f != NULL);

	FILE_HEADER fh;
	ZeroMemory(&fh, sizeof(FILE_HEADER));
	fh.signature = SIGNATURE;
	fh.ds = ds;
	fh.size = v.size();

	if (!fwrite(&fh, sizeof(FILE_HEADER), 1, f)) THROW;


	if (fh.ds.bits_per_sample <= 8)
	{
		UCHAR_VECTOR tmpv;
		tmpv.resize(v.size());
		for (UINT i = 0; i < v.size(); i++) tmpv[i] = (UCHAR)v[i];
		if (fwrite(&*tmpv.begin(), sizeof(UCHAR), tmpv.size(), f) !=
			tmpv.size()) THROW;
	}
	else if (fh.ds.bits_per_sample <= 16)
	{
		USHORT_VECTOR tmpv;
		tmpv.resize(v.size());
		for (UINT i = 0; i < v.size(); i++) tmpv[i] = (USHORT)v[i];
		if (fwrite(&*tmpv.begin(), sizeof(USHORT), tmpv.size(), f) !=
			tmpv.size()) THROW;
	}
	else
	{
		if (fwrite(&*v.begin(), sizeof(UINT), v.size(), f) !=
			v.size()) THROW;
	}

	return TRUE;
CATCH:
	return FALSE;
}

/*--------------------------------------------------------------------
Data::Load()
--------------------------------------------------------------------*/
BOOL Data::Load(FILE* f)
{
	ASSERT(f != NULL);

	FILE_HEADER fh;
	UINT_VECTOR uint_v;

	if (!fread(&fh, sizeof(FILE_HEADER), 1, f)) THROW;
	if (fh.signature != SIGNATURE) THROW;

	uint_v.resize(fh.size);

	if (fh.ds.bits_per_sample <= 8)
	{
		UCHAR_VECTOR tmpv;
		tmpv.resize(fh.size);
		if (fread(&*tmpv.begin(), sizeof(UCHAR), tmpv.size(), f) !=
			tmpv.size()) THROW;
		for (UINT i = 0; i < tmpv.size(); i++) uint_v[i] = tmpv[i];
	}
	else if (fh.ds.bits_per_sample <= 16)
	{
		USHORT_VECTOR tmpv;
		tmpv.resize(fh.size);
		if (fread(&*tmpv.begin(), sizeof(USHORT), tmpv.size(), f) !=
			tmpv.size()) THROW;
		for (UINT i = 0; i < tmpv.size(); i++) uint_v[i] = tmpv[i];
	}
	else
	{
		if (fread(&*uint_v.begin(), sizeof(UINT), uint_v.size(), f) !=
			uint_v.size()) THROW;
	}

	Setup(uint_v, fh.ds);

	return TRUE;
CATCH:
	Clear();
	return FALSE;
}

/*--------------------------------------------------------------------
Data::GetFFTMaxFreq()
���������� ������� ������������� ���������.
--------------------------------------------------------------------*/
REAL Data::GetFFTMaxFreq() const
{
	/* ������� ������� ������, ��������� � v.size(). */
	size_t pwr2_size = 1;
	do {
		pwr2_size <<= 1;
	} while (pwr2_size < v.size());

	static COMPLEX_VECTOR cv;
	if (cv.size() < pwr2_size) cv.resize(pwr2_size);
	ZeroMemory(&*cv.begin(), pwr2_size*sizeof(COMPLEX));

	/* ��������� ������ ����������� ����� � ��������
	������� �������. */
	for (size_t i = 0; i < v.size(); i++)
		cv[i].re = v[i];

	/* ��������� ���. */
	FFT::DoFFT(&*cv.begin(), pwr2_size);

	/* ���� ������������ ��������. */
	/* �������������� max_abs � max_index ���������� ������������. */
	int max_index = 0;
	REAL max_abs = FFT::Abs(cv[max_index]);
	/*---*/
	if (pwr2_size > 2)
	{
		/* ���� ������ ������� ������ 2 (4, 8, 16...),
		���������� ������������ ��� �� ����������. */
		max_index = 1;
		max_abs = FFT::Abs(cv[max_index]);
		for (size_t i = 2; i < (pwr2_size >> 1); i++)
		{
			REAL abs = FFT::Abs(cv[i]);
			if (abs > max_abs)
			{
				max_index = i;
				max_abs = abs;
			}
		}
	}

	return max_index/(pwr2_size*ds.time_per_sample);
}
