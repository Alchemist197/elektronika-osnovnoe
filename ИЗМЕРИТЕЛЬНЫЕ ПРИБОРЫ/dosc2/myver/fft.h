#ifndef __FFT_H
#define __FFT_H

/*--------------------------------------------------------------------
COMPLEX
--------------------------------------------------------------------*/
struct COMPLEX
{
	REAL re;
	REAL im;
};

typedef vector<COMPLEX> COMPLEX_VECTOR;

/*--------------------------------------------------------------------
FFT
�����-�������� ��� ����������� �������.
--------------------------------------------------------------------*/
class FFT
{
public:

	FFT() {
		pi = acos(-1.0); }

	/* (�������-��������)
	���������� ��� ������� v. ��������� ������������ �� ����� ��������
	������. */
	static void DoFFT(COMPLEX* v, 
		ULONG n /* ������ ������� - ������� ������. */) {
		Reorder(v, n);
		RFFT(v, n);	}
	
	/* ������ ������������ �����. */
	static REAL Abs(const COMPLEX& x) {
		return sqrt(x.re*x.re + x.im*x.im); }

protected:

	/* ����������� ���������� ��� ������� v. ��������� ������������
	�� ����� �������� ������. ������� ������ ������ ����
	�������������� ��������� �������� FFT::Reorder(). */
	static void RFFT(COMPLEX* v,
		ULONG n /* ������ ������� - ������� ������. */);

	/* ����������� ���������� ��� ������� v. ��������� ������������
	�� ����� �������� ������. ������� ������ ������ ����
	�������������� ��������� �������� FFT::Reorder(). */
	static void IFFT(COMPLEX* v,
		ULONG n /* ������ ������� - ������� ������. */);

	/* ��������������� ������������ ��������� �������� ������� ���
	������������� �������������� ������� ��� "�� �����". */
	static void Reorder(COMPLEX* v,
		ULONG n /* ������ ������� - ������� ������. */);
	
	/* ����������� ���������� ���������� ����� �� ������� n-��
	�������. ��� ��������� �������� ��� ������ 32-�� ������ �� �������
	����������� ���������������. */
	static void NextRoot(COMPLEX& w, const COMPLEX& w_main,
		ULONG i, ULONG n)
	{
		if ((i & 0x1F) == 0)
			RootOfUnity(w, i, n);
		else
		{
			COMPLEX t = w;
			w.re = t.re*w_main.re - t.im*w_main.im;
			w.im = t.re*w_main.im + t.im*w_main.re;
		}
	}
	
	/* ���������� i-��� ����� �� ������� n-�� �������. */
	static void RootOfUnity(COMPLEX& x, ULONG i, ULONG n) {
		REAL t = 2*pi*i/n;
		x.re = cos(t);
		x.im = sin(t); };

	static REAL pi;
};

extern FFT _fft;

#endif /* #ifndef __FFT_H */
