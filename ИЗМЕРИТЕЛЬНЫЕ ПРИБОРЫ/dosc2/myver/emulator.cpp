#include "precomp.h"
#include "global.h"
#include "emulator.h"

/*--------------------------------------------------------------------
Emulator::Init()
--------------------------------------------------------------------*/
/*BOOL Emulator::Init(HARDWARE_CALLBACK callback, void* pparam)
{
	BOOL ok = HardwareTh::Init(callback, pparam);

	caps.ranges.push_back(Range(0, 2));
	caps.ranges.push_back(Range(0, 20));
	caps.ranges.push_back(Range(-1, 1));
	caps.ranges.push_back(Range(-10, 10));

	caps.resolutions.push_back(
		Resolution(RES_LOW, "������", 1E-4, 1E-5, 100).
		AddBufferSize(256).
		AddBufferSize(512));

	caps.resolutions.push_back(
		Resolution(RES_NORM, "�������", 1E-6, 1E-6, 10).
		AddBufferSize(64).
		AddBufferSize(128));

	caps.resolutions.push_back(
		Resolution(RES_HIGH, "�������", 1E-9, 0, 0).
		AddBufferSize(32));

	if (ok)
		ok = CreateIOThread();

	if (!ok)
		Shutdown();

	return ok;
}*/

/*--------------------------------------------------------------------
Emulator::IO_Thread_OnDataRequest()
--------------------------------------------------------------------*/
/*void Emulator::IO_Thread_OnDataRequest()
{
	HARDWARE_SETTINGS hws;
	GetHWS(hws);

	Resolution& res = caps.resolutions[hws.resolution_index];
	REAL pi = acos(-1.0);
	
	UINT buffer_size = res.GetBufferSize(hws.buffer_size_index);
	REAL time_per_sample = res.GetTime(hws.resolution_scale);

	// ����� ������� (�). 
	REAL full_time = buffer_size*time_per_sample;
	// ������ ��������� (�). 
	REAL period = full_time/5;
	// �������� ������� (���/�).
	REAL w = 2*pi/period;
	// ��������� (������).
	UINT a = (1 << (caps.bits_per_sample - 2));
	// �������� (������).
	UINT offset = (1 << (caps.bits_per_sample - 1));
	// ����. 
	REAL phase = 2*pi*rand()/RAND_MAX;

	ULONG nsample = 0;
#define F(n)\
	(offset + (UINT)(a*sin(phase + w*(n)*time_per_sample)))
	
	UINT start_countdown = buffer_size;

	if (hws.flags & HWS_CAPTURE_ENABLED)
	{
		if (hws.flags & HWS_SLOPE_POSEDGE)
		{
			// ����� �� ����������.
			while (F(nsample++) >= hws.capture_level && 
				start_countdown != 0) --start_countdown;
			while (F(nsample++) < hws.capture_level && 
				start_countdown != 0) --start_countdown;
		}
		else
		{
			// ����� �� �����.
			while (F(nsample++) <= hws.capture_level && 
				start_countdown != 0) --start_countdown;
			while (F(nsample++) > hws.capture_level && 
				start_countdown != 0) --start_countdown;
		}

		// �������� �������. 
		nsample += hws.capture_delay;
	}
	
	UINT_VECTOR v;
	while (v.size() < buffer_size)
	{
		v.push_back(F(nsample++));
	}

	Notify(HWN_SAMPLE | HWN_SUCCESS, &v);
}*/

/*--------------------------------------------------------------------
Emulator::IO_Thread_OnUpdateSettingsRequest()
--------------------------------------------------------------------*/
/*BOOL Emulator::IO_Thread_OnUpdateSettingsRequest(
	const HARDWARE_SETTINGS& hws)
{
	Notify(HWN_SETTINGS | HWN_SUCCESS, NULL);
	return TRUE;
}*/
