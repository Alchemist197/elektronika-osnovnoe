#ifndef __DCON_H
#define __DCON_H

#include "window.h"
#include "resource.h"
#include "settings.h"

/*--------------------------------------------------------------------
DControls
������ ���������� ����������� ���� ����������� ������������ (View).
--------------------------------------------------------------------*/
class DControls: public Panel
{
public:

	void UpdateControls();

protected:

	MSG_HANDLER BOOL OnInitDialog(HWND hwnd, HWND hwndFocus, 
		LPARAM lParam);
	MSG_HANDLER void OnCommand(HWND hwnd, int id, HWND hwnd_ctl,
		UINT code_notify);

	virtual UINT GetId() {
		return IDD_DCON; }

	HWND combo_line_width;
	HWND combo_memory;
	HWND combo_minmax_timeout;

	HWND check_calc_fft_max;
	HWND check_show_data_points;
	HWND check_enable_min;
	HWND check_enable_max;
};

#endif /* #ifndef __DCON_H */