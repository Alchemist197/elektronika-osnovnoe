#ifndef __SETTINGS_H
#define __SETTINGS_H

#include "hardware.h"

/*--------------------------------------------------------------------
SETTINGS
--------------------------------------------------------------------*/
struct SETTINGS
{
	/* ������ ���������� ����������. */
	UINT selected_device_index;
};

/*--------------------------------------------------------------------
Settings
--------------------------------------------------------------------*/
class Settings
{
public:

	Settings();

	void Get(SETTINGS& s) {
		s = settings; }

	void Set(const SETTINGS& s) {
		settings = s; }

protected:

	SETTINGS settings;
};

extern Settings _settings;

#endif /* #ifndef __SETTINGS_H */
