#include "precomp.h"
#include "global.h"
#include "window.h"

/*////////////////////////////////////////////////////////////////////
WindowBase
////////////////////////////////////////////////////////////////////*/

WindowBase* WindowBase::plast_window_created = NULL;
CriticalSection WindowBase::cs;


/*--------------------------------------------------------------------
WindowBase::WindowBase()
--------------------------------------------------------------------*/
WindowBase::WindowBase(BOOL is_dialog)
{
	WindowBase::is_dialog = is_dialog;
	hwnd = NULL;
}

/*--------------------------------------------------------------------
WindowBase::~WindowBase()
--------------------------------------------------------------------*/
WindowBase::~WindowBase()
{
}

/*--------------------------------------------------------------------
WindowBase::PrepareObjectPointer()
--------------------------------------------------------------------*/
void WindowBase::PrepareObjectPointer() 
{
	cs.Enter();

	ASSERT(plast_window_created == NULL);
	plast_window_created = this; 
}

/*--------------------------------------------------------------------
WindowBase::FreeObjectPointer()
--------------------------------------------------------------------*/
void WindowBase::FreeObjectPointer()
{
	plast_window_created = NULL; 

	cs.Leave();
}

/*--------------------------------------------------------------------
WindowBase::WindowProc()
--------------------------------------------------------------------*/
LPARAM CALLBACK WindowBase::WindowProc(HWND hwnd, UINT msg, 
		WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	HANDLE_MSG(hwnd, WM_DESTROY, OnDestroy);
	HANDLE_MSG(hwnd, WM_CLOSE, OnClose);
	HANDLE_MSG(hwnd, WM_COMMAND, OnCommand);
	HANDLE_MSG(hwnd, WM_SIZE, OnSize);
	HANDLE_MSG(hwnd, WM_TIMER, OnTimer);
	/*---*/
	HANDLE_MSG(hwnd, WM_LBUTTONDOWN, OnLButtonDown);
	HANDLE_MSG(hwnd, WM_LBUTTONUP, OnLButtonUp);
	HANDLE_MSG(hwnd, WM_MOUSEMOVE, OnMouseMove);
	case WM_NOTIFY:
		return OnNotify(hwnd, (int)wParam, (NMHDR*)lParam);
	default:
		if (!is_dialog)
			return DefWindowProc(hwnd, msg, wParam, lParam);
	}

	return 0;
}

/*--------------------------------------------------------------------
WindowBase::StaticWindowProc()
--------------------------------------------------------------------*/
LRESULT CALLBACK WindowBase::StaticWindowProc(HWND hwnd, UINT msg,
								  WPARAM wParam, LPARAM lParam)
{
	WindowBase* object_pointer = plast_window_created;

	if (plast_window_created != NULL &&
		GetWindowLong(hwnd, GWL_USERDATA) == 0)
	{
		SetWindowLong(hwnd, GWL_USERDATA, (LONG)plast_window_created);
		plast_window_created = NULL;
	}
	else
	{
		object_pointer = 
			(WindowBase*)GetWindowLong(hwnd, GWL_USERDATA);
	}

	ASSERT(object_pointer != NULL);

	return object_pointer->WindowProc(hwnd, msg, wParam, lParam);
}

/*////////////////////////////////////////////////////////////////////
Window
////////////////////////////////////////////////////////////////////*/

/*--------------------------------------------------------------------
Window::Window()
--------------------------------------------------------------------*/
Window::Window(): WindowBase(FALSE)
{
	ZeroMemory(&wndclass, sizeof(WNDCLASS));

	wndclass.hbrBackground = (HBRUSH)GetStockObject(LTGRAY_BRUSH);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hInstance = Globals::GetInstanceHandle();
	wndclass.lpfnWndProc = StaticWindowProc;

	ZeroMemory(&prop, sizeof(CREATESTRUCT));

	prop.x = CW_USEDEFAULT;
	prop.y = CW_USEDEFAULT;
	prop.cx = CW_USEDEFAULT;
	prop.cy = CW_USEDEFAULT;
}

/*--------------------------------------------------------------------
Window::RegisterClass()
--------------------------------------------------------------------*/
BOOL Window::RegisterClass()
{
	return ::RegisterClass(&wndclass) != 0;
}

/*--------------------------------------------------------------------
Window::Create()
--------------------------------------------------------------------*/
BOOL Window::Create(HWND parent)
{
	VERIFY(RegisterClass());

	prop.hwndParent = parent;
	prop.lpszClass = wndclass.lpszClassName;

	PrepareObjectPointer();
	hwnd = CreateWindow(prop.lpszClass,
		prop.lpszName,
		prop.style,
		prop.x, prop.y, prop.cx, prop.cy,
		prop.hwndParent, prop.hMenu, 
		Globals::GetInstanceHandle(), 
		prop.lpCreateParams);
	FreeObjectPointer();

	return hwnd != NULL;
}

/*////////////////////////////////////////////////////////////////////
Dialog
////////////////////////////////////////////////////////////////////*/

/*--------------------------------------------------------------------
Dialog::Create()
--------------------------------------------------------------------*/
BOOL Dialog::Create(HWND parent)
{
	PrepareObjectPointer();
	hwnd = CreateDialog(Globals::GetInstanceHandle(),
		MAKEINTRESOURCE(GetId()),
		parent,
		(DLGPROC)StaticWindowProc);
	FreeObjectPointer();

	return hwnd != NULL;
}

/*--------------------------------------------------------------------
Dialog::WindowProc()
--------------------------------------------------------------------*/
LPARAM CALLBACK Dialog::WindowProc(HWND hwnd, UINT msg, 
	WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	HANDLE_MSG(hwnd, WM_INITDIALOG, OnInitDialog);
	}

	return WindowBase::WindowProc(hwnd, msg, wParam, lParam);
}

/*////////////////////////////////////////////////////////////////////
Panel
////////////////////////////////////////////////////////////////////*/

/*--------------------------------------------------------------------
Panel::Create()
--------------------------------------------------------------------*/
BOOL Panel::Create(HWND parent)
{
	if (Dialog::Create(parent))
	{
		RECT rect;
		VERIFY(GetWindowRect(hwnd, &rect));

		w = rect.right - rect.left;
		h = rect.bottom - rect.top;
	}

	return hwnd != NULL;
}

/*////////////////////////////////////////////////////////////////////
ModalDialog
////////////////////////////////////////////////////////////////////*/

/*--------------------------------------------------------------------
ModalDialog::Create()
--------------------------------------------------------------------*/
BOOL ModalDialog::Create(HWND parent)
{
	PrepareObjectPointer();
	BOOL ok = DialogBox(Globals::GetInstanceHandle(),
		MAKEINTRESOURCE(GetId()),
		parent,
		(DLGPROC)StaticWindowProc) == IDOK;
	FreeObjectPointer();

	return ok;
}

/*--------------------------------------------------------------------
ModalDialog::OnCommand()
--------------------------------------------------------------------*/
void ModalDialog::OnCommand(HWND hwnd, int id, HWND hwnd_ctl,
		UINT code_notify)
{
	if (id == IDOK || id == IDCANCEL)
		EndDialog(hwnd, id);
}

/*////////////////////////////////////////////////////////////////////
Tab
////////////////////////////////////////////////////////////////////*/

/*--------------------------------------------------------------------
Tab::Tab()
--------------------------------------------------------------------*/
Tab::Tab()
{
	wndclass.lpszClassName = "TAB_CLASS";
	prop.style = WS_CHILD;

	hwnd_tab = NULL;
	pcurrent_pan = NULL;
	w = h = cx_max = cy_max = 0;
}

/*--------------------------------------------------------------------
Tab::Create()
--------------------------------------------------------------------*/
BOOL Tab::Create(HWND parent)
{
	if (!Window::Create(parent))
		return FALSE;
	
    hwnd_tab = CreateWindow(WC_TABCONTROL, "",
        WS_CHILD | WS_CLIPSIBLINGS | WS_VISIBLE, 
        0, 0, 0, 0,
        hwnd, NULL, Globals::GetInstanceHandle(), NULL); 
	
	if (hwnd_tab != NULL)
	{
		/* ��������� ������. */
		SendMessage(hwnd_tab, WM_SETFONT,
			(WPARAM)GetStockObject(ANSI_VAR_FONT), (LPARAM)0);
	}
	else
	{
		DestroyWindow(hwnd);
		return FALSE;
	}

	return TRUE;
}

/*--------------------------------------------------------------------
Tab::OnSize()
--------------------------------------------------------------------*/
void Tab::OnSize(HWND hwnd, UINT state, int cx, int cy)
{
	/* ����������� TabControl �� ��� ���������� �������. */
	MoveWindow(hwnd_tab, 0, 0, cx, cy, TRUE);

	/* �������� ������� ����������� �������. */
	RECT rect;
	ZeroMemory(&rect, sizeof(RECT));
	rect.right = cx;
	rect.bottom = cy;
	TabCtrl_AdjustRect(hwnd_tab, FALSE, &rect);

	/* �������� ���������� �������. */
	for (int i = 0; i < TabCtrl_GetItemCount(hwnd_tab); i++)
	{
	    TCITEM tci;
		ZeroMemory(&tci, sizeof(TCITEM));
		tci.mask = TCIF_PARAM; 
		VERIFY(TabCtrl_GetItem(hwnd_tab, i, &tci));

		Panel* ppan = (Panel*)tci.lParam;
		ASSERT(ppan != NULL);

		MoveWindow(ppan->GetHwnd(), rect.left, rect.top,
			rect.right - rect.left,
			rect.bottom - rect.top, TRUE);
	}
}

/*--------------------------------------------------------------------
Tab::AddPanel()
--------------------------------------------------------------------*/
BOOL Tab::AddPanel(Panel* ppan, UINT idr_caption /* = 0 */)
{
	ASSERT(hwnd != NULL && ppan != NULL);
	ASSERT(ppan->GetHwnd() == NULL);

	/* ������� ������ ��� �������� ����. */
	if (!ppan->Create(hwnd)) return FALSE;

    TCITEM tci;
	char caption[256];
	ZeroMemory(&tci, sizeof(TCITEM));
	tci.mask = TCIF_TEXT | TCIF_IMAGE | TCIF_PARAM; 
    tci.iImage = -1; 
	tci.lParam = (LPARAM)ppan;
    tci.pszText = caption;
	if (idr_caption != 0)
	{
		VERIFY(LoadString(Globals::GetInstanceHandle(),
			idr_caption, caption, sizeof(caption)));
	}
	else
	{
		sprintf(caption, "Untitled#%d",
			TabCtrl_GetItemCount(hwnd_tab));
	}

	/* ��������� TAB. */
	if (TabCtrl_InsertItem(hwnd_tab,
		TabCtrl_GetItemCount(hwnd_tab), &tci) == -1)
		return FALSE;

	if (ppan->GetWidth() > cx_max) cx_max = ppan->GetWidth();
	if (ppan->GetHeight() > cy_max) cy_max = ppan->GetHeight();

	/* ��������� ������ � ������ TAB'�, ��������� ���
	����������� ����������� ���� ������� (� ������ ��
	������������ ������ � ������). */
	RECT rect;
	ZeroMemory(&rect, sizeof(rect));
	rect.right = cx_max;
	rect.bottom = cy_max;
	TabCtrl_AdjustRect(hwnd_tab, TRUE, &rect);
	w = rect.right - rect.left;
	h = rect.bottom - rect.top;

	/* ���� ��� ������ ���������� ������, ���������� �� �����. */
	if (TabCtrl_GetItemCount(hwnd_tab) == 1)
		OnSelChanged();

	return TRUE;
}

/*--------------------------------------------------------------------
Tab::OnSelChanged()
--------------------------------------------------------------------*/
void Tab::OnSelChanged() 
{ 
    if (TabCtrl_GetItemCount(hwnd_tab) == 0) return;

	int sel = TabCtrl_GetCurSel(hwnd_tab); 
 
	if (pcurrent_pan != NULL)
		ShowWindow(pcurrent_pan->GetHwnd(), SW_HIDE);
 
    TCITEM tci;
	ZeroMemory(&tci, sizeof(TCITEM));
	tci.mask = TCIF_PARAM; 
	VERIFY(TabCtrl_GetItem(hwnd_tab, sel, &tci));

	pcurrent_pan = (Panel*)tci.lParam;
	ASSERT(pcurrent_pan != NULL);
	ShowWindow(pcurrent_pan->GetHwnd(), SW_SHOWNORMAL);
}

/*--------------------------------------------------------------------
Tab::OnNotify()
--------------------------------------------------------------------*/
LRESULT Tab::OnNotify(HWND hwnd, int id, NMHDR* pnmh)
{
	if (pnmh->code == TCN_SELCHANGE) OnSelChanged();
	return 0;
}
