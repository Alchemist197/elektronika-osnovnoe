#include "precomp.h"
#include "global.h"
#include "wave.h"
#include "hrtimer.h"

/*const UINT WAVE_SAMPLE_RATE = 44100;
const UINT WAVE_BPS = 16;

//--------------------------------------------------------------------
//Wave::Wave()
//--------------------------------------------------------------------
Wave::Wave() 
{
	hwavein = NULL;
	wavein_started = FALSE;
}
*/
/*--------------------------------------------------------------------
Wave::Init()
--------------------------------------------------------------------*/
/*BOOL Wave::Init(HARDWARE_CALLBACK callback, void* pparam)
{
	BOOL ok = HardwareTh::Init(callback, pparam);

	// �������������� ������ �������. 
	hwavein = NULL;
	wavein_started = FALSE;

	caps.bits_per_sample = WAVE_BPS;
	caps.ranges.push_back(Range(-0.1, 0.1));
	caps.ranges.push_back(Range(-0.5, 0.5));
	caps.ranges.push_back(Range(-1, 1));

	caps.max_capture_level = (1 << caps.bits_per_sample) - 1;

	char res_name[256];
	sprintf(res_name, "%.1f��� %d���", 
		WAVE_SAMPLE_RATE/1000.0, 
		caps.bits_per_sample);
	caps.resolutions.push_back(
		Resolution(0, res_name, 1.0/WAVE_SAMPLE_RATE, 0, 0));
	Resolution& res = caps.resolutions[caps.resolutions.size() - 1];
	
	//��������� ������� �������, ������ �� 0.1�. 
	UINT size = 256;
	while ((REAL)size/WAVE_SAMPLE_RATE <= 0.1)
	{
		res.AddBufferSize(size);
		size <<= 1;
	}

	// �������������� ����������.
	WAVEFORMATEX wf;
	ZeroMemory(&wf, sizeof(WAVEFORMATEX));
	wf.wFormatTag = WAVE_FORMAT_PCM;
	wf.nChannels = 1;
	wf.nSamplesPerSec = WAVE_SAMPLE_RATE;
	wf.wBitsPerSample = caps.bits_per_sample;
	wf.nBlockAlign = wf.wBitsPerSample*wf.nChannels/8;
	wf.nAvgBytesPerSec = wf.nSamplesPerSec*wf.nBlockAlign;

	if (ok)
		ok = waveInOpen(&hwavein,
			WAVE_MAPPER,
			&wf,
			(DWORD)event_data_ready.GetHandle(),
			0,
			CALLBACK_EVENT) == MMSYSERR_NOERROR;

	if (ok)
		ok = CreateIOThread();

	if (!ok)
		Shutdown();

	return ok; 
}
*/
/*--------------------------------------------------------------------
Wave::Shutdown()
--------------------------------------------------------------------*/
/*BOOL Wave::Shutdown()
{
	BOOL ok = TerminateIOThread();

	if (hwavein != NULL)
	{
		if (wavein_started)
		{
			if (waveInStop(hwavein) != MMSYSERR_NOERROR)
				ok = FALSE;
			wavein_started = FALSE;
		}

		// ��������� ����������.
		if (waveInClose(hwavein) != MMSYSERR_NOERROR)
			ok = FALSE;
		hwavein = NULL;
	}

	if (!HardwareTh::Shutdown())
		ok = FALSE;

	return ok;
}
*/
/*--------------------------------------------------------------------
Wave::IO_Thread_OnDataRequest()
--------------------------------------------------------------------*/
/*void Wave::IO_Thread_OnDataRequest()
{
	HARDWARE_SETTINGS hws;
	GetHWS(hws);
	Resolution& res = caps.resolutions[hws.resolution_index];
	UINT buffer_size = res.GetBufferSize(hws.buffer_size_index);

	UINT nsamples = buffer_size;
	if (hws.flags & HWS_CAPTURE_ENABLED)
		nsamples += buffer_size  + 
			hws.capture_delay;

	const UINT WAVE_BUFFER_SIZE = 32768;
	const UINT STARTUP_SAMPLES = 256;
	USHORT buffer[STARTUP_SAMPLES + WAVE_BUFFER_SIZE];
	ZeroMemory(buffer, sizeof(buffer));

	UINT_VECTOR v;
	BOOL ok = GetWave(buffer, STARTUP_SAMPLES + nsamples);
	if (ok)
		ProcessData(buffer + STARTUP_SAMPLES, nsamples, 
		v, buffer_size, hws);

	Notify(HWN_SAMPLE | (ok ? HWN_SUCCESS : 0), ok ? &v : NULL);
}
*/
/*--------------------------------------------------------------------
Wave::IO_Thread_OnUpdateSettingsRequest()
--------------------------------------------------------------------*/
/*BOOL Wave::IO_Thread_OnUpdateSettingsRequest(
	const HARDWARE_SETTINGS& hws)
{
	Notify(HWN_SETTINGS | HWN_SUCCESS, NULL);
	return TRUE;
}*/

/*--------------------------------------------------------------------
Wave::ProcessData()
--------------------------------------------------------------------*/
/*void Wave::ProcessData(USHORT* buffer, UINT nsamples, 
					   UINT_VECTOR& v, UINT buffer_size, 
					   const HARDWARE_SETTINGS& hws)
{
	UINT start_countdown = buffer_size;
	UINT delay_countdown = hws.capture_delay;

	// �������������� signed -> unsigned.
	for (UINT index = 0; index < nsamples; index++)
	{
		// �������������� � ����� ��� �����.
		buffer[index] = (USHORT)(
			(1L << 15) + (short)buffer[index]);
	}

	// ������ � ������� buffer. 
	UINT i = 0;

#define LOOP0 { --start_countdown; ++i; }

	if (hws.flags & HWS_CAPTURE_ENABLED)
	{
		if (hws.flags & HWS_SLOPE_POSEDGE)
		{
			// ����� �� ����������. 
			while (i < nsamples && 
				buffer[i] >= hws.capture_level &&
				start_countdown != 0) LOOP0;
				
			while (i < nsamples && 
				buffer[i] < hws.capture_level &&
				start_countdown != 0) LOOP0;
		}
		else
		{
			// ����� �� �����. 
			while (i < nsamples && 
				buffer[i] <= hws.capture_level &&
				start_countdown != 0) LOOP0;

			while (i < nsamples &&
				buffer[i] > hws.capture_level &&
				start_countdown != 0) LOOP0;
		}

		// �������� �������. 
		while (i < nsamples &&
			delay_countdown != 0)
		{
			--delay_countdown;
			++i;
		}
	}

	v.clear();
	while (i < nsamples && v.size() < buffer_size)
		v.push_back(buffer[i++]);

	ASSERT(v.size() == buffer_size);
}
*/
/*--------------------------------------------------------------------
Wave::GetWave()
--------------------------------------------------------------------*/
/*BOOL Wave::GetWave(USHORT* buffer, UINT nsamples)
{
	event_data_ready.Reset();

	WAVEHDR whdr;
	ZeroMemory(&whdr, sizeof(WAVEHDR));
	whdr.lpData = (LPSTR)buffer;
	whdr.dwBufferLength = nsamples << 1;

	BOOL ok;
	
	if (ok = waveInPrepareHeader(hwavein, 
		&whdr, sizeof(WAVEHDR)) == MMSYSERR_NOERROR)
		ok = waveInAddBuffer(hwavein, 
			&whdr, sizeof(WAVEHDR)) == MMSYSERR_NOERROR;

	if (!wavein_started && ok)
	{
		ok = wavein_started = 
			waveInStart(hwavein) == MMSYSERR_NOERROR;
	}

	if (ok)
	{
		event_data_ready.Wait();
		ok = waveInUnprepareHeader(hwavein, &whdr,
			sizeof(WAVEHDR)) == MMSYSERR_NOERROR;
	}

	return ok;
}*/
