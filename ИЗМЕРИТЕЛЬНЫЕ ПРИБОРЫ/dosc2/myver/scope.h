#ifndef __SCOPE_H
#define __SCOPE_H

#include "hwthread.h"
#include "comm.h"

/*--------------------------------------------------------------------
Scope
--------------------------------------------------------------------*/
class Scope: public HardwareTh
{
public:

	virtual BOOL Init(HARDWARE_CALLBACK callback, void* pparam);
	virtual BOOL Shutdown();

	virtual char* GetName(char* name) {
		strcpy(name, "DSO"); 
		return name; }

protected:

	virtual void IO_Thread_OnDataRequest();
	virtual BOOL IO_Thread_OnUpdateSettingsRequest(
		const HARDWARE_SETTINGS& hws);

	void AddBufferSizes(Resolution& res, UINT max_size);

	Rs232 comm;
};

#endif /* #ifndef __SCOPE_H */
