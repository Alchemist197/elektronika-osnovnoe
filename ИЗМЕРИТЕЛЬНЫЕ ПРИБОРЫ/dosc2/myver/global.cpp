#include "precomp.h"
#include "global.h"
#include "mainwnd.h"

HINSTANCE Globals::hinstance = NULL;
void* Globals::pmainwnd = NULL;

/* �������� ����� ��� ������ ������������/�����������. */
Globals _globals;

#ifdef _DEBUG
FILE* Globals::ferrlog = NULL;
#endif /* #ifdef _DEBUG */

/*--------------------------------------------------------------------
Globals::Globals()
--------------------------------------------------------------------*/
Globals::Globals()
{
#ifdef _DEBUG
	ferrlog = fopen("errlog.txt", "wt");
#endif /* #ifdef _DEBUG */
}

/*--------------------------------------------------------------------
Globals::Globals()
--------------------------------------------------------------------*/
Globals::~Globals()
{
#ifdef _DEBUG
	if (ferrlog != NULL) fclose(ferrlog);
#endif /* #ifdef _DEBUG */
}

/*--------------------------------------------------------------------
Globals::GetFileExt()
--------------------------------------------------------------------*/
void Globals::GetFileExt(char* buffer, const char* file_name)
{
	const char* p = file_name + strlen(file_name);
	while (p >= file_name && *p != '.' &&
		*p != '\\' && *p != '/') --p;
	if (p < file_name || *p != '.')
		*buffer = '\0';
	else
		strcpy(buffer, p + 1);
}

/*--------------------------------------------------------------------
Globals::ErrLog()
--------------------------------------------------------------------*/
#ifdef _DEBUG
void Globals::ErrLog(const char* error)
{
	if (ferrlog != NULL)
		fprintf(ferrlog, "%s\n", error);
}
#endif /* #ifdef _DEBUG */

/*--------------------------------------------------------------------
Globals::Round()
--------------------------------------------------------------------*/
int Globals::Round(REAL x) 
{
	return x >= 0 ? (int)(x + 0.5) : (int)(x - 0.5); 
}

/*--------------------------------------------------------------------
Globals::GetModulePath()
--------------------------------------------------------------------*/
void Globals::GetModulePath(char* buffer, UINT buffer_size)
{
	char* p;

	GetModuleFileName(NULL, buffer, buffer_size);
	p = buffer + strlen(buffer) - 1;

	/* �������� ��� �����. */
	while (p >= buffer && *p != '\\' && *p != '/') p--;
	*(p + 1) = '\0';
}

/*--------------------------------------------------------------------
Globals::Log()
--------------------------------------------------------------------*/
void Globals::Log(const char* msg)
{
	((MainWnd*)pmainwnd)->Log(msg);
}

/*--------------------------------------------------------------------
Globals::Log()
--------------------------------------------------------------------*/
void Globals::Log(UINT string_id, const char* param)
{
	char msg[256];
	VERIFY(LoadString(hinstance, string_id, msg, sizeof(msg)));

	if (param != NULL)
	{
		char buffer[256];
		sprintf(buffer, msg, param);
		Log(buffer);
	}
	else
		Log(msg);
}

/*--------------------------------------------------------------------
Globals::IsUint()
--------------------------------------------------------------------*/
BOOL Globals::IsUint(const char* p)
{
	UINT n = 0;
	while (*p >= '0' && *p <= '9') { p++; n++; }
	return n != 0 && *p == '\0';
}

/*--------------------------------------------------------------------
Globals::IsFloat()
--------------------------------------------------------------------*/
BOOL Globals::IsFloat(const char* p)
{
	/* ����. */
	if (*p == '-' || *p == '+') p++;	

	/* ����� �����. */
	UINT n = 0;
	while (*p >= '0' && *p <= '9') { p++; n++; }

	if (*p == '.')
	{
		/* ������� �����. */
		p++;
		n = 0;
		while (*p >= '0' && *p <= '9') { p++; n++; }

		if (n == 0)
			/* �� ����� ����� � ����� � ������� �����. */
			return FALSE;
	}
	else
	{
		if (n == 0)
			/* �� ����� ����� � ����� � ������� �����. */
			return FALSE;
	}

	if (*p == 'e' || *p == 'E')
	{
		/* ����. */
		p++;
		if (*p == '-' || *p == '+') p++;

		n = 0;
		while (*p >= '0' && *p <= '9') { p++; n++; }
		if (n == 0)
			/* �� ����� �����. */
			return FALSE;
	}

	return *p == '\0';
}
