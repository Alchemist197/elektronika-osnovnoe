#ifndef __DATA_H
#define __DATA_H

/*--------------------------------------------------------------------
Data
--------------------------------------------------------------------*/
class Data
{
public:

#pragma pack(push)
#pragma pack(1)
	struct SETUP
	{
		UINT bits_per_sample;
		REAL min_voltage;
		REAL max_voltage;
		REAL time_per_sample;
	};
#pragma pack(pop)

	Data() {
		Clear(); }

	void Setup(const UINT_VECTOR& v, const SETUP& ds) {
		Data::v = v;
		Data::ds = ds;
	
		a = ds.min_voltage;
		b = (ds.max_voltage - ds.min_voltage)/
			((1 << ds.bits_per_sample) - 1);
	}

	void GetSetupData(SETUP& ds) {
		ds = Data::ds; }

	void Clear() {
		a = b = 0;
		ZeroMemory(&ds, sizeof(SETUP));
		v.clear(); }

	REAL SampleVoltage(UINT nsample) const {
		ASSERT(nsample < v.size());
		return a + b*v[nsample]; }

	REAL SampleTime(UINT nsample) const {
		ASSERT(nsample < v.size());
		return nsample*TimeLength()/(v.size() - 1); }

	/* ����� ������������� � ��������. */
	REAL TimeLength() const {
		return v.size()*ds.time_per_sample; }

	UINT Size() const {
		return v.size(); }

	void GetV(UINT_VECTOR& v) {
		v = Data::v; }

	/* ���������� ������� ������������� ���������. */
	REAL GetFFTMaxFreq() const;

	/* ���������� � �����. */
	BOOL Save(FILE* f) const;
	/* �������� �� �����. */
	BOOL Load(FILE* f);

protected:

#pragma pack(push)
#pragma pack(1)
	struct FILE_HEADER
	{
		ULONG signature;
		SETUP ds;
		UINT size;
		UCHAR reserved[16];
	};
#pragma pack(pop)

	UINT_VECTOR v;
	SETUP ds;

	REAL a, b;
};

#endif /* #ifndef __DATA_H */