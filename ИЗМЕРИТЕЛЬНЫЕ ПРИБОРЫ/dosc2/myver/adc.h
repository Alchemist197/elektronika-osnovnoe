#ifndef __ADC_H
#define __ADC_H

#define SCOPE_BUFFER_SIZE 400
#define SCOPE_BUFFER_SIZE_PLUS_CRC (SCOPE_BUFFER_SIZE + 4)

/*------------------------------------------------------------------*/
#ifdef __AVR_ASM
#define ADCPIN 0x13
#endif /* #ifdef __AVR_ASM */
/*------------------------------------------------------------------*/

/*------------------------------------------------------------------*/
#ifdef __AVR_C

#define ADCPIN PINC

typedef void (*ADC_PROC)(UCHAR);

extern void Do_Delay(UCHAR delay);
extern void Empty_Proc(void);

extern void Do_ADC0(UCHAR reserved);
extern void Do_ADC1(UCHAR reserved);
extern void Do_ADC2(UCHAR reserved);
extern void Do_ADC3(UCHAR delay);
extern void Do_ADC4(UCHAR delay);

extern UCHAR _buf[SCOPE_BUFFER_SIZE_PLUS_CRC];

#endif /* #ifdef __AVR_C */
/*------------------------------------------------------------------*/

#endif /* #ifndef __ADC_H */
