#include "precomp.h"
#include "global.h"
#include "scope.h"
#include "scopeini.h"
#include "common.h"
#include "resource.h"

/*--------------------------------------------------------------------
Scope::Init()
--------------------------------------------------------------------*/
BOOL Scope::Init(HARDWARE_CALLBACK callback, void* pparam)
{
	BOOL ok;

	ScopeIniFile::INI_DATA ini_data;
	if (!ScopeIniFile::Read(ini_data))
		ScopeIniFile::GetDefault(ini_data);

	if (ok = HardwareTh::Init(callback, pparam))
	{
		ok = comm.Init(ini_data.port, ini_data.baud_rate);
		if (!ok)
		{
			char port[256];
			sprintf(port, "%u @ %u ���", ini_data.port,
				ini_data.baud_rate);
			Globals::Log(IDS_COM_PORT_INIT_FAILED, port);
		}
	}

	/*---*/

	caps.bits_per_sample = ADC_BITS_PER_SAMPLE;
	caps.max_capture_delay = CAPTURE_DELAY_MAX;
	caps.max_capture_level = CAPTURE_LEVEL_MAX;
	/*caps.flags |= HWC_TIMEOUT_CONTROL;*/

	/*--- ������� ���������. ----*/

	char buffer[256];
	sprintf(buffer, "FREQ = %u, COM%u @ %u BOD, BUFFER = %u",
		ini_data.freq,
		ini_data.port,
		ini_data.baud_rate,
		ini_data.buffer_size);
	Globals::Log(buffer);

	/*---*/

	caps.ranges = ini_data.ranges;

	caps.resolutions.push_back(
		Resolution(0, "40MPSP", ini_data.freq, 
			/*ADC_MODE4_BASE_DIV*/ 10,
			/*ADC_MODE4_STEP_DIV*/ 10, 
			/*ADC_MODE4_DELAY_MAX*/ 12));
	AddBufferSizes(caps.resolutions.back(),
		ini_data.buffer_size);
/*
	caps.resolutions.push_back(
		Resolution(ADC_MODE3, "������", ini_data.freq, 
			ADC_MODE3_BASE_DIV,
			ADC_MODE3_STEP_DIV, 
			ADC_MODE3_DELAY_MAX));
	AddBufferSizes(caps.resolutions.back(),
		ini_data.buffer_size);

	caps.resolutions.push_back(
		Resolution(ADC_MODE2, "�������", ini_data.freq, 
			ADC_MODE2_BASE_DIV, 0, 0));
	AddBufferSizes(caps.resolutions.back(),
		ini_data.buffer_size);

	caps.resolutions.push_back(
		Resolution(ADC_MODE1, "�������", ini_data.freq, 
			ADC_MODE1_BASE_DIV, 0, 0));
	AddBufferSizes(caps.resolutions.back(),
		ini_data.buffer_size);

	caps.resolutions.push_back(
		Resolution(ADC_MODE0, "������������", ini_data.freq, 
			ADC_MODE0_BASE_DIV, 0, 0));
	AddBufferSizes(caps.resolutions.back(),
		ADC_MODE0_BUFSZ_MAX);*/

	if (ok)
		ok = CreateIOThread(THREAD_PRIORITY_TIME_CRITICAL);

	if (!ok)
		Shutdown();

	return ok;
}

/*--------------------------------------------------------------------
Scope::AddBufferSizes()
--------------------------------------------------------------------*/
void Scope::AddBufferSizes(Resolution& res, UINT max_size)
{
	while (max_size >= 32)
	{
		res.AddBufferSize(max_size);
		max_size >>= 1;
	}
}

/*--------------------------------------------------------------------
Scope::Shutdown()
--------------------------------------------------------------------*/
BOOL Scope::Shutdown()
{
	BOOL ok = TerminateIOThread();

	if (comm.GetHandle() != NULL)
		if (!comm.Shutdown()) ok = FALSE;

	if (!HardwareTh::Shutdown())
		ok = FALSE;

	return ok;
}

/*--------------------------------------------------------------------
Scope::IO_Thread_OnDataRequest()
--------------------------------------------------------------------*/
void Scope::IO_Thread_OnDataRequest()
{
	HARDWARE_SETTINGS hws;
	GetHWS(hws);
	UINT buffer_size = caps.resolutions[hws.resolution_index].
		GetBufferSize(hws.buffer_size_index);
	ASSERT(buffer_size <= SCOPE_BUFFER_SIZE);

	unsigned char frame_buffer[4096];
	SCOPE_COMMAND cmd;
	cmd.command = OSC_CMD_GET; /* ������� - ��������� �������. */
	
	BOOL ok;
	if (ok = comm.SendFrame((UCHAR*)&cmd, SIZEOF_SCOPE_COMMAND))
		/* �������� �������. */
		ok = comm.GetFrame(frame_buffer, buffer_size + sizeof(CRC));

	UINT_VECTOR v;
	if (ok)
	{
		Range& range = caps.ranges[hws.range_index];
		for (UINT i = 0; i < buffer_size; i++)
			v.push_back(frame_buffer[i]);
	}

	Notify(HWN_SAMPLE | (ok ? HWN_SUCCESS : 0), &v);
}

/*--------------------------------------------------------------------
Scope::IO_Thread_OnUpdateSettingsRequest()
--------------------------------------------------------------------*/
BOOL Scope::IO_Thread_OnUpdateSettingsRequest(
	const HARDWARE_SETTINGS& hws)
{
	SCOPE_SETTINGS s;
	ZeroMemory(&s, sizeof(SCOPE_SETTINGS));

	s.capture.level = hws.capture_level;
	if (hws.flags & HWS_CAPTURE_ENABLED)
		s.capture.flags |= CAPTURE_ENABLED;
	if (hws.flags & HWS_SLOPE_POSEDGE)
		s.capture.flags |= CAPTURE_POSEDGE;
	if (hws.flags & HWS_CAPTURE_TYPE)
		s.capture.flags |= HWS_CAPTURE_TYPE;	//��� ��������
	if (hws.flags & HWS_INPUT_ACDC)
		s.capture.flags |= HWS_INPUT_ACDC;	//AC/DC

	s.capture.delay = hws.capture_delay;

	s.adc.mode = caps.resolutions[hws.resolution_index].GetId();
//	if (s.adc.mode == ADC_MODE3 || s.adc.mode == ADC_MODE4)
		s.adc.delay = hws.resolution_scale;
	s.adc.buffer_size = caps.resolutions[hws.resolution_index].
		GetBufferSize(hws.buffer_size_index);

	s.mask = ((UCHAR)caps.ranges[hws.range_index].GetMask() & 0x7); //������ 3 ������ ����

	/* IO. */
	SCOPE_COMMAND cmd;
	cmd.command = OSC_CMD_SETTINGS;
	
	BOOL ok;
	if (ok = comm.SendFrame((UCHAR*)&cmd, SIZEOF_SCOPE_COMMAND))
		ok = comm.SendFrame((UCHAR*)&s, SIZEOF_SCOPE_SETTINGS);

	Notify(HWN_SETTINGS | (ok ? HWN_SUCCESS : 0), NULL);
	return ok;
}
