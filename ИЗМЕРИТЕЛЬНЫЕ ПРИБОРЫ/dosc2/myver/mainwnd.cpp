#include "precomp.h"
#include "global.h"
#include "mainwnd.h"
#include "about.h"
#include "view.h"
#include "comm.h"
#include "settings.h"
//#include "emulator.h"
#include "scope.h"
#include "wave.h"
#include "expbmp.h"
#include "units.h"
#include "resource.h"
#ifdef USE_CXIMAGE
#   include "ximage.h"	/* CXIMAGE library. */
#endif

/*--------------------------------------------------------------------
MainWnd::MainWnd()
--------------------------------------------------------------------*/
MainWnd::MainWnd() 
{
	wndclass.lpszClassName = "OSC_MAINWND_CLASS";
	prop.style = WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN;

	VERIFY(LoadString(Globals::GetInstanceHandle(),
		IDS_CAPTION, caption, sizeof(caption)));
	prop.lpszName = caption;

	flags = 0;
}

/*--------------------------------------------------------------------
MainWnd::MainWnd()
--------------------------------------------------------------------*/
MainWnd::~MainWnd()
{
}

/*--------------------------------------------------------------------
MainWnd::CmdGet()
--------------------------------------------------------------------*/
void MainWnd::CmdGet()
{
	flags |= F_DATA_REQUEST;
}

/*--------------------------------------------------------------------
MainWnd::CmdStart()
--------------------------------------------------------------------*/
void MainWnd::CmdStart()
{
	if (!(flags & F_STARTED))
	{
		EnableMenuItem(GetMenu(hwnd), IDM_START, 
			MF_BYCOMMAND | MF_GRAYED);
		EnableMenuItem(GetMenu(hwnd), IDM_STOP, 
			MF_BYCOMMAND | MF_ENABLED);
		DrawMenuBar(hwnd);

		flags |= F_STARTED;

		View::SETUP vs;
		view.GetVS(vs);
		vs.flags |= VF_MEMORY_ENABLED;
		vs.flags &= ~VF_AUTOUPDATE_ENABLED;
		view.SetVS(vs);
	}
}

/*--------------------------------------------------------------------
MainWnd::CmdStop()
--------------------------------------------------------------------*/
void MainWnd::CmdStop()
{
	if (flags & F_STARTED)
	{
		EnableMenuItem(GetMenu(hwnd), IDM_START, 
			MF_BYCOMMAND | MF_ENABLED);
		EnableMenuItem(GetMenu(hwnd), IDM_STOP, 
			MF_BYCOMMAND | MF_GRAYED);
		DrawMenuBar(hwnd);
		
		flags &= ~F_STARTED;

		View::SETUP vs;
		view.GetVS(vs);
		vs.flags &= ~VF_MEMORY_ENABLED;
		vs.flags |= VF_AUTOUPDATE_ENABLED;
		view.SetVS(vs);
	}
}

/*--------------------------------------------------------------------
MainWnd::CmdAbout()
--------------------------------------------------------------------*/
void MainWnd::CmdAbout()
{
	About about;
	about.Create(hwnd);
}

/*--------------------------------------------------------------------
MainWnd::CmdCursors()
--------------------------------------------------------------------*/
void MainWnd::CmdCursors()
{
	view.ResetCursors();
	view.UpdateClient();
}

/*--------------------------------------------------------------------
MainWnd::CmdExportBmp()
--------------------------------------------------------------------*/
void MainWnd::CmdExportBmp()
{
	HDC windowdc = NULL, memdc = NULL;
	HBITMAP hbmp = NULL, old_bmp = NULL;
	void* pbits = NULL;
	BITMAPINFO* pbi = NULL;
	BITMAPFILEHEADER bfh;
	int bi_size;

	/* ����� BMP � ������. */
	UCHAR* pbmp_file_data = NULL;
	DWORD bmp_file_size;

#ifdef USE_CXIMAGE
    CxImage img;
#endif
	char ext[MAX_PATH];
	BOOL save_ok = FALSE;
	FILE* f = NULL;

	/* ���������� ����. */
	ExpBmpDialog dlg;
	EXPORT_BMP expbmp;
	if (!dlg.Create(hwnd)) return;
	dlg.GetData(expbmp);

	/* ������ "�������� ����". */
	SetCapture(hwnd);
	SetCursor(LoadCursor(NULL, IDC_WAIT));

	if ((windowdc = GetDC(NULL)) == NULL ||
		(memdc = CreateCompatibleDC(NULL)) == NULL ||	
		(hbmp = CreateCompatibleBitmap(windowdc,
			expbmp.w, expbmp.h)) == NULL)
	{
		Globals::Log(IDS_OUT_OF_MEMORY);
		THROW;
	}
	/* ������ �� ��������� memdc. */
	old_bmp = (HBITMAP)SelectObject(memdc, hbmp);
	View::SETUP vs, old_vs;
	view.GetVS(vs);
	old_vs = vs;
	/* ��������� ���������� ���������. */
	vs.flags &= ~VF_MINY_ENABLED & ~VF_MAXY_ENABLED;
	view.SetVS(vs, FALSE /* update */);
	view.Draw(memdc, expbmp.w, expbmp.h);
	view.SetVS(old_vs, FALSE /* update */);
	SelectObject(memdc, old_bmp);

	/* ����� ��� BITMAPINFO � �������. */
	bi_size = sizeof(BITMAPINFO) + (sizeof(RGBQUAD) << 8);
	if ((pbi = (BITMAPINFO*)malloc(bi_size)) == NULL)
	{
		Globals::Log(IDS_OUT_OF_MEMORY);
		THROW;
	}
	ZeroMemory(pbi, bi_size);
	pbi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	pbi->bmiHeader.biWidth = expbmp.w;
	pbi->bmiHeader.biHeight = expbmp.h;
	pbi->bmiHeader.biPlanes = 1;
	pbi->bmiHeader.biBitCount = 8; /* 256 ������. */
	pbi->bmiHeader.biCompression = expbmp.rle ? BI_RLE8 : BI_RGB;
	pbi->bmiHeader.biSizeImage = pbi->bmiHeader.biHeight*
		Globals::Align(pbi->bmiHeader.biWidth*
		pbi->bmiHeader.biBitCount >> 3, sizeof(DWORD));
	/*---*/
	pbits = malloc(pbi->bmiHeader.biSizeImage);
	if (pbits == NULL)
	{
		Globals::Log(IDS_OUT_OF_MEMORY);
		THROW;
	}
	expbmp.h = GetDIBits(memdc, hbmp, 0, expbmp.h, pbits, pbi,
		DIB_RGB_COLORS);
	ASSERT(expbmp.h != 0);

	/* ��������� ��������� BITMAPFILEHEADER. */
	ZeroMemory(&bfh, sizeof(BITMAPFILEHEADER));
	bfh.bfType = 0x4D42;
	bfh.bfOffBits = sizeof(BITMAPFILEHEADER) + bi_size;
	bfh.bfSize = bfh.bfOffBits + pbi->bmiHeader.biSizeImage;

	/* �������� ������ ��� ����� BMP � ������. */
	bmp_file_size = sizeof(BITMAPFILEHEADER) +
		bi_size +
		pbi->bmiHeader.biSizeImage;
	pbmp_file_data = (UCHAR*)malloc(bmp_file_size);
	if (pbmp_file_data == NULL)
	{
		Globals::Log(IDS_OUT_OF_MEMORY);
		THROW;
	}

	/* �������� ������. */
	memcpy(pbmp_file_data, &bfh, sizeof(BITMAPFILEHEADER));
	memcpy(pbmp_file_data + sizeof(BITMAPFILEHEADER),
		pbi, bi_size);
	memcpy(pbmp_file_data + sizeof(BITMAPFILEHEADER) + bi_size,
		pbits, pbi->bmiHeader.biSizeImage);

#ifdef USE_CXIMAGE
	/* ������� ������ CXImage. */
	if (!img.Decode(pbmp_file_data, bmp_file_size,
		CXIMAGE_FORMAT_BMP))
	{
		Globals::Log(IDS_OUT_OF_MEMORY);
		THROW;
	}
#endif

	/* �������� ���������� �����. */
	Globals::GetFileExt(ext, expbmp.file_name);
	CharUpper(ext);

	/* GIF? */
	if (strcmp(ext, "GIF") == 0)
	{
#ifdef USE_CXIMAGE
		img.SetCodecOption(2 /* LZW */);
		save_ok = img.Save(expbmp.file_name, CXIMAGE_FORMAT_GIF);
#else
        MessageBox(hwnd,
                   "������� � ������� GIF �������� �� ����� ����������",
                   "������",
                   MB_ICONEXCLAMATION | MB_OK);
#endif
	}
	/* PNG? */
    else if (strcmp(ext, "PNG") == 0)
    {
#ifdef USE_CXIMAGE
		save_ok = img.Save(expbmp.file_name, CXIMAGE_FORMAT_PNG);
#else
        MessageBox(hwnd,
                   "������� � ������� PNG �������� �� ����� ����������",
                   "������",
                   MB_ICONEXCLAMATION | MB_OK);
#endif
    }
	/* BMP? */
	else if (strcmp(ext, "BMP") == 0)
	{
		/* ��������� ��� CxImage. */
		if ((f = fopen(expbmp.file_name, "wb")) == NULL)
			THROW;
		save_ok = fwrite(pbmp_file_data, 1, bmp_file_size, f) ==
			bmp_file_size;
	}
	/* ���-�� �� ��... */
	else
	{
		ASSERT(0);
	}

	/* ��������� ��� �������? */
	if (!save_ok)
		Globals::Log(IDS_CREATE_FILE_FAILED, expbmp.file_name);

CATCH:	

	if (f != NULL) fclose(f);
	if (pbmp_file_data != NULL) free(pbmp_file_data);
	if (pbi != NULL) free(pbi);
	if (pbits != NULL) free(pbits);
	if (hbmp != NULL) DeleteObject(hbmp);
	if (memdc != NULL) DeleteDC(memdc);
	if (windowdc != NULL) ReleaseDC(hwnd, windowdc);

	ReleaseCapture();
}

/*--------------------------------------------------------------------
MainWnd::CmdFileSave()
--------------------------------------------------------------------*/
void MainWnd::CmdFileSave()
{
	char module_path[MAX_PATH];
	char file_name[MAX_PATH];
	FILE* f = NULL;
	GRAPH_VECTOR graphs;

	view.GetGraphs(graphs);
	if (graphs.size() == 0)
	{
		char msg[256];
		VERIFY(LoadString(Globals::GetInstanceHandle(),
			IDS_NOTHING_TO_SAVE, msg, sizeof(msg)));
		MessageBox(hwnd, msg, "", MB_OK | MB_ICONINFORMATION);
		return;
	}

	Data& data = graphs[0].data;

	Globals::GetModulePath(module_path, sizeof(module_path));
	*file_name = '\0';

	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hwnd;
	ofn.lpstrFile = file_name;
	ofn.lpstrDefExt = "osc";
	ofn.nMaxFile = sizeof(file_name);
	ofn.lpstrFilter = "(*.osc)\0*.osc\0\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = module_path;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_OVERWRITEPROMPT;

	if (GetSaveFileName(&ofn))
	{
		if ((f = fopen(file_name, "wb")) == NULL)
		{
			Globals::Log(IDS_CREATE_FILE_FAILED, file_name);
			THROW;
		}

		if (!data.Save(f))
		{
			Globals::Log(IDS_WRITE_ERROR, file_name);
			THROW;
		}
	}

CATCH:
	if (f != NULL) fclose(f);
}

/*--------------------------------------------------------------------
MainWnd::CmdFileLoadBackground()
--------------------------------------------------------------------*/
void MainWnd::CmdFileLoadBackground()
{
	char module_path[MAX_PATH];
	char file_name[MAX_PATH];
	FILE* f = NULL;

	Globals::GetModulePath(module_path, sizeof(module_path));
	*file_name = '\0';

	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hwnd;
	ofn.lpstrFile = file_name;
	ofn.lpstrDefExt = "osc";
	ofn.nMaxFile = sizeof(file_name);
	ofn.lpstrFilter = "(*.osc)\0*.osc\0\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = module_path;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

	if (GetOpenFileName(&ofn))
	{
		if ((f = fopen(file_name, "rb")) == NULL)
		{
			Globals::Log(IDS_OPEN_FILE_FAILED, file_name);
			THROW;
		}

		if (!background.Load(f))
		{
			Globals::Log(IDS_READ_ERROR, file_name);
			THROW;
		}

		EnableMenuItem(GetMenu(hwnd), IDM_FILE_CLOSE_BACKGROUND, 
			MF_BYCOMMAND | MF_ENABLED);

		Data::SETUP ds;
		background.GetSetupData(ds);

		char buffer[256];
		VERIFY(LoadString(Globals::GetInstanceHandle(),
			IDS_CAPTION_WITH_BACKGROUND, buffer, sizeof(buffer)));
		
		char ts[16];
		Units units;
		units.Setup(ds.time_per_sample, "s");
		units.Translate(ts, ds.time_per_sample);
		
		sprintf(caption, buffer, file_name, ts, background.Size());
		SetWindowText(hwnd, caption);

		flags |= F_DATA_REQUEST;
	}

CATCH:
	if (f != NULL) fclose(f);
}

/*--------------------------------------------------------------------
MainWnd::CmdFileCloseBackground()
--------------------------------------------------------------------*/
void MainWnd::CmdFileCloseBackground()
{
	background.Clear();

	EnableMenuItem(GetMenu(hwnd), IDM_FILE_CLOSE_BACKGROUND, 
		MF_BYCOMMAND | MF_GRAYED);

	VERIFY(LoadString(Globals::GetInstanceHandle(),
		IDS_CAPTION, caption, sizeof(caption)));
	SetWindowText(hwnd, caption);

	flags |= F_DATA_REQUEST;
}

/*--------------------------------------------------------------------
MainWnd::OnCommand()
--------------------------------------------------------------------*/
void MainWnd::OnCommand(HWND hwnd, int id, HWND hwnd_ctl,
					   UINT code_notify)
{
	if (code_notify == 0 /* ��������� �� ����. */)
    {
		switch (id)
        {
		case IDM_FILE_CLOSE_BACKGROUND: CmdFileCloseBackground(); break;
		case IDM_FILE_LOAD_BACKGROUND: CmdFileLoadBackground(); break;
		case IDM_FILE_SAVE: CmdFileSave(); break;
		case IDM_EXPORT_BMP: CmdExportBmp(); break;
		case IDM_ABOUT: CmdAbout(); break;
		case IDM_GET: CmdGet(); break;
		case IDM_START: CmdStart();	break;
		case IDM_CURSORS: CmdCursors(); break;
		case IDM_STOP: CmdStop(); break;
		case IDM_EXIT:
			PostMessage(hwnd, WM_CLOSE, 0, 0);
			break;
		}
	}
}

/*--------------------------------------------------------------------
MainWnd::OnDestroy()
--------------------------------------------------------------------*/
void MainWnd::OnDestroy(HWND hwnd)
{
	SETTINGS s;
	_settings.Get(s);

	Hardware& hw = *_hardware[s.selected_device_index];

	if (hw.Shutdown())
	{
		char name[256];
		Globals::Log(IDS_HARDWARE_SHUTDOWN_FAILED,
			hw.GetName(name));
	}

	PostQuitMessage(0);
}

/*--------------------------------------------------------------------
MainWnd::OnSize()
--------------------------------------------------------------------*/
void MainWnd::OnSize(HWND hwnd, UINT state, int cx, int cy)
{
	const int BORDER = 8;

	if (view.GetHwnd() != NULL)
		MoveWindow(view.GetHwnd(),
			BORDER, BORDER,
			cx - 2*BORDER - tab.GetWidth(), 
			cy - 2*BORDER - infopan.GetHeight(),
			FALSE);

	if (tab.GetHwnd() != NULL)
		MoveWindow(tab.GetHwnd(),
			cx - BORDER - tab.GetWidth(),
			BORDER,
			tab.GetWidth(),
			cy - 2*BORDER,
			TRUE);

	if (infopan.GetHwnd() != NULL)
		MoveWindow(infopan.GetHwnd(),
			BORDER,
			cy - BORDER - infopan.GetHeight(),
			cx - 2*BORDER - tab.GetWidth(),
			infopan.GetHeight(),
			FALSE);

	if (view.GetHwnd() != NULL) view.UpdateClient(FALSE);
	if (infopan.GetHwnd() != NULL) infopan.UpdateAll(TRUE);

	UpdateClient(TRUE);
}

/*--------------------------------------------------------------------
MainWnd::OnClose()
--------------------------------------------------------------------*/
void MainWnd::OnClose(HWND hwnd)
{
	DestroyWindow(hwnd);
}

/*--------------------------------------------------------------------
MainWnd::Hardware_Callback()
--------------------------------------------------------------------*/
void MainWnd::HandleHardwareNotify(const HARDWARE_NOTIFY& hwn)
{
	BOOL update_settings = FALSE;
	char name[256];
	hwn.phw->GetName(name);

	if (hwn.flags & HWN_SETTINGS)
	{
		if (!(hwn.flags & HWN_SUCCESS))
		{
			Globals::Log(IDS_HARDWARE_SETTINGS_FAILED, name);
			update_settings = TRUE;
		}
	}
	else if (hwn.flags & HWN_SAMPLE)
	{
		if (hwn.flags & HWN_SUCCESS)
		{
			/* ��������� ���������� ���� �����������. */
			GRAPH_VECTOR graphs;
			GRAPH graph;
			graph.color = RGB(0x00, 0xFF, 0x00);
			graph.data = hwn.data;
			graphs.push_back(graph);

			if (background.Size() != 0)
			{
				graph.color = RGB(0xA0, 0xA0, 0xFF);
				graph.data = background;
				graphs.push_back(graph);
			}
			
			view.AddGraphs(graphs);
			view.UpdateClient();
		}
		else
		{
			/* �� ��������� ������, ������ �������� ��
			���� ������������. */
			Globals::Log(IDS_DATA_TRANSFER_FAILED, name);
			update_settings = TRUE;
		}
	}

	if (update_settings)
	{
		Globals::Log(IDS_HARDWARE_SETTINGS_RETRY, name);
		flags |= F_UPDATE_SETTINGS;
	}
}

/*--------------------------------------------------------------------
MainWnd::Hardware_Callback()
--------------------------------------------------------------------*/
void MainWnd::Hardware_Callback(const HARDWARE_NOTIFY& hwn)
{
	MainWnd* pthis = (MainWnd*)hwn.pparam;
	pthis->hwn_queue.Push(hwn);
}

/*--------------------------------------------------------------------
MainWnd::OnIdle()
--------------------------------------------------------------------*/
void MainWnd::OnIdle()
{
	/* ������������ ������������ ����������� �� ����������. */
	HARDWARE_NOTIFY hwn;
	while (hwn_queue.Pop(hwn))
		HandleHardwareNotify(hwn);

	SETTINGS s;
	_settings.Get(s);
	Hardware& hw = *_hardware[s.selected_device_index];

	if (flags & F_UPDATE_SETTINGS)
	{
		flags &= ~F_UPDATE_SETTINGS;
		hw.UpdateHWS();
	}

	if (flags & F_STARTED || flags & F_DATA_REQUEST)
	{
		flags &= ~F_DATA_REQUEST;
		hw.RequestSample();
	}
}

/*--------------------------------------------------------------------
MainWnd::WindowProc()
--------------------------------------------------------------------*/
LPARAM CALLBACK MainWnd::WindowProc(HWND hwnd, UINT msg, 
		WPARAM wParam, LPARAM lParam)
{
	return Window::WindowProc(hwnd, msg, wParam, lParam);
}

/*--------------------------------------------------------------------
MainWnd::Create()
--------------------------------------------------------------------*/
BOOL MainWnd::Create()
{
	if (Window::Create(NULL))
	{
		/* ������� ������ ���������� �� ����������. */
		//_hardware.push_back(new Emulator());
		//_hardware.push_back(new Wave());
		_hardware.push_back(new Scope());
		SETTINGS s;
		_settings.Get(s);
		VERIFY(_hardware[0]->Init(Hardware_Callback, this));
		flags |= F_UPDATE_SETTINGS | F_DATA_REQUEST;

		/*---*/

		HMENU hmenu = 
			LoadMenu(Globals::GetInstanceHandle(), 
			MAKEINTRESOURCE(IDR_MAINWND_MENU));
		ASSERT(hmenu != NULL);
		VERIFY(SetMenu(hwnd, hmenu));

		/* ������� �������� ����. */

		VERIFY(tab.Create(hwnd));
		VERIFY(tab.AddPanel(&controls, IDS_CONTROLS_CAPTION));
		VERIFY(tab.AddPanel(&dcon, IDS_DCONTROLS_CAPTION));
//		VERIFY(tab.AddPanel(&strobe, IDS_STROBE_CAPTION));
		ShowWindow(tab.GetHwnd(), SW_SHOWNORMAL);

		VERIFY(view.Create(hwnd));
		ShowWindow(view.GetHwnd(), SW_SHOWNORMAL);
		View::SETUP vs;
		view.GetVS(vs);
		vs.flags |= VF_AUTOUPDATE_ENABLED;
		view.SetVS(vs);

		VERIFY(infopan.Create(hwnd));
		ShowWindow(infopan.GetHwnd(), SW_SHOWNORMAL);
		
		Globals::Log(IDS_PROGRAM_RUNNED);

		CmdStart(); //����� �����...
	}


	return hwnd != NULL;
}
