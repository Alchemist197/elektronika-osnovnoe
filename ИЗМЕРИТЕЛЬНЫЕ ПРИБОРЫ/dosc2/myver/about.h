#ifndef __ABOUT_H
#define __ABOUT_H

#include "window.h"
#include "resource.h"

/*--------------------------------------------------------------------
About
--------------------------------------------------------------------*/
class About: public ModalDialog
{
protected:

	virtual UINT GetId() {
		return IDD_ABOUT; }
};

#endif /* #ifndef __ABOUT_H */
