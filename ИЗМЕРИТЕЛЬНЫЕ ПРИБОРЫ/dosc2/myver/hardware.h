#ifndef __HARDWARE_H
#define __HARDWARE_H

#include "data.h"

/*--------------------------------------------------------------------
Range
--------------------------------------------------------------------*/
class Range
{
public:

	Range() {
		min = 0;
		max = 0;
		mask = 0; }
	Range(REAL min, REAL max, UINT mask = 0) {
		Set(min, max, mask); }

	REAL GetMin() {
		return min; }
	REAL GetMax() {
		return max; }
	REAL GetInterval() {
		return max - min; }
	UINT GetMask() {
		return mask; }

	void Set(REAL min, REAL max, UINT mask = 0) {
		Range::min = min;
		Range::max = max; 
		Range::mask = mask; }

protected:

	REAL min;
	REAL max;
	UINT mask;
};

typedef vector<Range> RANGE_VECTOR;

/*--------------------------------------------------------------------
Resolution
--------------------------------------------------------------------*/
class Resolution
{
public:

	Resolution(UINT id, const char* name, 
			REAL base_time, 
			REAL step_time, 
			UINT max_scale) {
		Resolution::id = id;
		strcpy(Resolution::name, name);
		Resolution::base_time = base_time;
		Resolution::step_time = step_time; 
		Resolution::max_scale = max_scale; }

	Resolution(UINT id, const char* name, 
			DWORD freq, 
			UINT base_time_div, 
			UINT step_time_div /* = 0 */, 
			UINT max_scale /* = 0 */) {
		strcpy(Resolution::name, name);
		Resolution::id = id;
		base_time = (REAL)base_time_div/freq;
		step_time = (REAL)step_time_div/freq;
		Resolution::max_scale = max_scale; }

	void SetName(const char* name) {
		strcpy(Resolution::name, name); }
	void GetName(char* buffer) {
		strcpy(buffer, name); }

	UINT GetMaxScale() {
		return max_scale; }
	UINT GetId() {
		return id; }

	Resolution& AddBufferSize(UINT buffer_size) {
		buffer_sizes.push_back(buffer_size); 
		return* this; }

	void GetBufferSizes(UINT_VECTOR& sizes) {
		sizes = buffer_sizes; }
	UINT GetBufferSize(UINT index) {
		ASSERT(index < buffer_sizes.size());
		return buffer_sizes[index]; }

	REAL GetTime(UINT scale) {
		ASSERT(scale <= max_scale);
		if (scale == 0)  return(0.000000025);//25nS	/ 40MSPS
		if (scale == 1)  return(0.000000050);//50nS	/ 20MSPS
		if (scale == 2)  return(0.000000125);//125nS / 8MSPS
		if (scale == 3)  return(0.000000250);//250nS / 4MSPS
		if (scale == 4)  return(0.000000500);//500nS / 2MSPS
		if (scale == 5)  return(0.000001250);//1.25uS / 0.8MSPS
		if (scale == 6)  return(0.000002500);//2.5 uS / 0.4MSPS
		if (scale == 7)  return(0.000005000);//5.0 uS / 0.2MSPS
		if (scale == 8)  return(0.000012500);//12.5uS / 0.08MSPS
		if (scale == 9)  return(0.000025000);//25.0uS / 0.04MSPS
		if (scale == 10) return(0.000050000);//50.0uS / 0.02MSPS
		if (scale == 11) return(0.000125000);//125 uS / 0.008MSPS
		if (scale == 12) return(0.000250000);//250 uS / 0.004MSPS
		return(1);
	}
		//return base_time + scale * step_time; }

protected:

	REAL base_time;
	REAL step_time;
	UINT max_scale;

	UINT_VECTOR buffer_sizes;

	char name[256];
	UINT id;
};

typedef vector<Resolution> RESOLUTION_VECTOR;

/*--------------------------------------------------------------------
HARDWARE_CAPS
--------------------------------------------------------------------*/
const UINT HWC_TIMEOUT_CONTROL = 0x0001;
struct HARDWARE_CAPS
{
	RANGE_VECTOR ranges;
	RESOLUTION_VECTOR resolutions;

	UINT max_capture_level;
	UINT max_capture_delay;

	UINT bits_per_sample;

	UINT flags;
};

/*--------------------------------------------------------------------
HARDWARE_SETTINGS
--------------------------------------------------------------------*/
const UINT HWS_SLOPE_POSEDGE	= 0x0001;
const UINT HWS_CAPTURE_ENABLED	= 0x0002;
const UINT HWS_CAPTURE_TYPE		= 0x0004;
const UINT HWS_INPUT_ACDC		= 0x0008;	//3-�� ���
//
struct HARDWARE_SETTINGS
{
	/* �� ��������� ��� �������� ��������� ��������������� � ����. */

	UINT range_index;
	UINT resolution_index;
	UINT resolution_scale;
	UINT buffer_size_index;

	UINT capture_level;
	UINT capture_delay;

	UINT flags;
	/* ������� �������� ������� (����������������� �����).
	0 == �����. ����� ��������. */
	REAL input_frequency;
};

class Hardware;

/*--------------------------------------------------------------------
HARDWARE_NOTIFY
--------------------------------------------------------------------*/

#define HWN_SUCCESS		0x0001
#define HWN_SETTINGS	0x0002
#define HWN_SAMPLE		0x0004

struct HARDWARE_NOTIFY
{
	UINT flags;
	Hardware* phw;
	void* pparam;
	Data data;	
};

/* ��������:
void Hardware_Callback(const HARDWARE_NOTIFY& hwn); 
������ ������� �� ������ ���� ������� WinAPI, ������� �����
������� deadlock. */
typedef void (*HARDWARE_CALLBACK)(const HARDWARE_NOTIFY& hwn);
typedef queue<HARDWARE_NOTIFY> HARDWARE_NOTIFY_QUEUE;

/*--------------------------------------------------------------------
HardwareNotifyQueue
--------------------------------------------------------------------*/
class HardwareNotifyQueue
{
public:

	/* Interlocked. */
	void Push(const HARDWARE_NOTIFY& hwn) {
		cs.Enter();
		q.push(hwn);
		cs.Leave(); }

	/* Interlocked. */
	BOOL Pop(HARDWARE_NOTIFY& hwn) {
		BOOL ok = TRUE;
		cs.Enter();
		if (q.size() > 0)
		{
			hwn = q.front();
			q.pop();
		}
		else
			ok = FALSE;
		cs.Leave();
		return ok; }

protected:

	HARDWARE_NOTIFY_QUEUE q;
	CriticalSection cs;
};

/*--------------------------------------------------------------------
Hardware
--------------------------------------------------------------------*/
class Hardware
{
public:

	Hardware();
	virtual ~Hardware() {}

	virtual BOOL Init(HARDWARE_CALLBACK callback, void* pparam);
	virtual BOOL Shutdown();

	virtual char* GetName(char* name) {
		return strcpy(name, "Hardware -- Base Class"); }

	void GetCaps(HARDWARE_CAPS& caps) {
		caps = Hardware::caps; }

	/* ������ �������� ��� ������ ������� ����������. */
	/* Interlocked. */
	void SetHWSBuffer(const HARDWARE_SETTINGS& hws) {
		cs.Enter();
		tmp_hws = hws;
		cs.Leave(); }
	/* Interlocked. */
	void GetHWSBuffer(HARDWARE_SETTINGS& hws) {
		cs.Enter();
		hws = tmp_hws;
		cs.Leave(); }

	/* ������ �� ��������� �������� ����������. ����� ���������� 
	�������� ���������� ����� callback, ��� ��������� HWN_SETTINGS. */
	/* Interlocked. */
	virtual void UpdateHWS() {
		cs.Enter();
		hardware_settings = tmp_hws;
		cs.Leave(); }

	/* ������ �������. ����� ���������� 
	�������� ���������� ����� callback, ��� ��������� HWN_SAMPLE. */
	virtual void RequestSample() {}

protected:

	/* Interlocked. */
	void SetHWS(const HARDWARE_SETTINGS& hws) {
		cs.Enter();
		hardware_settings = hws;
		cs.Leave(); }

	/* Interlocked. */
	void GetHWS(HARDWARE_SETTINGS& hws) {
		cs.Enter();
		hws = hardware_settings;
		cs.Leave(); }

	void Notify(UINT flags, UINT_VECTOR* pv);

	HARDWARE_CAPS caps;

private:

	CriticalSection cs;
	/* ���������, �������������� _����������_. */
	HARDWARE_SETTINGS hardware_settings;
	/* �����. */
	HARDWARE_SETTINGS tmp_hws;
	HARDWARE_CALLBACK callback;
	void* pparam;
};

typedef vector<Hardware*> PHARDWARE_VECTOR;
extern PHARDWARE_VECTOR _hardware;

#endif /* #ifndef __HARDWARE_H */