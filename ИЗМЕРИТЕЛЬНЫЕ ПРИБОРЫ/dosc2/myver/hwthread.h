#ifndef __HWTHREAD_H
#define __HWTHREAD_H

#include "hardware.h"

/*--------------------------------------------------------------------
HardwareTh
--------------------------------------------------------------------*/
class HardwareTh: public Hardware
{
public:

	HardwareTh();
	virtual ~HardwareTh();

	virtual BOOL Init(HARDWARE_CALLBACK callback, void* pparam);
	virtual BOOL Shutdown();

	virtual void UpdateHWS();
	virtual void RequestSample();

protected:

	virtual void IO_Thread_OnDataRequest() = 0;
	virtual BOOL IO_Thread_OnUpdateSettingsRequest(
		const HARDWARE_SETTINGS& hws) = 0;

	/* ��� ������� ������ ���� ������� � ����� Init()
	��������� ������. */
	BOOL CreateIOThread(int priority = THREAD_PRIORITY_NORMAL);

	/* ��� ������� ������ ���� ������� � ������ Shutdown()
	��������� ������. */
	BOOL TerminateIOThread();

private:

	static unsigned __stdcall IO_ThreadProc(void* pparam);

	HANDLE io_thread_handle;

	enum { NUMBER_OF_EVENTS = 3 };
	/* ������� � ������� events. */
	enum {
		EVENT_TERMINATE_IO_THREAD_REQUEST = 0,
		EVENT_DATA_REQUEST = 1,
		EVENT_UPDATE_SETTINGS_REQUEST = 2 };
	HANDLE events[NUMBER_OF_EVENTS];
};

#endif /* #ifndef __HWTHREAD_H */