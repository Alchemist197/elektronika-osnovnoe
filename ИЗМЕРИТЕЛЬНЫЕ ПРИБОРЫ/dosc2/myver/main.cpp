#include "precomp.h"
#include "global.h"
#include "mainwnd.h"
#include "settings.h" 
#include "comm.h"
#include "hardware.h"

/*--------------------------------------------------------------------
WinMain()
--------------------------------------------------------------------*/
int APIENTRY WinMain(HINSTANCE hinstance, HINSTANCE hprev_instance,
					 char* cmd_line, int cmd_show)
{
	Globals::SetInstanceHandle(hinstance);

	MainWnd mainwnd;
	Globals::SetMainWindowPointer(&mainwnd);

	INITCOMMONCONTROLSEX iccex;
	ZeroMemory(&iccex, sizeof(INITCOMMONCONTROLSEX));
	iccex.dwSize = sizeof(INITCOMMONCONTROLSEX);
	iccex.dwICC = ICC_BAR_CLASSES | ICC_TAB_CLASSES;
	VERIFY(InitCommonControlsEx(&iccex));

	VERIFY(mainwnd.Create());
	ShowWindow(mainwnd.GetHwnd(), SW_MAXIMIZE);//cmd_show); //��������������� ����...
	UpdateWindow(mainwnd.GetHwnd());

	MSG msg;
	BOOL get_message_result = FALSE;
	do {
		while (!PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
		{
			mainwnd.OnIdle();

			/* ��������� ��������� �� 100% ��������. */
			Sleep(10);
		}

		if (get_message_result = GetMessage(&msg, NULL, 0, 0))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	} while (get_message_result);

	for (size_t i = 0; i < _hardware.size(); i++)
		delete _hardware[i];

	return 0;
}
