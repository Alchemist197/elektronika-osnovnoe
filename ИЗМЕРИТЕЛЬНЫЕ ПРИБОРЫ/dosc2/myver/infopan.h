#ifndef __INFOPAN_H
#define __INFOPAN_H

#include "window.h"
#include "resource.h"

/*--------------------------------------------------------------------
InfoPan
--------------------------------------------------------------------*/
class InfoPan: public Panel
{
public:

	void Log(const char* msg);

protected:

	MSG_HANDLER BOOL OnInitDialog(HWND hwnd, HWND hwndFocus, 
		LPARAM lParam);
	MSG_HANDLER void OnSize(HWND hwnd, UINT state, int cx, int cy);

	virtual UINT GetId() {
		return IDD_INFOPAN; }

	HWND edit_log;
};

#endif /* #ifndef __INFOPAN_H */