#include "precomp.h"
#include "global.h"
#include "hardware.h"

PHARDWARE_VECTOR _hardware;

/*--------------------------------------------------------------------
Hardware::Hardware()
--------------------------------------------------------------------*/
Hardware::Hardware()
{
	callback = NULL;
	pparam = NULL;

	ZeroMemory(&hardware_settings, sizeof(HARDWARE_SETTINGS));
	ZeroMemory(&tmp_hws, sizeof(HARDWARE_SETTINGS));
}

/*--------------------------------------------------------------------
Hardware::Init()
--------------------------------------------------------------------*/
BOOL Hardware::Init(HARDWARE_CALLBACK callback, void* pparam)
{
	Hardware::callback= callback;
	Hardware::pparam = pparam;

	caps.ranges.clear();
	caps.resolutions.clear();

	caps.bits_per_sample = 8;
	caps.max_capture_delay = 255;
	caps.max_capture_level = (1 << caps.bits_per_sample) - 1;
	caps.flags = 0;

	ZeroMemory(&hardware_settings, sizeof(HARDWARE_SETTINGS));
	ZeroMemory(&tmp_hws, sizeof(HARDWARE_SETTINGS));

	return TRUE;
}

/*--------------------------------------------------------------------
Hardware::Notify()
--------------------------------------------------------------------*/
void Hardware::Notify(UINT flags, UINT_VECTOR* pv)
{
	HARDWARE_NOTIFY hwn;
	ZeroMemory(&hwn, sizeof(HARDWARE_NOTIFY));

	hwn.flags = flags;
	hwn.phw = this;
	hwn.pparam = pparam;

	HARDWARE_SETTINGS hws;
	GetHWS(hws);

	Data::SETUP ds;
	ZeroMemory(&ds, sizeof(Data::SETUP));

	/* �����. ����� �������? */
	if (pv != NULL && hws.input_frequency != 0)
	{
		UINT_VECTOR& v = *pv;
		UINT scan_level;
		if (hws.flags & HWS_CAPTURE_ENABLED)
			scan_level = hws.capture_level;
		else
		{
			scan_level = *min_element(v.begin(), v.end()) +
				*max_element(v.begin(), v.end());
			scan_level >>= 1;
		}

		/* ������������ ����� ������ ��������, ����������
		������ ������� � ���������� �������. */
        size_t i = 0;
		int nperiods = 0, origin = -1, end;
		while (i < v.size())
		{
			while (i < v.size() && v[i] < scan_level) i++;
			if (origin == -1) origin = i; else end = i;

			while (i < v.size() && v[i] >= scan_level) i++;
			while (i < v.size() && v[i] < scan_level) i++;
			if (i < v.size()) nperiods++;
		}
		
		/* ������ �� ����������. */
		if (nperiods == 0 || origin == end)
			ds.time_per_sample = 1e+18;
		else
		{
			ds.time_per_sample =
				nperiods/(hws.input_frequency*(end - origin));
		}
	}
	else
	{
		ds.time_per_sample =
			caps.resolutions[hws.resolution_index].GetTime(
			hws.resolution_scale);
	}
	/*---*/
	ds.bits_per_sample = caps.bits_per_sample;
	ds.min_voltage =
		caps.ranges[hws.range_index].GetMin();
	ds.max_voltage =
		caps.ranges[hws.range_index].GetMax();
	
	hwn.data.Setup(pv != NULL ? *pv : UINT_VECTOR(), ds);

	callback(hwn);
}

/*--------------------------------------------------------------------
Hardware::Shutdown()
--------------------------------------------------------------------*/
BOOL Hardware::Shutdown()
{
	return TRUE;
}
