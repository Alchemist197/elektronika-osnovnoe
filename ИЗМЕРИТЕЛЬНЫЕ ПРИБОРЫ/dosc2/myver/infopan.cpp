#include "precomp.h"
#include "global.h"
#include "infopan.h"
#include "resource.h"

/*--------------------------------------------------------------------
InfoPan::Log()
--------------------------------------------------------------------*/
void InfoPan::Log(const char* msg)
{
	const int LINES_MAX = 256;

	SendMessage(edit_log, WM_SETREDRAW, (WPARAM)FALSE, 0);
	int nlines = Edit_GetLineCount(edit_log);
	if (nlines > LINES_MAX)
	{
		Edit_SetSel(edit_log, 0,
			Edit_LineIndex(edit_log, nlines >> 1));
		/* ������� �������� �����. */
		Edit_ReplaceSel(edit_log, "");
	}
	SendMessage(edit_log, WM_SETREDRAW, (WPARAM)TRUE, 0);

	SYSTEMTIME time;
	GetLocalTime(&time);

	char buffer[256];
	sprintf(buffer, "%02d:%02d:%02d %s\r\n", 
		time.wHour, 
		time.wMinute, 
		time.wSecond,
		msg);

	Edit_SetSel(edit_log,
		Edit_GetTextLength(edit_log),
		Edit_GetTextLength(edit_log));
	Edit_ReplaceSel(edit_log, buffer);
}

/*--------------------------------------------------------------------
InfoPan::OnInitDialog()
--------------------------------------------------------------------*/
BOOL InfoPan::OnInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam) 
{
	VERIFY(edit_log = GetDlgItem(hwnd, IDC_LOG));

	return FALSE;
}

/*--------------------------------------------------------------------
InfoPan::OnSize()
--------------------------------------------------------------------*/
void InfoPan::OnSize(HWND hwnd, UINT state, int cx, int cy) 
{
	const int BORDER = 2;

	MoveWindow(edit_log, BORDER, BORDER,
		cx - 2*BORDER, cy - 2*BORDER, TRUE);
}
