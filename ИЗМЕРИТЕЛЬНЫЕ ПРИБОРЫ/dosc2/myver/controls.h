#ifndef __CONTROLS_H
#define __CONTROLS_H

#include "window.h"
#include "resource.h"
#include "settings.h"

/*--------------------------------------------------------------------
Controls
������ ���������� ������������.
--------------------------------------------------------------------*/
class Controls: public Panel
{
public:

	void UpdateControls();

protected:

	MSG_HANDLER void OnTrack(HWND hwnd, HWND hwnd_track,
		UINT notify_code, UINT position);

	MSG_HANDLER BOOL OnInitDialog(HWND hwnd, HWND hwndFocus, 
		LPARAM lParam);
	MSG_HANDLER void OnCommand(HWND hwnd, int id, HWND hwnd_ctl,
		UINT code_notify);

	virtual LPARAM CALLBACK WindowProc(HWND hwnd, UINT msg, 
		WPARAM wParam, LPARAM lParam);

	virtual UINT GetId() {
		return IDD_CONTROLS; }

	HWND combo_devices;
	HWND combo_buffer_sizes;
	HWND combo_sample_times;
	HWND combo_voltage_ranges;
	HWND combo_resolutions;
	HWND combo_slope;
	HWND combo_slope2;	//��� �������� int/ext
	HWND combo_slope3;	//AC/DC
//	HWND butt_start;	//������ START
//	HWND butt_stop;		//������ STOP
//	HWND butt_sample;	//������ SAMPLE

	HWND track_capture_level;
	HWND track_capture_delay;

	HWND check_capture_enabled;
	HWND check_timeout;
};


#endif /* #ifndef __CONTROLS_H */