#ifndef __CURSOR_H
#define __CURSOR_H

/*--------------------------------------------------------------------
Cursor
--------------------------------------------------------------------*/
class Cursor
{
public:

	Cursor(char name, REAL pos);
	~Cursor();

	void SetRect(const RECT& rect) {
		Cursor::rect = rect; }

	void SetPos(REAL pos) {
		Cursor::pos = pos; }
	REAL GetPos() {
		return pos; }
	char GetName() {
		return name; }

	void Draw(HDC dc, BOOL is_selected);

	BOOL MouseTest(POINT mouse);
	virtual void OnMouseMove(POINT mouse) = 0;

	static int GetRSize() {
		return RSIZE; }

protected:
	
	HFONT GetFont();

	/* ������ ������� ��������, ��������� ��� ������� �������
	�����. */
	enum { RSIZE = 12 };

	/* ��������� ��������� ������ ��������, ��������� ���
	������� ������� �����. */
	virtual void GetCenterCoords(POINT& p) = 0;

	/* ��������� ������� ��������� "����������" �������� �������. */
	virtual void GetPolygonCoords(POINT_VECTOR& v) = 0;

	/* ��������� ��������� �������. */
	virtual void GetLineCoords(POINT& p1, POINT& p2) = 0;

	char name;
	/* ���� �����������. */
	RECT rect;
	/* ������� �������, � ��������� [0..1]. */
	REAL pos;
	HFONT hfont;
};

typedef vector<Cursor*> PCURSOR_VECTOR;

/*--------------------------------------------------------------------
CursorV
--------------------------------------------------------------------*/
class CursorV: public Cursor
{
public:

	CursorV(char name, REAL pos): Cursor(name, pos) {}

	virtual void OnMouseMove(POINT mouse);

protected:

	virtual void GetCenterCoords(POINT& p);
	virtual void GetLineCoords(POINT& p1, POINT& p2);
	virtual void GetPolygonCoords(POINT_VECTOR& v);
};

/*--------------------------------------------------------------------
CursorH
--------------------------------------------------------------------*/
class CursorH: public Cursor
{
public:

	CursorH(char name, REAL pos): Cursor(name, pos) {}

	virtual void OnMouseMove(POINT mouse);

protected:

	virtual void GetCenterCoords(POINT& p);
	virtual void GetLineCoords(POINT& p1, POINT& p2);
	virtual void GetPolygonCoords(POINT_VECTOR& v);
};

/*--------------------------------------------------------------------
CursorsCollection
--------------------------------------------------------------------*/
class CursorsCollection
{
public:

	CursorsCollection();
	~CursorsCollection();

	void Enable(BOOL enable);
	BOOL IsEnabled() {
		return enabled; }

	BOOL GetPosByName(char name, REAL& pos);
	BOOL SetPosByName(char name, REAL pos);

	void AddCursor(Cursor* pcursor) {
		ASSERT(pcursor != NULL);
		pcursors.push_back(pcursor); }

	void Draw(HDC dc);
	void SetRect(const RECT& rect);

	/* ��� ������� Mouse.... ���������� TRUE, ���� ���������
	����������� ��������. */
	BOOL OnMouseLButtonDown(HWND hwnd, POINT mouse);
	BOOL OnMouseLButtonUp(POINT mouse);
	BOOL OnMouseMove(POINT mouse);

protected:

	PCURSOR_VECTOR pcursors;
	int active_index; /* ����� ���� -1. */
	BOOL enabled;
};

#endif /* #ifndef __CURSOR_H */
