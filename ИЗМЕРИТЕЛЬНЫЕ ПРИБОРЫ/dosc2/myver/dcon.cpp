#include "precomp.h"
#include "global.h"
#include "dcon.h"
#include "mainwnd.h"
#include "units.h"
#include "resource.h"

/*--------------------------------------------------------------------
DControls::UpdateControls()
--------------------------------------------------------------------*/
void DControls::UpdateControls()
{
	int i;
	char buffer[256];
	
	View::SETUP vs;
	MainWnd& mainwnd = *(MainWnd*)Globals::GetMainWindowPointer();
	View& view = mainwnd.GetView();
	view.GetVS(vs);
	
	/* combo_line_width. */
	const int MAX_LINE_WIDTH = 8;
	ComboBox_ResetContent(combo_line_width);
	for (i = 1; i <= MAX_LINE_WIDTH; i++)
	{
		sprintf(buffer, "%d", i);
		ComboBox_AddString(combo_line_width, buffer);
	}
	ComboBox_SetCurSel(combo_line_width, vs.line_width - 1);

	/* combo_memory. */
	ComboBox_ResetContent(combo_memory);
	const int MAX_MEMORY = 8;
	for (i = 1; i <= MAX_MEMORY; i++)
	{
		sprintf(buffer, "%d", i);
		ComboBox_AddString(combo_memory, buffer);
	}
	ComboBox_SetCurSel(combo_memory, vs.memory - 1);

	/* check_calc_fft_max. */
	Button_SetCheck(check_calc_fft_max,
		(vs.flags & VF_CALC_FFT_MAX) ? 
		BST_CHECKED : BST_UNCHECKED);

	/* check_show_data_points. */
	Button_SetCheck(check_show_data_points,
		(vs.flags & VF_SHOW_POINTS) ? 
		BST_CHECKED : BST_UNCHECKED);

	/* check_enable_min. */
	Button_SetCheck(check_enable_min,
		(vs.flags & VF_MAXY_ENABLED) ? 
		BST_CHECKED : BST_UNCHECKED);

	/* check_enable_max. */
	Button_SetCheck(check_enable_max,
		(vs.flags & VF_MINY_ENABLED) ? 
		BST_CHECKED : BST_UNCHECKED);

	/* combo_minmax_timeout. */
	ComboBox_ResetContent(combo_minmax_timeout);
	ComboBox_AddString(combo_minmax_timeout, "0s");
	const int MAX_MINMAX_TIMEOUT = 90;
	for (int timeout = 1; timeout <= MAX_MINMAX_TIMEOUT; timeout <<= 1)
	{
		sprintf(buffer, "%ds", timeout);
		ComboBox_AddString(combo_minmax_timeout, buffer);
	}
	sprintf(buffer, "%ds", vs.minmax_timeout);
#ifdef _DEBUG
	ASSERT(ComboBox_SelectString(combo_minmax_timeout,
		0, buffer) != -1);
#else
	ComboBox_SelectString(combo_minmax_timeout,
		0, buffer);
#endif /* #ifdef DEBUG */
}

/*--------------------------------------------------------------------
DControls::OnInitDialog()
--------------------------------------------------------------------*/
BOOL DControls::OnInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam) 
{
	VERIFY(combo_memory = GetDlgItem(hwnd, IDC_MEMORY));
	VERIFY(combo_line_width = GetDlgItem(hwnd, IDC_LINE_WIDTH));
	VERIFY(combo_minmax_timeout = GetDlgItem(hwnd, IDC_MINMAX_TIMEOUT));

	VERIFY(check_calc_fft_max = GetDlgItem(hwnd, IDC_CALC_FFT_MAX));
	VERIFY(check_show_data_points =
		GetDlgItem(hwnd, IDC_SHOW_DATA_POINTS));
	VERIFY(check_enable_min = GetDlgItem(hwnd, IDC_ENABLE_MIN));
	VERIFY(check_enable_max = GetDlgItem(hwnd, IDC_ENABLE_MAX));

	UpdateControls();

	return TRUE; 
}

/*--------------------------------------------------------------------
DControls::OnCommand()
--------------------------------------------------------------------*/
void DControls::OnCommand(HWND hwnd, int id, HWND hwnd_ctl,
	UINT code_notify)
{
	View::SETUP vs, backup;
	MainWnd& mainwnd = *(MainWnd*)Globals::GetMainWindowPointer();
	View& view = mainwnd.GetView();
	view.GetVS(vs);
	backup = vs;

	if (hwnd_ctl == check_calc_fft_max)
	{
		if (Button_GetCheck(check_calc_fft_max) == BST_CHECKED)
			vs.flags |= VF_CALC_FFT_MAX;
		else
			vs.flags &= ~VF_CALC_FFT_MAX;
	}
	else if (hwnd_ctl == check_show_data_points)
	{
		if (Button_GetCheck(check_show_data_points) == BST_CHECKED)
			vs.flags |= VF_SHOW_POINTS;
		else
			vs.flags &= ~VF_SHOW_POINTS;
	}
	else if (hwnd_ctl == check_enable_min)
	{
		if (Button_GetCheck(check_enable_min) == BST_CHECKED)
			vs.flags |= VF_MAXY_ENABLED;
		else
			vs.flags &= ~VF_MAXY_ENABLED;
	}
	else if (hwnd_ctl == check_enable_max)
	{
		if (Button_GetCheck(check_enable_max) == BST_CHECKED)
			vs.flags |= VF_MINY_ENABLED;
		else
			vs.flags &= ~VF_MINY_ENABLED;
	}

	if (code_notify == CBN_SELENDOK)
	{
		if (hwnd_ctl == combo_line_width)
		{
			vs.line_width = ComboBox_GetCurSel(
				combo_line_width) + 1;
		}
		else if (hwnd_ctl == combo_memory)
		{
			vs.memory = ComboBox_GetCurSel(combo_memory) + 1;
		}
		else if (hwnd_ctl == combo_minmax_timeout)
		{
			char buffer[256];
			ComboBox_GetText(combo_minmax_timeout,
				buffer, sizeof(buffer));
			vs.minmax_timeout = atoi(buffer);
		}
	}

	/* ���������� ���������? */
	if (memcmp(&backup, &vs, sizeof(View::SETUP)) != 0)
		view.SetVS(vs);
};
