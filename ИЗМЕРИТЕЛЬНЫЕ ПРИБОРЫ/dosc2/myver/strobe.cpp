#include "precomp.h"
#include "global.h"
#include "strobe.h"
#include "mainwnd.h"
#include "units.h"
#include "resource.h"

/*--------------------------------------------------------------------
Strobe::UpdateControls()
--------------------------------------------------------------------*/
/*void Strobe::UpdateControls()
{
	SETTINGS s;
	_settings.Get(s);

	Hardware& hw = *_hardware[s.selected_device_index];

	HARDWARE_SETTINGS hws;
	hw.GetHWSBuffer(hws);

	Button_SetCheck(check_strobe_enabled, hws.input_frequency != 0);

	char buffer[256];
	*buffer = '\0';
	if (hws.input_frequency != 0)
	{
		Units units;
		units.Setup(hws.input_frequency, "Hz");
		units.Translate(buffer, hws.input_frequency);
	}
	SetWindowText(static_strobe_freq, buffer);
}
*/
/*--------------------------------------------------------------------
Strobe::OnInitDialog()
--------------------------------------------------------------------*/
/*BOOL Strobe::OnInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam) 
{
	VERIFY(check_strobe_enabled = GetDlgItem(hwnd, IDC_STROBE_ENABLE));
	VERIFY(edit_strobe_freq = GetDlgItem(hwnd, IDC_STROBE_FREQ_EDIT));
	VERIFY(button_strobe_freq_enter = GetDlgItem(hwnd,
		IDC_STROBE_FREQ_ENTER));
	VERIFY(static_strobe_freq = GetDlgItem(hwnd,
		IDC_STROBE_FREQ_DISPLAY));

	UpdateControls();

	return TRUE; 
}
*/
/*--------------------------------------------------------------------
Strobe::OnCommand()
--------------------------------------------------------------------*/
/*void Strobe::OnCommand(HWND hwnd, int id, HWND hwnd_ctl,
	UINT code_notify)
{
	SETTINGS s;
	_settings.Get(s);

	Hardware& hw = *_hardware[s.selected_device_index];

	HARDWARE_SETTINGS hws, backup_hws;
	hw.GetHWSBuffer(hws);
	backup_hws = hws;

	if (hwnd_ctl == button_strobe_freq_enter)
	{
		char buffer[256];
		GetWindowText(edit_strobe_freq, buffer, sizeof(buffer));
		if (!Units::atof(buffer))
		{
			char msg[256];
			VERIFY(LoadString(Globals::GetInstanceHandle(),
				IDS_NAN, msg, sizeof(msg)));
			MessageBox(hwnd, msg, "", MB_OK);
		}
		else
			Units::atof(buffer, &hws.input_frequency);
	}

	// ���������� ��������� ����������? 
	if (memcmp(&backup_hws, &hws, sizeof(HARDWARE_SETTINGS)) != 0)
	{
		hw.SetHWSBuffer(hws);
		MainWnd& mainwnd = *(MainWnd*)Globals::GetMainWindowPointer();
		mainwnd.SetFlags(F_UPDATE_SETTINGS | F_DATA_REQUEST);
		mainwnd.GetView().ResetMemory();
		mainwnd.GetView().UpdateClient();
	}

	UpdateControls();
};*/
