#ifndef __WINDOW_H
#define __WINDOW_H

#define MSG_HANDLER virtual

/*--------------------------------------------------------------------
WindowBase
--------------------------------------------------------------------*/
class WindowBase
{
public:

	WindowBase(BOOL is_dialog);
	virtual ~WindowBase();

	HWND GetHwnd() {
		return hwnd; }

	void UpdateClient(BOOL erase = FALSE) {
		VERIFY(InvalidateRect(hwnd, NULL, erase));
		VERIFY(UpdateWindow(hwnd)); }
	
	void UpdateAll(BOOL erase = FALSE) {
		SendMessage(hwnd, WM_NCPAINT, (WPARAM)1, (LPARAM)0);
		UpdateClient(erase); }

protected:

	/* �������������:
	PrepareObjectPointer();
	hwnd = CreateWindow(...);
	FreeObjectPointer(); */
	void PrepareObjectPointer();
	void FreeObjectPointer();

	/* ����� ������������ ������� ������� �� ���������. */
	MSG_HANDLER void OnCommand(HWND hwnd, int id, HWND hwnd_ctl,
		UINT code_notify) {}
	MSG_HANDLER void OnDestroy(HWND hwnd) {}
	MSG_HANDLER void OnSize(HWND hwnd, UINT state, int cx, int cy) {}
	MSG_HANDLER void OnClose(HWND hwnd) {}
	MSG_HANDLER void OnTimer(HWND hwnd, UINT id) {}
	/*---*/
	MSG_HANDLER void OnLButtonDown(HWND hwnd, BOOL fDoubleClick,
		int x, int y, UINT keyFlags) {}
	MSG_HANDLER void OnLButtonUp(HWND hwnd,
		int x, int y, UINT keyFlags) {}
	MSG_HANDLER void OnMouseMove(HWND hwnd,
		int x, int y, UINT keyFlags) {}
	/*---*/
	MSG_HANDLER LRESULT OnNotify(HWND hwnd, int id, NMHDR* pnmh) {
		return 0; }

	virtual LPARAM CALLBACK WindowProc(HWND hwnd, UINT msg, 
		WPARAM wParam, LPARAM lParam);

	static LPARAM CALLBACK StaticWindowProc(HWND hwnd, UINT msg, 
		WPARAM wParam, LPARAM lParam);

	HWND hwnd;

private:

	WindowBase() {}

	BOOL is_dialog;

	static WindowBase* plast_window_created;
	static CriticalSection cs;
};

/*--------------------------------------------------------------------
Window
--------------------------------------------------------------------*/
class Window: public WindowBase
{
public:

	Window();

	virtual BOOL Create(HWND parent);

protected:

	WNDCLASS wndclass;
	CREATESTRUCT prop;

private:

	BOOL RegisterClass();
};

/*--------------------------------------------------------------------
Dialog
--------------------------------------------------------------------*/
class Dialog: public WindowBase
{
public:

	Dialog(): WindowBase(TRUE) {}

	virtual BOOL Create(HWND parent);

protected:

	/* ����� ������������ ������� ������� �� ���������. */
	MSG_HANDLER BOOL OnInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam) {
		return TRUE; }

	virtual UINT GetId() = 0;

	virtual LPARAM CALLBACK WindowProc(HWND hwnd, UINT msg, 
		WPARAM wParam, LPARAM lParam);
};

/*--------------------------------------------------------------------
ModalDialog
--------------------------------------------------------------------*/
class ModalDialog: public Dialog
{
public:

	virtual BOOL Create(HWND parent);

protected:

	MSG_HANDLER void OnCommand(HWND hwnd, int id, HWND hwnd_ctl,
		UINT code_notify);
};

/*--------------------------------------------------------------------
Panel
--------------------------------------------------------------------*/
class Panel: public Dialog
{
public:

	Panel() {
		w = 0;
		h = 0; }

	virtual BOOL Create(HWND parent);

	int GetWidth() {
		return w; }
	int GetHeight() {
		return h; }

protected:

	int w, h;
};

/*--------------------------------------------------------------------
Tab
--------------------------------------------------------------------*/
class Tab: public Window
{
public:

	Tab();

	virtual BOOL Create(HWND parent);

	/*  ��� ������� ����� ���� ������� ������ ����� Create().
	������ ��������� �������������. */
	BOOL AddPanel(Panel* ppan, UINT idr_caption = 0);

	int GetWidth() {
		return w; }
	int GetHeight() {
		return h; }

protected:

	MSG_HANDLER void OnSelChanged();
	MSG_HANDLER LRESULT OnNotify(HWND hwnd, int id, NMHDR* pnmh);
	MSG_HANDLER void OnSize(HWND hwnd, UINT state, int cx, int cy);

	HWND hwnd_tab;
	Panel* pcurrent_pan;
	
	/* ����. ������ � ������ �������. */
	int cx_max, cy_max;

	/* ����������� ������ � ������ ����,
	��������� ��� ����������� ����������� ���� �������. */
	int w, h;
};

#endif /* #ifndef __WINDOW_H */
