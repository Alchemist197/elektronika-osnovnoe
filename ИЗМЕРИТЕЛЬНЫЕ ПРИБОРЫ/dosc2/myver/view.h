#ifndef __VIEW_H
#define __VIEW_H

#include "window.h"
#include "settings.h"
#include "cursor.h"
#include "hrtimer.h"

/*--------------------------------------------------------------------
GRAPH
--------------------------------------------------------------------*/
struct GRAPH
{
	COLORREF color;
	Data data;
};

typedef vector<GRAPH> GRAPH_VECTOR;
typedef vector<GRAPH_VECTOR*> PGRAPH_VECTOR_VECTOR;
typedef vector<GRAPH_VECTOR> GRAPH_VECTOR_VECTOR;

/*--------------------------------------------------------------------
VIEW_PROP
--------------------------------------------------------------------*/
struct VIEW_PROP
{
	REAL min_voltage;
	REAL max_voltage;
	REAL time;			/* ������������ ���������. */

	UINT max_sample_value;

	UINT capture_level;
	BOOL capture_enabled;
};

/*--------------------------------------------------------------------
Mapper
����� ��� ��������� �������������� ��������� ����
f(x, y) = { xa + xb*(x + x0), ya + yb*(y + y0) };
--------------------------------------------------------------------*/
class Mapper
{
public:

	struct SETUP
	{
		/* ���������� ������ ������� � ������� �������� ����
		���� �����������. */
		POINT lb;
		POINT rt;
		
		REAL min_time;
		REAL max_time;
		REAL min_voltage;
		REAL max_voltage;
	};

	Mapper() {
		ZeroMemory(&ms, sizeof(SETUP)); }

	void Setup(const SETUP& ms) {
		Mapper::ms = ms;
		/* ��������� ������������ (��� X). */
		xa = ms.lb.x;
		xb = (ms.rt.x - ms.lb.x)/
			(ms.max_time - ms.min_time);
		x0 = ms.min_time;
		/* ��������� ������������ (��� Y). */
		ya = ms.lb.y;
		yb = (ms.rt.y - ms.lb.y)/
			(ms.max_voltage - ms.min_voltage);
		y0 = ms.min_voltage;
	}

	inline POINT GetLB() { return ms.lb; }
	inline POINT GetRT() { return ms.rt; }

	inline POINT Map(REAL time, REAL voltage) {
		POINT p;
		REAL x = xa + xb*(time - x0);
		REAL y = ya + yb*(voltage - y0);
		if (x < COORD_MIN)
			x = COORD_MIN;
		else if (x > COORD_MAX)
			x = COORD_MAX;
		if (y < COORD_MIN)
			y = COORD_MIN;
		else if (y > COORD_MAX)
			y = COORD_MAX;
		p.x = Globals::Round(x);
		p.y = Globals::Round(y);
		return p; }

protected:

	REAL xa, xb, x0;
	REAL ya, yb, y0;
	SETUP ms;
};

/*--------------------------------------------------------------------
ViewBase
--------------------------------------------------------------------*/
class ViewBase
{
public:
	
	ViewBase() {
		limit = 1; }
	~ViewBase() {
		ClearGraphs(); }

	/* Interlocked. */
	void ClearGraphs() {
		cs.Enter();
		for (size_t i = 0; i < q.size(); i++)
			delete q[i];
		q.clear();
		cs.Leave(); }

	/* Interlocked. */
	void AddGraphs(const GRAPH_VECTOR& graphs) {
		cs.Enter();
		if (q.size() == limit)
		{
			delete q.back();
			q.pop_back();
		}
		q.insert(q.begin(), new GRAPH_VECTOR(graphs));
		cs.Leave(); }

	/* Interlocked. */
	BOOL GetGraphs(GRAPH_VECTOR& graphs) {
		cs.Enter();
		BOOL ok = q.size() > 0;
		if (ok) graphs = *q.front();
		cs.Leave();
		return ok; }

protected:

	UINT GetLimit() {
		return limit; }

	/* Interlocked. */
	void SetLimit(UINT limit) {
		ASSERT(limit != 0);
		cs.Enter();
		ViewBase::limit = limit;
		while (q.size() > limit)
		{
			delete q.back();
			q.pop_back();
		}
		cs.Leave(); }

	/* Interlocked. */
	void GetQueue(GRAPH_VECTOR_VECTOR& q) {
		cs.Enter();
		q.clear();
		for (size_t i = 0; i < ViewBase::q.size(); i++)
			q.push_back(*ViewBase::q[i]);
		cs.Leave(); }

private:

	PGRAPH_VECTOR_VECTOR q;
	CriticalSection cs;
	UINT limit;
};

struct MINMAX
{
	int y;
	long last_update_time_ms;
	long last_shift_time_ms;
};

typedef vector<MINMAX> MINMAX_VECTOR;

const UINT VF_MEMORY_ENABLED		= 0x0001;
const UINT VF_MINY_ENABLED			= 0x0002;
const UINT VF_MAXY_ENABLED			= 0x0004;
const UINT VF_CALC_FFT_MAX			= 0x0008;
const UINT VF_SHOW_POINTS			= 0x0010;
const UINT VF_AUTOUPDATE_ENABLED	= 0x0020;

/*--------------------------------------------------------------------
View
--------------------------------------------------------------------*/
class View: public Window, public ViewBase
{
public:

	struct SETUP
	{
		/* ������� ����� �������� >= 1. */
		UINT line_width;
		/* ������������� >= 1. */
		UINT memory;
		/* ����-��� ��� ��������� ���������/����������, �. */
		UINT minmax_timeout;
		/* �����, ��. ��������� VF_xxx. */
		UINT flags;
	};

	View();
	~View();

	void GetVS(SETUP& vs) {
		vs = View::vs; }
	/* ��������� �������� � ���������� �����������. */
	void SetVS(const SETUP& vs, BOOL update = TRUE);

	virtual BOOL Create(HWND parent);

	void ResetCursors();
	void Draw(HDC dc, int w, int h);

	void ResetMemory() {
		UINT limit = GetLimit();
		SetLimit(1);
		SetLimit(limit); }

protected:

	/* ����� ��������� ���������/���������� �
	�������� ���������. */
	void ResetMinMax(BOOL reset_min, BOOL reset_max);
	void UpdateMinMax(const POINT_VECTOR& points, int graph_w);

	void Redraw(HDC dc /* ������ ���� NULL. */);
	
	virtual LPARAM CALLBACK WindowProc(HWND hwnd, UINT msg, 
		WPARAM wParam, LPARAM lParam);

	MSG_HANDLER void OnDestroy(HWND hwnd);
	MSG_HANDLER void OnTimer(HWND hwnd, UINT id);
	MSG_HANDLER void OnPaint(HWND hwnd);
	MSG_HANDLER void OnSize(HWND hwnd, UINT state, int cx, int cy);
	/*---*/
	MSG_HANDLER void OnLButtonDown(HWND hwnd, BOOL fDoubleClick,
		int x, int y, UINT keyFlags);
	MSG_HANDLER void OnLButtonUp(HWND hwnd,
		int x, int y, UINT keyFlags);
	MSG_HANDLER void OnMouseMove(HWND hwnd,
		int x, int y, UINT keyFlags);

	HFONT GetScaleFont();
	HFONT GetMetricsFont();

	void PlotGraph(HDC dc, const VIEW_PROP& vp,
		int w, int h, const GRAPH& graph,
		REAL color_mult, REAL width_mult);
	void PlotMinMax(HDC dc, const VIEW_PROP& vp, int w, int h);
	void DrawMetrics(HDC dc, const VIEW_PROP& vp, int w, int h,
		const GRAPH_VECTOR& graphs);
	void DrawGrid(HDC dc, const VIEW_PROP& vp, int w, int h);

	virtual char* GetClassName() {
		return "VIEW_WND_CLASS"; }
	
	/*--- ������. ---*/

	SETUP vs;

	enum { AUTOUPDATE_TIMER_ID = 0x1101 };
	UINT autoupdate_timer;

	MINMAX_VECTOR miny;
	MINMAX_VECTOR maxy;
	Timer minmax_timer;
	
	CursorsCollection cursors;
	Mapper mapper;
	HFONT hmetrics_font;
	HFONT hscale_font;
};

#endif /* #ifndef __VIEW_H */
