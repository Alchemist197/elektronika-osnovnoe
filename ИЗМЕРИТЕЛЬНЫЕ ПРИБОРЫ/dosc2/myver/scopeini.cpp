#include "precomp.h"
#include "global.h"
#include "scopeini.h"
#include "resource.h"

const char DEFAULT_INI_FILE_COMMENT[] = 
	"{ ������ �����:\n"
	"freq <������� ��������� ����������, ��>\n"
	"port <����� COM-�����> <��������, ���>\n"
	"buffer <������ ������, ����>\n"
	"range <���, �> <����, �> <�����>\n...\n"
	"range <���, �> <����, �> <�����>\n\n"
	"<�����> - ��, ��� ��������� � PORTA ��� ������ ���������������� "
	"���������.\n"
	"��������� ����������� � �������� �������.\n"
	"������� ���� �����������.\n"
	"������ �������, ��������� � ������ ������ ������������.\n"
	"��������� freq, port, buffer, range �����������. }\n\n";

/*--------------------------------------------------------------------
ScopeIniFile::GetDefault()
--------------------------------------------------------------------*/
void ScopeIniFile::GetDefault(INI_DATA& ini_data)
{
	ini_data.freq = 16000000;
	ini_data.port = 1;
	ini_data.baud_rate = 115200;
	ini_data.buffer_size = 400;
	ini_data.ranges.clear();
	ini_data.ranges.push_back(Range(0, 2));
}

/*--------------------------------------------------------------------
ScopeIniFile::Create()
--------------------------------------------------------------------*/
BOOL ScopeIniFile::Create(const char* file_name)
{
	UINT i;
	INI_DATA ini_data;
	GetDefault(ini_data);

	FILE* f = fopen(file_name, "wt");

	if (f == NULL)
	{
		Globals::Log(IDS_CREATE_FILE_FAILED, file_name);
		THROW;
	}

	/* ����� �����������. */
	if (fputs(DEFAULT_INI_FILE_COMMENT, f) == EOF ||
		/* �������. */
		fprintf(f, "freq %lu\n", ini_data.freq) < 0 ||
		/* ����� ����� � ��������. */
		fprintf(f, "port %u %u\n", ini_data.port,
			ini_data.baud_rate) < 0 ||
		/* ������ ������. */
		fprintf(f, "buffer %u\n", ini_data.buffer_size) < 0)
	{
		Globals::Log(IDS_WRITE_ERROR, file_name);
		THROW;
	}

	/* ��������� ����������. */
	for (i = 0; i < ini_data.ranges.size(); i++)
		if (fprintf(f, "range %f %f %u\n",
			ini_data.ranges[i].GetMin(),
			ini_data.ranges[i].GetMax(),
			ini_data.ranges[i].GetMask()) < 0)
		{
			Globals::Log(IDS_WRITE_ERROR, file_name);
			THROW;
		}
	
	/* ��������� ����. */
	if (fclose(f) != 0)
	{
		Globals::Log(IDS_CLOSE_FILE_FAILED, file_name);
		THROW;
	}
	return TRUE;
CATCH:
	if (f != NULL) fclose(f);
	return FALSE;
}

/*--------------------------------------------------------------------
ScopeIniFile::Read()
--------------------------------------------------------------------*/
BOOL ScopeIniFile::Read(INI_DATA& ini_data)
{
	const char* SCOPE_INI_FILE_NAME = "scope.ini";
	char file_name[MAX_PATH];
	FILE* f = NULL;
	long file_size = -1;
	char* buffer = NULL;

	ini_data.freq = 0;
	ini_data.port = 0;
	ini_data.baud_rate = 0;
	ini_data.buffer_size = 0;
	ini_data.ranges.clear();

	Globals::GetModulePath(file_name, sizeof(file_name));
	strcat(file_name, SCOPE_INI_FILE_NAME);

	if ((f = fopen(file_name, "rb")) == NULL)
	{
		Globals::Log(IDS_SCOPE_INI_FILE_NOT_FOUND, file_name);
		/* ������� ���� �������������. */
		if (!Create(file_name)) THROW;
	}
	else
		fclose(f);

	/* ��������� ������. */
	if ((f = fopen(file_name, "rb")) == NULL)
	{
		Globals::Log(IDS_OPEN_FILE_FAILED, file_name);
		THROW;
	}

	/* ���������� ������ �����. */
	if (fseek(f, 0, SEEK_END) != 0)
	{
		Globals::Log(IDS_FSEEK_FAILED, file_name);
		THROW;
	}
	if ((file_size = ftell(f)) == -1L)
	{
		Globals::Log(IDS_FTELL_FAILED, file_name);
		THROW;
	}
	/* ������������ � ������. */
	if (fseek(f, 0, SEEK_SET) != 0)
	{
		Globals::Log(IDS_FSEEK_FAILED, file_name);
		THROW;
	}

	/* �������� ������. */
	if ((buffer = (char*)malloc(file_size)) == NULL) 
	{
		Globals::Log(IDS_OUT_OF_MEMORY);		
		THROW; 
	}
	/* ������ ���� �������. */
	if (fread(buffer, 1, file_size, f) != file_size)
	{
		Globals::Log(IDS_READ_ERROR, file_name);
		THROW;
	}

	/* �����������. */
	if (!Translate(ini_data, buffer, file_size))
	{
		Globals::Log(IDS_SCOPE_INI_FILE_BAD_SYNTAX, file_name);
		THROW;
	}

	/* ����������� �������. */
	if (fclose(f) != 0)
	{
		Globals::Log(IDS_CLOSE_FILE_FAILED, file_name);
		THROW;
	}
	free(buffer);
	return TRUE;
CATCH:
	if (f != NULL) fclose(f);
	if (buffer != NULL) free(buffer);
	return FALSE;
}

/*--------------------------------------------------------------------
ScopeIniFile::Translate()
--------------------------------------------------------------------*/
BOOL ScopeIniFile::Translate(INI_DATA& ini_data,
							 char* buffer, long size)
{
	const char* p = buffer;
	const char* pend = buffer + size;
	BOOL ok = TRUE;
	char word[256];
	
	while (ok && p < pend)
	{
		NextWord(p, pend, word, sizeof(word), ok);
/*		if (strcmp(word, "freq") == 0)
		{
			NextWord(p, pend, word, sizeof(word), ok);
			if (!Globals::IsUint(word)) ok = FALSE;	*/
			ini_data.freq = 400000000;//atol(word);
			ini_data.buffer_size = 1000;
	//	}
		/*else*/ if (strcmp(word, "port") == 0)
		{
			/* ����� �����. */
			NextWord(p, pend, word, sizeof(word), ok);
			if (!Globals::IsUint(word)) ok = FALSE;
			ini_data.port = atoi(word);
			/* ��������. */
			NextWord(p, pend, word, sizeof(word), ok);
			if (!Globals::IsUint(word)) ok = FALSE;
			ini_data.baud_rate = atoi(word);
		}
		else /*if (strcmp(word, "buffer") == 0)
		{
			NextWord(p, pend, word, sizeof(word), ok);
			if (!Globals::IsUint(word)) ok = FALSE;
			ini_data.buffer_size = atol(word);
		}
		else*/ if (strcmp(word, "range") == 0)
		{
			NextWord(p, pend, word, sizeof(word), ok);
			if (!Globals::IsFloat(word)) ok = FALSE;
			REAL min = atof(word);
			/*---*/
			NextWord(p, pend, word, sizeof(word), ok);
			if (!Globals::IsFloat(word)) ok = FALSE;
			REAL max = atof(word);
			/*---*/
			if (max <= min) ok = FALSE;
			/*---*/
			NextWord(p, pend, word, sizeof(word), ok);
			if (!Globals::IsUint(word)) ok = FALSE;
			UINT mask = atoi(word);
			ini_data.ranges.push_back(Range(min, max, mask));
		}
		else if (*word != '\0')
			ok = FALSE;
	}

	if (ini_data.freq == 0 || ini_data.port == 0 ||
		ini_data.baud_rate == 0 || ini_data.buffer_size == 0 ||
		ini_data.ranges.size() == 0)
		ok = FALSE;

	return ok;
}

/*--------------------------------------------------------------------
ScopeIniFile::NextWord()
--------------------------------------------------------------------*/
void ScopeIniFile::NextWord(const char*& p, const char* pend,
							char* word, UINT sizeof_word, BOOL& ok)
{
#define WHITE(c) ((c) == ' ' || (c) == '\t' || (c) == '\r' ||\
	(c) == '\n')

	/* ���������� whitespace � �����������. */
	while (ok && p < pend &&
		WHITE(*p) || *p == '{')
	{
		while (p < pend && WHITE(*p)) p++;	

		if (p < pend && *p == '{')
		{
			/* ���������� ������������. */
			while (p < pend && *p != '}') p++;
			
			if (p == pend)
				/* ���������� �����������. */
				ok = FALSE;
			else
				p++;
		}
	}

	ASSERT(sizeof_word != 0);
	const char* word_end = word + sizeof_word;
	while (ok && p < pend &&
		!WHITE(*p) && *p != '{')
	{
		if (word < word_end - 1)
			*word++ = *p++;
		else
			ok = FALSE;
	}

	*word = '\0';
}
