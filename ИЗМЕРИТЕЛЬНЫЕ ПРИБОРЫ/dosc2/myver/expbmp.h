#ifndef __EXPBMP_H
#define __EXPBMP_H

#include "window.h"
#include "resource.h"

/*--------------------------------------------------------------------
EXPORT_BMP
--------------------------------------------------------------------*/
struct EXPORT_BMP
{
	int w;
	int h;
	BOOL rle;
	char file_name[MAX_PATH];
};

/*--------------------------------------------------------------------
ExpBmpDialog
--------------------------------------------------------------------*/
class ExpBmpDialog: public ModalDialog
{
public:

	ExpBmpDialog();

	void GetData(EXPORT_BMP& expbmp) {
		expbmp = ExpBmpDialog::expbmp; }

protected:

	MSG_HANDLER BOOL OnInitDialog(HWND hwnd, HWND hwndFocus, 
		LPARAM lParam);
	MSG_HANDLER void OnCommand(HWND hwnd, int id, HWND hwnd_ctl,
		UINT code_notify);

	virtual UINT GetId() {
		return IDD_EXPORT_BMP; }

	HWND edit_w;
	HWND edit_h;
	HWND static_filename;
	HWND check_rle;

	EXPORT_BMP expbmp;
};

#endif /* #ifndef __EXPBMP_H */