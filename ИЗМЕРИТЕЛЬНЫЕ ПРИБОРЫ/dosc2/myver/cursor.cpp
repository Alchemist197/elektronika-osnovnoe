#include "precomp.h"
#include "global.h"
#include "cursor.h"

/*////////////////////////////////////////////////////////////////////
CursorsCollection
////////////////////////////////////////////////////////////////////*/

/*--------------------------------------------------------------------
CursorsCollection::CursorsCollection()
--------------------------------------------------------------------*/
CursorsCollection::CursorsCollection()
{
	active_index = -1;
	enabled = TRUE;
}

/*--------------------------------------------------------------------
CursorsCollection::~CursorsCollection()
--------------------------------------------------------------------*/
CursorsCollection::~CursorsCollection()
{
	for (size_t i = 0; i < pcursors.size(); i++)
		delete pcursors[i];
}

/*--------------------------------------------------------------------
CursorsCollection::GetPosByName()
--------------------------------------------------------------------*/
BOOL CursorsCollection::GetPosByName(char name, REAL& pos)
{
	BOOL ok = FALSE;

	for (size_t i = 0; i < pcursors.size() && !ok; i++)
		if (pcursors[i]->GetName() == name)
		{
			ok = TRUE;
			pos = pcursors[i]->GetPos();
		}

	return ok;
}

/*--------------------------------------------------------------------
CursorsCollection::SetPosByName()
--------------------------------------------------------------------*/
BOOL CursorsCollection::SetPosByName(char name, REAL pos)
{
	BOOL ok = FALSE;

	for (size_t i = 0; i < pcursors.size() && !ok; i++)
		if (pcursors[i]->GetName() == name)
		{
			ok = TRUE;
			pcursors[i]->SetPos(pos);
		}

	return ok;
}

/*--------------------------------------------------------------------
CursorsCollection::Enable()
--------------------------------------------------------------------*/
void CursorsCollection::Enable(BOOL enable)
{
	enabled = enable;

	if (!enabled && active_index != -1)
	{
		ReleaseCapture();
		active_index = -1;
	}
}

/*--------------------------------------------------------------------
CursorsCollection::Draw()
--------------------------------------------------------------------*/
void CursorsCollection::Draw(HDC dc)
{
	if (enabled)
	{
		for (size_t i = 0; i < pcursors.size(); i++)
		{
			if (i != active_index)
				pcursors[i]->Draw(dc, FALSE);
		}

		if (active_index != -1)
			pcursors[active_index]->Draw(dc, TRUE);
	}
}

/*--------------------------------------------------------------------
CursorsCollection::SetRect()
--------------------------------------------------------------------*/
void CursorsCollection::SetRect(const RECT& rect)
{
	for (size_t i = 0; i < pcursors.size(); i++)
		pcursors[i]->SetRect(rect);
}

/*--------------------------------------------------------------------
CursorsCollection::OnMouseLButtonDown()
--------------------------------------------------------------------*/
BOOL CursorsCollection::OnMouseLButtonDown(HWND hwnd, POINT mouse)
{
	if (enabled)
	{
		for (size_t i = 0; i < pcursors.size() && active_index == -1; i++)
		{
			if (pcursors[i]->MouseTest(mouse))
				active_index = i;
		}

		if (active_index != -1)
			SetCapture(hwnd);
	}

	return active_index != -1;
}

/*--------------------------------------------------------------------
CursorsCollection::OnMouseLButtonUp()
--------------------------------------------------------------------*/
BOOL CursorsCollection::OnMouseLButtonUp(POINT mouse)
{
	BOOL result = active_index != -1;

	if (enabled)
	{
		if (active_index != -1)
		{
			active_index = -1;
			ReleaseCapture();
		}
	}

	return result;
}

/*--------------------------------------------------------------------
CursorsCollection::OnMouseMove()
--------------------------------------------------------------------*/
BOOL CursorsCollection::OnMouseMove(POINT mouse)
{
	if (enabled)
	{
		if (active_index != -1)
			pcursors[active_index]->OnMouseMove(mouse);
	}

	return active_index != -1;
}

/*////////////////////////////////////////////////////////////////////
Cursor
////////////////////////////////////////////////////////////////////*/

/*--------------------------------------------------------------------
Cursor::Cursor()
--------------------------------------------------------------------*/
Cursor::Cursor(char name, REAL pos)
{
	ASSERT(pos >= 0 && pos <= 1);

	Cursor::name = name;
	Cursor::pos = pos;

	ZeroMemory(&rect, sizeof(RECT));

	hfont = GetFont();
}

/*--------------------------------------------------------------------
Cursor::~Cursor()
--------------------------------------------------------------------*/
Cursor::~Cursor()
{
	DeleteObject(hfont);
}

/*--------------------------------------------------------------------
Cursor::MouseTest()
--------------------------------------------------------------------*/
BOOL Cursor::MouseTest(POINT mouse)
{
	POINT_VECTOR v;
	GetPolygonCoords(v);
	HRGN hrgn = CreatePolygonRgn(&*v.begin(), v.size(), WINDING);
	ASSERT(hrgn != NULL);

	BOOL is_inside = PtInRegion(hrgn, mouse.x, mouse.y);

	DeleteObject(hrgn);

	return is_inside;
}

/*--------------------------------------------------------------------
Cursor::Draw()
--------------------------------------------------------------------*/
void Cursor::Draw(HDC dc, BOOL is_selected)
{
	const int LINE_WIDTH = 1;
	COLORREF line_color = is_selected ?
		RGB(0xFF, 0xFF, 0xFF) : RGB(0xE0, 0xE0, 0x00);
	COLORREF text_color = is_selected ?
		RGB(0x00, 0x00, 0x00) : RGB(0x00, 0xFF, 0xFF);
	COLORREF bk_color = is_selected ?
		RGB(0xFF, 0xFF, 0xFF) : RGB(0x00, 0x00, 0x00);

	HBRUSH old_brush = (HBRUSH)SelectObject(dc,
		CreateSolidBrush(bk_color));
	HPEN old_pen = (HPEN)SelectObject(dc,
		CreatePen(PS_SOLID, LINE_WIDTH, line_color));
	HFONT old_font = (HFONT)SelectObject(dc, hfont);
	
	int old_bk_mode = SetBkMode(dc, TRANSPARENT);
	COLORREF old_text_color = SetTextColor(dc, text_color);

	/* ������ �������, �� ������� "���������" �����. */
	POINT_VECTOR v;
	GetPolygonCoords(v);
	Polygon(dc, &*v.begin(), v.size());

	POINT center;
	GetCenterCoords(center);

	/* ������ �������� - "���" �������. */
	SIZE name_extent;
	VERIFY(GetTextExtentPoint32(dc, &name, 1, &name_extent));
	TextOut(dc, 
		center.x - (name_extent.cx >> 1), 
		center.y - (name_extent.cy >> 1), 
		&name, 1);

	DeleteObject(SelectObject(dc,
		CreatePen(PS_DOT, LINE_WIDTH, line_color)));

	/* ������ ���������� ������, �.�. �����. */
	int old_r2_mode = SetROP2(dc, R2_MERGEPEN);
	POINT p1, p2;
	GetLineCoords(p1, p2);
	MoveToEx(dc, p1.x, p1.y, NULL);
	LineTo(dc, p2.x, p2.y);
	SetROP2(dc, old_r2_mode);

	SetTextColor(dc, old_text_color);
	SetBkMode(dc, old_bk_mode);
	DeleteObject(SelectObject(dc, old_pen));
	DeleteObject(SelectObject(dc, old_brush));
	SelectObject(dc, old_font);
}

/*--------------------------------------------------------------------
Cursor::GetFont()
--------------------------------------------------------------------*/
HFONT Cursor::GetFont()
{
	LOGFONT lf;
	ZeroMemory(&lf, sizeof(LOGFONT));

	lf.lfHeight = 15;
	lf.lfCharSet = ANSI_CHARSET;
	lf.lfOutPrecision = OUT_DEFAULT_PRECIS;
	lf.lfClipPrecision = CLIP_DEFAULT_PRECIS;
	lf.lfQuality = DEFAULT_QUALITY;
	lf.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	lf.lfWeight = FW_THIN;

	HFONT font = CreateFontIndirect(&lf);
	ASSERT(font != NULL);
	return font;
}

/*////////////////////////////////////////////////////////////////////
CursorV
////////////////////////////////////////////////////////////////////*/

/*--------------------------------------------------------------------
CursorV::GetCenterCoords()
--------------------------------------------------------------------*/
void CursorV::GetCenterCoords(POINT& p)
{
	p.x = rect.left + Globals::Round((rect.right - rect.left)*pos);
	p.y = rect.top - RSIZE;
}

/*--------------------------------------------------------------------
CursorV::GetLineCoords()
--------------------------------------------------------------------*/
void CursorV::GetLineCoords(POINT& p1, POINT& p2)
{
	POINT p;
	GetCenterCoords(p);

	p1.x = p.x;
	p1.y = rect.top;
	p2.x = p.x;
	p2.y = rect.bottom;
}

/*--------------------------------------------------------------------
CursorV::GetLineCoords()
--------------------------------------------------------------------*/
void CursorV::GetPolygonCoords(POINT_VECTOR& v)
{
	POINT center;
	GetCenterCoords(center);

	POINT p;

	p.x = center.x - RSIZE/2;
	p.y = center.y + RSIZE/2;
	v.push_back(p);
	/*---*/
	p.x = center.x - RSIZE/2;
	p.y = center.y - RSIZE/2;
	v.push_back(p);
	/*---*/
	p.x = center.x + RSIZE/2;
	p.y = center.y - RSIZE/2;
	v.push_back(p);
	/*---*/
	p.x = center.x + RSIZE/2;
	p.y = center.y + RSIZE/2;
	v.push_back(p);
	/*---*/
	p.x = center.x;
	p.y = center.y + RSIZE;
	v.push_back(p);
}

/*--------------------------------------------------------------------
CursorV::OnMouseMove()
--------------------------------------------------------------------*/
void CursorV::OnMouseMove(POINT mouse)
{
	if (rect.right > rect.left)
	{
		pos = (REAL)(mouse.x - rect.left)/(rect.right - rect.left);
		if (pos < 0) pos = 0; else if (pos > 1) pos = 1;
	}
}

/*////////////////////////////////////////////////////////////////////
CursorH
////////////////////////////////////////////////////////////////////*/

/*--------------------------------------------------------------------
CursorH::GetCenterCoords()
--------------------------------------------------------------------*/
void CursorH::GetCenterCoords(POINT& p)
{
	p.x = rect.right + RSIZE;
	p.y = rect.top  + Globals::Round((rect.bottom - rect.top)*pos);
}

/*--------------------------------------------------------------------
CursorH::GetLineCoords()
--------------------------------------------------------------------*/
void CursorH::GetLineCoords(POINT& p1, POINT& p2)
{
	POINT p;
	GetCenterCoords(p);

	p1.x = rect.left;
	p1.y = p.y;
	p2.x = rect.right;
	p2.y = p.y;
}

/*--------------------------------------------------------------------
CursorH::GetLineCoords()
--------------------------------------------------------------------*/
void CursorH::GetPolygonCoords(POINT_VECTOR& v)
{
	POINT center;
	GetCenterCoords(center);

	POINT p;

	p.x = center.x - RSIZE/2;
	p.y = center.y - RSIZE/2;
	v.push_back(p);
	/*---*/
	p.x = center.x + RSIZE/2;
	p.y = center.y - RSIZE/2;
	v.push_back(p);
	/*---*/
	p.x = center.x + RSIZE/2;
	p.y = center.y + RSIZE/2;
	v.push_back(p);
	/*---*/
	p.x = center.x - RSIZE/2;
	p.y = center.y + RSIZE/2;
	v.push_back(p);
	/*---*/
	p.x = center.x - RSIZE;
	p.y = center.y;
	v.push_back(p);
}

/*--------------------------------------------------------------------
CursorH::OnMouseMove()
--------------------------------------------------------------------*/
void CursorH::OnMouseMove(POINT mouse)
{
	if (rect.bottom > rect.top)
	{
		pos = (REAL)(mouse.y - rect.top)/(rect.bottom - rect.top);
		if (pos < 0) pos = 0; else if (pos > 1) pos = 1;
	}
}
