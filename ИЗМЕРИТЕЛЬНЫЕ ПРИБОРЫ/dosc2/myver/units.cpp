#include "precomp.h"
#include "global.h"
#include "units.h"

/*--------------------------------------------------------------------
Units::Setup()
--------------------------------------------------------------------*/
void Units::Setup(REAL x, const char* units_name)
{
	/* �� 1E-18 �� 1E+18. */
	static char* postfix[] = {
		"a", "f", "p", "n", "u", "m",
		"",
		"k", "M", "G", "T", "P", "E" };
	
	static REAL m[] = {
		1E-18, 1E-15, 1E-12, 1E-9, 1E-6, 1E-3,
		1,
		1E+3, 1E+6, 1E+9, 1E+12, 1E+15, 1E+18 };

	int i;
	int size = sizeof(m)/sizeof(REAL);
	REAL absx = ABS(x);

	strcpy(units, postfix[size - 1]);
	divisor = m[size - 1];

	if (absx < m[0]/1E+3)
	{
		/* ������� ��������� ��������. */

		*units = '\0';
		divisor = 1;
	}
	else
	{
		for (i = 1; i < size - 1; i++)
			if (absx < m[i])
			{
				strcpy(units, postfix[i - 1]);
				divisor = m[i - 1];
				break;
			}
	}
	
	strcat(units, units_name);
}

/*--------------------------------------------------------------------
Units::atof()
--------------------------------------------------------------------*/
BOOL Units::atof(const char* text, REAL* pvalue /* = NULL */)
{
	if (pvalue != NULL) *pvalue = 0;

	char buffer[256];
	/* �������� ������� � ��������� � ������ � ����� ������. */
	while (*text == ' ' || *text == '\t') text++;
	if (strlen(text) >= sizeof(buffer)) return FALSE;
	strcpy(buffer, text);
	/*---*/
	char* p = buffer + strlen(buffer) - 1;
	while (*p == ' ' || *p == '\t')
	{
		*p = '\0';
		p--;
	}

	typedef map<char, REAL> UMAP;
	UMAP map;
	map.insert(UMAP::value_type('a', 1e-18));
	map.insert(UMAP::value_type('f', 1e-15));
	map.insert(UMAP::value_type('p', 1e-12));
	map.insert(UMAP::value_type('n', 1e-9));
	map.insert(UMAP::value_type('u', 1e-6));
	map.insert(UMAP::value_type('m', 1e-3));
	map.insert(UMAP::value_type('k', 1e+3));
	map.insert(UMAP::value_type('M', 1e+6));
	map.insert(UMAP::value_type('G', 1e+9));
	map.insert(UMAP::value_type('T', 1e+12));
	map.insert(UMAP::value_type('P', 1e+15));
	map.insert(UMAP::value_type('E', 1e+18));

	char suffix = buffer[strlen(buffer) - 1];
	REAL factor = 1;
	if (map.find(suffix) != map.end())
	{
		factor = map[suffix];
		buffer[strlen(buffer) - 1] = '\0';
	}

	if (!Globals::IsFloat(buffer)) return FALSE;
	if (pvalue != NULL) *pvalue = ::atof(buffer)*factor;

	return TRUE;
}
