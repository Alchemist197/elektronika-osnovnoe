function faqChangeIcon (el, viewClass, hideClass, viewTitle, hideTitle ) {

	if ( el.className.search(viewClass) !== -1 )
	{
		el.className	= el.className.replace(viewClass, hideClass);
		el.title			= hideTitle;
	}
	else
	{
		el.className	= el.className.replace(hideClass, viewClass)
		el.title			= viewTitle;
	}

}
