SEMICONDUCTORS :
TDA8703                       2
LM741                         2
LM6361N                       4
AD744                         2
LM319                         1
74HCT574                      2
ULN2003A                      1
74HC257                       2
AM7202A-35RC (Skinny DIP)     2
74HC107                       1
74HC86                        1
74HC74                        1
74HC20                        1
74HC390                       2
74HC151                       1
20MHz XTAL OSC. MODULE        1
7805 REGULATOR (TO-220)       1
7812 REGULATOR (TO-220)       1
7912 REGULATOR (TO-220)       1
1N914 DIODE                   6
1N4002 DIODE                  2
5mm RED LED                   1
8-PIN DUAL WIPE IC SOCKETS    8
14-PIN DUAL WIPE IC SOCKETS   10
16-PIN DUAL WIPE IC SOCKETS   6
20-PIN DUAL WIPE IC SOCKETS   2
24-PIN DUAL WIPE IC SOCKETS   2

CAPACITORS :
100nF 0.2 PITCH MONO          33
10uF 0.2 PITCH 16V TANTULUM   4
470uF 25V ELECTRO             1
2200uF 25V ELECTRO            1
5-30pF TRIMMER CAP            2
150pF 0.2 PITCH CERAMIC       2
5.6pF 0.1 PITCH CERAMIC       2                 

RESISTORS :
47R 1%                        3
1K 1%                         14
68R 1%                        2
470R 1%                       4
220R 1%                       2
390R 1%                       2
1K5 1%                        2
2K2 1%                        2
4K7 1%                        2
6K8 1%                        2
100K 1%                       3
220K 1%                       2
680K 1%                       2
10K LINEAR POT                2
1K LINEAR POT                 1

MISC :
SPDT CENTRE-OFF SWITCH        2
12V DPDT MINI DIL PCB RELAY   
(MATSUSHITA TQ2 OR SIMILAR)   6
KNOBS                         3
SMALL TO-220 HEATSINK         1
IEC FUSED PANEL MOUNT SOCKET  1
BNC PANEL SOCKET              2
D25 IDC SOCKET                1
26 WAY IDC HEADER SOCKET      1
26 WAY DUAL ROW HEADER PINS   1
12.6VAC 500mA TRANSFORMER     1
260(W)x180(D)x65(H) CASE
(WITH ALUMINIUM FRONT PANEL)  1
BLANK COPPER CLAD PCB
(100mm x 100mm)               1
PCB STANDOFFS                 4
RG-174 COAXIAL CABLE          300mm
26 WAY RIBBON CABLE           150mm
TINNED COPPER WIRE
PCB (Code DSOA331)            1
